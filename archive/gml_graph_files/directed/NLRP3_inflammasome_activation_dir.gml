# generated with VANTED V2.8.0 at Tue Apr 27 20:00:52 CEST 2021
graph [
  background_coloring "true"
  cluster_colors "255,128,128,255:0,0,0,255;255,237,128,255:0,0,0,255;164,255,128,255:0,0,0,255;128,255,200,255:0,0,0,255;128,200,255,255:0,0,0,255;164,128,255,255:0,0,0,255;255,128,237,255:0,0,0,255"
  clusterbackground_fill_outer_region "false"
  clusterbackground_grid 50.0
  clusterbackground_low_alpha 0.2
  clusterbackground_radius 200.0
  clusterbackground_space_fill "true"
  graphbackgroundcolor "#ffffff"
  sbml [
    model_meta_id "NLRP3_Activation_COVID19"
    model_non_rdf_annotation ""
    model_notes ""
    model_sbml_id "NLRP3_Activation_COVID19"
    namespace "xmlns:celldesigner=\"http://www.sbml.org/2001/ns/celldesigner\" "
    sbml_level "2"
    sbml_version "4"
  ]
  sbml_compartment_default [
    sbml_compartment_default_id "default"
    sbml_compartment_default_meta_id "default"
    sbml_compartment_default_non_rdf_annotation ""
    sbml_compartment_default_notes ""
    sbml_compartment_default_size "1.0"
    sbml_compartment_default_units "volume"
  ]
  sbml_compartment_s_id_ca1 [
    sbml_compartment_s_id_ca1_annotation ""
    sbml_compartment_s_id_ca1_id "s_id_ca1"
    sbml_compartment_s_id_ca1_meta_id "s_id_ca1"
    sbml_compartment_s_id_ca1_name "nucleus"
    sbml_compartment_s_id_ca1_non_rdf_annotation ""
    sbml_compartment_s_id_ca1_notes ""
    sbml_compartment_s_id_ca1_outside "s_id_ca2"
    sbml_compartment_s_id_ca1_size "1.0"
    sbml_compartment_s_id_ca1_units "volume"
  ]
  sbml_compartment_s_id_ca2 [
    sbml_compartment_s_id_ca2_annotation ""
    sbml_compartment_s_id_ca2_id "s_id_ca2"
    sbml_compartment_s_id_ca2_meta_id "s_id_ca2"
    sbml_compartment_s_id_ca2_name "cell"
    sbml_compartment_s_id_ca2_non_rdf_annotation ""
    sbml_compartment_s_id_ca2_notes ""
    sbml_compartment_s_id_ca2_outside "default"
    sbml_compartment_s_id_ca2_size "1.0"
    sbml_compartment_s_id_ca2_units "volume"
  ]
  sbml_compartment_s_id_ca3 [
    sbml_compartment_s_id_ca3_annotation ""
    sbml_compartment_s_id_ca3_id "s_id_ca3"
    sbml_compartment_s_id_ca3_meta_id "s_id_ca3"
    sbml_compartment_s_id_ca3_name "Golgi_space_apparatus"
    sbml_compartment_s_id_ca3_non_rdf_annotation ""
    sbml_compartment_s_id_ca3_notes ""
    sbml_compartment_s_id_ca3_outside "s_id_ca2"
    sbml_compartment_s_id_ca3_size "1.0"
    sbml_compartment_s_id_ca3_units "volume"
  ]
  sbml_compartment_s_id_ca4 [
    sbml_compartment_s_id_ca4_annotation ""
    sbml_compartment_s_id_ca4_id "s_id_ca4"
    sbml_compartment_s_id_ca4_meta_id "s_id_ca4"
    sbml_compartment_s_id_ca4_name "endoplasmic_space_reticulum_minus_Golgi_space_intermediate_space_compartment"
    sbml_compartment_s_id_ca4_non_rdf_annotation ""
    sbml_compartment_s_id_ca4_notes ""
    sbml_compartment_s_id_ca4_outside "s_id_ca2"
    sbml_compartment_s_id_ca4_size "1.0"
    sbml_compartment_s_id_ca4_units "volume"
  ]
  sbml_compartment_s_id_ca5 [
    sbml_compartment_s_id_ca5_annotation ""
    sbml_compartment_s_id_ca5_id "s_id_ca5"
    sbml_compartment_s_id_ca5_meta_id "s_id_ca5"
    sbml_compartment_s_id_ca5_name "mitochondrial_space_matrix"
    sbml_compartment_s_id_ca5_non_rdf_annotation ""
    sbml_compartment_s_id_ca5_notes ""
    sbml_compartment_s_id_ca5_outside "s_id_ca6"
    sbml_compartment_s_id_ca5_size "1.0"
    sbml_compartment_s_id_ca5_units "volume"
  ]
  sbml_compartment_s_id_ca6 [
    sbml_compartment_s_id_ca6_annotation ""
    sbml_compartment_s_id_ca6_id "s_id_ca6"
    sbml_compartment_s_id_ca6_meta_id "s_id_ca6"
    sbml_compartment_s_id_ca6_name "mitochondrion"
    sbml_compartment_s_id_ca6_non_rdf_annotation ""
    sbml_compartment_s_id_ca6_notes ""
    sbml_compartment_s_id_ca6_outside "s_id_ca2"
    sbml_compartment_s_id_ca6_size "1.0"
    sbml_compartment_s_id_ca6_units "volume"
  ]
  sbml_unit_definition_1 [
    sbml_unit_definition_1_id "time"
    sbml_unit_definition_1_name "time"
    sbml_unit_definition_1_sub_unit_1_ "(1.0 * 10^0 * second)^1.0"
    sbml_unit_definition_1unit "(1.0 * 10^0 * second)^1.0"
  ]
  sbml_unit_definition_2 [
    sbml_unit_definition_2_id "length"
    sbml_unit_definition_2_name "length"
    sbml_unit_definition_2_sub_unit_1_ "(1.0 * 10^0 * metre)^1.0"
    sbml_unit_definition_2unit "(1.0 * 10^0 * metre)^1.0"
  ]
  sbml_unit_definition_3 [
    sbml_unit_definition_3_id "substance"
    sbml_unit_definition_3_name "substance"
    sbml_unit_definition_3_sub_unit_1_ "(1.0 * 10^0 * mole)^1.0"
    sbml_unit_definition_3unit "(1.0 * 10^0 * mole)^1.0"
  ]
  sbml_unit_definition_4 [
    sbml_unit_definition_4_id "area"
    sbml_unit_definition_4_name "area"
    sbml_unit_definition_4_sub_unit_1_ "(1.0 * 10^0 * metre)^2.0"
    sbml_unit_definition_4unit "(1.0 * 10^0 * metre)^2.0"
  ]
  sbml_unit_definition_5 [
    sbml_unit_definition_5_id "volume"
    sbml_unit_definition_5_name "volume"
    sbml_unit_definition_5_sub_unit_1_ "(1.0 * 10^0 * litre)^1.0"
    sbml_unit_definition_5unit "(1.0 * 10^0 * litre)^1.0"
  ]
  directed 1
  node [
    id 1
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 200.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nadph__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa153"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa153"
      minerva_fullName "NADPH"
      minerva_name "NADPH"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16474"
      minerva_ref_type__resource1 "CHEBI__CHEBI:16474"
      minerva_synonyms "NADPH; NADPH DIHYDRO-NICOTINAMIDE-ADENINE-DINUCLEOTIDE PHOSPHATE; Reduced nicotinamide adenine dinucleotide phosphate; TPNH; dihydronicotinamide-adenine dinucleotide phosphate; reduced nicotinamide-adenine dinucleotide phosphate"
      minerva_type "Simple molecule"
      minerva_x 684.0
      minerva_y 1790.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa153"
      species_meta_id "s_id_sa153"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 2
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 300.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "pamps__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa90"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa90"
      minerva_fullName "pattern recognition receptor signaling pathway"
      minerva_name "PAMPs"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0002221"
      minerva_ref_type__resource1 "GO__GO:0002221"
      minerva_type "Phenotype"
      minerva_x 1504.0
      minerva_y 1570.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa90"
      species_meta_id "s_id_sa90"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 3
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 400.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "reactive_space_oxygen_space_species__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa1"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa1"
      minerva_elementId2 "sa2"
      minerva_fullName "reactive oxygen species"
      minerva_name "Reactive Oxygen Species"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:26523"
      minerva_ref_type__resource1 "CHEBI__CHEBI:26523"
      minerva_synonyms "ROS"
      minerva_type "Simple molecule"
      minerva_x 1254.0
      minerva_x2 1154.0
      minerva_y 1460.0
      minerva_y2 940.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa1"
      species_meta_id "s_id_sa1"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 4
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 500.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "asc__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa104"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa104"
      minerva_name "ASC"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/29108"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PYCARD"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/Q9ULZ3"
      minerva_ref_type__resource1 "ENTREZ__29108"
      minerva_ref_type__resource2 "HGNC_SYMBOL__PYCARD"
      minerva_ref_type__resource3 "UNIPROT__Q9ULZ3"
      minerva_type "Protein"
      minerva_x 1004.0
      minerva_y 2008.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa104"
      species_meta_id "s_id_sa104"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 5
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 600.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nf_minus_kb_space_complex__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa22"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa22"
      minerva_name "Nf-KB Complex"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/Q00653"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P19838"
      minerva_ref_type__resource1 "UNIPROT__Q00653"
      minerva_ref_type__resource2 "UNIPROT__P19838"
      minerva_type "Complex"
      minerva_x 2664.0
      minerva_y 1030.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa22"
      species_meta_id "s_id_csa22"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 6
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 700.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ca2_plus___golgi_space_apparatus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa141"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa141"
      minerva_fullName "calcium(2+)"
      minerva_name "Ca2+"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:29108"
      minerva_ref_type__resource1 "CHEBI__CHEBI:29108"
      minerva_synonyms "CALCIUM ION; Ca(2+); Ca(2+); Ca2+; calcium, doubly charged positive ion"
      minerva_type "Ion"
      minerva_x 990.875
      minerva_y 1257.1875
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "Golgi_space_apparatus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa141"
      species_meta_id "s_id_sa141"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 7
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 800.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "thioredoxin:txnip__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa1"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa1"
      minerva_name "Thioredoxin:TXNIP"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P10599"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q9H3M7"
      minerva_ref_type__resource1 "UNIPROT__P10599"
      minerva_ref_type__resource2 "UNIPROT__Q9H3M7"
      minerva_type "Complex"
      minerva_x 1454.0
      minerva_y 727.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa1"
      species_meta_id "s_id_csa1"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 8
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 900.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "e__golgi_space_apparatus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa140"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa140"
      minerva_name "E"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P59637"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/1489671"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=E"
      minerva_ref_type__resource1 "UNIPROT__P59637"
      minerva_ref_type__resource2 "ENTREZ__1489671"
      minerva_ref_type__resource3 "HGNC_SYMBOL__E"
      minerva_type "Protein"
      minerva_x 990.875
      minerva_y 1317.1875
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "Golgi_space_apparatus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa140"
      species_meta_id "s_id_sa140"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 9
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1000.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mavs__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa102"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa102"
      minerva_name "MAVS"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/Q7Z434"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAVS"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAVS"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000088888"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/57506"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/57506"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/29233"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_020746"
      minerva_ref_type__resource1 "UNIPROT__Q7Z434"
      minerva_ref_type__resource2 "HGNC_SYMBOL__MAVS"
      minerva_ref_type__resource3 "HGNC_SYMBOL__MAVS"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000088888"
      minerva_ref_type__resource5 "ENTREZ__57506"
      minerva_ref_type__resource6 "ENTREZ__57506"
      minerva_ref_type__resource7 "HGNC__29233"
      minerva_ref_type__resource8 "REFSEQ__NM_020746"
      minerva_type "Protein"
      minerva_x 1089.0
      minerva_y 2160.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa102"
      species_meta_id "s_id_sa102"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 10
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1100.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nadp_plus___cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa155"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa155"
      minerva_fullName "NADP(+)"
      minerva_name "NADP+"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18009"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18009"
      minerva_synonyms "NADP; NADP+; Nicotinamide adenine dinucleotide phosphate; TPN; Triphosphopyridine nucleotide; beta-Nicotinamide adenine dinucleotide phosphate; beta-nicotinamide adenine dinucleotide phosphate; oxidized nicotinamide-adenine dinucleotide phosphate"
      minerva_type "Simple molecule"
      minerva_x 934.0
      minerva_y 1830.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa155"
      species_meta_id "s_id_sa155"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 11
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 1200.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s876__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa162"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa162"
      minerva_name "s876"
      minerva_type "Degraded"
      minerva_x 964.0
      minerva_y 1460.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa162"
      species_meta_id "s_id_sa162"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 12
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 200.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "proil_minus_1b__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa15"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa15"
      minerva_name "proIL-1B"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL1B"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P01584"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/3553"
      minerva_ref_type__resource1 "HGNC_SYMBOL__IL1B"
      minerva_ref_type__resource2 "UNIPROT__P01584"
      minerva_ref_type__resource3 "ENTREZ__3553"
      minerva_type "Protein"
      minerva_x 2674.0
      minerva_y 1740.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa15"
      species_meta_id "s_id_sa15"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 13
    zlevel -1

    cluster [
      cluster "s_id_ca4"
    ]
    graphics [
      x 300.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "e__endoplasmic_space_reticulum_minus_golgi_space_intermediate_space_compartment__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa146"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa146"
      minerva_name "E"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P59637"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=694009"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/1489671"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=E"
      minerva_ref_type__resource1 "UNIPROT__P59637"
      minerva_ref_type__resource2 "TAXONOMY__694009"
      minerva_ref_type__resource3 "ENTREZ__1489671"
      minerva_ref_type__resource4 "HGNC_SYMBOL__E"
      minerva_type "Protein"
      minerva_x 1154.0
      minerva_y 1192.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca4"
      compartment_name "endoplasmic_space_reticulum_minus_Golgi_space_intermediate_space_compartment"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa146"
      species_meta_id "s_id_sa146"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 14
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 400.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "co__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa158"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa158"
      minerva_fullName "carbon monoxide"
      minerva_name "CO"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17245"
      minerva_ref_type__resource1 "CHEBI__CHEBI:17245"
      minerva_synonyms "C#O; CARBON MONOXIDE; CO; CO; Carbon monoxide; [CO]"
      minerva_type "Simple molecule"
      minerva_x 1074.0
      minerva_y 1580.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa158"
      species_meta_id "s_id_sa158"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 15
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 500.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "biliverdin__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa156"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa156"
      minerva_fullName "biliverdin"
      minerva_name "Biliverdin"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17033"
      minerva_ref_type__resource1 "CHEBI__CHEBI:17033"
      minerva_synonyms "8,12-bis(2-carboxyethyl)-2,7,13,17-tetramethyl-3,18-divinylbilin-1(19)(21H,24H)-dione; BILIVERDINE IX ALPHA; Biliverdin; Biliverdin IX alpha; Biliverdin IXalpha; Biliverdine"
      minerva_type "Simple molecule"
      minerva_x 954.0
      minerva_y 1770.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa156"
      species_meta_id "s_id_sa156"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 16
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 600.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf3a__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa147"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa3"
      minerva_elementId2 "sa147"
      minerva_name "Orf3a"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/P59632"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/1489669"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=694009"
      minerva_ref_type__resource1 "UNIPROT__P59632"
      minerva_ref_type__resource2 "ENTREZ__1489669"
      minerva_ref_type__resource3 "TAXONOMY__694009"
      minerva_type "Protein"
      minerva_x 1304.0
      minerva_x2 2464.0
      minerva_y 2230.0
      minerva_y2 470.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa147"
      species_meta_id "s_id_sa147"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 17
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 700.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nlrp3__cell__activated__2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa101"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa101"
      minerva_former_symbols "C1orf7; CIAS1; DFNA34"
      minerva_homodimer "2"
      minerva_name "NLRP3"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/Q96P20"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/16400"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_004895"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000162711"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NLRP3"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NLRP3"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/114548"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/114548"
      minerva_ref_type__resource1 "UNIPROT__Q96P20"
      minerva_ref_type__resource2 "HGNC__16400"
      minerva_ref_type__resource3 "REFSEQ__NM_004895"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000162711"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NLRP3"
      minerva_ref_type__resource6 "HGNC_SYMBOL__NLRP3"
      minerva_ref_type__resource7 "ENTREZ__114548"
      minerva_ref_type__resource8 "ENTREZ__114548"
      minerva_structuralState "Activated"
      minerva_type "Protein"
      minerva_x 1864.0
      minerva_y 1620.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa101"
      species_meta_id "s_id_sa101"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 19
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 900.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nlrp3_space_elicitor_space_small_space_molecules__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa135"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa135"
      minerva_fullName "asbestos"
      minerva_name "NLRP3 Elicitor Small Molecules"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:46661"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:30563"
      minerva_ref_link3 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16336"
      minerva_ref_type__resource1 "CHEBI__CHEBI:46661"
      minerva_ref_type__resource2 "CHEBI__CHEBI:30563"
      minerva_ref_type__resource3 "CHEBI__CHEBI:16336"
      minerva_synonyms "Asbest; asbesto; asbestos"
      minerva_type "Simple molecule"
      minerva_x 1574.0
      minerva_y 1740.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa135"
      species_meta_id "s_id_sa135"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 20
    zlevel -1

    cluster [
      cluster "default"
    ]
    graphics [
      x 1000.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "il_minus_1b__default__activated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa21"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa21"
      minerva_name "IL-1B"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL1B"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P01584"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/3553"
      minerva_ref_type__resource1 "HGNC_SYMBOL__IL1B"
      minerva_ref_type__resource2 "UNIPROT__P01584"
      minerva_ref_type__resource3 "ENTREZ__3553"
      minerva_structuralState "activated"
      minerva_type "Protein"
      minerva_x 3194.0
      minerva_y 1740.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "default"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa21"
      species_meta_id "s_id_sa21"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 21
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1100.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "p105__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa151"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa151"
      minerva_name "p105"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4790"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKB1"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P19838"
      minerva_ref_type__resource1 "ENTREZ__4790"
      minerva_ref_type__resource2 "HGNC_SYMBOL__NFKB1"
      minerva_ref_type__resource3 "UNIPROT__P19838"
      minerva_type "Protein"
      minerva_x 2314.0
      minerva_y 790.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa151"
      species_meta_id "s_id_sa151"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 22
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1200.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "il_minus_1b__cell__activated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa17"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa17"
      minerva_name "IL-1B"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL1B"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P01584"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/3553"
      minerva_ref_type__resource1 "HGNC_SYMBOL__IL1B"
      minerva_ref_type__resource2 "UNIPROT__P01584"
      minerva_ref_type__resource3 "ENTREZ__3553"
      minerva_structuralState "activated"
      minerva_type "Protein"
      minerva_x 2974.0
      minerva_y 1737.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa17"
      species_meta_id "s_id_sa17"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 23
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 200.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "heme__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa152"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa152"
      minerva_fullName "heme"
      minerva_name "Heme"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:30413"
      minerva_ref_type__resource1 "CHEBI__CHEBI:30413"
      minerva_synonyms "Haem; haem; haem; haeme; hem; heme; heme; hemos"
      minerva_type "Simple molecule"
      minerva_x 614.0
      minerva_y 1770.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa152"
      species_meta_id "s_id_sa152"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 24
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 300.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "p65__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa86"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa86"
      minerva_name "p65"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKB2"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/4791"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/Q00653"
      minerva_ref_type__resource1 "HGNC_SYMBOL__NFKB2"
      minerva_ref_type__resource2 "ENTREZ__4791"
      minerva_ref_type__resource3 "UNIPROT__Q00653"
      minerva_type "Protein"
      minerva_x 2774.0
      minerva_y 790.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa86"
      species_meta_id "s_id_sa86"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 25
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 400.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nf_minus_kb_space_complex:ikba__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa21"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa21"
      minerva_name "NF-KB COMPLEX:IKBA"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P19838"
      minerva_ref_type__resource1 "UNIPROT__P19838"
      minerva_type "Complex"
      minerva_x 2950.0
      minerva_y 1030.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa21"
      species_meta_id "s_id_csa21"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 26
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 500.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "fe2_plus___cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa163"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa163"
      minerva_fullName "iron(2+)"
      minerva_name "Fe2+"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:29033"
      minerva_ref_type__resource1 "CHEBI__CHEBI:29033"
      minerva_synonyms "FE (II) ION; Fe(2+); Fe(II); Fe2+; Ferrous ion; Iron(2+); iron ion(2+)"
      minerva_type "Ion"
      minerva_x 874.0
      minerva_y 1850.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa163"
      species_meta_id "s_id_sa163"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 27
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 600.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "proil_minus_18__cell__aa 1-36__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa18"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa18"
      minerva_name "proIL-18"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_structuralState "AA 1-36"
      minerva_type "Protein"
      minerva_x 2949.0
      minerva_y 1608.25
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa18"
      species_meta_id "s_id_sa18"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 28
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 700.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "damps__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa91"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa91"
      minerva_fullName "pattern recognition receptor signaling pathway"
      minerva_name "DAMPs"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0002221"
      minerva_ref_type__resource1 "GO__GO:0002221"
      minerva_type "Phenotype"
      minerva_x 1504.0
      minerva_y 1500.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa91"
      species_meta_id "s_id_sa91"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 29
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 800.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "p105__cell__ubiquitinated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa84"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa84"
      minerva_name "p105"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4790"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKB1"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P19838"
      minerva_ref_type__resource1 "ENTREZ__4790"
      minerva_ref_type__resource2 "HGNC_SYMBOL__NFKB1"
      minerva_ref_type__resource3 "UNIPROT__P19838"
      minerva_state1 "UBIQUITINATED"
      minerva_type "Protein"
      minerva_x 2464.0
      minerva_y 790.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa84"
      species_meta_id "s_id_sa84"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 30
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 900.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nlrp3__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa92"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa92"
      minerva_former_symbols "C1orf7; CIAS1; DFNA34"
      minerva_name "NLRP3"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/Q96P20"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/16400"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_004895"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000162711"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NLRP3"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NLRP3"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/114548"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/114548"
      minerva_ref_type__resource1 "UNIPROT__Q96P20"
      minerva_ref_type__resource2 "HGNC__16400"
      minerva_ref_type__resource3 "REFSEQ__NM_004895"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000162711"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NLRP3"
      minerva_ref_type__resource6 "HGNC_SYMBOL__NLRP3"
      minerva_ref_type__resource7 "ENTREZ__114548"
      minerva_ref_type__resource8 "ENTREZ__114548"
      minerva_type "Protein"
      minerva_x 1864.0
      minerva_y 950.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa92"
      species_meta_id "s_id_sa92"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 31
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1000.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "caspase_minus_1_space_tetramer__cell__activated__2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa5"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa5"
      minerva_homodimer "2"
      minerva_name "Caspase-1 Tetramer"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/P29466"
      minerva_ref_type__resource1 "UNIPROT__P29466"
      minerva_state1 "activated"
      minerva_type "Complex"
      minerva_x 2853.5
      minerva_y 1870.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa5"
      species_meta_id "s_id_csa5"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 32
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1100.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "casp1__cell__aa 298-316__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa170"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa170"
      minerva_former_symbols "IL1BC"
      minerva_name "CASP1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/834"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/834"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.22.36"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1499"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000137752"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P29466"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CASP1"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CASP1"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_033292"
      minerva_ref_type__resource1 "ENTREZ__834"
      minerva_ref_type__resource2 "ENTREZ__834"
      minerva_ref_type__resource3 "EC__3.4.22.36"
      minerva_ref_type__resource4 "HGNC__1499"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000137752"
      minerva_ref_type__resource6 "UNIPROT__P29466"
      minerva_ref_type__resource7 "HGNC_SYMBOL__CASP1"
      minerva_ref_type__resource8 "HGNC_SYMBOL__CASP1"
      minerva_ref_type__resource9 "REFSEQ__NM_033292"
      minerva_structuralState "AA 298-316"
      minerva_type "Protein"
      minerva_x 2702.333333333333
      minerva_y 2102.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa170"
      species_meta_id "s_id_sa170"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 34
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 200.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "proil_minus_18__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa14"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa14"
      minerva_name "proIL-18"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_type "Protein"
      minerva_x 2714.0
      minerva_y 1670.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa14"
      species_meta_id "s_id_sa14"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 36
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 400.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "traf3:sars_space_orf3a__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa24"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa24"
      minerva_elementId2 "csa30"
      minerva_name "TRAF3:SARS Orf3a"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/P59632"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q13114"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=694009"
      minerva_ref_type__resource1 "UNIPROT__P59632"
      minerva_ref_type__resource2 "UNIPROT__Q13114"
      minerva_ref_type__resource3 "TAXONOMY__694009"
      minerva_type "Complex"
      minerva_x 1534.0
      minerva_x2 2384.0
      minerva_y 2240.0
      minerva_y2 640.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa24"
      species_meta_id "s_id_csa24"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 37
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 500.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nlrp3_space_elicitor_space_proteins__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa136"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa136"
      minerva_former_symbols "C1orf7; CIAS1; DFNA34"
      minerva_name "NLRP3 Elicitor Proteins"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/16400"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=hly"
      minerva_ref_link11 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NLRP3"
      minerva_ref_link12 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NLRP3"
      minerva_ref_link13 "https://www.ncbi.nlm.nih.gov/gene/114548"
      minerva_ref_link14 "https://www.ncbi.nlm.nih.gov/gene/114548"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/Q96P20"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/Q96P20"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000162711"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/351"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_004895"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P05067"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=APP"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P09616"
      minerva_ref_type__resource1 "HGNC__16400"
      minerva_ref_type__resource10 "HGNC_SYMBOL__hly"
      minerva_ref_type__resource11 "HGNC_SYMBOL__NLRP3"
      minerva_ref_type__resource12 "HGNC_SYMBOL__NLRP3"
      minerva_ref_type__resource13 "ENTREZ__114548"
      minerva_ref_type__resource14 "ENTREZ__114548"
      minerva_ref_type__resource2 "UNIPROT__Q96P20"
      minerva_ref_type__resource3 "UNIPROT__Q96P20"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000162711"
      minerva_ref_type__resource5 "ENTREZ__351"
      minerva_ref_type__resource6 "REFSEQ__NM_004895"
      minerva_ref_type__resource7 "UNIPROT__P05067"
      minerva_ref_type__resource8 "HGNC_SYMBOL__APP"
      minerva_ref_type__resource9 "UNIPROT__P09616"
      minerva_type "Protein"
      minerva_x 1690.0
      minerva_y 1670.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa136"
      species_meta_id "s_id_sa136"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 38
    zlevel -1

    cluster [
      cluster "default"
    ]
    graphics [
      x 600.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "il_minus_18__default__activated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa20"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa20"
      minerva_name "IL-18"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_structuralState "activated"
      minerva_type "Protein"
      minerva_x 3194.0
      minerva_y 1670.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "default"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa20"
      species_meta_id "s_id_sa20"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 39
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 700.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "hmox1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa161"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa161"
      minerva_name "HMOX1"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000100292"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/5013"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=HMOX1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=HMOX1"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_002133"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P09601"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/3162"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/3162"
      minerva_ref_link9 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=1.14.14.18"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000100292"
      minerva_ref_type__resource2 "HGNC__5013"
      minerva_ref_type__resource3 "HGNC_SYMBOL__HMOX1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__HMOX1"
      minerva_ref_type__resource5 "REFSEQ__NM_002133"
      minerva_ref_type__resource6 "UNIPROT__P09601"
      minerva_ref_type__resource7 "ENTREZ__3162"
      minerva_ref_type__resource8 "ENTREZ__3162"
      minerva_ref_type__resource9 "EC__1.14.14.18"
      minerva_type "Protein"
      minerva_x 837.0
      minerva_y 1690.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa161"
      species_meta_id "s_id_sa161"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 40
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 800.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nf_minus_kb_space_complex__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa23"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa23"
      minerva_name "Nf-KB Complex"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/Q00653"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P19838"
      minerva_ref_type__resource1 "UNIPROT__Q00653"
      minerva_ref_type__resource2 "UNIPROT__P19838"
      minerva_type "Complex"
      minerva_x 2664.0
      minerva_y 1280.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa23"
      species_meta_id "s_id_csa23"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 41
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 900.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "traf3__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa99"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa99"
      minerva_elementId2 "sa148"
      minerva_name "TRAF3"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRAF3"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000131323"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRAF3"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/7187"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/7187"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_145725"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q13114"
      minerva_ref_link9 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12033"
      minerva_ref_type__resource1 "HGNC_SYMBOL__TRAF3"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000131323"
      minerva_ref_type__resource3 "HGNC_SYMBOL__TRAF3"
      minerva_ref_type__resource4 "EC__2.3.2.27"
      minerva_ref_type__resource5 "ENTREZ__7187"
      minerva_ref_type__resource6 "ENTREZ__7187"
      minerva_ref_type__resource7 "REFSEQ__NM_145725"
      minerva_ref_type__resource8 "UNIPROT__Q13114"
      minerva_ref_type__resource9 "HGNC__12033"
      minerva_type "Protein"
      minerva_x 1274.0
      minerva_x2 2314.0
      minerva_y 2120.0
      minerva_y2 470.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa99"
      species_meta_id "s_id_sa99"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 42
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1000.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "txnip__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa8"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa8"
      minerva_name "TXNIP"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/16952"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/10628"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000265972"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/10628"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/Q9H3M7"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_006472"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TXNIP"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TXNIP"
      minerva_ref_type__resource1 "HGNC__16952"
      minerva_ref_type__resource2 "ENTREZ__10628"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000265972"
      minerva_ref_type__resource4 "ENTREZ__10628"
      minerva_ref_type__resource5 "UNIPROT__Q9H3M7"
      minerva_ref_type__resource6 "REFSEQ__NM_006472"
      minerva_ref_type__resource7 "HGNC_SYMBOL__TXNIP"
      minerva_ref_type__resource8 "HGNC_SYMBOL__TXNIP"
      minerva_type "Protein"
      minerva_x 1544.0
      minerva_y 867.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa8"
      species_meta_id "s_id_sa8"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 43
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1100.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "il_minus_18__cell__activated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa16"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa16"
      minerva_name "IL-18"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_structuralState "activated"
      minerva_type "Protein"
      minerva_x 2974.0
      minerva_y 1670.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa16"
      species_meta_id "s_id_sa16"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 44
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1200.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "casp1(120_minus_197):casp1(317_minus_404)__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa7"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa7"
      minerva_name "CASP1(120-197):CASP1(317-404)"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/P29466"
      minerva_ref_type__resource1 "UNIPROT__P29466"
      minerva_type "Complex"
      minerva_x 2854.0
      minerva_y 2005.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa7"
      species_meta_id "s_id_csa7"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 45
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 200.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sugt1:hsp90ab1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa4"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa4"
      minerva_name "SUGT1:HSP90AB1"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P08238"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q9Y2Z0"
      minerva_ref_type__resource1 "UNIPROT__P08238"
      minerva_ref_type__resource2 "UNIPROT__Q9Y2Z0"
      minerva_type "Complex"
      minerva_x 2064.0
      minerva_y 990.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa4"
      species_meta_id "s_id_csa4"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 46
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 300.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "txnip:nlrp3__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa3"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa3"
      minerva_name "TXNIP:NLRP3"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/Q96P20"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q9H3M7"
      minerva_ref_type__resource1 "UNIPROT__Q96P20"
      minerva_ref_type__resource2 "UNIPROT__Q9H3M7"
      minerva_type "Complex"
      minerva_x 1634.0
      minerva_y 1057.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa3"
      species_meta_id "s_id_csa3"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 47
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 400.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "hsp90ab1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa4"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa4"
      minerva_former_symbols "HSPC2; HSPCB"
      minerva_name "HSP90AB1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3326"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/3326"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P08238"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/5258"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_007355"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000096384"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=HSP90AB1"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=HSP90AB1"
      minerva_ref_type__resource1 "ENTREZ__3326"
      minerva_ref_type__resource2 "ENTREZ__3326"
      minerva_ref_type__resource3 "UNIPROT__P08238"
      minerva_ref_type__resource4 "HGNC__5258"
      minerva_ref_type__resource5 "REFSEQ__NM_007355"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000096384"
      minerva_ref_type__resource7 "HGNC_SYMBOL__HSP90AB1"
      minerva_ref_type__resource8 "HGNC_SYMBOL__HSP90AB1"
      minerva_type "Protein"
      minerva_x 2014.0
      minerva_y 830.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa4"
      species_meta_id "s_id_sa4"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 48
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 500.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ctsg__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa26"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa26"
      minerva_name "CTSG"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P08311"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/1511"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/1511"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.21.20"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/2532"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CTSG"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CTSG"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_001911"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000100448"
      minerva_ref_type__resource1 "UNIPROT__P08311"
      minerva_ref_type__resource2 "ENTREZ__1511"
      minerva_ref_type__resource3 "ENTREZ__1511"
      minerva_ref_type__resource4 "EC__3.4.21.20"
      minerva_ref_type__resource5 "HGNC__2532"
      minerva_ref_type__resource6 "HGNC_SYMBOL__CTSG"
      minerva_ref_type__resource7 "HGNC_SYMBOL__CTSG"
      minerva_ref_type__resource8 "REFSEQ__NM_001911"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000100448"
      minerva_type "Protein"
      minerva_x 2490.0
      minerva_y 1907.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa26"
      species_meta_id "s_id_sa26"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 49
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 600.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "asc:mavs__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa28"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa28"
      minerva_name "ASC:MAVS"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/Q9ULZ3"
      minerva_ref_type__resource1 "UNIPROT__Q9ULZ3"
      minerva_type "Complex"
      minerva_x 1224.0
      minerva_y 2007.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa28"
      species_meta_id "s_id_csa28"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 50
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 700.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nlrp3_space_oligomer:asc:procaspase1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa10"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa10"
      minerva_name "NLRP3 oligomer:ASC:proCaspase1"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/Q96P20"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P29466"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/Q9ULZ3"
      minerva_ref_type__resource1 "UNIPROT__Q96P20"
      minerva_ref_type__resource2 "UNIPROT__P29466"
      minerva_ref_type__resource3 "UNIPROT__Q9ULZ3"
      minerva_type "Complex"
      minerva_x 2314.0
      minerva_y 2010.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa10"
      species_meta_id "s_id_csa10"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 51
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 800.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "casp1__cell__aa 1-119__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa171"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa171"
      minerva_former_symbols "IL1BC"
      minerva_name "CASP1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/834"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/834"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.22.36"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1499"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000137752"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P29466"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CASP1"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CASP1"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_033292"
      minerva_ref_type__resource1 "ENTREZ__834"
      minerva_ref_type__resource2 "ENTREZ__834"
      minerva_ref_type__resource3 "EC__3.4.22.36"
      minerva_ref_type__resource4 "HGNC__1499"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000137752"
      minerva_ref_type__resource6 "UNIPROT__P29466"
      minerva_ref_type__resource7 "HGNC_SYMBOL__CASP1"
      minerva_ref_type__resource8 "HGNC_SYMBOL__CASP1"
      minerva_ref_type__resource9 "REFSEQ__NM_033292"
      minerva_structuralState "AA 1-119"
      minerva_type "Protein"
      minerva_x 2601.666666666666
      minerva_y 2136.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa171"
      species_meta_id "s_id_sa171"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 52
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 900.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ikba__cell__phosphorylated_phosphorylated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa82"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa82"
      minerva_name "IKBA"
      minerva_state1 "PHOSPHORYLATED"
      minerva_state2 "PHOSPHORYLATED"
      minerva_type "Protein"
      minerva_x 2792.0
      minerva_y 931.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa82"
      species_meta_id "s_id_sa82"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 53
    zlevel -1

    cluster [
      cluster "s_id_ca4"
    ]
    graphics [
      x 1000.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ca2_plus___endoplasmic_space_reticulum_minus_golgi_space_intermediate_space_compartment__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa144"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa144"
      minerva_fullName "calcium(2+)"
      minerva_name "Ca2+"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:29108"
      minerva_ref_type__resource1 "CHEBI__CHEBI:29108"
      minerva_synonyms "CALCIUM ION; Ca(2+); Ca(2+); Ca2+; calcium, doubly charged positive ion"
      minerva_type "Ion"
      minerva_x 1174.0
      minerva_y 1122.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca4"
      compartment_name "endoplasmic_space_reticulum_minus_Golgi_space_intermediate_space_compartment"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa144"
      species_meta_id "s_id_sa144"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 54
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1100.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "o2__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa154"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa154"
      minerva_fullName "dioxygen"
      minerva_name "O2"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15379"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15379"
      minerva_synonyms "Disauerstoff; E 948; E-948; E948; O2; O2; O2; OXYGEN MOLECULE; Oxygen; [OO]; dioxygene; molecular oxygen"
      minerva_type "Simple molecule"
      minerva_x 724.0
      minerva_y 1810.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa154"
      species_meta_id "s_id_sa154"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 56
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 200.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "txn__cell__oxidized__2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa175"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa175"
      minerva_homodimer "2"
      minerva_name "TXN"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7295"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/7295"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P10599"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12435"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000136810"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_001244938"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TXN"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TXN"
      minerva_ref_type__resource1 "ENTREZ__7295"
      minerva_ref_type__resource2 "ENTREZ__7295"
      minerva_ref_type__resource3 "UNIPROT__P10599"
      minerva_ref_type__resource4 "HGNC__12435"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000136810"
      minerva_ref_type__resource6 "REFSEQ__NM_001244938"
      minerva_ref_type__resource7 "HGNC_SYMBOL__TXN"
      minerva_ref_type__resource8 "HGNC_SYMBOL__TXN"
      minerva_structuralState "oxidized"
      minerva_type "Protein"
      minerva_x 1334.5
      minerva_y 1023.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa175"
      species_meta_id "s_id_sa175"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 58
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 400.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "casp1__cell__p20__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa168"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa168"
      minerva_former_symbols "IL1BC"
      minerva_name "CASP1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/834"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/834"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.22.36"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1499"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000137752"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P29466"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CASP1"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CASP1"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_033292"
      minerva_ref_type__resource1 "ENTREZ__834"
      minerva_ref_type__resource2 "ENTREZ__834"
      minerva_ref_type__resource3 "EC__3.4.22.36"
      minerva_ref_type__resource4 "HGNC__1499"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000137752"
      minerva_ref_type__resource6 "UNIPROT__P29466"
      minerva_ref_type__resource7 "HGNC_SYMBOL__CASP1"
      minerva_ref_type__resource8 "HGNC_SYMBOL__CASP1"
      minerva_ref_type__resource9 "REFSEQ__NM_033292"
      minerva_structuralState "p20"
      minerva_type "Protein"
      minerva_x 2602.583333333333
      minerva_y 2007.375
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa168"
      species_meta_id "s_id_sa168"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 59
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 500.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s878__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa164"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa164"
      minerva_name "s878"
      minerva_type "Degraded"
      minerva_x 1144.0
      minerva_y 2360.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa164"
      species_meta_id "s_id_sa164"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 60
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 600.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nlrp3:sugt1:hsp90__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa6"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa6"
      minerva_name "NLRP3:SUGT1:HSP90"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/Q96P20"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P08238"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/Q9Y2Z0"
      minerva_ref_type__resource1 "UNIPROT__Q96P20"
      minerva_ref_type__resource2 "UNIPROT__P08238"
      minerva_ref_type__resource3 "UNIPROT__Q9Y2Z0"
      minerva_type "Complex"
      minerva_x 1864.0
      minerva_y 1270.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa6"
      species_meta_id "s_id_csa6"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 61
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 700.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nlrp3_space_oligomer:asc__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa13"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa13"
      minerva_name "NLRP3 oligomer:ASC"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/Q96P20"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q9ULZ3"
      minerva_ref_type__resource1 "UNIPROT__Q96P20"
      minerva_ref_type__resource2 "UNIPROT__Q9ULZ3"
      minerva_type "Complex"
      minerva_x 1974.0
      minerva_y 2010.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa13"
      species_meta_id "s_id_csa13"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 62
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 800.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "casp1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa100"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa100"
      minerva_former_symbols "IL1BC"
      minerva_name "CASP1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/834"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/834"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.22.36"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1499"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000137752"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P29466"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CASP1"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CASP1"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_033292"
      minerva_ref_type__resource1 "ENTREZ__834"
      minerva_ref_type__resource2 "ENTREZ__834"
      minerva_ref_type__resource3 "EC__3.4.22.36"
      minerva_ref_type__resource4 "HGNC__1499"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000137752"
      minerva_ref_type__resource6 "UNIPROT__P29466"
      minerva_ref_type__resource7 "HGNC_SYMBOL__CASP1"
      minerva_ref_type__resource8 "HGNC_SYMBOL__CASP1"
      minerva_ref_type__resource9 "REFSEQ__NM_033292"
      minerva_type "Protein"
      minerva_x 2124.0
      minerva_y 1830.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa100"
      species_meta_id "s_id_sa100"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 64
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1000.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ca2_plus___cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa142"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa142"
      minerva_fullName "calcium(2+)"
      minerva_name "Ca2+"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:29108"
      minerva_ref_type__resource1 "CHEBI__CHEBI:29108"
      minerva_synonyms "CALCIUM ION; Ca(2+); Ca(2+); Ca2+; calcium, doubly charged positive ion"
      minerva_type "Ion"
      minerva_x 1284.0
      minerva_y 1310.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa142"
      species_meta_id "s_id_sa142"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 65
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1100.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "p50__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa83"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa83"
      minerva_name "p50"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4790"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKB1"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P19838"
      minerva_ref_type__resource1 "ENTREZ__4790"
      minerva_ref_type__resource2 "HGNC_SYMBOL__NFKB1"
      minerva_ref_type__resource3 "UNIPROT__P19838"
      minerva_type "Protein"
      minerva_x 2664.0
      minerva_y 790.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa83"
      species_meta_id "s_id_sa83"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 66
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1200.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "txn__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa7"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa7"
      minerva_name "TXN"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7295"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/7295"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P10599"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12435"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000136810"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_001244938"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TXN"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TXN"
      minerva_ref_type__resource1 "ENTREZ__7295"
      minerva_ref_type__resource2 "ENTREZ__7295"
      minerva_ref_type__resource3 "UNIPROT__P10599"
      minerva_ref_type__resource4 "HGNC__12435"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000136810"
      minerva_ref_type__resource6 "REFSEQ__NM_001244938"
      minerva_ref_type__resource7 "HGNC_SYMBOL__TXN"
      minerva_ref_type__resource8 "HGNC_SYMBOL__TXN"
      minerva_type "Protein"
      minerva_x 1334.0
      minerva_y 867.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa7"
      species_meta_id "s_id_sa7"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 67
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 200.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "proil_minus_1b__cell__aa 1-116__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa172"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa172"
      minerva_name "proIL-1B"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL1B"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P01584"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/3553"
      minerva_ref_type__resource1 "HGNC_SYMBOL__IL1B"
      minerva_ref_type__resource2 "UNIPROT__P01584"
      minerva_ref_type__resource3 "ENTREZ__3553"
      minerva_structuralState "AA 1-116"
      minerva_type "Protein"
      minerva_x 2958.5
      minerva_y 1798.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa172"
      species_meta_id "s_id_sa172"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 68
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 300.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "asc__cell__activated_ubiquitinated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa103"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa103"
      minerva_name "ASC"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/29108"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PYCARD"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/Q9ULZ3"
      minerva_ref_type__resource1 "ENTREZ__29108"
      minerva_ref_type__resource2 "HGNC_SYMBOL__PYCARD"
      minerva_ref_type__resource3 "UNIPROT__Q9ULZ3"
      minerva_state1 "UBIQUITINATED"
      minerva_structuralState "activated"
      minerva_type "Protein"
      minerva_x 1764.0
      minerva_y 2010.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa103"
      species_meta_id "s_id_sa103"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 69
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 400.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf9b__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa165"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa165"
      minerva_name "Orf9b"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/1489679"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P59636"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=694009"
      minerva_ref_type__resource1 "ENTREZ__1489679"
      minerva_ref_type__resource2 "UNIPROT__P59636"
      minerva_ref_type__resource3 "TAXONOMY__694009"
      minerva_type "Protein"
      minerva_x 1304.0
      minerva_y 2370.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa165"
      species_meta_id "s_id_sa165"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 70
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 500.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sugt1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa5"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa5"
      minerva_name "SUGT1"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/16987"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/10910"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/10910"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/Q9Y2Z0"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000165416"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_001130912"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SUGT1"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SUGT1"
      minerva_ref_type__resource1 "HGNC__16987"
      minerva_ref_type__resource2 "ENTREZ__10910"
      minerva_ref_type__resource3 "ENTREZ__10910"
      minerva_ref_type__resource4 "UNIPROT__Q9Y2Z0"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000165416"
      minerva_ref_type__resource6 "REFSEQ__NM_001130912"
      minerva_ref_type__resource7 "HGNC_SYMBOL__SUGT1"
      minerva_ref_type__resource8 "HGNC_SYMBOL__SUGT1"
      minerva_type "Protein"
      minerva_x 2114.0
      minerva_y 830.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa5"
      species_meta_id "s_id_sa5"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 71
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 600.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "h2o__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa157"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa157"
      minerva_fullName "water"
      minerva_name "H2O"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15377"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15377"
      minerva_synonyms "BOUND WATER; H2O; H2O; HOH; WATER; Wasser; Water; [OH2]; acqua; agua; aqua; dihydridooxygen; dihydrogen oxide; eau; hydrogen hydroxide"
      minerva_type "Simple molecule"
      minerva_x 964.0
      minerva_y 1800.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa157"
      species_meta_id "s_id_sa157"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 72
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 700.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "casp1__cell__p10__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa169"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa169"
      minerva_former_symbols "IL1BC"
      minerva_name "CASP1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/834"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/834"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.22.36"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1499"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000137752"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P29466"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CASP1"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CASP1"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_033292"
      minerva_ref_type__resource1 "ENTREZ__834"
      minerva_ref_type__resource2 "ENTREZ__834"
      minerva_ref_type__resource3 "EC__3.4.22.36"
      minerva_ref_type__resource4 "HGNC__1499"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000137752"
      minerva_ref_type__resource6 "UNIPROT__P29466"
      minerva_ref_type__resource7 "HGNC_SYMBOL__CASP1"
      minerva_ref_type__resource8 "HGNC_SYMBOL__CASP1"
      minerva_ref_type__resource9 "REFSEQ__NM_033292"
      minerva_structuralState "p10"
      minerva_type "Protein"
      minerva_x 2601.083333333333
      minerva_y 1924.625
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa169"
      species_meta_id "s_id_sa169"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 73
    zlevel -1

    graphics [
      x 800.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re23"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_type "Known transition omitted"
      minerva_x 3086.5
      minerva_y 1738.7784090909058
    ]
    sbml [
      reaction_id "re23"
      reaction_meta_id "re23"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 76
    zlevel -1

    graphics [
      x 900.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re66"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_type "State transition"
      minerva_x 1118.1893740679152
      minerva_y 2266.143178428785
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re66"
      reaction_meta_id "re66"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 80
    zlevel -1

    graphics [
      x 1000.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re48"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_type "Known transition omitted"
      minerva_x 1864.0
      minerva_y 1468.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re48"
      reaction_meta_id "re48"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 91
    zlevel -1

    graphics [
      x 1100.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re51"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_type "State transition"
      minerva_x 1674.0
      minerva_y 2008.3928571428578
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re51"
      reaction_meta_id "re51"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 96
    zlevel -1

    graphics [
      x 1200.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re47"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_type "State transition"
      minerva_x 2664.0
      minerva_y 1156.25
    ]
    sbml [
      reaction_id "re47"
      reaction_meta_id "re47"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 100
    zlevel -1

    graphics [
      x 200.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re61"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_type "Heterodimer association"
      minerva_x 2384.0
      minerva_y 535.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re61"
      reaction_meta_id "re61"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 104
    zlevel -1

    graphics [
      x 300.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re52"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_type "Known transition omitted"
      minerva_x 1482.5
      minerva_y 2007.25
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re52"
      reaction_meta_id "re52"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 108
    zlevel -1

    graphics [
      x 400.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re46"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_type "Heterodimer association"
      minerva_x 2835.9999999999995
      minerva_y 1029.500000000001
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re46"
      reaction_meta_id "re46"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 112
    zlevel -1

    graphics [
      x 500.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re67"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_type "State transition"
      minerva_x 1205.488465927495
      minerva_y 2246.4828321338528
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re67"
      reaction_meta_id "re67"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 116
    zlevel -1

    graphics [
      x 600.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re49"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_type "State transition"
      minerva_x 1109.0
      minerva_y 2007.761363636363
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re49"
      reaction_meta_id "re49"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 120
    zlevel -1

    graphics [
      x 700.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re44"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_type "Known transition omitted"
      minerva_x 2564.0
      minerva_y 790.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re44"
      reaction_meta_id "re44"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 123
    zlevel -1

    graphics [
      x 800.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re4"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_type "Heterodimer association"
      minerva_x 2064.0
      minerva_y 912.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re4"
      reaction_meta_id "re4"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 127
    zlevel -1

    graphics [
      x 900.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re54"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_type "Heterodimer association"
      minerva_x 1864.0
      minerva_y 1130.0
    ]
    sbml [
      reaction_id "re54"
      reaction_meta_id "re54"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 131
    zlevel -1

    graphics [
      x 1000.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re6"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_type "Heterodimer association"
      minerva_x 1634.0
      minerva_y 955.0
    ]
    sbml [
      reaction_id "re6"
      reaction_meta_id "re6"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 135
    zlevel -1

    graphics [
      x 1100.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re43"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_type "Heterodimer association"
      minerva_x 1444.0000000000005
      minerva_y 2232.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re43"
      reaction_meta_id "re43"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 139
    zlevel -1

    graphics [
      x 1200.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re65"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_type "State transition"
      minerva_x 1072.25
      minerva_y 1460.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re65"
      reaction_meta_id "re65"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 143
    zlevel -1

    graphics [
      x 200.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re60"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_type "State transition"
      minerva_x 1229.0
      minerva_y 1216.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re60"
      reaction_meta_id "re60"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 147
    zlevel -1

    graphics [
      x 300.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re21"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_type "State transition"
      minerva_x 2853.7567129629624
      minerva_y 1939.3125
    ]
    sbml [
      reaction_id "re21"
      reaction_meta_id "re21"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 150
    zlevel -1

    graphics [
      x 400.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re62"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_type "Known transition omitted"
      minerva_x 2389.0
      minerva_y 790.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re62"
      reaction_meta_id "re62"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 154
    zlevel -1

    graphics [
      x 500.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re37"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_type "Transcription"
      minerva_x 2504.0
      minerva_y 1380.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re37"
      reaction_meta_id "re37"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 158
    zlevel -1

    graphics [
      x 600.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re38"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_type "Transcription"
      minerva_x 2834.0
      minerva_y 1375.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re38"
      reaction_meta_id "re38"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 162
    zlevel -1

    graphics [
      x 700.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re22"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_type "State transition"
      minerva_x 2824.0
      minerva_y 1738.75
    ]
    sbml [
      reaction_id "re22"
      reaction_meta_id "re22"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 167
    zlevel -1

    graphics [
      x 800.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re45"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_type "Heterodimer association"
      minerva_x 1889.6875
      minerva_y 1963.1170648738414
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re45"
      reaction_meta_id "re45"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 171
    zlevel -1

    graphics [
      x 900.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re56"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_type "Translation"
      minerva_x 2774.652173913043
      minerva_y 1553.75
    ]
    sbml [
      reaction_id "re56"
      reaction_meta_id "re56"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 174
    zlevel -1

    graphics [
      x 1000.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re63"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_type "State transition"
      minerva_x 836.7187500000005
      minerva_y 1770.0
    ]
    sbml [
      reaction_id "re63"
      reaction_meta_id "re63"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 184
    zlevel -1

    graphics [
      x 1100.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re27"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_type "Heterodimer association"
      minerva_x 2162.9686772285922
      minerva_y 2008.071532339828
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re27"
      reaction_meta_id "re27"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 188
    zlevel -1

    graphics [
      x 1200.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re20"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_type "Heterodimer association"
      minerva_x 2711.812500000001
      minerva_y 2007.0909926470588
    ]
    sbml [
      reaction_id "re20"
      reaction_meta_id "re20"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 192
    zlevel -1

    graphics [
      x 200.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re26"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_type "Known transition omitted"
      minerva_x 3084.0
      minerva_y 1670.0
    ]
    sbml [
      reaction_id "re26"
      reaction_meta_id "re26"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 195
    zlevel -1

    graphics [
      x 300.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re59"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_type "State transition"
      minerva_x 1137.4375
      minerva_y 1283.59375
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re59"
      reaction_meta_id "re59"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 199
    zlevel -1

    graphics [
      x 400.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re3"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_type "Heterodimer association"
      minerva_x 1454.0000000000005
      minerva_y 808.125
    ]
    sbml [
      reaction_id "re3"
      reaction_meta_id "re3"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 203
    zlevel -1

    graphics [
      x 500.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re2"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_type "State transition"
      minerva_x 1334.2451768488745
      minerva_y 943.75
    ]
    sbml [
      reaction_id "re2"
      reaction_meta_id "re2"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 207
    zlevel -1

    graphics [
      x 600.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re19"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_type "State transition"
      minerva_x 2491.947916666667
      minerva_y 2008.3813573852144
    ]
    sbml [
      reaction_id "re19"
      reaction_meta_id "re19"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 215
    zlevel -1

    graphics [
      x 700.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_type "Dissociation"
      minerva_x 1421.9794520547953
      minerva_y 844.375
    ]
    sbml [
      reaction_id "re1"
      reaction_meta_id "re1"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 219
    zlevel -1

    graphics [
      x 800.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re55"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_type "Translation"
      minerva_x 2588.2916666666683
      minerva_y 1588.75
    ]
    sbml [
      reaction_id "re55"
      reaction_meta_id "re55"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 222
    zlevel -1

    graphics [
      x 900.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re25"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_type "State transition"
      minerva_x 2849.0
      minerva_y 1670.0
    ]
    sbml [
      reaction_id "re25"
      reaction_meta_id "re25"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 227
    zlevel -1

    graphics [
      x 1000.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re50"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_type "State transition"
      minerva_x 1307.5
      minerva_y 2007.2572674418598
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re50"
      reaction_meta_id "re50"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 231
    zlevel -1

    graphics [
      x 1100.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re42"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14116"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL18"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_ref_type__resource2 "UNIPROT__Q14116"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL18"
      minerva_type "State transition"
      minerva_x 2664.0
      minerva_y 885.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re42"
      reaction_meta_id "re42"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 235
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1000.0
      y 350.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "asc:mavs:traf3__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa29"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa29"
      minerva_name "ASC:MAVS:TRAF3"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/25847972"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q13114"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/Q9ULZ3"
      minerva_ref_type__resource1 "PUBMED__25847972"
      minerva_ref_type__resource2 "UNIPROT__Q13114"
      minerva_ref_type__resource3 "UNIPROT__Q9ULZ3"
      minerva_type "Complex"
      minerva_x 1396.0
      minerva_y 2007.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation "Annotation [about=\"#s_id_csa29\", history=]"
      species_id "s_id_csa29"
      species_meta_id "s_id_csa29"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
    ]
  ]
  node [
    id 240
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 750.0
      y 450.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "proil_minus_1b__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa94"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa94"
      minerva_name "proIL-1B"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3553"
      minerva_ref_type__resource1 "ENTREZ__3553"
      minerva_type "RNA"
      minerva_x 2504.0
      minerva_y 1440.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation "Annotation [about=\"#s_id_sa94\", history=]"
      species_id "s_id_sa94"
      species_meta_id "s_id_sa94"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
    ]
  ]
  node [
    id 244
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "proil_minus_18__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa95"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa95"
      minerva_name "proIL-18"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3606"
      minerva_ref_type__resource1 "ENTREZ__3606"
      minerva_type "Gene"
      minerva_x 2834.0
      minerva_y 1310.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation "Annotation [about=\"#s_id_sa95\", history=]"
      species_id "s_id_sa95"
      species_meta_id "s_id_sa95"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
    ]
  ]
  edge [
    id 74
    source 22
    target 73
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa17"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 75
    source 73
    target 20
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa21"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 77
    source 9
    target 76
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa102"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 78
    source 76
    target 59
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa164"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 79
    source 69
    target 76
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR0"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa165"
    ]
  ]
  edge [
    id 81
    source 60
    target 80
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa6"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 82
    source 80
    target 17
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa101"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 83
    source 80
    target 45
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa4"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 84
    source 28
    target 80
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR1"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa91"
    ]
  ]
  edge [
    id 85
    source 2
    target 80
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR2"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa90"
    ]
  ]
  edge [
    id 86
    source 19
    target 80
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR3"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa135"
    ]
  ]
  edge [
    id 87
    source 37
    target 80
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR4"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa136"
    ]
  ]
  edge [
    id 88
    source 3
    target 80
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR5"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa1"
    ]
  ]
  edge [
    id 89
    source 46
    target 80
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR6"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa3"
    ]
  ]
  edge [
    id 90
    source 64
    target 80
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR7"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa142"
    ]
  ]
  edge [
    id 93
    source 91
    target 68
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa103"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 94
    source 91
    target 41
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa99"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 95
    source 91
    target 9
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa102"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 97
    source 5
    target 96
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa22"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 98
    source 96
    target 40
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa23"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 99
    source 25
    target 96
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR8"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa21"
    ]
  ]
  edge [
    id 101
    source 16
    target 100
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa147"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 102
    source 41
    target 100
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa99"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 103
    source 100
    target 36
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa24"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 107
    source 36
    target 104
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR9"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa24"
    ]
  ]
  edge [
    id 109
    source 52
    target 108
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa82"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 110
    source 5
    target 108
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa22"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 111
    source 108
    target 25
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa21"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 113
    source 41
    target 112
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa99"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 114
    source 112
    target 59
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa164"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 115
    source 69
    target 112
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR10"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa165"
    ]
  ]
  edge [
    id 117
    source 4
    target 116
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa104"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 118
    source 9
    target 116
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa102"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 119
    source 116
    target 49
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa28"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 121
    source 29
    target 120
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa84"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 122
    source 120
    target 65
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa83"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 124
    source 47
    target 123
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa4"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 125
    source 70
    target 123
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa5"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 126
    source 123
    target 45
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa4"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 128
    source 30
    target 127
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa92"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 129
    source 45
    target 127
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa4"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 130
    source 127
    target 60
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa6"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 132
    source 30
    target 131
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa92"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 133
    source 42
    target 131
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa8"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 134
    source 131
    target 46
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa3"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 136
    source 16
    target 135
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa147"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 137
    source 41
    target 135
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa99"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 138
    source 135
    target 36
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa24"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 140
    source 11
    target 139
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa162"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 141
    source 139
    target 3
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa1"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 142
    source 14
    target 139
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR11"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa158"
    ]
  ]
  edge [
    id 144
    source 53
    target 143
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa144"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 145
    source 143
    target 64
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa142"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 146
    source 13
    target 143
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR12"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa146"
    ]
  ]
  edge [
    id 148
    source 44
    target 147
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa7"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 149
    source 147
    target 31
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa5"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 151
    source 21
    target 150
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa151"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 152
    source 150
    target 29
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa84"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 153
    source 36
    target 150
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR13"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa24"
    ]
  ]
  edge [
    id 157
    source 40
    target 154
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR14"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa23"
    ]
  ]
  edge [
    id 161
    source 40
    target 158
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR15"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa23"
    ]
  ]
  edge [
    id 163
    source 12
    target 162
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa15"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 164
    source 162
    target 22
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa17"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 165
    source 162
    target 67
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa172"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 166
    source 31
    target 162
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR16"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa5"
    ]
  ]
  edge [
    id 168
    source 68
    target 167
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa103"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 169
    source 17
    target 167
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa101"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 170
    source 167
    target 61
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa13"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 173
    source 171
    target 34
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa14"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 175
    source 23
    target 174
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa152"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 176
    source 54
    target 174
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa154"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 177
    source 1
    target 174
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa153"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 178
    source 174
    target 15
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa156"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 179
    source 174
    target 71
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa157"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 180
    source 174
    target 10
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa155"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 181
    source 174
    target 14
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa158"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 182
    source 174
    target 26
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa163"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 183
    source 39
    target 174
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR17"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa161"
    ]
  ]
  edge [
    id 185
    source 62
    target 184
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa100"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 186
    source 61
    target 184
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa13"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 187
    source 184
    target 50
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa10"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 189
    source 58
    target 188
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa168"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 190
    source 72
    target 188
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa169"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 191
    source 188
    target 44
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa7"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 193
    source 43
    target 192
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa16"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 194
    source 192
    target 38
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa20"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 196
    source 6
    target 195
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa141"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 197
    source 195
    target 64
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa142"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 198
    source 8
    target 195
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR18"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa140"
    ]
  ]
  edge [
    id 200
    source 42
    target 199
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa8"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 201
    source 66
    target 199
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa7"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 202
    source 199
    target 7
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa1"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 204
    source 66
    target 203
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa7"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 205
    source 203
    target 56
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa175"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 206
    source 3
    target 203
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR19"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa1"
    ]
  ]
  edge [
    id 208
    source 50
    target 207
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa10"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 209
    source 207
    target 58
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa168"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 210
    source 207
    target 32
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa170"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 211
    source 207
    target 51
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa171"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 212
    source 207
    target 72
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa169"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 213
    source 207
    target 61
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa13"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 214
    source 48
    target 207
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR20"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa26"
    ]
  ]
  edge [
    id 216
    source 7
    target 215
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa1"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 217
    source 215
    target 42
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa8"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 218
    source 215
    target 56
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa175"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 221
    source 219
    target 12
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa15"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 223
    source 34
    target 222
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa14"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 224
    source 222
    target 43
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa16"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 225
    source 222
    target 27
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa18"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 226
    source 31
    target 222
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR21"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa5"
    ]
  ]
  edge [
    id 228
    source 49
    target 227
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa28"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 229
    source 41
    target 227
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa99"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 232
    source 65
    target 231
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa83"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 233
    source 24
    target 231
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa86"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 234
    source 231
    target 5
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa22"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 236
    source 227
    target 235
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa29"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 237
    source 235
    target 104
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa29"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 238
    source 104
    target 235
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa27"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 239
    source 235
    target 91
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa27"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 241
    source 154
    target 240
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa94"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 242
    source 240
    target 219
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa94"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 243
    source 240
    target 154
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa96"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 245
    source 244
    target 158
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa95"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 246
    source 158
    target 244
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa93"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 247
    source 244
    target 171
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa93"
      stoichiometry "1.0"
    ]
  ]
]
