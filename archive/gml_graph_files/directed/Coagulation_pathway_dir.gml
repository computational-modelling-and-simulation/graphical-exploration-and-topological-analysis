# generated with VANTED V2.8.0 at Tue Apr 27 20:00:45 CEST 2021
graph [
  background_coloring "true"
  cluster_colors "255,128,128,255:0,0,0,255;230,255,128,255:0,0,0,255;128,255,179,255:0,0,0,255;128,178,255,255:0,0,0,255;230,128,255,255:0,0,0,255"
  clusterbackground_fill_outer_region "false"
  clusterbackground_grid 50.0
  clusterbackground_low_alpha 0.2
  clusterbackground_radius 200.0
  clusterbackground_space_fill "true"
  graphbackgroundcolor "#ffffff"
  sbml [
    model_annotation ""
    model_meta_id "Coagulation_goar"
    model_non_rdf_annotation ""
    model_notes ""
    model_sbml_id "Coagulation_goar"
    namespace "xmlns:celldesigner=\"http://www.sbml.org/2001/ns/celldesigner\" "
    sbml_level "2"
    sbml_version "4"
  ]
  sbml_compartment_default [
    sbml_compartment_default_id "default"
    sbml_compartment_default_meta_id "default"
    sbml_compartment_default_non_rdf_annotation ""
    sbml_compartment_default_notes ""
    sbml_compartment_default_size "1.0"
    sbml_compartment_default_units "volume"
  ]
  sbml_compartment_s_id_ca13 [
    sbml_compartment_s_id_ca13_annotation ""
    sbml_compartment_s_id_ca13_id "s_id_ca13"
    sbml_compartment_s_id_ca13_meta_id "s_id_ca13"
    sbml_compartment_s_id_ca13_name "endothelium,_space_vascular"
    sbml_compartment_s_id_ca13_non_rdf_annotation ""
    sbml_compartment_s_id_ca13_notes ""
    sbml_compartment_s_id_ca13_outside "s_id_ca16"
    sbml_compartment_s_id_ca13_size "1.0"
    sbml_compartment_s_id_ca13_units "volume"
  ]
  sbml_compartment_s_id_ca14 [
    sbml_compartment_s_id_ca14_annotation ""
    sbml_compartment_s_id_ca14_id "s_id_ca14"
    sbml_compartment_s_id_ca14_meta_id "s_id_ca14"
    sbml_compartment_s_id_ca14_name "platelet"
    sbml_compartment_s_id_ca14_non_rdf_annotation ""
    sbml_compartment_s_id_ca14_notes ""
    sbml_compartment_s_id_ca14_outside "s_id_ca15"
    sbml_compartment_s_id_ca14_size "1.0"
    sbml_compartment_s_id_ca14_units "volume"
  ]
  sbml_compartment_s_id_ca15 [
    sbml_compartment_s_id_ca15_annotation ""
    sbml_compartment_s_id_ca15_id "s_id_ca15"
    sbml_compartment_s_id_ca15_meta_id "s_id_ca15"
    sbml_compartment_s_id_ca15_name "blood"
    sbml_compartment_s_id_ca15_non_rdf_annotation ""
    sbml_compartment_s_id_ca15_notes ""
    sbml_compartment_s_id_ca15_outside "s_id_ca16"
    sbml_compartment_s_id_ca15_size "1.0"
    sbml_compartment_s_id_ca15_units "volume"
  ]
  sbml_compartment_s_id_ca16 [
    sbml_compartment_s_id_ca16_id "s_id_ca16"
    sbml_compartment_s_id_ca16_meta_id "s_id_ca16"
    sbml_compartment_s_id_ca16_name "human_space_host"
    sbml_compartment_s_id_ca16_non_rdf_annotation ""
    sbml_compartment_s_id_ca16_notes ""
    sbml_compartment_s_id_ca16_outside "default"
    sbml_compartment_s_id_ca16_size "1.0"
    sbml_compartment_s_id_ca16_units "volume"
  ]
  sbml_unit_definition_1 [
    sbml_unit_definition_1_id "area"
    sbml_unit_definition_1_name "area"
    sbml_unit_definition_1_sub_unit_1_ "(1.0 * 10^0 * metre)^2.0"
    sbml_unit_definition_1unit "(1.0 * 10^0 * metre)^2.0"
  ]
  sbml_unit_definition_2 [
    sbml_unit_definition_2_id "length"
    sbml_unit_definition_2_name "length"
    sbml_unit_definition_2_sub_unit_1_ "(1.0 * 10^0 * metre)^1.0"
    sbml_unit_definition_2unit "(1.0 * 10^0 * metre)^1.0"
  ]
  sbml_unit_definition_3 [
    sbml_unit_definition_3_id "volume"
    sbml_unit_definition_3_name "volume"
    sbml_unit_definition_3_sub_unit_1_ "(1.0 * 10^0 * litre)^1.0"
    sbml_unit_definition_3unit "(1.0 * 10^0 * litre)^1.0"
  ]
  sbml_unit_definition_4 [
    sbml_unit_definition_4_id "substance"
    sbml_unit_definition_4_name "substance"
    sbml_unit_definition_4_sub_unit_1_ "(1.0 * 10^0 * mole)^1.0"
    sbml_unit_definition_4unit "(1.0 * 10^0 * mole)^1.0"
  ]
  sbml_unit_definition_5 [
    sbml_unit_definition_5_id "time"
    sbml_unit_definition_5_name "time"
    sbml_unit_definition_5_sub_unit_1_ "(1.0 * 10^0 * second)^1.0"
    sbml_unit_definition_5unit "(1.0 * 10^0 * second)^1.0"
  ]
  directed 1
  node [
    id 1
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 200.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "il6__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa242"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa242"
      minerva_former_symbols "IFNB2"
      minerva_name "IL6"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3569"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_000600"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/3569"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P05231"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P05231"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000136244"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6018"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL6"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL6"
      minerva_ref_type__resource1 "ENTREZ__3569"
      minerva_ref_type__resource10 "REFSEQ__NM_000600"
      minerva_ref_type__resource2 "ENTREZ__3569"
      minerva_ref_type__resource3 "TAXONOMY__9606"
      minerva_ref_type__resource4 "UNIPROT__P05231"
      minerva_ref_type__resource5 "UNIPROT__P05231"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000136244"
      minerva_ref_type__resource7 "HGNC__6018"
      minerva_ref_type__resource8 "HGNC_SYMBOL__IL6"
      minerva_ref_type__resource9 "HGNC_SYMBOL__IL6"
      minerva_type "Protein"
      minerva_x 1570.0
      minerva_y 1743.5625000000002
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa242"
      species_meta_id "s_id_sa242"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 2
    zlevel -1

    cluster [
      cluster "s_id_ca13"
    ]
    graphics [
      x 300.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "vascular_space_inflammation__endothelium,_space_vascular__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa530"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa530"
      minerva_name "vascular inflammation"
      minerva_type "Phenotype"
      minerva_x 293.0
      minerva_y 650.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca13"
      compartment_name "endothelium,_space_vascular"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa530"
      species_meta_id "s_id_sa530"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 3
    zlevel -1

    cluster [
      cluster "s_id_ca14"
    ]
    graphics [
      x 400.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mas1__platelet__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa496"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa496"
      minerva_name "MAS1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_002377"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000130368"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6899"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4142"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/4142"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAS1"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/P04201"
      minerva_ref_link8 "https://purl.uniprot.org/uniprot/P04201"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAS1"
      minerva_ref_type__resource1 "REFSEQ__NM_002377"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000130368"
      minerva_ref_type__resource3 "HGNC__6899"
      minerva_ref_type__resource4 "ENTREZ__4142"
      minerva_ref_type__resource5 "ENTREZ__4142"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MAS1"
      minerva_ref_type__resource7 "UNIPROT__P04201"
      minerva_ref_type__resource8 "UNIPROT__P04201"
      minerva_ref_type__resource9 "HGNC_SYMBOL__MAS1"
      minerva_type "Protein"
      minerva_x 2725.0
      minerva_y 2445.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca14"
      compartment_name "platelet"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa496"
      species_meta_id "s_id_sa496"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 4
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 500.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s534__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa455"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa453"
      minerva_elementId2 "sa457"
      minerva_elementId3 "sa454"
      minerva_elementId4 "sa456"
      minerva_elementId5 "sa451"
      minerva_elementId6 "sa452"
      minerva_elementId7 "sa455"
      minerva_name "s534"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/32278764"
      minerva_ref_link2 "http://id.nlm.nih.gov/mesh/D062106"
      minerva_ref_type__resource1 "PUBMED__32278764"
      minerva_ref_type__resource2 "MESH_2012__D062106"
      minerva_type "Degraded"
      minerva_x 1759.0
      minerva_x2 773.8333333333335
      minerva_x3 1749.0
      minerva_x4 1436.0
      minerva_x5 1779.0
      minerva_x6 1768.0
      minerva_x7 1750.0
      minerva_y 1685.8333333333335
      minerva_y2 1463.8333333333335
      minerva_y3 1742.8541666666665
      minerva_y4 1569.0
      minerva_y5 1570.0
      minerva_y6 1627.8333333333335
      minerva_y7 1836.4166666666665
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa455"
      species_meta_id "s_id_sa455"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 5
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 600.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "f9a__blood__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa175"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa175"
      minerva_name "F9a"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_000133"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/2158"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/gene/2158"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/3551"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000101981"
      minerva_ref_link5 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.21.22"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P00740"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P00740"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=F9"
      minerva_ref_link9 "http://id.nlm.nih.gov/mesh/D015949"
      minerva_ref_type__resource1 "REFSEQ__NM_000133"
      minerva_ref_type__resource10 "ENTREZ__2158"
      minerva_ref_type__resource11 "ENTREZ__2158"
      minerva_ref_type__resource2 "TAXONOMY__9606"
      minerva_ref_type__resource3 "HGNC__3551"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000101981"
      minerva_ref_type__resource5 "EC__3.4.21.22"
      minerva_ref_type__resource6 "UNIPROT__P00740"
      minerva_ref_type__resource7 "UNIPROT__P00740"
      minerva_ref_type__resource8 "HGNC_SYMBOL__F9"
      minerva_ref_type__resource9 "MESH_2012__D015949"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 334.0
      minerva_y 1448.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa175"
      species_meta_id "s_id_sa175"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 6
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 700.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "f5a__blood__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa201"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa201"
      minerva_name "F5a"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=F5"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000198734"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P12259"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P12259"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/3542"
      minerva_ref_link6 "http://id.nlm.nih.gov/mesh/D015943"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_000130"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/2153"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/2153"
      minerva_ref_type__resource1 "HGNC_SYMBOL__F5"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000198734"
      minerva_ref_type__resource2 "UNIPROT__P12259"
      minerva_ref_type__resource3 "UNIPROT__P12259"
      minerva_ref_type__resource4 "TAXONOMY__9606"
      minerva_ref_type__resource5 "HGNC__3542"
      minerva_ref_type__resource6 "MESH_2012__D015943"
      minerva_ref_type__resource7 "REFSEQ__NM_000130"
      minerva_ref_type__resource8 "ENTREZ__2153"
      minerva_ref_type__resource9 "ENTREZ__2153"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 668.0
      minerva_y 1747.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa201"
      species_meta_id "s_id_sa201"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 7
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 800.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s539__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa462"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa462"
      minerva_name "s539"
      minerva_ref_link1 "http://id.nlm.nih.gov/mesh/D062106"
      minerva_ref_type__resource1 "MESH_2012__D062106"
      minerva_type "Degraded"
      minerva_x 2853.0
      minerva_y 1007.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa462"
      species_meta_id "s_id_sa462"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 8
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 900.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "f13a__blood__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa425"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa425"
      minerva_name "F13a"
      minerva_ref_link1 "http://id.nlm.nih.gov/mesh/D026122"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link3 "https://www.brenda-enzymes.org/php/result_flat.php4?ecno=2.3.2.13"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=F13"
      minerva_ref_type__resource1 "MESH_2012__D026122"
      minerva_ref_type__resource2 "TAXONOMY__9606"
      minerva_ref_type__resource3 "BRENDA__2.3.2.13"
      minerva_ref_type__resource4 "HGNC_SYMBOL__F13"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 1628.0
      minerva_y 2052.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa425"
      species_meta_id "s_id_sa425"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 9
    zlevel -1

    cluster [
      cluster "s_id_ca16"
    ]
    graphics [
      x 1000.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "septal_space_capillary_space_necrosis__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa389"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa389"
      minerva_name "septal capillary necrosis"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link2 "http://id.nlm.nih.gov/mesh/D007681"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource2 "MESH_2012__D007681"
      minerva_type "Phenotype"
      minerva_x 1563.0
      minerva_y 459.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca16"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa389"
      species_meta_id "s_id_sa389"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 10
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 1100.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "k_plus___blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa521"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa521"
      minerva_fullName "potassium(1+)"
      minerva_name "K+"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:29103"
      minerva_ref_type__resource1 "CHEBI__CHEBI:29103"
      minerva_synonyms "K(+); K(+); K+; POTASSIUM ION"
      minerva_type "Ion"
      minerva_x 842.0
      minerva_y 1229.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa521"
      species_meta_id "s_id_sa521"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 11
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 1200.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "c3b:bb:c3b__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa27"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa27"
      minerva_name "C3b:Bb:C3b"
      minerva_ref_link1 "https://www.brenda-enzymes.org/php/result_flat.php4?ecno=34.4.21.47"
      minerva_ref_link2 "http://id.nlm.nih.gov/mesh/D003179"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link4 "http://id.nlm.nih.gov/mesh/D051566"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/pubmed/12440962"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1037"
      minerva_ref_type__resource1 "BRENDA__34.4.21.47"
      minerva_ref_type__resource2 "MESH_2012__D003179"
      minerva_ref_type__resource3 "TAXONOMY__9606"
      minerva_ref_type__resource4 "MESH_2012__D051566"
      minerva_ref_type__resource5 "PUBMED__12440962"
      minerva_ref_type__resource6 "HGNC__1037"
      minerva_type "Complex"
      minerva_x 2169.0
      minerva_y 1102.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa27"
      species_meta_id "s_id_csa27"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 12
    zlevel -1

    cluster [
      cluster "s_id_ca16"
    ]
    graphics [
      x 200.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "agt__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa196"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa196"
      minerva_former_symbols "SERPINA8"
      minerva_name "AGT"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/P01019"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_000029"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P01019"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGT"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGT"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000135744"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/333"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/183"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/183"
      minerva_ref_type__resource1 "UNIPROT__P01019"
      minerva_ref_type__resource10 "REFSEQ__NM_000029"
      minerva_ref_type__resource2 "UNIPROT__P01019"
      minerva_ref_type__resource3 "TAXONOMY__9606"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGT"
      minerva_ref_type__resource5 "HGNC_SYMBOL__AGT"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000135744"
      minerva_ref_type__resource7 "HGNC__333"
      minerva_ref_type__resource8 "ENTREZ__183"
      minerva_ref_type__resource9 "ENTREZ__183"
      minerva_type "Protein"
      minerva_x 1027.0
      minerva_y 524.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca16"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa196"
      species_meta_id "s_id_sa196"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 13
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 300.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s86__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa504"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa532"
      minerva_elementId2 "sa501"
      minerva_elementId3 "sa504"
      minerva_elementId4 "sa399"
      minerva_name "s86"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/20689271"
      minerva_ref_link2 "http://id.nlm.nih.gov/mesh/D062106"
      minerva_ref_type__resource1 "PUBMED__20689271"
      minerva_ref_type__resource2 "MESH_2012__D062106"
      minerva_type "Degraded"
      minerva_x 845.5
      minerva_x2 931.5
      minerva_x3 899.0
      minerva_x4 1303.0
      minerva_y 1303.5
      minerva_y2 1241.5
      minerva_y3 1171.6
      minerva_y4 855.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa504"
      species_meta_id "s_id_sa504"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 14
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 400.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sars_minus_cov_minus_2_space_infection__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa480"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa481"
      minerva_elementId2 "sa499"
      minerva_elementId3 "sa480"
      minerva_elementId4 "sa207"
      minerva_name "SARS-CoV-2 infection"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=2697049"
      minerva_ref_link2 "http://id.nlm.nih.gov/mesh/D012327"
      minerva_ref_type__resource1 "TAXONOMY__2697049"
      minerva_ref_type__resource2 "MESH_2012__D012327"
      minerva_type "Phenotype"
      minerva_x 546.0
      minerva_x2 1738.0
      minerva_x3 1348.0
      minerva_x4 1370.0
      minerva_y 2282.0
      minerva_y2 1963.0
      minerva_y3 957.0
      minerva_y4 1516.625
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa480"
      species_meta_id "s_id_sa480"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 15
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 500.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "serpinf2__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa412"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa412"
      minerva_former_symbols "PLI"
      minerva_name "SERPINF2"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/P08697"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SERPINF2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P08697"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000167711"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/5345"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/5345"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/9075"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_000934"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SERPINF2"
      minerva_ref_type__resource1 "UNIPROT__P08697"
      minerva_ref_type__resource10 "HGNC_SYMBOL__SERPINF2"
      minerva_ref_type__resource2 "UNIPROT__P08697"
      minerva_ref_type__resource3 "TAXONOMY__9606"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000167711"
      minerva_ref_type__resource5 "ENTREZ__5345"
      minerva_ref_type__resource6 "ENTREZ__5345"
      minerva_ref_type__resource7 "HGNC__9075"
      minerva_ref_type__resource8 "REFSEQ__NM_000934"
      minerva_ref_type__resource9 "HGNC_SYMBOL__SERPINF2"
      minerva_type "Protein"
      minerva_x 513.0
      minerva_y 2492.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa412"
      species_meta_id "s_id_sa412"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 16
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 600.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "proc__blood__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa205"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa205"
      minerva_name "PROC"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.21.69"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PROC"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/gene/5624"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000115718"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/9451"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_000312"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P04070"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P04070"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PROC"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/5624"
      minerva_ref_type__resource1 "EC__3.4.21.69"
      minerva_ref_type__resource10 "HGNC_SYMBOL__PROC"
      minerva_ref_type__resource11 "ENTREZ__5624"
      minerva_ref_type__resource2 "TAXONOMY__9606"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000115718"
      minerva_ref_type__resource4 "HGNC__9451"
      minerva_ref_type__resource5 "REFSEQ__NM_000312"
      minerva_ref_type__resource6 "UNIPROT__P04070"
      minerva_ref_type__resource7 "UNIPROT__P04070"
      minerva_ref_type__resource8 "HGNC_SYMBOL__PROC"
      minerva_ref_type__resource9 "ENTREZ__5624"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 523.0
      minerva_y 1585.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa205"
      species_meta_id "s_id_sa205"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 17
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 700.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s585__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa494"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa494"
      minerva_name "s585"
      minerva_type "Degraded"
      minerva_x 753.0
      minerva_y 2500.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa494"
      species_meta_id "s_id_sa494"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 18
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 800.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s553__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa479"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa479"
      minerva_name "s553"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/32302438"
      minerva_ref_link2 "http://id.nlm.nih.gov/mesh/D062106"
      minerva_ref_type__resource1 "PUBMED__32302438"
      minerva_ref_type__resource2 "MESH_2012__D062106"
      minerva_type "Degraded"
      minerva_x 664.0
      minerva_y 1857.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa479"
      species_meta_id "s_id_sa479"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 19
    zlevel -1

    cluster [
      cluster "s_id_ca13"
    ]
    graphics [
      x 900.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ace2__endothelium,_space_vascular__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa394"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa394"
      minerva_name "ACE2"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000130234"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/59272"
      minerva_ref_link11 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ACE2"
      minerva_ref_link12 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ACE2"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.17.23"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.17.-"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/13557"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/Q9BYF1"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/Q9BYF1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/59272"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_001371415"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000130234"
      minerva_ref_type__resource10 "ENTREZ__59272"
      minerva_ref_type__resource11 "HGNC_SYMBOL__ACE2"
      minerva_ref_type__resource12 "HGNC_SYMBOL__ACE2"
      minerva_ref_type__resource2 "TAXONOMY__9606"
      minerva_ref_type__resource3 "EC__3.4.17.23"
      minerva_ref_type__resource4 "EC__3.4.17.-"
      minerva_ref_type__resource5 "HGNC__13557"
      minerva_ref_type__resource6 "UNIPROT__Q9BYF1"
      minerva_ref_type__resource7 "UNIPROT__Q9BYF1"
      minerva_ref_type__resource8 "ENTREZ__59272"
      minerva_ref_type__resource9 "REFSEQ__NM_001371415"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 1167.0
      minerva_y 777.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca13"
      compartment_name "endothelium,_space_vascular"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa394"
      species_meta_id "s_id_sa394"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 20
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 1000.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "masp1__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa458"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa458"
      minerva_former_symbols "CRARF; PRSS5"
      minerva_name "MASP1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "http://purl.uniprot.org/uniprot/P48740"
      minerva_ref_link11 "http://purl.uniprot.org/uniprot/P48740"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000127241"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/5648"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/5648"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MASP1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_001879"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MASP1"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6901"
      minerva_ref_link9 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.21.-"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "UNIPROT__P48740"
      minerva_ref_type__resource11 "UNIPROT__P48740"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000127241"
      minerva_ref_type__resource3 "ENTREZ__5648"
      minerva_ref_type__resource4 "ENTREZ__5648"
      minerva_ref_type__resource5 "HGNC_SYMBOL__MASP1"
      minerva_ref_type__resource6 "REFSEQ__NM_001879"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MASP1"
      minerva_ref_type__resource8 "HGNC__6901"
      minerva_ref_type__resource9 "EC__3.4.21.-"
      minerva_type "Protein"
      minerva_x 1890.625
      minerva_y 986.125
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa458"
      species_meta_id "s_id_sa458"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 21
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 1100.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "c5__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa314"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa314"
      minerva_name "C5"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/727"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1331"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C5"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C5"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_001735"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000106804"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P01031"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P01031"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/727"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "ENTREZ__727"
      minerva_ref_type__resource2 "HGNC__1331"
      minerva_ref_type__resource3 "HGNC_SYMBOL__C5"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C5"
      minerva_ref_type__resource5 "REFSEQ__NM_001735"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000106804"
      minerva_ref_type__resource7 "UNIPROT__P01031"
      minerva_ref_type__resource8 "UNIPROT__P01031"
      minerva_ref_type__resource9 "ENTREZ__727"
      minerva_type "Protein"
      minerva_x 1806.0
      minerva_y 1394.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa314"
      species_meta_id "s_id_sa314"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 22
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 1200.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "heparin__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa355"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa355"
      minerva_fullName "heparin"
      minerva_name "Heparin"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:28304"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/pubmed/708377"
      minerva_ref_type__resource1 "CHEBI__CHEBI:28304"
      minerva_ref_type__resource2 "TAXONOMY__9606"
      minerva_ref_type__resource3 "PUBMED__708377"
      minerva_synonyms "Bemiparin; Bemiparin; Certoparin; Cy 222; Enoxaparin; Fluxum; Heparin; Heparinic acid; Parnaparin; Reviparin; Sandoparin; heparin; heparina; heparine; heparinum"
      minerva_type "Simple molecule"
      minerva_x 720.0
      minerva_y 1992.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa355"
      species_meta_id "s_id_sa355"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 23
    zlevel -1

    cluster [
      cluster "s_id_ca16"
    ]
    graphics [
      x 200.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa391"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa391"
      minerva_name "S"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P0DTC2"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/1489668"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=2697049"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P59594"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/43740568"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=S"
      minerva_ref_type__resource1 "UNIPROT__P0DTC2"
      minerva_ref_type__resource2 "ENTREZ__1489668"
      minerva_ref_type__resource3 "TAXONOMY__2697049"
      minerva_ref_type__resource4 "UNIPROT__P59594"
      minerva_ref_type__resource5 "ENTREZ__43740568"
      minerva_ref_type__resource6 "HGNC_SYMBOL__S"
      minerva_type "Protein"
      minerva_x 1255.0
      minerva_y 484.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca16"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa391"
      species_meta_id "s_id_sa391"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 24
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 300.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "plasmin__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa212"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa212"
      minerva_elementId2 "sa468"
      minerva_name "Plasmin"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PLG"
      minerva_ref_link10 "https://www.brenda-enzymes.org/php/result_flat.php4?ecno=3.4.21.7"
      minerva_ref_link11 "http://purl.uniprot.org/uniprot/P00747"
      minerva_ref_link12 "http://purl.uniprot.org/uniprot/P00747"
      minerva_ref_link2 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.21.7"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000122194"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/5340"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/5340"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/9071"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_000301"
      minerva_ref_link9 "http://id.nlm.nih.gov/mesh/D005341"
      minerva_ref_type__resource1 "HGNC_SYMBOL__PLG"
      minerva_ref_type__resource10 "BRENDA__3.4.21.7"
      minerva_ref_type__resource11 "UNIPROT__P00747"
      minerva_ref_type__resource12 "UNIPROT__P00747"
      minerva_ref_type__resource2 "EC__3.4.21.7"
      minerva_ref_type__resource3 "TAXONOMY__9606"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000122194"
      minerva_ref_type__resource5 "ENTREZ__5340"
      minerva_ref_type__resource6 "ENTREZ__5340"
      minerva_ref_type__resource7 "HGNC__9071"
      minerva_ref_type__resource8 "REFSEQ__NM_000301"
      minerva_ref_type__resource9 "MESH_2012__D005341"
      minerva_type "Protein"
      minerva_x 648.0
      minerva_x2 2168.857142857143
      minerva_y 2379.0
      minerva_y2 1399.1428571428573
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa212"
      species_meta_id "s_id_sa212"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 25
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 400.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "small_space_peptide__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa183"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa178"
      minerva_elementId2 "sa397"
      minerva_elementId3 "sa183"
      minerva_elementId4 "sa171"
      minerva_name "Small peptide"
      minerva_ref_link1 "http://id.nlm.nih.gov/mesh/D010446"
      minerva_ref_type__resource1 "MESH_2012__D010446"
      minerva_type "Protein"
      minerva_x 438.0
      minerva_x2 677.5
      minerva_x3 630.0
      minerva_x4 433.0
      minerva_y 1792.0
      minerva_y2 960.5
      minerva_y3 1331.0
      minerva_y4 1197.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa183"
      species_meta_id "s_id_sa183"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 26
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 500.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "kallikrein__blood__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa251"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa251"
      minerva_former_symbols "KLK3"
      minerva_name "Kallikrein"
      minerva_ref_link1 "https://www.brenda-enzymes.org/php/result_flat.php4?ecno=3.4.21.34"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/3818"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/gene/3818"
      minerva_ref_link12 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=KLKB1"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000892"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000164344"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link5 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.21.34"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P03952"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P03952"
      minerva_ref_link8 "http://id.nlm.nih.gov/mesh/D020842"
      minerva_ref_link9 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6371"
      minerva_ref_type__resource1 "BRENDA__3.4.21.34"
      minerva_ref_type__resource10 "ENTREZ__3818"
      minerva_ref_type__resource11 "ENTREZ__3818"
      minerva_ref_type__resource12 "HGNC_SYMBOL__KLKB1"
      minerva_ref_type__resource2 "REFSEQ__NM_000892"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000164344"
      minerva_ref_type__resource4 "TAXONOMY__9606"
      minerva_ref_type__resource5 "EC__3.4.21.34"
      minerva_ref_type__resource6 "UNIPROT__P03952"
      minerva_ref_type__resource7 "UNIPROT__P03952"
      minerva_ref_type__resource8 "MESH_2012__D020842"
      minerva_ref_type__resource9 "HGNC__6371"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 453.0
      minerva_y 1007.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa251"
      species_meta_id "s_id_sa251"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 27
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 600.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "il2ra__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa306"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa306"
      minerva_former_symbols "IDDM10; IL2R"
      minerva_name "IL2RA"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000134460"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P01589"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P01589"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/3559"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/3559"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6008"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL2RA"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_000417"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL2RA"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000134460"
      minerva_ref_type__resource2 "UNIPROT__P01589"
      minerva_ref_type__resource3 "UNIPROT__P01589"
      minerva_ref_type__resource4 "ENTREZ__3559"
      minerva_ref_type__resource5 "ENTREZ__3559"
      minerva_ref_type__resource6 "HGNC__6008"
      minerva_ref_type__resource7 "HGNC_SYMBOL__IL2RA"
      minerva_ref_type__resource8 "REFSEQ__NM_000417"
      minerva_ref_type__resource9 "HGNC_SYMBOL__IL2RA"
      minerva_type "Protein"
      minerva_x 1600.0
      minerva_y 1570.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa306"
      species_meta_id "s_id_sa306"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 28
    zlevel -1

    cluster [
      cluster "s_id_ca16"
    ]
    graphics [
      x 700.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "c4d_space_deposition__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa388"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa388"
      minerva_name "C4d deposition"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/32299776"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link3 "http://id.nlm.nih.gov/mesh/D018366"
      minerva_ref_type__resource1 "PUBMED__32299776"
      minerva_ref_type__resource2 "TAXONOMY__9606"
      minerva_ref_type__resource3 "MESH_2012__D018366"
      minerva_type "Phenotype"
      minerva_x 1563.0
      minerva_y 554.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca16"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa388"
      species_meta_id "s_id_sa388"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 29
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 800.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "c5b:c6__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa28"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa28"
      minerva_name "C5b:C6"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link2 "http://id.nlm.nih.gov/mesh/D050776"
      minerva_ref_link3 "http://id.nlm.nih.gov/mesh/C050974"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1339"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource2 "MESH_2012__D050776"
      minerva_ref_type__resource3 "MESH_2012__C050974"
      minerva_ref_type__resource4 "HGNC__1339"
      minerva_type "Complex"
      minerva_x 2171.0
      minerva_y 1319.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa28"
      species_meta_id "s_id_csa28"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 30
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 900.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "plat__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa225"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa225"
      minerva_name "PLAT"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.21.68"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/5327"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/protein/NM_000930"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000104368"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/9051"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PLAT"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PLAT"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P00750"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P00750"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/5327"
      minerva_ref_type__resource1 "EC__3.4.21.68"
      minerva_ref_type__resource10 "ENTREZ__5327"
      minerva_ref_type__resource11 "REFSEQ__NM_000930"
      minerva_ref_type__resource2 "TAXONOMY__9606"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000104368"
      minerva_ref_type__resource4 "HGNC__9051"
      minerva_ref_type__resource5 "HGNC_SYMBOL__PLAT"
      minerva_ref_type__resource6 "HGNC_SYMBOL__PLAT"
      minerva_ref_type__resource7 "UNIPROT__P00750"
      minerva_ref_type__resource8 "UNIPROT__P00750"
      minerva_ref_type__resource9 "ENTREZ__5327"
      minerva_type "Protein"
      minerva_x 1177.0
      minerva_y 2546.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa225"
      species_meta_id "s_id_sa225"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 31
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 1000.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "f5a:f10a__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa13"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa13"
      minerva_name "F5a:F10a"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link2 "http://id.nlm.nih.gov/mesh/D015951"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/pubmed/2303476"
      minerva_ref_link5 "http://id.nlm.nih.gov/mesh/C022475"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource2 "MESH_2012__D015951"
      minerva_ref_type__resource3 "PUBMED__2303476"
      minerva_ref_type__resource4 "MESH_2012__D15943"
      minerva_ref_type__resource5 "MESH_2012__C022475"
      minerva_type "Complex"
      minerva_x 550.5
      minerva_y 1867.985294117647
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa13"
      species_meta_id "s_id_csa13"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 32
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 1100.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s618__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa526"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa526"
      minerva_name "s618"
      minerva_type "Degraded"
      minerva_x 578.0
      minerva_y 831.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa526"
      species_meta_id "s_id_sa526"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 33
    zlevel -1

    cluster [
      cluster "s_id_ca13"
    ]
    graphics [
      x 1200.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sars_minus_cov_minus_2_space_viral_space_entry__endothelium,_space_vascular__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa485"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa485"
      minerva_name "SARS-CoV-2 viral entry"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=2697049"
      minerva_ref_link2 "http://id.nlm.nih.gov/mesh/D012327"
      minerva_ref_type__resource1 "TAXONOMY__2697049"
      minerva_ref_type__resource2 "MESH_2012__D012327"
      minerva_type "Phenotype"
      minerva_x 1337.0
      minerva_y 742.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca13"
      compartment_name "endothelium,_space_vascular"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa485"
      species_meta_id "s_id_sa485"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 34
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 200.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "serpine1__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa505"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa505"
      minerva_former_symbols "PAI1; PLANH1"
      minerva_name "SERPINE1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SERPINE1"
      minerva_ref_link11 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/8583"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P05121"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P05121"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/5054"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/5054"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000106366"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/8593"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_000602"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SERPINE1"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "HGNC_SYMBOL__SERPINE1"
      minerva_ref_type__resource11 "HGNC__8583"
      minerva_ref_type__resource2 "UNIPROT__P05121"
      minerva_ref_type__resource3 "UNIPROT__P05121"
      minerva_ref_type__resource4 "ENTREZ__5054"
      minerva_ref_type__resource5 "ENTREZ__5054"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000106366"
      minerva_ref_type__resource7 "HGNC__8593"
      minerva_ref_type__resource8 "REFSEQ__NM_000602"
      minerva_ref_type__resource9 "HGNC_SYMBOL__SERPINE1"
      minerva_type "Protein"
      minerva_x 1368.0
      minerva_y 2297.6
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa505"
      species_meta_id "s_id_sa505"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 35
    zlevel -1

    cluster [
      cluster "s_id_ca14"
    ]
    graphics [
      x 300.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "fibrinogen:gp6__platelet__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa42"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa42"
      minerva_fullName "fibrinogen complex"
      minerva_name "Fibrinogen:GP6"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/14388"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/pubmed/19296670"
      minerva_ref_link4 "http://amigo.geneontology.org/amigo/term/GO:0005577"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource2 "HGNC__14388"
      minerva_ref_type__resource3 "PUBMED__19296670"
      minerva_ref_type__resource4 "GO__GO:0005577"
      minerva_type "Complex"
      minerva_x 2318.0
      minerva_y 2417.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca14"
      compartment_name "platelet"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa42"
      species_meta_id "s_id_csa42"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 36
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 400.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "fibrinogen__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa17"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa17"
      minerva_fullName "fibrinogen complex"
      minerva_name "Fibrinogen"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/3662"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/3661"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/3694"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/pubmed/19296670"
      minerva_ref_link6 "http://amigo.geneontology.org/amigo/term/GO:0005577"
      minerva_ref_type__resource1 "HGNC__3662"
      minerva_ref_type__resource2 "TAXONOMY__9606"
      minerva_ref_type__resource3 "HGNC__3661"
      minerva_ref_type__resource4 "HGNC__3694"
      minerva_ref_type__resource5 "PUBMED__19296670"
      minerva_ref_type__resource6 "GO__GO:0005577"
      minerva_type "Complex"
      minerva_x 1928.0
      minerva_y 2112.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa17"
      species_meta_id "s_id_csa17"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 37
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 500.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "bradykinin(1_minus_5)__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa407"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa407"
      minerva_name "Bradykinin(1-5)"
      minerva_ref_link1 "http://id.nlm.nih.gov/mesh/C079000"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_type__resource1 "MESH_2012__C079000"
      minerva_ref_type__resource2 "TAXONOMY__9606"
      minerva_type "Protein"
      minerva_x 688.0
      minerva_y 907.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa407"
      species_meta_id "s_id_sa407"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 38
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 600.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "thrombosis__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa271"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa271"
      minerva_name "Thrombosis"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link2 "http://id.nlm.nih.gov/mesh/D013923"
      minerva_ref_link3 "http://id.nlm.nih.gov/mesh/D055806"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource2 "MESH_2012__D013923"
      minerva_ref_type__resource3 "MESH_2012__D055806"
      minerva_type "Phenotype"
      minerva_x 1195.5
      minerva_y 1706.75
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa271"
      species_meta_id "s_id_sa271"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 39
    zlevel -1

    cluster [
      cluster "s_id_ca16"
    ]
    graphics [
      x 700.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "prorenin__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa415"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa415"
      minerva_name "Prorenin"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=REN"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/5972"
      minerva_ref_link11 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.23.15"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=REN"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P00797"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P00797"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/9958"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000143839"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/5972"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_000537"
      minerva_ref_type__resource1 "HGNC_SYMBOL__REN"
      minerva_ref_type__resource10 "ENTREZ__5972"
      minerva_ref_type__resource11 "EC__3.4.23.15"
      minerva_ref_type__resource2 "HGNC_SYMBOL__REN"
      minerva_ref_type__resource3 "TAXONOMY__9606"
      minerva_ref_type__resource4 "UNIPROT__P00797"
      minerva_ref_type__resource5 "UNIPROT__P00797"
      minerva_ref_type__resource6 "HGNC__9958"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000143839"
      minerva_ref_type__resource8 "ENTREZ__5972"
      minerva_ref_type__resource9 "REFSEQ__NM_000537"
      minerva_type "Protein"
      minerva_x 772.0
      minerva_y 524.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca16"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa415"
      species_meta_id "s_id_sa415"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 40
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 800.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "thrombin__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa498"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa498"
      minerva_elementId2 "sa203"
      minerva_name "Thrombin"
      minerva_ref_link1 "http://id.nlm.nih.gov/mesh/D013917"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/2147"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/gene/2147"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P00734"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P00734"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link5 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.21.5"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/3535"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_000506"
      minerva_ref_link8 "https://www.ensembl.org/id/ENSG00000180210"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=F2"
      minerva_ref_type__resource1 "MESH_2012__D013917"
      minerva_ref_type__resource10 "ENTREZ__2147"
      minerva_ref_type__resource11 "ENTREZ__2147"
      minerva_ref_type__resource2 "UNIPROT__P00734"
      minerva_ref_type__resource3 "UNIPROT__P00734"
      minerva_ref_type__resource4 "TAXONOMY__9606"
      minerva_ref_type__resource5 "EC__3.4.21.5"
      minerva_ref_type__resource6 "HGNC__3535"
      minerva_ref_type__resource7 "REFSEQ__NM_000506"
      minerva_ref_type__resource8 "ENSEMBL__ENSG00000180210"
      minerva_ref_type__resource9 "HGNC_SYMBOL__F2"
      minerva_type "Protein"
      minerva_x 1823.0
      minerva_x2 617.0
      minerva_y 2037.0
      minerva_y2 2041.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa498"
      species_meta_id "s_id_sa498"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 41
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 900.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "c2a:c4b__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa35"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa35"
      minerva_name "C2a:C4b"
      minerva_ref_link1 "http://id.nlm.nih.gov/mesh/D051574"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link3 "https://www.brenda-enzymes.org/php/result_flat.php4?ecno=3.4.21.43"
      minerva_ref_link4 "http://id.nlm.nih.gov/mesh/D050678"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1324"
      minerva_ref_type__resource1 "MESH_2012__D051574"
      minerva_ref_type__resource2 "TAXONOMY__9606"
      minerva_ref_type__resource3 "BRENDA__3.4.21.43"
      minerva_ref_type__resource4 "MESH_2012__D050678"
      minerva_ref_type__resource5 "HGNC__1324"
      minerva_type "Complex"
      minerva_x 1640.0
      minerva_y 1330.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa35"
      species_meta_id "s_id_csa35"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 42
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 1000.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s537__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa460"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa460"
      minerva_name "s537"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/32299776"
      minerva_ref_link2 "http://id.nlm.nih.gov/mesh/D062106"
      minerva_ref_type__resource1 "PUBMED__32299776"
      minerva_ref_type__resource2 "MESH_2012__D062106"
      minerva_type "Degraded"
      minerva_x 1477.0
      minerva_y 892.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa460"
      species_meta_id "s_id_sa460"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 43
    zlevel -1

    cluster [
      cluster "s_id_ca13"
    ]
    graphics [
      x 1100.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "c5b:c6:c7:c8a:c8b:c8g__endothelium,_space_vascular__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa31"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa31"
      minerva_name "C5b:C6:C7:C8A:C8B:C8G"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1352"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1353"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/pubmed/28630159"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1354"
      minerva_ref_link6 "http://id.nlm.nih.gov/mesh/C042295"
      minerva_ref_link7 "http://id.nlm.nih.gov/mesh/D050776"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1339"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource2 "HGNC__1352"
      minerva_ref_type__resource3 "HGNC__1353"
      minerva_ref_type__resource4 "PUBMED__28630159"
      minerva_ref_type__resource5 "HGNC__1354"
      minerva_ref_type__resource6 "MESH_2012__C042295"
      minerva_ref_type__resource7 "MESH_2012__D050776"
      minerva_ref_type__resource8 "HGNC__1339"
      minerva_type "Complex"
      minerva_x 2554.0
      minerva_y 765.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca13"
      compartment_name "endothelium,_space_vascular"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa31"
      species_meta_id "s_id_csa31"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 44
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 1200.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "c8a:c8b:c8g__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa30"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa30"
      minerva_name "C8A:C8B:C8G"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1352"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1353"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1354"
      minerva_ref_link5 "http://id.nlm.nih.gov/mesh/D003185"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource2 "HGNC__1352"
      minerva_ref_type__resource3 "HGNC__1353"
      minerva_ref_type__resource4 "HGNC__1354"
      minerva_ref_type__resource5 "MESH_2012__D003185"
      minerva_type "Complex"
      minerva_x 2326.75
      minerva_y 1004.75
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa30"
      species_meta_id "s_id_csa30"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 45
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 200.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "plau__blood__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa236"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa236"
      minerva_name "PLAU"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "http://purl.uniprot.org/uniprot/P00749"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/protein/NM_002658"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/5328"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/5328"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PLAU"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PLAU"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.21.73"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000122861"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/9052"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P00749"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "UNIPROT__P00749"
      minerva_ref_type__resource11 "REFSEQ__NM_002658"
      minerva_ref_type__resource2 "ENTREZ__5328"
      minerva_ref_type__resource3 "ENTREZ__5328"
      minerva_ref_type__resource4 "HGNC_SYMBOL__PLAU"
      minerva_ref_type__resource5 "HGNC_SYMBOL__PLAU"
      minerva_ref_type__resource6 "EC__3.4.21.73"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000122861"
      minerva_ref_type__resource8 "HGNC__9052"
      minerva_ref_type__resource9 "UNIPROT__P00749"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 923.0
      minerva_y 2595.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa236"
      species_meta_id "s_id_sa236"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 46
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 300.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "il8__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa304"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa304"
      minerva_former_symbols "IL8"
      minerva_name "IL8"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_000584"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/3576"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000169429"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6025"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CXCL8"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CXCL8"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P10145"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P10145"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/3576"
      minerva_ref_type__resource1 "REFSEQ__NM_000584"
      minerva_ref_type__resource10 "ENTREZ__3576"
      minerva_ref_type__resource2 "TAXONOMY__9606"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000169429"
      minerva_ref_type__resource4 "HGNC__6025"
      minerva_ref_type__resource5 "HGNC_SYMBOL__CXCL8"
      minerva_ref_type__resource6 "HGNC_SYMBOL__CXCL8"
      minerva_ref_type__resource7 "UNIPROT__P10145"
      minerva_ref_type__resource8 "UNIPROT__P10145"
      minerva_ref_type__resource9 "ENTREZ__3576"
      minerva_type "Protein"
      minerva_x 1590.0
      minerva_y 1627.8541666666667
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa304"
      species_meta_id "s_id_sa304"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 47
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 400.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "masp1__blood__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa358"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa358"
      minerva_former_symbols "CRARF; PRSS5"
      minerva_name "MASP1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "http://purl.uniprot.org/uniprot/P48740"
      minerva_ref_link11 "http://purl.uniprot.org/uniprot/P48740"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000127241"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/5648"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/5648"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MASP1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_001879"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MASP1"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6901"
      minerva_ref_link9 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.21.-"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "UNIPROT__P48740"
      minerva_ref_type__resource11 "UNIPROT__P48740"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000127241"
      minerva_ref_type__resource3 "ENTREZ__5648"
      minerva_ref_type__resource4 "ENTREZ__5648"
      minerva_ref_type__resource5 "HGNC_SYMBOL__MASP1"
      minerva_ref_type__resource6 "REFSEQ__NM_001879"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MASP1"
      minerva_ref_type__resource8 "HGNC__6901"
      minerva_ref_type__resource9 "EC__3.4.21.-"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 1757.0
      minerva_y 987.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa358"
      species_meta_id "s_id_sa358"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 48
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 500.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "plasminogen__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa211"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa211"
      minerva_name "Plasminogen"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PLG"
      minerva_ref_link10 "http://purl.uniprot.org/uniprot/P00747"
      minerva_ref_link11 "http://purl.uniprot.org/uniprot/P00747"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PLG"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.21.7"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000122194"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/5340"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/5340"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/9071"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_000301"
      minerva_ref_type__resource1 "HGNC_SYMBOL__PLG"
      minerva_ref_type__resource10 "UNIPROT__P00747"
      minerva_ref_type__resource11 "UNIPROT__P00747"
      minerva_ref_type__resource2 "HGNC_SYMBOL__PLG"
      minerva_ref_type__resource3 "EC__3.4.21.7"
      minerva_ref_type__resource4 "TAXONOMY__9606"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000122194"
      minerva_ref_type__resource6 "ENTREZ__5340"
      minerva_ref_type__resource7 "ENTREZ__5340"
      minerva_ref_type__resource8 "HGNC__9071"
      minerva_ref_type__resource9 "REFSEQ__NM_000301"
      minerva_type "Protein"
      minerva_x 649.0
      minerva_y 2615.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa211"
      species_meta_id "s_id_sa211"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 49
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 600.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "f12__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa170"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa170"
      minerva_name "F12"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/3530"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=F12"
      minerva_ref_link11 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.21.38"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000131187"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/2161"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/2161"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000505"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=F12"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P00748"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P00748"
      minerva_ref_type__resource1 "HGNC__3530"
      minerva_ref_type__resource10 "HGNC_SYMBOL__F12"
      minerva_ref_type__resource11 "EC__3.4.21.38"
      minerva_ref_type__resource2 "TAXONOMY__9606"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000131187"
      minerva_ref_type__resource4 "ENTREZ__2161"
      minerva_ref_type__resource5 "ENTREZ__2161"
      minerva_ref_type__resource6 "REFSEQ__NM_000505"
      minerva_ref_type__resource7 "HGNC_SYMBOL__F12"
      minerva_ref_type__resource8 "UNIPROT__P00748"
      minerva_ref_type__resource9 "UNIPROT__P00748"
      minerva_type "Protein"
      minerva_x 267.0
      minerva_y 1154.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa170"
      species_meta_id "s_id_sa170"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 50
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 700.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "kininogen__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa250"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa250"
      minerva_former_symbols "BDK; KNG"
      minerva_name "Kininogen"
      minerva_ref_link1 "http://id.nlm.nih.gov/mesh/D019679"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000113889"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=KNG1"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/3827"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/3827"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P01042"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P01042"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_001102416"
      minerva_ref_link9 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6383"
      minerva_ref_type__resource1 "MESH_2012__D019679"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000113889"
      minerva_ref_type__resource2 "HGNC_SYMBOL__KNG1"
      minerva_ref_type__resource3 "TAXONOMY__9606"
      minerva_ref_type__resource4 "ENTREZ__3827"
      minerva_ref_type__resource5 "ENTREZ__3827"
      minerva_ref_type__resource6 "UNIPROT__P01042"
      minerva_ref_type__resource7 "UNIPROT__P01042"
      minerva_ref_type__resource8 "REFSEQ__NM_001102416"
      minerva_ref_type__resource9 "HGNC__6383"
      minerva_type "Protein"
      minerva_x 473.0
      minerva_y 952.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa250"
      species_meta_id "s_id_sa250"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 51
    zlevel -1

    cluster [
      cluster "s_id_ca14"
    ]
    graphics [
      x 800.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s521__platelet__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa440"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa440"
      minerva_name "s521"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/19286885"
      minerva_ref_link2 "http://id.nlm.nih.gov/mesh/D062106"
      minerva_ref_type__resource1 "PUBMED__19286885"
      minerva_ref_type__resource2 "MESH_2012__D062106"
      minerva_type "Degraded"
      minerva_x 1873.0
      minerva_y 2742.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca14"
      compartment_name "platelet"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa440"
      species_meta_id "s_id_sa440"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 52
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 900.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "acth__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa527"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa527"
      minerva_fullName "corticotropin"
      minerva_name "ACTH"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:3892"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/9201"
      minerva_ref_type__resource1 "CHEBI__CHEBI:3892"
      minerva_ref_type__resource2 "HGNC__9201"
      minerva_synonyms "ACTH; Adrenocorticotropic hormone; Corticotropin; SYSMEHFRWGKPVGKKRRPVKVYPDGAEDQLAEAFPLEF; adrenocorticotropin; corticotrofina; corticotrophine; corticotrophinum; corticotropin; cortrophin"
      minerva_type "Simple molecule"
      minerva_x 905.0
      minerva_y 1130.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa527"
      species_meta_id "s_id_sa527"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 53
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 1000.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "c4__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa363"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa363"
      minerva_name "C4"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource2 "MESH_2012__D00318"
      minerva_type "Protein"
      minerva_x 1558.0
      minerva_y 982.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa363"
      species_meta_id "s_id_sa363"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 54
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 1100.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "masp2__blood__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa459"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa459"
      minerva_former_symbols "MASP1P1"
      minerva_name "MASP2"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/O00187"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MASP2"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/gene/10747"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/O00187"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_006610"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000009724"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.21.104"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6902"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MASP2"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/10747"
      minerva_ref_type__resource1 "UNIPROT__O00187"
      minerva_ref_type__resource10 "HGNC_SYMBOL__MASP2"
      minerva_ref_type__resource11 "ENTREZ__10747"
      minerva_ref_type__resource2 "UNIPROT__O00187"
      minerva_ref_type__resource3 "TAXONOMY__9606"
      minerva_ref_type__resource4 "REFSEQ__NM_006610"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000009724"
      minerva_ref_type__resource6 "EC__3.4.21.104"
      minerva_ref_type__resource7 "HGNC__6902"
      minerva_ref_type__resource8 "HGNC_SYMBOL__MASP2"
      minerva_ref_type__resource9 "ENTREZ__10747"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 1713.0
      minerva_y 902.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa459"
      species_meta_id "s_id_sa459"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 55
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 1200.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "f8a__blood__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa180"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa180"
      minerva_former_symbols "F8C"
      minerva_name "F8a"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_000132"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000185010"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=F8"
      minerva_ref_link4 "http://id.nlm.nih.gov/mesh/D015944"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/3546"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P00451"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P00451"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/2157"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/2157"
      minerva_ref_type__resource1 "REFSEQ__NM_000132"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000185010"
      minerva_ref_type__resource2 "TAXONOMY__9606"
      minerva_ref_type__resource3 "HGNC_SYMBOL__F8"
      minerva_ref_type__resource4 "MESH_2012__D015944"
      minerva_ref_type__resource5 "HGNC__3546"
      minerva_ref_type__resource6 "UNIPROT__P00451"
      minerva_ref_type__resource7 "UNIPROT__P00451"
      minerva_ref_type__resource8 "ENTREZ__2157"
      minerva_ref_type__resource9 "ENTREZ__2157"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 523.0
      minerva_y 1448.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa180"
      species_meta_id "s_id_sa180"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 56
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 200.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "c4a__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa364"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa364"
      minerva_name "C4a"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Protein"
      minerva_x 1638.0
      minerva_y 1072.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa364"
      species_meta_id "s_id_sa364"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 57
    zlevel -1

    cluster [
      cluster "s_id_ca14"
    ]
    graphics [
      x 300.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "itga2:itgab1__platelet__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa41"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa41"
      minerva_name "ITGA2:ITGAB1"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6137"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link3 "https://www.ebi.ac.uk/intact/interaction/EBI-16428357"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6153"
      minerva_ref_type__resource1 "HGNC__6137"
      minerva_ref_type__resource2 "TAXONOMY__9606"
      minerva_ref_type__resource3 "INTACT__EBI-16428357"
      minerva_ref_type__resource4 "HGNC__6153"
      minerva_type "Complex"
      minerva_x 2048.0
      minerva_y 2332.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca14"
      compartment_name "platelet"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa41"
      species_meta_id "s_id_csa41"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 58
    zlevel -1

    cluster [
      cluster "s_id_ca14"
    ]
    graphics [
      x 400.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "gp6__platelet__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa431"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa431"
      minerva_name "GP6"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "http://purl.uniprot.org/uniprot/Q9HCN6"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=GP6"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=GP6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/14388"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000088053"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/51206"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/51206"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_001083899"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q9HCN6"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "UNIPROT__Q9HCN6"
      minerva_ref_type__resource2 "HGNC_SYMBOL__GP6"
      minerva_ref_type__resource3 "HGNC_SYMBOL__GP6"
      minerva_ref_type__resource4 "HGNC__14388"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000088053"
      minerva_ref_type__resource6 "ENTREZ__51206"
      minerva_ref_type__resource7 "ENTREZ__51206"
      minerva_ref_type__resource8 "REFSEQ__NM_001083899"
      minerva_ref_type__resource9 "UNIPROT__Q9HCN6"
      minerva_type "Protein"
      minerva_x 2163.0
      minerva_y 2372.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca14"
      compartment_name "platelet"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa431"
      species_meta_id "s_id_sa431"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 59
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 500.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "masp2__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa357"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa357"
      minerva_former_symbols "MASP1P1"
      minerva_name "MASP2"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/O00187"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MASP2"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/gene/10747"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/O00187"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_006610"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000009724"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.21.104"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6902"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MASP2"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/10747"
      minerva_ref_type__resource1 "UNIPROT__O00187"
      minerva_ref_type__resource10 "HGNC_SYMBOL__MASP2"
      minerva_ref_type__resource11 "ENTREZ__10747"
      minerva_ref_type__resource2 "UNIPROT__O00187"
      minerva_ref_type__resource3 "TAXONOMY__9606"
      minerva_ref_type__resource4 "REFSEQ__NM_006610"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000009724"
      minerva_ref_type__resource6 "EC__3.4.21.104"
      minerva_ref_type__resource7 "HGNC__6902"
      minerva_ref_type__resource8 "HGNC_SYMBOL__MASP2"
      minerva_ref_type__resource9 "ENTREZ__10747"
      minerva_type "Protein"
      minerva_x 1558.0
      minerva_y 902.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa357"
      species_meta_id "s_id_sa357"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 61
    zlevel -1

    cluster [
      cluster "s_id_ca14"
    ]
    graphics [
      x 700.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "agtr1__platelet__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa500"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa500"
      minerva_former_symbols "AGTR1B"
      minerva_name "AGTR1"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "Protein"
      minerva_x 2526.0
      minerva_y 2375.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca14"
      compartment_name "platelet"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa500"
      species_meta_id "s_id_sa500"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 62
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 800.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "bradykinin__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa402"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa402"
      minerva_name "Bradykinin"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:3165"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource2 "CHEBI__CHEBI:3165"
      minerva_type "Protein"
      minerva_x 483.0
      minerva_y 907.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa402"
      species_meta_id "s_id_sa402"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 63
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 900.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "c9__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa337"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa337"
      minerva_name "C9"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/735"
      minerva_ref_link10 "http://purl.uniprot.org/uniprot/P02748"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/735"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1358"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000113600"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C9"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C9"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_001737"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P02748"
      minerva_ref_type__resource1 "ENTREZ__735"
      minerva_ref_type__resource10 "UNIPROT__P02748"
      minerva_ref_type__resource2 "ENTREZ__735"
      minerva_ref_type__resource3 "TAXONOMY__9606"
      minerva_ref_type__resource4 "HGNC__1358"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000113600"
      minerva_ref_type__resource6 "HGNC_SYMBOL__C9"
      minerva_ref_type__resource7 "HGNC_SYMBOL__C9"
      minerva_ref_type__resource8 "REFSEQ__NM_001737"
      minerva_ref_type__resource9 "UNIPROT__P02748"
      minerva_type "Protein"
      minerva_x 2641.0
      minerva_y 919.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa337"
      species_meta_id "s_id_sa337"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 64
    zlevel -1

    cluster [
      cluster "s_id_ca14"
    ]
    graphics [
      x 1000.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mas1__platelet__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa483"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa483"
      minerva_name "MAS1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_002377"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000130368"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6899"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4142"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/4142"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAS1"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/P04201"
      minerva_ref_link8 "https://purl.uniprot.org/uniprot/P04201"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAS1"
      minerva_ref_type__resource1 "REFSEQ__NM_002377"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000130368"
      minerva_ref_type__resource3 "HGNC__6899"
      minerva_ref_type__resource4 "ENTREZ__4142"
      minerva_ref_type__resource5 "ENTREZ__4142"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MAS1"
      minerva_ref_type__resource7 "UNIPROT__P04201"
      minerva_ref_type__resource8 "UNIPROT__P04201"
      minerva_ref_type__resource9 "HGNC_SYMBOL__MAS1"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 2825.0
      minerva_y 2543.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca14"
      compartment_name "platelet"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa483"
      species_meta_id "s_id_sa483"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 65
    zlevel -1

    cluster [
      cluster "s_id_ca13"
    ]
    graphics [
      x 1100.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nr3c2__endothelium,_space_vascular__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa516"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa516"
      minerva_former_symbols "MLR"
      minerva_name "NR3C2"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NR3C2"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P08235"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P08235"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NR3C2"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/4306"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/4306"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_000901"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7979"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000151623"
      minerva_ref_type__resource1 "HGNC_SYMBOL__NR3C2"
      minerva_ref_type__resource2 "UNIPROT__P08235"
      minerva_ref_type__resource3 "UNIPROT__P08235"
      minerva_ref_type__resource4 "HGNC_SYMBOL__NR3C2"
      minerva_ref_type__resource5 "ENTREZ__4306"
      minerva_ref_type__resource6 "ENTREZ__4306"
      minerva_ref_type__resource7 "REFSEQ__NM_000901"
      minerva_ref_type__resource8 "HGNC__7979"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000151623"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 443.5
      minerva_y 598.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca13"
      compartment_name "endothelium,_space_vascular"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa516"
      species_meta_id "s_id_sa516"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 66
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 1200.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "kng1:klkb1__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa11"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa11"
      minerva_name "KNG1:KLKB1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/17598838"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link3 "https://www.ebi.ac.uk/intact/interaction/EBI-10087151"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6371"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6383"
      minerva_ref_type__resource1 "PUBMED__17598838"
      minerva_ref_type__resource2 "TAXONOMY__9606"
      minerva_ref_type__resource3 "INTACT__EBI-10087151"
      minerva_ref_type__resource4 "HGNC__6371"
      minerva_ref_type__resource5 "HGNC__6383"
      minerva_type "Complex"
      minerva_x 241.0
      minerva_y 917.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa11"
      species_meta_id "s_id_csa11"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 67
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 200.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "fibrin_space_polymer__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa424"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa424"
      minerva_name "Fibrin polymer"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link2 "http://id.nlm.nih.gov/mesh/C465961"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource2 "MESH_2012__C465961"
      minerva_type "Protein"
      minerva_x 1498.0
      minerva_y 2112.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa424"
      species_meta_id "s_id_sa424"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 68
    zlevel -1

    cluster [
      cluster "s_id_ca14"
    ]
    graphics [
      x 300.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "thrombus_space_formation__platelet__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa441"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa441"
      minerva_name "thrombus formation"
      minerva_ref_link1 "http://id.nlm.nih.gov/mesh/D013917"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_type__resource1 "MESH_2012__D013917"
      minerva_ref_type__resource2 "TAXONOMY__9606"
      minerva_type "Phenotype"
      minerva_x 2433.0
      minerva_y 2578.25
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca14"
      compartment_name "platelet"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa441"
      species_meta_id "s_id_sa441"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 69
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 400.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "hypokalemia__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa531"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa531"
      minerva_name "Hypokalemia"
      minerva_ref_link1 "http://id.nlm.nih.gov/mesh/D007008"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_type__resource1 "MESH_2012__D007008"
      minerva_ref_type__resource2 "TAXONOMY__9606"
      minerva_type "Phenotype"
      minerva_x 686.0
      minerva_y 1106.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa531"
      species_meta_id "s_id_sa531"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 70
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 500.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "thrombin:thrombomodulin__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa25"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa25"
      minerva_name "Thrombin:Thrombomodulin"
      minerva_ref_link1 "http://id.nlm.nih.gov/mesh/D013917"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11784"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9986"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/pubmed/6282863"
      minerva_ref_type__resource1 "MESH_2012__D013917"
      minerva_ref_type__resource2 "HGNC__11784"
      minerva_ref_type__resource3 "TAXONOMY__9986"
      minerva_ref_type__resource4 "PUBMED__6282863"
      minerva_type "Complex"
      minerva_x 884.0
      minerva_y 2238.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa25"
      species_meta_id "s_id_csa25"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 71
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 600.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "c4b__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa365"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa365"
      minerva_name "C4b"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_001002029"
      minerva_ref_link10 "http://purl.uniprot.org/uniprot/P0C0L5"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000224389"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/100293534"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4B"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/721"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4B"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1324"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L5"
      minerva_ref_type__resource1 "REFSEQ__NM_001002029"
      minerva_ref_type__resource10 "UNIPROT__P0C0L5"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000224389"
      minerva_ref_type__resource3 "TAXONOMY__9606"
      minerva_ref_type__resource4 "ENTREZ__100293534"
      minerva_ref_type__resource5 "HGNC_SYMBOL__C4B"
      minerva_ref_type__resource6 "ENTREZ__721"
      minerva_ref_type__resource7 "HGNC_SYMBOL__C4B"
      minerva_ref_type__resource8 "HGNC__1324"
      minerva_ref_type__resource9 "UNIPROT__P0C0L5"
      minerva_type "Protein"
      minerva_x 1498.0
      minerva_y 1147.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa365"
      species_meta_id "s_id_sa365"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 72
    zlevel -1

    cluster [
      cluster "s_id_ca13"
    ]
    graphics [
      x 700.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s552__endothelium,_space_vascular__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa478"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa478"
      minerva_name "s552"
      minerva_ref_link1 "http://id.nlm.nih.gov/mesh/D062106"
      minerva_ref_link2 "https://doi.org/10.1101/2020.04.25.200"
      minerva_ref_type__resource1 "MESH_2012__D062106"
      minerva_ref_type__resource2 "DOI__10.1101/2020.04.25.200"
      minerva_type "Degraded"
      minerva_x 1143.0
      minerva_y 2920.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca13"
      compartment_name "endothelium,_space_vascular"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa478"
      species_meta_id "s_id_sa478"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 73
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 800.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cytokine_space_storm__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa416"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa416"
      minerva_name "cytokine storm"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/pubmed/2504360"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource2 "PUBMED__2504360"
      minerva_type "Phenotype"
      minerva_x 1911.0
      minerva_y 1873.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa416"
      species_meta_id "s_id_sa416"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 74
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 900.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "c6__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa318"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa318"
      minerva_name "C6"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/729"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C6"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C6"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_000065"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000039537"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P13671"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P13671"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1339"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/729"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "ENTREZ__729"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C6"
      minerva_ref_type__resource3 "HGNC_SYMBOL__C6"
      minerva_ref_type__resource4 "REFSEQ__NM_000065"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000039537"
      minerva_ref_type__resource6 "UNIPROT__P13671"
      minerva_ref_type__resource7 "UNIPROT__P13671"
      minerva_ref_type__resource8 "HGNC__1339"
      minerva_ref_type__resource9 "ENTREZ__729"
      minerva_type "Protein"
      minerva_x 1993.0
      minerva_y 1280.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa318"
      species_meta_id "s_id_sa318"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 75
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 1000.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "serpinf2:plasmin__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa38"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa38"
      minerva_name "SERPINF2:Plasmin"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/pubmed/2437112"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/9075"
      minerva_ref_link4 "http://id.nlm.nih.gov/mesh/D005341"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource2 "PUBMED__2437112"
      minerva_ref_type__resource3 "HGNC__9075"
      minerva_ref_type__resource4 "MESH_2012__D005341"
      minerva_type "Complex"
      minerva_x 393.0
      minerva_y 2430.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa38"
      species_meta_id "s_id_csa38"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 76
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 1100.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "proc__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa482"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa482"
      minerva_name "PROC"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.21.69"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PROC"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/gene/5624"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000115718"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/9451"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_000312"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P04070"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P04070"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PROC"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/5624"
      minerva_ref_type__resource1 "EC__3.4.21.69"
      minerva_ref_type__resource10 "HGNC_SYMBOL__PROC"
      minerva_ref_type__resource11 "ENTREZ__5624"
      minerva_ref_type__resource2 "TAXONOMY__9606"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000115718"
      minerva_ref_type__resource4 "HGNC__9451"
      minerva_ref_type__resource5 "REFSEQ__NM_000312"
      minerva_ref_type__resource6 "UNIPROT__P04070"
      minerva_ref_type__resource7 "UNIPROT__P04070"
      minerva_ref_type__resource8 "HGNC_SYMBOL__PROC"
      minerva_ref_type__resource9 "ENTREZ__5624"
      minerva_type "Protein"
      minerva_x 693.0
      minerva_y 1585.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa482"
      species_meta_id "s_id_sa482"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 77
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 1200.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "plat__blood__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa213"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa213"
      minerva_name "PLAT"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.21.68"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/5327"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/protein/NM_000930"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000104368"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/9051"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PLAT"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PLAT"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P00750"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P00750"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/5327"
      minerva_ref_type__resource1 "EC__3.4.21.68"
      minerva_ref_type__resource10 "ENTREZ__5327"
      minerva_ref_type__resource11 "REFSEQ__NM_000930"
      minerva_ref_type__resource2 "TAXONOMY__9606"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000104368"
      minerva_ref_type__resource4 "HGNC__9051"
      minerva_ref_type__resource5 "HGNC_SYMBOL__PLAT"
      minerva_ref_type__resource6 "HGNC_SYMBOL__PLAT"
      minerva_ref_type__resource7 "UNIPROT__P00750"
      minerva_ref_type__resource8 "UNIPROT__P00750"
      minerva_ref_type__resource9 "ENTREZ__5327"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 862.0
      minerva_y 2546.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa213"
      species_meta_id "s_id_sa213"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 78
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 200.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "angiotensin_space_ii__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa194"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa194"
      minerva_fullName "angiotensin I"
      minerva_name "angiotensin II"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:2718"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource2 "CHEBI__CHEBI:2718"
      minerva_synonyms "Asp-Arg-Val-Tyr-Ile-His-Pro-Phe-His-Leu; D-R-V-Y-I-H-P-F-H-L; DRVYIHPFHL; L-Asp-L-Arg-L-Val-L-Tyr-L-Ile-L-His-L-Pro-L-Phe-L-His-L-Leu; angiotensin I; pepsitensin; proangiotensin"
      minerva_type "Simple molecule"
      minerva_x 1063.25
      minerva_y 1244.75
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa194"
      species_meta_id "s_id_sa194"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 79
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 300.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "aldosterone__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa503"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa503"
      minerva_fullName "aldosterone"
      minerva_name "aldosterone"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:27584"
      minerva_ref_type__resource1 "CHEBI__CHEBI:27584"
      minerva_synonyms "(+)-aldosterone; (11beta)-11,21-dihydroxy-3,20-dioxopregn-4-en-18-al; 11beta,21-Dihydroxy-3,20-dioxo-4-pregnen-18-al; ALDOSTERONE; Aldosterone; aldosterone"
      minerva_type "Simple molecule"
      minerva_x 805.4000000000001
      minerva_y 1158.1
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa503"
      species_meta_id "s_id_sa503"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 80
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 400.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "f8__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa179"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa179"
      minerva_former_symbols "F8C"
      minerva_name "F8"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_000132"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000185010"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=F8"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=F8"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/3546"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P00451"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P00451"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/2157"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/2157"
      minerva_ref_type__resource1 "REFSEQ__NM_000132"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000185010"
      minerva_ref_type__resource2 "TAXONOMY__9606"
      minerva_ref_type__resource3 "HGNC_SYMBOL__F8"
      minerva_ref_type__resource4 "HGNC_SYMBOL__F8"
      minerva_ref_type__resource5 "HGNC__3546"
      minerva_ref_type__resource6 "UNIPROT__P00451"
      minerva_ref_type__resource7 "UNIPROT__P00451"
      minerva_ref_type__resource8 "ENTREZ__2157"
      minerva_ref_type__resource9 "ENTREZ__2157"
      minerva_type "Protein"
      minerva_x 662.0
      minerva_y 1463.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa179"
      species_meta_id "s_id_sa179"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 81
    zlevel -1

    cluster [
      cluster "s_id_ca16"
    ]
    graphics [
      x 500.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "apoptosis__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa408"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa408"
      minerva_fullName "apoptotic process"
      minerva_name "apoptosis"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link2 "http://amigo.geneontology.org/amigo/term/GO:0006915"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource2 "GO__GO:0006915"
      minerva_type "Phenotype"
      minerva_x 3093.0
      minerva_y 435.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca16"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa408"
      species_meta_id "s_id_sa408"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 82
    zlevel -1

    cluster [
      cluster "s_id_ca13"
    ]
    graphics [
      x 600.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "c5b:c6:c7__endothelium,_space_vascular__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa29"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa29"
      minerva_name "C5b:C6:C7"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/pubmed/28630159"
      minerva_ref_link3 "http://id.nlm.nih.gov/mesh/C037453"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1346"
      minerva_ref_link5 "http://id.nlm.nih.gov/mesh/D050776"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1339"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource2 "PUBMED__28630159"
      minerva_ref_type__resource3 "MESH_2012__C037453"
      minerva_ref_type__resource4 "HGNC__1346"
      minerva_ref_type__resource5 "MESH_2012__D050776"
      minerva_ref_type__resource6 "HGNC__1339"
      minerva_type "Complex"
      minerva_x 2254.0
      minerva_y 823.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca13"
      compartment_name "endothelium,_space_vascular"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa29"
      species_meta_id "s_id_csa29"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 83
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 700.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "fibrin_space_monomer__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa395"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa395"
      minerva_name "Fibrin monomer"
      minerva_ref_link1 "http://id.nlm.nih.gov/mesh/C011468"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_type__resource1 "MESH_2012__C011468"
      minerva_ref_type__resource2 "TAXONOMY__9606"
      minerva_type "Protein"
      minerva_x 1666.0
      minerva_y 2113.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa395"
      species_meta_id "s_id_sa395"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 84
    zlevel -1

    cluster [
      cluster "s_id_ca13"
    ]
    graphics [
      x 800.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "agtr1__endothelium,_space_vascular__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa520"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa520"
      minerva_former_symbols "AGTR1B"
      minerva_name "AGTR1"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "Protein"
      minerva_x 926.5
      minerva_y 781.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca13"
      compartment_name "endothelium,_space_vascular"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa520"
      species_meta_id "s_id_sa520"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 85
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 900.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "c3b:bb__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa26"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa26"
      minerva_name "C3b:Bb"
      minerva_ref_link1 "http://id.nlm.nih.gov/mesh/D003179"
      minerva_ref_link2 "https://www.brenda-enzymes.org/php/result_flat.php4?ecno=3.4.21.47"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/pubmed/12440962"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1037"
      minerva_ref_link6 "http://id.nlm.nih.gov/mesh/D051561"
      minerva_ref_type__resource1 "MESH_2012__D003179"
      minerva_ref_type__resource2 "BRENDA__3.4.21.47"
      minerva_ref_type__resource3 "TAXONOMY__9606"
      minerva_ref_type__resource4 "PUBMED__12440962"
      minerva_ref_type__resource5 "HGNC__1037"
      minerva_ref_type__resource6 "MESH_2012__D051561"
      minerva_type "Complex"
      minerva_x 1991.0
      minerva_y 1045.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa26"
      species_meta_id "s_id_csa26"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 86
    zlevel -1

    cluster [
      cluster "s_id_ca13"
    ]
    graphics [
      x 1000.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ace2:spike__endothelium,_space_vascular__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa37"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa37"
      minerva_name "ACE2:Spike"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P0DTC2"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/13557"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=2697049"
      minerva_ref_link5 "https://www.rcsb.org/structure/6CS2"
      minerva_ref_type__resource1 "UNIPROT__P0DTC2"
      minerva_ref_type__resource2 "TAXONOMY__9606"
      minerva_ref_type__resource3 "HGNC__13557"
      minerva_ref_type__resource4 "TAXONOMY__2697049"
      minerva_ref_type__resource5 "PDB__6CS2"
      minerva_type "Complex"
      minerva_x 1262.0
      minerva_y 647.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca13"
      compartment_name "endothelium,_space_vascular"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa37"
      species_meta_id "s_id_csa37"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 87
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 1100.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "f11a__blood__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa173"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa173"
      minerva_name "F11a"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/3529"
      minerva_ref_link11 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.21.27"
      minerva_ref_link12 "https://www.ncbi.nlm.nih.gov/protein/NM_000128"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/2160"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/2160"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P03951"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P03951"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000088926"
      minerva_ref_link7 "http://id.nlm.nih.gov/mesh/D015945"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=F11"
      minerva_ref_link9 "https://www.brenda-enzymes.org/php/result_flat.php4?ecno=3.4.21.27"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "HGNC__3529"
      minerva_ref_type__resource11 "EC__3.4.21.27"
      minerva_ref_type__resource12 "REFSEQ__NM_000128"
      minerva_ref_type__resource2 "ENTREZ__2160"
      minerva_ref_type__resource3 "ENTREZ__2160"
      minerva_ref_type__resource4 "UNIPROT__P03951"
      minerva_ref_type__resource5 "UNIPROT__P03951"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000088926"
      minerva_ref_type__resource7 "MESH_2012__D015945"
      minerva_ref_type__resource8 "HGNC_SYMBOL__F11"
      minerva_ref_type__resource9 "BRENDA__3.4.21.27"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 393.0
      minerva_y 1269.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa173"
      species_meta_id "s_id_sa173"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 88
    zlevel -1

    cluster [
      cluster "s_id_ca14"
    ]
    graphics [
      x 1200.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "agtr1__platelet__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa484"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa484"
      minerva_former_symbols "AGTR1B"
      minerva_name "AGTR1"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 2656.0
      minerva_y 2395.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca14"
      compartment_name "platelet"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa484"
      species_meta_id "s_id_sa484"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 89
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 200.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tnf__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa238"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa238"
      minerva_former_symbols "TNFA"
      minerva_name "TNF"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_000594"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000232810"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TNF"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TNF"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P01375"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P01375"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11892"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/7124"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/7124"
      minerva_ref_type__resource1 "REFSEQ__NM_000594"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000232810"
      minerva_ref_type__resource2 "HGNC_SYMBOL__TNF"
      minerva_ref_type__resource3 "HGNC_SYMBOL__TNF"
      minerva_ref_type__resource4 "TAXONOMY__9606"
      minerva_ref_type__resource5 "UNIPROT__P01375"
      minerva_ref_type__resource6 "UNIPROT__P01375"
      minerva_ref_type__resource7 "HGNC__11892"
      minerva_ref_type__resource8 "ENTREZ__7124"
      minerva_ref_type__resource9 "ENTREZ__7124"
      minerva_type "Protein"
      minerva_x 1523.0
      minerva_y 1839.416666666667
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa238"
      species_meta_id "s_id_sa238"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 90
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 300.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "serpine1__blood__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa224"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa224"
      minerva_former_symbols "PAI1; PLANH1"
      minerva_name "SERPINE1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SERPINE1"
      minerva_ref_link11 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/8583"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P05121"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P05121"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/5054"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/5054"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000106366"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/8593"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_000602"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SERPINE1"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "HGNC_SYMBOL__SERPINE1"
      minerva_ref_type__resource11 "HGNC__8583"
      minerva_ref_type__resource2 "UNIPROT__P05121"
      minerva_ref_type__resource3 "UNIPROT__P05121"
      minerva_ref_type__resource4 "ENTREZ__5054"
      minerva_ref_type__resource5 "ENTREZ__5054"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000106366"
      minerva_ref_type__resource7 "HGNC__8593"
      minerva_ref_type__resource8 "REFSEQ__NM_000602"
      minerva_ref_type__resource9 "HGNC_SYMBOL__SERPINE1"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 1142.0
      minerva_y 2418.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa224"
      species_meta_id "s_id_sa224"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 91
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 400.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "prothrombin__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa182"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa182"
      minerva_name "Prothrombin"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P00734"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/2147"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/gene/2147"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P00734"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.21.5"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/3535"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000506"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000180210"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=F2"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=F2"
      minerva_ref_type__resource1 "UNIPROT__P00734"
      minerva_ref_type__resource10 "ENTREZ__2147"
      minerva_ref_type__resource11 "ENTREZ__2147"
      minerva_ref_type__resource2 "UNIPROT__P00734"
      minerva_ref_type__resource3 "TAXONOMY__9606"
      minerva_ref_type__resource4 "EC__3.4.21.5"
      minerva_ref_type__resource5 "HGNC__3535"
      minerva_ref_type__resource6 "REFSEQ__NM_000506"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000180210"
      minerva_ref_type__resource8 "HGNC_SYMBOL__F2"
      minerva_ref_type__resource9 "HGNC_SYMBOL__F2"
      minerva_type "Protein"
      minerva_x 484.0
      minerva_y 2042.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa182"
      species_meta_id "s_id_sa182"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 92
    zlevel -1

    cluster [
      cluster "s_id_ca16"
    ]
    graphics [
      x 500.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ren__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa197"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa197"
      minerva_name "REN"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=REN"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.23.15"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P00797"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P00797"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/9958"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000143839"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/5972"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_000537"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/5972"
      minerva_ref_type__resource1 "HGNC_SYMBOL__REN"
      minerva_ref_type__resource10 "EC__3.4.23.15"
      minerva_ref_type__resource2 "TAXONOMY__9606"
      minerva_ref_type__resource3 "UNIPROT__P00797"
      minerva_ref_type__resource4 "UNIPROT__P00797"
      minerva_ref_type__resource5 "HGNC__9958"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000143839"
      minerva_ref_type__resource7 "ENTREZ__5972"
      minerva_ref_type__resource8 "REFSEQ__NM_000537"
      minerva_ref_type__resource9 "ENTREZ__5972"
      minerva_type "Protein"
      minerva_x 912.0
      minerva_y 524.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca16"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa197"
      species_meta_id "s_id_sa197"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 93
    zlevel -1

    cluster [
      cluster "s_id_ca13"
    ]
    graphics [
      x 600.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "aldosterone__endothelium,_space_vascular__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa509"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa509"
      minerva_fullName "aldosterone"
      minerva_name "aldosterone"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:27584"
      minerva_ref_type__resource1 "CHEBI__CHEBI:27584"
      minerva_synonyms "(+)-aldosterone; (11beta)-11,21-dihydroxy-3,20-dioxopregn-4-en-18-al; 11beta,21-Dihydroxy-3,20-dioxo-4-pregnen-18-al; ALDOSTERONE; Aldosterone; aldosterone"
      minerva_type "Simple molecule"
      minerva_x 791.5
      minerva_y 682.0999999999999
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca13"
      compartment_name "endothelium,_space_vascular"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa509"
      species_meta_id "s_id_sa509"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 94
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 700.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "f11__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa172"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa172"
      minerva_name "F11"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.21.27"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/protein/NM_000128"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/2160"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/2160"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P03951"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P03951"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000088926"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=F11"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=F11"
      minerva_ref_link9 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/3529"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "EC__3.4.21.27"
      minerva_ref_type__resource11 "REFSEQ__NM_000128"
      minerva_ref_type__resource2 "ENTREZ__2160"
      minerva_ref_type__resource3 "ENTREZ__2160"
      minerva_ref_type__resource4 "UNIPROT__P03951"
      minerva_ref_type__resource5 "UNIPROT__P03951"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000088926"
      minerva_ref_type__resource7 "HGNC_SYMBOL__F11"
      minerva_ref_type__resource8 "HGNC_SYMBOL__F11"
      minerva_ref_type__resource9 "HGNC__3529"
      minerva_type "Protein"
      minerva_x 450.0
      minerva_y 1346.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa172"
      species_meta_id "s_id_sa172"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 95
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 800.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "f10__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa177"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa177"
      minerva_name "F10"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.21.6"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/2159"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/gene/2159"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P00742"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P00742"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_000504"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000126218"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=F10"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=F10"
      minerva_ref_link9 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/3528"
      minerva_ref_type__resource1 "EC__3.4.21.6"
      minerva_ref_type__resource10 "ENTREZ__2159"
      minerva_ref_type__resource11 "ENTREZ__2159"
      minerva_ref_type__resource2 "TAXONOMY__9606"
      minerva_ref_type__resource3 "UNIPROT__P00742"
      minerva_ref_type__resource4 "UNIPROT__P00742"
      minerva_ref_type__resource5 "REFSEQ__NM_000504"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000126218"
      minerva_ref_type__resource7 "HGNC_SYMBOL__F10"
      minerva_ref_type__resource8 "HGNC_SYMBOL__F10"
      minerva_ref_type__resource9 "HGNC__3528"
      minerva_type "Protein"
      minerva_x 283.0
      minerva_y 1746.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa177"
      species_meta_id "s_id_sa177"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 96
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 900.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "c5b__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa315"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa315"
      minerva_name "C5b"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/727"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1331"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C5"
      minerva_ref_link4 "http://id.nlm.nih.gov/mesh/D050776"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_001735"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000106804"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P01031"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P01031"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/727"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "ENTREZ__727"
      minerva_ref_type__resource2 "HGNC__1331"
      minerva_ref_type__resource3 "HGNC_SYMBOL__C5"
      minerva_ref_type__resource4 "MESH_2012__D050776"
      minerva_ref_type__resource5 "REFSEQ__NM_001735"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000106804"
      minerva_ref_type__resource7 "UNIPROT__P01031"
      minerva_ref_type__resource8 "UNIPROT__P01031"
      minerva_ref_type__resource9 "ENTREZ__727"
      minerva_type "Protein"
      minerva_x 1992.0
      minerva_y 1348.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa315"
      species_meta_id "s_id_sa315"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 97
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 1000.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "c3__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa252"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa252"
      minerva_name "C3"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C3"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/718"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C3"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000125730"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_000064"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P01024"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P01024"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1318"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/718"
      minerva_ref_type__resource1 "HGNC_SYMBOL__C3"
      minerva_ref_type__resource10 "ENTREZ__718"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C3"
      minerva_ref_type__resource3 "TAXONOMY__9606"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000125730"
      minerva_ref_type__resource5 "REFSEQ__NM_000064"
      minerva_ref_type__resource6 "UNIPROT__P01024"
      minerva_ref_type__resource7 "UNIPROT__P01024"
      minerva_ref_type__resource8 "HGNC__1318"
      minerva_ref_type__resource9 "ENTREZ__718"
      minerva_type "Protein"
      minerva_x 1805.0
      minerva_y 1171.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa252"
      species_meta_id "s_id_sa252"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 98
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 1100.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "angiotensin_space_i_minus_7__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa400"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa400"
      minerva_fullName "Ile(5)-angiotensin II (1-7)"
      minerva_name "angiotensin I-7"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:55438"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_type__resource1 "CHEBI__CHEBI:55438"
      minerva_ref_type__resource2 "TAXONOMY__9606"
      minerva_synonyms "8-Des-phe-angiotensin II; Angiotensin (1-7); Angiotensin I (1-7); Angiotensin II (1-7); Angiotensin II (1-7) heptapeptide"
      minerva_type "Simple molecule"
      minerva_x 1329.0
      minerva_y 1244.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa400"
      species_meta_id "s_id_sa400"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 99
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 1200.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "f5__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa200"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa200"
      minerva_name "F5"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/3541"
      minerva_ref_link10 "http://purl.uniprot.org/uniprot/P13726"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=F3"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_001993"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=F3"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/2152"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/2152"
      minerva_ref_link8 "https://www.ensembl.org/id/ENSG00000117525"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P13726"
      minerva_ref_type__resource1 "HGNC__3541"
      minerva_ref_type__resource10 "UNIPROT__P13726"
      minerva_ref_type__resource2 "TAXONOMY__9606"
      minerva_ref_type__resource3 "HGNC_SYMBOL__F3"
      minerva_ref_type__resource4 "REFSEQ__NM_001993"
      minerva_ref_type__resource5 "HGNC_SYMBOL__F3"
      minerva_ref_type__resource6 "ENTREZ__2152"
      minerva_ref_type__resource7 "ENTREZ__2152"
      minerva_ref_type__resource8 "ENSEMBL__ENSG00000117525"
      minerva_ref_type__resource9 "UNIPROT__P13726"
      minerva_type "Protein"
      minerva_x 668.0
      minerva_y 1634.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa200"
      species_meta_id "s_id_sa200"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 100
    zlevel -1

    cluster [
      cluster "s_id_ca13"
    ]
    graphics [
      x 200.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nr3c2__endothelium,_space_vascular__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa506"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa506"
      minerva_former_symbols "MLR"
      minerva_name "NR3C2"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NR3C2"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P08235"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P08235"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NR3C2"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/4306"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/4306"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_000901"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7979"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000151623"
      minerva_ref_type__resource1 "HGNC_SYMBOL__NR3C2"
      minerva_ref_type__resource2 "UNIPROT__P08235"
      minerva_ref_type__resource3 "UNIPROT__P08235"
      minerva_ref_type__resource4 "HGNC_SYMBOL__NR3C2"
      minerva_ref_type__resource5 "ENTREZ__4306"
      minerva_ref_type__resource6 "ENTREZ__4306"
      minerva_ref_type__resource7 "REFSEQ__NM_000901"
      minerva_ref_type__resource8 "HGNC__7979"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000151623"
      minerva_type "Protein"
      minerva_x 602.0
      minerva_y 598.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca13"
      compartment_name "endothelium,_space_vascular"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa506"
      species_meta_id "s_id_sa506"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 101
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 300.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ace2__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa398"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa398"
      minerva_name "ACE2"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000130234"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/59272"
      minerva_ref_link11 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ACE2"
      minerva_ref_link12 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ACE2"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.17.23"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.17.-"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/13557"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/Q9BYF1"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/Q9BYF1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/59272"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_001371415"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000130234"
      minerva_ref_type__resource10 "ENTREZ__59272"
      minerva_ref_type__resource11 "HGNC_SYMBOL__ACE2"
      minerva_ref_type__resource12 "HGNC_SYMBOL__ACE2"
      minerva_ref_type__resource2 "TAXONOMY__9606"
      minerva_ref_type__resource3 "EC__3.4.17.23"
      minerva_ref_type__resource4 "EC__3.4.17.-"
      minerva_ref_type__resource5 "HGNC__13557"
      minerva_ref_type__resource6 "UNIPROT__Q9BYF1"
      minerva_ref_type__resource7 "UNIPROT__Q9BYF1"
      minerva_ref_type__resource8 "ENTREZ__59272"
      minerva_ref_type__resource9 "REFSEQ__NM_001371415"
      minerva_type "Protein"
      minerva_x 1174.5
      minerva_y 873.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa398"
      species_meta_id "s_id_sa398"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 102
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 400.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "c2b__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa368"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa368"
      minerva_name "C2b"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C2"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/717"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/gene/717"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P06681"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P06681"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000166278"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.21.43"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_000063"
      minerva_ref_link8 "http://id.nlm.nih.gov/mesh/D050679"
      minerva_ref_link9 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1248"
      minerva_ref_type__resource1 "HGNC_SYMBOL__C2"
      minerva_ref_type__resource10 "ENTREZ__717"
      minerva_ref_type__resource11 "ENTREZ__717"
      minerva_ref_type__resource2 "UNIPROT__P06681"
      minerva_ref_type__resource3 "UNIPROT__P06681"
      minerva_ref_type__resource4 "TAXONOMY__9606"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000166278"
      minerva_ref_type__resource6 "EC__3.4.21.43"
      minerva_ref_type__resource7 "REFSEQ__NM_000063"
      minerva_ref_type__resource8 "MESH_2012__D050679"
      minerva_ref_type__resource9 "HGNC__1248"
      minerva_type "Protein"
      minerva_x 1678.0
      minerva_y 1172.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa368"
      species_meta_id "s_id_sa368"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 103
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 500.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "vwf__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa472"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa472"
      minerva_former_symbols "F8VWF"
      minerva_name "VWF"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12726"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000110799"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_000552"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P04275"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P04275"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/7450"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/7450"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=VWF"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=VWF"
      minerva_ref_type__resource1 "HGNC__12726"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000110799"
      minerva_ref_type__resource2 "TAXONOMY__9606"
      minerva_ref_type__resource3 "REFSEQ__NM_000552"
      minerva_ref_type__resource4 "UNIPROT__P04275"
      minerva_ref_type__resource5 "UNIPROT__P04275"
      minerva_ref_type__resource6 "ENTREZ__7450"
      minerva_ref_type__resource7 "ENTREZ__7450"
      minerva_ref_type__resource8 "HGNC_SYMBOL__VWF"
      minerva_ref_type__resource9 "HGNC_SYMBOL__VWF"
      minerva_type "Protein"
      minerva_x 1588.0
      minerva_y 2207.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa472"
      species_meta_id "s_id_sa472"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 104
    zlevel -1

    cluster [
      cluster "s_id_ca16"
    ]
    graphics [
      x 600.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "masp2_space_deposition__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa417"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa417"
      minerva_name "MASP2 deposition"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/32299776"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_type__resource1 "PUBMED__32299776"
      minerva_ref_type__resource2 "TAXONOMY__9606"
      minerva_type "Phenotype"
      minerva_x 1713.0
      minerva_y 554.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca16"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa417"
      species_meta_id "s_id_sa417"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 105
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 700.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "d_minus_dimer__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa210"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa210"
      minerva_name "D-dimer"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link2 "http://id.nlm.nih.gov/mesh/C036309"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/pubmed/19008457"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource2 "MESH_2012__C036309"
      minerva_ref_type__resource3 "PUBMED__19008457"
      minerva_type "Protein"
      minerva_x 1331.0
      minerva_y 2113.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa210"
      species_meta_id "s_id_sa210"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 106
    zlevel -1

    cluster [
      cluster "s_id_ca14"
    ]
    graphics [
      x 800.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "gp6:alpha2beta1:vwf__platelet__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa40"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa40"
      minerva_name "GP6:alpha2beta1:VWF"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6137"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12726"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/pubmed/25051961"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=10090"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/14338"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6153"
      minerva_ref_type__resource1 "HGNC__6137"
      minerva_ref_type__resource2 "HGNC__12726"
      minerva_ref_type__resource3 "PUBMED__25051961"
      minerva_ref_type__resource4 "TAXONOMY__10090"
      minerva_ref_type__resource5 "HGNC__14338"
      minerva_ref_type__resource6 "HGNC__6153"
      minerva_type "Complex"
      minerva_x 1873.0
      minerva_y 2579.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca14"
      compartment_name "platelet"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa40"
      species_meta_id "s_id_csa40"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 107
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 900.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tat_space_complex__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa14"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa14"
      minerva_name "TAT complex"
      minerva_ref_link1 "http://id.nlm.nih.gov/mesh/D013917"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/775"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link4 "http://id.nlm.nih.gov/mesh/C046193"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/pubmed/22930518"
      minerva_ref_type__resource1 "MESH_2012__D013917"
      minerva_ref_type__resource2 "HGNC__775"
      minerva_ref_type__resource3 "TAXONOMY__9606"
      minerva_ref_type__resource4 "MESH_2012__C046193"
      minerva_ref_type__resource5 "PUBMED__22930518"
      minerva_type "Complex"
      minerva_x 392.0
      minerva_y 2158.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa14"
      species_meta_id "s_id_csa14"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 108
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 1000.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tafi__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa226"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa226"
      minerva_former_symbols "CBP1; CBP2; SERPINH2"
      minerva_name "TAFI"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "http://purl.uniprot.org/uniprot/P50454"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000149257"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_004353"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SERPINH1"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SERPINH1"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1546"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/871"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/871"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P50454"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "UNIPROT__P50454"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000149257"
      minerva_ref_type__resource3 "REFSEQ__NM_004353"
      minerva_ref_type__resource4 "HGNC_SYMBOL__SERPINH1"
      minerva_ref_type__resource5 "HGNC_SYMBOL__SERPINH1"
      minerva_ref_type__resource6 "HGNC__1546"
      minerva_ref_type__resource7 "ENTREZ__871"
      minerva_ref_type__resource8 "ENTREZ__871"
      minerva_ref_type__resource9 "UNIPROT__P50454"
      minerva_type "Protein"
      minerva_x 724.0
      minerva_y 2186.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa226"
      species_meta_id "s_id_sa226"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 109
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 1100.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "f10a__blood__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa176"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa176"
      minerva_name "F10a"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.21.6"
      minerva_ref_link10 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/3528"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/gene/2159"
      minerva_ref_link12 "https://www.ncbi.nlm.nih.gov/gene/2159"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P00742"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P00742"
      minerva_ref_link5 "http://id.nlm.nih.gov/mesh/D015951"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000504"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000126218"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=F10"
      minerva_ref_link9 "https://www.brenda-enzymes.org/php/result_flat.php4?ecno=3.4.21.6"
      minerva_ref_type__resource1 "EC__3.4.21.6"
      minerva_ref_type__resource10 "HGNC__3528"
      minerva_ref_type__resource11 "ENTREZ__2159"
      minerva_ref_type__resource12 "ENTREZ__2159"
      minerva_ref_type__resource2 "TAXONOMY__9606"
      minerva_ref_type__resource3 "UNIPROT__P00742"
      minerva_ref_type__resource4 "UNIPROT__P00742"
      minerva_ref_type__resource5 "MESH_2012__D015951"
      minerva_ref_type__resource6 "REFSEQ__NM_000504"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000126218"
      minerva_ref_type__resource8 "HGNC_SYMBOL__F10"
      minerva_ref_type__resource9 "BRENDA__3.4.21.6"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 483.0
      minerva_y 1747.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa176"
      species_meta_id "s_id_sa176"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 110
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 1200.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mbl2__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa362"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa362"
      minerva_former_symbols "MASP1P1"
      minerva_name "MBL2"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/O00187"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MASP2"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/gene/10747"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/O00187"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_006610"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000009724"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.21.104"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6902"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MASP2"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/10747"
      minerva_ref_type__resource1 "UNIPROT__O00187"
      minerva_ref_type__resource10 "HGNC_SYMBOL__MASP2"
      minerva_ref_type__resource11 "ENTREZ__10747"
      minerva_ref_type__resource2 "UNIPROT__O00187"
      minerva_ref_type__resource3 "TAXONOMY__9606"
      minerva_ref_type__resource4 "REFSEQ__NM_006610"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000009724"
      minerva_ref_type__resource6 "EC__3.4.21.104"
      minerva_ref_type__resource7 "HGNC__6902"
      minerva_ref_type__resource8 "HGNC_SYMBOL__MASP2"
      minerva_ref_type__resource9 "ENTREZ__10747"
      minerva_type "Protein"
      minerva_x 1858.0
      minerva_y 907.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa362"
      species_meta_id "s_id_sa362"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 111
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 200.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s538__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa461"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa461"
      minerva_name "s538"
      minerva_ref_link1 "http://id.nlm.nih.gov/mesh/D062106"
      minerva_ref_type__resource1 "MESH_2012__D062106"
      minerva_type "Degraded"
      minerva_x 1793.0
      minerva_y 1444.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa461"
      species_meta_id "s_id_sa461"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 112
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 300.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tafi__blood__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa227"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa227"
      minerva_former_symbols "CBP1; CBP2; SERPINH2"
      minerva_name "TAFI"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "http://purl.uniprot.org/uniprot/P50454"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000149257"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_004353"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SERPINH1"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SERPINH1"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1546"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/871"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/871"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P50454"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "UNIPROT__P50454"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000149257"
      minerva_ref_type__resource3 "REFSEQ__NM_004353"
      minerva_ref_type__resource4 "HGNC_SYMBOL__SERPINH1"
      minerva_ref_type__resource5 "HGNC_SYMBOL__SERPINH1"
      minerva_ref_type__resource6 "HGNC__1546"
      minerva_ref_type__resource7 "ENTREZ__871"
      minerva_ref_type__resource8 "ENTREZ__871"
      minerva_ref_type__resource9 "UNIPROT__P50454"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 723.0
      minerva_y 2329.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa227"
      species_meta_id "s_id_sa227"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 113
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 400.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "plat:serpine1__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa16"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa16"
      minerva_name "PLAT:SERPINE1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/9051"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/pubmed/22449964"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/8593"
      minerva_ref_link5 "https://www.ebi.ac.uk/intact/interaction/EBI-7800882"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource2 "HGNC__9051"
      minerva_ref_type__resource3 "PUBMED__22449964"
      minerva_ref_type__resource4 "HGNC__8593"
      minerva_ref_type__resource5 "INTACT__EBI-7800882"
      minerva_type "Complex"
      minerva_x 1376.5
      minerva_y 2468.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa16"
      species_meta_id "s_id_csa16"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 114
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 500.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "antithrombin__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa202"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa202"
      minerva_former_symbols "AT3"
      minerva_name "Antithrombin"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P01008"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SERPINC1"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P01008"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/775"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000117601"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/462"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/462"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_000488"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SERPINC1"
      minerva_ref_type__resource1 "UNIPROT__P01008"
      minerva_ref_type__resource10 "HGNC_SYMBOL__SERPINC1"
      minerva_ref_type__resource2 "UNIPROT__P01008"
      minerva_ref_type__resource3 "HGNC__775"
      minerva_ref_type__resource4 "TAXONOMY__9606"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000117601"
      minerva_ref_type__resource6 "ENTREZ__462"
      minerva_ref_type__resource7 "ENTREZ__462"
      minerva_ref_type__resource8 "REFSEQ__NM_000488"
      minerva_ref_type__resource9 "HGNC_SYMBOL__SERPINC1"
      minerva_type "Protein"
      minerva_x 664.0
      minerva_y 1954.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa202"
      species_meta_id "s_id_sa202"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 115
    zlevel -1

    cluster [
      cluster "s_id_ca16"
    ]
    graphics [
      x 600.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "thrombosis__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa529"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa529"
      minerva_name "Thrombosis"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link2 "http://id.nlm.nih.gov/mesh/D013923"
      minerva_ref_link3 "http://id.nlm.nih.gov/mesh/D055806"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource2 "MESH_2012__D013923"
      minerva_ref_type__resource3 "MESH_2012__D055806"
      minerva_type "Phenotype"
      minerva_x 464.5
      minerva_y 481.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca16"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa529"
      species_meta_id "s_id_sa529"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 116
    zlevel -1

    cluster [
      cluster "s_id_ca13"
    ]
    graphics [
      x 700.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "vascular_space_remodeling__endothelium,_space_vascular__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa518"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa518"
      minerva_fullName "blood vessel remodeling"
      minerva_name "vascular remodeling"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0001974"
      minerva_ref_type__resource1 "GO__GO:0001974"
      minerva_type "Phenotype"
      minerva_x 314.0
      minerva_y 704.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca13"
      compartment_name "endothelium,_space_vascular"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa518"
      species_meta_id "s_id_sa518"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 117
    zlevel -1

    cluster [
      cluster "s_id_ca13"
    ]
    graphics [
      x 800.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "agtr1__endothelium,_space_vascular__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa519"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa519"
      minerva_former_symbols "AGTR1B"
      minerva_name "AGTR1"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 710.5
      minerva_y 782.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca13"
      compartment_name "endothelium,_space_vascular"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa519"
      species_meta_id "s_id_sa519"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 118
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 900.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "c5a__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa253"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa253"
      minerva_name "C5a"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/727"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1331"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C5"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_001735"
      minerva_ref_link5 "http://id.nlm.nih.gov/mesh/D015936"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000106804"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P01031"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P01031"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/727"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "ENTREZ__727"
      minerva_ref_type__resource2 "HGNC__1331"
      minerva_ref_type__resource3 "HGNC_SYMBOL__C5"
      minerva_ref_type__resource4 "REFSEQ__NM_001735"
      minerva_ref_type__resource5 "MESH_2012__D015936"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000106804"
      minerva_ref_type__resource7 "UNIPROT__P01031"
      minerva_ref_type__resource8 "UNIPROT__P01031"
      minerva_ref_type__resource9 "ENTREZ__727"
      minerva_type "Protein"
      minerva_x 1986.0
      minerva_y 1444.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa253"
      species_meta_id "s_id_sa253"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 119
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 1000.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "klkb1__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa167"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa167"
      minerva_former_symbols "KLK3"
      minerva_name "KLKB1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_000892"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=KLKB1"
      minerva_ref_link11 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=KLKB1"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000164344"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.21.34"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P03952"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P03952"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6371"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/3818"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/3818"
      minerva_ref_type__resource1 "REFSEQ__NM_000892"
      minerva_ref_type__resource10 "HGNC_SYMBOL__KLKB1"
      minerva_ref_type__resource11 "HGNC_SYMBOL__KLKB1"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000164344"
      minerva_ref_type__resource3 "TAXONOMY__9606"
      minerva_ref_type__resource4 "EC__3.4.21.34"
      minerva_ref_type__resource5 "UNIPROT__P03952"
      minerva_ref_type__resource6 "UNIPROT__P03952"
      minerva_ref_type__resource7 "HGNC__6371"
      minerva_ref_type__resource8 "ENTREZ__3818"
      minerva_ref_type__resource9 "ENTREZ__3818"
      minerva_type "Protein"
      minerva_x 349.0
      minerva_y 1105.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa167"
      species_meta_id "s_id_sa167"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 120
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 1100.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "c2__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa366"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa366"
      minerva_name "C2"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C2"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/717"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/gene/717"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P06681"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P06681"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C2"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000166278"
      minerva_ref_link7 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.21.43"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_000063"
      minerva_ref_link9 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1248"
      minerva_ref_type__resource1 "HGNC_SYMBOL__C2"
      minerva_ref_type__resource10 "ENTREZ__717"
      minerva_ref_type__resource11 "ENTREZ__717"
      minerva_ref_type__resource2 "UNIPROT__P06681"
      minerva_ref_type__resource3 "UNIPROT__P06681"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C2"
      minerva_ref_type__resource5 "TAXONOMY__9606"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000166278"
      minerva_ref_type__resource7 "EC__3.4.21.43"
      minerva_ref_type__resource8 "REFSEQ__NM_000063"
      minerva_ref_type__resource9 "HGNC__1248"
      minerva_type "Protein"
      minerva_x 1818.0
      minerva_y 1097.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa366"
      species_meta_id "s_id_sa366"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 121
    zlevel -1

    cluster [
      cluster "s_id_ca16"
    ]
    graphics [
      x 1200.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "platelet_space_aggregation__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa409"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa409"
      minerva_fullName "platelet activation"
      minerva_name "platelet aggregation"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link2 "http://amigo.geneontology.org/amigo/term/GO:0030168"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource2 "GO__GO:0030168"
      minerva_type "Phenotype"
      minerva_x 2913.0
      minerva_y 435.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca16"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa409"
      species_meta_id "s_id_sa409"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 122
    zlevel -1

    cluster [
      cluster "s_id_ca13"
    ]
    graphics [
      x 200.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "c4d__endothelium,_space_vascular__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa387"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa387"
      minerva_name "C4d"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_001002029"
      minerva_ref_link10 "http://purl.uniprot.org/uniprot/P0C0L5"
      minerva_ref_link11 "http://purl.uniprot.org/uniprot/P0C0L5"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000224389"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/100293534"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4b"
      minerva_ref_link6 "http://id.nlm.nih.gov/mesh/C032261"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/721"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4B"
      minerva_ref_link9 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1324"
      minerva_ref_type__resource1 "REFSEQ__NM_001002029"
      minerva_ref_type__resource10 "UNIPROT__P0C0L5"
      minerva_ref_type__resource11 "UNIPROT__P0C0L5"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000224389"
      minerva_ref_type__resource3 "TAXONOMY__9606"
      minerva_ref_type__resource4 "ENTREZ__100293534"
      minerva_ref_type__resource5 "HGNC_SYMBOL__C4b"
      minerva_ref_type__resource6 "MESH_2012__C032261"
      minerva_ref_type__resource7 "ENTREZ__721"
      minerva_ref_type__resource8 "HGNC_SYMBOL__C4B"
      minerva_ref_type__resource9 "HGNC__1324"
      minerva_type "Protein"
      minerva_x 1492.0
      minerva_y 787.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca13"
      compartment_name "endothelium,_space_vascular"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa387"
      species_meta_id "s_id_sa387"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 123
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 300.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "f9__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa174"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa174"
      minerva_name "F9"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=RPS3AP29"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NG_011230"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000237818"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/35531"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/730861"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource2 "HGNC_SYMBOL__RPS3AP29"
      minerva_ref_type__resource3 "REFSEQ__NG_011230"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000237818"
      minerva_ref_type__resource5 "HGNC__35531"
      minerva_ref_type__resource6 "ENTREZ__730861"
      minerva_type "Protein"
      minerva_x 334.0
      minerva_y 1349.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa174"
      species_meta_id "s_id_sa174"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 124
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 400.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s589__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa497"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa497"
      minerva_name "s589"
      minerva_type "Degraded"
      minerva_x 1926.0
      minerva_y 1952.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa497"
      species_meta_id "s_id_sa497"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 125
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 500.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ace__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa198"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa198"
      minerva_former_symbols "DCP1"
      minerva_name "ACE"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/2707"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ACE"
      minerva_ref_link11 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.2.1.-"
      minerva_ref_link12 "https://www.ensembl.org/id/ENSG00000159640"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P12821"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P12821"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.15.1"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/1636"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/1636"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ACE"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_000789"
      minerva_ref_type__resource1 "HGNC__2707"
      minerva_ref_type__resource10 "HGNC_SYMBOL__ACE"
      minerva_ref_type__resource11 "EC__3.2.1.-"
      minerva_ref_type__resource12 "ENSEMBL__ENSG00000159640"
      minerva_ref_type__resource2 "UNIPROT__P12821"
      minerva_ref_type__resource3 "UNIPROT__P12821"
      minerva_ref_type__resource4 "EC__3.4.15.1"
      minerva_ref_type__resource5 "TAXONOMY__9606"
      minerva_ref_type__resource6 "ENTREZ__1636"
      minerva_ref_type__resource7 "ENTREZ__1636"
      minerva_ref_type__resource8 "HGNC_SYMBOL__ACE"
      minerva_ref_type__resource9 "REFSEQ__NM_000789"
      minerva_type "Protein"
      minerva_x 691.0
      minerva_y 848.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa198"
      species_meta_id "s_id_sa198"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 126
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 600.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "f8:f9__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa12"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa12"
      minerva_name "F8:F9"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/22471307"
      minerva_ref_link2 "https://www.ebi.ac.uk/intact/interaction/EBI-11621595"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/3546"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/35531"
      minerva_ref_type__resource1 "PUBMED__22471307"
      minerva_ref_type__resource2 "INTACT__EBI-11621595"
      minerva_ref_type__resource3 "TAXONOMY__9606"
      minerva_ref_type__resource4 "HGNC__3546"
      minerva_ref_type__resource5 "HGNC__35531"
      minerva_type "Complex"
      minerva_x 421.0
      minerva_y 1571.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa12"
      species_meta_id "s_id_csa12"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 127
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 700.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "c2a__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa367"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa367"
      minerva_name "C2a"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C2"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/717"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/gene/717"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P06681"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P06681"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000166278"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.21.43"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_000063"
      minerva_ref_link8 "http://id.nlm.nih.gov/mesh/D050678"
      minerva_ref_link9 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1248"
      minerva_ref_type__resource1 "HGNC_SYMBOL__C2"
      minerva_ref_type__resource10 "ENTREZ__717"
      minerva_ref_type__resource11 "ENTREZ__717"
      minerva_ref_type__resource2 "UNIPROT__P06681"
      minerva_ref_type__resource3 "UNIPROT__P06681"
      minerva_ref_type__resource4 "TAXONOMY__9606"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000166278"
      minerva_ref_type__resource6 "EC__3.4.21.43"
      minerva_ref_type__resource7 "REFSEQ__NM_000063"
      minerva_ref_type__resource8 "MESH_2012__D050678"
      minerva_ref_type__resource9 "HGNC__1248"
      minerva_type "Protein"
      minerva_x 1638.0
      minerva_y 1117.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa367"
      species_meta_id "s_id_sa367"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 128
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 800.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "kng1__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa169"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa169"
      minerva_former_symbols "BDK; KNG"
      minerva_name "KNG1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6383"
      minerva_ref_link11 "https://www.ensembl.org/id/ENSG00000113889"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=KNG1"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=KNG1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=6383"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/3827"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/3827"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P01042"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P01042"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_001102416"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "HGNC__6383"
      minerva_ref_type__resource11 "ENSEMBL__ENSG00000113889"
      minerva_ref_type__resource2 "HGNC_SYMBOL__KNG1"
      minerva_ref_type__resource3 "HGNC_SYMBOL__KNG1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__6383"
      minerva_ref_type__resource5 "ENTREZ__3827"
      minerva_ref_type__resource6 "ENTREZ__3827"
      minerva_ref_type__resource7 "UNIPROT__P01042"
      minerva_ref_type__resource8 "UNIPROT__P01042"
      minerva_ref_type__resource9 "REFSEQ__NM_001102416"
      minerva_type "Protein"
      minerva_x 211.0
      minerva_y 1066.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa169"
      species_meta_id "s_id_sa169"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 130
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 1000.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "c3b__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa310"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa310"
      minerva_name "C3b"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C3"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/718"
      minerva_ref_link2 "http://id.nlm.nih.gov/mesh/D003179"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000125730"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_000064"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P01024"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P01024"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1318"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/718"
      minerva_ref_type__resource1 "HGNC_SYMBOL__C3"
      minerva_ref_type__resource10 "ENTREZ__718"
      minerva_ref_type__resource2 "MESH_2012__D003179"
      minerva_ref_type__resource3 "TAXONOMY__9606"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000125730"
      minerva_ref_type__resource5 "REFSEQ__NM_000064"
      minerva_ref_type__resource6 "UNIPROT__P01024"
      minerva_ref_type__resource7 "UNIPROT__P01024"
      minerva_ref_type__resource8 "HGNC__1318"
      minerva_ref_type__resource9 "ENTREZ__718"
      minerva_type "Protein"
      minerva_x 1991.0
      minerva_y 1145.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa310"
      species_meta_id "s_id_sa310"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 131
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 1100.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "angiotensin_space_i__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa195"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa195"
      minerva_fullName "angiotensin I"
      minerva_name "angiotensin I"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:2718"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource2 "CHEBI__CHEBI:2718"
      minerva_synonyms "Asp-Arg-Val-Tyr-Ile-His-Pro-Phe-His-Leu; D-R-V-Y-I-H-P-F-H-L; DRVYIHPFHL; L-Asp-L-Arg-L-Val-L-Tyr-L-Ile-L-His-L-Pro-L-Phe-L-His-L-Leu; angiotensin I; pepsitensin; proangiotensin"
      minerva_type "Simple molecule"
      minerva_x 1061.25
      minerva_y 996.75
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa195"
      species_meta_id "s_id_sa195"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 132
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 1200.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "il1b__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa244"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa244"
      minerva_name "IL1B"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000125538"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/5992"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL1B"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL1B"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P01584"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P01584"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_000576"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/3553"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/3553"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000125538"
      minerva_ref_type__resource2 "HGNC__5992"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL1B"
      minerva_ref_type__resource4 "HGNC_SYMBOL__IL1B"
      minerva_ref_type__resource5 "UNIPROT__P01584"
      minerva_ref_type__resource6 "UNIPROT__P01584"
      minerva_ref_type__resource7 "REFSEQ__NM_000576"
      minerva_ref_type__resource8 "ENTREZ__3553"
      minerva_ref_type__resource9 "ENTREZ__3553"
      minerva_type "Protein"
      minerva_x 1580.0
      minerva_y 1685.7083333333335
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa244"
      species_meta_id "s_id_sa244"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 133
    zlevel -1

    cluster [
      cluster "s_id_ca14"
    ]
    graphics [
      x 200.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "platelet_space_activation__platelet__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa430"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa430"
      minerva_fullName "platelet activation"
      minerva_name "platelet activation"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link2 "http://amigo.geneontology.org/amigo/term/GO:0030168"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource2 "GO__GO:0030168"
      minerva_type "Phenotype"
      minerva_x 2173.0
      minerva_y 2579.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca14"
      compartment_name "platelet"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa430"
      species_meta_id "s_id_sa430"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 134
    zlevel -1

    cluster [
      cluster "s_id_ca13"
    ]
    graphics [
      x 300.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "plau__endothelium,_space_vascular__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa356"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa356"
      minerva_name "PLAU"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "http://purl.uniprot.org/uniprot/P00749"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/protein/NM_002658"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/5328"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/5328"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PLAU"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PLAU"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.21.73"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000122861"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/9052"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P00749"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "UNIPROT__P00749"
      minerva_ref_type__resource11 "REFSEQ__NM_002658"
      minerva_ref_type__resource2 "ENTREZ__5328"
      minerva_ref_type__resource3 "ENTREZ__5328"
      minerva_ref_type__resource4 "HGNC_SYMBOL__PLAU"
      minerva_ref_type__resource5 "HGNC_SYMBOL__PLAU"
      minerva_ref_type__resource6 "EC__3.4.21.73"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000122861"
      minerva_ref_type__resource8 "HGNC__9052"
      minerva_ref_type__resource9 "UNIPROT__P00749"
      minerva_type "Protein"
      minerva_x 1232.0
      minerva_y 2884.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca13"
      compartment_name "endothelium,_space_vascular"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa356"
      species_meta_id "s_id_sa356"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 135
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 400.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "c3a__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa308"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa308"
      minerva_name "C3a"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C3"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/718"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000125730"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_000064"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P01024"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P01024"
      minerva_ref_link7 "http://id.nlm.nih.gov/mesh/D015926"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1318"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/718"
      minerva_ref_type__resource1 "HGNC_SYMBOL__C3"
      minerva_ref_type__resource10 "ENTREZ__718"
      minerva_ref_type__resource2 "TAXONOMY__9606"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000125730"
      minerva_ref_type__resource4 "REFSEQ__NM_000064"
      minerva_ref_type__resource5 "UNIPROT__P01024"
      minerva_ref_type__resource6 "UNIPROT__P01024"
      minerva_ref_type__resource7 "MESH_2012__D015926"
      minerva_ref_type__resource8 "HGNC__1318"
      minerva_ref_type__resource9 "ENTREZ__718"
      minerva_type "Protein"
      minerva_x 1991.0
      minerva_y 1213.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa308"
      species_meta_id "s_id_sa308"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 136
    zlevel -1

    cluster [
      cluster "s_id_ca16"
    ]
    graphics [
      x 500.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "c5b_minus_9_space_deposition__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa390"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa390"
      minerva_name "C5b-9 deposition"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/32299776"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link3 "http://id.nlm.nih.gov/mesh/D018366"
      minerva_ref_type__resource1 "PUBMED__32299776"
      minerva_ref_type__resource2 "TAXONOMY__9606"
      minerva_ref_type__resource3 "MESH_2012__D018366"
      minerva_type "Phenotype"
      minerva_x 3003.0
      minerva_y 520.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca16"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa390"
      species_meta_id "s_id_sa390"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 137
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 600.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "thrombin__blood__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa181"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa181"
      minerva_name "Thrombin"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P00734"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/2147"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/gene/2147"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P00734"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.21.5"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/3535"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000506"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000180210"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=F2"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=F2"
      minerva_ref_type__resource1 "UNIPROT__P00734"
      minerva_ref_type__resource10 "ENTREZ__2147"
      minerva_ref_type__resource11 "ENTREZ__2147"
      minerva_ref_type__resource2 "UNIPROT__P00734"
      minerva_ref_type__resource3 "TAXONOMY__9606"
      minerva_ref_type__resource4 "EC__3.4.21.5"
      minerva_ref_type__resource5 "HGNC__3535"
      minerva_ref_type__resource6 "REFSEQ__NM_000506"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000180210"
      minerva_ref_type__resource8 "HGNC_SYMBOL__F2"
      minerva_ref_type__resource9 "HGNC_SYMBOL__F2"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 784.0
      minerva_y 2042.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa181"
      species_meta_id "s_id_sa181"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 138
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 700.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "crp__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa410"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa410"
      minerva_name "CRP"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/1401"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_000567"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/1401"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/2367"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CRP"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P02741"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P02741"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CRP"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000132693"
      minerva_ref_type__resource1 "ENTREZ__1401"
      minerva_ref_type__resource10 "REFSEQ__NM_000567"
      minerva_ref_type__resource2 "ENTREZ__1401"
      minerva_ref_type__resource3 "TAXONOMY__9606"
      minerva_ref_type__resource4 "HGNC__2367"
      minerva_ref_type__resource5 "HGNC_SYMBOL__CRP"
      minerva_ref_type__resource6 "UNIPROT__P02741"
      minerva_ref_type__resource7 "UNIPROT__P02741"
      minerva_ref_type__resource8 "HGNC_SYMBOL__CRP"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000132693"
      minerva_type "Protein"
      minerva_x 1371.0
      minerva_y 1636.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa410"
      species_meta_id "s_id_sa410"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 139
    zlevel -1

    cluster [
      cluster "s_id_ca14"
    ]
    graphics [
      x 800.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "gp6:alpha2:beta1__platelet__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa39"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa39"
      minerva_name "GP6:alpha2:beta1"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6137"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/pubmed/25051961"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=10090"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/14338"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6153"
      minerva_ref_type__resource1 "HGNC__6137"
      minerva_ref_type__resource2 "PUBMED__25051961"
      minerva_ref_type__resource3 "TAXONOMY__10090"
      minerva_ref_type__resource4 "HGNC__14338"
      minerva_ref_type__resource5 "HGNC__6153"
      minerva_type "Complex"
      minerva_x 1913.0
      minerva_y 2382.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca14"
      compartment_name "platelet"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa39"
      species_meta_id "s_id_csa39"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 140
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 900.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s546__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa467"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa467"
      minerva_name "s546"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/27077125"
      minerva_ref_link2 "http://id.nlm.nih.gov/mesh/D062106"
      minerva_ref_type__resource1 "PUBMED__27077125"
      minerva_ref_type__resource2 "MESH_2012__D062106"
      minerva_type "Degraded"
      minerva_x 2146.0
      minerva_y 1443.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa467"
      species_meta_id "s_id_sa467"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 141
    zlevel -1

    cluster [
      cluster "s_id_ca13"
    ]
    graphics [
      x 1000.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "thrombomodulin__endothelium,_space_vascular__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa301"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa301"
      minerva_name "Thrombomodulin"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=THBD"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/7056"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7056"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_000361"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P07204"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P07204"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11784"
      minerva_ref_link8 "https://www.ensembl.org/id/ENSG00000178726"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=THBD"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "HGNC_SYMBOL__THBD"
      minerva_ref_type__resource2 "ENTREZ__7056"
      minerva_ref_type__resource3 "ENTREZ__7056"
      minerva_ref_type__resource4 "REFSEQ__NM_000361"
      minerva_ref_type__resource5 "UNIPROT__P07204"
      minerva_ref_type__resource6 "UNIPROT__P07204"
      minerva_ref_type__resource7 "HGNC__11784"
      minerva_ref_type__resource8 "ENSEMBL__ENSG00000178726"
      minerva_ref_type__resource9 "HGNC_SYMBOL__THBD"
      minerva_type "Protein"
      minerva_x 1007.0
      minerva_y 2883.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca13"
      compartment_name "endothelium,_space_vascular"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa301"
      species_meta_id "s_id_sa301"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 142
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 1100.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cfi__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa385"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa385"
      minerva_former_symbols "DF; PFD"
      minerva_name "CFI"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.21.46"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/1675"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/gene/1675"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/2771"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CFD"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CFD"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_001928"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000197766"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P00746"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P00746"
      minerva_ref_type__resource1 "EC__3.4.21.46"
      minerva_ref_type__resource10 "ENTREZ__1675"
      minerva_ref_type__resource11 "ENTREZ__1675"
      minerva_ref_type__resource2 "TAXONOMY__9606"
      minerva_ref_type__resource3 "HGNC__2771"
      minerva_ref_type__resource4 "HGNC_SYMBOL__CFD"
      minerva_ref_type__resource5 "HGNC_SYMBOL__CFD"
      minerva_ref_type__resource6 "REFSEQ__NM_001928"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000197766"
      minerva_ref_type__resource8 "UNIPROT__P00746"
      minerva_ref_type__resource9 "UNIPROT__P00746"
      minerva_type "Protein"
      minerva_x 1447.0
      minerva_y 1039.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa385"
      species_meta_id "s_id_sa385"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 143
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 1200.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s586__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa495"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa495"
      minerva_name "s586"
      minerva_type "Degraded"
      minerva_x 848.0
      minerva_y 2615.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa495"
      species_meta_id "s_id_sa495"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 144
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 200.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "vwf__blood__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa411"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa411"
      minerva_former_symbols "F8VWF"
      minerva_name "VWF"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12726"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000110799"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_000552"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P04275"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P04275"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/7450"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/7450"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=VWF"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=VWF"
      minerva_ref_type__resource1 "HGNC__12726"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000110799"
      minerva_ref_type__resource2 "TAXONOMY__9606"
      minerva_ref_type__resource3 "REFSEQ__NM_000552"
      minerva_ref_type__resource4 "UNIPROT__P04275"
      minerva_ref_type__resource5 "UNIPROT__P04275"
      minerva_ref_type__resource6 "ENTREZ__7450"
      minerva_ref_type__resource7 "ENTREZ__7450"
      minerva_ref_type__resource8 "HGNC_SYMBOL__VWF"
      minerva_ref_type__resource9 "HGNC_SYMBOL__VWF"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 1313.0
      minerva_y 2206.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa411"
      species_meta_id "s_id_sa411"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 145
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 300.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "f12a__blood__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa165"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa165"
      minerva_name "F12a"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "http://purl.uniprot.org/uniprot/P00748"
      minerva_ref_link11 "http://purl.uniprot.org/uniprot/P00748"
      minerva_ref_link12 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.21.38"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/3530"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000131187"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/2161"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/2161"
      minerva_ref_link6 "http://id.nlm.nih.gov/mesh/D015956"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_000505"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=F12"
      minerva_ref_link9 "https://www.brenda-enzymes.org/php/result_flat.php4?ecno=3.4.21.38"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "UNIPROT__P00748"
      minerva_ref_type__resource11 "UNIPROT__P00748"
      minerva_ref_type__resource12 "EC__3.4.21.38"
      minerva_ref_type__resource2 "HGNC__3530"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000131187"
      minerva_ref_type__resource4 "ENTREZ__2161"
      minerva_ref_type__resource5 "ENTREZ__2161"
      minerva_ref_type__resource6 "MESH_2012__D015956"
      minerva_ref_type__resource7 "REFSEQ__NM_000505"
      minerva_ref_type__resource8 "HGNC_SYMBOL__F12"
      minerva_ref_type__resource9 "BRENDA__3.4.21.38"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 518.0
      minerva_y 1124.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa165"
      species_meta_id "s_id_sa165"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 146
    zlevel -1

    cluster [
      cluster "s_id_ca15"
    ]
    graphics [
      x 400.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "c7__blood__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa321"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa321"
      minerva_name "C7"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P10643"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_000587"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P10643"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000112936"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C7"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/730"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C7"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/730"
      minerva_ref_link9 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1346"
      minerva_ref_type__resource1 "UNIPROT__P10643"
      minerva_ref_type__resource10 "REFSEQ__NM_000587"
      minerva_ref_type__resource2 "UNIPROT__P10643"
      minerva_ref_type__resource3 "TAXONOMY__9606"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000112936"
      minerva_ref_type__resource5 "HGNC_SYMBOL__C7"
      minerva_ref_type__resource6 "ENTREZ__730"
      minerva_ref_type__resource7 "HGNC_SYMBOL__C7"
      minerva_ref_type__resource8 "ENTREZ__730"
      minerva_ref_type__resource9 "HGNC__1346"
      minerva_type "Protein"
      minerva_x 2311.0
      minerva_y 1301.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca15"
      compartment_name "blood"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa321"
      species_meta_id "s_id_sa321"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 147
    zlevel -1

    graphics [
      x 500.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:20483636"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re260"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Trigger"
      minerva_x 1366.8958512297747
      minerva_y 1706.6177885960835
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re260"
      reaction_meta_id "re260"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 150
    zlevel -1

    graphics [
      x 600.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:21199867"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re268"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 1077.5
      minerva_y 2739.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re268"
      reaction_meta_id "re268"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 154
    zlevel -1

    graphics [
      x 700.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:692685"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re330"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 842.0
      minerva_y 524.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re330"
      reaction_meta_id "re330"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 158
    zlevel -1

    graphics [
      x 800.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:32286245"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re333"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 1675.5000313180126
      minerva_y 1743.145018498044
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re333"
      reaction_meta_id "re333"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 163
    zlevel -1

    graphics [
      x 900.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:19362461"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re288"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 1494.8235294117646
      minerva_y 967.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re288"
      reaction_meta_id "re288"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 167
    zlevel -1

    graphics [
      x 1000.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re398"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re398"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 842.2192662705397
      minerva_y 1268.804481869955
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re398"
      reaction_meta_id "re398"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 171
    zlevel -1

    graphics [
      x 1100.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:27077125"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re343"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 2082.0000781227113
      minerva_y 1443.399999511732
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re343"
      reaction_meta_id "re343"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 175
    zlevel -1

    graphics [
      x 1200.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:427127, PMID:17395591"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re261"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Truncation"
      minerva_x 1869.0000000000005
      minerva_y 1170.1818181818178
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re261"
      reaction_meta_id "re261"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 181
    zlevel -1

    graphics [
      x 200.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:9066005"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re372"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 791.0463393846618
      minerva_y 2519.8502640267798
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re372"
      reaction_meta_id "re372"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 185
    zlevel -1

    graphics [
      x 300.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:32302438"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re296"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 664.0
      minerva_y 1899.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re296"
      reaction_meta_id "re296"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 189
    zlevel -1

    graphics [
      x 400.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:23392115, PMID:32336612"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re307"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 1195.8752470525349
      minerva_y 1244.665784250246
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re307"
      reaction_meta_id "re307"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 193
    zlevel -1

    graphics [
      x 500.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re377"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re377"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 1255.0
      minerva_y 2357.8
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re377"
      reaction_meta_id "re377"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 201
    zlevel -1

    graphics [
      x 600.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:32172226"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re192"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 1927.0999687646408
      minerva_y 1995.9987505856325
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re192"
      reaction_meta_id "re192"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 205
    zlevel -1

    graphics [
      x 700.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re353"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re353"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Trigger"
      minerva_x 2023.78125
      minerva_y 2579.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re353"
      reaction_meta_id "re353"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 208
    zlevel -1

    graphics [
      x 800.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:8388351"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re256"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Heterodimer association"
      minerva_x 940.9999999999991
      minerva_y 2159.999999999998
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re256"
      reaction_meta_id "re256"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 212
    zlevel -1

    graphics [
      x 900.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re355"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re355"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Trigger"
      minerva_x 2295.54943292986
      minerva_y 2578.6464920203953
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re355"
      reaction_meta_id "re355"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 215
    zlevel -1

    graphics [
      x 1000.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:32299776"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re292"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Trigger"
      minerva_x 2999.036644935608
      minerva_y 578.5357055664062
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re292"
      reaction_meta_id "re292"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 218
    zlevel -1

    graphics [
      x 1100.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID: 8136018"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re179"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Heterodimer association"
      minerva_x 560.0000000000007
      minerva_y 2126.8749999999995
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re179"
      reaction_meta_id "re179"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 223
    zlevel -1

    graphics [
      x 1200.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:22949645"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re276"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Truncation"
      minerva_x 1558.0
      minerva_y 1027.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re276"
      reaction_meta_id "re276"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 228
    zlevel -1

    graphics [
      x 200.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:32299776"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re309"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 2041.8103893959728
      minerva_y 524.4162639023132
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re309"
      reaction_meta_id "re309"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 232
    zlevel -1

    graphics [
      x 300.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:2437112, DOI:10.1101/2020.04.25.20077842"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re326"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Heterodimer association"
      minerva_x 490.5000000000002
      minerva_y 2430.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re326"
      reaction_meta_id "re326"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 237
    zlevel -1

    graphics [
      x 400.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:10946292"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re338"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Truncation"
      minerva_x 1750.5
      minerva_y 1097.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re338"
      reaction_meta_id "re338"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 242
    zlevel -1

    graphics [
      x 500.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re379"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re379"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Transport"
      minerva_x 798.45
      minerva_y 920.0999999999999
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re379"
      reaction_meta_id "re379"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 245
    zlevel -1

    graphics [
      x 600.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:11290788, PMID:32299776"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re274"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 1635.5
      minerva_y 902.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re274"
      reaction_meta_id "re274"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 249
    zlevel -1

    graphics [
      x 700.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:25573909"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re342"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Trigger"
      minerva_x 1220.4797308215898
      minerva_y 1753.795192905694
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re342"
      reaction_meta_id "re342"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 252
    zlevel -1

    graphics [
      x 800.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re390"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re390"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Truncation"
      minerva_x 622.5
      minerva_y 1422.5000000000005
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re390"
      reaction_meta_id "re390"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 258
    zlevel -1

    graphics [
      x 900.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:579490"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re174"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 700.5
      minerva_y 2041.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re174"
      reaction_meta_id "re174"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 263
    zlevel -1

    graphics [
      x 1000.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:32048163"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re183"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 1087.1244231037308
      minerva_y 1120.75
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re183"
      reaction_meta_id "re183"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 266
    zlevel -1

    graphics [
      x 1100.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "DOI:10.1101/2020.04.25.20077842"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re328"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 1095.175945613501
      minerva_y 2905.3267105859604
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re328"
      reaction_meta_id "re328"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 270
    zlevel -1

    graphics [
      x 1200.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:32299776"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re339"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Trigger"
      minerva_x 1713.0
      minerva_y 726.75
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re339"
      reaction_meta_id "re339"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 273
    zlevel -1

    graphics [
      x 200.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:21304106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re329"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 421.0
      minerva_y 1056.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re329"
      reaction_meta_id "re329"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 277
    zlevel -1

    graphics [
      x 300.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:18026570"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re364"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 2788.3823529411757
      minerva_y 2492.3455882352937
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re364"
      reaction_meta_id "re364"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 281
    zlevel -1

    graphics [
      x 400.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:4430674,PMID:3818642"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re173"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 550.5
      minerva_y 2041.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re173"
      reaction_meta_id "re173"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 285
    zlevel -1

    graphics [
      x 500.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:32299776"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re308"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 1474.9999999999995
      minerva_y 656.9999999999997
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re308"
      reaction_meta_id "re308"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 288
    zlevel -1

    graphics [
      x 600.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re359"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re359"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Trigger"
      minerva_x 2958.0
      minerva_y 477.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re359"
      reaction_meta_id "re359"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 291
    zlevel -1

    graphics [
      x 700.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:32171076"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re315"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 1410.4162400536507
      minerva_y 1595.3709525600834
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re315"
      reaction_meta_id "re315"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 295
    zlevel -1

    graphics [
      x 800.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:32504360"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re344"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 1652.500349272108
      minerva_y 1837.7052083062422
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re344"
      reaction_meta_id "re344"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 300
    zlevel -1

    graphics [
      x 900.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:32048163"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re375"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 968.5705181300452
      minerva_y 1244.6557337294603
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re375"
      reaction_meta_id "re375"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 304
    zlevel -1

    graphics [
      x 1000.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:32302438"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re323"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 608.0
      minerva_y 1585.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re323"
      reaction_meta_id "re323"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 308
    zlevel -1

    graphics [
      x 1100.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:19065996"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re157"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 1037.3755768962692
      minerva_y 1120.75
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re157"
      reaction_meta_id "re157"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 312
    zlevel -1

    graphics [
      x 1200.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re376"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re376"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 870.1588623567577
      minerva_y 1170.2615402932106
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re376"
      reaction_meta_id "re376"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 320
    zlevel -1

    graphics [
      x 200.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMCID:PMC7260598"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re334"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 1873.5
      minerva_y 1444.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re334"
      reaction_meta_id "re334"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 324
    zlevel -1

    graphics [
      x 300.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:29472360"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re363"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Heterodimer association"
      minerva_x 2271.875000000002
      minerva_y 2306.7500000000014
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re363"
      reaction_meta_id "re363"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 328
    zlevel -1

    graphics [
      x 400.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:32359396"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re332"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 1685.5000009753126
      minerva_y 1685.7820065183714
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re332"
      reaction_meta_id "re332"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 333
    zlevel -1

    graphics [
      x 500.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:10585461"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re154"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 1061.9898532969064
      minerva_y 833.87182228041
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re154"
      reaction_meta_id "re154"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 337
    zlevel -1

    graphics [
      x 600.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID: 89876, PMID:385647"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re178"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 648.6097170523456
      minerva_y 2545.5045422599364
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re178"
      reaction_meta_id "re178"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 344
    zlevel -1

    graphics [
      x 700.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:10373228, PMID:3124286"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re180"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 1019.5
      minerva_y 2546.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re180"
      reaction_meta_id "re180"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 352
    zlevel -1

    graphics [
      x 800.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:7391081"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re159"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Truncation"
      minerva_x 347.5000000000001
      minerva_y 1155.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re159"
      reaction_meta_id "re159"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 357
    zlevel -1

    graphics [
      x 900.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:9012652"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re341"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 764.5
      minerva_y 2615.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re341"
      reaction_meta_id "re341"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 361
    zlevel -1

    graphics [
      x 1000.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re391"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re391"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 616.1167891818948
      minerva_y 829.0803039933611
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re391"
      reaction_meta_id "re391"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 365
    zlevel -1

    graphics [
      x 1100.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:284414"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re266"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Heterodimer association"
      minerva_x 2384.5
      minerva_y 850.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re266"
      reaction_meta_id "re266"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 369
    zlevel -1

    graphics [
      x 1200.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re371"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re371"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Trigger"
      minerva_x 1169.4502006827206
      minerva_y 1577.9949622053664
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re371"
      reaction_meta_id "re371"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 372
    zlevel -1

    graphics [
      x 200.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:21304106, PMID:8631976"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re160"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Truncation"
      minerva_x 519.0000000000002
      minerva_y 1206.4999999999995
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re160"
      reaction_meta_id "re160"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 377
    zlevel -1

    graphics [
      x 300.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re382"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re382"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 522.75
      minerva_y 593.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re382"
      reaction_meta_id "re382"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 382
    zlevel -1

    graphics [
      x 400.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:26521297"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re285"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Heterodimer association"
      minerva_x 2093.5
      minerva_y 1102.4999999999998
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re285"
      reaction_meta_id "re285"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 386
    zlevel -1

    graphics [
      x 500.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:32286245"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re337"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 1705.5
      minerva_y 1570.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re337"
      reaction_meta_id "re337"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 391
    zlevel -1

    graphics [
      x 600.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:30083158, PMID: 12878586"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re262"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Truncation"
      minerva_x 1866.499999999999
      minerva_y 1394.7469135802476
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re262"
      reaction_meta_id "re262"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 397
    zlevel -1

    graphics [
      x 700.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:22471307"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re170"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Heterodimer association"
      minerva_x 420.5000000000002
      minerva_y 1486.5000000000002
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re170"
      reaction_meta_id "re170"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 401
    zlevel -1

    graphics [
      x 800.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:10749699"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re374"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 583.0
      minerva_y 907.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re374"
      reaction_meta_id "re374"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 406
    zlevel -1

    graphics [
      x 900.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:9100000"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re169"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 334.0
      minerva_y 1398.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re169"
      reaction_meta_id "re169"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 410
    zlevel -1

    graphics [
      x 1000.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:7944388"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re163"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Heterodimer association"
      minerva_x 240.00000000000068
      minerva_y 1000.2720588235297
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re163"
      reaction_meta_id "re163"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 414
    zlevel -1

    graphics [
      x 1100.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:6768384, PMID:4627469"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re202"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Truncation"
      minerva_x 354.3499999999979
      minerva_y 888.3639705882358
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re202"
      reaction_meta_id "re202"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 420
    zlevel -1

    graphics [
      x 1200.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:2303476"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re172"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Heterodimer association"
      minerva_x 550.7499999999998
      minerva_y 1783.9852941176466
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re172"
      reaction_meta_id "re172"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 424
    zlevel -1

    graphics [
      x 200.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:25051961"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re357"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Heterodimer association"
      minerva_x 2008.0
      minerva_y 2420.375
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re357"
      reaction_meta_id "re357"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 428
    zlevel -1

    graphics [
      x 300.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:25051961"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re358"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Heterodimer association"
      minerva_x 1795.7187499999998
      minerva_y 2431.0625
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re358"
      reaction_meta_id "re358"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 432
    zlevel -1

    graphics [
      x 400.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:11551226"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re155"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Truncation"
      minerva_x 365.4999999999993
      minerva_y 1746.4999999999995
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re155"
      reaction_meta_id "re155"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 438
    zlevel -1

    graphics [
      x 500.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:27561337"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re229"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Trigger"
      minerva_x 1369.3018538635451
      minerva_y 1706.6754320377609
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re229"
      reaction_meta_id "re229"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 441
    zlevel -1

    graphics [
      x 600.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:5058233"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re264"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Heterodimer association"
      minerva_x 2249.9999999999995
      minerva_y 1049.2499999999995
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re264"
      reaction_meta_id "re264"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 445
    zlevel -1

    graphics [
      x 700.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re360"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re360"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Trigger"
      minerva_x 3048.0
      minerva_y 477.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re360"
      reaction_meta_id "re360"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 448
    zlevel -1

    graphics [
      x 800.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re356"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re356"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Trigger"
      minerva_x 2236.006134969325
      minerva_y 2527.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re356"
      reaction_meta_id "re356"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 451
    zlevel -1

    graphics [
      x 900.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:32299776"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re325"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 1474.2651119121942
      minerva_y 845.5069025073001
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re325"
      reaction_meta_id "re325"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 455
    zlevel -1

    graphics [
      x 1000.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:25573909"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re340"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Trigger"
      minerva_x 1221.9997741748684
      minerva_y 1232.749645333976
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re340"
      reaction_meta_id "re340"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 458
    zlevel -1

    graphics [
      x 1100.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:27561337"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re231"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Trigger"
      minerva_x 1366.7860920942321
      minerva_y 1706.7702203089711
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re231"
      reaction_meta_id "re231"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 461
    zlevel -1

    graphics [
      x 1200.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:32336612, PMID:16008552"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re304"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 1170.75
      minerva_y 829.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re304"
      reaction_meta_id "re304"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 465
    zlevel -1

    graphics [
      x 200.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMICID:PMC7260598"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re324"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 2857.411006850371
      minerva_y 897.5032417143223
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re324"
      reaction_meta_id "re324"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 469
    zlevel -1

    graphics [
      x 300.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re373"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re373"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Trigger"
      minerva_x 1337.103652081863
      minerva_y 700.8051340860973
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re373"
      reaction_meta_id "re373"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 472
    zlevel -1

    graphics [
      x 400.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:32367170"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re295"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 733.9169358025658
      minerva_y 1463.370266076596
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re295"
      reaction_meta_id "re295"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 476
    zlevel -1

    graphics [
      x 500.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re394"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re394"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Trigger"
      minerva_x 356.6595156191914
      minerva_y 730.097210042774
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re394"
      reaction_meta_id "re394"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 479
    zlevel -1

    graphics [
      x 600.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:27561337"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re227"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Trigger"
      minerva_x 1371.8424888612672
      minerva_y 1706.6575809061521
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re227"
      reaction_meta_id "re227"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 482
    zlevel -1

    graphics [
      x 700.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:9490235"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re320"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Trigger"
      minerva_x 1288.9423076923076
      minerva_y 1670.8173076923076
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re320"
      reaction_meta_id "re320"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 485
    zlevel -1

    graphics [
      x 800.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:20689271"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re306"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 1254.7886753535981
      minerva_y 861.7533373045542
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re306"
      reaction_meta_id "re306"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 488
    zlevel -1

    graphics [
      x 900.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:29096812, PMID:7577232"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re345"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 1582.0
      minerva_y 2112.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re345"
      reaction_meta_id "re345"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 492
    zlevel -1

    graphics [
      x 1000.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID: 32299776"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re289"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Trigger"
      minerva_x 1527.5
      minerva_y 671.0357142857142
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re289"
      reaction_meta_id "re289"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 495
    zlevel -1

    graphics [
      x 1100.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:8158359"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re366"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 2591.0
      minerva_y 2385.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re366"
      reaction_meta_id "re366"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 499
    zlevel -1

    graphics [
      x 1200.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re369"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re369"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Trigger"
      minerva_x 2548.489280245021
      minerva_y 2485.375
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re369"
      reaction_meta_id "re369"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 502
    zlevel -1

    graphics [
      x 200.0
      y 2200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:26521297"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re278"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Heterodimer association"
      minerva_x 1608.9999999999998
      minerva_y 1216.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re278"
      reaction_meta_id "re278"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 506
    zlevel -1

    graphics [
      x 300.0
      y 2200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:29096812, PMID:10574983, PMID:32172226"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re346"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 1414.5
      minerva_y 2112.499999999999
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re346"
      reaction_meta_id "re346"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 512
    zlevel -1

    graphics [
      x 400.0
      y 2200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re386"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re386"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 818.5
      minerva_y 776.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re386"
      reaction_meta_id "re386"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 516
    zlevel -1

    graphics [
      x 500.0
      y 2200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:28228446, PMID: 6282863, PMID:2117226 "
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re234"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 1792.625
      minerva_y 2112.4062133645957
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re234"
      reaction_meta_id "re234"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 522
    zlevel -1

    graphics [
      x 600.0
      y 2200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:19286885"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re352"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Known transition omitted"
      minerva_x 1873.0
      minerva_y 2704.9375
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re352"
      reaction_meta_id "re352"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 525
    zlevel -1

    graphics [
      x 700.0
      y 2200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re392"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re392"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Trigger"
      minerva_x 409.0
      minerva_y 536.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re392"
      reaction_meta_id "re392"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 528
    zlevel -1

    graphics [
      x 800.0
      y 2200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:5058233"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re263"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Heterodimer association"
      minerva_x 2091.4999999999995
      minerva_y 1315.25
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re263"
      reaction_meta_id "re263"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 532
    zlevel -1

    graphics [
      x 900.0
      y 2200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:6796960"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re267"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Heterodimer association"
      minerva_x 2752.000000000001
      minerva_y 817.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re267"
      reaction_meta_id "re267"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 536
    zlevel -1

    graphics [
      x 1000.0
      y 2200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:3500650"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re321"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Trigger"
      minerva_x 1168.5007480421273
      minerva_y 1964.6822196130647
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re321"
      reaction_meta_id "re321"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 539
    zlevel -1

    graphics [
      x 1100.0
      y 2200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re395"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re395"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Trigger"
      minerva_x 458.75152309207965
      minerva_y 650.0085628406744
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re395"
      reaction_meta_id "re395"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 542
    zlevel -1

    graphics [
      x 1200.0
      y 2200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:32275855"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re305"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Heterodimer association"
      minerva_x 1181.5
      minerva_y 673.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re305"
      reaction_meta_id "re305"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 546
    zlevel -1

    graphics [
      x 200.0
      y 2300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re361"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re361"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Trigger"
      minerva_x 1563.0
      minerva_y 506.50001525878906
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re361"
      reaction_meta_id "re361"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 549
    zlevel -1

    graphics [
      x 300.0
      y 2300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:23809134"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re181"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 723.5000000000001
      minerva_y 2257.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re181"
      reaction_meta_id "re181"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 553
    zlevel -1

    graphics [
      x 400.0
      y 2300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:11290788"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re275"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 1823.8125
      minerva_y 986.5624999999995
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re275"
      reaction_meta_id "re275"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 557
    zlevel -1

    graphics [
      x 500.0
      y 2300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:22449964"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re182"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Heterodimer association"
      minerva_x 1243.4999999999998
      minerva_y 2467.9999999999995
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re182"
      reaction_meta_id "re182"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 562
    zlevel -1

    graphics [
      x 600.0
      y 2300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:32286245"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re336"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 1695.0000000273972
      minerva_y 1627.8418773408207
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re336"
      reaction_meta_id "re336"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 567
    zlevel -1

    graphics [
      x 700.0
      y 2300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re365"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re365"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Negative influence"
      minerva_x 2651.5
      minerva_y 2558.125
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re365"
      reaction_meta_id "re365"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 570
    zlevel -1

    graphics [
      x 800.0
      y 2300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re399"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re399"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Modulation"
      minerva_x 746.4567640899693
      minerva_y 1131.0270292269906
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re399"
      reaction_meta_id "re399"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 573
    zlevel -1

    graphics [
      x 900.0
      y 2300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:2322551, PMID:6282863, PMID:6572921"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re171"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 668.0
      minerva_y 1690.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re171"
      reaction_meta_id "re171"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 578
    zlevel -1

    graphics [
      x 1000.0
      y 2300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re396"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re396"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Trigger"
      minerva_x 1024.358550202008
      minerva_y 1106.6357203989298
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re396"
      reaction_meta_id "re396"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 581
    zlevel -1

    graphics [
      x 1100.0
      y 2300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:27561337"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re259"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Trigger"
      minerva_x 1369.3591536077388
      minerva_y 1706.6220464932449
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re259"
      reaction_meta_id "re259"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 584
    zlevel -1

    graphics [
      x 1200.0
      y 2300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:32367170"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re319"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "State transition"
      minerva_x 1450.5
      minerva_y 2206.500000000001
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re319"
      reaction_meta_id "re319"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 588
    zlevel -1

    graphics [
      x 200.0
      y 2400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re393"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re393"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_007293"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=C4A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/720"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1323"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000244731"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P0C0L4"
      minerva_ref_type__resource1 "TAXONOMY__9606"
      minerva_ref_type__resource10 "REFSEQ__NM_007293"
      minerva_ref_type__resource2 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource3 "ENTREZ__720"
      minerva_ref_type__resource4 "HGNC_SYMBOL__C4A"
      minerva_ref_type__resource5 "ENTREZ__720"
      minerva_ref_type__resource6 "HGNC__1323"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000244731"
      minerva_ref_type__resource8 "UNIPROT__P0C0L4"
      minerva_ref_type__resource9 "UNIPROT__P0C0L4"
      minerva_type "Trigger"
      minerva_x 527.1584346537268
      minerva_y 506.9779038085181
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re393"
      reaction_meta_id "re393"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 591
    zlevel -1

    cluster [
      cluster "s_id_ca13"
    ]
    graphics [
      x 750.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "c5b_minus_9__endothelium,_space_vascular__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa32"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "true"
      minerva_elementId "csa32"
      minerva_fullName "membrane attack complex"
      minerva_name "C5b-9"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0005579"
      minerva_ref_link10 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1339"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=9606"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1352"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1353"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1354"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1346"
      minerva_ref_link7 "http://id.nlm.nih.gov/mesh/D050776"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1358"
      minerva_ref_link9 "http://id.nlm.nih.gov/mesh/D015938"
      minerva_ref_type__resource1 "GO__GO:0005579"
      minerva_ref_type__resource10 "HGNC__1339"
      minerva_ref_type__resource2 "TAXONOMY__9606"
      minerva_ref_type__resource3 "HGNC__1352"
      minerva_ref_type__resource4 "HGNC__1353"
      minerva_ref_type__resource5 "HGNC__1354"
      minerva_ref_type__resource6 "HGNC__1346"
      minerva_ref_type__resource7 "MESH_2012__D050776"
      minerva_ref_type__resource8 "HGNC__1358"
      minerva_ref_type__resource9 "MESH_2012__D015938"
      minerva_type "Complex"
      minerva_x 2929.0
      minerva_y 706.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca13"
      compartment_name "endothelium,_space_vascular"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation "Annotation [about=\"#s_id_csa32\", history=]"
      species_id "s_id_csa32"
      species_meta_id "s_id_csa32"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
      species_notes "XMLNode 'notes' [childElements size=1]"
    ]
  ]
  edge [
    id 148
    source 27
    target 147
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa306"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 149
    source 147
    target 38
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa271"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 151
    source 134
    target 150
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa356"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 152
    source 150
    target 45
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa236"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 153
    source 90
    target 150
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR0"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa224"
    ]
  ]
  edge [
    id 155
    source 39
    target 154
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa415"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 156
    source 154
    target 92
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa197"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 157
    source 26
    target 154
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR1"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa251"
    ]
  ]
  edge [
    id 159
    source 4
    target 158
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa455"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 160
    source 158
    target 1
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa242"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 161
    source 14
    target 158
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR2"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa480"
    ]
  ]
  edge [
    id 162
    source 73
    target 158
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR3"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa416"
    ]
  ]
  edge [
    id 164
    source 71
    target 163
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa365"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 165
    source 163
    target 122
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa387"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 166
    source 142
    target 163
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR4"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa385"
    ]
  ]
  edge [
    id 168
    source 13
    target 167
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa504"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 169
    source 167
    target 10
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa521"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 170
    source 69
    target 167
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR5"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa531"
    ]
  ]
  edge [
    id 172
    source 140
    target 171
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa467"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 173
    source 171
    target 118
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa253"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 174
    source 24
    target 171
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR6"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa212"
    ]
  ]
  edge [
    id 176
    source 97
    target 175
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa252"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 177
    source 175
    target 135
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa308"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 178
    source 175
    target 130
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa310"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 179
    source 85
    target 175
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR7"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa26"
    ]
  ]
  edge [
    id 180
    source 41
    target 175
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR8"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa35"
    ]
  ]
  edge [
    id 182
    source 17
    target 181
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa494"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 183
    source 181
    target 77
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa213"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 184
    source 62
    target 181
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR9"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa402"
    ]
  ]
  edge [
    id 186
    source 18
    target 185
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa479"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 187
    source 185
    target 114
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa202"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 188
    source 14
    target 185
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR10"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa480"
    ]
  ]
  edge [
    id 190
    source 78
    target 189
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa194"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 191
    source 189
    target 98
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa400"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 192
    source 19
    target 189
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR11"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa394"
    ]
  ]
  edge [
    id 194
    source 34
    target 193
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa505"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 195
    source 193
    target 90
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa224"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 196
    source 79
    target 193
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR12"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa503"
    ]
  ]
  edge [
    id 197
    source 78
    target 193
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR13"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa194"
    ]
  ]
  edge [
    id 198
    source 98
    target 193
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR14"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa400"
    ]
  ]
  edge [
    id 199
    source 1
    target 193
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR15"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa242"
    ]
  ]
  edge [
    id 200
    source 117
    target 193
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR16"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa519"
    ]
  ]
  edge [
    id 202
    source 124
    target 201
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa497"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 203
    source 201
    target 36
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa17"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 204
    source 14
    target 201
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR17"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa480"
    ]
  ]
  edge [
    id 206
    source 106
    target 205
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa40"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 207
    source 205
    target 133
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa430"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 209
    source 137
    target 208
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa181"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 210
    source 141
    target 208
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa301"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 211
    source 208
    target 70
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa25"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 213
    source 133
    target 212
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa430"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 214
    source 212
    target 68
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa441"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 217
    source 215
    target 136
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa390"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 219
    source 114
    target 218
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa202"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 220
    source 137
    target 218
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa181"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 221
    source 218
    target 107
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa14"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 222
    source 14
    target 218
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR18"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa480"
    ]
  ]
  edge [
    id 224
    source 53
    target 223
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa363"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 225
    source 223
    target 71
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa365"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 226
    source 223
    target 56
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa364"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 227
    source 54
    target 223
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR19"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa459"
    ]
  ]
  edge [
    id 230
    source 23
    target 228
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa391"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 233
    source 24
    target 232
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa212"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 234
    source 15
    target 232
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa412"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 235
    source 232
    target 75
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa38"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 236
    source 14
    target 232
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR20"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa480"
    ]
  ]
  edge [
    id 238
    source 120
    target 237
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa366"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 239
    source 237
    target 127
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa367"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 240
    source 237
    target 102
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa368"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 241
    source 47
    target 237
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR21"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa358"
    ]
  ]
  edge [
    id 243
    source 79
    target 242
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa503"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 244
    source 242
    target 93
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa509"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 246
    source 59
    target 245
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa357"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 247
    source 245
    target 54
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa459"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 248
    source 14
    target 245
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR22"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa480"
    ]
  ]
  edge [
    id 251
    source 249
    target 38
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa271"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 253
    source 80
    target 252
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa179"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 254
    source 252
    target 25
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa183"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 255
    source 252
    target 55
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa180"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 256
    source 137
    target 252
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR23"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa181"
    ]
  ]
  edge [
    id 257
    source 16
    target 252
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR24"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa205"
    ]
  ]
  edge [
    id 259
    source 40
    target 258
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa498"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 260
    source 258
    target 137
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa181"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 261
    source 114
    target 258
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR25"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa202"
    ]
  ]
  edge [
    id 262
    source 22
    target 258
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR26"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa355"
    ]
  ]
  edge [
    id 264
    source 131
    target 263
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa195"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 265
    source 263
    target 78
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa194"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 267
    source 72
    target 266
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa478"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 268
    source 266
    target 141
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa301"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 269
    source 14
    target 266
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR27"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa480"
    ]
  ]
  edge [
    id 271
    source 54
    target 270
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa459"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 272
    source 270
    target 104
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa417"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 274
    source 119
    target 273
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa167"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 275
    source 273
    target 26
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa251"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 276
    source 145
    target 273
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR28"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa165"
    ]
  ]
  edge [
    id 278
    source 3
    target 277
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa496"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 279
    source 277
    target 64
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa483"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 280
    source 98
    target 277
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR29"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa400"
    ]
  ]
  edge [
    id 282
    source 91
    target 281
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa182"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 283
    source 281
    target 40
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa498"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 284
    source 31
    target 281
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR30"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa13"
    ]
  ]
  edge [
    id 286
    source 23
    target 285
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa391"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 287
    source 285
    target 122
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa387"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 289
    source 136
    target 288
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa390"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 290
    source 288
    target 121
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa409"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 292
    source 4
    target 291
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa455"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 293
    source 291
    target 138
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa410"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 294
    source 14
    target 291
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR31"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa480"
    ]
  ]
  edge [
    id 296
    source 4
    target 295
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa455"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 297
    source 295
    target 89
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa238"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 298
    source 14
    target 295
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR32"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa480"
    ]
  ]
  edge [
    id 299
    source 73
    target 295
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR33"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa416"
    ]
  ]
  edge [
    id 301
    source 13
    target 300
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa504"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 302
    source 300
    target 78
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa194"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 303
    source 14
    target 300
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR34"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa480"
    ]
  ]
  edge [
    id 305
    source 76
    target 304
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa482"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 306
    source 304
    target 16
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa205"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 307
    source 14
    target 304
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR35"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa480"
    ]
  ]
  edge [
    id 309
    source 131
    target 308
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa195"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 310
    source 308
    target 78
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa194"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 311
    source 125
    target 308
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR36"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa198"
    ]
  ]
  edge [
    id 313
    source 13
    target 312
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa504"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 314
    source 312
    target 79
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa503"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 315
    source 14
    target 312
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR37"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa480"
    ]
  ]
  edge [
    id 316
    source 78
    target 312
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR38"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa194"
    ]
  ]
  edge [
    id 317
    source 10
    target 312
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR39"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa521"
    ]
  ]
  edge [
    id 318
    source 117
    target 312
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR40"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa519"
    ]
  ]
  edge [
    id 319
    source 52
    target 312
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR41"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa527"
    ]
  ]
  edge [
    id 321
    source 111
    target 320
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa461"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 322
    source 320
    target 118
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa253"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 323
    source 14
    target 320
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR42"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa480"
    ]
  ]
  edge [
    id 325
    source 36
    target 324
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa17"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 326
    source 58
    target 324
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa431"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 327
    source 324
    target 35
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa42"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 329
    source 4
    target 328
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa455"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 330
    source 328
    target 132
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa244"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 331
    source 14
    target 328
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR43"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa480"
    ]
  ]
  edge [
    id 332
    source 73
    target 328
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR44"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa416"
    ]
  ]
  edge [
    id 334
    source 12
    target 333
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa196"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 335
    source 333
    target 131
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa195"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 336
    source 92
    target 333
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR45"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa197"
    ]
  ]
  edge [
    id 338
    source 48
    target 337
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa211"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 339
    source 337
    target 24
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa212"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 340
    source 77
    target 337
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR46"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa213"
    ]
  ]
  edge [
    id 341
    source 45
    target 337
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR47"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa236"
    ]
  ]
  edge [
    id 342
    source 87
    target 337
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR48"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa173"
    ]
  ]
  edge [
    id 343
    source 119
    target 337
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR49"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa167"
    ]
  ]
  edge [
    id 345
    source 30
    target 344
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa225"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 346
    source 344
    target 77
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa213"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 347
    source 90
    target 344
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR50"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa224"
    ]
  ]
  edge [
    id 348
    source 62
    target 344
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR51"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa402"
    ]
  ]
  edge [
    id 349
    source 78
    target 344
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR52"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa194"
    ]
  ]
  edge [
    id 350
    source 98
    target 344
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR53"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa400"
    ]
  ]
  edge [
    id 351
    source 16
    target 344
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR54"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa205"
    ]
  ]
  edge [
    id 353
    source 49
    target 352
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa170"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 354
    source 352
    target 145
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa165"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 355
    source 352
    target 25
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa183"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 356
    source 119
    target 352
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR55"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa167"
    ]
  ]
  edge [
    id 358
    source 143
    target 357
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa495"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 359
    source 357
    target 48
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa211"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 362
    source 32
    target 361
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa526"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 363
    source 361
    target 125
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa198"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 364
    source 79
    target 361
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR57"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa503"
    ]
  ]
  edge [
    id 366
    source 82
    target 365
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa29"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 367
    source 44
    target 365
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa30"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 368
    source 365
    target 43
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa31"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 370
    source 78
    target 369
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa194"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 371
    source 369
    target 38
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa271"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 373
    source 145
    target 372
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa165"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 374
    source 372
    target 94
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa172"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 375
    source 372
    target 87
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa173"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 376
    source 137
    target 372
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR58"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa181"
    ]
  ]
  edge [
    id 378
    source 100
    target 377
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa506"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 379
    source 377
    target 65
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa516"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 380
    source 93
    target 377
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR59"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa509"
    ]
  ]
  edge [
    id 381
    source 117
    target 377
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR60"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa519"
    ]
  ]
  edge [
    id 383
    source 130
    target 382
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa310"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 384
    source 85
    target 382
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa26"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 385
    source 382
    target 11
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa27"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 387
    source 4
    target 386
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa455"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 388
    source 386
    target 27
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa306"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 389
    source 14
    target 386
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR61"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa480"
    ]
  ]
  edge [
    id 390
    source 73
    target 386
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR62"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa416"
    ]
  ]
  edge [
    id 392
    source 21
    target 391
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa314"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 393
    source 391
    target 96
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa315"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 394
    source 391
    target 118
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa253"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 395
    source 41
    target 391
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR63"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa35"
    ]
  ]
  edge [
    id 396
    source 11
    target 391
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR64"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa27"
    ]
  ]
  edge [
    id 398
    source 5
    target 397
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa175"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 399
    source 55
    target 397
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa180"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 400
    source 397
    target 126
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa12"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 402
    source 62
    target 401
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa402"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 403
    source 401
    target 37
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa407"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 404
    source 401
    target 25
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa183"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 405
    source 125
    target 401
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR65"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa198"
    ]
  ]
  edge [
    id 407
    source 123
    target 406
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa174"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 408
    source 406
    target 5
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa175"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 409
    source 87
    target 406
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR66"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa173"
    ]
  ]
  edge [
    id 411
    source 119
    target 410
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa167"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 412
    source 128
    target 410
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa169"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 413
    source 410
    target 66
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa11"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 415
    source 66
    target 414
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa11"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 416
    source 414
    target 50
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa250"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 417
    source 414
    target 62
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa402"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 418
    source 414
    target 26
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa251"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 421
    source 109
    target 420
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa176"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 422
    source 6
    target 420
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa201"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 423
    source 420
    target 31
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa13"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 425
    source 58
    target 424
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa431"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 426
    source 57
    target 424
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa41"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 427
    source 424
    target 139
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa39"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 429
    source 139
    target 428
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa39"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 430
    source 144
    target 428
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa411"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 431
    source 428
    target 106
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa40"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 433
    source 95
    target 432
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa177"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 434
    source 432
    target 109
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa176"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 435
    source 432
    target 25
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa183"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 436
    source 5
    target 432
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR68"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa175"
    ]
  ]
  edge [
    id 437
    source 114
    target 432
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR69"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa202"
    ]
  ]
  edge [
    id 439
    source 1
    target 438
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa242"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 440
    source 438
    target 38
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa271"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 442
    source 29
    target 441
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa28"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 443
    source 146
    target 441
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa321"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 444
    source 441
    target 82
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa29"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 446
    source 136
    target 445
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa390"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 447
    source 445
    target 81
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa408"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 449
    source 35
    target 448
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa42"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 450
    source 448
    target 133
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa430"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 452
    source 42
    target 451
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa460"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 453
    source 451
    target 122
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa387"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 454
    source 14
    target 451
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR70"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa480"
    ]
  ]
  edge [
    id 456
    source 122
    target 455
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa387"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 457
    source 455
    target 38
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa271"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 459
    source 89
    target 458
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa238"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 460
    source 458
    target 38
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa271"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 462
    source 101
    target 461
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa398"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 463
    source 461
    target 19
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa394"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 464
    source 14
    target 461
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR71"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa480"
    ]
  ]
  edge [
    id 466
    source 7
    target 465
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa462"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 468
    source 14
    target 465
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR72"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa480"
    ]
  ]
  edge [
    id 470
    source 86
    target 469
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa37"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 471
    source 469
    target 33
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa485"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 473
    source 4
    target 472
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa455"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 474
    source 472
    target 80
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa179"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 475
    source 14
    target 472
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR73"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa480"
    ]
  ]
  edge [
    id 477
    source 93
    target 476
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa509"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 478
    source 476
    target 116
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa518"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 480
    source 132
    target 479
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa244"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 481
    source 479
    target 38
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa271"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 483
    source 138
    target 482
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa410"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 484
    source 482
    target 38
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa271"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 486
    source 13
    target 485
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa504"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 487
    source 485
    target 101
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa398"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 489
    source 83
    target 488
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa395"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 490
    source 488
    target 67
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa424"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 491
    source 8
    target 488
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR74"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa425"
    ]
  ]
  edge [
    id 493
    source 122
    target 492
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa387"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 494
    source 492
    target 28
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa388"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 496
    source 61
    target 495
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa500"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 497
    source 495
    target 88
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa484"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 498
    source 78
    target 495
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR75"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa194"
    ]
  ]
  edge [
    id 500
    source 88
    target 499
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa484"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 501
    source 499
    target 68
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa441"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 503
    source 71
    target 502
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa365"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 504
    source 127
    target 502
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa367"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 505
    source 502
    target 41
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa35"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 507
    source 67
    target 506
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa424"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 508
    source 506
    target 105
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa210"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 509
    source 24
    target 506
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR76"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa212"
    ]
  ]
  edge [
    id 510
    source 112
    target 506
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR77"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa227"
    ]
  ]
  edge [
    id 511
    source 14
    target 506
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR78"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa480"
    ]
  ]
  edge [
    id 513
    source 84
    target 512
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa520"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 514
    source 512
    target 117
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa519"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 515
    source 78
    target 512
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR79"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa194"
    ]
  ]
  edge [
    id 517
    source 36
    target 516
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa17"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 518
    source 516
    target 83
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa395"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 519
    source 40
    target 516
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR80"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa498"
    ]
  ]
  edge [
    id 520
    source 70
    target 516
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR81"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa25"
    ]
  ]
  edge [
    id 521
    source 14
    target 516
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR82"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa480"
    ]
  ]
  edge [
    id 523
    source 106
    target 522
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa40"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 524
    source 522
    target 51
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa440"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 526
    source 65
    target 525
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa516"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 527
    source 525
    target 115
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa529"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 529
    source 96
    target 528
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa315"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 530
    source 74
    target 528
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa318"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 531
    source 528
    target 29
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa28"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 533
    source 43
    target 532
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa31"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 534
    source 63
    target 532
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa337"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 537
    source 144
    target 536
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa411"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 538
    source 536
    target 38
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa271"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 540
    source 93
    target 539
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa509"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 541
    source 539
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa530"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 543
    source 101
    target 542
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa398"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 544
    source 23
    target 542
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa391"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 545
    source 542
    target 86
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa37"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 547
    source 28
    target 546
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa388"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 548
    source 546
    target 9
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa389"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 550
    source 108
    target 549
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa226"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 551
    source 549
    target 112
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa227"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 552
    source 137
    target 549
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR83"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa181"
    ]
  ]
  edge [
    id 554
    source 20
    target 553
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa458"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 555
    source 553
    target 47
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa358"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 556
    source 110
    target 553
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR84"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa362"
    ]
  ]
  edge [
    id 558
    source 90
    target 557
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa224"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 559
    source 30
    target 557
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa225"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 560
    source 557
    target 113
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa16"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 561
    source 14
    target 557
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR85"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa480"
    ]
  ]
  edge [
    id 563
    source 4
    target 562
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa455"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 564
    source 562
    target 46
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa304"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 565
    source 14
    target 562
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR86"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa480"
    ]
  ]
  edge [
    id 566
    source 73
    target 562
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR87"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa416"
    ]
  ]
  edge [
    id 568
    source 64
    target 567
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa483"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 569
    source 567
    target 68
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa441"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 571
    source 79
    target 570
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa503"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 572
    source 570
    target 69
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa531"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 574
    source 99
    target 573
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa200"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 575
    source 573
    target 6
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa201"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 576
    source 70
    target 573
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR88"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa25"
    ]
  ]
  edge [
    id 577
    source 16
    target 573
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR89"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa205"
    ]
  ]
  edge [
    id 579
    source 14
    target 578
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa480"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 580
    source 578
    target 69
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa531"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 582
    source 46
    target 581
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa304"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 583
    source 581
    target 38
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa271"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 585
    source 103
    target 584
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa472"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 586
    source 584
    target 144
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa411"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 587
    source 14
    target 584
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR90"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa480"
    ]
  ]
  edge [
    id 589
    source 117
    target 588
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa519"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 590
    source 588
    target 115
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa529"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 592
    source 591
    target 357
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR56"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa32"
    ]
  ]
  edge [
    id 593
    source 465
    target 591
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa32"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 594
    source 532
    target 591
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa32"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 596
    source 591
    target 215
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa32"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 597
    source 591
    target 228
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa32"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 598
    source 591
    target 249
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa32"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 600
    source 228
    target 591
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa43"
      stoichiometry "1.0"
    ]
  ]
]
