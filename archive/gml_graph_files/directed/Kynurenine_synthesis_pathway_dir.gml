# generated with VANTED V2.8.0 at Tue Apr 27 20:00:44 CEST 2021
graph [
  background_coloring "true"
  cluster_colors "255,128,128,255:0,0,0,255;255,213,128,255:0,0,0,255;213,255,128,255:0,0,0,255;128,255,128,255:0,0,0,255;128,255,213,255:0,0,0,255;128,212,255,255:0,0,0,255;128,128,255,255:0,0,0,255;213,128,255,255:0,0,0,255;255,128,212,255:0,0,0,255"
  clusterbackground_fill_outer_region "false"
  clusterbackground_grid 50.0
  clusterbackground_low_alpha 0.2
  clusterbackground_radius 200.0
  clusterbackground_space_fill "true"
  graphbackgroundcolor "#ffffff"
  sbml [
    model_meta_id "TRP_KYN_pathway"
    model_non_rdf_annotation ""
    model_notes ""
    model_sbml_id "TRP_KYN_pathway"
    namespace "xmlns:celldesigner=\"http://www.sbml.org/2001/ns/celldesigner\" "
    sbml_level "2"
    sbml_version "4"
  ]
  sbml_compartment_default [
    sbml_compartment_default_id "default"
    sbml_compartment_default_meta_id "default"
    sbml_compartment_default_non_rdf_annotation ""
    sbml_compartment_default_notes ""
    sbml_compartment_default_size "1.0"
    sbml_compartment_default_units "volume"
  ]
  sbml_compartment_s_id_ca1 [
    sbml_compartment_s_id_ca1_annotation ""
    sbml_compartment_s_id_ca1_id "s_id_ca1"
    sbml_compartment_s_id_ca1_meta_id "s_id_ca1"
    sbml_compartment_s_id_ca1_name "cell"
    sbml_compartment_s_id_ca1_non_rdf_annotation ""
    sbml_compartment_s_id_ca1_notes ""
    sbml_compartment_s_id_ca1_outside "s_id_ca3"
    sbml_compartment_s_id_ca1_size "1.0"
    sbml_compartment_s_id_ca1_units "volume"
  ]
  sbml_compartment_s_id_ca10 [
    sbml_compartment_s_id_ca10_id "s_id_ca10"
    sbml_compartment_s_id_ca10_meta_id "s_id_ca10"
    sbml_compartment_s_id_ca10_name "human_space_host"
    sbml_compartment_s_id_ca10_non_rdf_annotation ""
    sbml_compartment_s_id_ca10_notes ""
    sbml_compartment_s_id_ca10_outside "default"
    sbml_compartment_s_id_ca10_size "1.0"
    sbml_compartment_s_id_ca10_units "volume"
  ]
  sbml_compartment_s_id_ca3 [
    sbml_compartment_s_id_ca3_annotation ""
    sbml_compartment_s_id_ca3_id "s_id_ca3"
    sbml_compartment_s_id_ca3_meta_id "s_id_ca3"
    sbml_compartment_s_id_ca3_name "cell_space_membrane"
    sbml_compartment_s_id_ca3_non_rdf_annotation ""
    sbml_compartment_s_id_ca3_notes ""
    sbml_compartment_s_id_ca3_outside "s_id_ca10"
    sbml_compartment_s_id_ca3_size "1.0"
    sbml_compartment_s_id_ca3_units "volume"
  ]
  sbml_compartment_s_id_ca5 [
    sbml_compartment_s_id_ca5_annotation ""
    sbml_compartment_s_id_ca5_id "s_id_ca5"
    sbml_compartment_s_id_ca5_meta_id "s_id_ca5"
    sbml_compartment_s_id_ca5_name "mitochondrial_space_matrix"
    sbml_compartment_s_id_ca5_non_rdf_annotation ""
    sbml_compartment_s_id_ca5_notes ""
    sbml_compartment_s_id_ca5_outside "s_id_ca9"
    sbml_compartment_s_id_ca5_size "1.0"
    sbml_compartment_s_id_ca5_units "volume"
  ]
  sbml_compartment_s_id_ca6 [
    sbml_compartment_s_id_ca6_annotation ""
    sbml_compartment_s_id_ca6_id "s_id_ca6"
    sbml_compartment_s_id_ca6_meta_id "s_id_ca6"
    sbml_compartment_s_id_ca6_name "nucleus"
    sbml_compartment_s_id_ca6_non_rdf_annotation ""
    sbml_compartment_s_id_ca6_notes ""
    sbml_compartment_s_id_ca6_outside "s_id_ca1"
    sbml_compartment_s_id_ca6_size "1.0"
    sbml_compartment_s_id_ca6_units "volume"
  ]
  sbml_compartment_s_id_ca7 [
    sbml_compartment_s_id_ca7_annotation ""
    sbml_compartment_s_id_ca7_id "s_id_ca7"
    sbml_compartment_s_id_ca7_meta_id "s_id_ca7"
    sbml_compartment_s_id_ca7_name "peroxisome"
    sbml_compartment_s_id_ca7_non_rdf_annotation ""
    sbml_compartment_s_id_ca7_notes ""
    sbml_compartment_s_id_ca7_outside "s_id_ca1"
    sbml_compartment_s_id_ca7_size "1.0"
    sbml_compartment_s_id_ca7_units "volume"
  ]
  sbml_compartment_s_id_ca8 [
    sbml_compartment_s_id_ca8_annotation ""
    sbml_compartment_s_id_ca8_id "s_id_ca8"
    sbml_compartment_s_id_ca8_meta_id "s_id_ca8"
    sbml_compartment_s_id_ca8_name "endoplasmic_space_reticulum"
    sbml_compartment_s_id_ca8_non_rdf_annotation ""
    sbml_compartment_s_id_ca8_notes ""
    sbml_compartment_s_id_ca8_outside "s_id_ca1"
    sbml_compartment_s_id_ca8_size "1.0"
    sbml_compartment_s_id_ca8_units "volume"
  ]
  sbml_compartment_s_id_ca9 [
    sbml_compartment_s_id_ca9_annotation ""
    sbml_compartment_s_id_ca9_id "s_id_ca9"
    sbml_compartment_s_id_ca9_meta_id "s_id_ca9"
    sbml_compartment_s_id_ca9_name "mitochondrion"
    sbml_compartment_s_id_ca9_non_rdf_annotation ""
    sbml_compartment_s_id_ca9_notes ""
    sbml_compartment_s_id_ca9_outside "s_id_ca1"
    sbml_compartment_s_id_ca9_size "1.0"
    sbml_compartment_s_id_ca9_units "volume"
  ]
  sbml_unit_definition_1 [
    sbml_unit_definition_1_id "substance"
    sbml_unit_definition_1_name "substance"
    sbml_unit_definition_1_sub_unit_1_ "(1.0 * 10^0 * mole)^1.0"
    sbml_unit_definition_1unit "(1.0 * 10^0 * mole)^1.0"
  ]
  sbml_unit_definition_2 [
    sbml_unit_definition_2_id "volume"
    sbml_unit_definition_2_name "volume"
    sbml_unit_definition_2_sub_unit_1_ "(1.0 * 10^0 * litre)^1.0"
    sbml_unit_definition_2unit "(1.0 * 10^0 * litre)^1.0"
  ]
  sbml_unit_definition_3 [
    sbml_unit_definition_3_id "length"
    sbml_unit_definition_3_name "length"
    sbml_unit_definition_3_sub_unit_1_ "(1.0 * 10^0 * metre)^1.0"
    sbml_unit_definition_3unit "(1.0 * 10^0 * metre)^1.0"
  ]
  sbml_unit_definition_4 [
    sbml_unit_definition_4_id "area"
    sbml_unit_definition_4_name "area"
    sbml_unit_definition_4_sub_unit_1_ "(1.0 * 10^0 * metre)^2.0"
    sbml_unit_definition_4unit "(1.0 * 10^0 * metre)^2.0"
  ]
  sbml_unit_definition_5 [
    sbml_unit_definition_5_id "time"
    sbml_unit_definition_5_name "time"
    sbml_unit_definition_5_sub_unit_1_ "(1.0 * 10^0 * second)^1.0"
    sbml_unit_definition_5unit "(1.0 * 10^0 * second)^1.0"
  ]
  directed 1
  node [
    id 1
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "l_minus_kynurenine__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa17"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa17"
      minerva_fullName "L-kynurenine"
      minerva_name "L-Kynurenine"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16946"
      minerva_ref_type__resource1 "CHEBI__CHEBI:16946"
      minerva_synonyms "(2S)-2-amino-4-(2-aminophenyl)-4-oxobutanoic acid; 3-Anthraniloyl-L-alanine; KYNURENINE; L-Kynurenine"
      minerva_type "Simple molecule"
      minerva_x 1965.0
      minerva_y 1200.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa17"
      species_meta_id "s_id_sa17"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 2
    zlevel -1

    cluster [
      cluster "s_id_ca7"
    ]
    graphics [
      x 300.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "h_plus___peroxisome__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa287"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa287"
      minerva_fullName "hydron"
      minerva_name "H+"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15378"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15378"
      minerva_synonyms "H(+); H+; Hydron"
      minerva_type "Ion"
      minerva_x 801.8928571428575
      minerva_y 3013.7142857142862
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca7"
      compartment_name "peroxisome"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa287"
      species_meta_id "s_id_sa287"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 3
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "parps__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa209"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa209"
      minerva_name "PARPs"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/Q53GL7"
      minerva_ref_link10 "http://purl.uniprot.org/uniprot/Q8IXQ6"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/54956"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.4.2.-"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/Q8N5Y8"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/84875"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/83666"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PARP9"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PARP10"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PARP16"
      minerva_ref_type__resource1 "UNIPROT__Q53GL7"
      minerva_ref_type__resource10 "UNIPROT__Q8IXQ6"
      minerva_ref_type__resource2 "ENTREZ__54956"
      minerva_ref_type__resource3 "EC__2.4.2.-"
      minerva_ref_type__resource4 "UNIPROT__Q8N5Y8"
      minerva_ref_type__resource5 "ENTREZ__84875"
      minerva_ref_type__resource6 "ENTREZ__83666"
      minerva_ref_type__resource7 "HGNC_SYMBOL__PARP9"
      minerva_ref_type__resource8 "HGNC_SYMBOL__PARP10"
      minerva_ref_type__resource9 "HGNC_SYMBOL__PARP16"
      minerva_type "Protein"
      minerva_x 1625.0
      minerva_y 2790.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa209"
      species_meta_id "s_id_sa209"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 4
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 500.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "pi__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa262"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa262"
      minerva_elementId2 "sa259"
      minerva_fullName "phosphate(3-)"
      minerva_name "Pi"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18367"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18367"
      minerva_synonyms "Orthophosphate; PHOSPHATE ION; PO4(3-); Phosphate; [PO4](3-)"
      minerva_type "Ion"
      minerva_x 2705.0
      minerva_x2 2645.0
      minerva_y 680.0
      minerva_y2 500.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa262"
      species_meta_id "s_id_sa262"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 5
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "h2o__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa10"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa21"
      minerva_elementId2 "sa36"
      minerva_elementId3 "sa27"
      minerva_elementId4 "sa33"
      minerva_elementId5 "sa194"
      minerva_elementId6 "sa10"
      minerva_elementId7 "sa205"
      minerva_elementId8 "sa46"
      minerva_elementId9 "sa353"
      minerva_fullName "water"
      minerva_name "H2O"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15377"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15377"
      minerva_synonyms "BOUND WATER; H2O; H2O; HOH; WATER; Wasser; Water; [OH2]; acqua; agua; aqua; dihydridooxygen; dihydrogen oxide; eau; hydrogen hydroxide"
      minerva_type "Simple molecule"
      minerva_x 2015.0
      minerva_x2 2005.0
      minerva_x3 1555.0
      minerva_x4 1995.0
      minerva_x5 1535.0
      minerva_x6 1923.75
      minerva_x7 1375.0
      minerva_x8 1785.0
      minerva_x9 855.0
      minerva_y 1380.0
      minerva_y2 1870.0
      minerva_y3 1260.0
      minerva_y4 1450.0
      minerva_y5 2140.0
      minerva_y6 1012.75
      minerva_y7 2447.25
      minerva_y8 1790.0
      minerva_y9 1490.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa10"
      species_meta_id "s_id_sa10"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 6
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 700.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nad_plus___mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa108"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa108"
      minerva_fullName "NAD(1-)"
      minerva_name "NAD+"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:57540"
      minerva_ref_type__resource1 "CHEBI__CHEBI:57540"
      minerva_synonyms "NAD anion; NAD(+)"
      minerva_type "Simple molecule"
      minerva_x 290.0
      minerva_y 2310.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa108"
      species_meta_id "s_id_sa108"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 7
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 800.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "adp__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa99"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa99"
      minerva_fullName "ADP(3-)"
      minerva_name "ADP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:456216"
      minerva_ref_type__resource1 "CHEBI__CHEBI:456216"
      minerva_synonyms "5'-O-[(phosphonatooxy)phosphinato]adenosine; ADP; ADP trianion"
      minerva_type "Simple molecule"
      minerva_x 489.0
      minerva_y 2242.25
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa99"
      species_meta_id "s_id_sa99"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 9
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 1000.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "rnls:fad__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa20"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa20"
      minerva_name "RNLS:FAD"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/Q5VYX0"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16238"
      minerva_ref_type__resource1 "UNIPROT__Q5VYX0"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16238"
      minerva_type "Complex"
      minerva_x 2205.0
      minerva_y 130.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa20"
      species_meta_id "s_id_csa20"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 10
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "na_plus___cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa337"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa337"
      minerva_fullName "sodium(1+)"
      minerva_name "Na+"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:29101"
      minerva_ref_type__resource1 "CHEBI__CHEBI:29101"
      minerva_synonyms "Na(+); Na(+); Na+; SODIUM ION"
      minerva_type "Ion"
      minerva_x 2295.0
      minerva_y 2400.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa337"
      species_meta_id "s_id_sa337"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 11
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 1200.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "monocarboxylates_space_transported_space_by_space_slc5a8__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa338"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa338"
      minerva_name "monocarboxylates transported by SLC5A8"
      minerva_type "Simple molecule"
      minerva_x 2675.0
      minerva_y 2440.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa338"
      species_meta_id "s_id_sa338"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 12
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "l_minus_phe__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa16"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa16"
      minerva_fullName "L-phenylalanine zwitterion"
      minerva_name "L-Phe"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:58095"
      minerva_ref_type__resource1 "CHEBI__CHEBI:58095"
      minerva_synonyms "(2S)-2-ammonio-3-phenylpropanoate; L-phenylalanine; phenylalanine"
      minerva_type "Simple molecule"
      minerva_x 1815.0
      minerva_y 1260.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa16"
      species_meta_id "s_id_sa16"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 13
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "acmsd__cell__none__2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa37"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa37"
      minerva_homodimer "2"
      minerva_name "ACMSD"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_001307983"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ACMSD"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ACMSD"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/130013"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/130013"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/19288"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000153086"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q8TDX5"
      minerva_ref_link9 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=4.1.1.45"
      minerva_ref_type__resource1 "REFSEQ__NM_001307983"
      minerva_ref_type__resource2 "HGNC_SYMBOL__ACMSD"
      minerva_ref_type__resource3 "HGNC_SYMBOL__ACMSD"
      minerva_ref_type__resource4 "ENTREZ__130013"
      minerva_ref_type__resource5 "ENTREZ__130013"
      minerva_ref_type__resource6 "HGNC__19288"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000153086"
      minerva_ref_type__resource8 "UNIPROT__Q8TDX5"
      minerva_ref_type__resource9 "EC__4.1.1.45"
      minerva_type "Protein"
      minerva_x 1875.0
      minerva_y 1820.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa37"
      species_meta_id "s_id_sa37"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 14
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ido1__cell__activated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa39"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa39"
      minerva_name "IDO1"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P14902"
      minerva_ref_type__resource1 "UNIPROT__P14902"
      minerva_state1 "Activated"
      minerva_type "Complex"
      minerva_x 1845.0
      minerva_y 840.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa39"
      species_meta_id "s_id_csa39"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 17
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ido2__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa7"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa7"
      minerva_former_symbols "INDOL1"
      minerva_name "IDO2"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IDO2"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IDO2"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000188676"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=1.13.11.-"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/Q6ZQW0"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/27269"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/169355"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/169355"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_194294"
      minerva_ref_type__resource1 "HGNC_SYMBOL__IDO2"
      minerva_ref_type__resource2 "HGNC_SYMBOL__IDO2"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000188676"
      minerva_ref_type__resource4 "EC__1.13.11.-"
      minerva_ref_type__resource5 "UNIPROT__Q6ZQW0"
      minerva_ref_type__resource6 "HGNC__27269"
      minerva_ref_type__resource7 "ENTREZ__169355"
      minerva_ref_type__resource8 "ENTREZ__169355"
      minerva_ref_type__resource9 "REFSEQ__NM_194294"
      minerva_type "Protein"
      minerva_x 2075.0
      minerva_y 810.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa7"
      species_meta_id "s_id_sa7"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 18
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 800.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nadph__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa100"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa100"
      minerva_fullName "NADPH"
      minerva_name "NADPH"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16474"
      minerva_ref_type__resource1 "CHEBI__CHEBI:16474"
      minerva_synonyms "NADPH; NADPH DIHYDRO-NICOTINAMIDE-ADENINE-DINUCLEOTIDE PHOSPHATE; Reduced nicotinamide adenine dinucleotide phosphate; TPNH; dihydronicotinamide-adenine dinucleotide phosphate; reduced nicotinamide-adenine dinucleotide phosphate"
      minerva_type "Simple molecule"
      minerva_x 392.5
      minerva_y 2236.75
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa100"
      species_meta_id "s_id_sa100"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 19
    zlevel -1

    cluster [
      cluster "s_id_ca7"
    ]
    graphics [
      x 900.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "amp__peroxisome__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa288"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa288"
      minerva_fullName "adenosine 5'-monophosphate"
      minerva_name "AMP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16027"
      minerva_ref_type__resource1 "CHEBI__CHEBI:16027"
      minerva_synonyms "5'-AMP; 5'-Adenosine monophosphate; 5'-Adenylic acid; 5'-O-phosphonoadenosine; ADENOSINE MONOPHOSPHATE; AMP; Adenosine 5'-monophosphate; Adenosine 5'-phosphate; Adenosine-5'-monophosphoric acid; Adenylate; Adenylic acid; Ado5'P; PAdo; adenosine 5'-(dihydrogen phosphate); adenosine phosphate; adenosine phosphate; adenosine-5'P; adenosini phosphas; fosfato de adenosina; pA; phosphate d'adenosine"
      minerva_type "Simple molecule"
      minerva_x 610.1428571428572
      minerva_y 3005.2142857142862
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca7"
      compartment_name "peroxisome"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa288"
      species_meta_id "s_id_sa288"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 20
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1000.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "fe2_plus___cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa357"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa357"
      minerva_fullName "iron(2+)"
      minerva_name "Fe2+"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:29033"
      minerva_ref_type__resource1 "CHEBI__CHEBI:29033"
      minerva_synonyms "FE (II) ION; Fe(2+); Fe(II); Fe2+; Ferrous ion; Iron(2+); iron ion(2+)"
      minerva_type "Ion"
      minerva_x 836.75
      minerva_y 1437.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa357"
      species_meta_id "s_id_sa357"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 21
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "kynu__cell__none__2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa34"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa34"
      minerva_homodimer "2"
      minerva_name "KYNU"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/Q16719"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6469"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000115919"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/8942"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_001032998"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/8942"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=KYNU"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=KYNU"
      minerva_ref_link9 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.7.1.3"
      minerva_ref_type__resource1 "UNIPROT__Q16719"
      minerva_ref_type__resource2 "HGNC__6469"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000115919"
      minerva_ref_type__resource4 "ENTREZ__8942"
      minerva_ref_type__resource5 "REFSEQ__NM_001032998"
      minerva_ref_type__resource6 "ENTREZ__8942"
      minerva_ref_type__resource7 "HGNC_SYMBOL__KYNU"
      minerva_ref_type__resource8 "HGNC_SYMBOL__KYNU"
      minerva_ref_type__resource9 "EC__3.7.1.3"
      minerva_type "Protein"
      minerva_x 1875.0
      minerva_y 1470.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa34"
      species_meta_id "s_id_sa34"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 22
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1200.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "h_plus___cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa190"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa381"
      minerva_elementId2 "sa219"
      minerva_elementId3 "sa195"
      minerva_elementId4 "sa190"
      minerva_elementId5 "sa43"
      minerva_elementId6 "sa25"
      minerva_elementId7 "sa235"
      minerva_fullName "hydron"
      minerva_name "H+"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15378"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15378"
      minerva_synonyms "H(+); H+; Hydron"
      minerva_type "Ion"
      minerva_x 1215.0
      minerva_x2 1393.75
      minerva_x3 1785.0
      minerva_x4 1965.0
      minerva_x5 1645.0
      minerva_x6 2005.0
      minerva_x7 1165.0
      minerva_y 1380.0
      minerva_y2 2281.25
      minerva_y3 1990.0
      minerva_y4 2180.0
      minerva_y5 1790.0
      minerva_y6 1250.0
      minerva_y7 2730.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa190"
      species_meta_id "s_id_sa190"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 23
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 200.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "apoa1bp__mitochondrial_space_matrix__none__2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa329"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa329"
      minerva_homodimer "2"
      minerva_name "APOA1BP"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/65220"
      minerva_ref_link2 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.1.23"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/O95544"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NADK"
      minerva_ref_type__resource1 "ENTREZ__65220"
      minerva_ref_type__resource2 "EC__2.7.1.23"
      minerva_ref_type__resource3 "UNIPROT__O95544"
      minerva_ref_type__resource4 "HGNC_SYMBOL__NADK"
      minerva_type "Protein"
      minerva_x 302.5
      minerva_y 1996.75
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa329"
      species_meta_id "s_id_sa329"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 24
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nadph__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa356"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa24"
      minerva_elementId2 "sa356"
      minerva_elementId3 "sa379"
      minerva_fullName "NADPH"
      minerva_name "NADPH"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16474"
      minerva_ref_type__resource1 "CHEBI__CHEBI:16474"
      minerva_synonyms "NADPH; NADPH DIHYDRO-NICOTINAMIDE-ADENINE-DINUCLEOTIDE PHOSPHATE; Reduced nicotinamide adenine dinucleotide phosphate; TPNH; dihydronicotinamide-adenine dinucleotide phosphate; reduced nicotinamide-adenine dinucleotide phosphate"
      minerva_type "Simple molecule"
      minerva_x 2016.0
      minerva_x2 587.0
      minerva_x3 1195.0
      minerva_y 1270.25
      minerva_y2 1449.75
      minerva_y3 1500.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa356"
      species_meta_id "s_id_sa356"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 25
    zlevel -1

    cluster [
      cluster "s_id_ca8"
    ]
    graphics [
      x 400.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "pgg2__endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa321"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa321"
      minerva_fullName "prostaglandin G2"
      minerva_name "PGG2"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:27647"
      minerva_ref_type__resource1 "CHEBI__CHEBI:27647"
      minerva_synonyms "PGG2; Prostaglandin G2"
      minerva_type "Simple molecule"
      minerva_x 1690.0
      minerva_y 3127.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca8"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa321"
      species_meta_id "s_id_sa321"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 26
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "l_minus_tryptophan__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa1"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa1"
      minerva_fullName "L-tryptophan zwitterion"
      minerva_name "L-Tryptophan"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:57912"
      minerva_ref_type__resource1 "CHEBI__CHEBI:57912"
      minerva_synonyms "(2S)-2-ammonio-3-(1H-indol-3-yl)propanoate; L-tryptophan; tryptophan"
      minerva_type "Simple molecule"
      minerva_x 1965.0
      minerva_y 645.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa1"
      species_meta_id "s_id_sa1"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 27
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "citrulline__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa382"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa382"
      minerva_fullName "L-citrulline"
      minerva_name "Citrulline"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16349"
      minerva_ref_type__resource1 "CHEBI__CHEBI:16349"
      minerva_synonyms "(S)-2-Amino-5-ureidopentanoic acid; 2-Amino-5-ureidovaleric acid; CITRULLINE; Cit; Citrulline; L-2-Amino-5-ureidovaleric acid; L-Citrulline; N(5)-(aminocarbonyl)-L-ornithine; N(delta)-carbamylornithine; N5-(Aminocarbonyl)ornithine; N5-carbamoylornithine; alpha-amino-delta-ureidovaleric acid; delta-ureidonorvaline"
      minerva_type "Simple molecule"
      minerva_x 1095.0
      minerva_y 1300.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa382"
      species_meta_id "s_id_sa382"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 28
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nadp_plus___cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa241"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa241"
      minerva_elementId2 "sa23"
      minerva_elementId3 "sa354"
      minerva_elementId4 "sa380"
      minerva_fullName "NADP(+)"
      minerva_name "NADP+"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18009"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18009"
      minerva_synonyms "NADP; NADP+; Nicotinamide adenine dinucleotide phosphate; TPN; Triphosphopyridine nucleotide; beta-Nicotinamide adenine dinucleotide phosphate; beta-nicotinamide adenine dinucleotide phosphate; oxidized nicotinamide-adenine dinucleotide phosphate"
      minerva_type "Simple molecule"
      minerva_x 1438.0
      minerva_x2 2035.0
      minerva_x3 783.5
      minerva_x4 1215.0
      minerva_y 2930.25
      minerva_y2 1350.0
      minerva_y3 1463.25
      minerva_y4 1340.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa241"
      species_meta_id "s_id_sa241"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 29
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 800.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "h2o__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa257"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa261"
      minerva_elementId2 "sa255"
      minerva_elementId3 "sa257"
      minerva_fullName "water"
      minerva_name "H2O"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15377"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15377"
      minerva_synonyms "BOUND WATER; H2O; H2O; HOH; WATER; Wasser; Water; [OH2]; acqua; agua; aqua; dihydridooxygen; dihydrogen oxide; eau; hydrogen hydroxide"
      minerva_type "Simple molecule"
      minerva_x 2675.0
      minerva_x2 2355.0
      minerva_x3 2475.0
      minerva_y 590.0
      minerva_y2 350.0
      minerva_y3 390.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa257"
      species_meta_id "s_id_sa257"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 30
    zlevel -1

    cluster [
      cluster "s_id_ca7"
    ]
    graphics [
      x 900.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nudt12__peroxisome__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa289"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa289"
      minerva_name "NUDT12"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_031438"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NUDT12"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/83594"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/83594"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/Q9BQG2"
      minerva_ref_link5 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.6.1.-"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000112874"
      minerva_ref_link7 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.6.1.22"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/18826"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NUDT12"
      minerva_ref_type__resource1 "REFSEQ__NM_031438"
      minerva_ref_type__resource10 "HGNC_SYMBOL__NUDT12"
      minerva_ref_type__resource2 "ENTREZ__83594"
      minerva_ref_type__resource3 "ENTREZ__83594"
      minerva_ref_type__resource4 "UNIPROT__Q9BQG2"
      minerva_ref_type__resource5 "EC__3.6.1.-"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000112874"
      minerva_ref_type__resource7 "EC__3.6.1.22"
      minerva_ref_type__resource8 "HGNC__18826"
      minerva_ref_type__resource9 "HGNC_SYMBOL__NUDT12"
      minerva_type "Protein"
      minerva_x 712.5
      minerva_y 2991.25
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca7"
      compartment_name "peroxisome"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa289"
      species_meta_id "s_id_sa289"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 31
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 1000.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nadk2__mitochondrial_space_matrix__none__2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa324"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa324"
      minerva_former_symbols "C5orf33; NADKD1"
      minerva_homodimer "2"
      minerva_name "NADK2"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/133686"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/133686"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_153013"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000152620"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/26404"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NADK2"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NADK2"
      minerva_ref_link8 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.1.23"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q4G0N4"
      minerva_ref_type__resource1 "ENTREZ__133686"
      minerva_ref_type__resource2 "ENTREZ__133686"
      minerva_ref_type__resource3 "REFSEQ__NM_153013"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000152620"
      minerva_ref_type__resource5 "HGNC__26404"
      minerva_ref_type__resource6 "HGNC_SYMBOL__NADK2"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NADK2"
      minerva_ref_type__resource8 "EC__2.7.1.23"
      minerva_ref_type__resource9 "UNIPROT__Q4G0N4"
      minerva_type "Protein"
      minerva_x 422.5
      minerva_y 2366.75
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa324"
      species_meta_id "s_id_sa324"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 32
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nar__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa224"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa224"
      minerva_fullName "D-ribosylnicotinate"
      minerva_name "NAR"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:58527"
      minerva_ref_type__resource1 "CHEBI__CHEBI:58527"
      minerva_synonyms "beta-D-ribosylnicotinate"
      minerva_type "Simple molecule"
      minerva_x 1085.0
      minerva_y 2240.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa224"
      species_meta_id "s_id_sa224"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 33
    zlevel -1

    cluster [
      cluster "s_id_ca6"
    ]
    graphics [
      x 1200.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ppi(3_minus_)__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa292"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa292"
      minerva_fullName "diphosphate(3-)"
      minerva_name "PPi(3-)"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:33019"
      minerva_ref_type__resource1 "CHEBI__CHEBI:33019"
      minerva_synonyms "HP2O7(3-); diphosphate"
      minerva_type "Simple molecule"
      minerva_x 589.25
      minerva_y 885.046875
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca6"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa292"
      species_meta_id "s_id_sa292"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 34
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "3haa__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa31"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa31"
      minerva_fullName "3-hydroxyanthranilic acid"
      minerva_name "3HAA"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15793"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15793"
      minerva_synonyms "2-Amino-3-hydroxy-benzoic acid; 2-Amino-3-hydroxybenzoic acid; 3-Hydroxyanthranilic acid; 3-Ohaa; 3-Oxyanthranilic acid"
      minerva_type "Simple molecule"
      minerva_x 1965.0
      minerva_y 1570.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa31"
      species_meta_id "s_id_sa31"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 35
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mna__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa220"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa220"
      minerva_fullName "1-methylnicotinamide"
      minerva_name "MNA"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16797"
      minerva_ref_type__resource1 "CHEBI__CHEBI:16797"
      minerva_synonyms "1-Methylnicotinamide; 1-methylnicotinamide; 1-methylnicotinamide cation; 3-(Aminocarbonyl)-1-methylpyridinium; N(1)-Methylnicotinamide; Trigonellinamide"
      minerva_type "Simple molecule"
      minerva_x 2182.5
      minerva_y 2673.75
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa220"
      species_meta_id "s_id_sa220"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 36
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "i3lact__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa14"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa14"
      minerva_name "I3LACT"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:55528"
      minerva_ref_type__resource1 "CHEBI__CHEBI:55528"
      minerva_type "Protein"
      minerva_x 1795.0
      minerva_y 1050.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa14"
      species_meta_id "s_id_sa14"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 37
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nad_plus___cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa222"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa45"
      minerva_elementId2 "sa222"
      minerva_fullName "NAD(1-)"
      minerva_name "NAD+"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:57540"
      minerva_ref_type__resource1 "CHEBI__CHEBI:57540"
      minerva_synonyms "NAD anion; NAD(+)"
      minerva_type "Simple molecule"
      minerva_x 1735.0
      minerva_x2 1435.0
      minerva_y 1790.0
      minerva_y2 2670.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa222"
      species_meta_id "s_id_sa222"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 38
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "2am__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa49"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa49"
      minerva_fullName "2-aminomuconic acid"
      minerva_name "2AM"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16886"
      minerva_ref_type__resource1 "CHEBI__CHEBI:16886"
      minerva_synonyms "2-Aminomuconate; 2-Aminomuconic acid; o-Aminomuconate"
      minerva_type "Simple molecule"
      minerva_x 1615.0
      minerva_y 1750.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa49"
      species_meta_id "s_id_sa49"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 39
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nmrk1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa236"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa236"
      minerva_former_symbols "C9orf95"
      minerva_name "NMRK1"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000106733"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NMRK1"
      minerva_ref_link2 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.1.173"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/Q9NWW6"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/54981"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_017881"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/54981"
      minerva_ref_link7 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.1.22"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/26057"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NMRK1"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000106733"
      minerva_ref_type__resource10 "HGNC_SYMBOL__NMRK1"
      minerva_ref_type__resource2 "EC__2.7.1.173"
      minerva_ref_type__resource3 "UNIPROT__Q9NWW6"
      minerva_ref_type__resource4 "ENTREZ__54981"
      minerva_ref_type__resource5 "REFSEQ__NM_017881"
      minerva_ref_type__resource6 "ENTREZ__54981"
      minerva_ref_type__resource7 "EC__2.7.1.22"
      minerva_ref_type__resource8 "HGNC__26057"
      minerva_ref_type__resource9 "HGNC_SYMBOL__NMRK1"
      minerva_type "Protein"
      minerva_x 1135.0
      minerva_y 2410.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa236"
      species_meta_id "s_id_sa236"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 40
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 800.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "prpp__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa202"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa202"
      minerva_elementId2 "sa210"
      minerva_elementId3 "sa187"
      minerva_fullName "5-O-phosphono-alpha-D-ribofuranosyl diphosphate"
      minerva_name "PRPP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17111"
      minerva_ref_type__resource1 "CHEBI__CHEBI:17111"
      minerva_synonyms "5-Phospho-alpha-D-ribose 1-diphosphate; 5-Phosphoribosyl 1-pyrophosphate; 5-Phosphoribosyl diphosphate; ALPHA-PHOSPHORIBOSYLPYROPHOSPHORIC ACID; PRPP; PRib-PP; alpha-D-ribofuranose 5-(dihydrogen phosphate) 1-(trihydrogen diphosphate); phosphoribosyl pyrophosphate; phosphoribosylpyrophosphate"
      minerva_type "Simple molecule"
      minerva_x 1745.0
      minerva_x2 1795.0
      minerva_x3 1995.0
      minerva_y 1990.0
      minerva_y2 2600.0
      minerva_y3 2190.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa202"
      species_meta_id "s_id_sa202"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 41
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 900.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "regulatory_space_t_minus_cell_space_generation__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa35"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa35"
      minerva_fullName "regulatory T cell differentiation"
      minerva_name "Regulatory T-cell generation"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0045066"
      minerva_ref_type__resource1 "GO__GO:0045066"
      minerva_type "Phenotype"
      minerva_x 2555.0
      minerva_y 1250.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa35"
      species_meta_id "s_id_sa35"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 42
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 1000.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "namn__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa95"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa95"
      minerva_fullName "nicotinic acid D-ribonucleotide"
      minerva_name "NAMN"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15763"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15763"
      minerva_synonyms "Nicotinate D-ribonucleotide; Nicotinate ribonucleotide; Nicotinic acid ribonucleotide; beta-Nicotinate D-ribonucleotide"
      minerva_type "Simple molecule"
      minerva_x 672.5
      minerva_y 1995.75
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa95"
      species_meta_id "s_id_sa95"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 43
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nam__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa221"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa221"
      minerva_fullName "nicotinamide"
      minerva_name "NAM"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17154"
      minerva_ref_type__resource1 "CHEBI__CHEBI:17154"
      minerva_synonyms "3-pyridinecarboxamide; NICOTINAMIDE; Niacinamide; Nicotinamid; Nicotinamide; Nicotinic acid amide; Nicotinsaeureamid; Nikotinamid; Nikotinsaeureamid; Vitamin PP; beta-pyridinecarboxamide; niacin; nicotinamide; vitamin B3"
      minerva_type "Simple molecule"
      minerva_x 1825.0
      minerva_y 2670.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa221"
      species_meta_id "s_id_sa221"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 44
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1200.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nmrk2__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa215"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa215"
      minerva_elementId2 "sa237"
      minerva_former_symbols "ITGB1BP3"
      minerva_name "NMRK2"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/17871"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NMRK2"
      minerva_ref_link11 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NMRK2"
      minerva_ref_link2 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.1.173"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_014446"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000077009"
      minerva_ref_link5 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.1.22"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/27231"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/27231"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q9NPI5"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_170678"
      minerva_ref_type__resource1 "HGNC__17871"
      minerva_ref_type__resource10 "HGNC_SYMBOL__NMRK2"
      minerva_ref_type__resource11 "HGNC_SYMBOL__NMRK2"
      minerva_ref_type__resource2 "EC__2.7.1.173"
      minerva_ref_type__resource3 "REFSEQ__NM_014446"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000077009"
      minerva_ref_type__resource5 "EC__2.7.1.22"
      minerva_ref_type__resource6 "ENTREZ__27231"
      minerva_ref_type__resource7 "ENTREZ__27231"
      minerva_ref_type__resource8 "UNIPROT__Q9NPI5"
      minerva_ref_type__resource9 "REFSEQ__NM_170678"
      minerva_type "Protein"
      minerva_x 1265.0
      minerva_x2 1075.0
      minerva_y 2170.0
      minerva_y2 2750.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa215"
      species_meta_id "s_id_sa215"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 45
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nfk__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa11"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa11"
      minerva_fullName "N-formylkynurenine"
      minerva_name "NFK"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18377"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18377"
      minerva_synonyms "3-(2-formamidobenzoyl)alanine; Formylkynurenine; N-Formylkynurenine"
      minerva_type "Simple molecule"
      minerva_x 1965.0
      minerva_y 970.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa11"
      species_meta_id "s_id_sa11"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 46
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "afmid__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa18"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa18"
      minerva_name "AFMID"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/20910"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_001145526"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.5.1.9"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/Q63HM1"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AFMID"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/125061"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000183077"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AFMID"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/125061"
      minerva_ref_type__resource1 "HGNC__20910"
      minerva_ref_type__resource2 "REFSEQ__NM_001145526"
      minerva_ref_type__resource3 "EC__3.5.1.9"
      minerva_ref_type__resource4 "UNIPROT__Q63HM1"
      minerva_ref_type__resource5 "HGNC_SYMBOL__AFMID"
      minerva_ref_type__resource6 "ENTREZ__125061"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000183077"
      minerva_ref_type__resource8 "HGNC_SYMBOL__AFMID"
      minerva_ref_type__resource9 "ENTREZ__125061"
      minerva_type "Protein"
      minerva_x 2045.0
      minerva_y 1040.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa18"
      species_meta_id "s_id_sa18"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 47
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nmnat2:mg2_plus___cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa16"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa16"
      minerva_name "NMNAT2:Mg2+"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/Q9BZQ4"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "UNIPROT__Q9BZQ4"
      minerva_ref_type__resource2 "CHEBI__CHEBI:18420"
      minerva_type "Complex"
      minerva_x 1265.0
      minerva_y 2420.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa16"
      species_meta_id "s_id_csa16"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 48
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 500.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "pi__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa105"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa105"
      minerva_fullName "phosphate(3-)"
      minerva_name "Pi"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18367"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18367"
      minerva_synonyms "Orthophosphate; PHOSPHATE ION; PO4(3-); Phosphate; [PO4](3-)"
      minerva_type "Simple molecule"
      minerva_x 319.25
      minerva_y 2219.625
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa105"
      species_meta_id "s_id_sa105"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 49
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "(adp_minus_d_minus_ribosyl)(n_plus_1)_minus_acceptor__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa234"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa234"
      minerva_fullName "(ADP-D-ribosyl)(n+1)-acceptor"
      minerva_name "(ADP-D-ribosyl)(n+1)-acceptor"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:133203"
      minerva_ref_type__resource1 "CHEBI__CHEBI:133203"
      minerva_type "Simple molecule"
      minerva_x 1745.0
      minerva_y 2720.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa234"
      species_meta_id "s_id_sa234"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 50
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "atp__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa227"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa204"
      minerva_elementId2 "sa227"
      minerva_elementId3 "sa239"
      minerva_elementId4 "sa218"
      minerva_elementId5 "sa229"
      minerva_fullName "ATP(4-)"
      minerva_name "ATP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:30616"
      minerva_ref_type__resource1 "CHEBI__CHEBI:30616"
      minerva_synonyms "ATP; atp"
      minerva_type "Simple molecule"
      minerva_x 1390.5
      minerva_x2 1411.5
      minerva_x3 985.0
      minerva_x4 1165.0
      minerva_x5 1273.25
      minerva_y 2494.125
      minerva_y2 2753.125
      minerva_y3 2710.0
      minerva_y4 2280.0
      minerva_y5 2721.875
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa227"
      species_meta_id "s_id_sa227"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 51
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 800.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "pyr__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa20"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa20"
      minerva_fullName "pyruvate"
      minerva_name "PYR"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15361"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15361"
      minerva_synonyms "2-oxopropanoate; 2-oxopropanoic acid, ion(1-); pyruvate"
      minerva_type "Simple molecule"
      minerva_x 1875.0
      minerva_y 1250.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa20"
      species_meta_id "s_id_sa20"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 52
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nt5e:zn2_plus___cell__none__2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa35"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa35"
      minerva_homodimer "2"
      minerva_name "NT5E:Zn2+"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:29105"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P21589"
      minerva_ref_type__resource1 "CHEBI__CHEBI:29105"
      minerva_ref_type__resource2 "UNIPROT__P21589"
      minerva_type "Complex"
      minerva_x 2385.0
      minerva_y 640.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa35"
      species_meta_id "s_id_csa35"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 53
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 1000.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "atp__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa94"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa103"
      minerva_elementId2 "sa107"
      minerva_elementId3 "sa94"
      minerva_fullName "ATP(4-)"
      minerva_name "ATP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:30616"
      minerva_ref_type__resource1 "CHEBI__CHEBI:30616"
      minerva_synonyms "ATP; atp"
      minerva_type "Simple molecule"
      minerva_x 359.0
      minerva_x2 320.0
      minerva_x3 727.5
      minerva_y 2096.0
      minerva_y2 2357.75
      minerva_y3 2036.75
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa94"
      species_meta_id "s_id_sa94"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 54
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "naad__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa200"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa200"
      minerva_fullName "deamido-NAD(+)"
      minerva_name "NAAD"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18304"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18304"
      minerva_synonyms "Deamido-NAD; Deamido-NAD+; Deamino-NAD+"
      minerva_type "Simple molecule"
      minerva_x 1435.0
      minerva_y 2420.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa200"
      species_meta_id "s_id_sa200"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 55
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1200.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "(adp_minus_d_minus_ribosyl)(n)_minus_acceptor__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa233"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa233"
      minerva_fullName "(ADP-D-ribosyl)(n)-acceptor"
      minerva_name "(ADP-D-ribosyl)(n)-acceptor"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:133202"
      minerva_ref_type__resource1 "CHEBI__CHEBI:133202"
      minerva_type "Simple molecule"
      minerva_x 1535.0
      minerva_y 2720.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa233"
      species_meta_id "s_id_sa233"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 56
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ppi(3_minus_)__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa228"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa203"
      minerva_elementId2 "sa228"
      minerva_fullName "diphosphate(3-)"
      minerva_name "PPi(3-)"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:33019"
      minerva_ref_type__resource1 "CHEBI__CHEBI:33019"
      minerva_synonyms "HP2O7(3-); diphosphate"
      minerva_type "Simple molecule"
      minerva_x 1395.5
      minerva_x2 1385.75
      minerva_y 2389.25
      minerva_y2 2720.375
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa228"
      species_meta_id "s_id_sa228"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 57
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 300.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nca__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa271"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa271"
      minerva_fullName "nicotinate"
      minerva_name "NCA"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:32544"
      minerva_ref_type__resource1 "CHEBI__CHEBI:32544"
      minerva_synonyms "3-pyridinecarboxylate; nicotinate"
      minerva_type "Simple molecule"
      minerva_x 2615.0
      minerva_y 2230.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa271"
      species_meta_id "s_id_sa271"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 58
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "adp__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa217"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa226"
      minerva_elementId2 "sa217"
      minerva_elementId3 "sa240"
      minerva_fullName "ADP(3-)"
      minerva_name "ADP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:456216"
      minerva_ref_type__resource1 "CHEBI__CHEBI:456216"
      minerva_synonyms "5'-O-[(phosphonatooxy)phosphinato]adenosine; ADP; ADP trianion"
      minerva_type "Simple molecule"
      minerva_x 1355.0
      minerva_x2 1185.0
      minerva_x3 1402.75
      minerva_y 2290.0
      minerva_y2 2620.0
      minerva_y3 2896.125
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa217"
      species_meta_id "s_id_sa217"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 59
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "monocarboxylates_space_transported_space_by_space_slc5a8__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa334"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa334"
      minerva_name "monocarboxylates transported by SLC5A8"
      minerva_type "Simple molecule"
      minerva_x 2115.0
      minerva_y 2440.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa334"
      species_meta_id "s_id_sa334"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 62
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 800.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "acs__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa38"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa38"
      minerva_fullName "2-amino-3-(3-oxoprop-1-enyl)but-2-enedioate"
      minerva_name "ACS"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:29044"
      minerva_ref_type__resource1 "CHEBI__CHEBI:29044"
      minerva_type "Simple molecule"
      minerva_x 1965.0
      minerva_y 1750.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa38"
      species_meta_id "s_id_sa38"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 63
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 900.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "adp_minus_ribose__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa258"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa258"
      minerva_fullName "ADP-D-ribose(2-)"
      minerva_name "ADP-ribose"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:57967"
      minerva_ref_type__resource1 "CHEBI__CHEBI:57967"
      minerva_synonyms "ADP-D-ribose; D-ribofuranos-5-yl-ADP dianion"
      minerva_type "Simple molecule"
      minerva_x 2115.0
      minerva_y 420.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa258"
      species_meta_id "s_id_sa258"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 64
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 1000.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "th1_space_cell_space_cytokine_space_production__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa374"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa374"
      minerva_fullName "T-helper 1 cell cytokine production"
      minerva_name "Th1 cell cytokine production"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0035744"
      minerva_ref_type__resource1 "GO__GO:0035744"
      minerva_type "Phenotype"
      minerva_x 2535.0
      minerva_y 1700.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa374"
      species_meta_id "s_id_sa374"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 65
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "biliverdin__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa361"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa361"
      minerva_fullName "biliverdin"
      minerva_name "Biliverdin"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17033"
      minerva_ref_type__resource1 "CHEBI__CHEBI:17033"
      minerva_synonyms "8,12-bis(2-carboxyethyl)-2,7,13,17-tetramethyl-3,18-divinylbilin-1(19)(21H,24H)-dione; BILIVERDINE IX ALPHA; Biliverdin; Biliverdin IX alpha; Biliverdin IXalpha; Biliverdine"
      minerva_type "Simple molecule"
      minerva_x 865.0
      minerva_y 1395.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa361"
      species_meta_id "s_id_sa361"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 66
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 1200.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "h2o2__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa254"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa254"
      minerva_fullName "hydrogen peroxide"
      minerva_name "H2O2"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16240"
      minerva_ref_type__resource1 "CHEBI__CHEBI:16240"
      minerva_synonyms "H2O2; H2O2; H2O2; HOOH; HYDROGEN PEROXIDE; Hydrogen peroxide; Oxydol; [OH(OH)]; dihydrogen dioxide; perhydrol"
      minerva_type "Simple molecule"
      minerva_x 2344.0
      minerva_y 248.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa254"
      species_meta_id "s_id_sa254"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 67
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "i3propa__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa13"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa13"
      minerva_name "I3PROPA"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:43580"
      minerva_ref_type__resource1 "CHEBI__CHEBI:43580"
      minerva_type "Protein"
      minerva_x 1885.0
      minerva_y 1110.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa13"
      species_meta_id "s_id_sa13"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 68
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ahr__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa26"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa26"
      minerva_name "AHR"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/348"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AHR"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AHR"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000106546"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P35869"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_001621"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/196"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/196"
      minerva_ref_type__resource1 "HGNC__348"
      minerva_ref_type__resource2 "HGNC_SYMBOL__AHR"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AHR"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000106546"
      minerva_ref_type__resource5 "UNIPROT__P35869"
      minerva_ref_type__resource6 "REFSEQ__NM_001621"
      minerva_ref_type__resource7 "ENTREZ__196"
      minerva_ref_type__resource8 "ENTREZ__196"
      minerva_type "Protein"
      minerva_x 2035.0
      minerva_y 1120.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa26"
      species_meta_id "s_id_sa26"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 69
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nnmt__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa214"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa214"
      minerva_name "NNMT"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000166741"
      minerva_ref_link2 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.1.1.1"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7861"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_006169"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/4837"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/4837"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NNMT"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NNMT"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P40261"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000166741"
      minerva_ref_type__resource2 "EC__2.1.1.1"
      minerva_ref_type__resource3 "HGNC__7861"
      minerva_ref_type__resource4 "REFSEQ__NM_006169"
      minerva_ref_type__resource5 "ENTREZ__4837"
      minerva_ref_type__resource6 "ENTREZ__4837"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NNMT"
      minerva_ref_type__resource8 "HGNC_SYMBOL__NNMT"
      minerva_ref_type__resource9 "UNIPROT__P40261"
      minerva_type "Protein"
      minerva_x 2035.0
      minerva_y 2590.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa214"
      species_meta_id "s_id_sa214"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 70
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "l_minus_arginine__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa378"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa378"
      minerva_fullName "L-arginine"
      minerva_name "L-Arginine"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16467"
      minerva_ref_type__resource1 "CHEBI__CHEBI:16467"
      minerva_synonyms "(2S)-2-amino-5-(carbamimidamido)pentanoic acid; (2S)-2-amino-5-guanidinopentanoic acid; (S)-2-Amino-5-guanidinovaleric acid; (S)-2-amino-5-guanidinopentanoic acid; Arg; L-(+)-arginine; L-Arg; L-Arginin; L-Arginine; R; arginine"
      minerva_type "Simple molecule"
      minerva_x 1095.0
      minerva_y 1520.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa378"
      species_meta_id "s_id_sa378"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 71
    zlevel -1

    cluster [
      cluster "s_id_ca8"
    ]
    graphics [
      x 600.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "pgh2__endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa317"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa317"
      minerva_fullName "prostaglandin H2"
      minerva_name "PGH2"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15554"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15554"
      minerva_synonyms "(5Z,13E)-(15S)-9alpha,11alpha-Epidioxy-15-hydroxyprosta-5,13-dienoate; (5Z,13E,15S)-9alpha,11alpha-Epidioxy-15-hydroxyprosta-5,13-dienoate; (5Z,9alpha,11alpha,13E,15S)-9,11-epidioxy-15-hydroxyprosta-5,13-dien-1-oic acid; 9,11-Epoxymethano-pgh2; PGH2; PGH2; Prostaglandin H2"
      minerva_type "Simple molecule"
      minerva_x 2000.0
      minerva_y 3127.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca8"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa317"
      species_meta_id "s_id_sa317"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 72
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "adomet__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa232"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa232"
      minerva_fullName "S-adenosyl-L-methionine"
      minerva_name "AdoMet"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15414"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15414"
      minerva_synonyms "(3S)-5'-[(3-amino-3-carboxypropyl)methylsulfonio]-5'-deoxyadenosine, inner salt; Acylcarnitine; AdoMet; S-(5'-deoxyadenosin-5'-yl)-L-methionine; S-Adenosyl-L-methionine; S-Adenosylmethionine; S-adenosyl-L-methionine; S-adenosylmethionine; SAM; SAMe; [1-(adenin-9-yl)-1,5-dideoxy-beta-D-ribofuranos-5-yl][(3S)-3-amino-3-carboxypropyl](methyl)sulfonium"
      minerva_type "Simple molecule"
      minerva_x 1925.0
      minerva_y 2700.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa232"
      species_meta_id "s_id_sa232"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 73
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 800.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "t_minus_cell_space_apoptosis__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa375"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa375"
      minerva_fullName "T cell apoptotic process"
      minerva_name "T-cell apoptosis"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0070231"
      minerva_ref_type__resource1 "GO__GO:0070231"
      minerva_type "Phenotype"
      minerva_x 2525.0
      minerva_y 1420.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa375"
      species_meta_id "s_id_sa375"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 74
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "pxlp_minus_kyat1__cell__none__2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa1"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa1"
      minerva_homodimer "2"
      minerva_name "PXLP-KYAT1"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/Q16773"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18405"
      minerva_ref_type__resource1 "UNIPROT__Q16773"
      minerva_ref_type__resource2 "CHEBI__CHEBI:18405"
      minerva_type "Complex"
      minerva_x 1685.0
      minerva_y 1080.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa1"
      species_meta_id "s_id_csa1"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 75
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1000.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nampt__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa206"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa206"
      minerva_former_symbols "PBEF1"
      minerva_name "NAMPT"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NAMPT"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NAMPT"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.4.2.12"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/30092"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P43490"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/10135"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_182790"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/10135"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000105835"
      minerva_ref_type__resource1 "HGNC_SYMBOL__NAMPT"
      minerva_ref_type__resource2 "HGNC_SYMBOL__NAMPT"
      minerva_ref_type__resource3 "EC__2.4.2.12"
      minerva_ref_type__resource4 "HGNC__30092"
      minerva_ref_type__resource5 "UNIPROT__P43490"
      minerva_ref_type__resource6 "ENTREZ__10135"
      minerva_ref_type__resource7 "REFSEQ__NM_182790"
      minerva_ref_type__resource8 "ENTREZ__10135"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000105835"
      minerva_type "Protein"
      minerva_x 1915.0
      minerva_y 2550.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa206"
      species_meta_id "s_id_sa206"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 76
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 1100.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "dh_minus_beta_minus_nad__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa249"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa249"
      minerva_fullName "6-hydro-beta-NAD"
      minerva_name "dh-beta-NAD"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:90174"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:90171"
      minerva_ref_type__resource1 "CHEBI__CHEBI:90174"
      minerva_ref_type__resource2 "CHEBI__CHEBI:90171"
      minerva_synonyms "6-HNAD; 6-NADH; 6-hydro-beta-nicotinamide adenine dinucleotide"
      minerva_type "Simple molecule"
      minerva_x 2056.0
      minerva_y 251.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa249"
      species_meta_id "s_id_sa249"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 77
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 1200.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nrnam__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa263"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa263"
      minerva_fullName "N-ribosylnicotinamide"
      minerva_name "NRNAM"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15927"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15927"
      minerva_synonyms "1-(beta-D-Ribofuranosyl)nicotinamide; N-Ribosylnicotinamide; Nicotinamide riboside; beta-nicotinamide D-riboside; nicotinamide ribonucleoside; nicotinamide ribose; nicotinamide-beta-riboside"
      minerva_type "Simple molecule"
      minerva_x 2675.0
      minerva_y 720.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa263"
      species_meta_id "s_id_sa263"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 78
    zlevel -1

    cluster [
      cluster "s_id_ca6"
    ]
    graphics [
      x 200.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ifn_minus_g__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa62"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa62"
      minerva_name "IFN-G"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/Q14609"
      minerva_ref_type__resource1 "UNIPROT__Q14609"
      minerva_type "Protein"
      minerva_x 848.310546875
      minerva_y 788.9453125
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca6"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa62"
      species_meta_id "s_id_sa62"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 79
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "heme__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa355"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa355"
      minerva_fullName "heme"
      minerva_name "Heme"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:30413"
      minerva_ref_type__resource1 "CHEBI__CHEBI:30413"
      minerva_synonyms "Haem; haem; haem; haeme; hem; heme; heme; hemos"
      minerva_type "Simple molecule"
      minerva_x 523.5
      minerva_y 1393.25
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa355"
      species_meta_id "s_id_sa355"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 80
    zlevel -1

    cluster [
      cluster "s_id_ca6"
    ]
    graphics [
      x 400.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nf_minus_kb_space_complex__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa36"
      type "text"
    ]
    minerva [
      minerva_activity "true"
      minerva_elementId "csa38"
      minerva_elementId2 "csa36"
      minerva_name "Nf-KB Complex"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/Q00653"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P19838"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/Q04206"
      minerva_ref_type__resource1 "UNIPROT__Q00653"
      minerva_ref_type__resource2 "UNIPROT__P19838"
      minerva_ref_type__resource3 "UNIPROT__Q04206"
      minerva_type "Complex"
      minerva_x 448.310546875
      minerva_x2 698.310546875
      minerva_y 1108.9453125
      minerva_y2 1108.9453125
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca6"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa36"
      species_meta_id "s_id_csa36"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 81
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "o2__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa4"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa358"
      minerva_elementId2 "sa22"
      minerva_elementId3 "sa39"
      minerva_elementId4 "sa4"
      minerva_elementId5 "sa383"
      minerva_fullName "dioxygen"
      minerva_name "O2"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15379"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15379"
      minerva_synonyms "Disauerstoff; E 948; E-948; E948; O2; O2; O2; OXYGEN MOLECULE; Oxygen; [OO]; dioxygene; molecular oxygen"
      minerva_type "Simple molecule"
      minerva_x 547.0
      minerva_x2 1985.0
      minerva_x3 1925.0
      minerva_x4 2015.0
      minerva_x5 1205.0
      minerva_y 1434.25
      minerva_y2 1240.0
      minerva_y3 1610.0
      minerva_y4 705.0
      minerva_y5 1460.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa4"
      species_meta_id "s_id_sa4"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 82
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "kyna__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa28"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa28"
      minerva_fullName "kynurenic acid"
      minerva_name "KYNA"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18344"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18344"
      minerva_synonyms "4-Hydroxy-2-chinolincarbonsaeure; 4-Hydroxy-2-quinolinecarboxylic acid; 4-hydroxy-2-quinolinecarboxylic acid; 4-hydroxyquinaldic acid; 4-hydroxyquinaldinic acid; Kynurenate; Kynurenic acid; Kynurensaeure"
      minerva_type "Simple molecule"
      minerva_x 1525.0
      minerva_y 1200.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa28"
      species_meta_id "s_id_sa28"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 83
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nca__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa189"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa189"
      minerva_fullName "nicotinate"
      minerva_name "NCA"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:32544"
      minerva_ref_type__resource1 "CHEBI__CHEBI:32544"
      minerva_synonyms "3-pyridinecarboxylate; nicotinate"
      minerva_type "Simple molecule"
      minerva_x 2155.0
      minerva_y 2230.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa189"
      species_meta_id "s_id_sa189"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 84
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 800.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "co2__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa40"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa40"
      minerva_elementId2 "sa201"
      minerva_fullName "carbon dioxide"
      minerva_name "CO2"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16526"
      minerva_ref_type__resource1 "CHEBI__CHEBI:16526"
      minerva_synonyms "CARBON DIOXIDE; CO2; CO2; Carbon dioxide; E 290; E-290; E290; R-744; [CO2]; carbonic anhydride"
      minerva_type "Simple molecule"
      minerva_x 1805.0
      minerva_x2 1565.0
      minerva_y 1720.0
      minerva_y2 2110.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa40"
      species_meta_id "s_id_sa40"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 85
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ccbl2__cell__none__2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa19"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa19"
      minerva_homodimer "2"
      minerva_name "CCBL2"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/Q6YP21"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/56267"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=4.4.1.13"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=KYAT3"
      minerva_ref_link5 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.6.1.7"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.6.1.63"
      minerva_ref_type__resource1 "UNIPROT__Q6YP21"
      minerva_ref_type__resource2 "ENTREZ__56267"
      minerva_ref_type__resource3 "EC__4.4.1.13"
      minerva_ref_type__resource4 "HGNC_SYMBOL__KYAT3"
      minerva_ref_type__resource5 "EC__2.6.1.7"
      minerva_ref_type__resource6 "EC__2.6.1.63"
      minerva_type "Protein"
      minerva_x 1865.0
      minerva_y 1410.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa19"
      species_meta_id "s_id_sa19"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 86
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1000.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nadh__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa44"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa44"
      minerva_fullName "NADH(2-)"
      minerva_name "NADH"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:57945"
      minerva_ref_type__resource1 "CHEBI__CHEBI:57945"
      minerva_synonyms "NADH; NADH dianion"
      minerva_type "Simple molecule"
      minerva_x 1575.0
      minerva_y 1790.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa44"
      species_meta_id "s_id_sa44"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 87
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 1100.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "r_minus_nadphx__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa102"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa102"
      minerva_fullName "(R)-NADPHX"
      minerva_name "R-NADPHX"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:64085"
      minerva_ref_type__resource1 "CHEBI__CHEBI:64085"
      minerva_synonyms "(6R)-6beta-hydroxy-1,4,5,6-tetrahydronicotinamide adenine dinucleotide phosphate"
      minerva_type "Simple molecule"
      minerva_x 392.5
      minerva_y 1956.75
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa102"
      species_meta_id "s_id_sa102"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 88
    zlevel -1

    cluster [
      cluster "s_id_ca7"
    ]
    graphics [
      x 1200.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nmnh__peroxisome__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa285"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa285"
      minerva_fullName "NMNH"
      minerva_name "NMNH"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:74452"
      minerva_ref_type__resource1 "CHEBI__CHEBI:74452"
      minerva_type "Simple molecule"
      minerva_x 836.7857142857147
      minerva_y 3054.428571428572
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca7"
      compartment_name "peroxisome"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa285"
      species_meta_id "s_id_sa285"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 89
    zlevel -1

    cluster [
      cluster "s_id_ca8"
    ]
    graphics [
      x 200.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "pgi2__endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa316"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa316"
      minerva_fullName "prostaglandin I2"
      minerva_name "PGI2"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15552"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15552"
      minerva_synonyms "(5Z,13E)-(15S)-6,9alpha-Epoxy-11alpha,15-dihydroxyprosta-5,13-dienoate; (5Z,13E)-(15S)-6,9alpha-Epoxy-11alpha,15-dihydroxyprosta-5,13-dienoate; (5Z,9alpha,11alpha,13E,15S)-6,9-epoxy-11,15-dihydroxyprosta-5,13-dien-1-oic acid; Epoprostenol; Flolan; PGI2; PGI2; PGX; Prostacyclin; Prostaglandin I2; Vasocyclin; prostaglandin X"
      minerva_type "Simple molecule"
      minerva_x 2160.0
      minerva_y 3127.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca8"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa316"
      species_meta_id "s_id_sa316"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 90
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 300.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "h_plus___human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa251"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa252"
      minerva_elementId2 "sa251"
      minerva_fullName "hydron"
      minerva_name "H+"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15378"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15378"
      minerva_synonyms "H(+); H+; Hydron"
      minerva_type "Ion"
      minerva_x 2175.0
      minerva_x2 2155.0
      minerva_y 330.0
      minerva_y2 290.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa251"
      species_meta_id "s_id_sa251"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 91
    zlevel -1

    cluster [
      cluster "s_id_ca6"
    ]
    graphics [
      x 400.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "atp__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa293"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa293"
      minerva_fullName "ATP(4-)"
      minerva_name "ATP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:30616"
      minerva_ref_type__resource1 "CHEBI__CHEBI:30616"
      minerva_synonyms "ATP; atp"
      minerva_type "Simple molecule"
      minerva_x 435.0
      minerva_y 885.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca6"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa293"
      species_meta_id "s_id_sa293"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 92
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "namn__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa199"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa199"
      minerva_fullName "nicotinic acid D-ribonucleotide"
      minerva_name "NAMN"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15763"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15763"
      minerva_synonyms "Nicotinate D-ribonucleotide; Nicotinate ribonucleotide; Nicotinic acid ribonucleotide; beta-Nicotinate D-ribonucleotide"
      minerva_type "Simple molecule"
      minerva_x 1435.0
      minerva_y 2240.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa199"
      species_meta_id "s_id_sa199"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 93
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "hmox1__cell__activated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa352"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa352"
      minerva_name "HMOX1"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000100292"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=1.14.14.18"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/5013"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=HMOX1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=HMOX1"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_002133"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/3162"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/P09601"
      minerva_ref_link8 "https://purl.uniprot.org/uniprot/P09601"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/3162"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000100292"
      minerva_ref_type__resource10 "EC__1.14.14.18"
      minerva_ref_type__resource2 "HGNC__5013"
      minerva_ref_type__resource3 "HGNC_SYMBOL__HMOX1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__HMOX1"
      minerva_ref_type__resource5 "REFSEQ__NM_002133"
      minerva_ref_type__resource6 "ENTREZ__3162"
      minerva_ref_type__resource7 "UNIPROT__P09601"
      minerva_ref_type__resource8 "UNIPROT__P09601"
      minerva_ref_type__resource9 "ENTREZ__3162"
      minerva_structuralState "Activated"
      minerva_type "Protein"
      minerva_x 695.0
      minerva_y 1330.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa352"
      species_meta_id "s_id_sa352"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 94
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "hmox1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa351"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa351"
      minerva_name "HMOX1"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000100292"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=1.14.14.18"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/5013"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=HMOX1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=HMOX1"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_002133"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/3162"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/P09601"
      minerva_ref_link8 "https://purl.uniprot.org/uniprot/P09601"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/3162"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000100292"
      minerva_ref_type__resource10 "EC__1.14.14.18"
      minerva_ref_type__resource2 "HGNC__5013"
      minerva_ref_type__resource3 "HGNC_SYMBOL__HMOX1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__HMOX1"
      minerva_ref_type__resource5 "REFSEQ__NM_002133"
      minerva_ref_type__resource6 "ENTREZ__3162"
      minerva_ref_type__resource7 "UNIPROT__P09601"
      minerva_ref_type__resource8 "UNIPROT__P09601"
      minerva_ref_type__resource9 "ENTREZ__3162"
      minerva_type "Protein"
      minerva_x 285.0
      minerva_y 1330.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa351"
      species_meta_id "s_id_sa351"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 95
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 800.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "effector_space_t_minus_cell_space_apoptosis__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa371"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa371"
      minerva_fullName "cytotoxic T cell pyroptotic process"
      minerva_name "Effector T-cell apoptosis"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:1902483"
      minerva_ref_type__resource1 "GO__GO:1902483"
      minerva_type "Phenotype"
      minerva_x 1735.0
      minerva_y 370.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa371"
      species_meta_id "s_id_sa371"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 96
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 900.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "l_minus_tryptophan__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2"
      minerva_fullName "L-tryptophan zwitterion"
      minerva_name "L-Tryptophan"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:57912"
      minerva_ref_type__resource1 "CHEBI__CHEBI:57912"
      minerva_synonyms "(2S)-2-ammonio-3-(1H-indol-3-yl)propanoate; L-tryptophan; tryptophan"
      minerva_type "Simple molecule"
      minerva_x 1965.0
      minerva_y 370.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2"
      species_meta_id "s_id_sa2"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 97
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1000.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "l_minus_glu__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa212"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa212"
      minerva_fullName "L-glutamate(1-)"
      minerva_name "L-Glu"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:29985"
      minerva_ref_type__resource1 "CHEBI__CHEBI:29985"
      minerva_synonyms "(2S)-2-ammoniopentanedioate; L-glutamate; L-glutamate; L-glutamate(1-); L-glutamic acid monoanion; L-glutamic acid, ion(1-)"
      minerva_type "Simple molecule"
      minerva_x 1493.75
      minerva_y 2610.625
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa212"
      species_meta_id "s_id_sa212"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 98
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "slc5a8__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa269"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa269"
      minerva_name "SLC5A8"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/Q8N695"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000256870"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/19119"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SLC5A8"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SLC5A8"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_145913"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/160728"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/160728"
      minerva_ref_type__resource1 "UNIPROT__Q8N695"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000256870"
      minerva_ref_type__resource3 "HGNC__19119"
      minerva_ref_type__resource4 "HGNC_SYMBOL__SLC5A8"
      minerva_ref_type__resource5 "HGNC_SYMBOL__SLC5A8"
      minerva_ref_type__resource6 "REFSEQ__NM_145913"
      minerva_ref_type__resource7 "ENTREZ__160728"
      minerva_ref_type__resource8 "ENTREZ__160728"
      minerva_type "Protein"
      minerva_x 2395.0
      minerva_y 2520.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa269"
      species_meta_id "s_id_sa269"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 99
    zlevel -1

    cluster [
      cluster "s_id_ca8"
    ]
    graphics [
      x 1200.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "e_minus___endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa320"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa320"
      minerva_fullName "electron"
      minerva_name "e-"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:10545"
      minerva_ref_type__resource1 "CHEBI__CHEBI:10545"
      minerva_synonyms "Elektron; beta; beta(-); beta-particle; e; e(-); e-; electron; electron; negatron"
      minerva_type "Ion"
      minerva_x 1758.75
      minerva_y 3160.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca8"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa320"
      species_meta_id "s_id_sa320"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 100
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "2ama__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa47"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa47"
      minerva_fullName "2-aminomuconic 6-semialdehyde"
      minerva_name "2AMA"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15745"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15745"
      minerva_synonyms "2-Aminomuconate 6-semialdehyde; 2-Aminomuconate semialdehyde; 2-aminomuconic semialdehyde"
      minerva_type "Simple molecule"
      minerva_x 1785.0
      minerva_y 1750.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa47"
      species_meta_id "s_id_sa47"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 101
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 300.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "dendritic_space_cell_space_maturation__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa376"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa376"
      minerva_fullName "mature conventional dendritic cell differentiation"
      minerva_name "Dendritic cell maturation"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0097029"
      minerva_ref_type__resource1 "GO__GO:0097029"
      minerva_type "Phenotype"
      minerva_x 2525.0
      minerva_y 1500.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa376"
      species_meta_id "s_id_sa376"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 102
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 400.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nmn__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa260"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa260"
      minerva_fullName "NMN(-)"
      minerva_name "NMN"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:14649"
      minerva_ref_type__resource1 "CHEBI__CHEBI:14649"
      minerva_synonyms "beta-nicotinamide D-ribonucleotide"
      minerva_type "Simple molecule"
      minerva_x 2635.0
      minerva_y 550.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa260"
      species_meta_id "s_id_sa260"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 103
    zlevel -1

    cluster [
      cluster "s_id_ca7"
    ]
    graphics [
      x 500.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "h2o__peroxisome__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa284"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa284"
      minerva_fullName "water"
      minerva_name "H2O"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15377"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15377"
      minerva_synonyms "BOUND WATER; H2O; H2O; HOH; WATER; Wasser; Water; [OH2]; acqua; agua; aqua; dihydridooxygen; dihydrogen oxide; eau; hydrogen hydroxide"
      minerva_type "Simple molecule"
      minerva_x 611.6428571428572
      minerva_y 3092.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca7"
      compartment_name "peroxisome"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa284"
      species_meta_id "s_id_sa284"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 104
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf3a__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa350"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa350"
      minerva_name "Orf3a"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P0DTC3"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/43740569"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/BCD58754"
      minerva_ref_type__resource1 "UNIPROT__P0DTC3"
      minerva_ref_type__resource2 "ENTREZ__43740569"
      minerva_ref_type__resource3 "NCBI_PROTEIN__BCD58754"
      minerva_type "Protein"
      minerva_x 415.0
      minerva_y 1370.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa350"
      species_meta_id "s_id_sa350"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 105
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "bst1__cell__none__2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa264"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa264"
      minerva_homodimer "2"
      minerva_name "BST1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_004334"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000109743"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.2.2.6"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/683"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/683"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1118"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/Q10588"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BST1"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BST1"
      minerva_ref_type__resource1 "REFSEQ__NM_004334"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000109743"
      minerva_ref_type__resource3 "EC__3.2.2.6"
      minerva_ref_type__resource4 "ENTREZ__683"
      minerva_ref_type__resource5 "ENTREZ__683"
      minerva_ref_type__resource6 "HGNC__1118"
      minerva_ref_type__resource7 "UNIPROT__Q10588"
      minerva_ref_type__resource8 "HGNC_SYMBOL__BST1"
      minerva_ref_type__resource9 "HGNC_SYMBOL__BST1"
      minerva_type "Protein"
      minerva_x 2325.0
      minerva_y 470.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa264"
      species_meta_id "s_id_sa264"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 106
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 800.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "hcooh__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa15"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa15"
      minerva_fullName "formic acid"
      minerva_name "HCOOH"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:30751"
      minerva_ref_type__resource1 "CHEBI__CHEBI:30751"
      minerva_synonyms "Acide formique; Ameisensaeure; FORMIC ACID; Formic acid; H-COOH; HCO2H; HCOOH; Methanoic acid; aminic acid; bilorin; formylic acid; hydrogen carboxylic acid; methoic acid"
      minerva_type "Simple molecule"
      minerva_x 1915.0
      minerva_y 1160.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa15"
      species_meta_id "s_id_sa15"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 107
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mg2_plus___cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa191"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa191"
      minerva_fullName "magnesium(2+)"
      minerva_name "Mg2+"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_synonyms "MAGNESIUM ION; Mg(2+); Mg(2+); Mg2+; magnesium, doubly charged positive ion; magnesium, ion (Mg(2+))"
      minerva_type "Ion"
      minerva_x 1795.0
      minerva_y 2310.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa191"
      species_meta_id "s_id_sa191"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 108
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1000.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "apo_minus_ido1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa5"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa5"
      minerva_name "apo-IDO1"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P14902"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IDO1"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/3620"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=1.13.11.52"
      minerva_ref_type__resource1 "UNIPROT__P14902"
      minerva_ref_type__resource2 "HGNC_SYMBOL__IDO1"
      minerva_ref_type__resource3 "ENTREZ__3620"
      minerva_ref_type__resource4 "EC__1.13.11.52"
      minerva_type "Protein"
      minerva_x 1455.0
      minerva_y 750.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa5"
      species_meta_id "s_id_sa5"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 109
    zlevel -1

    cluster [
      cluster "s_id_ca7"
    ]
    graphics [
      x 1100.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nadh__peroxisome__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa286"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa286"
      minerva_fullName "NADH(2-)"
      minerva_name "NADH"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:57945"
      minerva_ref_type__resource1 "CHEBI__CHEBI:57945"
      minerva_synonyms "NADH; NADH dianion"
      minerva_type "Simple molecule"
      minerva_x 579.7857142857147
      minerva_y 3055.428571428572
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca7"
      compartment_name "peroxisome"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa286"
      species_meta_id "s_id_sa286"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 110
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1200.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ppi__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa193"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa211"
      minerva_elementId2 "sa193"
      minerva_elementId3 "sa192"
      minerva_fullName "diphosphoric acid"
      minerva_name "PPi"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:29888"
      minerva_ref_type__resource1 "CHEBI__CHEBI:29888"
      minerva_synonyms "Diphosphoric acid; Diphosphorsaeure; H4P2O7; PYROPHOSPHATE; Pyrophosphate; Pyrophosphoric acid; Pyrophosphorsaeure; [(HO)2P(O)OP(O)(OH)2]; acide diphosphorique"
      minerva_type "Simple molecule"
      minerva_x 1385.0
      minerva_x2 1825.0
      minerva_x3 1635.0
      minerva_y 2600.0
      minerva_y2 2400.0
      minerva_y3 2190.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa193"
      species_meta_id "s_id_sa193"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 111
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "quin__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa41"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa41"
      minerva_fullName "quinolinic acid"
      minerva_name "QUIN"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16675"
      minerva_ref_type__resource1 "CHEBI__CHEBI:16675"
      minerva_synonyms "2,3-Pyridinedicarboxylic acid; Pyridine-2,3-dicarboxylate; QUINOLINIC ACID; Quinolinic acid"
      minerva_type "Simple molecule"
      minerva_x 1965.0
      minerva_y 1900.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa41"
      species_meta_id "s_id_sa41"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 112
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 300.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nad_plus___human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa256"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa256"
      minerva_fullName "NAD(1-)"
      minerva_name "NAD+"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:57540"
      minerva_ref_type__resource1 "CHEBI__CHEBI:57540"
      minerva_synonyms "NAD anion; NAD(+)"
      minerva_type "Simple molecule"
      minerva_x 2415.0
      minerva_y 400.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa256"
      species_meta_id "s_id_sa256"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 113
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "l_minus_gln__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa197"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa197"
      minerva_fullName "L-glutamine zwitterion"
      minerva_name "L-Gln"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:58359"
      minerva_ref_type__resource1 "CHEBI__CHEBI:58359"
      minerva_synonyms "(2S)-5-amino-2-ammonio-5-oxopentanoate; L-glutamine"
      minerva_type "Simple molecule"
      minerva_x 1503.25
      minerva_y 2451.875
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa197"
      species_meta_id "s_id_sa197"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 114
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 500.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "carkd__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa106"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa106"
      minerva_name "CARKD"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NAXD"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q8IW45"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=4.2.1.93"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/55739"
      minerva_ref_type__resource1 "HGNC_SYMBOL__NAXD"
      minerva_ref_type__resource2 "UNIPROT__Q8IW45"
      minerva_ref_type__resource3 "EC__4.2.1.93"
      minerva_ref_type__resource4 "ENTREZ__55739"
      minerva_type "Protein"
      minerva_x 292.5
      minerva_y 2136.75
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa106"
      species_meta_id "s_id_sa106"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 115
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 600.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "h_plus___mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa98"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa98"
      minerva_fullName "hydron"
      minerva_name "H+"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15378"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15378"
      minerva_synonyms "H(+); H+; Hydron"
      minerva_type "Ion"
      minerva_x 568.25
      minerva_y 2234.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa98"
      species_meta_id "s_id_sa98"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 116
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 700.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "o2__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa250"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa250"
      minerva_fullName "dioxygen"
      minerva_name "O2"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15379"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15379"
      minerva_synonyms "Disauerstoff; E 948; E-948; E948; O2; O2; O2; OXYGEN MOLECULE; Oxygen; [OO]; dioxygene; molecular oxygen"
      minerva_type "Ion"
      minerva_x 2115.0
      minerva_y 280.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa250"
      species_meta_id "s_id_sa250"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 117
    zlevel -1

    cluster [
      cluster "s_id_ca8"
    ]
    graphics [
      x 800.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "h2o__endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa318"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa318"
      minerva_fullName "water"
      minerva_name "H2O"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15377"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15377"
      minerva_synonyms "BOUND WATER; H2O; H2O; HOH; WATER; Wasser; Water; [OH2]; acqua; agua; aqua; dihydridooxygen; dihydrogen oxide; eau; hydrogen hydroxide"
      minerva_type "Simple molecule"
      minerva_x 1887.25
      minerva_y 3099.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca8"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa318"
      species_meta_id "s_id_sa318"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 118
    zlevel -1

    cluster [
      cluster "s_id_ca6"
    ]
    graphics [
      x 900.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "namn__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa291"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa291"
      minerva_fullName "nicotinic acid D-ribonucleotide"
      minerva_name "NAMN"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15763"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15763"
      minerva_synonyms "Nicotinate D-ribonucleotide; Nicotinate ribonucleotide; Nicotinic acid ribonucleotide; beta-Nicotinate D-ribonucleotide"
      minerva_type "Simple molecule"
      minerva_x 395.0
      minerva_y 925.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca6"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa291"
      species_meta_id "s_id_sa291"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 119
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1000.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ap_minus_dobu__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa12"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa12"
      minerva_fullName "4-(2-aminophenyl)-2,4-dioxobutanoic acid"
      minerva_name "AP-DOBu"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17442"
      minerva_ref_type__resource1 "CHEBI__CHEBI:17442"
      minerva_synonyms "4-(2-Aminophenyl)-2,4-dioxobutanoate"
      minerva_type "Simple molecule"
      minerva_x 1685.0
      minerva_y 1200.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa12"
      species_meta_id "s_id_sa12"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 120
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "co__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa362"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa362"
      minerva_fullName "carbon monoxide"
      minerva_name "CO"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17245"
      minerva_ref_type__resource1 "CHEBI__CHEBI:17245"
      minerva_synonyms "C#O; CARBON MONOXIDE; CO; CO; Carbon monoxide; [CO]"
      minerva_type "Simple molecule"
      minerva_x 825.0
      minerva_y 1290.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa362"
      species_meta_id "s_id_sa362"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 121
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1200.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cd38__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa265"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa265"
      minerva_name "CD38"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/952"
      minerva_ref_link10 "https://purl.uniprot.org/uniprot/P28907"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/952"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.2.2.6"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CD38"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CD38"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000004468"
      minerva_ref_link7 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.4.99.20"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1667"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_001775"
      minerva_ref_type__resource1 "ENTREZ__952"
      minerva_ref_type__resource10 "UNIPROT__P28907"
      minerva_ref_type__resource2 "ENTREZ__952"
      minerva_ref_type__resource3 "EC__3.2.2.6"
      minerva_ref_type__resource4 "HGNC_SYMBOL__CD38"
      minerva_ref_type__resource5 "HGNC_SYMBOL__CD38"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000004468"
      minerva_ref_type__resource7 "EC__2.4.99.20"
      minerva_ref_type__resource8 "HGNC__1667"
      minerva_ref_type__resource9 "REFSEQ__NM_001775"
      minerva_type "Protein"
      minerva_x 2185.0
      minerva_y 470.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa265"
      species_meta_id "s_id_sa265"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 122
    zlevel -1

    cluster [
      cluster "s_id_ca6"
    ]
    graphics [
      x 200.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "naad__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa290"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa290"
      minerva_fullName "deamido-NAD(+)"
      minerva_name "NAAD"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18304"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18304"
      minerva_synonyms "Deamido-NAD; Deamido-NAD+; Deamino-NAD+"
      minerva_type "Simple molecule"
      minerva_x 625.0
      minerva_y 925.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca6"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa290"
      species_meta_id "s_id_sa290"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 123
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 300.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s_minus_nadphx__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa101"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa101"
      minerva_fullName "(S)-NADPHX"
      minerva_name "S-NADPHX"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:64084"
      minerva_ref_type__resource1 "CHEBI__CHEBI:64084"
      minerva_synonyms "(6S)-6beta-hydroxy-1,4,5,6-tetrahydronicotinamide adenine dinucleotide phosphate"
      minerva_type "Simple molecule"
      minerva_x 392.5
      minerva_y 2046.75
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa101"
      species_meta_id "s_id_sa101"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 124
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 400.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ppi_space_(3_minus_)__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa96"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa96"
      minerva_fullName "diphosphate(3-)"
      minerva_name "PPi (3-)"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:33019"
      minerva_ref_type__resource1 "CHEBI__CHEBI:33019"
      minerva_synonyms "HP2O7(3-); diphosphate"
      minerva_type "Simple molecule"
      minerva_x 742.5
      minerva_y 2135.75
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa96"
      species_meta_id "s_id_sa96"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 125
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 500.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "naad__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa97"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa97"
      minerva_fullName "deamido-NAD(+)"
      minerva_name "NAAD"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18304"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18304"
      minerva_synonyms "Deamido-NAD; Deamido-NAD+; Deamino-NAD+"
      minerva_type "Simple molecule"
      minerva_x 672.5
      minerva_y 2176.75
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa97"
      species_meta_id "s_id_sa97"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 126
    zlevel -1

    cluster [
      cluster "s_id_ca8"
    ]
    graphics [
      x 600.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "h_plus___endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa319"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa319"
      minerva_fullName "hydron"
      minerva_name "H+"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15378"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15378"
      minerva_synonyms "H(+); H+; Hydron"
      minerva_type "Ion"
      minerva_x 1748.75
      minerva_y 3101.25
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca8"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa319"
      species_meta_id "s_id_sa319"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 127
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "slc36a4__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa8"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa8"
      minerva_name "SLC36A4"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SLC36A4"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SLC36A4"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_001286139"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/Q6YBV0"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000180773"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/120103"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/120103"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/19660"
      minerva_ref_type__resource1 "HGNC_SYMBOL__SLC36A4"
      minerva_ref_type__resource2 "HGNC_SYMBOL__SLC36A4"
      minerva_ref_type__resource3 "REFSEQ__NM_001286139"
      minerva_ref_type__resource4 "UNIPROT__Q6YBV0"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000180773"
      minerva_ref_type__resource6 "ENTREZ__120103"
      minerva_ref_type__resource7 "ENTREZ__120103"
      minerva_ref_type__resource8 "HGNC__19660"
      minerva_type "Protein"
      minerva_x 1905.0
      minerva_y 480.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa8"
      species_meta_id "s_id_sa8"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 128
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 800.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "inos__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa60"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa60"
      minerva_name "iNOS"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NOS2"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P35228"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/4843"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=1.14.13.39"
      minerva_ref_type__resource1 "HGNC_SYMBOL__NOS2"
      minerva_ref_type__resource2 "UNIPROT__P35228"
      minerva_ref_type__resource3 "ENTREZ__4843"
      minerva_ref_type__resource4 "EC__1.14.13.39"
      minerva_type "Protein"
      minerva_x 1018.0
      minerva_y 1410.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa60"
      species_meta_id "s_id_sa60"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 129
    zlevel -1

    cluster [
      cluster "s_id_ca8"
    ]
    graphics [
      x 900.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ptcs2:celecoxib__endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa34"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa34"
      minerva_name "PTCS2:celecoxib"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P35354"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:41423"
      minerva_ref_type__resource1 "UNIPROT__P35354"
      minerva_ref_type__resource2 "CHEBI__CHEBI:41423"
      minerva_type "Complex"
      minerva_x 1720.0
      minerva_y 2987.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca8"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa34"
      species_meta_id "s_id_csa34"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 130
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 1000.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nmnat3:mg2_plus___mitochondrial_space_matrix__none__4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa3"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa3"
      minerva_homodimer "4"
      minerva_name "NMNAT3:Mg2+"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/Q96T66"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "UNIPROT__Q96T66"
      minerva_ref_type__resource2 "CHEBI__CHEBI:18420"
      minerva_type "Complex"
      minerva_x 562.5
      minerva_y 2096.75
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa3"
      species_meta_id "s_id_csa3"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 131
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "amp__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa216"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa216"
      minerva_fullName "adenosine 5'-monophosphate"
      minerva_name "AMP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16027"
      minerva_ref_type__resource1 "CHEBI__CHEBI:16027"
      minerva_synonyms "5'-AMP; 5'-Adenosine monophosphate; 5'-Adenylic acid; 5'-O-phosphonoadenosine; ADENOSINE MONOPHOSPHATE; AMP; Adenosine 5'-monophosphate; Adenosine 5'-phosphate; Adenosine-5'-monophosphoric acid; Adenylate; Adenylic acid; Ado5'P; PAdo; adenosine 5'-(dihydrogen phosphate); adenosine phosphate; adenosine phosphate; adenosine-5'P; adenosini phosphas; fosfato de adenosina; pA; phosphate d'adenosine"
      minerva_type "Simple molecule"
      minerva_x 1405.0
      minerva_y 2620.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa216"
      species_meta_id "s_id_sa216"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 132
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1200.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "slc22a13__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa268"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa268"
      minerva_former_symbols "ORCTL3"
      minerva_name "SLC22A13"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SLC22A13"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_004256"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SLC22A13"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000172940"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/Q9Y226"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/8494"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/9390"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/9390"
      minerva_ref_type__resource1 "HGNC_SYMBOL__SLC22A13"
      minerva_ref_type__resource2 "REFSEQ__NM_004256"
      minerva_ref_type__resource3 "HGNC_SYMBOL__SLC22A13"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000172940"
      minerva_ref_type__resource5 "UNIPROT__Q9Y226"
      minerva_ref_type__resource6 "HGNC__8494"
      minerva_ref_type__resource7 "ENTREZ__9390"
      minerva_ref_type__resource8 "ENTREZ__9390"
      minerva_type "Protein"
      minerva_x 2385.0
      minerva_y 2160.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa268"
      species_meta_id "s_id_sa268"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 133
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ahr_slash_l_minus_kyn__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa2"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa2"
      minerva_name "AHR/L-KYN"
      minerva_type "Complex"
      minerva_x 2255.0
      minerva_y 1160.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa2"
      species_meta_id "s_id_csa2"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 134
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ca__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa373"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa373"
      minerva_fullName "calcium(2+)"
      minerva_name "CA"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:29108"
      minerva_ref_type__resource1 "CHEBI__CHEBI:29108"
      minerva_synonyms "CALCIUM ION; Ca(2+); Ca(2+); Ca2+; calcium, doubly charged positive ion"
      minerva_type "Simple molecule"
      minerva_x 2175.0
      minerva_y 1570.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa373"
      species_meta_id "s_id_sa373"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 135
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "haao__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa30"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa30"
      minerva_name "HAAO"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P46952"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/23498"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/23498"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/4796"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000162882"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_012205"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=HAAO"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=HAAO"
      minerva_ref_link9 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=1.13.11.6"
      minerva_ref_type__resource1 "UNIPROT__P46952"
      minerva_ref_type__resource2 "ENTREZ__23498"
      minerva_ref_type__resource3 "ENTREZ__23498"
      minerva_ref_type__resource4 "HGNC__4796"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000162882"
      minerva_ref_type__resource6 "REFSEQ__NM_012205"
      minerva_ref_type__resource7 "HGNC_SYMBOL__HAAO"
      minerva_ref_type__resource8 "HGNC_SYMBOL__HAAO"
      minerva_ref_type__resource9 "EC__1.13.11.6"
      minerva_type "Protein"
      minerva_x 1875.0
      minerva_y 1650.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa30"
      species_meta_id "s_id_sa30"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 136
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "l_minus_ala__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa29"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa29"
      minerva_fullName "L-alanine zwitterion"
      minerva_name "L-Ala"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:57972"
      minerva_ref_type__resource1 "CHEBI__CHEBI:57972"
      minerva_synonyms "(2S)-2-ammoniopropanoate; L-alanine"
      minerva_type "Simple molecule"
      minerva_x 1665.0
      minerva_y 1490.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa29"
      species_meta_id "s_id_sa29"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 137
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nmn__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa223"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa223"
      minerva_fullName "NMN(-)"
      minerva_name "NMN"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:14649"
      minerva_ref_type__resource1 "CHEBI__CHEBI:14649"
      minerva_synonyms "beta-nicotinamide D-ribonucleotide"
      minerva_type "Simple molecule"
      minerva_x 1213.25
      minerva_y 2670.25
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa223"
      species_meta_id "s_id_sa223"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 138
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 700.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nam__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa253"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa253"
      minerva_fullName "nicotinamide"
      minerva_name "NAM"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17154"
      minerva_ref_type__resource1 "CHEBI__CHEBI:17154"
      minerva_synonyms "3-pyridinecarboxamide; NICOTINAMIDE; Niacinamide; Nicotinamid; Nicotinamide; Nicotinic acid amide; Nicotinsaeureamid; Nikotinamid; Nikotinsaeureamid; Vitamin PP; beta-pyridinecarboxamide; niacin; nicotinamide; vitamin B3"
      minerva_type "Simple molecule"
      minerva_x 2115.0
      minerva_y 380.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa253"
      species_meta_id "s_id_sa253"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 139
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 800.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "epacadostat__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa3"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa3"
      minerva_name "Epacadostat"
      minerva_type "Drug"
      minerva_x 1885.0
      minerva_y 735.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa3"
      species_meta_id "s_id_sa3"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 140
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "3hkyn__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa32"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa32"
      minerva_fullName "3-hydroxy-L-kynurenine"
      minerva_name "3HKYN"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17380"
      minerva_ref_type__resource1 "CHEBI__CHEBI:17380"
      minerva_synonyms "(2S)-2-amino-4-(2-amino-3-hydroxyphenyl)-4-oxobutanoic acid; 3-(3-hydroxyanthraniloyl)-L-alanine; 3-Hydroxy-L-kynurenine; L-3-hydroxykynurenine"
      minerva_type "Simple molecule"
      minerva_x 1965.0
      minerva_y 1420.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa32"
      species_meta_id "s_id_sa32"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 142
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nr__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa238"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa238"
      minerva_fullName "N-ribosylnicotinamide"
      minerva_name "NR"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15927"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15927"
      minerva_synonyms "1-(beta-D-Ribofuranosyl)nicotinamide; N-Ribosylnicotinamide; Nicotinamide riboside; beta-nicotinamide D-riboside; nicotinamide ribonucleoside; nicotinamide ribose; nicotinamide-beta-riboside"
      minerva_type "Simple molecule"
      minerva_x 955.0
      minerva_y 2670.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa238"
      species_meta_id "s_id_sa238"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 143
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1200.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "kmo__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa377"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa377"
      minerva_name "KMO"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=KMO"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=KMO"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/O15229"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/8564"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/8564"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=1.14.13.9"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6381"
      minerva_ref_link8 "https://www.ensembl.org/id/ENSG00000117009"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_003679"
      minerva_ref_type__resource1 "HGNC_SYMBOL__KMO"
      minerva_ref_type__resource2 "HGNC_SYMBOL__KMO"
      minerva_ref_type__resource3 "UNIPROT__O15229"
      minerva_ref_type__resource4 "ENTREZ__8564"
      minerva_ref_type__resource5 "ENTREZ__8564"
      minerva_ref_type__resource6 "EC__1.14.13.9"
      minerva_ref_type__resource7 "HGNC__6381"
      minerva_ref_type__resource8 "ENSEMBL__ENSG00000117009"
      minerva_ref_type__resource9 "REFSEQ__NM_003679"
      minerva_type "Protein"
      minerva_x 1905.0
      minerva_y 1340.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa377"
      species_meta_id "s_id_sa377"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 144
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "adohcy__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa231"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa231"
      minerva_fullName "S-adenosyl-L-homocysteine"
      minerva_name "AdoHcy"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16680"
      minerva_ref_type__resource1 "CHEBI__CHEBI:16680"
      minerva_synonyms "(2S)-2-amino-4-({[(2S,3S,4R,5R)-5-(6-amino-9H-purin-9-yl)-3,4-dihydroxytetrahydrofuran-2-yl]methyl}sulfanyl)butanoic acid; 2-S-adenosyl-L-homocysteine; Adenosyl-L-homocysteine; AdoHcy; S-(5'-adenosyl)-L-homocysteine; S-(5'-adenosyl)-L-homocysteine; S-ADENOSYL-L-HOMOCYSTEINE; S-Adenosyl-L-homocysteine; S-Adenosylhomocysteine; S-[1-(adenin-9-yl)-1,5-dideoxy-beta-D-ribofuranos-5-yl]-L-homocysteine; SAH; adenosylhomocysteine"
      minerva_type "Simple molecule"
      minerva_x 2105.0
      minerva_y 2700.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa231"
      species_meta_id "s_id_sa231"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 145
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nadk:zn2_plus__space_tetramer__cell__none__4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa17"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa17"
      minerva_homodimer "4"
      minerva_name "NADK:Zn2+ tetramer"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:29105"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/O95544"
      minerva_ref_type__resource1 "CHEBI__CHEBI:29105"
      minerva_ref_type__resource2 "UNIPROT__O95544"
      minerva_type "Complex"
      minerva_x 1330.1875
      minerva_y 2805.4375
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa17"
      species_meta_id "s_id_csa17"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 146
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nadsyn1_space_hexamer__cell__activated__6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa208"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa208"
      minerva_homodimer "6"
      minerva_name "NADSYN1 hexamer"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/55191"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/55191"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NADSYN1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NADSYN1"
      minerva_ref_link5 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=6.3.5.1"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/Q6IA69"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/29832"
      minerva_ref_link8 "https://www.ensembl.org/id/ENSG00000172890"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_018161"
      minerva_ref_type__resource1 "ENTREZ__55191"
      minerva_ref_type__resource2 "ENTREZ__55191"
      minerva_ref_type__resource3 "HGNC_SYMBOL__NADSYN1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__NADSYN1"
      minerva_ref_type__resource5 "EC__6.3.5.1"
      minerva_ref_type__resource6 "UNIPROT__Q6IA69"
      minerva_ref_type__resource7 "HGNC__29832"
      minerva_ref_type__resource8 "ENSEMBL__ENSG00000172890"
      minerva_ref_type__resource9 "REFSEQ__NM_018161"
      minerva_structuralState "Activated"
      minerva_type "Protein"
      minerva_x 1605.0
      minerva_y 2560.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa208"
      species_meta_id "s_id_sa208"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 147
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 500.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "na_plus___human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa336"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa336"
      minerva_fullName "sodium(1+)"
      minerva_name "Na+"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:29101"
      minerva_ref_type__resource1 "CHEBI__CHEBI:29101"
      minerva_synonyms "Na(+); Na(+); Na+; SODIUM ION"
      minerva_type "Ion"
      minerva_x 2475.0
      minerva_y 2400.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa336"
      species_meta_id "s_id_sa336"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 148
    zlevel -1

    cluster [
      cluster "s_id_ca6"
    ]
    graphics [
      x 600.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nmnat1:zn2_plus___nucleus__none__6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa26"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa26"
      minerva_homodimer "6"
      minerva_name "NMNAT1:Zn2+"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:29105"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/Q9HAN9"
      minerva_ref_type__resource1 "CHEBI__CHEBI:29105"
      minerva_ref_type__resource2 "UNIPROT__Q9HAN9"
      minerva_type "Complex"
      minerva_x 525.0
      minerva_y 785.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca6"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa26"
      species_meta_id "s_id_csa26"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 149
    zlevel -1

    cluster [
      cluster "s_id_ca8"
    ]
    graphics [
      x 700.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ptgs2__endoplasmic_space_reticulum__none__2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa315"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa315"
      minerva_homodimer "2"
      minerva_name "PTGS2"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000073756"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/9605"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PTGS2"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PTGS2"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P35354"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000963"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/5743"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/5743"
      minerva_ref_link9 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=1.14.99.1"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000073756"
      minerva_ref_type__resource2 "HGNC__9605"
      minerva_ref_type__resource3 "HGNC_SYMBOL__PTGS2"
      minerva_ref_type__resource4 "HGNC_SYMBOL__PTGS2"
      minerva_ref_type__resource5 "UNIPROT__P35354"
      minerva_ref_type__resource6 "REFSEQ__NM_000963"
      minerva_ref_type__resource7 "ENTREZ__5743"
      minerva_ref_type__resource8 "ENTREZ__5743"
      minerva_ref_type__resource9 "EC__1.14.99.1"
      minerva_type "Protein"
      minerva_x 1849.0
      minerva_y 3187.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca8"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa315"
      species_meta_id "s_id_sa315"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 150
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 800.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "qprt__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa207"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa207"
      minerva_name "QPRT"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/9755"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=QPRT"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/23475"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000103485"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=QPRT"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/23475"
      minerva_ref_link7 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.4.2.19"
      minerva_ref_link8 "https://purl.uniprot.org/uniprot/Q15274"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_014298"
      minerva_ref_type__resource1 "HGNC__9755"
      minerva_ref_type__resource2 "HGNC_SYMBOL__QPRT"
      minerva_ref_type__resource3 "ENTREZ__23475"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000103485"
      minerva_ref_type__resource5 "HGNC_SYMBOL__QPRT"
      minerva_ref_type__resource6 "ENTREZ__23475"
      minerva_ref_type__resource7 "EC__2.4.2.19"
      minerva_ref_type__resource8 "UNIPROT__Q15274"
      minerva_ref_type__resource9 "REFSEQ__NM_014298"
      minerva_type "Protein"
      minerva_x 1655.0
      minerva_y 2010.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa207"
      species_meta_id "s_id_sa207"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 151
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "naprt1__cell__none__2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa188"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa188"
      minerva_homodimer "2"
      minerva_name "NAPRT1"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=6.3.4.21"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q6XQN6"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/93100"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NAPRT"
      minerva_ref_type__resource1 "EC__6.3.4.21"
      minerva_ref_type__resource2 "UNIPROT__Q6XQN6"
      minerva_ref_type__resource3 "ENTREZ__93100"
      minerva_ref_type__resource4 "HGNC_SYMBOL__NAPRT"
      minerva_type "Protein"
      minerva_x 1805.0
      minerva_y 2150.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa188"
      species_meta_id "s_id_sa188"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 152
    zlevel -1

    cluster [
      cluster "s_id_ca8"
    ]
    graphics [
      x 1000.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ptgis__endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa339"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa339"
      minerva_name "PTGIS"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=5.3.99.4"
      minerva_ref_link10 "http://purl.uniprot.org/uniprot/Q16647"
      minerva_ref_link2 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=4.2.1.152"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000124212"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/9603"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/5740"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/5740"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_000961"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PTGIS"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PTGIS"
      minerva_ref_type__resource1 "EC__5.3.99.4"
      minerva_ref_type__resource10 "UNIPROT__Q16647"
      minerva_ref_type__resource2 "EC__4.2.1.152"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000124212"
      minerva_ref_type__resource4 "HGNC__9603"
      minerva_ref_type__resource5 "ENTREZ__5740"
      minerva_ref_type__resource6 "ENTREZ__5740"
      minerva_ref_type__resource7 "REFSEQ__NM_000961"
      minerva_ref_type__resource8 "HGNC_SYMBOL__PTGIS"
      minerva_ref_type__resource9 "HGNC_SYMBOL__PTGIS"
      minerva_type "Protein"
      minerva_x 2080.0
      minerva_y 3197.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca8"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa339"
      species_meta_id "s_id_sa339"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 153
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "no__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa64"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa64"
      minerva_fullName "nitric oxide"
      minerva_name "NO"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16480"
      minerva_ref_type__resource1 "CHEBI__CHEBI:16480"
      minerva_synonyms "(.)NO; (NO)(.); EDRF; NO; NO(.); Nitric oxide; Nitrogen monoxide; Stickstoff(II)-oxid; Stickstoffmonoxid; [NO]; endothelium-derived relaxing factor; mononitrogen monoxide; monoxido de nitrogeno; monoxyde d'azote; nitric oxide; nitrogen monooxide; nitrogen monoxide; nitrosyl; oxido de nitrogeno(II); oxido nitrico; oxyde azotique; oxyde nitrique"
      minerva_type "Simple molecule"
      minerva_x 1185.0
      minerva_y 1290.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa64"
      species_meta_id "s_id_sa64"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 154
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 1200.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nadp_plus___mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa104"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa104"
      minerva_fullName "NADP(+)"
      minerva_name "NADP+"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18009"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18009"
      minerva_synonyms "NADP; NADP+; Nicotinamide adenine dinucleotide phosphate; TPN; Triphosphopyridine nucleotide; beta-Nicotinamide adenine dinucleotide phosphate; beta-nicotinamide adenine dinucleotide phosphate; oxidized nicotinamide-adenine dinucleotide phosphate"
      minerva_type "Simple molecule"
      minerva_x 557.5
      minerva_y 2316.75
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa104"
      species_meta_id "s_id_sa104"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 155
    zlevel -1

    graphics [
      x 200.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re71"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 1795.0
      minerva_y 2235.0
    ]
    sbml [
      reaction_id "re71"
      reaction_meta_id "re71"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 163
    zlevel -1

    graphics [
      x 300.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re41"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 425.12563132923543
      minerva_y 2309.205582617584
    ]
    sbml [
      reaction_id "re41"
      reaction_meta_id "re41"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 170
    zlevel -1

    graphics [
      x 400.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re100"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 1845.0
      minerva_y 3127.5
    ]
    sbml [
      reaction_id "re100"
      reaction_meta_id "re100"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 179
    zlevel -1

    graphics [
      x 500.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re70"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 1082.6223980899786
      minerva_y 2683.0296987566544
    ]
    sbml [
      reaction_id "re70"
      reaction_meta_id "re70"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 186
    zlevel -1

    graphics [
      x 600.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re82"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 2270.125631329235
      minerva_y 414.41941738241593
    ]
    sbml [
      reaction_id "re82"
      reaction_meta_id "re82"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 193
    zlevel -1

    graphics [
      x 700.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re10"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 1965.0
      minerva_y 1660.0
    ]
    sbml [
      reaction_id "re10"
      reaction_meta_id "re10"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 198
    zlevel -1

    graphics [
      x 800.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re11"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 1875.0
      minerva_y 1750.0
    ]
    sbml [
      reaction_id "re11"
      reaction_meta_id "re11"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 203
    zlevel -1

    graphics [
      x 900.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re65"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 1260.0
      minerva_y 2248.838834764832
    ]
    sbml [
      reaction_id "re65"
      reaction_meta_id "re65"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 210
    zlevel -1

    graphics [
      x 1000.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re28"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 1354.7144578222997
      minerva_y 750.2393197559165
    ]
    sbml [
      reaction_id "re28"
      reaction_meta_id "re28"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 213
    zlevel -1

    graphics [
      x 1100.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re104"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 573.125
      minerva_y 999.9999999999998
    ]
    sbml [
      reaction_id "re104"
      reaction_meta_id "re104"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 216
    zlevel -1

    graphics [
      x 1200.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re23"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 1018.310546875
      minerva_y 948.9453125
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re23"
      reaction_meta_id "re23"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 222
    zlevel -1

    graphics [
      x 200.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re12"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 1700.0
      minerva_y 1750.0
    ]
    sbml [
      reaction_id "re12"
      reaction_meta_id "re12"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 229
    zlevel -1

    graphics [
      x 300.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re89"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 2388.75
      minerva_y 2230.0
    ]
    sbml [
      reaction_id "re89"
      reaction_meta_id "re89"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 233
    zlevel -1

    graphics [
      x 400.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re21"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "Trigger"
      minerva_x 2397.3966061562746
      minerva_y 1191.8676006314129
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re21"
      reaction_meta_id "re21"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 236
    zlevel -1

    graphics [
      x 500.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re84"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 2203.5
      minerva_y 249.5
    ]
    sbml [
      reaction_id "re84"
      reaction_meta_id "re84"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 243
    zlevel -1

    graphics [
      x 600.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re116"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "Trigger"
      minerva_x 2383.75
      minerva_y 1420.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re116"
      reaction_meta_id "re116"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 248
    zlevel -1

    graphics [
      x 700.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re3"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 1955.0502307585077
      minerva_y 807.717156727579
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re3"
      reaction_meta_id "re3"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 254
    zlevel -1

    graphics [
      x 800.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re83"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 2266.9457640831606
      minerva_y 383.093140307211
    ]
    sbml [
      reaction_id "re83"
      reaction_meta_id "re83"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 261
    zlevel -1

    graphics [
      x 900.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re86"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 2655.0
      minerva_y 635.0
    ]
    sbml [
      reaction_id "re86"
      reaction_meta_id "re86"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 267
    zlevel -1

    graphics [
      x 1000.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re119"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "Trigger"
      minerva_x 2375.0
      minerva_y 1500.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re119"
      reaction_meta_id "re119"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 271
    zlevel -1

    graphics [
      x 1100.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re25"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 1018.310546875
      minerva_y 1174.1992187499995
    ]
    sbml [
      reaction_id "re25"
      reaction_meta_id "re25"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 274
    zlevel -1

    graphics [
      x 1200.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re68"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 1324.5
      minerva_y 2670.125
    ]
    sbml [
      reaction_id "re68"
      reaction_meta_id "re68"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 280
    zlevel -1

    graphics [
      x 200.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re109"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 573.310546875
      minerva_y 1108.9453125
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re109"
      reaction_meta_id "re109"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 284
    zlevel -1

    graphics [
      x 300.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re67"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 1435.0
      minerva_y 2547.75
    ]
    sbml [
      reaction_id "re67"
      reaction_meta_id "re67"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 294
    zlevel -1

    graphics [
      x 400.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re79"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 1436.5
      minerva_y 2800.125
    ]
    sbml [
      reaction_id "re79"
      reaction_meta_id "re79"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 300
    zlevel -1

    graphics [
      x 500.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re42"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 672.5
      minerva_y 2086.25
    ]
    sbml [
      reaction_id "re42"
      reaction_meta_id "re42"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 306
    zlevel -1

    graphics [
      x 600.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re69"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 1082.6223980899786
      minerva_y 2657.2203012433456
    ]
    sbml [
      reaction_id "re69"
      reaction_meta_id "re69"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 313
    zlevel -1

    graphics [
      x 700.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re19"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "Heterodimer association"
      minerva_x 2147.0
      minerva_y 1160.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re19"
      reaction_meta_id "re19"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 317
    zlevel -1

    graphics [
      x 800.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re22"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 1128.310546875
      minerva_y 948.9453125
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re22"
      reaction_meta_id "re22"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 322
    zlevel -1

    graphics [
      x 900.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re111"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "Negative influence"
      minerva_x 1864.0
      minerva_y 370.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re111"
      reaction_meta_id "re111"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 325
    zlevel -1

    graphics [
      x 1000.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re117"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "Trigger"
      minerva_x 2338.4313165162375
      minerva_y 1499.958007207327
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re117"
      reaction_meta_id "re117"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 328
    zlevel -1

    graphics [
      x 1100.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re112"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "Known transition omitted"
      minerva_x 2075.0
      minerva_y 1570.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re112"
      reaction_meta_id "re112"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 331
    zlevel -1

    graphics [
      x 1200.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re16"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 1965.0
      minerva_y 1825.0
    ]
    sbml [
      reaction_id "re16"
      reaction_meta_id "re16"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 335
    zlevel -1

    graphics [
      x 200.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re107"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 490.0
      minerva_y 1330.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re107"
      reaction_meta_id "re107"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 339
    zlevel -1

    graphics [
      x 300.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 1965.0
      minerva_y 555.1328125000002
    ]
    sbml [
      reaction_id "re1"
      reaction_meta_id "re1"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 343
    zlevel -1

    graphics [
      x 400.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re95"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 510.0
      minerva_y 925.0
    ]
    sbml [
      reaction_id "re95"
      reaction_meta_id "re95"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 349
    zlevel -1

    graphics [
      x 500.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re64"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 1435.0
      minerva_y 2327.25
    ]
    sbml [
      reaction_id "re64"
      reaction_meta_id "re64"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 354
    zlevel -1

    graphics [
      x 600.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re101"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 2080.0
      minerva_y 3127.5
    ]
    sbml [
      reaction_id "re101"
      reaction_meta_id "re101"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 358
    zlevel -1

    graphics [
      x 700.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re102"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 2395.0
      minerva_y 2440.0
    ]
    sbml [
      reaction_id "re102"
      reaction_meta_id "re102"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 364
    zlevel -1

    graphics [
      x 800.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re78"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 1700.0
      minerva_y 2070.0
    ]
    sbml [
      reaction_id "re78"
      reaction_meta_id "re78"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 372
    zlevel -1

    graphics [
      x 900.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re7"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 1598.0
      minerva_y 1200.0
    ]
    sbml [
      reaction_id "re7"
      reaction_meta_id "re7"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 376
    zlevel -1

    graphics [
      x 1000.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re9"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 1965.0
      minerva_y 1495.0
    ]
    sbml [
      reaction_id "re9"
      reaction_meta_id "re9"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 382
    zlevel -1

    graphics [
      x 1100.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re6"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 1815.6191470690515
      minerva_y 1347.4760548967147
    ]
    sbml [
      reaction_id "re6"
      reaction_meta_id "re6"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 387
    zlevel -1

    graphics [
      x 1200.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re66"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 1260.0
      minerva_y 2231.161165235168
    ]
    sbml [
      reaction_id "re66"
      reaction_meta_id "re66"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 394
    zlevel -1

    graphics [
      x 200.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re39"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 392.5
      minerva_y 2001.75
    ]
    sbml [
      reaction_id "re39"
      reaction_meta_id "re39"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 398
    zlevel -1

    graphics [
      x 300.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re73"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 1627.875
      minerva_y 2670.0
    ]
    sbml [
      reaction_id "re73"
      reaction_meta_id "re73"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 404
    zlevel -1

    graphics [
      x 400.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re5"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 1818.25
      minerva_y 1200.0
    ]
    sbml [
      reaction_id "re5"
      reaction_meta_id "re5"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 413
    zlevel -1

    graphics [
      x 500.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re106"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 694.500153308833
      minerva_y 1394.1262818983614
    ]
    sbml [
      reaction_id "re106"
      reaction_meta_id "re106"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 423
    zlevel -1

    graphics [
      x 600.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re115"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "Negative influence"
      minerva_x 2230.2424143985077
      minerva_y 1630.4938839856245
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re115"
      reaction_meta_id "re115"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 426
    zlevel -1

    graphics [
      x 700.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re110"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "Heterodimer association"
      minerva_x 1590.0000000000014
      minerva_y 839.9999999999991
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re110"
      reaction_meta_id "re110"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 430
    zlevel -1

    graphics [
      x 800.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re40"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 392.5
      minerva_y 2141.75
    ]
    sbml [
      reaction_id "re40"
      reaction_meta_id "re40"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 438
    zlevel -1

    graphics [
      x 900.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re85"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 2535.4456854963983
      minerva_y 469.28028553762016
    ]
    sbml [
      reaction_id "re85"
      reaction_meta_id "re85"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 444
    zlevel -1

    graphics [
      x 1000.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re2"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 1975.3482105965008
      minerva_y 807.7187768917918
    ]
    sbml [
      reaction_id "re2"
      reaction_meta_id "re2"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 449
    zlevel -1

    graphics [
      x 1100.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re94"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 708.2857142857147
      minerva_y 3054.928571428572
    ]
    sbml [
      reaction_id "re94"
      reaction_meta_id "re94"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 456
    zlevel -1

    graphics [
      x 1200.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re20"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "Trigger"
      minerva_x 2226.39670817067
      minerva_y 1411.8679319878422
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re20"
      reaction_meta_id "re20"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 459
    zlevel -1

    graphics [
      x 200.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re4"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 1965.0
      minerva_y 1084.25
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re4"
      reaction_meta_id "re4"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 465
    zlevel -1

    graphics [
      x 300.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re8"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 1965.0
      minerva_y 1310.75
    ]
    sbml [
      reaction_id "re8"
      reaction_meta_id "re8"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 474
    zlevel -1

    graphics [
      x 400.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re24"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 1095.0
      minerva_y 1410.625
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re24"
      reaction_meta_id "re24"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 483
    zlevel -1

    graphics [
      x 500.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re74"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 1825.0
      minerva_y 2532.5
    ]
    sbml [
      reaction_id "re74"
      reaction_meta_id "re74"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 489
    zlevel -1

    graphics [
      x 600.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re75"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 2005.125
      minerva_y 2671.875
    ]
    sbml [
      reaction_id "re75"
      reaction_meta_id "re75"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 495
    zlevel -1

    graphics [
      x 700.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re103"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18420"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18420"
      minerva_type "State transition"
      minerva_x 895.0
      minerva_y 950.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re103"
      reaction_meta_id "re103"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 501
    zlevel -1

    cluster [
      cluster "s_id_ca6"
    ]
    graphics [
      x 550.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "inos__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa59"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa59"
      minerva_name "iNOS"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4348"
      minerva_ref_type__resource1 "ENTREZ__4348"
      minerva_type "RNA"
      minerva_x 1018.310546875
      minerva_y 998.9453125
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca6"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation "Annotation [about=\"#s_id_sa59\", history=]"
      species_id "s_id_sa59"
      species_meta_id "s_id_sa59"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
    ]
  ]
  node [
    id 505
    zlevel -1

    cluster [
      cluster "s_id_ca6"
    ]
    graphics [
      x 950.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ido1__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa65"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa65"
      minerva_former_symbols "IDO; INDO"
      minerva_name "IDO1"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6059"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IDO1"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P14902"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000131203"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_002164"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/3620"
      minerva_ref_type__resource1 "HGNC__6059"
      minerva_ref_type__resource2 "HGNC_SYMBOL__IDO1"
      minerva_ref_type__resource3 "UNIPROT__P14902"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000131203"
      minerva_ref_type__resource5 "REFSEQ__NM_002164"
      minerva_ref_type__resource6 "ENTREZ__3620"
      minerva_type "RNA"
      minerva_x 1128.310546875
      minerva_y 998.9453125
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca6"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation "Annotation [about=\"#s_id_sa65\", history=]"
      species_id "s_id_sa65"
      species_meta_id "s_id_sa65"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
      species_notes "XMLNode 'notes' [childElements size=1]"
    ]
  ]
  node [
    id 509
    zlevel -1

    cluster [
      cluster "s_id_ca6"
    ]
    graphics [
      x 650.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "hmox1__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa340"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa340"
      minerva_name "HMOX1"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000100292"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/5013"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=HMOX1"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_002133"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/3162"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P09601"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000100292"
      minerva_ref_type__resource2 "HGNC__5013"
      minerva_ref_type__resource3 "HGNC_SYMBOL__HMOX1"
      minerva_ref_type__resource4 "REFSEQ__NM_002133"
      minerva_ref_type__resource5 "ENTREZ__3162"
      minerva_ref_type__resource6 "UNIPROT__P09601"
      minerva_type "Gene"
      minerva_x 895.0
      minerva_y 900.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca6"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation "Annotation [about=\"#s_id_sa340\", history=]"
      species_id "s_id_sa340"
      species_meta_id "s_id_sa340"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
      species_notes "XMLNode 'notes' [childElements size=1]"
    ]
  ]
  edge [
    id 156
    source 83
    target 155
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa189"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 157
    source 40
    target 155
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa202"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 158
    source 22
    target 155
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa190"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 159
    source 155
    target 92
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa199"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 160
    source 155
    target 110
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa193"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 161
    source 151
    target 155
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR0"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa188"
    ]
  ]
  edge [
    id 162
    source 107
    target 155
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR1"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa191"
    ]
  ]
  edge [
    id 164
    source 6
    target 163
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa108"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 165
    source 53
    target 163
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa94"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 166
    source 163
    target 154
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa104"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 167
    source 163
    target 7
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa99"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 168
    source 163
    target 115
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa98"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 169
    source 31
    target 163
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR2"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa324"
    ]
  ]
  edge [
    id 171
    source 25
    target 170
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa321"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 172
    source 99
    target 170
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa320"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 173
    source 126
    target 170
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa319"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 174
    source 170
    target 71
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa317"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 175
    source 170
    target 117
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa318"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 176
    source 149
    target 170
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR3"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa315"
    ]
  ]
  edge [
    id 177
    source 129
    target 170
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR4"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa34"
    ]
  ]
  edge [
    id 178
    source 35
    target 170
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR5"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa220"
    ]
  ]
  edge [
    id 180
    source 142
    target 179
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa238"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 181
    source 50
    target 179
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa227"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 182
    source 179
    target 137
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa223"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 183
    source 179
    target 58
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa217"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 184
    source 179
    target 22
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa190"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 185
    source 44
    target 179
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR6"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa215"
    ]
  ]
  edge [
    id 187
    source 112
    target 186
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa256"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 188
    source 29
    target 186
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa257"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 189
    source 186
    target 63
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa258"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 190
    source 186
    target 138
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa253"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 191
    source 186
    target 90
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa251"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 192
    source 105
    target 186
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR7"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa264"
    ]
  ]
  edge [
    id 194
    source 34
    target 193
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa31"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 195
    source 81
    target 193
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa4"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 196
    source 193
    target 62
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa38"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 197
    source 135
    target 193
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR8"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa30"
    ]
  ]
  edge [
    id 199
    source 62
    target 198
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa38"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 200
    source 198
    target 100
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa47"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 201
    source 198
    target 84
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa40"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 202
    source 13
    target 198
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR9"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa37"
    ]
  ]
  edge [
    id 204
    source 32
    target 203
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa224"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 205
    source 50
    target 203
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa227"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 206
    source 203
    target 92
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa199"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 207
    source 203
    target 22
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa190"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 208
    source 203
    target 58
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa217"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 209
    source 39
    target 203
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR10"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa236"
    ]
  ]
  edge [
    id 212
    source 210
    target 108
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa5"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 215
    source 213
    target 94
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa351"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 219
    source 78
    target 216
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR11"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa62"
    ]
  ]
  edge [
    id 220
    source 80
    target 216
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR12"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa36"
    ]
  ]
  edge [
    id 221
    source 120
    target 216
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR13"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa362"
    ]
  ]
  edge [
    id 223
    source 100
    target 222
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa47"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 224
    source 5
    target 222
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa10"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 225
    source 37
    target 222
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa222"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 226
    source 222
    target 38
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa49"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 227
    source 222
    target 86
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa44"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 228
    source 222
    target 22
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa190"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 230
    source 57
    target 229
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa271"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 231
    source 229
    target 83
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa189"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 232
    source 132
    target 229
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR14"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa268"
    ]
  ]
  edge [
    id 234
    source 133
    target 233
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa2"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 235
    source 233
    target 41
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa35"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 237
    source 76
    target 236
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa249"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 238
    source 116
    target 236
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa250"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 239
    source 90
    target 236
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa251"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 240
    source 236
    target 66
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa254"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 241
    source 236
    target 112
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa256"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 242
    source 9
    target 236
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR15"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa20"
    ]
  ]
  edge [
    id 244
    source 34
    target 243
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa31"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 245
    source 140
    target 243
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa32"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 246
    source 1
    target 243
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa17"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 247
    source 243
    target 73
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa375"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 249
    source 26
    target 248
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa1"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 250
    source 81
    target 248
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa4"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 251
    source 248
    target 45
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa11"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 252
    source 14
    target 248
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR16"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa39"
    ]
  ]
  edge [
    id 253
    source 139
    target 248
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR17"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa3"
    ]
  ]
  edge [
    id 255
    source 112
    target 254
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa256"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 256
    source 29
    target 254
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa257"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 257
    source 254
    target 138
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa253"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 258
    source 254
    target 63
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa258"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 259
    source 254
    target 90
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa251"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 260
    source 121
    target 254
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR18"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa265"
    ]
  ]
  edge [
    id 262
    source 102
    target 261
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa260"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 263
    source 29
    target 261
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa257"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 264
    source 261
    target 77
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa263"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 265
    source 261
    target 4
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa262"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 266
    source 52
    target 261
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR19"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa35"
    ]
  ]
  edge [
    id 268
    source 140
    target 267
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa32"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 269
    source 34
    target 267
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa31"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 270
    source 267
    target 101
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa376"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 273
    source 271
    target 128
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa60"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 275
    source 137
    target 274
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa223"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 276
    source 50
    target 274
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa227"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 277
    source 274
    target 37
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa222"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 278
    source 274
    target 56
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa228"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 279
    source 47
    target 274
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR20"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa16"
    ]
  ]
  edge [
    id 281
    source 80
    target 280
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa36"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 282
    source 280
    target 80
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa36"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 283
    source 120
    target 280
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR21"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa362"
    ]
  ]
  edge [
    id 285
    source 54
    target 284
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa200"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 286
    source 50
    target 284
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa227"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 287
    source 113
    target 284
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa197"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 288
    source 5
    target 284
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa10"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 289
    source 284
    target 37
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa222"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 290
    source 284
    target 97
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa212"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 291
    source 284
    target 131
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa216"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 292
    source 284
    target 110
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa193"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 293
    source 146
    target 284
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR22"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa208"
    ]
  ]
  edge [
    id 295
    source 37
    target 294
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa222"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 296
    source 50
    target 294
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa227"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 297
    source 294
    target 28
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa241"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 298
    source 294
    target 58
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa217"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 299
    source 145
    target 294
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR23"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa17"
    ]
  ]
  edge [
    id 301
    source 42
    target 300
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa95"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 302
    source 53
    target 300
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa94"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 303
    source 300
    target 125
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa97"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 304
    source 300
    target 124
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa96"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 305
    source 130
    target 300
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR24"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa3"
    ]
  ]
  edge [
    id 307
    source 142
    target 306
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa238"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 308
    source 50
    target 306
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa227"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 309
    source 306
    target 137
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa223"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 310
    source 306
    target 58
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa217"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 311
    source 306
    target 22
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa190"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 312
    source 39
    target 306
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR25"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa236"
    ]
  ]
  edge [
    id 314
    source 1
    target 313
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa17"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 315
    source 68
    target 313
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa26"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 316
    source 313
    target 133
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa2"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 320
    source 153
    target 317
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR26"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa64"
    ]
  ]
  edge [
    id 321
    source 78
    target 317
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR27"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa62"
    ]
  ]
  edge [
    id 323
    source 96
    target 322
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa2"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 324
    source 322
    target 95
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa371"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 326
    source 134
    target 325
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa373"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 327
    source 325
    target 73
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa375"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 329
    source 34
    target 328
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa31"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 330
    source 328
    target 134
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa373"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 332
    source 62
    target 331
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa38"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 333
    source 331
    target 111
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa41"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 334
    source 331
    target 5
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa10"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 336
    source 94
    target 335
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa351"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 337
    source 335
    target 93
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa352"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 338
    source 104
    target 335
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR28"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa350"
    ]
  ]
  edge [
    id 340
    source 96
    target 339
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa2"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 341
    source 339
    target 26
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa1"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 342
    source 127
    target 339
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR29"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa8"
    ]
  ]
  edge [
    id 344
    source 118
    target 343
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa291"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 345
    source 91
    target 343
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa293"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 346
    source 343
    target 122
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa290"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 347
    source 343
    target 33
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa292"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 348
    source 148
    target 343
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR30"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa26"
    ]
  ]
  edge [
    id 350
    source 92
    target 349
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa199"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 351
    source 349
    target 54
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa200"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 352
    source 349
    target 56
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa228"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 353
    source 47
    target 349
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR31"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa16"
    ]
  ]
  edge [
    id 355
    source 71
    target 354
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa317"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 356
    source 354
    target 89
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa316"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 357
    source 152
    target 354
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR32"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa339"
    ]
  ]
  edge [
    id 359
    source 11
    target 358
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa338"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 360
    source 147
    target 358
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa336"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 361
    source 358
    target 59
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa334"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 362
    source 358
    target 10
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa337"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 363
    source 98
    target 358
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR33"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa269"
    ]
  ]
  edge [
    id 365
    source 111
    target 364
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa41"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 366
    source 40
    target 364
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa202"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 367
    source 22
    target 364
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa190"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 368
    source 364
    target 92
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa199"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 369
    source 364
    target 84
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa40"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 370
    source 364
    target 5
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa10"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 371
    source 150
    target 364
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR34"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa207"
    ]
  ]
  edge [
    id 373
    source 119
    target 372
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa12"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 374
    source 372
    target 82
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa28"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 375
    source 372
    target 5
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa10"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 377
    source 140
    target 376
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa32"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 378
    source 5
    target 376
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa10"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 379
    source 376
    target 34
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa31"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 380
    source 376
    target 136
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa29"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 381
    source 21
    target 376
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR35"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa34"
    ]
  ]
  edge [
    id 383
    source 1
    target 382
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa17"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 384
    source 51
    target 382
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa20"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 385
    source 382
    target 136
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa29"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 386
    source 85
    target 382
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR36"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa19"
    ]
  ]
  edge [
    id 388
    source 32
    target 387
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa224"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 389
    source 50
    target 387
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa227"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 390
    source 387
    target 92
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa199"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 391
    source 387
    target 22
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa190"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 392
    source 387
    target 58
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa217"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 393
    source 44
    target 387
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR37"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa215"
    ]
  ]
  edge [
    id 395
    source 87
    target 394
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa102"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 396
    source 394
    target 123
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa101"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 397
    source 23
    target 394
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR38"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa329"
    ]
  ]
  edge [
    id 399
    source 37
    target 398
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa222"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 400
    source 55
    target 398
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa233"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 401
    source 398
    target 43
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa221"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 402
    source 398
    target 49
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa234"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 403
    source 3
    target 398
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR39"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa209"
    ]
  ]
  edge [
    id 405
    source 1
    target 404
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa17"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 406
    source 51
    target 404
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa20"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 407
    source 404
    target 119
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa12"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 408
    source 404
    target 136
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa29"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 409
    source 74
    target 404
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR40"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa1"
    ]
  ]
  edge [
    id 410
    source 67
    target 404
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR41"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa13"
    ]
  ]
  edge [
    id 411
    source 12
    target 404
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR42"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa16"
    ]
  ]
  edge [
    id 412
    source 36
    target 404
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR43"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa14"
    ]
  ]
  edge [
    id 414
    source 79
    target 413
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa355"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 415
    source 81
    target 413
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa4"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 416
    source 24
    target 413
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa356"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 417
    source 413
    target 65
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa361"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 418
    source 413
    target 5
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa10"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 419
    source 413
    target 20
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa357"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 420
    source 413
    target 28
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa241"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 421
    source 413
    target 120
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa362"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 422
    source 93
    target 413
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR44"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa352"
    ]
  ]
  edge [
    id 424
    source 34
    target 423
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa31"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 425
    source 423
    target 64
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa374"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 427
    source 79
    target 426
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa355"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 428
    source 108
    target 426
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa5"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 429
    source 426
    target 14
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa39"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 431
    source 123
    target 430
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa101"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 432
    source 53
    target 430
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa94"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 433
    source 430
    target 18
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa100"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 434
    source 430
    target 7
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa99"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 435
    source 430
    target 115
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa98"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 436
    source 430
    target 48
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa105"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 437
    source 114
    target 430
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR45"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa106"
    ]
  ]
  edge [
    id 439
    source 112
    target 438
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa256"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 440
    source 29
    target 438
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa257"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 441
    source 438
    target 102
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa260"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 442
    source 438
    target 4
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa262"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 443
    source 52
    target 438
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR46"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa35"
    ]
  ]
  edge [
    id 445
    source 26
    target 444
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa1"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 446
    source 81
    target 444
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa4"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 447
    source 444
    target 45
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa11"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 448
    source 17
    target 444
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR47"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa7"
    ]
  ]
  edge [
    id 450
    source 109
    target 449
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa286"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 451
    source 19
    target 449
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa288"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 452
    source 103
    target 449
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa284"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 453
    source 449
    target 88
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa285"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 454
    source 449
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa287"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 455
    source 30
    target 449
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR48"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa289"
    ]
  ]
  edge [
    id 457
    source 34
    target 456
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa31"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 458
    source 456
    target 41
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa35"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 460
    source 45
    target 459
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa11"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 461
    source 5
    target 459
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa10"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 462
    source 459
    target 1
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa17"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 463
    source 459
    target 106
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa15"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 464
    source 46
    target 459
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR49"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa18"
    ]
  ]
  edge [
    id 466
    source 1
    target 465
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa17"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 467
    source 81
    target 465
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa4"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 468
    source 24
    target 465
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa356"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 469
    source 22
    target 465
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa190"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 470
    source 465
    target 140
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa32"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 471
    source 465
    target 5
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa10"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 472
    source 465
    target 28
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa241"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 473
    source 143
    target 465
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR50"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa377"
    ]
  ]
  edge [
    id 475
    source 70
    target 474
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa378"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 476
    source 24
    target 474
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa356"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 477
    source 81
    target 474
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa4"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 478
    source 474
    target 27
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa382"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 479
    source 474
    target 28
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa241"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 480
    source 474
    target 22
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa190"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 481
    source 474
    target 153
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa64"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 482
    source 128
    target 474
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR51"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa60"
    ]
  ]
  edge [
    id 484
    source 43
    target 483
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa221"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 485
    source 40
    target 483
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa202"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 486
    source 483
    target 110
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa193"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 487
    source 483
    target 92
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa199"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 488
    source 75
    target 483
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR52"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa206"
    ]
  ]
  edge [
    id 490
    source 43
    target 489
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa221"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 491
    source 72
    target 489
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa232"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 492
    source 489
    target 35
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa220"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 493
    source 489
    target 144
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa231"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 494
    source 69
    target 489
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR53"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa214"
    ]
  ]
  edge [
    id 498
    source 78
    target 495
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR54"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa62"
    ]
  ]
  edge [
    id 499
    source 153
    target 495
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR55"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa64"
    ]
  ]
  edge [
    id 500
    source 34
    target 495
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR56"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa31"
    ]
  ]
  edge [
    id 502
    source 216
    target 501
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa59"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 503
    source 501
    target 271
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa59"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 504
    source 501
    target 216
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa57"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 506
    source 317
    target 505
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa65"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 507
    source 505
    target 210
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa65"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 508
    source 505
    target 317
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa58"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 510
    source 509
    target 495
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa340"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 511
    source 495
    target 509
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa341"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 512
    source 509
    target 213
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa341"
      stoichiometry "1.0"
    ]
  ]
]
