# generated with VANTED V2.8.0 at Tue Apr 27 20:00:43 CEST 2021
graph [
  background_coloring "true"
  cluster_colors "255,128,128,255:0,0,0,255;230,255,128,255:0,0,0,255;128,255,179,255:0,0,0,255;128,178,255,255:0,0,0,255;230,128,255,255:0,0,0,255"
  clusterbackground_fill_outer_region "false"
  clusterbackground_grid 50.0
  clusterbackground_low_alpha 0.2
  clusterbackground_radius 200.0
  clusterbackground_space_fill "true"
  graphbackgroundcolor "#ffffff"
  sbml [
    model_meta_id "Covid_19_apoptosis_21_04_2020"
    model_non_rdf_annotation ""
    model_notes ""
    model_sbml_id "Covid_19_apoptosis_21_04_2020"
    namespace "xmlns:celldesigner=\"http://www.sbml.org/2001/ns/celldesigner\" "
    sbml_level "2"
    sbml_version "4"
  ]
  sbml_compartment_default [
    sbml_compartment_default_id "default"
    sbml_compartment_default_meta_id "default"
    sbml_compartment_default_non_rdf_annotation ""
    sbml_compartment_default_notes ""
    sbml_compartment_default_size "1.0"
    sbml_compartment_default_units "volume"
  ]
  sbml_compartment_s_id_ca4 [
    sbml_compartment_s_id_ca4_annotation ""
    sbml_compartment_s_id_ca4_id "s_id_ca4"
    sbml_compartment_s_id_ca4_meta_id "s_id_ca4"
    sbml_compartment_s_id_ca4_name "cell_space_membrane"
    sbml_compartment_s_id_ca4_non_rdf_annotation ""
    sbml_compartment_s_id_ca4_notes ""
    sbml_compartment_s_id_ca4_outside "s_id_ca7"
    sbml_compartment_s_id_ca4_size "1.0"
    sbml_compartment_s_id_ca4_units "volume"
  ]
  sbml_compartment_s_id_ca5 [
    sbml_compartment_s_id_ca5_annotation ""
    sbml_compartment_s_id_ca5_id "s_id_ca5"
    sbml_compartment_s_id_ca5_meta_id "s_id_ca5"
    sbml_compartment_s_id_ca5_name "cell"
    sbml_compartment_s_id_ca5_non_rdf_annotation ""
    sbml_compartment_s_id_ca5_notes ""
    sbml_compartment_s_id_ca5_outside "s_id_ca7"
    sbml_compartment_s_id_ca5_size "1.0"
    sbml_compartment_s_id_ca5_units "volume"
  ]
  sbml_compartment_s_id_ca6 [
    sbml_compartment_s_id_ca6_annotation ""
    sbml_compartment_s_id_ca6_id "s_id_ca6"
    sbml_compartment_s_id_ca6_meta_id "s_id_ca6"
    sbml_compartment_s_id_ca6_name "mitochondrion"
    sbml_compartment_s_id_ca6_non_rdf_annotation ""
    sbml_compartment_s_id_ca6_notes ""
    sbml_compartment_s_id_ca6_outside "s_id_ca5"
    sbml_compartment_s_id_ca6_size "1.0"
    sbml_compartment_s_id_ca6_units "volume"
  ]
  sbml_compartment_s_id_ca7 [
    sbml_compartment_s_id_ca7_id "s_id_ca7"
    sbml_compartment_s_id_ca7_meta_id "s_id_ca7"
    sbml_compartment_s_id_ca7_name "extracellular_space_space"
    sbml_compartment_s_id_ca7_non_rdf_annotation ""
    sbml_compartment_s_id_ca7_notes ""
    sbml_compartment_s_id_ca7_outside "default"
    sbml_compartment_s_id_ca7_size "1.0"
    sbml_compartment_s_id_ca7_units "volume"
  ]
  sbml_unit_definition_1 [
    sbml_unit_definition_1_id "substance"
    sbml_unit_definition_1_name "substance"
    sbml_unit_definition_1_sub_unit_1_ "(1.0 * 10^0 * mole)^1.0"
    sbml_unit_definition_1unit "(1.0 * 10^0 * mole)^1.0"
  ]
  sbml_unit_definition_2 [
    sbml_unit_definition_2_id "length"
    sbml_unit_definition_2_name "length"
    sbml_unit_definition_2_sub_unit_1_ "(1.0 * 10^0 * metre)^1.0"
    sbml_unit_definition_2unit "(1.0 * 10^0 * metre)^1.0"
  ]
  sbml_unit_definition_3 [
    sbml_unit_definition_3_id "volume"
    sbml_unit_definition_3_name "volume"
    sbml_unit_definition_3_sub_unit_1_ "(1.0 * 10^0 * litre)^1.0"
    sbml_unit_definition_3unit "(1.0 * 10^0 * litre)^1.0"
  ]
  sbml_unit_definition_4 [
    sbml_unit_definition_4_id "time"
    sbml_unit_definition_4_name "time"
    sbml_unit_definition_4_sub_unit_1_ "(1.0 * 10^0 * second)^1.0"
    sbml_unit_definition_4unit "(1.0 * 10^0 * second)^1.0"
  ]
  sbml_unit_definition_5 [
    sbml_unit_definition_5_id "area"
    sbml_unit_definition_5_name "area"
    sbml_unit_definition_5_sub_unit_1_ "(1.0 * 10^0 * metre)^2.0"
    sbml_unit_definition_5unit "(1.0 * 10^0 * metre)^2.0"
  ]
  directed 1
  node [
    id 1
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 200.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "casp9__cell__pro__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa47"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa47"
      minerva_name "CASP9"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CASP9"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_032996"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/842"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1511"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000132906"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.22.62"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/P55211"
      minerva_ref_type__resource1 "HGNC_SYMBOL__CASP9"
      minerva_ref_type__resource2 "REFSEQ__NM_032996"
      minerva_ref_type__resource3 "ENTREZ__842"
      minerva_ref_type__resource4 "HGNC__1511"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000132906"
      minerva_ref_type__resource6 "EC__3.4.22.62"
      minerva_ref_type__resource7 "UNIPROT__P55211"
      minerva_structuralState "pro"
      minerva_type "Protein"
      minerva_x 860.0
      minerva_y 675.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa47"
      species_meta_id "s_id_sa47"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 2
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 300.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "bad_slash_bbc3_slash_bcl2l11__cell__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa3"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa3"
      minerva_name "BAD/BBC3/BCL2L11"
      minerva_state1 "active"
      minerva_type "Complex"
      minerva_x 1219.0
      minerva_y 690.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa3"
      species_meta_id "s_id_csa3"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 3
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 400.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "bad_slash_bbc3_slash_bcl2l11__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa9"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa9"
      minerva_name "BAD/BBC3/BCL2L11"
      minerva_type "Complex"
      minerva_x 1219.0
      minerva_y 405.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa9"
      species_meta_id "s_id_csa9"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 4
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 500.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "m__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa42"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa42"
      minerva_name "M"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/APO40582"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/pubmed/16845612"
      minerva_ref_type__resource1 "NCBI_PROTEIN__APO40582"
      minerva_ref_type__resource2 "PUBMED__16845612"
      minerva_ref_type__resource3 "UNIPROT__M"
      minerva_type "Protein"
      minerva_x 712.0
      minerva_y 681.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa42"
      species_meta_id "s_id_sa42"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 5
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 600.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "casp9__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa18"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa18"
      minerva_name "CASP9"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CASP9"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_032996"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/842"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1511"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000132906"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.22.62"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/P55211"
      minerva_ref_type__resource1 "HGNC_SYMBOL__CASP9"
      minerva_ref_type__resource2 "REFSEQ__NM_032996"
      minerva_ref_type__resource3 "ENTREZ__842"
      minerva_ref_type__resource4 "HGNC__1511"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000132906"
      minerva_ref_type__resource6 "EC__3.4.22.62"
      minerva_ref_type__resource7 "UNIPROT__P55211"
      minerva_type "Protein"
      minerva_x 161.125
      minerva_y 531.375
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa18"
      species_meta_id "s_id_sa18"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 6
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 700.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "bid__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa22"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa22"
      minerva_name "BID"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BID"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/637"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_197966"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P55957"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000015475"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1050"
      minerva_ref_type__resource1 "HGNC_SYMBOL__BID"
      minerva_ref_type__resource2 "ENTREZ__637"
      minerva_ref_type__resource3 "REFSEQ__NM_197966"
      minerva_ref_type__resource4 "UNIPROT__P55957"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000015475"
      minerva_ref_type__resource6 "HGNC__1050"
      minerva_type "Protein"
      minerva_x 682.125
      minerva_y 429.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa22"
      species_meta_id "s_id_sa22"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 7
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 800.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "bad__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa78"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa78"
      minerva_name "BAD"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_032989"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/Q92934"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/572"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BAD"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000002330"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/936"
      minerva_ref_type__resource1 "REFSEQ__NM_032989"
      minerva_ref_type__resource2 "UNIPROT__Q92934"
      minerva_ref_type__resource3 "ENTREZ__572"
      minerva_ref_type__resource4 "HGNC_SYMBOL__BAD"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000002330"
      minerva_ref_type__resource6 "HGNC__936"
      minerva_type "Protein"
      minerva_x 682.125
      minerva_y 486.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa78"
      species_meta_id "s_id_sa78"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 8
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 900.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "casp8__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa13"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa13"
      minerva_name "CASP8"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1509"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_001228"
      minerva_ref_link11 "https://www.ensembl.org/id/ENSG00000064012"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CASP8"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/Q14790"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/Q14790"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CASP8"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/841"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/841"
      minerva_ref_link8 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.22.61"
      minerva_ref_link9 "https://doi.org/10.1038/s41392-020-00334-0"
      minerva_ref_type__resource1 "HGNC__1509"
      minerva_ref_type__resource10 "REFSEQ__NM_001228"
      minerva_ref_type__resource11 "ENSEMBL__ENSG00000064012"
      minerva_ref_type__resource2 "HGNC_SYMBOL__CASP8"
      minerva_ref_type__resource3 "UNIPROT__Q14790"
      minerva_ref_type__resource4 "UNIPROT__Q14790"
      minerva_ref_type__resource5 "HGNC_SYMBOL__CASP8"
      minerva_ref_type__resource6 "ENTREZ__841"
      minerva_ref_type__resource7 "ENTREZ__841"
      minerva_ref_type__resource8 "EC__3.4.22.61"
      minerva_ref_type__resource9 "DOI__10.1038/s41392-020-00334-0"
      minerva_type "Protein"
      minerva_x 161.125
      minerva_y 441.375
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa13"
      species_meta_id "s_id_sa13"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 9
    zlevel -1

    cluster [
      cluster "s_id_ca4"
    ]
    graphics [
      x 1000.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "fas_slash_fasl__cell_space_membrane__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa2"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa2"
      minerva_name "FAS/FASL"
      minerva_type "Complex"
      minerva_x 410.5
      minerva_y 139.875
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca4"
      compartment_name "cell_space_membrane"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa2"
      species_meta_id "s_id_csa2"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 10
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 1100.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "casp3__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa15"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa15"
      minerva_name "CASP3"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_004346"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/836"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.22.56"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000164305"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1504"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P42574"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/pubmed/32555321"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CASP3"
      minerva_ref_type__resource1 "REFSEQ__NM_004346"
      minerva_ref_type__resource2 "ENTREZ__836"
      minerva_ref_type__resource3 "EC__3.4.22.56"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000164305"
      minerva_ref_type__resource5 "HGNC__1504"
      minerva_ref_type__resource6 "UNIPROT__P42574"
      minerva_ref_type__resource7 "PUBMED__32555321"
      minerva_ref_type__resource8 "HGNC_SYMBOL__CASP3"
      minerva_type "Protein"
      minerva_x 161.125
      minerva_y 651.375
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa15"
      species_meta_id "s_id_sa15"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 11
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 1200.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "bcl2_slash_mcl1_slash_bcl2l1__cell__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa11"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa11"
      minerva_name "BCL2/MCL1/BCL2L1"
      minerva_state1 "active"
      minerva_type "Complex"
      minerva_x 1068.5
      minerva_y 548.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa11"
      species_meta_id "s_id_csa11"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 12
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 200.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf9b__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa77"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa77"
      minerva_name "Orf9b"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/1489679"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P59636"
      minerva_ref_type__resource1 "ENTREZ__1489679"
      minerva_ref_type__resource2 "UNIPROT__P59636"
      minerva_type "Protein"
      minerva_x 290.0
      minerva_y 1050.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa77"
      species_meta_id "s_id_sa77"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 13
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 300.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf3b__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa72"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa72"
      minerva_name "Orf3b"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P59633"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/1489670"
      minerva_ref_type__resource1 "UNIPROT__P59633"
      minerva_ref_type__resource2 "ENTREZ__1489670"
      minerva_type "Protein"
      minerva_x 290.0
      minerva_y 915.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa72"
      species_meta_id "s_id_sa72"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 14
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 400.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "apoptosis__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa41"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa41"
      minerva_name "Apoptosis"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link2 "http://id.nlm.nih.gov/mesh/D017209"
      minerva_ref_link3 "https://doi.org/10.1007/s10495-021-01656-2"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource2 "MESH_2012__D017209"
      minerva_ref_type__resource3 "DOI__10.1007/s10495-021-01656-2"
      minerva_type "Phenotype"
      minerva_x 560.0
      minerva_y 1035.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa41"
      species_meta_id "s_id_sa41"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 15
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 500.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tradd_slash_fadd__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa6"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa6"
      minerva_name "TRADD/FADD"
      minerva_type "Complex"
      minerva_x 880.625
      minerva_y 292.875
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa6"
      species_meta_id "s_id_csa6"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 16
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 600.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "bid__cell__tbid__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa23"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa23"
      minerva_name "BID"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BID"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/637"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_197966"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P55957"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000015475"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1050"
      minerva_ref_type__resource1 "HGNC_SYMBOL__BID"
      minerva_ref_type__resource2 "ENTREZ__637"
      minerva_ref_type__resource3 "REFSEQ__NM_197966"
      minerva_ref_type__resource4 "UNIPROT__P55957"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000015475"
      minerva_ref_type__resource6 "HGNC__1050"
      minerva_structuralState "tBID"
      minerva_type "Protein"
      minerva_x 849.125
      minerva_y 429.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa23"
      species_meta_id "s_id_sa23"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 17
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 700.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "fadd__cell__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa10"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa10"
      minerva_name "FADD"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/3573"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8772"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/Q13158"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000168040"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003824"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=FADD"
      minerva_ref_type__resource1 "HGNC__3573"
      minerva_ref_type__resource2 "ENTREZ__8772"
      minerva_ref_type__resource3 "UNIPROT__Q13158"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000168040"
      minerva_ref_type__resource5 "REFSEQ__NM_003824"
      minerva_ref_type__resource6 "HGNC_SYMBOL__FADD"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 572.0
      minerva_y 293.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa10"
      species_meta_id "s_id_sa10"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 18
    zlevel -1

    cluster [
      cluster "s_id_ca4"
    ]
    graphics [
      x 800.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "fas__cell_space_membrane__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa5"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa5"
      minerva_former_symbols "APT1; FAS1; TNFRSF6"
      minerva_name "FAS"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11920"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P25445"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_000043"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000026103"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/355"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=FAS"
      minerva_ref_type__resource1 "HGNC__11920"
      minerva_ref_type__resource2 "UNIPROT__P25445"
      minerva_ref_type__resource3 "REFSEQ__NM_000043"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000026103"
      minerva_ref_type__resource5 "ENTREZ__355"
      minerva_ref_type__resource6 "HGNC_SYMBOL__FAS"
      minerva_type "Protein"
      minerva_x 170.0
      minerva_y 142.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca4"
      compartment_name "cell_space_membrane"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa5"
      species_meta_id "s_id_sa5"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 19
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 900.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cycs__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa25"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa25"
      minerva_name "CYCS"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/19986"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P99999"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/54205"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CYCS"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000172115"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_018947"
      minerva_ref_type__resource1 "HGNC__19986"
      minerva_ref_type__resource2 "UNIPROT__P99999"
      minerva_ref_type__resource3 "ENTREZ__54205"
      minerva_ref_type__resource4 "HGNC_SYMBOL__CYCS"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000172115"
      minerva_ref_type__resource6 "REFSEQ__NM_018947"
      minerva_type "Protein"
      minerva_x 1010.0
      minerva_y 760.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa25"
      species_meta_id "s_id_sa25"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 20
    zlevel -1

    cluster [
      cluster "s_id_ca6"
    ]
    graphics [
      x 1000.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cycs__mitochondrion__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa24"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa24"
      minerva_name "CYCS"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/19986"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P99999"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/54205"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CYCS"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000172115"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_018947"
      minerva_ref_type__resource1 "HGNC__19986"
      minerva_ref_type__resource2 "UNIPROT__P99999"
      minerva_ref_type__resource3 "ENTREZ__54205"
      minerva_ref_type__resource4 "HGNC_SYMBOL__CYCS"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000172115"
      minerva_ref_type__resource6 "REFSEQ__NM_018947"
      minerva_type "Protein"
      minerva_x 1010.0
      minerva_y 920.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca6"
      compartment_name "mitochondrion"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa24"
      species_meta_id "s_id_sa24"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 21
    zlevel -1

    cluster [
      cluster "s_id_ca4"
    ]
    graphics [
      x 1100.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tnfrsf1a__cell_space_membrane__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa8"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa8"
      minerva_former_symbols "TNFR1"
      minerva_name "TNFRSF1A"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7132"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_001065"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000067182"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P19438"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TNFRSF1A"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11916"
      minerva_ref_type__resource1 "ENTREZ__7132"
      minerva_ref_type__resource2 "REFSEQ__NM_001065"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000067182"
      minerva_ref_type__resource4 "UNIPROT__P19438"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TNFRSF1A"
      minerva_ref_type__resource6 "HGNC__11916"
      minerva_type "Protein"
      minerva_x 1040.0
      minerva_y 142.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca4"
      compartment_name "cell_space_membrane"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa8"
      species_meta_id "s_id_sa8"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 22
    zlevel -1

    cluster [
      cluster "s_id_ca7"
    ]
    graphics [
      x 1200.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "faslg__extracellular_space_space__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2"
      minerva_former_symbols "APT1LG1; TNFSF6"
      minerva_name "FASLG"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=FASLG"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/356"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_000639"
      minerva_ref_link4 "https://doi.org/10.1101/2020.12.04.412494"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000117560"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P48023"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11936"
      minerva_ref_type__resource1 "HGNC_SYMBOL__FASLG"
      minerva_ref_type__resource2 "ENTREZ__356"
      minerva_ref_type__resource3 "REFSEQ__NM_000639"
      minerva_ref_type__resource4 "DOI__10.1101/2020.12.04.412494"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000117560"
      minerva_ref_type__resource6 "UNIPROT__P48023"
      minerva_ref_type__resource7 "HGNC__11936"
      minerva_type "Protein"
      minerva_x 253.0
      minerva_y 41.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca7"
      compartment_name "extracellular_space_space"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2"
      species_meta_id "s_id_sa2"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 23
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 200.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "casp3__cell__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa17"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa17"
      minerva_name "CASP3"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/836"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CASP3"
      minerva_ref_link11 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CASP3"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_004346"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/836"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.22.56"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000164305"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/pubmed/32555321"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1504"
      minerva_ref_link8 "https://purl.uniprot.org/uniprot/P42574"
      minerva_ref_link9 "https://purl.uniprot.org/uniprot/P42574"
      minerva_ref_type__resource1 "ENTREZ__836"
      minerva_ref_type__resource10 "HGNC_SYMBOL__CASP3"
      minerva_ref_type__resource11 "HGNC_SYMBOL__CASP3"
      minerva_ref_type__resource2 "REFSEQ__NM_004346"
      minerva_ref_type__resource3 "ENTREZ__836"
      minerva_ref_type__resource4 "EC__3.4.22.56"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000164305"
      minerva_ref_type__resource6 "PUBMED__32555321"
      minerva_ref_type__resource7 "HGNC__1504"
      minerva_ref_type__resource8 "UNIPROT__P42574"
      minerva_ref_type__resource9 "UNIPROT__P42574"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 361.125
      minerva_y 651.375
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa17"
      species_meta_id "s_id_sa17"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 24
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 300.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "casp7__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa39"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa39"
      minerva_name "CASP7"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1508"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CASP7"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/840"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.22.60"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_033338"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000165806"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/P55210"
      minerva_ref_type__resource1 "HGNC__1508"
      minerva_ref_type__resource2 "HGNC_SYMBOL__CASP7"
      minerva_ref_type__resource3 "ENTREZ__840"
      minerva_ref_type__resource4 "EC__3.4.22.60"
      minerva_ref_type__resource5 "REFSEQ__NM_033338"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000165806"
      minerva_ref_type__resource7 "UNIPROT__P55210"
      minerva_type "Protein"
      minerva_x 161.125
      minerva_y 771.375
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa39"
      species_meta_id "s_id_sa39"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 25
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 400.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "akt1__cell__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa29"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa29"
      minerva_name "AKT1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/207"
      minerva_ref_link2 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.1"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/391"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AKT1"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_005163"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P31749"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000142208"
      minerva_ref_type__resource1 "ENTREZ__207"
      minerva_ref_type__resource2 "EC__2.7.11.1"
      minerva_ref_type__resource3 "HGNC__391"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AKT1"
      minerva_ref_type__resource5 "REFSEQ__NM_005163"
      minerva_ref_type__resource6 "UNIPROT__P31749"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000142208"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 815.0
      minerva_y 615.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa29"
      species_meta_id "s_id_sa29"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 26
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 500.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "akt1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa28"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa28"
      minerva_name "AKT1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/207"
      minerva_ref_link2 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.1"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/391"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AKT1"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_005163"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P31749"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000142208"
      minerva_ref_type__resource1 "ENTREZ__207"
      minerva_ref_type__resource2 "EC__2.7.11.1"
      minerva_ref_type__resource3 "HGNC__391"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AKT1"
      minerva_ref_type__resource5 "REFSEQ__NM_005163"
      minerva_ref_type__resource6 "UNIPROT__P31749"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000142208"
      minerva_type "Protein"
      minerva_x 610.0
      minerva_y 615.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa28"
      species_meta_id "s_id_sa28"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 27
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 600.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa76"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa76"
      minerva_name "S"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/1489668"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/pubmed/32275855"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/pubmed/32075877"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/pubmed/32155444"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/pubmed/32225176"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P59594"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=S"
      minerva_ref_type__resource1 "ENTREZ__1489668"
      minerva_ref_type__resource2 "PUBMED__32275855"
      minerva_ref_type__resource3 "PUBMED__32075877"
      minerva_ref_type__resource4 "PUBMED__32155444"
      minerva_ref_type__resource5 "PUBMED__32225176"
      minerva_ref_type__resource6 "UNIPROT__P59594"
      minerva_ref_type__resource7 "HGNC_SYMBOL__S"
      minerva_type "Protein"
      minerva_x 290.0
      minerva_y 825.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa76"
      species_meta_id "s_id_sa76"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 28
    zlevel -1

    cluster [
      cluster "s_id_ca6"
    ]
    graphics [
      x 700.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "bax__mitochondrion__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa31"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa31"
      minerva_name "BAX"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_138763"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/959"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000087088"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BAX"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/581"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/Q07812"
      minerva_ref_type__resource1 "REFSEQ__NM_138763"
      minerva_ref_type__resource2 "HGNC__959"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000087088"
      minerva_ref_type__resource4 "HGNC_SYMBOL__BAX"
      minerva_ref_type__resource5 "ENTREZ__581"
      minerva_ref_type__resource6 "UNIPROT__Q07812"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 1180.0
      minerva_y 1050.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca6"
      compartment_name "mitochondrion"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa31"
      species_meta_id "s_id_sa31"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 29
    zlevel -1

    cluster [
      cluster "s_id_ca6"
    ]
    graphics [
      x 800.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf3a__mitochondrion__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa48"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa48"
      minerva_name "Orf3a"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P59637"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/YP_009724391.1"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/1489671"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=E"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/pubmed/33100263"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/pubmed/32555321"
      minerva_ref_type__resource1 "UNIPROT__P59637"
      minerva_ref_type__resource2 "NCBI_PROTEIN__YP_009724391.1"
      minerva_ref_type__resource3 "ENTREZ__1489671"
      minerva_ref_type__resource4 "HGNC_SYMBOL__E"
      minerva_ref_type__resource5 "PUBMED__33100263"
      minerva_ref_type__resource6 "PUBMED__32555321"
      minerva_type "Protein"
      minerva_x 786.0
      minerva_y 855.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca6"
      compartment_name "mitochondrion"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa48"
      species_meta_id "s_id_sa48"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 30
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 900.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf3a__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa92"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa92"
      minerva_name "Orf3a"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P59637"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/YP_009724391.1"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/1489671"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=E"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/pubmed/33100263"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/pubmed/32555321"
      minerva_ref_type__resource1 "UNIPROT__P59637"
      minerva_ref_type__resource2 "NCBI_PROTEIN__YP_009724391.1"
      minerva_ref_type__resource3 "ENTREZ__1489671"
      minerva_ref_type__resource4 "HGNC_SYMBOL__E"
      minerva_ref_type__resource5 "PUBMED__33100263"
      minerva_ref_type__resource6 "PUBMED__32555321"
      minerva_type "Protein"
      minerva_x 160.0
      minerva_y 360.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa92"
      species_meta_id "s_id_sa92"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 31
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 1000.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tradd__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa44"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa44"
      minerva_name "TRADD"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/8717"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000102871"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_001323552"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/Q15628"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12030"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRADD"
      minerva_ref_type__resource1 "ENTREZ__8717"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000102871"
      minerva_ref_type__resource3 "REFSEQ__NM_001323552"
      minerva_ref_type__resource4 "UNIPROT__Q15628"
      minerva_ref_type__resource5 "HGNC__12030"
      minerva_ref_type__resource6 "HGNC_SYMBOL__TRADD"
      minerva_type "Protein"
      minerva_x 689.125
      minerva_y 246.375
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa44"
      species_meta_id "s_id_sa44"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 32
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 1100.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "casp7__cell__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa40"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa40"
      minerva_name "CASP7"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1508"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CASP7"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/840"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.22.60"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_033338"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000165806"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/P55210"
      minerva_ref_type__resource1 "HGNC__1508"
      minerva_ref_type__resource2 "HGNC_SYMBOL__CASP7"
      minerva_ref_type__resource3 "ENTREZ__840"
      minerva_ref_type__resource4 "EC__3.4.22.60"
      minerva_ref_type__resource5 "REFSEQ__NM_033338"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000165806"
      minerva_ref_type__resource7 "UNIPROT__P55210"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 407.125
      minerva_y 771.375
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa40"
      species_meta_id "s_id_sa40"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 33
    zlevel -1

    cluster [
      cluster "s_id_ca7"
    ]
    graphics [
      x 1200.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tnf__extracellular_space_space__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa3"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa3"
      minerva_former_symbols "TNFA"
      minerva_name "TNF"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_000594"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TNF"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P01375"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11892"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/7124"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000232810"
      minerva_ref_type__resource1 "REFSEQ__NM_000594"
      minerva_ref_type__resource2 "HGNC_SYMBOL__TNF"
      minerva_ref_type__resource3 "UNIPROT__P01375"
      minerva_ref_type__resource4 "HGNC__11892"
      minerva_ref_type__resource5 "ENTREZ__7124"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000232810"
      minerva_type "Protein"
      minerva_x 945.0
      minerva_y 41.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca7"
      compartment_name "extracellular_space_space"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa3"
      species_meta_id "s_id_sa3"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 34
    zlevel -1

    cluster [
      cluster "s_id_ca6"
    ]
    graphics [
      x 200.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "bid__mitochondrion__tbid__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa89"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa89"
      minerva_name "BID"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BID"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/637"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_197966"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P55957"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000015475"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1050"
      minerva_ref_type__resource1 "HGNC_SYMBOL__BID"
      minerva_ref_type__resource2 "ENTREZ__637"
      minerva_ref_type__resource3 "REFSEQ__NM_197966"
      minerva_ref_type__resource4 "UNIPROT__P55957"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000015475"
      minerva_ref_type__resource6 "HGNC__1050"
      minerva_structuralState "tBID"
      minerva_type "Protein"
      minerva_x 940.0
      minerva_y 975.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca6"
      compartment_name "mitochondrion"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa89"
      species_meta_id "s_id_sa89"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 35
    zlevel -1

    cluster [
      cluster "s_id_ca6"
    ]
    graphics [
      x 300.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "bax__mitochondrion__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa30"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa30"
      minerva_name "BAX"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_138763"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/959"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000087088"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BAX"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/581"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/Q07812"
      minerva_ref_type__resource1 "REFSEQ__NM_138763"
      minerva_ref_type__resource2 "HGNC__959"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000087088"
      minerva_ref_type__resource4 "HGNC_SYMBOL__BAX"
      minerva_ref_type__resource5 "ENTREZ__581"
      minerva_ref_type__resource6 "UNIPROT__Q07812"
      minerva_type "Protein"
      minerva_x 960.0
      minerva_y 1050.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca6"
      compartment_name "mitochondrion"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa30"
      species_meta_id "s_id_sa30"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 36
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 400.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "apaf1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa27"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa27"
      minerva_name "APAF1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/317"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/576"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_181861.1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=APAF1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/O14727"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000120868"
      minerva_ref_type__resource1 "ENTREZ__317"
      minerva_ref_type__resource2 "HGNC__576"
      minerva_ref_type__resource3 "REFSEQ__NM_181861.1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__APAF1"
      minerva_ref_type__resource5 "UNIPROT__O14727"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000120868"
      minerva_type "Protein"
      minerva_x 1000.0
      minerva_y 660.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa27"
      species_meta_id "s_id_sa27"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 37
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 500.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "casp9__cell__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa19"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa19"
      minerva_name "CASP9"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CASP9"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_032996"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/842"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1511"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000132906"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.22.62"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/P55211"
      minerva_ref_type__resource1 "HGNC_SYMBOL__CASP9"
      minerva_ref_type__resource2 "REFSEQ__NM_032996"
      minerva_ref_type__resource3 "ENTREZ__842"
      minerva_ref_type__resource4 "HGNC__1511"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000132906"
      minerva_ref_type__resource6 "EC__3.4.22.62"
      minerva_ref_type__resource7 "UNIPROT__P55211"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 509.125
      minerva_y 531.375
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa19"
      species_meta_id "s_id_sa19"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 38
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 600.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf7a__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa43"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa43"
      minerva_name "Orf7a"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ORF7a"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q19QW4"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/1489674"
      minerva_ref_type__resource1 "HGNC_SYMBOL__ORF7a"
      minerva_ref_type__resource2 "UNIPROT__Q19QW4"
      minerva_ref_type__resource3 "ENTREZ__1489674"
      minerva_type "Protein"
      minerva_x 944.0
      minerva_y 378.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa43"
      species_meta_id "s_id_sa43"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 39
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 700.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "apoptosome__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa5"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa5"
      minerva_name "Apoptosome"
      minerva_type "Complex"
      minerva_x 559.0
      minerva_y 767.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa5"
      species_meta_id "s_id_csa5"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 40
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 800.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "fadd__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa9"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa9"
      minerva_name "FADD"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/3573"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8772"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/Q13158"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000168040"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003824"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=FADD"
      minerva_ref_type__resource1 "HGNC__3573"
      minerva_ref_type__resource2 "ENTREZ__8772"
      minerva_ref_type__resource3 "UNIPROT__Q13158"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000168040"
      minerva_ref_type__resource5 "REFSEQ__NM_003824"
      minerva_ref_type__resource6 "HGNC_SYMBOL__FADD"
      minerva_type "Protein"
      minerva_x 251.0
      minerva_y 295.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa9"
      species_meta_id "s_id_sa9"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 41
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 900.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "e__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa69"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa69"
      minerva_name "E"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/32785274"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/pubmed/32818817"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P59637"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/1489671"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=E"
      minerva_ref_type__resource1 "PUBMED__32785274"
      minerva_ref_type__resource2 "PUBMED__32818817"
      minerva_ref_type__resource3 "UNIPROT__P59637"
      minerva_ref_type__resource4 "ENTREZ__1489671"
      minerva_ref_type__resource5 "HGNC_SYMBOL__E"
      minerva_type "Protein"
      minerva_x 944.0
      minerva_y 429.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa69"
      species_meta_id "s_id_sa69"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 42
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 1000.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mapk14__cell__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa50"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa50"
      minerva_former_symbols "CSBP1; CSBP2; CSPB1"
      minerva_name "MAPK14"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.24"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000112062"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6876"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/Q16539"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAPK14"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/1432"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_001315"
      minerva_ref_type__resource1 "EC__2.7.11.24"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000112062"
      minerva_ref_type__resource3 "HGNC__6876"
      minerva_ref_type__resource4 "UNIPROT__Q16539"
      minerva_ref_type__resource5 "HGNC_SYMBOL__MAPK14"
      minerva_ref_type__resource6 "ENTREZ__1432"
      minerva_ref_type__resource7 "REFSEQ__NM_001315"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 892.0
      minerva_y 795.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa50"
      species_meta_id "s_id_sa50"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 43
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 1100.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf8a__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa73"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa73"
      minerva_name "Orf8a"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/Q7TFA0"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/1489676"
      minerva_ref_type__resource1 "UNIPROT__Q7TFA0"
      minerva_ref_type__resource2 "ENTREZ__1489676"
      minerva_type "Protein"
      minerva_x 290.0
      minerva_y 960.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa73"
      species_meta_id "s_id_sa73"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 44
    zlevel -1

    cluster [
      cluster "s_id_ca4"
    ]
    graphics [
      x 1200.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tnf_slash_tnfrsf1a__cell_space_membrane__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa1"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa1"
      minerva_name "TNF/TNFRSF1A"
      minerva_type "Complex"
      minerva_x 760.5
      minerva_y 143.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca4"
      compartment_name "cell_space_membrane"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa1"
      species_meta_id "s_id_csa1"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 45
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 200.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf6__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa75"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa75"
      minerva_name "Orf6"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/P59634"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/1489673"
      minerva_ref_type__resource1 "UNIPROT__P59634"
      minerva_ref_type__resource2 "ENTREZ__1489673"
      minerva_type "Protein"
      minerva_x 290.0
      minerva_y 870.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa75"
      species_meta_id "s_id_sa75"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 46
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 300.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mapk14__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa49"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa49"
      minerva_former_symbols "CSBP1; CSBP2; CSPB1"
      minerva_name "MAPK14"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.24"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000112062"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6876"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/Q16539"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAPK14"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/1432"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_001315"
      minerva_ref_type__resource1 "EC__2.7.11.24"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000112062"
      minerva_ref_type__resource3 "HGNC__6876"
      minerva_ref_type__resource4 "UNIPROT__Q16539"
      minerva_ref_type__resource5 "HGNC_SYMBOL__MAPK14"
      minerva_ref_type__resource6 "ENTREZ__1432"
      minerva_ref_type__resource7 "REFSEQ__NM_001315"
      minerva_type "Protein"
      minerva_x 680.0
      minerva_y 795.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa49"
      species_meta_id "s_id_sa49"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 47
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 400.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "bcl2_slash_mcl1_slash_bcl2l1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa10"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa10"
      minerva_name "BCL2/MCL1/BCL2L1"
      minerva_type "Complex"
      minerva_x 1069.5
      minerva_y 299.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa10"
      species_meta_id "s_id_csa10"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 48
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 500.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "bad__cell__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa79"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa79"
      minerva_name "BAD"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_032989"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/Q92934"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/572"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BAD"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000002330"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/936"
      minerva_ref_type__resource1 "REFSEQ__NM_032989"
      minerva_ref_type__resource2 "UNIPROT__Q92934"
      minerva_ref_type__resource3 "ENTREZ__572"
      minerva_ref_type__resource4 "HGNC_SYMBOL__BAD"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000002330"
      minerva_ref_type__resource6 "HGNC__936"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 944.0
      minerva_y 484.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa79"
      species_meta_id "s_id_sa79"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 49
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 600.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "n__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa74"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa74"
      minerva_name "N"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/32654247"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/pubmed/33264373"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/pubmed/32416961"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/pubmed/16112641"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=N"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/pubmed/32363136"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P59595"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/pubmed/16845612"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/1489678"
      minerva_ref_type__resource1 "PUBMED__32654247"
      minerva_ref_type__resource2 "PUBMED__33264373"
      minerva_ref_type__resource3 "PUBMED__32416961"
      minerva_ref_type__resource4 "PUBMED__16112641"
      minerva_ref_type__resource5 "HGNC_SYMBOL__N"
      minerva_ref_type__resource6 "PUBMED__32363136"
      minerva_ref_type__resource7 "UNIPROT__P59595"
      minerva_ref_type__resource8 "PUBMED__16845612"
      minerva_ref_type__resource9 "ENTREZ__1489678"
      minerva_type "Protein"
      minerva_x 290.0
      minerva_y 1005.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa74"
      species_meta_id "s_id_sa74"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 50
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 700.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "casp8__cell__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa14"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa14"
      minerva_name "CASP8"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1509"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CASP8"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/Q14790"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/841"
      minerva_ref_link5 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.22.61"
      minerva_ref_link6 "https://doi.org/10.1038/s41392-020-00334-0"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_001228"
      minerva_ref_link8 "https://www.ensembl.org/id/ENSG00000064012"
      minerva_ref_type__resource1 "HGNC__1509"
      minerva_ref_type__resource2 "HGNC_SYMBOL__CASP8"
      minerva_ref_type__resource3 "UNIPROT__Q14790"
      minerva_ref_type__resource4 "ENTREZ__841"
      minerva_ref_type__resource5 "EC__3.4.22.61"
      minerva_ref_type__resource6 "DOI__10.1038/s41392-020-00334-0"
      minerva_ref_type__resource7 "REFSEQ__NM_001228"
      minerva_ref_type__resource8 "ENSEMBL__ENSG00000064012"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 361.125
      minerva_y 441.375
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa14"
      species_meta_id "s_id_sa14"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 51
    zlevel -1

    graphics [
      x 800.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re34"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BCL2"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000657"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/596"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P10415"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000171791"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000633"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/990"
      minerva_ref_type__resource1 "HGNC_SYMBOL__BCL2"
      minerva_ref_type__resource2 "REFSEQ__NM_000657"
      minerva_ref_type__resource3 "ENTREZ__596"
      minerva_ref_type__resource4 "UNIPROT__P10415"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000171791"
      minerva_ref_type__resource6 "REFSEQ__NM_000633"
      minerva_ref_type__resource7 "HGNC__990"
      minerva_type "State transition"
      minerva_x 813.0625
      minerva_y 485.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re34"
      reaction_meta_id "re34"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 55
    zlevel -1

    graphics [
      x 900.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re26"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BCL2"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000657"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/596"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P10415"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000171791"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000633"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/990"
      minerva_ref_type__resource1 "HGNC_SYMBOL__BCL2"
      minerva_ref_type__resource2 "REFSEQ__NM_000657"
      minerva_ref_type__resource3 "ENTREZ__596"
      minerva_ref_type__resource4 "UNIPROT__P10415"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000171791"
      minerva_ref_type__resource6 "REFSEQ__NM_000633"
      minerva_ref_type__resource7 "HGNC__990"
      minerva_type "Heterodimer association"
      minerva_x 878.0624999999999
      minerva_y 142.1875
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re26"
      reaction_meta_id "re26"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 59
    zlevel -1

    graphics [
      x 1000.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re30"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BCL2"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000657"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/596"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P10415"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000171791"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000633"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/990"
      minerva_ref_type__resource1 "HGNC_SYMBOL__BCL2"
      minerva_ref_type__resource2 "REFSEQ__NM_000657"
      minerva_ref_type__resource3 "ENTREZ__596"
      minerva_ref_type__resource4 "UNIPROT__P10415"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000171791"
      minerva_ref_type__resource6 "REFSEQ__NM_000633"
      minerva_ref_type__resource7 "HGNC__990"
      minerva_type "State transition"
      minerva_x 801.0
      minerva_y 763.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re30"
      reaction_meta_id "re30"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 65
    zlevel -1

    graphics [
      x 1100.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re8"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BCL2"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000657"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/596"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P10415"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000171791"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000633"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/990"
      minerva_ref_type__resource1 "HGNC_SYMBOL__BCL2"
      minerva_ref_type__resource2 "REFSEQ__NM_000657"
      minerva_ref_type__resource3 "ENTREZ__596"
      minerva_ref_type__resource4 "UNIPROT__P10415"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000171791"
      minerva_ref_type__resource6 "REFSEQ__NM_000633"
      minerva_ref_type__resource7 "HGNC__990"
      minerva_type "State transition"
      minerva_x 765.625
      minerva_y 429.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re8"
      reaction_meta_id "re8"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 69
    zlevel -1

    graphics [
      x 1200.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re14"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BCL2"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000657"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/596"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P10415"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000171791"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000633"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/990"
      minerva_ref_type__resource1 "HGNC_SYMBOL__BCL2"
      minerva_ref_type__resource2 "REFSEQ__NM_000657"
      minerva_ref_type__resource3 "ENTREZ__596"
      minerva_ref_type__resource4 "UNIPROT__P10415"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000171791"
      minerva_ref_type__resource6 "REFSEQ__NM_000633"
      minerva_ref_type__resource7 "HGNC__990"
      minerva_type "Physical stimulation"
      minerva_x 552.9733510634726
      minerva_y 937.2934617872966
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re14"
      reaction_meta_id "re14"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 72
    zlevel -1

    graphics [
      x 200.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re35"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BCL2"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000657"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/596"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P10415"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000171791"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000633"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/990"
      minerva_ref_type__resource1 "HGNC_SYMBOL__BCL2"
      minerva_ref_type__resource2 "REFSEQ__NM_000657"
      minerva_ref_type__resource3 "ENTREZ__596"
      minerva_ref_type__resource4 "UNIPROT__P10415"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000171791"
      minerva_ref_type__resource6 "REFSEQ__NM_000633"
      minerva_ref_type__resource7 "HGNC__990"
      minerva_type "Physical stimulation"
      minerva_x 560.052102031781
      minerva_y 924.4133816002131
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re35"
      reaction_meta_id "re35"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 75
    zlevel -1

    graphics [
      x 300.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re38"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BCL2"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000657"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/596"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P10415"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000171791"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000633"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/990"
      minerva_ref_type__resource1 "HGNC_SYMBOL__BCL2"
      minerva_ref_type__resource2 "REFSEQ__NM_000657"
      minerva_ref_type__resource3 "ENTREZ__596"
      minerva_ref_type__resource4 "UNIPROT__P10415"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000171791"
      minerva_ref_type__resource6 "REFSEQ__NM_000633"
      minerva_ref_type__resource7 "HGNC__990"
      minerva_type "Transport"
      minerva_x 940.0
      minerva_y 747.4999999999984
    ]
    sbml [
      reaction_id "re38"
      reaction_meta_id "re38"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 78
    zlevel -1

    graphics [
      x 400.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re29"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BCL2"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000657"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/596"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P10415"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000171791"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000633"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/990"
      minerva_ref_type__resource1 "HGNC_SYMBOL__BCL2"
      minerva_ref_type__resource2 "REFSEQ__NM_000657"
      minerva_ref_type__resource3 "ENTREZ__596"
      minerva_ref_type__resource4 "UNIPROT__P10415"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000171791"
      minerva_ref_type__resource6 "REFSEQ__NM_000633"
      minerva_ref_type__resource7 "HGNC__990"
      minerva_type "Physical stimulation"
      minerva_x 385.0
      minerva_y 1020.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re29"
      reaction_meta_id "re29"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 81
    zlevel -1

    graphics [
      x 500.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re16"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BCL2"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000657"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/596"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P10415"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000171791"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000633"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/990"
      minerva_ref_type__resource1 "HGNC_SYMBOL__BCL2"
      minerva_ref_type__resource2 "REFSEQ__NM_000657"
      minerva_ref_type__resource3 "ENTREZ__596"
      minerva_ref_type__resource4 "UNIPROT__P10415"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000171791"
      minerva_ref_type__resource6 "REFSEQ__NM_000633"
      minerva_ref_type__resource7 "HGNC__990"
      minerva_type "Heterodimer association"
      minerva_x 759.6250000000005
      minerva_y 292.125
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re16"
      reaction_meta_id "re16"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 86
    zlevel -1

    graphics [
      x 600.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re6"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BCL2"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000657"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/596"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P10415"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000171791"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000633"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/990"
      minerva_ref_type__resource1 "HGNC_SYMBOL__BCL2"
      minerva_ref_type__resource2 "REFSEQ__NM_000657"
      minerva_ref_type__resource3 "ENTREZ__596"
      minerva_ref_type__resource4 "UNIPROT__P10415"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000171791"
      minerva_ref_type__resource6 "REFSEQ__NM_000633"
      minerva_ref_type__resource7 "HGNC__990"
      minerva_type "State transition"
      minerva_x 261.125
      minerva_y 651.375
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re6"
      reaction_meta_id "re6"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 91
    zlevel -1

    graphics [
      x 700.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re27"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BCL2"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000657"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/596"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P10415"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000171791"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000633"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/990"
      minerva_ref_type__resource1 "HGNC_SYMBOL__BCL2"
      minerva_ref_type__resource2 "REFSEQ__NM_000657"
      minerva_ref_type__resource3 "ENTREZ__596"
      minerva_ref_type__resource4 "UNIPROT__P10415"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000171791"
      minerva_ref_type__resource6 "REFSEQ__NM_000633"
      minerva_ref_type__resource7 "HGNC__990"
      minerva_type "Physical stimulation"
      minerva_x 385.0
      minerva_y 975.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re27"
      reaction_meta_id "re27"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 94
    zlevel -1

    graphics [
      x 800.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re23"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BCL2"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000657"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/596"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P10415"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000171791"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000633"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/990"
      minerva_ref_type__resource1 "HGNC_SYMBOL__BCL2"
      minerva_ref_type__resource2 "REFSEQ__NM_000657"
      minerva_ref_type__resource3 "ENTREZ__596"
      minerva_ref_type__resource4 "UNIPROT__P10415"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000171791"
      minerva_ref_type__resource6 "REFSEQ__NM_000633"
      minerva_ref_type__resource7 "HGNC__990"
      minerva_type "State transition"
      minerva_x 1069.0
      minerva_y 423.75
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re23"
      reaction_meta_id "re23"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 100
    zlevel -1

    graphics [
      x 900.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re5"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BCL2"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000657"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/596"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P10415"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000171791"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000633"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/990"
      minerva_ref_type__resource1 "HGNC_SYMBOL__BCL2"
      minerva_ref_type__resource2 "REFSEQ__NM_000657"
      minerva_ref_type__resource3 "ENTREZ__596"
      minerva_ref_type__resource4 "UNIPROT__P10415"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000171791"
      minerva_ref_type__resource6 "REFSEQ__NM_000633"
      minerva_ref_type__resource7 "HGNC__990"
      minerva_type "State transition"
      minerva_x 261.125
      minerva_y 441.375
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re5"
      reaction_meta_id "re5"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 106
    zlevel -1

    graphics [
      x 1000.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re3"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BCL2"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000657"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/596"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P10415"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000171791"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000633"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/990"
      minerva_ref_type__resource1 "HGNC_SYMBOL__BCL2"
      minerva_ref_type__resource2 "REFSEQ__NM_000657"
      minerva_ref_type__resource3 "ENTREZ__596"
      minerva_ref_type__resource4 "UNIPROT__P10415"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000171791"
      minerva_ref_type__resource6 "REFSEQ__NM_000633"
      minerva_ref_type__resource7 "HGNC__990"
      minerva_type "State transition"
      minerva_x 411.5
      minerva_y 294.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re3"
      reaction_meta_id "re3"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 110
    zlevel -1

    graphics [
      x 1100.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re7"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BCL2"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000657"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/596"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P10415"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000171791"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000633"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/990"
      minerva_ref_type__resource1 "HGNC_SYMBOL__BCL2"
      minerva_ref_type__resource2 "REFSEQ__NM_000657"
      minerva_ref_type__resource3 "ENTREZ__596"
      minerva_ref_type__resource4 "UNIPROT__P10415"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000171791"
      minerva_ref_type__resource6 "REFSEQ__NM_000633"
      minerva_ref_type__resource7 "HGNC__990"
      minerva_type "State transition"
      minerva_x 335.125
      minerva_y 531.375
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re7"
      reaction_meta_id "re7"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 115
    zlevel -1

    graphics [
      x 1200.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re10"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BCL2"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000657"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/596"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P10415"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000171791"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000633"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/990"
      minerva_ref_type__resource1 "HGNC_SYMBOL__BCL2"
      minerva_ref_type__resource2 "REFSEQ__NM_000657"
      minerva_ref_type__resource3 "ENTREZ__596"
      minerva_ref_type__resource4 "UNIPROT__P10415"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000171791"
      minerva_ref_type__resource6 "REFSEQ__NM_000633"
      minerva_ref_type__resource7 "HGNC__990"
      minerva_type "Transport"
      minerva_x 1010.0
      minerva_y 840.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re10"
      reaction_meta_id "re10"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 120
    zlevel -1

    graphics [
      x 200.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re15"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BCL2"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000657"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/596"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P10415"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000171791"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000633"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/990"
      minerva_ref_type__resource1 "HGNC_SYMBOL__BCL2"
      minerva_ref_type__resource2 "REFSEQ__NM_000657"
      minerva_ref_type__resource3 "ENTREZ__596"
      minerva_ref_type__resource4 "UNIPROT__P10415"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000171791"
      minerva_ref_type__resource6 "REFSEQ__NM_000633"
      minerva_ref_type__resource7 "HGNC__990"
      minerva_type "State transition"
      minerva_x 712.5
      minerva_y 615.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re15"
      reaction_meta_id "re15"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 124
    zlevel -1

    graphics [
      x 300.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re33"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BCL2"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000657"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/596"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P10415"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000171791"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000633"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/990"
      minerva_ref_type__resource1 "HGNC_SYMBOL__BCL2"
      minerva_ref_type__resource2 "REFSEQ__NM_000657"
      minerva_ref_type__resource3 "ENTREZ__596"
      minerva_ref_type__resource4 "UNIPROT__P10415"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000171791"
      minerva_ref_type__resource6 "REFSEQ__NM_000633"
      minerva_ref_type__resource7 "HGNC__990"
      minerva_type "Physical stimulation"
      minerva_x 385.0
      minerva_y 952.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re33"
      reaction_meta_id "re33"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 127
    zlevel -1

    graphics [
      x 400.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re31"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BCL2"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000657"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/596"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P10415"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000171791"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000633"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/990"
      minerva_ref_type__resource1 "HGNC_SYMBOL__BCL2"
      minerva_ref_type__resource2 "REFSEQ__NM_000657"
      minerva_ref_type__resource3 "ENTREZ__596"
      minerva_ref_type__resource4 "UNIPROT__P10415"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000171791"
      minerva_ref_type__resource6 "REFSEQ__NM_000633"
      minerva_ref_type__resource7 "HGNC__990"
      minerva_type "Physical stimulation"
      minerva_x 385.0
      minerva_y 930.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re31"
      reaction_meta_id "re31"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 130
    zlevel -1

    graphics [
      x 500.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re9"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BCL2"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000657"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/596"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P10415"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000171791"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000633"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/990"
      minerva_ref_type__resource1 "HGNC_SYMBOL__BCL2"
      minerva_ref_type__resource2 "REFSEQ__NM_000657"
      minerva_ref_type__resource3 "ENTREZ__596"
      minerva_ref_type__resource4 "UNIPROT__P10415"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000171791"
      minerva_ref_type__resource6 "REFSEQ__NM_000633"
      minerva_ref_type__resource7 "HGNC__990"
      minerva_type "State transition"
      minerva_x 1070.0
      minerva_y 1050.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re9"
      reaction_meta_id "re9"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 136
    zlevel -1

    graphics [
      x 600.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re32"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BCL2"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000657"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/596"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P10415"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000171791"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000633"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/990"
      minerva_ref_type__resource1 "HGNC_SYMBOL__BCL2"
      minerva_ref_type__resource2 "REFSEQ__NM_000657"
      minerva_ref_type__resource3 "ENTREZ__596"
      minerva_ref_type__resource4 "UNIPROT__P10415"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000171791"
      minerva_ref_type__resource6 "REFSEQ__NM_000633"
      minerva_ref_type__resource7 "HGNC__990"
      minerva_type "Physical stimulation"
      minerva_x 385.0
      minerva_y 1042.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re32"
      reaction_meta_id "re32"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 139
    zlevel -1

    graphics [
      x 700.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BCL2"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000657"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/596"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P10415"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000171791"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000633"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/990"
      minerva_ref_type__resource1 "HGNC_SYMBOL__BCL2"
      minerva_ref_type__resource2 "REFSEQ__NM_000657"
      minerva_ref_type__resource3 "ENTREZ__596"
      minerva_ref_type__resource4 "UNIPROT__P10415"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000171791"
      minerva_ref_type__resource6 "REFSEQ__NM_000633"
      minerva_ref_type__resource7 "HGNC__990"
      minerva_type "Heterodimer association"
      minerva_x 306.5
      minerva_y 140.9375
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1"
      reaction_meta_id "re1"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 143
    zlevel -1

    graphics [
      x 800.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re12"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BCL2"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000657"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/596"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P10415"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000171791"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000633"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/990"
      minerva_ref_type__resource1 "HGNC_SYMBOL__BCL2"
      minerva_ref_type__resource2 "REFSEQ__NM_000657"
      minerva_ref_type__resource3 "ENTREZ__596"
      minerva_ref_type__resource4 "UNIPROT__P10415"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000171791"
      minerva_ref_type__resource6 "REFSEQ__NM_000633"
      minerva_ref_type__resource7 "HGNC__990"
      minerva_type "State transition"
      minerva_x 284.125
      minerva_y 771.375
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re12"
      reaction_meta_id "re12"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 148
    zlevel -1

    graphics [
      x 900.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re28"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BCL2"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000657"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/596"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P10415"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000171791"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000633"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/990"
      minerva_ref_type__resource1 "HGNC_SYMBOL__BCL2"
      minerva_ref_type__resource2 "REFSEQ__NM_000657"
      minerva_ref_type__resource3 "ENTREZ__596"
      minerva_ref_type__resource4 "UNIPROT__P10415"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000171791"
      minerva_ref_type__resource6 "REFSEQ__NM_000633"
      minerva_ref_type__resource7 "HGNC__990"
      minerva_type "Physical stimulation"
      minerva_x 385.0
      minerva_y 997.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re28"
      reaction_meta_id "re28"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 151
    zlevel -1

    graphics [
      x 1000.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re19"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BCL2"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000657"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/596"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P10415"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000171791"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000633"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/990"
      minerva_ref_type__resource1 "HGNC_SYMBOL__BCL2"
      minerva_ref_type__resource2 "REFSEQ__NM_000657"
      minerva_ref_type__resource3 "ENTREZ__596"
      minerva_ref_type__resource4 "UNIPROT__P10415"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000171791"
      minerva_ref_type__resource6 "REFSEQ__NM_000633"
      minerva_ref_type__resource7 "HGNC__990"
      minerva_type "State transition"
      minerva_x 786.0
      minerva_y 795.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re19"
      reaction_meta_id "re19"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 155
    zlevel -1

    graphics [
      x 1100.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re22"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BCL2"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000657"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/596"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P10415"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000171791"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000633"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/990"
      minerva_ref_type__resource1 "HGNC_SYMBOL__BCL2"
      minerva_ref_type__resource2 "REFSEQ__NM_000657"
      minerva_ref_type__resource3 "ENTREZ__596"
      minerva_ref_type__resource4 "UNIPROT__P10415"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000171791"
      minerva_ref_type__resource6 "REFSEQ__NM_000633"
      minerva_ref_type__resource7 "HGNC__990"
      minerva_type "State transition"
      minerva_x 1219.0
      minerva_y 547.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re22"
      reaction_meta_id "re22"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  edge [
    id 52
    source 7
    target 51
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa78"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 53
    source 51
    target 48
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa79"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 54
    source 25
    target 51
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR0"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa29"
    ]
  ]
  edge [
    id 56
    source 21
    target 55
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa8"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 57
    source 33
    target 55
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa3"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 58
    source 55
    target 44
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa1"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 60
    source 19
    target 59
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa25"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 61
    source 36
    target 59
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa27"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 62
    source 1
    target 59
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa47"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 63
    source 59
    target 39
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa5"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 64
    source 25
    target 59
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR1"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa29"
    ]
  ]
  edge [
    id 66
    source 6
    target 65
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa22"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 67
    source 65
    target 16
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa23"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 68
    source 50
    target 65
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR2"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa14"
    ]
  ]
  edge [
    id 70
    source 32
    target 69
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa40"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 71
    source 69
    target 14
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa41"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 73
    source 23
    target 72
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa17"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 74
    source 72
    target 14
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa41"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 76
    source 16
    target 75
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa23"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 77
    source 75
    target 34
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa89"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 79
    source 49
    target 78
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa74"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 80
    source 78
    target 14
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa41"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 82
    source 17
    target 81
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa10"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 83
    source 31
    target 81
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa44"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 84
    source 81
    target 15
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa6"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 85
    source 44
    target 81
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR3"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa1"
    ]
  ]
  edge [
    id 87
    source 10
    target 86
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa15"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 88
    source 86
    target 23
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa17"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 89
    source 50
    target 86
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR4"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa14"
    ]
  ]
  edge [
    id 90
    source 37
    target 86
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR5"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa19"
    ]
  ]
  edge [
    id 92
    source 13
    target 91
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa72"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 93
    source 91
    target 14
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa41"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 95
    source 47
    target 94
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa10"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 96
    source 94
    target 11
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa11"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 97
    source 38
    target 94
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR6"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa43"
    ]
  ]
  edge [
    id 98
    source 41
    target 94
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR7"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa69"
    ]
  ]
  edge [
    id 99
    source 48
    target 94
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR8"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa79"
    ]
  ]
  edge [
    id 101
    source 8
    target 100
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa13"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 102
    source 100
    target 50
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa14"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 103
    source 17
    target 100
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR9"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa10"
    ]
  ]
  edge [
    id 104
    source 15
    target 100
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR10"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa6"
    ]
  ]
  edge [
    id 105
    source 30
    target 100
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR11"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa92"
    ]
  ]
  edge [
    id 107
    source 40
    target 106
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa9"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 108
    source 106
    target 17
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa10"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 109
    source 9
    target 106
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR12"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa2"
    ]
  ]
  edge [
    id 111
    source 5
    target 110
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa18"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 112
    source 110
    target 37
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa19"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 113
    source 39
    target 110
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR13"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa5"
    ]
  ]
  edge [
    id 114
    source 25
    target 110
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR14"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa29"
    ]
  ]
  edge [
    id 116
    source 20
    target 115
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa24"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 117
    source 115
    target 19
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa25"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 118
    source 42
    target 115
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR15"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa50"
    ]
  ]
  edge [
    id 119
    source 28
    target 115
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR16"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa31"
    ]
  ]
  edge [
    id 121
    source 26
    target 120
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa28"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 122
    source 120
    target 25
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa29"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 123
    source 4
    target 120
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR17"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa42"
    ]
  ]
  edge [
    id 125
    source 45
    target 124
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa75"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 126
    source 124
    target 14
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa41"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 128
    source 27
    target 127
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa76"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 129
    source 127
    target 14
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa41"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 131
    source 35
    target 130
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa30"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 132
    source 130
    target 28
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa31"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 133
    source 2
    target 130
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR18"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa3"
    ]
  ]
  edge [
    id 134
    source 11
    target 130
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR19"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa11"
    ]
  ]
  edge [
    id 135
    source 34
    target 130
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR20"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa89"
    ]
  ]
  edge [
    id 137
    source 12
    target 136
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa77"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 138
    source 136
    target 14
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa41"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 140
    source 22
    target 139
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa2"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 141
    source 18
    target 139
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa5"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 142
    source 139
    target 9
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa2"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 144
    source 24
    target 143
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa39"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 145
    source 143
    target 32
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa40"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 146
    source 37
    target 143
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR21"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa19"
    ]
  ]
  edge [
    id 147
    source 50
    target 143
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR22"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa14"
    ]
  ]
  edge [
    id 149
    source 43
    target 148
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa73"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 150
    source 148
    target 14
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa41"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 152
    source 46
    target 151
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa49"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 153
    source 151
    target 42
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa50"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 154
    source 29
    target 151
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR23"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa48"
    ]
  ]
  edge [
    id 156
    source 3
    target 155
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa9"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 157
    source 155
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa3"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 158
    source 11
    target 155
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR24"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa11"
    ]
  ]
]
