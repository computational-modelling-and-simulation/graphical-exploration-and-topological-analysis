# generated with VANTED V2.8.0 at Tue Apr 27 20:00:52 CEST 2021
graph [
  background_coloring "true"
  cluster_colors "255,128,128,255:0,0,0,255;128,255,128,255:0,0,0,255;128,128,255,255:0,0,0,255"
  clusterbackground_fill_outer_region "false"
  clusterbackground_grid 50.0
  clusterbackground_low_alpha 0.2
  clusterbackground_radius 200.0
  clusterbackground_space_fill "true"
  graphbackgroundcolor "#ffffff"
  sbml [
    model_annotation ""
    model_meta_id "untitled"
    model_non_rdf_annotation ""
    model_notes ""
    model_sbml_id "untitled"
    namespace "xmlns:celldesigner=\"http://www.sbml.org/2001/ns/celldesigner\" "
    sbml_level "2"
    sbml_version "4"
  ]
  sbml_compartment_default [
    sbml_compartment_default_id "default"
    sbml_compartment_default_meta_id "default"
    sbml_compartment_default_non_rdf_annotation ""
    sbml_compartment_default_notes ""
    sbml_compartment_default_size "1.0"
    sbml_compartment_default_units "volume"
  ]
  sbml_compartment_s_id_ca1 [
    sbml_compartment_s_id_ca1_annotation ""
    sbml_compartment_s_id_ca1_id "s_id_ca1"
    sbml_compartment_s_id_ca1_meta_id "s_id_ca1"
    sbml_compartment_s_id_ca1_name "cell"
    sbml_compartment_s_id_ca1_non_rdf_annotation ""
    sbml_compartment_s_id_ca1_notes ""
    sbml_compartment_s_id_ca1_outside "default"
    sbml_compartment_s_id_ca1_size "1.0"
    sbml_compartment_s_id_ca1_units "volume"
  ]
  sbml_compartment_s_id_ca2 [
    sbml_compartment_s_id_ca2_annotation ""
    sbml_compartment_s_id_ca2_id "s_id_ca2"
    sbml_compartment_s_id_ca2_meta_id "s_id_ca2"
    sbml_compartment_s_id_ca2_name "mitochondrion"
    sbml_compartment_s_id_ca2_non_rdf_annotation ""
    sbml_compartment_s_id_ca2_notes ""
    sbml_compartment_s_id_ca2_outside "s_id_ca1"
    sbml_compartment_s_id_ca2_size "1.0"
    sbml_compartment_s_id_ca2_units "volume"
  ]
  sbml_unit_definition_1 [
    sbml_unit_definition_1_id "volume"
    sbml_unit_definition_1_name "volume"
    sbml_unit_definition_1_sub_unit_1_ "(1.0 * 10^0 * litre)^1.0"
    sbml_unit_definition_1unit "(1.0 * 10^0 * litre)^1.0"
  ]
  sbml_unit_definition_2 [
    sbml_unit_definition_2_id "time"
    sbml_unit_definition_2_name "time"
    sbml_unit_definition_2_sub_unit_1_ "(1.0 * 10^0 * second)^1.0"
    sbml_unit_definition_2unit "(1.0 * 10^0 * second)^1.0"
  ]
  sbml_unit_definition_3 [
    sbml_unit_definition_3_id "area"
    sbml_unit_definition_3_name "area"
    sbml_unit_definition_3_sub_unit_1_ "(1.0 * 10^0 * metre)^2.0"
    sbml_unit_definition_3unit "(1.0 * 10^0 * metre)^2.0"
  ]
  sbml_unit_definition_4 [
    sbml_unit_definition_4_id "substance"
    sbml_unit_definition_4_name "substance"
    sbml_unit_definition_4_sub_unit_1_ "(1.0 * 10^0 * mole)^1.0"
    sbml_unit_definition_4unit "(1.0 * 10^0 * mole)^1.0"
  ]
  sbml_unit_definition_5 [
    sbml_unit_definition_5_id "length"
    sbml_unit_definition_5_name "length"
    sbml_unit_definition_5_sub_unit_1_ "(1.0 * 10^0 * metre)^1.0"
    sbml_unit_definition_5unit "(1.0 * 10^0 * metre)^1.0"
  ]
  directed 1
  node [
    id 1
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "h__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa103"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa325"
      minerva_elementId10 "sa288"
      minerva_elementId11 "sa129"
      minerva_elementId12 "sa178"
      minerva_elementId13 "sa327"
      minerva_elementId14 "sa169"
      minerva_elementId15 "sa133"
      minerva_elementId16 "sa31"
      minerva_elementId17 "sa78"
      minerva_elementId18 "sa266"
      minerva_elementId19 "sa190"
      minerva_elementId2 "sa83"
      minerva_elementId20 "sa123"
      minerva_elementId21 "sa164"
      minerva_elementId22 "sa89"
      minerva_elementId23 "sa281"
      minerva_elementId24 "sa188"
      minerva_elementId25 "sa341"
      minerva_elementId26 "sa103"
      minerva_elementId27 "sa336"
      minerva_elementId28 "sa221"
      minerva_elementId29 "sa367"
      minerva_elementId3 "sa254"
      minerva_elementId30 "sa183"
      minerva_elementId31 "sa134"
      minerva_elementId32 "sa282"
      minerva_elementId33 "sa222"
      minerva_elementId34 "sa298"
      minerva_elementId4 "sa302"
      minerva_elementId5 "sa286"
      minerva_elementId6 "sa297"
      minerva_elementId7 "sa187"
      minerva_elementId8 "sa206"
      minerva_elementId9 "sa360"
      minerva_fullName "proton"
      minerva_name "H"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:24636"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/compound/1038"
      minerva_ref_type__resource1 "CHEBI__CHEBI:24636"
      minerva_ref_type__resource2 "PUBCHEM__1038"
      minerva_synonyms "(1)1H(+); (1)H(+); p; p(+); proton"
      minerva_type "Simple molecule"
      minerva_x 4378.0
      minerva_x10 5340.875
      minerva_x11 507.7692307692307
      minerva_x12 2496.6985294117644
      minerva_x13 4534.25
      minerva_x14 2032.0
      minerva_x15 781.794117647059
      minerva_x16 2116.5
      minerva_x17 1580.0
      minerva_x18 4449.0
      minerva_x19 4078.4485294117644
      minerva_x2 1089.625
      minerva_x20 1452.0
      minerva_x21 1422.25
      minerva_x22 1178.0
      minerva_x23 5756.0
      minerva_x24 3529.0
      minerva_x25 4187.808823529412
      minerva_x26 1148.0
      minerva_x27 4438.0
      minerva_x28 5371.166666666666
      minerva_x29 3456.904411764706
      minerva_x3 4574.0
      minerva_x30 3091.9485294117644
      minerva_x31 761.794117647059
      minerva_x32 5766.0
      minerva_x33 5405.166666666666
      minerva_x34 5719.5
      minerva_x4 4481.25
      minerva_x5 5373.75
      minerva_x6 5700.5
      minerva_x7 3505.0
      minerva_x8 5401.416666666666
      minerva_x9 3791.0
      minerva_y 1368.0
      minerva_y10 1868.3676470588236
      minerva_y11 1628.8846153846152
      minerva_y12 800.4558823529411
      minerva_y13 1581.0
      minerva_y14 709.0
      minerva_y15 1417.735294117647
      minerva_y16 2118.0
      minerva_y17 1930.0
      minerva_y18 1857.0
      minerva_y19 818.1470588235294
      minerva_y2 1933.0
      minerva_y20 1340.0
      minerva_y21 822.5
      minerva_y22 1634.0
      minerva_y23 1911.8235294117649
      minerva_y24 793.0
      minerva_y25 961.7303921568628
      minerva_y26 1559.0
      minerva_y27 1042.0
      minerva_y28 1755.1470588235295
      minerva_y29 1373.3553921568628
      minerva_y3 1754.0
      minerva_y30 809.6470588235294
      minerva_y31 1383.735294117647
      minerva_y32 1929.1764705882354
      minerva_y33 1757.5
      minerva_y34 1673.8846153846152
      minerva_y4 883.0
      minerva_y5 2076.9117647058824
      minerva_y6 1689.8846153846152
      minerva_y7 808.0
      minerva_y8 1414.6525082851072
      minerva_y9 1458.625
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa103"
      species_meta_id "s_id_sa103"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 2
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "pnp__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa358"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa113"
      minerva_elementId2 "sa258"
      minerva_elementId3 "sa358"
      minerva_elementId4 "sa348"
      minerva_elementId5 "sa320"
      minerva_former_symbols "NP"
      minerva_name "PNP"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000198805"
      minerva_ref_link10 "https://purl.uniprot.org/uniprot/P00491"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PNP"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PNP"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4860"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/4860"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.4.2.1"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_000270.2"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7892"
      minerva_ref_link9 "https://purl.uniprot.org/uniprot/P00491"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000198805"
      minerva_ref_type__resource10 "UNIPROT__P00491"
      minerva_ref_type__resource2 "HGNC_SYMBOL__PNP"
      minerva_ref_type__resource3 "HGNC_SYMBOL__PNP"
      minerva_ref_type__resource4 "ENTREZ__4860"
      minerva_ref_type__resource5 "ENTREZ__4860"
      minerva_ref_type__resource6 "EC__2.4.2.1"
      minerva_ref_type__resource7 "REFSEQ__NM_000270.2"
      minerva_ref_type__resource8 "HGNC__7892"
      minerva_ref_type__resource9 "UNIPROT__P00491"
      minerva_type "Protein"
      minerva_x 1338.0
      minerva_x2 4267.5
      minerva_x3 4180.125
      minerva_x4 4157.5
      minerva_x5 4681.5
      minerva_y 1259.0
      minerva_y2 1568.5
      minerva_y3 1378.625
      minerva_y4 1149.5
      minerva_y5 1313.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa358"
      species_meta_id "s_id_sa358"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 3
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nicotinamide__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa66"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa66"
      minerva_fullName "nicotinamide"
      minerva_name "Nicotinamide"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17154"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/compound/936"
      minerva_ref_type__resource1 "CHEBI__CHEBI:17154"
      minerva_ref_type__resource2 "PUBCHEM__936"
      minerva_synonyms "3-pyridinecarboxamide; NICOTINAMIDE; Niacinamide; Nicotinamid; Nicotinamide; Nicotinic acid amide; Nicotinsaeureamid; Nikotinamid; Nikotinsaeureamid; Vitamin PP; beta-pyridinecarboxamide; niacin; nicotinamide; vitamin B3"
      minerva_type "Simple molecule"
      minerva_x 1248.0
      minerva_y 1137.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa66"
      species_meta_id "s_id_sa66"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 4
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "h2o__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa315"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa375"
      minerva_elementId10 "sa315"
      minerva_elementId11 "sa219"
      minerva_elementId12 "sa156"
      minerva_elementId13 "sa98"
      minerva_elementId14 "sa278"
      minerva_elementId15 "sa263"
      minerva_elementId16 "sa272"
      minerva_elementId17 "sa344"
      minerva_elementId18 "sa284"
      minerva_elementId19 "sa361"
      minerva_elementId2 "sa324"
      minerva_elementId20 "sa50"
      minerva_elementId21 "sa328"
      minerva_elementId22 "sa140"
      minerva_elementId23 "sa303"
      minerva_elementId24 "sa335"
      minerva_elementId25 "sa87"
      minerva_elementId26 "sa172"
      minerva_elementId27 "sa54"
      minerva_elementId28 "sa25"
      minerva_elementId3 "sa243"
      minerva_elementId4 "sa208"
      minerva_elementId5 "sa203"
      minerva_elementId6 "sa34"
      minerva_elementId7 "sa122"
      minerva_elementId8 "sa287"
      minerva_elementId9 "sa96"
      minerva_fullName "water"
      minerva_name "H2O"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15377"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/compound/962"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15377"
      minerva_ref_type__resource2 "PUBCHEM__962"
      minerva_synonyms "BOUND WATER; H2O; H2O; HOH; WATER; Wasser; Water; [OH2]; acqua; agua; aqua; dihydridooxygen; dihydrogen oxide; eau; hydrogen hydroxide"
      minerva_type "Simple molecule"
      minerva_x 3458.625
      minerva_x10 5177.958333333333
      minerva_x11 5220.916666666666
      minerva_x12 1170.0
      minerva_x13 1345.5
      minerva_x14 5748.5
      minerva_x15 4448.5
      minerva_x16 5072.5
      minerva_x17 4016.25
      minerva_x18 5482.5
      minerva_x19 3724.25
      minerva_x2 4390.5
      minerva_x20 1865.9230769230771
      minerva_x21 4543.75
      minerva_x22 495.47058823529414
      minerva_x23 4411.75
      minerva_x24 4500.875
      minerva_x25 692.5
      minerva_x26 2211.4485294117644
      minerva_x27 1469.6666666666667
      minerva_x28 2488.0
      minerva_x3 4769.5
      minerva_x4 5462.916666666666
      minerva_x5 5382.529411764706
      minerva_x6 1849.5
      minerva_x7 1470.5
      minerva_x8 5370.625
      minerva_x9 1252.5
      minerva_y 1148.375
      minerva_y10 1214.6911764705883
      minerva_y11 1488.3823529411766
      minerva_y12 1030.735294117647
      minerva_y13 1546.0
      minerva_y14 2100.0
      minerva_y15 2113.0
      minerva_y16 1637.0
      minerva_y17 1094.0
      minerva_y18 2181.0
      minerva_y19 1454.625
      minerva_y2 1282.0
      minerva_y20 2317.9615384615386
      minerva_y21 1552.0
      minerva_y22 1227.6764705882354
      minerva_y23 884.0
      minerva_y24 1023.0
      minerva_y25 1916.0
      minerva_y26 784.6911764705882
      minerva_y27 2296.8333333333335
      minerva_y28 2460.0
      minerva_y3 1982.0
      minerva_y4 1245.6525082851072
      minerva_y5 1080.2058823529412
      minerva_y6 2566.0
      minerva_y7 1591.0
      minerva_y8 1948.4558823529412
      minerva_y9 1841.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa315"
      species_meta_id "s_id_sa315"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 5
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "atp__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa150"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa161"
      minerva_elementId10 "sa76"
      minerva_elementId11 "sa249"
      minerva_elementId12 "sa81"
      minerva_elementId13 "sa217"
      minerva_elementId14 "sa180"
      minerva_elementId15 "sa139"
      minerva_elementId16 "sa88"
      minerva_elementId17 "sa6"
      minerva_elementId18 "sa227"
      minerva_elementId19 "sa128"
      minerva_elementId2 "sa354"
      minerva_elementId20 "sa174"
      minerva_elementId21 "sa371"
      minerva_elementId22 "sa338"
      minerva_elementId23 "sa365"
      minerva_elementId3 "sa252"
      minerva_elementId4 "sa387"
      minerva_elementId5 "sa246"
      minerva_elementId6 "sa150"
      minerva_elementId7 "sa191"
      minerva_elementId8 "sa230"
      minerva_elementId9 "sa101"
      minerva_fullName "ATP"
      minerva_name "ATP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15422"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/compound/5957"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15422"
      minerva_ref_type__resource2 "PUBCHEM__5957"
      minerva_synonyms "ADENOSINE-5'-TRIPHOSPHATE; ATP; Adenosine 5'-triphosphate; Adenosine triphosphate; Adephos; Adetol; Adynol; Atipi; Atriphos; Cardenosine; Fosfobion; Glucobasin; H4atp; Myotriphos; Triadenyl; Triphosphaden"
      minerva_type "Simple molecule"
      minerva_x 1176.6666666666665
      minerva_x10 1438.25
      minerva_x11 4687.75
      minerva_x12 711.875
      minerva_x13 5144.269607843136
      minerva_x14 2824.051470588235
      minerva_x15 477.0
      minerva_x16 1120.5
      minerva_x17 2447.6875
      minerva_x18 5203.5
      minerva_x19 450.2692307692307
      minerva_x2 3993.125
      minerva_x20 2160.801470588235
      minerva_x21 3324.125
      minerva_x22 4305.25
      minerva_x23 3574.345588235294
      minerva_x3 4288.75
      minerva_x4 3198.125
      minerva_x5 4583.250000000001
      minerva_x6 897.3529411764707
      minerva_x7 3855.551470588235
      minerva_x8 5241.75
      minerva_x9 1148.5
      minerva_y 795.7222222222222
      minerva_y10 1868.0
      minerva_y11 1817.5
      minerva_y12 1857.0
      minerva_y13 1497.8529411764707
      minerva_y14 692.3529411764706
      minerva_y15 1263.5
      minerva_y16 1650.0
      minerva_y17 2384.75
      minerva_y18 1839.0
      minerva_y19 1642.8846153846152
      minerva_y2 886.625
      minerva_y20 659.1617647058823
      minerva_y21 1356.125
      minerva_y22 1024.0
      minerva_y23 1443.625
      minerva_y3 1754.5
      minerva_y4 1136.375
      minerva_y5 2040.5
      minerva_y6 827.2058823529412
      minerva_y7 696.8529411764706
      minerva_y8 2048.5
      minerva_y9 1434.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa150"
      species_meta_id "s_id_sa150"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 6
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ammonium__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa307"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa307"
      minerva_elementId2 "sa334"
      minerva_elementId3 "sa362"
      minerva_elementId4 "sa329"
      minerva_elementId5 "sa294"
      minerva_fullName "ammonium"
      minerva_name "Ammonium"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/223"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:28938"
      minerva_ref_type__resource1 "PUBCHEM__223"
      minerva_ref_type__resource2 "CHEBI__CHEBI:28938"
      minerva_synonyms "Ammonium(1+); NH4(+); NH4(+); NH4+; [NH4](+); ammonium; ammonium cation; ammonium ion"
      minerva_type "Simple molecule"
      minerva_x 4875.0
      minerva_x2 4748.125
      minerva_x3 3957.125
      minerva_x4 4387.0
      minerva_x5 5710.5
      minerva_y 892.5
      minerva_y2 1031.5
      minerva_y3 1461.625
      minerva_y4 1479.5
      minerva_y5 1318.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa307"
      species_meta_id "s_id_sa307"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 7
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 800.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "entpd2__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa277"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa277"
      minerva_former_symbols "CD39L1"
      minerva_name "ENTPD2"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/954"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000054179"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/954"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/3364"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.6.1.-"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/Q9Y5L3"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/Q9Y5L3"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ENTPD2"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ENTPD2"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_203468"
      minerva_ref_type__resource1 "ENTREZ__954"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000054179"
      minerva_ref_type__resource2 "ENTREZ__954"
      minerva_ref_type__resource3 "HGNC__3364"
      minerva_ref_type__resource4 "EC__3.6.1.-"
      minerva_ref_type__resource5 "UNIPROT__Q9Y5L3"
      minerva_ref_type__resource6 "UNIPROT__Q9Y5L3"
      minerva_ref_type__resource7 "HGNC_SYMBOL__ENTPD2"
      minerva_ref_type__resource8 "HGNC_SYMBOL__ENTPD2"
      minerva_ref_type__resource9 "REFSEQ__NM_203468"
      minerva_type "Protein"
      minerva_x 5570.0
      minerva_y 2014.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa277"
      species_meta_id "s_id_sa277"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 8
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "l_minus_glutamate__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa154"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa220"
      minerva_elementId2 "sa154"
      minerva_elementId3 "sa177"
      minerva_elementId4 "sa86"
      minerva_fullName "L-glutamic acid"
      minerva_name "L-Glutamate"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16015"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/compound/33032"
      minerva_ref_type__resource1 "CHEBI__CHEBI:16015"
      minerva_ref_type__resource2 "PUBCHEM__33032"
      minerva_synonyms "(S)-2-aminopentanedioic acid; (S)-glutamic acid; E; GLUTAMIC ACID; Glu; Glutamate; L-Glu; L-Glutamic acid; L-Glutaminic acid; L-Glutaminsaeure; acide glutamique; acido glutamico; acidum glutamicum; glutamic acid"
      minerva_type "Simple molecule"
      minerva_x 5460.166666666666
      minerva_x2 1102.25
      minerva_x3 2489.6985294117644
      minerva_x4 1076.5
      minerva_y 1731.2352941176473
      minerva_y2 888.5882352941178
      minerva_y3 681.5441176470589
      minerva_y4 1971.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa154"
      species_meta_id "s_id_sa154"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 9
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1000.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "adp__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa228"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa192"
      minerva_elementId10 "sa141"
      minerva_elementId11 "sa339"
      minerva_elementId12 "sa182"
      minerva_elementId13 "sa228"
      minerva_elementId14 "sa386"
      minerva_elementId15 "sa102"
      minerva_elementId16 "sa247"
      minerva_elementId17 "sa352"
      minerva_elementId18 "sa250"
      minerva_elementId19 "sa77"
      minerva_elementId2 "sa253"
      minerva_elementId20 "sa231"
      minerva_elementId3 "sa163"
      minerva_elementId4 "sa388"
      minerva_elementId5 "sa7"
      minerva_elementId6 "sa372"
      minerva_elementId7 "sa176"
      minerva_elementId8 "sa82"
      minerva_elementId9 "sa366"
      minerva_fullName "ADP"
      minerva_name "ADP"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_synonyms "5'-adenylphosphoric acid; ADENOSINE-5'-DIPHOSPHATE; ADP; Adenosine 5'-diphosphate; H3adp"
      minerva_type "Simple molecule"
      minerva_x 4116.948529411764
      minerva_x10 466.2352941176471
      minerva_x11 4309.25
      minerva_x12 3146.4485294117644
      minerva_x13 5201.5
      minerva_x14 2862.125
      minerva_x15 1111.5
      minerva_x16 4577.250000000001
      minerva_x17 3379.625
      minerva_x18 4571.75
      minerva_x19 1557.25
      minerva_x2 4510.75
      minerva_x20 5224.75
      minerva_x3 1488.75
      minerva_x4 3595.0
      minerva_x5 2557.6875
      minerva_x6 3322.125
      minerva_x7 2559.1985294117644
      minerva_x8 1057.875
      minerva_x9 3449.345588235294
      minerva_y 792.1470588235294
      minerva_y10 1424.6176470588236
      minerva_y11 955.0
      minerva_y12 791.6470588235294
      minerva_y13 1967.0
      minerva_y14 1131.375
      minerva_y15 1533.0
      minerva_y16 2187.5
      minerva_y17 914.625
      minerva_y18 1966.5
      minerva_y19 1870.0
      minerva_y2 1744.5
      minerva_y20 2176.5
      minerva_y3 822.5
      minerva_y4 881.0
      minerva_y5 2384.75
      minerva_y6 1205.125
      minerva_y7 791.4558823529411
      minerva_y8 1857.0
      minerva_y9 1446.625
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa228"
      species_meta_id "s_id_sa228"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 10
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "gmpr2__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa300"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa300"
      minerva_name "GMPR2"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/4377"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/51292"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=GMPR2"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=GMPR2"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=1.7.1.7"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_016576"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000100938"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/51292"
      minerva_ref_link8 "https://purl.uniprot.org/uniprot/Q9P2T1"
      minerva_ref_link9 "https://purl.uniprot.org/uniprot/Q9P2T1"
      minerva_ref_type__resource1 "HGNC__4377"
      minerva_ref_type__resource10 "ENTREZ__51292"
      minerva_ref_type__resource2 "HGNC_SYMBOL__GMPR2"
      minerva_ref_type__resource3 "HGNC_SYMBOL__GMPR2"
      minerva_ref_type__resource4 "EC__1.7.1.7"
      minerva_ref_type__resource5 "REFSEQ__NM_016576"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000100938"
      minerva_ref_type__resource7 "ENTREZ__51292"
      minerva_ref_type__resource8 "UNIPROT__Q9P2T1"
      minerva_ref_type__resource9 "UNIPROT__Q9P2T1"
      minerva_type "Protein"
      minerva_x 5544.0
      minerva_y 1514.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa300"
      species_meta_id "s_id_sa300"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 11
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1200.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "gtp__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa229"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa229"
      minerva_fullName "GTP"
      minerva_name "GTP"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/35398633"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15996"
      minerva_ref_type__resource1 "PUBCHEM__35398633"
      minerva_ref_type__resource2 "CHEBI__CHEBI:15996"
      minerva_synonyms "5'-GTP; GTP; GUANOSINE-5'-TRIPHOSPHATE; Guanosine 5'-triphosphate; H4gtp; guanosine 5'-triphosphoric acid; guanosine triphosphate"
      minerva_type "Simple molecule"
      minerva_x 5292.5
      minerva_y 2220.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa229"
      species_meta_id "s_id_sa229"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 12
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 200.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "urea_space_cycle__mitochondrion__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa412"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa412"
      minerva_name "Urea cycle"
      minerva_type "Phenotype"
      minerva_x 1858.0
      minerva_y 1088.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrion"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa412"
      species_meta_id "s_id_sa412"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 13
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp14__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa35"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa35"
      minerva_elementId2 "sa410"
      minerva_name "Nsp14"
      minerva_ref_link1 "https://doi.org/10.1101/2020.03.22.002386"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/YP_009725309"
      minerva_ref_type__resource1 "DOI__10.1101/2020.03.22.002386"
      minerva_ref_type__resource2 "NCBI_PROTEIN__YP_009725309"
      minerva_type "Protein"
      minerva_x 1369.0
      minerva_x2 5045.5
      minerva_y 2604.0
      minerva_y2 1417.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa35"
      species_meta_id "s_id_sa35"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 14
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nt5e__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa275"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa275"
      minerva_elementId2 "sa316"
      minerva_elementId3 "sa343"
      minerva_elementId4 "sa100"
      minerva_former_symbols "NT5"
      minerva_name "NT5E"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.1.3.5"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000135318"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/protein/NM_001204813"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P21589"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P21589"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/pubmed/2848759"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/8021"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NT5E"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4907"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NT5E"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4907"
      minerva_ref_type__resource1 "EC__3.1.3.5"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000135318"
      minerva_ref_type__resource11 "REFSEQ__NM_001204813"
      minerva_ref_type__resource2 "UNIPROT__P21589"
      minerva_ref_type__resource3 "UNIPROT__P21589"
      minerva_ref_type__resource4 "PUBMED__2848759"
      minerva_ref_type__resource5 "HGNC__8021"
      minerva_ref_type__resource6 "HGNC_SYMBOL__NT5E"
      minerva_ref_type__resource7 "ENTREZ__4907"
      minerva_ref_type__resource8 "HGNC_SYMBOL__NT5E"
      minerva_ref_type__resource9 "ENTREZ__4907"
      minerva_type "Protein"
      minerva_x 5031.5
      minerva_x2 5090.5
      minerva_x3 4096.5
      minerva_x4 1382.0
      minerva_y 1559.5
      minerva_y2 1231.5
      minerva_y3 1106.5
      minerva_y4 1493.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa275"
      species_meta_id "s_id_sa275"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 15
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nadk__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa75"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa75"
      minerva_name "NADK"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/29831"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000008130"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/65220"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_023018"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/65220"
      minerva_ref_link5 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.1.23"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/O95544"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/O95544"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NADK"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NADK"
      minerva_ref_type__resource1 "HGNC__29831"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000008130"
      minerva_ref_type__resource2 "ENTREZ__65220"
      minerva_ref_type__resource3 "REFSEQ__NM_023018"
      minerva_ref_type__resource4 "ENTREZ__65220"
      minerva_ref_type__resource5 "EC__2.7.1.23"
      minerva_ref_type__resource6 "UNIPROT__O95544"
      minerva_ref_type__resource7 "UNIPROT__O95544"
      minerva_ref_type__resource8 "HGNC_SYMBOL__NADK"
      minerva_ref_type__resource9 "HGNC_SYMBOL__NADK"
      minerva_type "Protein"
      minerva_x 1483.0
      minerva_y 1969.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa75"
      species_meta_id "s_id_sa75"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 16
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nadsyn1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa84"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa84"
      minerva_name "NADSYN1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/55191"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000172890"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/protein/NM_018161"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/55191"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NADSYN1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NADSYN1"
      minerva_ref_link5 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=6.3.5.1"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/29832"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/Q6IA69"
      minerva_ref_link8 "https://purl.uniprot.org/uniprot/Q6IA69"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/pubmed/12547821"
      minerva_ref_type__resource1 "ENTREZ__55191"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000172890"
      minerva_ref_type__resource11 "REFSEQ__NM_018161"
      minerva_ref_type__resource2 "ENTREZ__55191"
      minerva_ref_type__resource3 "HGNC_SYMBOL__NADSYN1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__NADSYN1"
      minerva_ref_type__resource5 "EC__6.3.5.1"
      minerva_ref_type__resource6 "HGNC__29832"
      minerva_ref_type__resource7 "UNIPROT__Q6IA69"
      minerva_ref_type__resource8 "UNIPROT__Q6IA69"
      minerva_ref_type__resource9 "PUBMED__12547821"
      minerva_type "Protein"
      minerva_x 886.0
      minerva_y 1950.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa84"
      species_meta_id "s_id_sa84"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 17
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ak8__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa390"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa390"
      minerva_former_symbols "C9orf98"
      minerva_name "AK8"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/158067"
      minerva_ref_link10 "https://purl.uniprot.org/uniprot/Q96MA6"
      minerva_ref_link11 "https://purl.uniprot.org/uniprot/Q96MA6"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/158067"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AK8"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000165695"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AK8"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/26526"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_152572"
      minerva_ref_link8 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link9 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.3"
      minerva_ref_type__resource1 "ENTREZ__158067"
      minerva_ref_type__resource10 "UNIPROT__Q96MA6"
      minerva_ref_type__resource11 "UNIPROT__Q96MA6"
      minerva_ref_type__resource2 "ENTREZ__158067"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AK8"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000165695"
      minerva_ref_type__resource5 "HGNC_SYMBOL__AK8"
      minerva_ref_type__resource6 "HGNC__26526"
      minerva_ref_type__resource7 "REFSEQ__NM_152572"
      minerva_ref_type__resource8 "EC__2.7.4.6"
      minerva_ref_type__resource9 "EC__2.7.4.3"
      minerva_type "Protein"
      minerva_x 3847.0
      minerva_y 868.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa390"
      species_meta_id "s_id_sa390"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 18
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 800.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "dadp__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa369"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa369"
      minerva_fullName "dADP"
      minerva_name "dADP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16174"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/compound/188966"
      minerva_ref_type__resource1 "CHEBI__CHEBI:16174"
      minerva_ref_type__resource2 "PUBCHEM__188966"
      minerva_synonyms "2'-DEOXYADENOSINE-5'-DIPHOSPHATE; 2'-Deoxyadenosine 5'-diphosphate; 2'-deoxyadenosine 5'-diphosphate; dADP; deoxyadenosine diphosphate"
      minerva_type "Simple molecule"
      minerva_x 3377.5
      minerva_y 1167.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa369"
      species_meta_id "s_id_sa369"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 19
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nucleoside_space_diphosphate_space_kinase__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa2"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa5"
      minerva_elementId2 "csa2"
      minerva_name "Nucleoside diphosphate kinase"
      minerva_type "Complex"
      minerva_x 3037.875
      minerva_x2 4964.0
      minerva_y 1266.625
      minerva_y2 2217.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa2"
      species_meta_id "s_id_csa2"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 20
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1000.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "aminoimidazole_space_ribotide__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa179"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa179"
      minerva_fullName "5-amino-1-(5-phospho-beta-D-ribosyl)imidazole"
      minerva_name "Aminoimidazole ribotide"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:138560"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/compound/161500"
      minerva_ref_type__resource1 "CHEBI__CHEBI:138560"
      minerva_ref_type__resource2 "PUBCHEM__161500"
      minerva_synonyms "1-(5'-phosphoribosyl)-5-aminoimidazole; 1-(5-phospho-D-Ribosyl)-5-aminoimidazole; 5'-phosphoribosyl-5-aminoimidazole; 5-Amino-1-beta-D-ribofuranosyl-5'-(dihydrogen phosphate)-imidazole; 5-Amino-1-ribofuranosylimidazole 5'-phosphate; 5-aminoimidazole ribonucleotide; AIR; Aminoimidazole ribonucleotide; Aminoimidazole ribotide; phosphoribosylaminoimidazole"
      minerva_type "Simple molecule"
      minerva_x 3225.0
      minerva_y 752.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa179"
      species_meta_id "s_id_sa179"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 21
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nad__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa70"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa70"
      minerva_elementId2 "sa205"
      minerva_elementId3 "sa323"
      minerva_fullName "NAD(+)"
      minerva_name "NAD"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/5892"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15846"
      minerva_ref_type__resource1 "PUBCHEM__5892"
      minerva_ref_type__resource2 "CHEBI__CHEBI:15846"
      minerva_synonyms "DPN; Diphosphopyridine nucleotide; NAD; NAD+; Nadide; Nicotinamide adenine dinucleotide; beta-NAD"
      minerva_type "Simple molecule"
      minerva_x 1252.0
      minerva_x2 5411.916666666666
      minerva_x3 4361.25
      minerva_y 1905.0
      minerva_y2 1216.1525082851072
      minerva_y3 1254.5691749517873
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa70"
      species_meta_id "s_id_sa70"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 22
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1200.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "_alpha__minus_d_minus_galactose_minus_1p__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa4"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa4"
      minerva_fullName "alpha-D-galactose 1-phosphate"
      minerva_name "α-D-Galactose-1P"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/123912"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17973"
      minerva_ref_type__resource1 "PUBCHEM__123912"
      minerva_ref_type__resource2 "CHEBI__CHEBI:17973"
      minerva_synonyms "1-O-phosphono-alpha-D-galactopyranose; Galactose-1-phosphate; alpha-D-Gal-1-P; alpha-D-Galactopyranose 1-phosphate; alpha-D-Galactopyranose, 1-(dihydrogen phosphate); alpha-D-Galactopyranosyl phosphate; alpha-D-Galactose 1-phosphate; alpha-D-Galactosyl phosphate"
      minerva_type "Simple molecule"
      minerva_x 2622.6875
      minerva_y 2342.75
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa4"
      species_meta_id "s_id_sa4"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 23
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "pi__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa347"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa99"
      minerva_elementId10 "sa314"
      minerva_elementId11 "sa175"
      minerva_elementId12 "sa259"
      minerva_elementId13 "sa280"
      minerva_elementId14 "sa289"
      minerva_elementId15 "sa165"
      minerva_elementId16 "sa111"
      minerva_elementId17 "sa285"
      minerva_elementId18 "sa345"
      minerva_elementId19 "sa181"
      minerva_elementId2 "sa193"
      minerva_elementId20 "sa270"
      minerva_elementId21 "sa312"
      minerva_elementId3 "sa319"
      minerva_elementId4 "sa347"
      minerva_elementId5 "sa143"
      minerva_elementId6 "sa356"
      minerva_elementId7 "sa311"
      minerva_elementId8 "sa279"
      minerva_elementId9 "sa273"
      minerva_fullName "phosphate(3-)"
      minerva_name "Pi"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/1061"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18367"
      minerva_ref_type__resource1 "PUBCHEM__1061"
      minerva_ref_type__resource2 "CHEBI__CHEBI:18367"
      minerva_synonyms "Orthophosphate; PHOSPHATE ION; PO4(3-); Phosphate; [PO4](3-)"
      minerva_type "Simple molecule"
      minerva_x 1362.0
      minerva_x10 4999.041666666667
      minerva_x11 2559.6985294117644
      minerva_x12 4189.5
      minerva_x13 5750.5
      minerva_x14 5374.375
      minerva_x15 1517.25
      minerva_x16 1322.5
      minerva_x17 5380.25
      minerva_x18 4180.75
      minerva_x19 3140.9485294117644
      minerva_x2 4020.0
      minerva_x20 4761.5
      minerva_x21 4769.5
      minerva_x3 4806.25
      minerva_x4 4296.5
      minerva_x5 469.2352941176471
      minerva_x6 4096.625
      minerva_x7 4998.0
      minerva_x8 5737.5
      minerva_x9 4957.5
      minerva_y 1430.0
      minerva_y10 1219.3088235294117
      minerva_y11 713.4558823529411
      minerva_y12 1581.0
      minerva_y13 1882.0
      minerva_y14 1843.5441176470588
      minerva_y15 793.5
      minerva_y16 1320.0
      minerva_y17 2044.0882352941176
      minerva_y18 1098.0
      minerva_y19 713.6470588235294
      minerva_y2 831.5
      minerva_y20 1566.0
      minerva_y21 1476.0
      minerva_y3 1232.0
      minerva_y4 1098.0
      minerva_y5 1506.0882352941176
      minerva_y6 1338.125
      minerva_y7 1475.0
      minerva_y8 1862.0
      minerva_y9 1639.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa347"
      species_meta_id "s_id_sa347"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 24
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "dck__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa255"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa368"
      minerva_elementId2 "sa255"
      minerva_name "DCK"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DCK"
      minerva_ref_link10 "https://purl.uniprot.org/uniprot/P27707"
      minerva_ref_link11 "https://purl.uniprot.org/uniprot/P27707"
      minerva_ref_link12 "https://www.ncbi.nlm.nih.gov/protein/NM_000788"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/1633"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DCK"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/1633"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000156136"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.1.76"
      minerva_ref_link7 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.1.113"
      minerva_ref_link8 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.1.74"
      minerva_ref_link9 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/2704"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DCK"
      minerva_ref_type__resource10 "UNIPROT__P27707"
      minerva_ref_type__resource11 "UNIPROT__P27707"
      minerva_ref_type__resource12 "REFSEQ__NM_000788"
      minerva_ref_type__resource2 "ENTREZ__1633"
      minerva_ref_type__resource3 "HGNC_SYMBOL__DCK"
      minerva_ref_type__resource4 "ENTREZ__1633"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000156136"
      minerva_ref_type__resource6 "EC__2.7.1.76"
      minerva_ref_type__resource7 "EC__2.7.1.113"
      minerva_ref_type__resource8 "EC__2.7.1.74"
      minerva_ref_type__resource9 "HGNC__2704"
      minerva_type "Protein"
      minerva_x 3517.125
      minerva_x2 4414.0
      minerva_y 1488.125
      minerva_y2 1731.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa255"
      species_meta_id "s_id_sa255"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 25
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nicotinate_minus_adenine_space_dinucleotide_space_phosphate__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa107"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa107"
      minerva_fullName "nicotinate-adenine dinucleotide phosphate(4-)"
      minerva_name "nicotinate-adenine dinucleotide phosphate"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/71768143"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:75967"
      minerva_ref_type__resource1 "PUBCHEM__71768143"
      minerva_ref_type__resource2 "CHEBI__CHEBI:75967"
      minerva_synonyms "NAADP(4+); nicotinate-adenine dinucleotide phosphate; nicotinate-adenine dinucleotide phosphate(4-)"
      minerva_type "Simple molecule"
      minerva_x 1531.5
      minerva_y 1201.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa107"
      species_meta_id "s_id_sa107"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 26
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "l_minus_glutamine__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa218"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa218"
      minerva_elementId2 "sa155"
      minerva_elementId3 "sa173"
      minerva_elementId4 "sa85"
      minerva_fullName "L-glutamine"
      minerva_name "L-Glutamine"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18050"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/compound/5961"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18050"
      minerva_ref_type__resource2 "PUBCHEM__5961"
      minerva_synonyms "(2S)-2,5-diamino-5-oxopentanoic acid; (2S)-2-amino-4-carbamoylbutanoic acid; (S)-2,5-diamino-5-oxopentanoic acid; GLUTAMINE; Glutamic acid 5-amide; Glutamic acid amide; L-(+)-glutamine; L-2-Aminoglutaramic acid; L-2-aminoglutaramic acid; L-2-aminoglutaramic acid; L-Glutamin; L-Glutamine; L-Glutaminsaeure-5-amid; L-glutamic acid gamma-amide; Levoglutamide; Q"
      minerva_type "Simple molecule"
      minerva_x 5142.166666666666
      minerva_x2 1264.25
      minerva_x3 2167.6985294117644
      minerva_x4 712.5
      minerva_y 1536.7352941176473
      minerva_y2 1048.0882352941178
      minerva_y3 703.0441176470589
      minerva_y4 1949.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa218"
      species_meta_id "s_id_sa218"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 27
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "amp__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa152"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa97"
      minerva_elementId2 "sa224"
      minerva_elementId3 "sa152"
      minerva_elementId4 "sa301"
      minerva_fullName "adenosine 5'-monophosphate"
      minerva_name "AMP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16027"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/compound/6083"
      minerva_ref_type__resource1 "CHEBI__CHEBI:16027"
      minerva_ref_type__resource2 "PUBCHEM__6083"
      minerva_synonyms "5'-AMP; 5'-Adenosine monophosphate; 5'-Adenylic acid; 5'-O-phosphonoadenosine; ADENOSINE MONOPHOSPHATE; AMP; Adenosine 5'-monophosphate; Adenosine 5'-phosphate; Adenosine-5'-monophosphoric acid; Adenylate; Adenylic acid; Ado5'P; PAdo; adenosine 5'-(dihydrogen phosphate); adenosine phosphate; adenosine phosphate; adenosine-5'P; adenosini phosphas; fosfato de adenosina; pA; phosphate d'adenosine"
      minerva_type "Simple molecule"
      minerva_x 1345.0
      minerva_x2 5179.049019607843
      minerva_x3 923.0
      minerva_x4 4235.0
      minerva_y 1640.0
      minerva_y2 1740.686274509804
      minerva_y3 1113.3235294117646
      minerva_y4 924.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa152"
      species_meta_id "s_id_sa152"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 28
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "5_minus_phospho_minus__alpha__minus_d_minus_ribose_space_1_minus_diphosphate__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa351"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa330"
      minerva_elementId2 "sa269"
      minerva_elementId3 "sa351"
      minerva_elementId4 "sa110"
      minerva_fullName "5-O-phosphono-alpha-D-ribofuranosyl diphosphate"
      minerva_name "5-phospho-α-D-ribose 1-diphosphate"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17111"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/compound/7339"
      minerva_ref_type__resource1 "CHEBI__CHEBI:17111"
      minerva_ref_type__resource2 "PUBCHEM__7339"
      minerva_synonyms "5-Phospho-alpha-D-ribose 1-diphosphate; 5-Phosphoribosyl 1-pyrophosphate; 5-Phosphoribosyl diphosphate; ALPHA-PHOSPHORIBOSYLPYROPHOSPHORIC ACID; PRPP; PRib-PP; alpha-D-ribofuranose 5-(dihydrogen phosphate) 1-(trihydrogen diphosphate); phosphoribosyl pyrophosphate; phosphoribosylpyrophosphate"
      minerva_type "Simple molecule"
      minerva_x 4617.0
      minerva_x2 4786.0
      minerva_x3 3815.0
      minerva_x4 1043.5
      minerva_y 1104.0
      minerva_y2 1856.5
      minerva_y3 1176.0
      minerva_y4 1266.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa351"
      species_meta_id "s_id_sa351"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 29
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 800.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "1_minus_(5'_minus_phosphoribosyl)_minus_5_minus_amino_minus_4_minus_(n_minus_succinocarboxamide)_minus_imidazole__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa189"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa189"
      minerva_fullName "SAICAR"
      minerva_name "1-(5'-Phosphoribosyl)-5-amino-4-(N-succinocarboxamide)-imidazole"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18319"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/compound/160666"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18319"
      minerva_ref_type__resource2 "PUBCHEM__160666"
      minerva_synonyms "(2S)-2-[5-amino-1-(5-phospho-beta-D-ribosyl)imidazole-4-carboxamido]succinic acid; (S)-2-[5-Amino-1-(5-phospho-D-ribosyl)imidazole-4-carboxamido]succinate; 1-(5'-Phosphoribosyl)-4-(N-succinocarboxamide)-5-aminoimidazole; 1-(5'-Phosphoribosyl)-5-amino-4-(N-succinocarboxamide)-imidazole; 5'-Phosphoribosyl-4-(N-succinocarboxamide)-5-aminoimidazole; SAICA riboside; SAICAR; Succino-AICAR; succinyl-5-aminoimidazole-4-carboxamide-1-ribose-5-phosphate; succinylaminoimidazolecarboxamide ribose-5'-phosphate"
      minerva_type "Simple molecule"
      minerva_x 4250.0
      minerva_y 752.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa189"
      species_meta_id "s_id_sa189"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 30
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "gart__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa160"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa160"
      minerva_former_symbols "PGFT; PRGS"
      minerva_name "GART"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_000819"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=GART"
      minerva_ref_link11 "https://www.ensembl.org/id/ENSG00000159131"
      minerva_ref_link12 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=GART"
      minerva_ref_link2 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.1.2.2"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P22102"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P22102"
      minerva_ref_link5 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=6.3.3.1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/2618"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/2618"
      minerva_ref_link8 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=6.3.4.13"
      minerva_ref_link9 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/4163"
      minerva_ref_type__resource1 "REFSEQ__NM_000819"
      minerva_ref_type__resource10 "HGNC_SYMBOL__GART"
      minerva_ref_type__resource11 "ENSEMBL__ENSG00000159131"
      minerva_ref_type__resource12 "HGNC_SYMBOL__GART"
      minerva_ref_type__resource2 "EC__2.1.2.2"
      minerva_ref_type__resource3 "UNIPROT__P22102"
      minerva_ref_type__resource4 "UNIPROT__P22102"
      minerva_ref_type__resource5 "EC__6.3.3.1"
      minerva_ref_type__resource6 "ENTREZ__2618"
      minerva_ref_type__resource7 "ENTREZ__2618"
      minerva_ref_type__resource8 "EC__6.3.4.13"
      minerva_ref_type__resource9 "HGNC__4163"
      minerva_type "Protein"
      minerva_x 1622.0
      minerva_y 642.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa160"
      species_meta_id "s_id_sa160"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 31
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1000.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ppi__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa223"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa223"
      minerva_elementId10 "sa349"
      minerva_elementId11 "sa268"
      minerva_elementId12 "sa142"
      minerva_elementId2 "sa90"
      minerva_elementId3 "sa157"
      minerva_elementId4 "sa135"
      minerva_elementId5 "sa18"
      minerva_elementId6 "sa331"
      minerva_elementId7 "sa265"
      minerva_elementId8 "sa109"
      minerva_elementId9 "sa130"
      minerva_fullName "diphosphate(4-)"
      minerva_name "PPi"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/644102"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18361"
      minerva_ref_type__resource1 "PUBCHEM__644102"
      minerva_ref_type__resource2 "CHEBI__CHEBI:18361"
      minerva_synonyms "DIPHOSPHATE; Diphosphat; Diphosphate; P2O7(4-); PPi; Pyrophosphat; Pyrophosphate; [O3POPO3](4-); pyrophosphate ion"
      minerva_type "Simple molecule"
      minerva_x 5156.2843137254895
      minerva_x10 3902.5
      minerva_x11 5118.5
      minerva_x12 471.2352941176471
      minerva_x2 1174.5
      minerva_x3 1065.0
      minerva_x4 713.294117647059
      minerva_x5 2677.5
      minerva_x6 5054.5
      minerva_x7 4419.5
      minerva_x8 1080.5
      minerva_x9 485.7692307692307
      minerva_y 1704.980392156863
      minerva_y10 1001.0
      minerva_y11 1845.0
      minerva_y12 1468.6176470588236
      minerva_y2 1838.0
      minerva_y3 849.0294117647059
      minerva_y4 1554.735294117647
      minerva_y5 2171.0
      minerva_y6 1166.0
      minerva_y7 1888.0
      minerva_y8 1400.0
      minerva_y9 1830.9615384615386
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa223"
      species_meta_id "s_id_sa223"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 32
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "raffinose__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa49"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa49"
      minerva_fullName "raffinose"
      minerva_name "Raffinose"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16634"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/compound/439242"
      minerva_ref_type__resource1 "CHEBI__CHEBI:16634"
      minerva_ref_type__resource2 "PUBCHEM__439242"
      minerva_synonyms "6G-alpha-D-galactosylsucrose; Gossypose; Melitose; Melitriose; Raffinose; alpha-D-Galp-(1->6)-alpha-D-Glcp-(1<->2)-beta-D-Fruf; alpha-D-galactopyranosyl-(1->6)-alpha-D-glucopyranosyl beta-D-fructofuranoside; raffinose; rafinose; raflinose"
      minerva_type "Simple molecule"
      minerva_x 1714.0
      minerva_y 2345.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa49"
      species_meta_id "s_id_sa49"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 33
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1200.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nicotinamide_space_d_minus_ribonucleotide__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa69"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa69"
      minerva_fullName "NMN zwitterion"
      minerva_name "Nicotinamide D-ribonucleotide"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/14180"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16171"
      minerva_ref_type__resource1 "PUBCHEM__14180"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16171"
      minerva_synonyms "3-(aminocarbonyl)-1-(5-O-phosphonato-beta-D-ribofuranosyl)pyridinium; 3-(aminocarbonyl)-1-(5-O-phosphono-beta-D-ribofuranosyl)pyridinium, inner salt; NMN; Nicotinamide D-ribonucleotide; Nicotinamide mononucleotide; Nicotinamide nucleotide; Nicotinamide ribonucleotide; beta-Nicotinamide D-ribonucleotide; beta-Nicotinamide mononucleotide; beta-Nicotinamide ribonucleotide"
      minerva_type "Simple molecule"
      minerva_x 1249.5
      minerva_y 1588.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa69"
      species_meta_id "s_id_sa69"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 34
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "qprt__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa132"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa132"
      minerva_name "QPRT"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/9755"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_014298"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=QPRT"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/23475"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000103485"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=QPRT"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/23475"
      minerva_ref_link7 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.4.2.19"
      minerva_ref_link8 "https://purl.uniprot.org/uniprot/Q15274"
      minerva_ref_link9 "https://purl.uniprot.org/uniprot/Q15274"
      minerva_ref_type__resource1 "HGNC__9755"
      minerva_ref_type__resource10 "REFSEQ__NM_014298"
      minerva_ref_type__resource2 "HGNC_SYMBOL__QPRT"
      minerva_ref_type__resource3 "ENTREZ__23475"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000103485"
      minerva_ref_type__resource5 "HGNC_SYMBOL__QPRT"
      minerva_ref_type__resource6 "ENTREZ__23475"
      minerva_ref_type__resource7 "EC__2.4.2.19"
      minerva_ref_type__resource8 "UNIPROT__Q15274"
      minerva_ref_type__resource9 "UNIPROT__Q15274"
      minerva_type "Protein"
      minerva_x 858.0
      minerva_y 1506.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa132"
      species_meta_id "s_id_sa132"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 35
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nmnat3__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa93"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa93"
      minerva_name "NMNAT3"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.7.18"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/349565"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/gene/349565"
      minerva_ref_link12 "https://www.ncbi.nlm.nih.gov/pubmed/17402747"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000163864"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/Q96T66"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/Q96T66"
      minerva_ref_link5 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.7.1"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/20989"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NMNAT3"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_178177"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NMNAT3"
      minerva_ref_type__resource1 "EC__2.7.7.18"
      minerva_ref_type__resource10 "ENTREZ__349565"
      minerva_ref_type__resource11 "ENTREZ__349565"
      minerva_ref_type__resource12 "PUBMED__17402747"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000163864"
      minerva_ref_type__resource3 "UNIPROT__Q96T66"
      minerva_ref_type__resource4 "UNIPROT__Q96T66"
      minerva_ref_type__resource5 "EC__2.7.7.1"
      minerva_ref_type__resource6 "HGNC__20989"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NMNAT3"
      minerva_ref_type__resource8 "REFSEQ__NM_178177"
      minerva_ref_type__resource9 "HGNC_SYMBOL__NMNAT3"
      minerva_type "Protein"
      minerva_x 904.0
      minerva_y 1787.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa93"
      species_meta_id "s_id_sa93"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 36
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nadp__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa106"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa106"
      minerva_elementId2 "sa29"
      minerva_elementId3 "sa296"
      minerva_fullName "NADP(+)"
      minerva_name "NADP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18009"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/compound/5886"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18009"
      minerva_ref_type__resource2 "PUBCHEM__5886"
      minerva_synonyms "NADP; NADP+; Nicotinamide adenine dinucleotide phosphate; TPN; Triphosphopyridine nucleotide; beta-Nicotinamide adenine dinucleotide phosphate; beta-nicotinamide adenine dinucleotide phosphate; oxidized nicotinamide-adenine dinucleotide phosphate"
      minerva_type "Simple molecule"
      minerva_x 723.0
      minerva_x2 1848.5
      minerva_x3 5751.75
      minerva_y 1025.5
      minerva_y2 1905.5
      minerva_y3 1355.8846153846152
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa106"
      species_meta_id "s_id_sa106"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 37
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "5_minus_phosphoribosyl_minus_n_minus_formylglycinamide__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa166"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa166"
      minerva_name "5-phosphoribosyl-N-formylglycinamide"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/129652037"
      minerva_ref_type__resource1 "PUBCHEM__129652037"
      minerva_type "Simple molecule"
      minerva_x 2067.0
      minerva_y 752.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa166"
      species_meta_id "s_id_sa166"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 38
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "co2__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa136"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa186"
      minerva_elementId2 "sa136"
      minerva_fullName "carbon dioxide"
      minerva_name "CO2"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16526"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/compound/280"
      minerva_ref_type__resource1 "CHEBI__CHEBI:16526"
      minerva_ref_type__resource2 "PUBCHEM__280"
      minerva_synonyms "CARBON DIOXIDE; CO2; CO2; Carbon dioxide; E 290; E-290; E290; R-744; [CO2]; carbonic anhydride"
      minerva_type "Simple molecule"
      minerva_x 3317.5
      minerva_x2 700.0
      minerva_y 792.0
      minerva_y2 1593.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa136"
      species_meta_id "s_id_sa136"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 39
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "deamino_minus_nad__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa71"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa71"
      minerva_fullName "deamido-NAD(+)"
      minerva_name "Deamino-NAD"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/135421870"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18304"
      minerva_ref_type__resource1 "PUBCHEM__135421870"
      minerva_ref_type__resource2 "CHEBI__CHEBI:18304"
      minerva_synonyms "Deamido-NAD; Deamido-NAD+; Deamino-NAD+"
      minerva_type "Simple molecule"
      minerva_x 551.0
      minerva_y 1893.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa71"
      species_meta_id "s_id_sa71"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 40
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 800.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ak7__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa391"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa391"
      minerva_name "AK7"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/Q96M32"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.3"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/protein/NM_001350888"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/Q96M32"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/20091"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AK7"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AK7"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/122481"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/122481"
      minerva_ref_link8 "https://www.ensembl.org/id/ENSG00000140057"
      minerva_ref_link9 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_type__resource1 "UNIPROT__Q96M32"
      minerva_ref_type__resource10 "EC__2.7.4.3"
      minerva_ref_type__resource11 "REFSEQ__NM_001350888"
      minerva_ref_type__resource2 "UNIPROT__Q96M32"
      minerva_ref_type__resource3 "HGNC__20091"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AK7"
      minerva_ref_type__resource5 "HGNC_SYMBOL__AK7"
      minerva_ref_type__resource6 "ENTREZ__122481"
      minerva_ref_type__resource7 "ENTREZ__122481"
      minerva_ref_type__resource8 "ENSEMBL__ENSG00000140057"
      minerva_ref_type__resource9 "EC__2.7.4.6"
      minerva_type "Protein"
      minerva_x 3847.0
      minerva_y 963.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa391"
      species_meta_id "s_id_sa391"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 41
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "5_minus_phospho_minus_beta_minus_d_minus_ribosylamine__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa153"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa153"
      minerva_fullName "5-phospho-beta-D-ribosylamine"
      minerva_name "5-phospho-beta-D-ribosylamine"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:37737"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/compound/439905"
      minerva_ref_type__resource1 "CHEBI__CHEBI:37737"
      minerva_ref_type__resource2 "PUBCHEM__439905"
      minerva_synonyms "5-Phospho-D-ribosylamine; 5-Phospho-beta-D-ribosylamine; 5-Phosphoribosyl-1-amine; 5-Phosphoribosylamine"
      minerva_type "Simple molecule"
      minerva_x 1096.5
      minerva_y 752.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa153"
      species_meta_id "s_id_sa153"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 42
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1000.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "atic__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa199"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa199"
      minerva_name "ATIC"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000138363"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/471"
      minerva_ref_link11 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.1.2.3"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P31939"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P31939"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_004044"
      minerva_ref_link5 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.5.4.10"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/794"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATIC"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/471"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATIC"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000138363"
      minerva_ref_type__resource10 "ENTREZ__471"
      minerva_ref_type__resource11 "EC__2.1.2.3"
      minerva_ref_type__resource2 "UNIPROT__P31939"
      minerva_ref_type__resource3 "UNIPROT__P31939"
      minerva_ref_type__resource4 "REFSEQ__NM_004044"
      minerva_ref_type__resource5 "EC__3.5.4.10"
      minerva_ref_type__resource6 "HGNC__794"
      minerva_ref_type__resource7 "HGNC_SYMBOL__ATIC"
      minerva_ref_type__resource8 "ENTREZ__471"
      minerva_ref_type__resource9 "HGNC_SYMBOL__ATIC"
      minerva_type "Protein"
      minerva_x 5015.0
      minerva_y 873.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa199"
      species_meta_id "s_id_sa199"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 43
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "_alpha__minus_d_minus_ribose_space_1_minus_phosphate__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa271"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa271"
      minerva_elementId2 "sa318"
      minerva_elementId3 "sa112"
      minerva_fullName "alpha-D-ribose 1-phosphate"
      minerva_name "α-D-Ribose 1-phosphate"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/439236"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16300"
      minerva_ref_type__resource1 "PUBCHEM__439236"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16300"
      minerva_synonyms "1-O-phosphono-alpha-D-ribofuranose; 1-phospho-alpha-D-ribofuranose; D-Ribose 1-phosphate; Ribose 1-phosphate; alpha-D-Ribose 1-phosphate; alpha-D-ribose 1-phosphate"
      minerva_type "Simple molecule"
      minerva_x 4558.5
      minerva_x2 4563.25
      minerva_x3 1105.0
      minerva_y 1492.0
      minerva_y2 1232.0
      minerva_y3 1187.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa271"
      species_meta_id "s_id_sa271"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 44
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1200.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ugp2__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa12"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa12"
      minerva_former_symbols "UGP1"
      minerva_name "UGP2"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=UGP2"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_006759"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/7360"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/Q16851"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/Q16851"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=UGP2"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/7360"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12527"
      minerva_ref_link8 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.7.9"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000169764"
      minerva_ref_type__resource1 "HGNC_SYMBOL__UGP2"
      minerva_ref_type__resource10 "REFSEQ__NM_006759"
      minerva_ref_type__resource2 "ENTREZ__7360"
      minerva_ref_type__resource3 "UNIPROT__Q16851"
      minerva_ref_type__resource4 "UNIPROT__Q16851"
      minerva_ref_type__resource5 "HGNC_SYMBOL__UGP2"
      minerva_ref_type__resource6 "ENTREZ__7360"
      minerva_ref_type__resource7 "HGNC__12527"
      minerva_ref_type__resource8 "EC__2.7.7.9"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000169764"
      minerva_type "Protein"
      minerva_x 2769.0
      minerva_y 2162.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa12"
      species_meta_id "s_id_sa12"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 45
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nme6__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa382"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa382"
      minerva_elementId2 "sa236"
      minerva_name "NME6"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/20567"
      minerva_ref_link10 "https://purl.uniprot.org/uniprot/O75414"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME6"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME6"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_005793"
      minerva_ref_link5 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000172113"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/10201"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/10201"
      minerva_ref_link9 "https://purl.uniprot.org/uniprot/O75414"
      minerva_ref_type__resource1 "HGNC__20567"
      minerva_ref_type__resource10 "UNIPROT__O75414"
      minerva_ref_type__resource2 "HGNC_SYMBOL__NME6"
      minerva_ref_type__resource3 "HGNC_SYMBOL__NME6"
      minerva_ref_type__resource4 "REFSEQ__NM_005793"
      minerva_ref_type__resource5 "EC__2.7.4.6"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000172113"
      minerva_ref_type__resource7 "ENTREZ__10201"
      minerva_ref_type__resource8 "ENTREZ__10201"
      minerva_ref_type__resource9 "UNIPROT__O75414"
      minerva_type "Protein"
      minerva_x 3083.875
      minerva_x2 4959.0
      minerva_y 1213.125
      minerva_y2 2131.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa382"
      species_meta_id "s_id_sa382"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 46
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "gla__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa55"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa55"
      minerva_name "GLA"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=GLA"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_000169"
      minerva_ref_link11 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/4296"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=GLA"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.2.1.22"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000102393"
      minerva_ref_link5 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.2.1.47"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/2717"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/2717"
      minerva_ref_link8 "https://purl.uniprot.org/uniprot/P06280"
      minerva_ref_link9 "https://purl.uniprot.org/uniprot/P06280"
      minerva_ref_type__resource1 "HGNC_SYMBOL__GLA"
      minerva_ref_type__resource10 "REFSEQ__NM_000169"
      minerva_ref_type__resource11 "HGNC__4296"
      minerva_ref_type__resource2 "HGNC_SYMBOL__GLA"
      minerva_ref_type__resource3 "EC__3.2.1.22"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000102393"
      minerva_ref_type__resource5 "EC__3.2.1.47"
      minerva_ref_type__resource6 "ENTREZ__2717"
      minerva_ref_type__resource7 "ENTREZ__2717"
      minerva_ref_type__resource8 "UNIPROT__P06280"
      minerva_ref_type__resource9 "UNIPROT__P06280"
      minerva_type "Protein"
      minerva_x 1553.6666666666667
      minerva_y 2603.3333333333335
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa55"
      species_meta_id "s_id_sa55"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 47
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "adsl__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa197"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa197"
      minerva_name "ADSL"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ADSL"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000239900"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ADSL"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/158"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/158"
      minerva_ref_link5 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=4.3.2.2"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/291"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/P30566"
      minerva_ref_link8 "https://purl.uniprot.org/uniprot/P30566"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_000026"
      minerva_ref_type__resource1 "HGNC_SYMBOL__ADSL"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000239900"
      minerva_ref_type__resource2 "HGNC_SYMBOL__ADSL"
      minerva_ref_type__resource3 "ENTREZ__158"
      minerva_ref_type__resource4 "ENTREZ__158"
      minerva_ref_type__resource5 "EC__4.3.2.2"
      minerva_ref_type__resource6 "HGNC__291"
      minerva_ref_type__resource7 "UNIPROT__P30566"
      minerva_ref_type__resource8 "UNIPROT__P30566"
      minerva_ref_type__resource9 "REFSEQ__NM_000026"
      minerva_type "Protein"
      minerva_x 4520.0
      minerva_y 680.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa197"
      species_meta_id "s_id_sa197"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 48
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 500.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "o_minus_acetyl_minus_adp_minus_ribose__mitochondrion__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa117"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa117"
      minerva_fullName "2''-O-acetyl-ADP-D-ribose"
      minerva_name "O-Acetyl-ADP-ribose"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/72193709"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:76279"
      minerva_ref_type__resource1 "PUBCHEM__72193709"
      minerva_ref_type__resource2 "CHEBI__CHEBI:76279"
      minerva_synonyms "2'-O-acetyl adenosine-5-diphosphoribose; 2'-O-acetyl-ADP-ribose; 2'-OAADPr; O-acetyl-ADP-ribose; OAADPr"
      minerva_type "Simple molecule"
      minerva_x 1873.5
      minerva_y 945.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrion"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa117"
      species_meta_id "s_id_sa117"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 49
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "damp__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa364"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa364"
      minerva_fullName "2'-deoxyadenosine 5'-monophosphate"
      minerva_name "dAMP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17713"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/compound/12599"
      minerva_ref_type__resource1 "CHEBI__CHEBI:17713"
      minerva_ref_type__resource2 "PUBCHEM__12599"
      minerva_synonyms "2'-DEOXYADENOSINE-5'-MONOPHOSPHATE; 2'-Deoxyadenosine 5'-monophosphate; 2'-Deoxyadenosine 5'-phosphate; 2'-dAMP; 2'-deoxy-5'-adenosine monophosphate; 2'-deoxy-AMP; 2'-deoxyadenosine 5'-(dihydrogen phosphate); 2'-deoxyadenosine 5'-monophosphate; 2'-deoxyadenosine monophosphate; 2'-deoxyadenylic acid; Deoxyadenosine monophosphate; Deoxyadenylic acid; dAMP; deoxy-5'-adenylic acid; deoxy-AMP; deoxyadenosine 5'-monophosphate; deoxyadenosine 5'-phosphate"
      minerva_type "Simple molecule"
      minerva_x 3379.5
      minerva_y 1412.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa364"
      species_meta_id "s_id_sa364"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 50
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "n_minus_ribosyl_minus_nicotinamide__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa68"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa68"
      minerva_fullName "N-ribosylnicotinamide"
      minerva_name "N-Ribosyl-nicotinamide"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15927"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/compound/439924"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15927"
      minerva_ref_type__resource2 "PUBCHEM__439924"
      minerva_synonyms "1-(beta-D-Ribofuranosyl)nicotinamide; N-Ribosylnicotinamide; Nicotinamide riboside; beta-nicotinamide D-riboside; nicotinamide ribonucleoside; nicotinamide ribose; nicotinamide-beta-riboside"
      minerva_type "Simple molecule"
      minerva_x 1249.5
      minerva_y 1378.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa68"
      species_meta_id "s_id_sa68"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 51
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 800.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "enpp1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa94"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa94"
      minerva_elementId2 "sa262"
      minerva_former_symbols "M6S1; NPPS; PDNP1"
      minerva_name "ENPP1"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/3356"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_006208"
      minerva_ref_link11 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ENPP1"
      minerva_ref_link2 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.1.4.1"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P22413"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P22413"
      minerva_ref_link5 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.6.1.9"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/5167"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/5167"
      minerva_ref_link8 "https://www.ensembl.org/id/ENSG00000197594"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ENPP1"
      minerva_ref_type__resource1 "HGNC__3356"
      minerva_ref_type__resource10 "REFSEQ__NM_006208"
      minerva_ref_type__resource11 "HGNC_SYMBOL__ENPP1"
      minerva_ref_type__resource2 "EC__3.1.4.1"
      minerva_ref_type__resource3 "UNIPROT__P22413"
      minerva_ref_type__resource4 "UNIPROT__P22413"
      minerva_ref_type__resource5 "EC__3.6.1.9"
      minerva_ref_type__resource6 "ENTREZ__5167"
      minerva_ref_type__resource7 "ENTREZ__5167"
      minerva_ref_type__resource8 "ENSEMBL__ENSG00000197594"
      minerva_ref_type__resource9 "HGNC_SYMBOL__ENPP1"
      minerva_type "Protein"
      minerva_x 1376.0
      minerva_x2 4401.0
      minerva_y 1706.0
      minerva_y2 1989.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa94"
      species_meta_id "s_id_sa94"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 52
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "guanine__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa256"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa256"
      minerva_fullName "guanine"
      minerva_name "Guanine"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/135398634"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16235"
      minerva_ref_type__resource1 "PUBCHEM__135398634"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16235"
      minerva_synonyms "2-Amino-6-hydroxypurine; 2-amino-6-oxopurine; G; GUANINE; Gua; Guanine; guanine"
      minerva_type "Simple molecule"
      minerva_x 4427.5
      minerva_y 1605.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa256"
      species_meta_id "s_id_sa256"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 53
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1000.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "xdh__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa321"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa321"
      minerva_name "XDH"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=XDH"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_000379"
      minerva_ref_link11 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12805"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=XDH"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P47989"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P47989"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000158125"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/7498"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/7498"
      minerva_ref_link8 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=1.17.1.4"
      minerva_ref_link9 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=1.17.3.2"
      minerva_ref_type__resource1 "HGNC_SYMBOL__XDH"
      minerva_ref_type__resource10 "REFSEQ__NM_000379"
      minerva_ref_type__resource11 "HGNC__12805"
      minerva_ref_type__resource2 "HGNC_SYMBOL__XDH"
      minerva_ref_type__resource3 "UNIPROT__P47989"
      minerva_ref_type__resource4 "UNIPROT__P47989"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000158125"
      minerva_ref_type__resource6 "ENTREZ__7498"
      minerva_ref_type__resource7 "ENTREZ__7498"
      minerva_ref_type__resource8 "EC__1.17.1.4"
      minerva_ref_type__resource9 "EC__1.17.3.2"
      minerva_type "Protein"
      minerva_x 4533.0
      minerva_y 1316.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa321"
      species_meta_id "s_id_sa321"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 54
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nmnat1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa92"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa92"
      minerva_former_symbols "LCA9"
      minerva_name "NMNAT1"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000173614"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/64802"
      minerva_ref_link11 "https://purl.uniprot.org/uniprot/Q9HAN9"
      minerva_ref_link12 "https://purl.uniprot.org/uniprot/Q9HAN9"
      minerva_ref_link2 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.7.18"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/17877"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NMNAT1"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NMNAT1"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.7.1"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_001297778"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/pubmed/12359228"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/64802"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000173614"
      minerva_ref_type__resource10 "ENTREZ__64802"
      minerva_ref_type__resource11 "UNIPROT__Q9HAN9"
      minerva_ref_type__resource12 "UNIPROT__Q9HAN9"
      minerva_ref_type__resource2 "EC__2.7.7.18"
      minerva_ref_type__resource3 "HGNC__17877"
      minerva_ref_type__resource4 "HGNC_SYMBOL__NMNAT1"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NMNAT1"
      minerva_ref_type__resource6 "EC__2.7.7.1"
      minerva_ref_type__resource7 "REFSEQ__NM_001297778"
      minerva_ref_type__resource8 "PUBMED__12359228"
      minerva_ref_type__resource9 "ENTREZ__64802"
      minerva_type "Protein"
      minerva_x 904.0
      minerva_y 1692.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa92"
      species_meta_id "s_id_sa92"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 55
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1200.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "galt__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa9"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa9"
      minerva_name "GALT"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_000155"
      minerva_ref_link10 "https://purl.uniprot.org/uniprot/P07902"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000213930"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/4135"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.7.12"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=GALT"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=GALT"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/2592"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/2592"
      minerva_ref_link9 "https://purl.uniprot.org/uniprot/P07902"
      minerva_ref_type__resource1 "REFSEQ__NM_000155"
      minerva_ref_type__resource10 "UNIPROT__P07902"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000213930"
      minerva_ref_type__resource3 "HGNC__4135"
      minerva_ref_type__resource4 "EC__2.7.7.12"
      minerva_ref_type__resource5 "HGNC_SYMBOL__GALT"
      minerva_ref_type__resource6 "HGNC_SYMBOL__GALT"
      minerva_ref_type__resource7 "ENTREZ__2592"
      minerva_ref_type__resource8 "ENTREZ__2592"
      minerva_ref_type__resource9 "UNIPROT__P07902"
      minerva_type "Protein"
      minerva_x 2763.0
      minerva_y 2285.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa9"
      species_meta_id "s_id_sa9"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 56
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "galacitol__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa27"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa27"
      minerva_fullName "galactitol"
      minerva_name "Galacitol"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16813"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/compound/11850"
      minerva_ref_type__resource1 "CHEBI__CHEBI:16813"
      minerva_ref_type__resource2 "PUBCHEM__11850"
      minerva_synonyms "(2R,3S,4R,5S)-hexane-1,2,3,4,5,6-hexol; D-Dulcitol; D-galactitol; Dulcitol; Dulcose; Euonymit; Galactitol; L-galactitol; Melampyrin; Melampyrit; galactitol"
      minerva_type "Simple molecule"
      minerva_x 1809.0
      minerva_y 2054.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa27"
      species_meta_id "s_id_sa27"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 57
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "deoxyinosine__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa355"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa355"
      minerva_fullName "2'-deoxyinosine"
      minerva_name "Deoxyinosine"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:28997"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/compound/135398593"
      minerva_ref_type__resource1 "CHEBI__CHEBI:28997"
      minerva_ref_type__resource2 "PUBCHEM__135398593"
      minerva_synonyms "2'-deoxyinosine; 9-(2-deoxy-beta-D-erythro-pentofuranosyl)-9H-purin-6-ol; Deoxyinosine; Deoxyinosine"
      minerva_type "Simple molecule"
      minerva_x 4055.0
      minerva_y 1411.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa355"
      species_meta_id "s_id_sa355"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 58
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "guk1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa226"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa226"
      minerva_name "GUK1"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/4693"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_000858"
      minerva_ref_link11 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.8"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/Q16774"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/Q16774"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/2987"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/2987"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000143774"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=GUK1"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=GUK1"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/pubmed/8663313"
      minerva_ref_type__resource1 "HGNC__4693"
      minerva_ref_type__resource10 "REFSEQ__NM_000858"
      minerva_ref_type__resource11 "EC__2.7.4.8"
      minerva_ref_type__resource2 "UNIPROT__Q16774"
      minerva_ref_type__resource3 "UNIPROT__Q16774"
      minerva_ref_type__resource4 "ENTREZ__2987"
      minerva_ref_type__resource5 "ENTREZ__2987"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000143774"
      minerva_ref_type__resource7 "HGNC_SYMBOL__GUK1"
      minerva_ref_type__resource8 "HGNC_SYMBOL__GUK1"
      minerva_ref_type__resource9 "PUBMED__8663313"
      minerva_type "Protein"
      minerva_x 4947.0
      minerva_y 1889.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa226"
      species_meta_id "s_id_sa226"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 59
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nme5__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa235"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa381"
      minerva_elementId2 "sa235"
      minerva_name "NME5"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7853"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME5"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME5"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/8382"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/8382"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_003551"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000112981"
      minerva_ref_link8 "https://purl.uniprot.org/uniprot/P56597"
      minerva_ref_link9 "https://purl.uniprot.org/uniprot/P56597"
      minerva_ref_type__resource1 "HGNC__7853"
      minerva_ref_type__resource2 "HGNC_SYMBOL__NME5"
      minerva_ref_type__resource3 "HGNC_SYMBOL__NME5"
      minerva_ref_type__resource4 "ENTREZ__8382"
      minerva_ref_type__resource5 "ENTREZ__8382"
      minerva_ref_type__resource6 "REFSEQ__NM_003551"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000112981"
      minerva_ref_type__resource8 "UNIPROT__P56597"
      minerva_ref_type__resource9 "UNIPROT__P56597"
      minerva_type "Protein"
      minerva_x 2997.875
      minerva_x2 4959.0
      minerva_y 1212.625
      minerva_y2 2098.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa235"
      species_meta_id "s_id_sa235"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 60
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "glycine__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa162"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa162"
      minerva_fullName "glycine"
      minerva_name "Glycine"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/750"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15428"
      minerva_ref_type__resource1 "PUBCHEM__750"
      minerva_ref_type__resource2 "CHEBI__CHEBI:15428"
      minerva_synonyms "Aminoacetic acid; Aminoessigsaeure; G; GLYCINE; Gly; Glycin; Glycine; Glycocoll; Glykokoll; Glyzin; H2N-CH2-COOH; Hgly; Leimzucker; aminoethanoic acid; aminoethanoic acid"
      minerva_type "Simple molecule"
      minerva_x 1196.0
      minerva_y 828.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa162"
      species_meta_id "s_id_sa162"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 61
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nadph__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa295"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa295"
      minerva_elementId2 "sa30"
      minerva_fullName "NADPH"
      minerva_name "NADPH"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16474"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/compound/5884"
      minerva_ref_type__resource1 "CHEBI__CHEBI:16474"
      minerva_ref_type__resource2 "PUBCHEM__5884"
      minerva_synonyms "NADPH; NADPH DIHYDRO-NICOTINAMIDE-ADENINE-DINUCLEOTIDE PHOSPHATE; Reduced nicotinamide adenine dinucleotide phosphate; TPNH; dihydronicotinamide-adenine dinucleotide phosphate; reduced nicotinamide-adenine dinucleotide phosphate"
      minerva_type "Simple molecule"
      minerva_x 5760.25
      minerva_x2 2171.0
      minerva_y 1639.3846153846152
      minerva_y2 1902.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa295"
      species_meta_id "s_id_sa295"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 62
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 800.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "_alpha_d_minus_ribose_space_1p__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa145"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa145"
      minerva_fullName "alpha-D-ribose 1-phosphate"
      minerva_name "αD-Ribose 1P"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/439236"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16300"
      minerva_ref_type__resource1 "PUBCHEM__439236"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16300"
      minerva_synonyms "1-O-phosphono-alpha-D-ribofuranose; 1-phospho-alpha-D-ribofuranose; D-Ribose 1-phosphate; Ribose 1-phosphate; alpha-D-Ribose 1-phosphate; alpha-D-ribose 1-phosphate"
      minerva_type "Simple molecule"
      minerva_x 643.0
      minerva_y 755.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa145"
      species_meta_id "s_id_sa145"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 63
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "gmp__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa215"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa215"
      minerva_fullName "guanosine 5'-monophosphate"
      minerva_name "GMP"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/135398631"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17345"
      minerva_ref_type__resource1 "PUBCHEM__135398631"
      minerva_ref_type__resource2 "CHEBI__CHEBI:17345"
      minerva_synonyms "5'-GMP; GMP; Guanosine 5'-monophosphate; Guanosine 5'-phosphate; Guanosine monophosphate; Guanylic acid; guanosine-5'-monophosphate; pG"
      minerva_type "Simple molecule"
      minerva_x 5292.5
      minerva_y 1804.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa215"
      species_meta_id "s_id_sa215"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 64
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1000.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "thioredoxin_space_disulfide__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa241"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa374"
      minerva_elementId2 "sa241"
      minerva_fullName "thioredoxin disulfide"
      minerva_name "Thioredoxin disulfide"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18191"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/substance/11533266"
      minerva_ref_link3 "https://pubchem.ncbi.nlm.nih.gov/substance/3636"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18191"
      minerva_ref_type__resource2 "PUBCHEM_SUBSTANCE__11533266"
      minerva_ref_type__resource3 "PUBCHEM_SUBSTANCE__3636"
      minerva_synonyms "Oxidized thioredoxin; Oxidized thioredoxin; Thioredoxin disulfide; Thioredoxin sulfide"
      minerva_type "Simple molecule"
      minerva_x 3500.625
      minerva_x2 4773.5
      minerva_y 1117.375
      minerva_y2 2038.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa241"
      species_meta_id "s_id_sa241"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 65
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cd38__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa144"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa144"
      minerva_elementId2 "sa121"
      minerva_name "CD38"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/16690024"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_001775"
      minerva_ref_link11 "https://purl.uniprot.org/uniprot/P28907"
      minerva_ref_link12 "https://purl.uniprot.org/uniprot/P28907"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/952"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/952"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.2.2.6"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CD38"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CD38"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000004468"
      minerva_ref_link8 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.4.99.20"
      minerva_ref_link9 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1667"
      minerva_ref_type__resource1 "PUBMED__16690024"
      minerva_ref_type__resource10 "REFSEQ__NM_001775"
      minerva_ref_type__resource11 "UNIPROT__P28907"
      minerva_ref_type__resource12 "UNIPROT__P28907"
      minerva_ref_type__resource2 "ENTREZ__952"
      minerva_ref_type__resource3 "ENTREZ__952"
      minerva_ref_type__resource4 "EC__3.2.2.6"
      minerva_ref_type__resource5 "HGNC_SYMBOL__CD38"
      minerva_ref_type__resource6 "HGNC_SYMBOL__CD38"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000004468"
      minerva_ref_type__resource8 "EC__2.4.99.20"
      minerva_ref_type__resource9 "HGNC__1667"
      minerva_type "Protein"
      minerva_x 823.0000000000002
      minerva_x2 1596.5
      minerva_y 1102.5882352941178
      minerva_y2 1454.1666666666667
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa144"
      species_meta_id "s_id_sa144"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 66
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1200.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "akr1b1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa28"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa28"
      minerva_former_symbols "ALDR1"
      minerva_name "AKR1B1"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AKR1B1"
      minerva_ref_link10 "https://purl.uniprot.org/uniprot/P15121"
      minerva_ref_link11 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=1.1.1.372"
      minerva_ref_link12 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=1.1.1.54"
      minerva_ref_link13 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=1.1.1.21"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AKR1B1"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/231"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/231"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/381"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000085662"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_001628"
      minerva_ref_link8 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=1.1.1.300"
      minerva_ref_link9 "https://purl.uniprot.org/uniprot/P15121"
      minerva_ref_type__resource1 "HGNC_SYMBOL__AKR1B1"
      minerva_ref_type__resource10 "UNIPROT__P15121"
      minerva_ref_type__resource11 "EC__1.1.1.372"
      minerva_ref_type__resource12 "EC__1.1.1.54"
      minerva_ref_type__resource13 "EC__1.1.1.21"
      minerva_ref_type__resource2 "HGNC_SYMBOL__AKR1B1"
      minerva_ref_type__resource3 "ENTREZ__231"
      minerva_ref_type__resource4 "ENTREZ__231"
      minerva_ref_type__resource5 "HGNC__381"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000085662"
      minerva_ref_type__resource7 "REFSEQ__NM_001628"
      minerva_ref_type__resource8 "EC__1.1.1.300"
      minerva_ref_type__resource9 "UNIPROT__P15121"
      minerva_type "Protein"
      minerva_x 2020.0
      minerva_y 2117.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa28"
      species_meta_id "s_id_sa28"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 67
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "lactose_space_synthetase__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa1"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa1"
      minerva_name "lactose synthetase"
      minerva_type "Complex"
      minerva_x 2794.5
      minerva_y 2609.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa1"
      species_meta_id "s_id_csa1"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 68
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 300.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp14__mitochondrion__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa120"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa120"
      minerva_name "Nsp14"
      minerva_ref_link1 "https://doi.org/10.1101/2020.03.22.002386"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/YP_009725309"
      minerva_ref_type__resource1 "DOI__10.1101/2020.03.22.002386"
      minerva_ref_type__resource2 "NCBI_PROTEIN__YP_009725309"
      minerva_type "Protein"
      minerva_x 2138.0
      minerva_y 1045.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrion"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa120"
      species_meta_id "s_id_sa120"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 69
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "adk__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa342"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa342"
      minerva_name "ADK"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/P55263"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ADK"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P55263"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/257"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_006721"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/132"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/132"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000156110"
      minerva_ref_link8 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.1.20"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ADK"
      minerva_ref_type__resource1 "UNIPROT__P55263"
      minerva_ref_type__resource10 "HGNC_SYMBOL__ADK"
      minerva_ref_type__resource2 "UNIPROT__P55263"
      minerva_ref_type__resource3 "HGNC__257"
      minerva_ref_type__resource4 "REFSEQ__NM_006721"
      minerva_ref_type__resource5 "ENTREZ__132"
      minerva_ref_type__resource6 "ENTREZ__132"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000156110"
      minerva_ref_type__resource8 "EC__2.7.1.20"
      minerva_ref_type__resource9 "HGNC_SYMBOL__ADK"
      minerva_type "Protein"
      minerva_x 4121.0
      minerva_y 993.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa342"
      species_meta_id "s_id_sa342"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 70
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "2_minus_(formamido)_minus_n1_minus_(5'_minus_phosphoribosyl)acetamidine__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa170"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa170"
      minerva_fullName "2-formamido-N(1)-(5-phospho-D-ribosyl)acetamidine"
      minerva_name "2-(Formamido)-N1-(5'-phosphoribosyl)acetamidine"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18413"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/compound/5462266"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18413"
      minerva_ref_type__resource2 "PUBCHEM__5462266"
      minerva_synonyms "1-(5'-Phosphoribosyl)-N-formylglycinamidine; 2-(Formamido)-N1-(5'-phosphoribosyl)acetamidine; 2-(Formamido)-N1-(5-phospho-D-ribosyl)acetamidine; 5'-Phosphoribosyl-N-formylglycinamidine; 5'-Phosphoribosylformylglycinamidine"
      minerva_type "Simple molecule"
      minerva_x 2663.5
      minerva_y 752.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa170"
      species_meta_id "s_id_sa170"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 71
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sucrose__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa51"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa51"
      minerva_fullName "sucrose"
      minerva_name "Sucrose"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17992"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/compound/5988"
      minerva_ref_type__resource1 "CHEBI__CHEBI:17992"
      minerva_ref_type__resource2 "PUBCHEM__5988"
      minerva_synonyms "1-alpha-D-Glucopyranosyl-2-beta-D-fructofuranoside; Cane sugar; SUCROSE; Saccharose; Sacharose; Sucrose; White sugar; beta-D-Fruf-(2<->1)-alpha-D-Glcp; sacarosa; sucrose; table sugar"
      minerva_type "Simple molecule"
      minerva_x 2074.0
      minerva_y 2294.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa51"
      species_meta_id "s_id_sa51"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 72
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tetrahydrofolate__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa200"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa200"
      minerva_elementId2 "sa168"
      minerva_fullName "tetrahydrofolate"
      minerva_name "Tetrahydrofolate"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/135444742"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:67016"
      minerva_ref_type__resource1 "PUBCHEM__135444742"
      minerva_ref_type__resource2 "CHEBI__CHEBI:67016"
      minerva_synonyms "tetrahydrofolate anion; tetrahydrofolate anions; tetrahydrofolates"
      minerva_type "Simple molecule"
      minerva_x 5191.161764705883
      minerva_x2 1972.0
      minerva_y 693.8529411764707
      minerva_y2 683.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa200"
      species_meta_id "s_id_sa200"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 73
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 800.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "entpd5__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa291"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa291"
      minerva_former_symbols "CD39L4; PCPH"
      minerva_name "ENTPD5"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/O75356"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.6.1.42"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/protein/NM_001249"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/O75356"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000187097"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/957"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/957"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/3367"
      minerva_ref_link7 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.6.1.6"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ENTPD5"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ENTPD5"
      minerva_ref_type__resource1 "UNIPROT__O75356"
      minerva_ref_type__resource10 "EC__3.6.1.42"
      minerva_ref_type__resource11 "REFSEQ__NM_001249"
      minerva_ref_type__resource2 "UNIPROT__O75356"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000187097"
      minerva_ref_type__resource4 "ENTREZ__957"
      minerva_ref_type__resource5 "ENTREZ__957"
      minerva_ref_type__resource6 "HGNC__3367"
      minerva_ref_type__resource7 "EC__3.6.1.6"
      minerva_ref_type__resource8 "HGNC_SYMBOL__ENTPD5"
      minerva_ref_type__resource9 "HGNC_SYMBOL__ENTPD5"
      minerva_type "Protein"
      minerva_x 5569.0
      minerva_y 1871.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa291"
      species_meta_id "s_id_sa291"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 74
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "galm__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa3"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa3"
      minerva_name "GALM"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_138801"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=GALM"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/24063"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=5.1.3.3"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/Q96C23"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/Q96C23"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000143891"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/130589"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/130589"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=GALM"
      minerva_ref_type__resource1 "REFSEQ__NM_138801"
      minerva_ref_type__resource10 "HGNC_SYMBOL__GALM"
      minerva_ref_type__resource2 "HGNC__24063"
      minerva_ref_type__resource3 "EC__5.1.3.3"
      minerva_ref_type__resource4 "UNIPROT__Q96C23"
      minerva_ref_type__resource5 "UNIPROT__Q96C23"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000143891"
      minerva_ref_type__resource7 "ENTREZ__130589"
      minerva_ref_type__resource8 "ENTREZ__130589"
      minerva_ref_type__resource9 "HGNC_SYMBOL__GALM"
      minerva_type "Protein"
      minerva_x 2288.6875
      minerva_y 2287.75
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa3"
      species_meta_id "s_id_sa3"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 75
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1000.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "xmp__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa204"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa204"
      minerva_fullName "5'-xanthylic acid"
      minerva_name "XMP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15652"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/compound/73323"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15652"
      minerva_ref_type__resource2 "PUBCHEM__73323"
      minerva_synonyms "(9-D-Ribosylxanthine)-5'-phosphate; (9-D-ribosylxanthine)-5'-phosphate; XMP; Xanthosine 5'-phosphate; Xanthylic acid; xanthosine monophosphate"
      minerva_type "Simple molecule"
      minerva_x 5293.0
      minerva_y 1443.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa204"
      species_meta_id "s_id_sa204"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 76
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "_alpha__minus_d_minus_glucose__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa21"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa21"
      minerva_elementId2 "sa26"
      minerva_fullName "amylose"
      minerva_name "α-D-Glucose"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/79025"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:28102"
      minerva_ref_type__resource1 "PUBCHEM__79025"
      minerva_ref_type__resource2 "CHEBI__CHEBI:28102"
      minerva_synonyms "(1,4-alpha-D-Glucosyl)n; (1,4-alpha-D-Glucosyl)n+1; (1,4-alpha-D-Glucosyl)n-1; 1,4-alpha-D-Glucan; 4-{(1,4)-alpha-D-Glucosyl}(n-1)-D-glucose; Amylose; Amylose chain"
      minerva_type "Simple molecule"
      minerva_x 3032.0
      minerva_x2 2093.5
      minerva_y 2511.0
      minerva_y2 2511.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa21"
      species_meta_id "s_id_sa21"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 77
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1200.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "itpa__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa264"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa264"
      minerva_former_symbols "C20orf37"
      minerva_name "ITPA"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3704"
      minerva_ref_link10 "https://purl.uniprot.org/uniprot/Q9BY32"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/3704"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_033453"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.6.1.9"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000125877"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6176"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ITPA"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ITPA"
      minerva_ref_link9 "https://purl.uniprot.org/uniprot/Q9BY32"
      minerva_ref_type__resource1 "ENTREZ__3704"
      minerva_ref_type__resource10 "UNIPROT__Q9BY32"
      minerva_ref_type__resource2 "ENTREZ__3704"
      minerva_ref_type__resource3 "REFSEQ__NM_033453"
      minerva_ref_type__resource4 "EC__3.6.1.9"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000125877"
      minerva_ref_type__resource6 "HGNC__6176"
      minerva_ref_type__resource7 "HGNC_SYMBOL__ITPA"
      minerva_ref_type__resource8 "HGNC_SYMBOL__ITPA"
      minerva_ref_type__resource9 "UNIPROT__Q9BY32"
      minerva_type "Protein"
      minerva_x 4316.0
      minerva_y 2013.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa264"
      species_meta_id "s_id_sa264"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 78
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cant1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa293"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa293"
      minerva_name "CANT1"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/19721"
      minerva_ref_link10 "https://purl.uniprot.org/uniprot/Q8WVQ1"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CANT1"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CANT1"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/124583"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/124583"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.6.1.6"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000171302"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_138793"
      minerva_ref_link9 "https://purl.uniprot.org/uniprot/Q8WVQ1"
      minerva_ref_type__resource1 "HGNC__19721"
      minerva_ref_type__resource10 "UNIPROT__Q8WVQ1"
      minerva_ref_type__resource2 "HGNC_SYMBOL__CANT1"
      minerva_ref_type__resource3 "HGNC_SYMBOL__CANT1"
      minerva_ref_type__resource4 "ENTREZ__124583"
      minerva_ref_type__resource5 "ENTREZ__124583"
      minerva_ref_type__resource6 "EC__3.6.1.6"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000171302"
      minerva_ref_type__resource8 "REFSEQ__NM_138793"
      minerva_ref_type__resource9 "UNIPROT__Q8WVQ1"
      minerva_type "Protein"
      minerva_x 5568.0
      minerva_y 1957.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa293"
      species_meta_id "s_id_sa293"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 79
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "gdp__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa225"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa225"
      minerva_fullName "GDP"
      minerva_name "GDP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17552"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/compound/135398619"
      minerva_ref_type__resource1 "CHEBI__CHEBI:17552"
      minerva_ref_type__resource2 "PUBCHEM__135398619"
      minerva_synonyms "GDP; Guanosine 5'-diphosphate; Guanosine diphosphate"
      minerva_type "Simple molecule"
      minerva_x 5294.0
      minerva_y 2002.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa225"
      species_meta_id "s_id_sa225"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 80
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "enpp3__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa261"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa261"
      minerva_elementId2 "sa95"
      minerva_former_symbols "PDNP3"
      minerva_name "ENPP3"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.1.4.1"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ENPP3"
      minerva_ref_link11 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ENPP3"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/3358"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000154269"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.6.1.9"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_005021"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/5169"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/O14638"
      minerva_ref_link8 "https://purl.uniprot.org/uniprot/O14638"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/5169"
      minerva_ref_type__resource1 "EC__3.1.4.1"
      minerva_ref_type__resource10 "HGNC_SYMBOL__ENPP3"
      minerva_ref_type__resource11 "HGNC_SYMBOL__ENPP3"
      minerva_ref_type__resource2 "HGNC__3358"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000154269"
      minerva_ref_type__resource4 "EC__3.6.1.9"
      minerva_ref_type__resource5 "REFSEQ__NM_005021"
      minerva_ref_type__resource6 "ENTREZ__5169"
      minerva_ref_type__resource7 "UNIPROT__O14638"
      minerva_ref_type__resource8 "UNIPROT__O14638"
      minerva_ref_type__resource9 "ENTREZ__5169"
      minerva_type "Protein"
      minerva_x 4401.0
      minerva_x2 1375.0
      minerva_y 2037.5
      minerva_y2 1748.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa261"
      species_meta_id "s_id_sa261"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 81
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ak5__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa370"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa389"
      minerva_elementId2 "sa370"
      minerva_name "AK5"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AK5"
      minerva_ref_link10 "https://purl.uniprot.org/uniprot/Q9Y6K8"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/protein/NM_174858"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AK5"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/26289"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/26289"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000154027"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link7 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.3"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/365"
      minerva_ref_link9 "https://purl.uniprot.org/uniprot/Q9Y6K8"
      minerva_ref_type__resource1 "HGNC_SYMBOL__AK5"
      minerva_ref_type__resource10 "UNIPROT__Q9Y6K8"
      minerva_ref_type__resource11 "REFSEQ__NM_174858"
      minerva_ref_type__resource2 "HGNC_SYMBOL__AK5"
      minerva_ref_type__resource3 "ENTREZ__26289"
      minerva_ref_type__resource4 "ENTREZ__26289"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000154027"
      minerva_ref_type__resource6 "EC__2.7.4.6"
      minerva_ref_type__resource7 "EC__2.7.4.3"
      minerva_ref_type__resource8 "HGNC__365"
      minerva_ref_type__resource9 "UNIPROT__Q9Y6K8"
      minerva_type "Protein"
      minerva_x 3758.875
      minerva_x2 3458.0
      minerva_y 962.375
      minerva_y2 1288.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa370"
      species_meta_id "s_id_sa370"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 82
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nmnat2__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa91"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa91"
      minerva_former_symbols "C1orf15"
      minerva_name "NMNAT2"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/Q9BZQ4"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NMNAT2"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/pubmed/12359228"
      minerva_ref_link12 "https://www.ensembl.org/id/ENSG00000157064"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/Q9BZQ4"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/23057"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/23057"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_015039"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.7.18"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/16789"
      minerva_ref_link8 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.7.1"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NMNAT2"
      minerva_ref_type__resource1 "UNIPROT__Q9BZQ4"
      minerva_ref_type__resource10 "HGNC_SYMBOL__NMNAT2"
      minerva_ref_type__resource11 "PUBMED__12359228"
      minerva_ref_type__resource12 "ENSEMBL__ENSG00000157064"
      minerva_ref_type__resource2 "UNIPROT__Q9BZQ4"
      minerva_ref_type__resource3 "ENTREZ__23057"
      minerva_ref_type__resource4 "ENTREZ__23057"
      minerva_ref_type__resource5 "REFSEQ__NM_015039"
      minerva_ref_type__resource6 "EC__2.7.7.18"
      minerva_ref_type__resource7 "HGNC__16789"
      minerva_ref_type__resource8 "EC__2.7.7.1"
      minerva_ref_type__resource9 "HGNC_SYMBOL__NMNAT2"
      minerva_type "Protein"
      minerva_x 902.0
      minerva_y 1739.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa91"
      species_meta_id "s_id_sa91"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 83
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "deoxyguanosine__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa251"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa251"
      minerva_fullName "2'-deoxyguanosine"
      minerva_name "Deoxyguanosine"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17172"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/compound/135398592"
      minerva_ref_type__resource1 "CHEBI__CHEBI:17172"
      minerva_ref_type__resource2 "PUBCHEM__135398592"
      minerva_synonyms "2'-Deoxyguanosine; 2'-deoxyguanosine; 2'-deoxyguanosine; 2-amino-9-(2-deoxy-9-beta-D-ribofuranosyl)-9H-purin-6-ol; 9-(2-deoxy-beta-D-erythro-pentofuranosyl)-guanine; Deoxyguanosine; dG; guanine deoxy nucleoside"
      minerva_type "Simple molecule"
      minerva_x 4173.0
      minerva_y 1783.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa251"
      species_meta_id "s_id_sa251"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 84
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 800.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "glb1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa24"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa24"
      minerva_former_symbols "ELNR1"
      minerva_name "GLB1"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/4298"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/2720"
      minerva_ref_link2 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.2.1.23"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=GLB1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=GLB1"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_000404"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000170266"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/P16278"
      minerva_ref_link8 "https://purl.uniprot.org/uniprot/P16278"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/2720"
      minerva_ref_type__resource1 "HGNC__4298"
      minerva_ref_type__resource10 "ENTREZ__2720"
      minerva_ref_type__resource2 "EC__3.2.1.23"
      minerva_ref_type__resource3 "HGNC_SYMBOL__GLB1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__GLB1"
      minerva_ref_type__resource5 "REFSEQ__NM_000404"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000170266"
      minerva_ref_type__resource7 "UNIPROT__P16278"
      minerva_ref_type__resource8 "UNIPROT__P16278"
      minerva_ref_type__resource9 "ENTREZ__2720"
      minerva_type "Protein"
      minerva_x 2373.0
      minerva_y 2455.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa24"
      species_meta_id "s_id_sa24"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 85
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "entpd6__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa292"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa292"
      minerva_former_symbols "CD39L2; IL6ST2"
      minerva_name "ENTPD6"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/955"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000197586"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/955"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/O75354"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O75354"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ENTPD6"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ENTPD6"
      minerva_ref_link7 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.6.1.6"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/3368"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_001114089"
      minerva_ref_type__resource1 "ENTREZ__955"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000197586"
      minerva_ref_type__resource2 "ENTREZ__955"
      minerva_ref_type__resource3 "UNIPROT__O75354"
      minerva_ref_type__resource4 "UNIPROT__O75354"
      minerva_ref_type__resource5 "HGNC_SYMBOL__ENTPD6"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ENTPD6"
      minerva_ref_type__resource7 "EC__3.6.1.6"
      minerva_ref_type__resource8 "HGNC__3368"
      minerva_ref_type__resource9 "REFSEQ__NM_001114089"
      minerva_type "Protein"
      minerva_x 5570.0
      minerva_y 1914.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa292"
      species_meta_id "s_id_sa292"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 86
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1000.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "1_minus_(5'_minus_phosphoribosyl)_minus_5_minus_amino_minus_4_minus_imidazolecarboxamide__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa195"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa195"
      minerva_fullName "AICA ribonucleotide"
      minerva_name "1-(5'-Phosphoribosyl)-5-amino-4-imidazolecarboxamide"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18406"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/compound/65110"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18406"
      minerva_ref_type__resource2 "PUBCHEM__65110"
      minerva_synonyms "1-(5'-Phosphoribosyl)-5-amino-4-imidazolecarboxamide; 1-(5'-phosphoribosyl)-5-amino-4-imidazolecarboxamide; 5'-Phospho-ribosyl-5-amino-4-imidazole carboxamide; 5'-Phosphoribosyl-5-amino-4-imidazolecarboxamide; 5'-phospho-ribosyl-5-amino-4-imidazole carboxamide; 5'-phosphoribosyl-5-amino-4-imidazolecarboxamide; 5-Amino-1-(5-phospho-D-ribosyl)imidazole-4-carboxamide; 5-Aminoimidazole-4-carboxamide ribotide; 5-Phosphoribosyl-4-carbamoyl-5-aminoimidazole; 5-amino-1-(5-phospho-D-ribosyl)imidazole-4-carboxamide; 5-amino-1-(5-phospho-D-ribosyl)imidazole-4-carboxamide; 5-aminoimidazole-4-carboxamide ribotide; 5-phosphoribosyl-4-carbamoyl-5-aminoimidazole; AICA-ribonucleotide; AICAR; Aica ribonucleotide; acadesine 5'-monophosphate"
      minerva_type "Simple molecule"
      minerva_x 4754.0
      minerva_y 753.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa195"
      species_meta_id "s_id_sa195"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 87
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "aprt__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa350"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa350"
      minerva_name "APRT"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_000485"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.4.2.7"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/626"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P07741"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P07741"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000198931"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/353"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/353"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=APRT"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=APRT"
      minerva_ref_type__resource1 "REFSEQ__NM_000485"
      minerva_ref_type__resource10 "EC__2.4.2.7"
      minerva_ref_type__resource2 "HGNC__626"
      minerva_ref_type__resource3 "UNIPROT__P07741"
      minerva_ref_type__resource4 "UNIPROT__P07741"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000198931"
      minerva_ref_type__resource6 "ENTREZ__353"
      minerva_ref_type__resource7 "ENTREZ__353"
      minerva_ref_type__resource8 "HGNC_SYMBOL__APRT"
      minerva_ref_type__resource9 "HGNC_SYMBOL__APRT"
      minerva_type "Protein"
      minerva_x 3866.0
      minerva_y 1072.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa350"
      species_meta_id "s_id_sa350"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 88
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1200.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "naprt1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa138"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa138"
      minerva_name "NAPRT1"
      minerva_type "Protein"
      minerva_x 462.0
      minerva_y 1351.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa138"
      species_meta_id "s_id_sa138"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 89
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nme7__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa383"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa383"
      minerva_elementId2 "sa237"
      minerva_name "NME7"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/29922"
      minerva_ref_link10 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/20461"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/29922"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000143156"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/Q9Y5B8"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/Q9Y5B8"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_013330"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME7"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME7"
      minerva_ref_link9 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_type__resource1 "ENTREZ__29922"
      minerva_ref_type__resource10 "HGNC__20461"
      minerva_ref_type__resource2 "ENTREZ__29922"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000143156"
      minerva_ref_type__resource4 "UNIPROT__Q9Y5B8"
      minerva_ref_type__resource5 "UNIPROT__Q9Y5B8"
      minerva_ref_type__resource6 "REFSEQ__NM_013330"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME7"
      minerva_ref_type__resource8 "HGNC_SYMBOL__NME7"
      minerva_ref_type__resource9 "EC__2.7.4.6"
      minerva_type "Protein"
      minerva_x 3156.875
      minerva_x2 4959.0
      minerva_y 1212.625
      minerva_y2 2163.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa383"
      species_meta_id "s_id_sa383"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 90
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nme3__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa232"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa232"
      minerva_elementId2 "sa380"
      minerva_name "NME3"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000103024"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/4832"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7851"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME3"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME3"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/Q13232"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/Q13232"
      minerva_ref_link7 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_002513"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4832"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000103024"
      minerva_ref_type__resource10 "ENTREZ__4832"
      minerva_ref_type__resource2 "HGNC__7851"
      minerva_ref_type__resource3 "HGNC_SYMBOL__NME3"
      minerva_ref_type__resource4 "HGNC_SYMBOL__NME3"
      minerva_ref_type__resource5 "UNIPROT__Q13232"
      minerva_ref_type__resource6 "UNIPROT__Q13232"
      minerva_ref_type__resource7 "EC__2.7.4.6"
      minerva_ref_type__resource8 "REFSEQ__NM_002513"
      minerva_ref_type__resource9 "ENTREZ__4832"
      minerva_type "Protein"
      minerva_x 4957.0
      minerva_x2 2924.875
      minerva_y 2065.5
      minerva_y2 1213.625
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa232"
      species_meta_id "s_id_sa232"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 91
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "paics__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa185"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa185"
      minerva_former_symbols "PAIS"
      minerva_name "PAICS"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/P22234"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=6.3.2.6"
      minerva_ref_link11 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=4.1.1.21"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P22234"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/8587"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/10606"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/10606"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_006452"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000128050"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PAICS"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PAICS"
      minerva_ref_type__resource1 "UNIPROT__P22234"
      minerva_ref_type__resource10 "EC__6.3.2.6"
      minerva_ref_type__resource11 "EC__4.1.1.21"
      minerva_ref_type__resource2 "UNIPROT__P22234"
      minerva_ref_type__resource3 "HGNC__8587"
      minerva_ref_type__resource4 "ENTREZ__10606"
      minerva_ref_type__resource5 "ENTREZ__10606"
      minerva_ref_type__resource6 "REFSEQ__NM_006452"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000128050"
      minerva_ref_type__resource8 "HGNC_SYMBOL__PAICS"
      minerva_ref_type__resource9 "HGNC_SYMBOL__PAICS"
      minerva_type "Protein"
      minerva_x 3694.0
      minerva_y 627.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa185"
      species_meta_id "s_id_sa185"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 92
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "hypoxanthine__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa317"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa317"
      minerva_fullName "hypoxanthine"
      minerva_name "Hypoxanthine"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/135398638"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17368"
      minerva_ref_type__resource1 "PUBCHEM__135398638"
      minerva_ref_type__resource2 "CHEBI__CHEBI:17368"
      minerva_synonyms "6(1H)-purinone; 6-oxopurine; 9H-purin-6(1H)-one; HYPOXANTHINE; Hyp; Hypoxanthine; Purine-6-ol; hypoxanthine; purin-6(1H)-one"
      minerva_type "Simple molecule"
      minerva_x 4453.0
      minerva_y 1187.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa317"
      species_meta_id "s_id_sa317"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 93
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "dgmp__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa248"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa248"
      minerva_fullName "2'-deoxyguanosine 5'-monophosphate"
      minerva_name "dGMP"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/135398597"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16192"
      minerva_ref_type__resource1 "PUBCHEM__135398597"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16192"
      minerva_synonyms "2'-Deoxy-GMP; 2'-Deoxyguanosine 5'-(dihydrogen phosphate); 2'-Deoxyguanosine 5'-monophosphate; 2'-Deoxyguanosine 5'-phosphate; 2'-Deoxyguanylic acid; 2'-dGMP; 2'-deoxyguanosine 5'-monophosphate; Deoxy-GMP; Deoxyguanosine 5'-monophosphate; Deoxyguanosine 5'-phosphate; Deoxyguanosine monophosphate; Deoxyguanylic acid; dGMP"
      minerva_type "Simple molecule"
      minerva_x 4640.5
      minerva_y 1783.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa248"
      species_meta_id "s_id_sa248"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 94
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "quinolinate__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa131"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa131"
      minerva_fullName "quinolinic acid"
      minerva_name "Quinolinate"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/1066"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16675"
      minerva_ref_type__resource1 "PUBCHEM__1066"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16675"
      minerva_synonyms "2,3-Pyridinedicarboxylic acid; Pyridine-2,3-dicarboxylate; QUINOLINIC ACID; Quinolinic acid"
      minerva_type "Simple molecule"
      minerva_x 716.5
      minerva_y 1321.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa131"
      species_meta_id "s_id_sa131"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 95
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 800.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ribonucleoside_space_reductase__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa3"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa3"
      minerva_elementId2 "csa4"
      minerva_name "ribonucleoside reductase"
      minerva_type "Complex"
      minerva_x 4954.0
      minerva_x2 3552.125
      minerva_y 1946.0
      minerva_y2 1045.375
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa3"
      species_meta_id "s_id_csa3"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 96
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "dgtp__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa245"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa245"
      minerva_fullName "dGTP"
      minerva_name "dGTP"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/135398599"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16497"
      minerva_ref_type__resource1 "PUBCHEM__135398599"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16497"
      minerva_synonyms "2'-Deoxyguanosine 5'-triphosphate; 2'-deoxyguanosine 5'-triphosphate; Deoxyguanosine 5'-triphosphate; Deoxyguanosine triphosphate; dGTP"
      minerva_type "Simple molecule"
      minerva_x 4639.5
      minerva_y 2236.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa245"
      species_meta_id "s_id_sa245"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 97
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1000.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "migalastat__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa58"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa58"
      minerva_name "Migalastat"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/176077"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/pubmed/10866822"
      minerva_ref_link3 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:135923"
      minerva_ref_type__resource1 "PUBCHEM__176077"
      minerva_ref_type__resource2 "PUBMED__10866822"
      minerva_ref_type__resource3 "CHEBI__CHEBI:135923"
      minerva_type "Drug"
      minerva_x 1621.0
      minerva_y 2455.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa58"
      species_meta_id "s_id_sa58"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 98
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ntpcr__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa283"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa283"
      minerva_former_symbols "C1orf57"
      minerva_name "NTPCR"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/28204"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/84284"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_032324"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.6.1.15"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/Q9BSD7"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/Q9BSD7"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000135778"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NTPCR"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NTPCR"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/84284"
      minerva_ref_type__resource1 "HGNC__28204"
      minerva_ref_type__resource10 "ENTREZ__84284"
      minerva_ref_type__resource2 "REFSEQ__NM_032324"
      minerva_ref_type__resource3 "EC__3.6.1.15"
      minerva_ref_type__resource4 "UNIPROT__Q9BSD7"
      minerva_ref_type__resource5 "UNIPROT__Q9BSD7"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000135778"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NTPCR"
      minerva_ref_type__resource8 "HGNC_SYMBOL__NTPCR"
      minerva_ref_type__resource9 "ENTREZ__84284"
      minerva_type "Protein"
      minerva_x 5535.0
      minerva_y 2097.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa283"
      species_meta_id "s_id_sa283"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 99
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1200.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "d_minus_ribose_space_5p__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa146"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa146"
      minerva_fullName "D-ribose 5-phosphate"
      minerva_name "D-Ribose 5P"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:78679"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/compound/439167"
      minerva_ref_type__resource1 "CHEBI__CHEBI:78679"
      minerva_ref_type__resource2 "PUBCHEM__439167"
      minerva_type "Simple molecule"
      minerva_x 905.5
      minerva_y 755.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa146"
      species_meta_id "s_id_sa146"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 100
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "impdh1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa210"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa210"
      minerva_former_symbols "RP10"
      minerva_name "IMPDH1"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000106348"
      minerva_ref_link10 "https://purl.uniprot.org/uniprot/P20839"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/3614"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_000883"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/3614"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IMPDH1"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IMPDH1"
      minerva_ref_link7 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=1.1.1.205"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6052"
      minerva_ref_link9 "https://purl.uniprot.org/uniprot/P20839"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000106348"
      minerva_ref_type__resource10 "UNIPROT__P20839"
      minerva_ref_type__resource2 "ENTREZ__3614"
      minerva_ref_type__resource3 "REFSEQ__NM_000883"
      minerva_ref_type__resource4 "ENTREZ__3614"
      minerva_ref_type__resource5 "HGNC_SYMBOL__IMPDH1"
      minerva_ref_type__resource6 "HGNC_SYMBOL__IMPDH1"
      minerva_ref_type__resource7 "EC__1.1.1.205"
      minerva_ref_type__resource8 "HGNC__6052"
      minerva_ref_type__resource9 "UNIPROT__P20839"
      minerva_type "Protein"
      minerva_x 5229.0
      minerva_y 1278.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa210"
      species_meta_id "s_id_sa210"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 101
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "udp_minus__alpha__minus_d_minus_galactose__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa8"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa8"
      minerva_fullName "UDP-alpha-D-galactose(2-)"
      minerva_name "UDP-α-D-Galactose"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:66914"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/compound/18068"
      minerva_ref_type__resource1 "CHEBI__CHEBI:66914"
      minerva_ref_type__resource2 "PUBCHEM__18068"
      minerva_synonyms "UDP-alpha-D-galactose"
      minerva_type "Simple molecule"
      minerva_x 2906.0
      minerva_y 2343.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa8"
      species_meta_id "s_id_sa8"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 102
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "utp__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa13"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa13"
      minerva_fullName "UTP"
      minerva_name "UTP"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6133"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15713"
      minerva_ref_type__resource1 "PUBCHEM__6133"
      minerva_ref_type__resource2 "CHEBI__CHEBI:15713"
      minerva_synonyms "5'-UTP; H4utp; UTP; Uridine 5'-triphosphate; Uridine triphosphate; uridine 5'-triphosphoric acid"
      minerva_type "Simple molecule"
      minerva_x 2865.0
      minerva_y 2171.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa13"
      species_meta_id "s_id_sa13"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 103
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "udp__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa22"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa22"
      minerva_fullName "UDP"
      minerva_name "UDP"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6031"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17659"
      minerva_ref_type__resource1 "PUBCHEM__6031"
      minerva_ref_type__resource2 "CHEBI__CHEBI:17659"
      minerva_synonyms "UDP; Uridine 5'-diphosphate; Uridine diphosphate"
      minerva_type "Simple molecule"
      minerva_x 2725.0
      minerva_y 2455.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa22"
      species_meta_id "s_id_sa22"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 104
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "1_minus_(5'_minus_phosphoribosyl)_minus_5_minus_formamido_minus_4_minus_imidazolecarboxamide__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa198"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa198"
      minerva_fullName "5-formamido-1-(5-phospho-D-ribosyl)imidazole-4-carboxamide"
      minerva_name "1-(5'-Phosphoribosyl)-5-formamido-4-imidazolecarboxamide"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18381"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/compound/166760"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18381"
      minerva_ref_type__resource2 "PUBCHEM__166760"
      minerva_synonyms "1-(5'-Phosphoribosyl)-5-formamido-4-imidazolecarboxamide; 1-(5'-phosphoribosyl)-5-formamido-4-imidazolecarboxamide; 5'-Phosphoribosyl-5-formamido-4-imidazolecarboxamide; 5'-phosphoribosyl-5-formamido-4-imidazolecarboxamide; 5-Formamido-1-(5-phospho-D-ribosyl)imidazole-4-carboxamide; 5-Formamido-1-(5-phosphoribosyl)imidazole-4-carboxamide; 5-formamido-1-(5-phospho-D-ribosyl)imidazole-4-carboxamide; 5-formamido-1-(5-phosphoribosyl)imidazole-4-carboxamide; FAICAR"
      minerva_type "Simple molecule"
      minerva_x 5298.0
      minerva_y 752.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa198"
      species_meta_id "s_id_sa198"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 105
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "hprt1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa332"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa267"
      minerva_elementId2 "sa332"
      minerva_former_symbols "HPRT"
      minerva_name "HPRT1"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/5157"
      minerva_ref_link10 "https://purl.uniprot.org/uniprot/P00492"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=HPRT1"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=HPRT1"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.4.2.8"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_000194"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/3251"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/3251"
      minerva_ref_link8 "https://www.ensembl.org/id/ENSG00000165704"
      minerva_ref_link9 "https://purl.uniprot.org/uniprot/P00492"
      minerva_ref_type__resource1 "HGNC__5157"
      minerva_ref_type__resource10 "UNIPROT__P00492"
      minerva_ref_type__resource2 "HGNC_SYMBOL__HPRT1"
      minerva_ref_type__resource3 "HGNC_SYMBOL__HPRT1"
      minerva_ref_type__resource4 "EC__2.4.2.8"
      minerva_ref_type__resource5 "REFSEQ__NM_000194"
      minerva_ref_type__resource6 "ENTREZ__3251"
      minerva_ref_type__resource7 "ENTREZ__3251"
      minerva_ref_type__resource8 "ENSEMBL__ENSG00000165704"
      minerva_ref_type__resource9 "UNIPROT__P00492"
      minerva_type "Protein"
      minerva_x 4971.0
      minerva_x2 4842.5
      minerva_y 1748.0
      minerva_y2 1103.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa332"
      species_meta_id "s_id_sa332"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 106
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 800.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nnt__mitochondrion__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa80"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa80"
      minerva_name "NNT"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7863"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.1"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/23530"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/23530"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000112992"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NNT"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_182977"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NNT"
      minerva_ref_link8 "https://purl.uniprot.org/uniprot/Q13423"
      minerva_ref_link9 "https://purl.uniprot.org/uniprot/Q13423"
      minerva_ref_type__resource1 "HGNC__7863"
      minerva_ref_type__resource10 "EC__7.1.1.1"
      minerva_ref_type__resource2 "ENTREZ__23530"
      minerva_ref_type__resource3 "ENTREZ__23530"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000112992"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NNT"
      minerva_ref_type__resource6 "REFSEQ__NM_182977"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NNT"
      minerva_ref_type__resource8 "UNIPROT__Q13423"
      minerva_ref_type__resource9 "UNIPROT__Q13423"
      minerva_type "Protein"
      minerva_x 1924.0
      minerva_y 1821.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrion"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa80"
      species_meta_id "s_id_sa80"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 107
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "amdp2__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa305"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa305"
      minerva_name "AMDP2"
      minerva_type "Protein"
      minerva_x 4707.0
      minerva_y 878.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa305"
      species_meta_id "s_id_sa305"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 108
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1000.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "gda__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa326"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa326"
      minerva_name "GDA"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_001242505"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.5.4.3"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/4212"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/Q9Y2T3"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/Q9Y2T3"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000119125"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=GDA"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/9615"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=GDA"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/9615"
      minerva_ref_type__resource1 "REFSEQ__NM_001242505"
      minerva_ref_type__resource10 "EC__3.5.4.3"
      minerva_ref_type__resource2 "HGNC__4212"
      minerva_ref_type__resource3 "UNIPROT__Q9Y2T3"
      minerva_ref_type__resource4 "UNIPROT__Q9Y2T3"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000119125"
      minerva_ref_type__resource6 "HGNC_SYMBOL__GDA"
      minerva_ref_type__resource7 "ENTREZ__9615"
      minerva_ref_type__resource8 "HGNC_SYMBOL__GDA"
      minerva_ref_type__resource9 "ENTREZ__9615"
      minerva_type "Protein"
      minerva_x 4267.0
      minerva_y 1524.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa326"
      species_meta_id "s_id_sa326"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 109
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "_alpha__minus_d_minus_galactose__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2"
      minerva_fullName "alpha-D-galactose"
      minerva_name "α-D-Galactose"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/439357"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:28061"
      minerva_ref_type__resource1 "PUBCHEM__439357"
      minerva_ref_type__resource2 "CHEBI__CHEBI:28061"
      minerva_synonyms "ALPHA D-GALACTOSE; Gal-alpha; alpha-D-Gal; alpha-D-Galactose; alpha-D-galactose"
      minerva_type "Simple molecule"
      minerva_x 2398.6875
      minerva_y 2342.75
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2"
      species_meta_id "s_id_sa2"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 110
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1200.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "histone_minus_l_minus_lysine__mitochondrion__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa116"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa116"
      minerva_fullName "Histone-L-lysine"
      minerva_name "Histone-L-lysine"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:5738"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/substance/5447"
      minerva_ref_link3 "https://pubchem.ncbi.nlm.nih.gov/substance/223439948"
      minerva_ref_type__resource1 "CHEBI__CHEBI:5738"
      minerva_ref_type__resource2 "PUBCHEM_SUBSTANCE__5447"
      minerva_ref_type__resource3 "PUBCHEM_SUBSTANCE__223439948"
      minerva_synonyms "Histone-L-lysine"
      minerva_type "Simple molecule"
      minerva_x 1866.5
      minerva_y 906.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrion"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa116"
      species_meta_id "s_id_sa116"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 111
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "adenine__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa346"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa346"
      minerva_fullName "adenine"
      minerva_name "Adenine"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/190"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16708"
      minerva_ref_type__resource1 "PUBCHEM__190"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16708"
      minerva_synonyms "6-Aminopurine; A; ADENINE; Ade; Adenin; Adenine; adenine"
      minerva_type "Simple molecule"
      minerva_x 4235.0
      minerva_y 1219.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa346"
      species_meta_id "s_id_sa346"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 112
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nicotinate_space_d_minus_ribonucleotide__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa72"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa72"
      minerva_fullName "nicotinic acid D-ribonucleotide"
      minerva_name "Nicotinate D-ribonucleotide"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15763"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/compound/121992"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15763"
      minerva_ref_type__resource2 "PUBCHEM__121992"
      minerva_synonyms "Nicotinate D-ribonucleotide; Nicotinate ribonucleotide; Nicotinic acid ribonucleotide; beta-Nicotinate D-ribonucleotide"
      minerva_type "Simple molecule"
      minerva_x 550.0
      minerva_y 1579.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa72"
      species_meta_id "s_id_sa72"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 113
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "dgdp__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa238"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa238"
      minerva_fullName "dGDP"
      minerva_name "dGDP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:28862"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/compound/135398595"
      minerva_ref_type__resource1 "CHEBI__CHEBI:28862"
      minerva_ref_type__resource2 "PUBCHEM__135398595"
      minerva_synonyms "2'-Deoxyguanosine 5'-diphosphate; 2'-deoxyguanosine 5'-diphosphate; dGDP; deoxyguanosine diphosphate"
      minerva_type "Simple molecule"
      minerva_x 4640.5
      minerva_y 2001.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa238"
      species_meta_id "s_id_sa238"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 114
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ampd3__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa306"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa306"
      minerva_name "AMPD3"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AMPD3"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/272"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AMPD3"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_000480"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.5.4.6"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/470"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/Q01432"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/Q01432"
      minerva_ref_link8 "https://www.ensembl.org/id/ENSG00000133805"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/272"
      minerva_ref_type__resource1 "HGNC_SYMBOL__AMPD3"
      minerva_ref_type__resource10 "ENTREZ__272"
      minerva_ref_type__resource2 "HGNC_SYMBOL__AMPD3"
      minerva_ref_type__resource3 "REFSEQ__NM_000480"
      minerva_ref_type__resource4 "EC__3.5.4.6"
      minerva_ref_type__resource5 "HGNC__470"
      minerva_ref_type__resource6 "UNIPROT__Q01432"
      minerva_ref_type__resource7 "UNIPROT__Q01432"
      minerva_ref_type__resource8 "ENSEMBL__ENSG00000133805"
      minerva_ref_type__resource9 "ENTREZ__272"
      minerva_type "Protein"
      minerva_x 4663.0
      minerva_y 829.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa306"
      species_meta_id "s_id_sa306"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 115
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "pgm2__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa147"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa147"
      minerva_name "PGM2"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_018290"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PGM2"
      minerva_ref_link11 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PGM2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/Q96G03"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/Q96G03"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=5.4.2.7"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/8906"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000169299"
      minerva_ref_link7 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=5.4.2.2"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/55276"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/55276"
      minerva_ref_type__resource1 "REFSEQ__NM_018290"
      minerva_ref_type__resource10 "HGNC_SYMBOL__PGM2"
      minerva_ref_type__resource11 "HGNC_SYMBOL__PGM2"
      minerva_ref_type__resource2 "UNIPROT__Q96G03"
      minerva_ref_type__resource3 "UNIPROT__Q96G03"
      minerva_ref_type__resource4 "EC__5.4.2.7"
      minerva_ref_type__resource5 "HGNC__8906"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000169299"
      minerva_ref_type__resource7 "EC__5.4.2.2"
      minerva_ref_type__resource8 "ENTREZ__55276"
      minerva_ref_type__resource9 "ENTREZ__55276"
      minerva_type "Protein"
      minerva_x 773.0
      minerva_y 704.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa147"
      species_meta_id "s_id_sa147"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 116
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nrk1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa104"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa104"
      minerva_name "NRK1"
      minerva_type "Protein"
      minerva_x 1113.0
      minerva_y 1483.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa104"
      species_meta_id "s_id_sa104"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 117
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 800.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "10_minus_formyltetrahydrofolate__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa167"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa167"
      minerva_elementId2 "sa201"
      minerva_fullName "10-formyltetrahydrofolic acid"
      minerva_name "10-Formyltetrahydrofolate"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/135450591"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15637"
      minerva_ref_type__resource1 "PUBCHEM__135450591"
      minerva_ref_type__resource2 "CHEBI__CHEBI:15637"
      minerva_synonyms "(6S)-10-HCO-H4folate; 10-FTHF; 10-Formyl-THF; 10-Formyltetrahydrofolate; 10-formyltetrahydropteroylglutamic acid; N-(10-formyl-5,6,7,8-tetrahydropteroyl)-L-glutamic acid"
      minerva_type "Simple molecule"
      minerva_x 1646.5
      minerva_x2 4832.661764705883
      minerva_y 692.5
      minerva_y2 699.3529411764707
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa167"
      species_meta_id "s_id_sa167"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 118
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "udp_minus__alpha__minus_d_minus_glucose__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa10"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa10"
      minerva_fullName "UDP-alpha-D-glucose"
      minerva_name "UDP-α-D-Glucose"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/8629"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:46229"
      minerva_ref_type__resource1 "PUBCHEM__8629"
      minerva_ref_type__resource2 "CHEBI__CHEBI:46229"
      minerva_synonyms "GLUCOSE-URIDINE-C1,5'-DIPHOSPHATE; UDP-D-glucose; UDP-alpha-D-glucose; UDPglucose; URIDINE-5'-DIPHOSPHATE-GLUCOSE; Uridine diphosphate glucose"
      minerva_type "Simple molecule"
      minerva_x 2631.5
      minerva_y 2229.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa10"
      species_meta_id "s_id_sa10"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 119
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1000.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "histone_space_n6_minus_acetyl_minus_l_minus_lysine__mitochondrion__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa115"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa115"
      minerva_name "Histone N6-acetyl-L-lysine"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/substance/5094"
      minerva_ref_type__resource1 "PUBCHEM_SUBSTANCE__5094"
      minerva_type "Simple molecule"
      minerva_x 2171.0
      minerva_y 1121.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrion"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa115"
      species_meta_id "s_id_sa115"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 120
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1100.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sirt5:nsp14__mitochondrion__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa6"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa6"
      minerva_name "SIRT5:Nsp14"
      minerva_type "Complex"
      minerva_x 2051.0
      minerva_y 905.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrion"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa6"
      species_meta_id "s_id_csa6"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 121
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1200.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "impdh2__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa209"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa209"
      minerva_name "IMPDH2"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/3615"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000178035"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/3615"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IMPDH2"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IMPDH2"
      minerva_ref_link5 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=1.1.1.205"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P12268"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/P12268"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_000884"
      minerva_ref_link9 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6053"
      minerva_ref_type__resource1 "ENTREZ__3615"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000178035"
      minerva_ref_type__resource2 "ENTREZ__3615"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IMPDH2"
      minerva_ref_type__resource4 "HGNC_SYMBOL__IMPDH2"
      minerva_ref_type__resource5 "EC__1.1.1.205"
      minerva_ref_type__resource6 "UNIPROT__P12268"
      minerva_ref_type__resource7 "UNIPROT__P12268"
      minerva_ref_type__resource8 "REFSEQ__NM_000884"
      minerva_ref_type__resource9 "HGNC__6053"
      minerva_type "Protein"
      minerva_x 5090.0
      minerva_y 1277.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa209"
      species_meta_id "s_id_sa209"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 122
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "prps2__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa149"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa149"
      minerva_name "PRPS2"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/P11908"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PRPS2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P11908"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000101911"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.6.1"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/5634"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/5634"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/9465"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PRPS2"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_002765"
      minerva_ref_type__resource1 "UNIPROT__P11908"
      minerva_ref_type__resource10 "HGNC_SYMBOL__PRPS2"
      minerva_ref_type__resource2 "UNIPROT__P11908"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000101911"
      minerva_ref_type__resource4 "EC__2.7.6.1"
      minerva_ref_type__resource5 "ENTREZ__5634"
      minerva_ref_type__resource6 "ENTREZ__5634"
      minerva_ref_type__resource7 "HGNC__9465"
      minerva_ref_type__resource8 "HGNC_SYMBOL__PRPS2"
      minerva_ref_type__resource9 "REFSEQ__NM_002765"
      minerva_type "Protein"
      minerva_x 865.0
      minerva_y 973.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa149"
      species_meta_id "s_id_sa149"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 123
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "5_minus_phospho_minus_beta_minus_d_minus_ribosylglycinamide__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa159"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa159"
      minerva_fullName "N(1)-(5-phospho-beta-D-ribosyl)glycinamide(1-)"
      minerva_name "5-phospho-beta-D-ribosylglycinamide"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/129630972"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:143788"
      minerva_ref_type__resource1 "PUBCHEM__129630972"
      minerva_ref_type__resource2 "CHEBI__CHEBI:143788"
      minerva_synonyms "N(1)-(5-phospho-beta-D-ribosyl)glycinamide"
      minerva_type "Simple molecule"
      minerva_x 1556.0
      minerva_y 752.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa159"
      species_meta_id "s_id_sa159"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 124
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "2_minus_deoxy_minus__alpha__minus_d_minus_ribose_space_1_minus_phosphate__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa357"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa260"
      minerva_elementId2 "sa357"
      minerva_fullName "2-deoxy-D-ribofuranose 1-phosphate"
      minerva_name "2-deoxy-α-D-ribose 1-phosphate"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/439287"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:28542"
      minerva_ref_type__resource1 "PUBCHEM__439287"
      minerva_ref_type__resource2 "CHEBI__CHEBI:28542"
      minerva_synonyms "2-Deoxy-D-ribose 1-phosphate; 2-deoxy-1-O-phosphono-D-erythro-pentofuranose; 2-deoxy-1-O-phosphono-D-ribofuranose"
      minerva_type "Simple molecule"
      minerva_x 4412.0
      minerva_x2 4306.125
      minerva_y 1669.5
      minerva_y2 1337.625
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa357"
      species_meta_id "s_id_sa357"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 125
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "gmps__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa216"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa216"
      minerva_name "GMPS"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_003875"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/8833"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/gene/8833"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/pubmed/8089153"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/4378"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P49915"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P49915"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000163655"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=GMPS"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=GMPS"
      minerva_ref_link9 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=6.3.5.2"
      minerva_ref_type__resource1 "REFSEQ__NM_003875"
      minerva_ref_type__resource10 "ENTREZ__8833"
      minerva_ref_type__resource11 "ENTREZ__8833"
      minerva_ref_type__resource2 "PUBMED__8089153"
      minerva_ref_type__resource3 "HGNC__4378"
      minerva_ref_type__resource4 "UNIPROT__P49915"
      minerva_ref_type__resource5 "UNIPROT__P49915"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000163655"
      minerva_ref_type__resource7 "HGNC_SYMBOL__GMPS"
      minerva_ref_type__resource8 "HGNC_SYMBOL__GMPS"
      minerva_ref_type__resource9 "EC__6.3.5.2"
      minerva_type "Protein"
      minerva_x 5394.0
      minerva_y 1623.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa216"
      species_meta_id "s_id_sa216"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 126
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ribavirin__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa214"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa214"
      minerva_name "Ribavirin"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:63580"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/pubmed/22555152"
      minerva_ref_link3 "https://pubchem.ncbi.nlm.nih.gov/compound/37542"
      minerva_ref_type__resource1 "CHEBI__CHEBI:63580"
      minerva_ref_type__resource2 "PUBMED__22555152"
      minerva_ref_type__resource3 "PUBCHEM__37542"
      minerva_type "Drug"
      minerva_x 5202.0
      minerva_y 1406.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa214"
      species_meta_id "s_id_sa214"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 127
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "thioredoxin__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa376"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa376"
      minerva_elementId2 "sa242"
      minerva_fullName "thioredoxin"
      minerva_name "Thioredoxin"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/substance/223441017"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/substance/3635"
      minerva_ref_link3 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15033"
      minerva_ref_type__resource1 "PUBCHEM_SUBSTANCE__223441017"
      minerva_ref_type__resource2 "PUBCHEM_SUBSTANCE__3635"
      minerva_ref_type__resource3 "CHEBI__CHEBI:15033"
      minerva_synonyms "Reduced thioredoxin; thioredoxins"
      minerva_type "Simple molecule"
      minerva_x 3481.125
      minerva_x2 5132.0
      minerva_y 973.875
      minerva_y2 2035.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa376"
      species_meta_id "s_id_sa376"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 128
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 800.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "prps1l1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa151"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa151"
      minerva_former_symbols "PRPSL"
      minerva_name "PRPS1L1"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PRPS1L1"
      minerva_ref_link10 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/9463"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PRPS1L1"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000229937"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/221823"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/221823"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_175886"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/P21108"
      minerva_ref_link8 "https://purl.uniprot.org/uniprot/P21108"
      minerva_ref_link9 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.6.1"
      minerva_ref_type__resource1 "HGNC_SYMBOL__PRPS1L1"
      minerva_ref_type__resource10 "HGNC__9463"
      minerva_ref_type__resource2 "HGNC_SYMBOL__PRPS1L1"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000229937"
      minerva_ref_type__resource4 "ENTREZ__221823"
      minerva_ref_type__resource5 "ENTREZ__221823"
      minerva_ref_type__resource6 "REFSEQ__NM_175886"
      minerva_ref_type__resource7 "UNIPROT__P21108"
      minerva_ref_type__resource8 "UNIPROT__P21108"
      minerva_ref_type__resource9 "EC__2.7.6.1"
      minerva_type "Protein"
      minerva_x 865.0
      minerva_y 1016.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa151"
      species_meta_id "s_id_sa151"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 129
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nadh__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa79"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa79"
      minerva_elementId2 "sa207"
      minerva_elementId3 "sa322"
      minerva_fullName "NADH"
      minerva_name "NADH"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16908"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/compound/439153"
      minerva_ref_type__resource1 "CHEBI__CHEBI:16908"
      minerva_ref_type__resource2 "PUBCHEM__439153"
      minerva_synonyms "1,4-DIHYDRONICOTINAMIDE ADENINE DINUCLEOTIDE; DPNH; NADH; Reduced nicotinamide adenine dinucleotide; nicotinamide adenine dinucleotide (reduced)"
      minerva_type "Simple molecule"
      minerva_x 1994.0
      minerva_x2 5464.416666666666
      minerva_x3 4359.25
      minerva_y 1905.5
      minerva_y2 1389.1525082851072
      minerva_y3 1398.5691749517873
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa79"
      species_meta_id "s_id_sa79"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 130
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1000.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "adp_minus_d_minus_ribose__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa124"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa124"
      minerva_fullName "ADP-D-ribose"
      minerva_name "ADP-D-ribose"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/33576"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16960"
      minerva_ref_type__resource1 "PUBCHEM__33576"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16960"
      minerva_synonyms "(Rib5)ppA; 5-(adenosine 5'-pyrophosphoryl)-D-ribose; A5'pp5Rib; ADP ribose; ADP-D-ribose; ADP-Rib; ADP-riboses; ADPribose; Adenosine diphosphate ribose; AdoPPRib; D-ribofuranose, 5-5'-ester with adenosine 5'-(trihydrogen pyrophosphate); adenosine 5'-(trihydrogen diphosphate), P'-5-ester with D-ribose; adenosine 5'-diphosphoribose; adenosine 5'-pyrophosphate, 5'-5-ester with D-ribofuranose; adenosine diphosphate ribose; ribose adenosinediphosphate"
      minerva_type "Simple molecule"
      minerva_x 1432.5
      minerva_y 1290.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa124"
      species_meta_id "s_id_sa124"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 131
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "entpd4__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa290"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa290"
      minerva_former_symbols "LYSAL1"
      minerva_name "ENTPD4"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/14573"
      minerva_ref_link10 "https://purl.uniprot.org/uniprot/Q9Y227"
      minerva_ref_link11 "https://purl.uniprot.org/uniprot/Q9Y227"
      minerva_ref_link12 "https://www.ncbi.nlm.nih.gov/protein/NM_004901"
      minerva_ref_link2 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.6.1.6"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000197217"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ENTPD4"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ENTPD4"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.6.1.15"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/9583"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/9583"
      minerva_ref_link9 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.6.1.42"
      minerva_ref_type__resource1 "HGNC__14573"
      minerva_ref_type__resource10 "UNIPROT__Q9Y227"
      minerva_ref_type__resource11 "UNIPROT__Q9Y227"
      minerva_ref_type__resource12 "REFSEQ__NM_004901"
      minerva_ref_type__resource2 "EC__3.6.1.6"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000197217"
      minerva_ref_type__resource4 "HGNC_SYMBOL__ENTPD4"
      minerva_ref_type__resource5 "HGNC_SYMBOL__ENTPD4"
      minerva_ref_type__resource6 "EC__3.6.1.15"
      minerva_ref_type__resource7 "ENTREZ__9583"
      minerva_ref_type__resource8 "ENTREZ__9583"
      minerva_ref_type__resource9 "EC__3.6.1.42"
      minerva_type "Protein"
      minerva_x 5570.0
      minerva_y 1828.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa290"
      species_meta_id "s_id_sa290"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 132
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1200.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ada__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa363"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa363"
      minerva_elementId2 "sa337"
      minerva_name "ADA"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_000022"
      minerva_ref_link10 "https://purl.uniprot.org/uniprot/P00813"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000196839"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ADA"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ADA"
      minerva_ref_link5 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.5.4.4"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/186"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/100"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/100"
      minerva_ref_link9 "https://purl.uniprot.org/uniprot/P00813"
      minerva_ref_type__resource1 "REFSEQ__NM_000022"
      minerva_ref_type__resource10 "UNIPROT__P00813"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000196839"
      minerva_ref_type__resource3 "HGNC_SYMBOL__ADA"
      minerva_ref_type__resource4 "HGNC_SYMBOL__ADA"
      minerva_ref_type__resource5 "EC__3.5.4.4"
      minerva_ref_type__resource6 "HGNC__186"
      minerva_ref_type__resource7 "ENTREZ__100"
      minerva_ref_type__resource8 "ENTREZ__100"
      minerva_ref_type__resource9 "UNIPROT__P00813"
      minerva_type "Protein"
      minerva_x 3862.625
      minerva_x2 4611.0
      minerva_y 1464.125
      minerva_y2 1016.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa363"
      species_meta_id "s_id_sa363"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 133
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nicotinate__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa137"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa137"
      minerva_fullName "nicotinate"
      minerva_name "Nicotinate"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/937"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:32544"
      minerva_ref_type__resource1 "PUBCHEM__937"
      minerva_ref_type__resource2 "CHEBI__CHEBI:32544"
      minerva_synonyms "3-pyridinecarboxylate; nicotinate"
      minerva_type "Simple molecule"
      minerva_x 551.0
      minerva_y 1129.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa137"
      species_meta_id "s_id_sa137"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 134
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "xanthosine__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa308"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa308"
      minerva_fullName "xanthosine"
      minerva_name "Xanthosine"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/64959"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18107"
      minerva_ref_type__resource1 "PUBCHEM__64959"
      minerva_ref_type__resource2 "CHEBI__CHEBI:18107"
      minerva_synonyms "9-beta-D-Ribofuranosylxanthine; 9-beta-D-ribofuranosyl-3,9-dihydro-1H-purine-2,6-dione; 9-beta-D-ribofuranosylxanthine; Xanthine riboside; Xanthosine; xanthine 9-beta-D-ribofuranoside; xanthosine"
      minerva_type "Simple molecule"
      minerva_x 4899.0
      minerva_y 1443.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa308"
      species_meta_id "s_id_sa308"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 135
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "pfas__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa171"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa171"
      minerva_name "PFAS"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/8863"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PFAS"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_012393"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/5198"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/5198"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000178921"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/O15067"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/O15067"
      minerva_ref_link8 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=6.3.5.3"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PFAS"
      minerva_ref_type__resource1 "HGNC__8863"
      minerva_ref_type__resource10 "HGNC_SYMBOL__PFAS"
      minerva_ref_type__resource2 "REFSEQ__NM_012393"
      minerva_ref_type__resource3 "ENTREZ__5198"
      minerva_ref_type__resource4 "ENTREZ__5198"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000178921"
      minerva_ref_type__resource6 "UNIPROT__O15067"
      minerva_ref_type__resource7 "UNIPROT__O15067"
      minerva_ref_type__resource8 "EC__6.3.5.3"
      minerva_ref_type__resource9 "HGNC_SYMBOL__PFAS"
      minerva_type "Protein"
      minerva_x 2349.0
      minerva_y 680.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa171"
      species_meta_id "s_id_sa171"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 136
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "guanosine__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa257"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa257"
      minerva_fullName "guanosine"
      minerva_name "Guanosine"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/135398635"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16750"
      minerva_ref_type__resource1 "PUBCHEM__135398635"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16750"
      minerva_synonyms "2(3H)-imino-9-beta-D-ribofuranosyl-9H-purin-6(1H)-one; 2-amino-1,9-dihydro-9-beta-D-ribofuranosyl-6H-purin-6-one; 2-amino-9-beta-D-ribofuranosyl-1,9-dihydro-6H-purin-6-one; 9-beta-D-ribofuranosyl-guanine; G; Guanine riboside; Guanine-9-beta-D-ribofuranoside; Guanosin; Guanosine; Guo; guanosine"
      minerva_type "Simple molecule"
      minerva_x 4888.5
      minerva_y 1605.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa257"
      species_meta_id "s_id_sa257"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 137
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "d_minus_galactose__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa1"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa1"
      minerva_fullName "beta-D-galactose"
      minerva_name "D-Galactose"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/439353"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:27667"
      minerva_ref_type__resource1 "PUBCHEM__439353"
      minerva_ref_type__resource2 "CHEBI__CHEBI:27667"
      minerva_synonyms "BETA-D-GALACTOSE; Gal-beta; beta-D-Gal; beta-D-Galactose; beta-D-galactose"
      minerva_type "Simple molecule"
      minerva_x 2178.1875
      minerva_y 2342.75
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa1"
      species_meta_id "s_id_sa1"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 138
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "galk1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa5"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa5"
      minerva_former_symbols "GALK"
      minerva_name "GALK1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_000154"
      minerva_ref_link10 "https://purl.uniprot.org/uniprot/P51570"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000108479"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=GALK1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=GALK1"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/4118"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/2584"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/2584"
      minerva_ref_link8 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.1.6"
      minerva_ref_link9 "https://purl.uniprot.org/uniprot/P51570"
      minerva_ref_type__resource1 "REFSEQ__NM_000154"
      minerva_ref_type__resource10 "UNIPROT__P51570"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000108479"
      minerva_ref_type__resource3 "HGNC_SYMBOL__GALK1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__GALK1"
      minerva_ref_type__resource5 "HGNC__4118"
      minerva_ref_type__resource6 "ENTREZ__2584"
      minerva_ref_type__resource7 "ENTREZ__2584"
      minerva_ref_type__resource8 "EC__2.7.1.6"
      minerva_ref_type__resource9 "UNIPROT__P51570"
      minerva_type "Protein"
      minerva_x 2499.6875
      minerva_y 2286.75
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa5"
      species_meta_id "s_id_sa5"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 139
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 800.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "deoxyadenosine__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa359"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa359"
      minerva_fullName "2'-deoxyadenosine"
      minerva_name "Deoxyadenosine"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/13730"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17256"
      minerva_ref_type__resource1 "PUBCHEM__13730"
      minerva_ref_type__resource2 "CHEBI__CHEBI:17256"
      minerva_synonyms "(2R,3S,5R)-5-(6-amino-9H-purin-9-yl)-2-(hydroxymethyl)tetrahydrofuran-3-ol; 2'-Deoxyadenosine; 2'-deoxyadenosine; 5-(6-AMINO-PURIN-9-YL)-2-HYDROXYMETHYL-TETRAHYDRO-FURAN-3-OL; 9-(2-Deoxy-beta-D-erythro-pentofuranosyl)adenine; 9-(2-deoxy-beta-D-ribofuranosyl)-9H-purin-6-amine; Deoxyadenosine; adenine deoxyribonucleoside; adenyldeoxyriboside; dA"
      minerva_type "Simple molecule"
      minerva_x 3665.0
      minerva_y 1411.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa359"
      species_meta_id "s_id_sa359"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 140
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "gale__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa19"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa19"
      minerva_name "GALE"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=5.1.3.2"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_000403"
      minerva_ref_link11 "https://www.ensembl.org/id/ENSG00000117308"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=GALE"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/Q14376"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/Q14376"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=GALE"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/4116"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/2582"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/2582"
      minerva_ref_link9 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=5.1.3.7"
      minerva_ref_type__resource1 "EC__5.1.3.2"
      minerva_ref_type__resource10 "REFSEQ__NM_000403"
      minerva_ref_type__resource11 "ENSEMBL__ENSG00000117308"
      minerva_ref_type__resource2 "HGNC_SYMBOL__GALE"
      minerva_ref_type__resource3 "UNIPROT__Q14376"
      minerva_ref_type__resource4 "UNIPROT__Q14376"
      minerva_ref_type__resource5 "HGNC_SYMBOL__GALE"
      minerva_ref_type__resource6 "HGNC__4116"
      minerva_ref_type__resource7 "ENTREZ__2582"
      minerva_ref_type__resource8 "ENTREZ__2582"
      minerva_ref_type__resource9 "EC__5.1.3.7"
      minerva_type "Protein"
      minerva_x 3060.0
      minerva_y 2395.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa19"
      species_meta_id "s_id_sa19"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 141
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1000.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "prps1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa148"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa148"
      minerva_former_symbols "DFN2"
      minerva_name "PRPS1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_001204402"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PRPS1"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/9462"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000147224"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/5631"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/5631"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.6.1"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PRPS1"
      minerva_ref_link8 "https://purl.uniprot.org/uniprot/P60891"
      minerva_ref_link9 "https://purl.uniprot.org/uniprot/P60891"
      minerva_ref_type__resource1 "REFSEQ__NM_001204402"
      minerva_ref_type__resource10 "HGNC_SYMBOL__PRPS1"
      minerva_ref_type__resource2 "HGNC__9462"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000147224"
      minerva_ref_type__resource4 "ENTREZ__5631"
      minerva_ref_type__resource5 "ENTREZ__5631"
      minerva_ref_type__resource6 "EC__2.7.6.1"
      minerva_ref_type__resource7 "HGNC_SYMBOL__PRPS1"
      minerva_ref_type__resource8 "UNIPROT__P60891"
      minerva_ref_type__resource9 "UNIPROT__P60891"
      minerva_type "Protein"
      minerva_x 865.0
      minerva_y 930.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa148"
      species_meta_id "s_id_sa148"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 142
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1100.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "h2o__mitochondrion__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa119"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa119"
      minerva_fullName "water"
      minerva_name "H2O"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15377"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/compound/962"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15377"
      minerva_ref_type__resource2 "PUBCHEM__962"
      minerva_synonyms "BOUND WATER; H2O; H2O; HOH; WATER; Wasser; Water; [OH2]; acqua; agua; aqua; dihydridooxygen; dihydrogen oxide; eau; hydrogen hydroxide"
      minerva_type "Simple molecule"
      minerva_x 1930.5714285714287
      minerva_y 1137.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrion"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa119"
      species_meta_id "s_id_sa119"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 143
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1200.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "imp__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa202"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa202"
      minerva_fullName "IMP"
      minerva_name "IMP"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/135398640"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17202"
      minerva_ref_type__resource1 "PUBCHEM__135398640"
      minerva_ref_type__resource2 "CHEBI__CHEBI:17202"
      minerva_synonyms "2'-inosine-5'-monophosphate; 5'-IMP; 5'-Inosinate; 5'-Inosine monophosphate; 5'-Inosinic acid; IMP; Inosine 5'-monophosphate; Inosine 5'-phosphate; Inosine monophosphate; Inosinic acid; hypoxanthosine 5'-monophosphate; inosinic acid; ribosylhypoxanthine monophosphate"
      minerva_type "Simple molecule"
      minerva_x 5291.0
      minerva_y 1187.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa202"
      species_meta_id "s_id_sa202"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 144
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "lct__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa57"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa57"
      minerva_name "LCT"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_002299"
      minerva_ref_link10 "https://purl.uniprot.org/uniprot/P09848"
      minerva_ref_link11 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.2.1.62"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000115850"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6530"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=LCT"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=LCT"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.2.1.108"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/3938"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/3938"
      minerva_ref_link9 "https://purl.uniprot.org/uniprot/P09848"
      minerva_ref_type__resource1 "REFSEQ__NM_002299"
      minerva_ref_type__resource10 "UNIPROT__P09848"
      minerva_ref_type__resource11 "EC__3.2.1.62"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000115850"
      minerva_ref_type__resource3 "HGNC__6530"
      minerva_ref_type__resource4 "HGNC_SYMBOL__LCT"
      minerva_ref_type__resource5 "HGNC_SYMBOL__LCT"
      minerva_ref_type__resource6 "EC__3.2.1.108"
      minerva_ref_type__resource7 "ENTREZ__3938"
      minerva_ref_type__resource8 "ENTREZ__3938"
      minerva_ref_type__resource9 "UNIPROT__P09848"
      minerva_type "Protein"
      minerva_x 2372.0
      minerva_y 2557.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa57"
      species_meta_id "s_id_sa57"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 145
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ak1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa392"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa392"
      minerva_name "AK1"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/P00568"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AK1"
      minerva_ref_link11 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AK1"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P00568"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_000476"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000106992"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/203"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/203"
      minerva_ref_link7 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/361"
      minerva_ref_link9 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.3"
      minerva_ref_type__resource1 "UNIPROT__P00568"
      minerva_ref_type__resource10 "HGNC_SYMBOL__AK1"
      minerva_ref_type__resource11 "HGNC_SYMBOL__AK1"
      minerva_ref_type__resource2 "UNIPROT__P00568"
      minerva_ref_type__resource3 "REFSEQ__NM_000476"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000106992"
      minerva_ref_type__resource5 "ENTREZ__203"
      minerva_ref_type__resource6 "ENTREZ__203"
      minerva_ref_type__resource7 "EC__2.7.4.6"
      minerva_ref_type__resource8 "HGNC__361"
      minerva_ref_type__resource9 "EC__2.7.4.3"
      minerva_type "Protein"
      minerva_x 3759.0
      minerva_y 869.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa392"
      species_meta_id "s_id_sa392"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 146
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "_alpha__minus_d_minus_glucose_minus_1_minus_p__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa11"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa11"
      minerva_fullName "alpha-D-glucose 1-phosphate"
      minerva_name "α-D-Glucose-1-P"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/439165"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:29042"
      minerva_ref_type__resource1 "PUBCHEM__439165"
      minerva_ref_type__resource2 "CHEBI__CHEBI:29042"
      minerva_synonyms "1-O-phosphono-alpha-D-glucopyranose; ALPHA-D-GLUCOSE-1-PHOSPHATE; Cori ester; D-Glucose 1-phosphate; D-Glucose alpha-1-phosphate; alpha-D-Glucose 1-phosphate; alpha-D-Glucose-1-phosphate; alpha-D-glucopyranosyl phosphate"
      minerva_type "Simple molecule"
      minerva_x 2908.5
      minerva_y 2229.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa11"
      species_meta_id "s_id_sa11"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 147
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "merimepodib__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa212"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa212"
      minerva_name "Merimepodib"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/53241"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/pubmed/10878288"
      minerva_ref_type__resource1 "PUBCHEM__53241"
      minerva_ref_type__resource2 "PUBMED__10878288"
      minerva_type "Drug"
      minerva_x 5109.5
      minerva_y 1387.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa212"
      species_meta_id "s_id_sa212"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 148
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nampt__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa108"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa108"
      minerva_former_symbols "PBEF1"
      minerva_name "NAMPT"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NAMPT"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000105835"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NAMPT"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.4.2.12"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/30092"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P43490"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P43490"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/10135"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_182790"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/10135"
      minerva_ref_type__resource1 "HGNC_SYMBOL__NAMPT"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000105835"
      minerva_ref_type__resource2 "HGNC_SYMBOL__NAMPT"
      minerva_ref_type__resource3 "EC__2.4.2.12"
      minerva_ref_type__resource4 "HGNC__30092"
      minerva_ref_type__resource5 "UNIPROT__P43490"
      minerva_ref_type__resource6 "UNIPROT__P43490"
      minerva_ref_type__resource7 "ENTREZ__10135"
      minerva_ref_type__resource8 "REFSEQ__NM_182790"
      minerva_ref_type__resource9 "ENTREZ__10135"
      minerva_type "Protein"
      minerva_x 1015.0
      minerva_y 1527.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa108"
      species_meta_id "s_id_sa108"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 149
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ampd1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa304"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa304"
      minerva_name "AMPD1"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/468"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AMPD1"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/270"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_000036"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/270"
      minerva_ref_link5 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.5.4.6"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000116748"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/P23109"
      minerva_ref_link8 "https://purl.uniprot.org/uniprot/P23109"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AMPD1"
      minerva_ref_type__resource1 "HGNC__468"
      minerva_ref_type__resource10 "HGNC_SYMBOL__AMPD1"
      minerva_ref_type__resource2 "ENTREZ__270"
      minerva_ref_type__resource3 "REFSEQ__NM_000036"
      minerva_ref_type__resource4 "ENTREZ__270"
      minerva_ref_type__resource5 "EC__3.5.4.6"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000116748"
      minerva_ref_type__resource7 "UNIPROT__P23109"
      minerva_ref_type__resource8 "UNIPROT__P23109"
      minerva_ref_type__resource9 "HGNC_SYMBOL__AMPD1"
      minerva_type "Protein"
      minerva_x 4619.0
      minerva_y 873.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa304"
      species_meta_id "s_id_sa304"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 150
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 800.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sirt5__mitochondrion__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa403"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa403"
      minerva_name "SIRT5"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/Q9NXA8"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/23408"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/protein/NM_001193267"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/Q9NXA8"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.1.-"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/pubmed/17694089"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SIRT5"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SIRT5"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/14933"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/23408"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000124523"
      minerva_ref_type__resource1 "UNIPROT__Q9NXA8"
      minerva_ref_type__resource10 "ENTREZ__23408"
      minerva_ref_type__resource11 "REFSEQ__NM_001193267"
      minerva_ref_type__resource2 "UNIPROT__Q9NXA8"
      minerva_ref_type__resource3 "EC__2.3.1.-"
      minerva_ref_type__resource4 "PUBMED__17694089"
      minerva_ref_type__resource5 "HGNC_SYMBOL__SIRT5"
      minerva_ref_type__resource6 "HGNC_SYMBOL__SIRT5"
      minerva_ref_type__resource7 "HGNC__14933"
      minerva_ref_type__resource8 "ENTREZ__23408"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000124523"
      minerva_type "Protein"
      minerva_x 2002.0
      minerva_y 1043.8
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrion"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa403"
      species_meta_id "s_id_sa403"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 151
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "inosine__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa313"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa313"
      minerva_fullName "inosine"
      minerva_name "Inosine"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/35398641"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17596"
      minerva_ref_type__resource1 "PUBCHEM__35398641"
      minerva_ref_type__resource2 "CHEBI__CHEBI:17596"
      minerva_synonyms "9-beta-D-ribofuranosyl-9H-purin-6-ol; 9-beta-D-ribofuranosylhypoxanthine; INOSINE; Inosin; Inosine; hypoxanthine D-riboside; hypoxanthosine; i; inosina; inosine; inosine; inosinum"
      minerva_type "Simple molecule"
      minerva_x 4913.0
      minerva_y 1187.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa313"
      species_meta_id "s_id_sa313"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 152
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1000.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "xanthine__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa309"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa309"
      minerva_fullName "xanthine"
      minerva_name "Xanthine"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/1188"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15318"
      minerva_ref_type__resource1 "PUBCHEM__1188"
      minerva_ref_type__resource2 "CHEBI__CHEBI:15318"
      minerva_synonyms "2,6-dioxopurine; 2,6-dioxopurines; xanthine; xanthines"
      minerva_type "Simple molecule"
      minerva_x 4452.0
      minerva_y 1443.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa309"
      species_meta_id "s_id_sa309"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 153
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "impdh2:nsp14__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa8"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa8"
      minerva_name "IMPDH2:Nsp14"
      minerva_type "Complex"
      minerva_x 4888.0
      minerva_y 1347.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa8"
      species_meta_id "s_id_csa8"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 154
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1200.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "l_minus_aspartate__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa194"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa194"
      minerva_fullName "L-aspartic acid"
      minerva_name "L-Aspartate"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17053"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/compound/5960"
      minerva_ref_type__resource1 "CHEBI__CHEBI:17053"
      minerva_ref_type__resource2 "PUBCHEM__5960"
      minerva_synonyms "(S)-2-aminobutanedioic acid; (S)-2-aminosuccinic acid; 2-Aminosuccinic acid; ASPARTIC ACID; Asp; D; L-Asparaginsaeure; L-Aspartic acid"
      minerva_type "Simple molecule"
      minerva_x 3835.0
      minerva_y 798.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa194"
      species_meta_id "s_id_sa194"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 155
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "fumarate__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa196"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa196"
      minerva_fullName "fumarate(2-)"
      minerva_name "Fumarate"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/5460307"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:29806"
      minerva_ref_type__resource1 "PUBCHEM__5460307"
      minerva_ref_type__resource2 "CHEBI__CHEBI:29806"
      minerva_synonyms "(2E)-but-2-enedioate; (E)-2-butenedioic acid, ion(2-); FUMARATE; fumarate"
      minerva_type "Simple molecule"
      minerva_x 4620.0
      minerva_y 709.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa196"
      species_meta_id "s_id_sa196"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 156
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "guanine_space_nucleotide_space_synthesis__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa411"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa411"
      minerva_name "Guanine nucleotide synthesis"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/1969416"
      minerva_ref_type__resource1 "PUBMED__1969416"
      minerva_type "Phenotype"
      minerva_x 4903.5
      minerva_y 1266.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa411"
      species_meta_id "s_id_sa411"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 157
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "datp__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa373"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa373"
      minerva_fullName "dATP"
      minerva_name "dATP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16284"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/compound/15993"
      minerva_ref_type__resource1 "CHEBI__CHEBI:16284"
      minerva_ref_type__resource2 "PUBCHEM__15993"
      minerva_synonyms "2'-Deoxyadenosine 5'-triphosphate; 2'-deoxyadenosine 5'-triphosphate; Deoxyadenosine 5'-triphosphate; Deoxyadenosine triphosphate; dATP"
      minerva_type "Simple molecule"
      minerva_x 2695.5
      minerva_y 1167.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa373"
      species_meta_id "s_id_sa373"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 158
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "gla:nsp14__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa7"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa7"
      minerva_name "GLA:Nsp14"
      minerva_type "Complex"
      minerva_x 1459.0
      minerva_y 2459.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa7"
      species_meta_id "s_id_csa7"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 159
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "gmpr__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa299"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa299"
      minerva_name "GMPR"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/2766"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=GMPR"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/2766"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_006877"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/4376"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000137198"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=1.7.1.7"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/P36959"
      minerva_ref_link8 "https://purl.uniprot.org/uniprot/P36959"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=GMPR"
      minerva_ref_type__resource1 "ENTREZ__2766"
      minerva_ref_type__resource10 "HGNC_SYMBOL__GMPR"
      minerva_ref_type__resource2 "ENTREZ__2766"
      minerva_ref_type__resource3 "REFSEQ__NM_006877"
      minerva_ref_type__resource4 "HGNC__4376"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000137198"
      minerva_ref_type__resource6 "EC__1.7.1.7"
      minerva_ref_type__resource7 "UNIPROT__P36959"
      minerva_ref_type__resource8 "UNIPROT__P36959"
      minerva_ref_type__resource9 "HGNC_SYMBOL__GMPR"
      minerva_type "Protein"
      minerva_x 5544.0
      minerva_y 1467.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa299"
      species_meta_id "s_id_sa299"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 160
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "stachyose__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa53"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa53"
      minerva_fullName "stachyose"
      minerva_name "Stachyose"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/439531"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17164"
      minerva_ref_type__resource1 "PUBCHEM__439531"
      minerva_ref_type__resource2 "CHEBI__CHEBI:17164"
      minerva_synonyms "O-alpha-D-galactopyranosyl-(1->6)o-alpha-D-galactopyranosyl-(1->6)O-alpha-D-galactopyranosyl-beta-D-fructofuranoside; Stachyose; alpha-D-Galp-(1->6)-alpha-D-Galp-(1->6)-alpha-D-Glcp-(1<->2)-beta-D-Fruf; stachyose"
      minerva_type "Simple molecule"
      minerva_x 1404.0
      minerva_y 2345.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa53"
      species_meta_id "s_id_sa53"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 161
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 800.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "adenosine__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa333"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa333"
      minerva_fullName "adenosine"
      minerva_name "Adenosine"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/60961"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16335"
      minerva_ref_type__resource1 "PUBCHEM__60961"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16335"
      minerva_synonyms "(2R,3R,4S,5R)-2-(6-aminopurin-9-yl)-5-(hydroxymethyl)oxolane-3,4-diol; 6-Amino-9-beta-D-ribofuranosyl-9H-purine; 9-beta-D-Ribofuranosidoadenine; 9-beta-D-Ribofuranosyl-9H-purin-6-amine; 9-beta-D-ribofuranosyl-9H-purin-6-amine; ADENOSINE; Ade-Rib; Adenine Deoxyribonucleoside; Adenocard; Adenocor; Adenoscan; Adenosin; Adenosine; Adenyldeoxyriboside; Ado; Deoxyadenosine; Desoxyadenosine; adenosine; beta-D-Adenosine"
      minerva_type "Simple molecule"
      minerva_x 4234.5
      minerva_y 1059.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa333"
      species_meta_id "s_id_sa333"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 162
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ppat__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa158"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa158"
      minerva_name "PPAT"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/Q06203"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.4.2.14"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/Q06203"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/9238"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PPAT"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PPAT"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/5471"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000128059"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/5471"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_002703"
      minerva_ref_type__resource1 "UNIPROT__Q06203"
      minerva_ref_type__resource10 "EC__2.4.2.14"
      minerva_ref_type__resource2 "UNIPROT__Q06203"
      minerva_ref_type__resource3 "HGNC__9238"
      minerva_ref_type__resource4 "HGNC_SYMBOL__PPAT"
      minerva_ref_type__resource5 "HGNC_SYMBOL__PPAT"
      minerva_ref_type__resource6 "ENTREZ__5471"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000128059"
      minerva_ref_type__resource8 "ENTREZ__5471"
      minerva_ref_type__resource9 "REFSEQ__NM_002703"
      minerva_type "Protein"
      minerva_x 1067.0
      minerva_y 1003.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa158"
      species_meta_id "s_id_sa158"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 163
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1000.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "melibiose__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa33"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa33"
      minerva_fullName "melibiose"
      minerva_name "Melibiose"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:28053"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/compound/440658"
      minerva_ref_type__resource1 "CHEBI__CHEBI:28053"
      minerva_ref_type__resource2 "PUBCHEM__440658"
      minerva_synonyms "(Gal)1 (Glc)1; 6-O-(alpha-D-Galactopyranosyl)-D-glucopyranose; 6-O-alpha-D-galactopyranosyl-D-glucopyranose; D-Melibiose; D-mellibiose; Gal-alpha(1,6)Glc; Melibiose; alpha-D-Gal-(1->6)-D-Glc; alpha-D-Galp-(1->6)-D-Glcp; alpha-D-galactosyl-(1->6)-D-glucose; melibiose"
      minerva_type "Simple molecule"
      minerva_x 1805.0
      minerva_y 2511.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa33"
      species_meta_id "s_id_sa33"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 164
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "lactose__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa20"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa20"
      minerva_fullName "lactose"
      minerva_name "Lactose"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/440995"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17716"
      minerva_ref_type__resource1 "PUBCHEM__440995"
      minerva_ref_type__resource2 "CHEBI__CHEBI:17716"
      minerva_synonyms "(+)-lactose; (Gal)1 (Glc)1; 1-beta-D-Galactopyranosyl-4-D-glucopyranose; 4-(beta-D-galactosido)-D-glucose; 4-O-beta-D-galactopyranosyl-D-glucose; D-lactose; Galbeta1-4Glc; Lac; Laktobiose; Laktose; Milchzucker; Milk sugar; beta-D-Gal-(1->4)-D-Glc; beta-D-Gal-(1->4)-D-Glc; beta-D-Galp-(1->4)-D-Glcp; beta-Gal1,4-Glc; lactobiose; lactose; milk sugar"
      minerva_type "Simple molecule"
      minerva_x 2625.0
      minerva_y 2511.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa20"
      species_meta_id "s_id_sa20"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 165
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1200.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "1_minus_(5_minus_phospho_minus_d_minus_ribosyl)_minus_5_minus_amino_minus_4_minus_imidazolecarboxylate__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa184"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa184"
      minerva_fullName "5-amino-1-(5-phospho-D-ribosyl)imidazole-4-carboxylic acid"
      minerva_name "1-(5-Phospho-D-ribosyl)-5-amino-4-imidazolecarboxylate"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:28413"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/compound/165388"
      minerva_ref_type__resource1 "CHEBI__CHEBI:28413"
      minerva_ref_type__resource2 "PUBCHEM__165388"
      minerva_synonyms "1-(5'-Phosphoribosyl)-4-carboxy-5-aminoimidazole; 1-(5'-Phosphoribosyl)-5-amino-4-carboxyimidazole; 1-(5'-Phosphoribosyl)-5-amino-4-imidazolecarboxylate; 1-(5-Phospho-D-ribosyl)-5-amino-4-imidazolecarboxylate; 5'-Phosphoribosyl-4-carboxy-5-aminoimidazole; 5'-Phosphoribosyl-5-amino-4-imidazolecarboxylate; 5-Amino-1-(5-phospho-D-ribosyl)imidazole-4-carboxylate; 5-amino-1-(5-phospho-D-ribosyl)imidazole-4-carboxylic acid"
      minerva_type "Simple molecule"
      minerva_x 3690.0
      minerva_y 751.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa184"
      species_meta_id "s_id_sa184"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 166
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mycophenolic_space_acid__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa213"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa213"
      minerva_name "Mycophenolic acid"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/5799033"
      minerva_ref_link2 "https://pubchem.ncbi.nlm.nih.gov/compound/446541"
      minerva_ref_link3 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:168396"
      minerva_ref_type__resource1 "PUBMED__5799033"
      minerva_ref_type__resource2 "PUBCHEM__446541"
      minerva_ref_type__resource3 "CHEBI__CHEBI:168396"
      minerva_type "Drug"
      minerva_x 5087.0
      minerva_y 1347.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa213"
      species_meta_id "s_id_sa213"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 167
    zlevel -1

    graphics [
      x 300.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NAD kinase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re20"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 1571.5
      minerva_y 1905.25
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re20"
      reaction_meta_id "re20"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 174
    zlevel -1

    graphics [
      x 400.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "inosine:phosphate alpha-D-ribosyltransferase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re74"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 4682.0
      minerva_y 1187.25
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re74"
      reaction_meta_id "re74"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 180
    zlevel -1

    graphics [
      x 500.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "N-Ribosylnicotinamide:orthophosphate ribosyltransferase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re31"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 1248.75
      minerva_y 1258.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re31"
      reaction_meta_id "re31"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 186
    zlevel -1

    graphics [
      x 600.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Deoxyadenosine aminohydrolase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re87"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 3863.5
      minerva_y 1411.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re87"
      reaction_meta_id "re87"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 193
    zlevel -1

    graphics [
      x 700.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ATP:dGMP phosphotransferase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re58"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 4640.5
      minerva_y 1890.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re58"
      reaction_meta_id "re58"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 199
    zlevel -1

    graphics [
      x 800.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Xanthosine phosphorlyase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re72"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 4671.5
      minerva_y 1443.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re72"
      reaction_meta_id "re72"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 205
    zlevel -1

    graphics [
      x 900.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "adenosine 5'-monophosphate phosphohydrolase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re82"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 4096.750000000002
      minerva_y 1059.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re82"
      reaction_meta_id "re82"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 211
    zlevel -1

    graphics [
      x 1000.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "GTP phosphohydrolase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re66"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 5424.000000000004
      minerva_y 2111.0000000000005
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re66"
      reaction_meta_id "re66"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 219
    zlevel -1

    graphics [
      x 1100.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "adenoylsuccinate lyase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re48"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 4520.0
      minerva_y 752.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re48"
      reaction_meta_id "re48"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 224
    zlevel -1

    graphics [
      x 1200.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NAD+ phosphohydrolase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re24"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 1286.4352300680446
      minerva_y 1748.3667891818948
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re24"
      reaction_meta_id "re24"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 231
    zlevel -1

    graphics [
      x 200.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ATP:nicotinamide-nucleotide adenylyltransferase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re35"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 550.5
      minerva_y 1738.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re35"
      reaction_meta_id "re35"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 240
    zlevel -1

    graphics [
      x 300.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Adenine phosphoribosyltransferase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re84"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 3963.5000000000737
      minerva_y 1071.7500000000002
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re84"
      reaction_meta_id "re84"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 246
    zlevel -1

    graphics [
      x 400.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "guanosine 5'-monophosphate phosphohydrolase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re65"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 5031.750000000002
      minerva_y 1605.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re65"
      reaction_meta_id "re65"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 252
    zlevel -1

    graphics [
      x 500.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nicotinate-nucleotide pyrophosphorylase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re36"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 612.9325035256026
      minerva_y 1505.1611652351671
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re36"
      reaction_meta_id "re36"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 261
    zlevel -1

    graphics [
      x 600.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re105"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "Heterodimer association"
      minerva_x 1459.0
      minerva_y 2544.166666666667
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re105"
      reaction_meta_id "re105"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 265
    zlevel -1

    graphics [
      x 700.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "galactokinase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re2"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 2499.5
      minerva_y 2342.75
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re2"
      reaction_meta_id "re2"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 271
    zlevel -1

    graphics [
      x 800.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re41"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 993.5236221006736
      minerva_y 1002.8787148249539
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re41"
      reaction_meta_id "re41"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 279
    zlevel -1

    graphics [
      x 900.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NAD+ glycohydrolase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re33"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 1517.0000000000086
      minerva_y 1463.174621202458
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re33"
      reaction_meta_id "re33"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 286
    zlevel -1

    graphics [
      x 1000.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "GMP synthase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re53"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 5292.75
      minerva_y 1623.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re53"
      reaction_meta_id "re53"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 298
    zlevel -1

    graphics [
      x 1100.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re109"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "Positive influence"
      minerva_x 1950.0
      minerva_y 1088.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re109"
      reaction_meta_id "re109"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 301
    zlevel -1

    graphics [
      x 1200.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "phosphoribosylaminoimidazolecarboxamide formyltransferase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re49"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 5014.5
      minerva_y 752.75
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re49"
      reaction_meta_id "re49"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 307
    zlevel -1

    graphics [
      x 200.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "2'-phospho-ADP-ribosyl cyclase/2'-phospho-cyclic-ADP-ribose transferase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re27"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 907.4854878665587
      minerva_y 1078.499999999986
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re27"
      reaction_meta_id "re27"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 313
    zlevel -1

    graphics [
      x 300.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "2-(Formamido)-N1-(5-phosphoribosyl)acetamidine cyclo-ligase "
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re45"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 2984.5
      minerva_y 752.25
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re45"
      reaction_meta_id "re45"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 321
    zlevel -1

    graphics [
      x 400.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Deoxyinosine:orthophosphate ribosyltransferase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re86"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 4180.615700565144
      minerva_y 1305.333374509752
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re86"
      reaction_meta_id "re86"
      reaction_non_rdf_annotation ""
      reversible "true"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 327
    zlevel -1

    graphics [
      x 500.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re106"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "Heterodimer association"
      minerva_x 4998.75
      minerva_y 1377.0000000000002
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re106"
      reaction_meta_id "re106"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 331
    zlevel -1

    graphics [
      x 600.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Adenlyate kinase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re85"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 3802.974107881443
      minerva_y 914.7789570954365
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re85"
      reaction_meta_id "re85"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 340
    zlevel -1

    graphics [
      x 700.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "XMP 5'-nucleotidase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re71"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 5086.5
      minerva_y 1443.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re71"
      reaction_meta_id "re71"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 346
    zlevel -1

    graphics [
      x 800.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ATP:deoxyguanosine 5'-phosphotransferase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re59"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 4415.0
      minerva_y 1783.25
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re59"
      reaction_meta_id "re59"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 353
    zlevel -1

    graphics [
      x 900.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NAD synthetase 1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re22"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 885.5
      minerva_y 1899.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re22"
      reaction_meta_id "re22"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 363
    zlevel -1

    graphics [
      x 1000.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "AMP deaminase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re70"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 4660.249999999993
      minerva_y 924.9999999999927
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re70"
      reaction_meta_id "re70"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 372
    zlevel -1

    graphics [
      x 1100.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "adenylate kinase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re89"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 3378.5
      minerva_y 1288.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re89"
      reaction_meta_id "re89"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 378
    zlevel -1

    graphics [
      x 1200.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UDP glucose pyrophosphorylase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re4"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 2769.0
      minerva_y 2229.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re4"
      reaction_meta_id "re4"
      reaction_non_rdf_annotation ""
      reversible "true"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 384
    zlevel -1

    graphics [
      x 200.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "adenosine:phosphate alpha-D-ribosyltransferase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re83"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 4234.75
      minerva_y 1139.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re83"
      reaction_meta_id "re83"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 390
    zlevel -1

    graphics [
      x 300.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "inosine:phosphate alpha-D-ribosyltransferase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re75"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 4452.5
      minerva_y 1315.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re75"
      reaction_meta_id "re75"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 398
    zlevel -1

    graphics [
      x 400.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ATP:GDP phosphotransferase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re55"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 5293.25
      minerva_y 2111.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re55"
      reaction_meta_id "re55"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 408
    zlevel -1

    graphics [
      x 500.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "5-Phospho-D-ribosylamine:glycine ligase (ADP-forming)"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re42"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 1318.0
      minerva_y 752.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re42"
      reaction_meta_id "re42"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 417
    zlevel -1

    graphics [
      x 600.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "melibiose galactohydrolase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re9"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 1951.75
      minerva_y 2511.25
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re9"
      reaction_meta_id "re9"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 425
    zlevel -1

    graphics [
      x 700.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "UDP-galactose-4-epimerase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re5"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 3060.5000000000055
      minerva_y 2343.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re5"
      reaction_meta_id "re5"
      reaction_non_rdf_annotation ""
      reversible "true"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 429
    zlevel -1

    graphics [
      x 800.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NAD-dependent deacetylase sirtuin-5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re32"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 1562.5000000000011
      minerva_y 1137.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re32"
      reaction_meta_id "re32"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 438
    zlevel -1

    graphics [
      x 900.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "GTP diphosphohydrolase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re67"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 5642.9999999999945
      minerva_y 2012.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re67"
      reaction_meta_id "re67"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 447
    zlevel -1

    graphics [
      x 1000.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ribose-phosphate pyrophosphokinase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re40"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 950.1048160490641
      minerva_y 1010.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re40"
      reaction_meta_id "re40"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 455
    zlevel -1

    graphics [
      x 1100.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Adenosine aminohydrolase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re80"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 4611.0
      minerva_y 1059.4999999999998
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re80"
      reaction_meta_id "re80"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 462
    zlevel -1

    graphics [
      x 1200.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nicotinamide ribonucleotide phosphohydrolase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re26"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 1285.8549260746836
      minerva_y 1483.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re26"
      reaction_meta_id "re26"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 468
    zlevel -1

    graphics [
      x 200.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "galactose mutarotase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 2288.1875
      minerva_y 2342.75
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1"
      reaction_meta_id "re1"
      reaction_non_rdf_annotation ""
      reversible "true"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 472
    zlevel -1

    graphics [
      x 300.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Guanine deaminase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re77"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 4451.900198977592
      minerva_y 1524.7041143242704
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re77"
      reaction_meta_id "re77"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 479
    zlevel -1

    graphics [
      x 400.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "beta-1,4-galactosyltransferase 1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re17"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 2794.0
      minerva_y 2511.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re17"
      reaction_meta_id "re17"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 485
    zlevel -1

    graphics [
      x 500.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Deoxyguanosine phosphorylase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re61"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 4268.5
      minerva_y 1605.0000000000002
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re61"
      reaction_meta_id "re61"
      reaction_non_rdf_annotation ""
      reversible "true"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 491
    zlevel -1

    graphics [
      x 600.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nicotinamide-nucleotide adenylyltransferase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re23"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 1215.0647699319554
      minerva_y 1748.3667891818948
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re23"
      reaction_meta_id "re23"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 500
    zlevel -1

    graphics [
      x 700.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re38"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 892.6157222057138
      minerva_y 1129.4818239601666
    ]
    sbml [
      reaction_id "re38"
      reaction_meta_id "re38"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 503
    zlevel -1

    graphics [
      x 800.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Lactose galactohydrolase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re7"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 2373.09375
      minerva_y 2511.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re7"
      reaction_meta_id "re7"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 510
    zlevel -1

    graphics [
      x 900.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "phosphoribosylaminoimidazole-succinocarboxamide synthase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re47"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 3956.5
      minerva_y 751.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re47"
      reaction_meta_id "re47"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 519
    zlevel -1

    graphics [
      x 1000.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Adenosine kinase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re81"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 4234.75
      minerva_y 992.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re81"
      reaction_meta_id "re81"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 526
    zlevel -1

    graphics [
      x 1100.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "NAD(P) transhydrogenase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re21"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 1994.0000000000002
      minerva_y 1838.4999999999993
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re21"
      reaction_meta_id "re21"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 532
    zlevel -1

    graphics [
      x 1200.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "galactitol:NAD+ 1-oxidoreductase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re8"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 2020.09375
      minerva_y 2054.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re8"
      reaction_meta_id "re8"
      reaction_non_rdf_annotation ""
      reversible "true"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 539
    zlevel -1

    graphics [
      x 200.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "GDP phosphohydrolase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re68"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 5423.9999999999345
      minerva_y 1903.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re68"
      reaction_meta_id "re68"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 550
    zlevel -1

    graphics [
      x 300.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "GMP reductase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re69"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 5639.999999999923
      minerva_y 1490.6794445211622
    ]
    sbml [
      reaction_id "re69"
      reaction_meta_id "re69"
      reaction_non_rdf_annotation ""
      reversible "true"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 560
    zlevel -1

    graphics [
      x 400.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re108"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "Positive influence"
      minerva_x 5026.5
      minerva_y 1266.5
    ]
    sbml [
      reaction_id "re108"
      reaction_meta_id "re108"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 563
    zlevel -1

    graphics [
      x 500.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ATP:N-ribosylnicotinamide 5'-phosphotransferase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re25"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 1213.1450739253164
      minerva_y 1483.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re25"
      reaction_meta_id "re25"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 570
    zlevel -1

    graphics [
      x 600.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nicotinamide phosphoribosyltransferase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re29"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 1035.0966920888409
      minerva_y 1580.4636479203332
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re29"
      reaction_meta_id "re29"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 576
    zlevel -1

    graphics [
      x 700.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re104"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "Heterodimer association"
      minerva_x 2076.0
      minerva_y 987.9249999999997
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re104"
      reaction_meta_id "re104"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 580
    zlevel -1

    graphics [
      x 800.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "inosine 5'-monophosphate phosphohydrolase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re73"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 5090.0
      minerva_y 1187.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re73"
      reaction_meta_id "re73"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 586
    zlevel -1

    graphics [
      x 900.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "nicotinate phosphoribosyltransferase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re37"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 550.5
      minerva_y 1351.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re37"
      reaction_meta_id "re37"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 596
    zlevel -1

    graphics [
      x 1000.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ATP:dGDP phosphotransferase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re57"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 4640.0
      minerva_y 2118.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re57"
      reaction_meta_id "re57"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 606
    zlevel -1

    graphics [
      x 1100.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Stachyose galactohydrolase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re18"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 1553.5
      minerva_y 2345.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re18"
      reaction_meta_id "re18"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 614
    zlevel -1

    graphics [
      x 1200.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "phosphodeoxyribomutase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re39"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 773.0
      minerva_y 755.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re39"
      reaction_meta_id "re39"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 618
    zlevel -1

    graphics [
      x 200.0
      y 2200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "2'-Deoxyguanosine 5'-triphosphate diphosphohydrolase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re62"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 4497.0000000000055
      minerva_y 2013.4811634655598
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re62"
      reaction_meta_id "re62"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 627
    zlevel -1

    graphics [
      x 300.0
      y 2200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Raffinose galactohydrolase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re16"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 1949.84375
      minerva_y 2344.125
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re16"
      reaction_meta_id "re16"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 635
    zlevel -1

    graphics [
      x 400.0
      y 2200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "IMP cyclohydrolase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re50"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 5294.5
      minerva_y 974.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re50"
      reaction_meta_id "re50"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 640
    zlevel -1

    graphics [
      x 500.0
      y 2200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ATP:dADP phosphotransferase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re90"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 3037.0
      minerva_y 1167.25
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re90"
      reaction_meta_id "re90"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 650
    zlevel -1

    graphics [
      x 600.0
      y 2200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "GMP:diphosphate 5-phospho-alpha-D-ribosyltransferase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re63"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 4970.868330686373
      minerva_y 1814.141110957675
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re63"
      reaction_meta_id "re63"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 656
    zlevel -1

    graphics [
      x 700.0
      y 2200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "galactose-1-phosphate uridylyltransferase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re3"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 2762.78125
      minerva_y 2342.875
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re3"
      reaction_meta_id "re3"
      reaction_non_rdf_annotation ""
      reversible "true"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 662
    zlevel -1

    graphics [
      x 800.0
      y 2200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re52"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 5292.0
      minerva_y 1314.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re52"
      reaction_meta_id "re52"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 675
    zlevel -1

    graphics [
      x 900.0
      y 2200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "GDP reductase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re56"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 4953.5
      minerva_y 2001.75
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re56"
      reaction_meta_id "re56"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 682
    zlevel -1

    graphics [
      x 1000.0
      y 2200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "hypoxanthine-guanine phosphoribosyltransferase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re78"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 4842.655068580758
      minerva_y 1144.4999999999845
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re78"
      reaction_meta_id "re78"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 688
    zlevel -1

    graphics [
      x 1100.0
      y 2200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "phosphoribosylaminoimidazole carboxylase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re46"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 3407.5
      minerva_y 751.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re46"
      reaction_meta_id "re46"
      reaction_non_rdf_annotation ""
      reversible "true"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 695
    zlevel -1

    graphics [
      x 1200.0
      y 2200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ATP:deoxyadenosine 5'-phosphotransferase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re88"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 3517.0
      minerva_y 1411.75
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re88"
      reaction_meta_id "re88"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 702
    zlevel -1

    graphics [
      x 200.0
      y 2300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "Guanylate kinase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re54"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 5293.25
      minerva_y 1903.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re54"
      reaction_meta_id "re54"
      reaction_non_rdf_annotation ""
      reversible "true"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 708
    zlevel -1

    graphics [
      x 300.0
      y 2300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "10-formyltetrahydrofolate:5'-phosphoribosylglycinamide formyltransferase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re43"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 1807.5
      minerva_y 752.25
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re43"
      reaction_meta_id "re43"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 715
    zlevel -1

    graphics [
      x 400.0
      y 2300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "5'-Phosphoribosylformylglycinamide:L-glutamine amido-ligase "
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re44"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 2350.0
      minerva_y 752.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re44"
      reaction_meta_id "re44"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 726
    zlevel -1

    graphics [
      x 500.0
      y 2300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "guanosine:phosphate alpha-D-ribosyltransferase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re64"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 4658.461939766255
      minerva_y 1597.6920022107895
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re64"
      reaction_meta_id "re64"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 732
    zlevel -1

    graphics [
      x 600.0
      y 2300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "2'-Deoxyadenosine 5'-diphosphate:oxidized-thioredoxin 2'-oxidoreductase"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re91"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/6022"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "PUBCHEM__6022"
      minerva_ref_type__resource2 "CHEBI__CHEBI:16761"
      minerva_type "State transition"
      minerva_x 3378.5625
      minerva_y 1045.0625
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re91"
      reaction_meta_id "re91"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  edge [
    id 168
    source 21
    target 167
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa70"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 169
    source 5
    target 167
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa150"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 170
    source 167
    target 36
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa106"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 171
    source 167
    target 9
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa228"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 172
    source 167
    target 1
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa103"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 173
    source 15
    target 167
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR0"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa75"
    ]
  ]
  edge [
    id 175
    source 151
    target 174
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa313"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 176
    source 23
    target 174
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa347"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 177
    source 174
    target 92
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa317"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 178
    source 174
    target 43
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa271"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 179
    source 2
    target 174
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR1"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa358"
    ]
  ]
  edge [
    id 181
    source 50
    target 180
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa68"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 182
    source 23
    target 180
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa347"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 183
    source 180
    target 3
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa66"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 184
    source 180
    target 43
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa271"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 185
    source 2
    target 180
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR2"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa358"
    ]
  ]
  edge [
    id 187
    source 139
    target 186
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa359"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 188
    source 4
    target 186
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa315"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 189
    source 1
    target 186
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa103"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 190
    source 186
    target 57
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa355"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 191
    source 186
    target 6
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa307"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 192
    source 132
    target 186
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR3"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa363"
    ]
  ]
  edge [
    id 194
    source 93
    target 193
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa248"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 195
    source 5
    target 193
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa150"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 196
    source 193
    target 113
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa238"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 197
    source 193
    target 9
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa228"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 198
    source 58
    target 193
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR4"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa226"
    ]
  ]
  edge [
    id 200
    source 134
    target 199
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa308"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 201
    source 23
    target 199
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa347"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 202
    source 199
    target 152
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa309"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 203
    source 199
    target 43
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa271"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 204
    source 2
    target 199
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR5"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa358"
    ]
  ]
  edge [
    id 206
    source 27
    target 205
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa152"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 207
    source 4
    target 205
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa315"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 208
    source 205
    target 161
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa333"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 209
    source 205
    target 23
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa347"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 210
    source 14
    target 205
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR6"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa275"
    ]
  ]
  edge [
    id 212
    source 11
    target 211
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa229"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 213
    source 4
    target 211
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa315"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 214
    source 211
    target 79
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa225"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 215
    source 211
    target 1
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa103"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 216
    source 211
    target 23
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa347"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 217
    source 7
    target 211
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR7"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa277"
    ]
  ]
  edge [
    id 218
    source 98
    target 211
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR8"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa283"
    ]
  ]
  edge [
    id 220
    source 29
    target 219
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa189"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 221
    source 219
    target 86
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa195"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 222
    source 219
    target 155
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa196"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 223
    source 47
    target 219
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR9"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa197"
    ]
  ]
  edge [
    id 225
    source 21
    target 224
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa70"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 226
    source 4
    target 224
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa315"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 227
    source 224
    target 33
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa69"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 228
    source 224
    target 27
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa152"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 229
    source 51
    target 224
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR10"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa94"
    ]
  ]
  edge [
    id 230
    source 80
    target 224
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR11"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa261"
    ]
  ]
  edge [
    id 232
    source 112
    target 231
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa72"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 233
    source 1
    target 231
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa103"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 234
    source 5
    target 231
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa150"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 235
    source 231
    target 39
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa71"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 236
    source 231
    target 31
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa223"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 237
    source 54
    target 231
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR12"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa92"
    ]
  ]
  edge [
    id 238
    source 82
    target 231
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR13"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa91"
    ]
  ]
  edge [
    id 239
    source 35
    target 231
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR14"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa93"
    ]
  ]
  edge [
    id 241
    source 111
    target 240
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa346"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 242
    source 28
    target 240
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa351"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 243
    source 240
    target 27
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa152"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 244
    source 240
    target 31
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa223"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 245
    source 87
    target 240
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR15"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa350"
    ]
  ]
  edge [
    id 247
    source 63
    target 246
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa215"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 248
    source 4
    target 246
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa315"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 249
    source 246
    target 136
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa257"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 250
    source 246
    target 23
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa347"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 251
    source 14
    target 246
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR16"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa275"
    ]
  ]
  edge [
    id 253
    source 94
    target 252
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa131"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 254
    source 28
    target 252
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa351"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 255
    source 1
    target 252
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa103"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 257
    source 252
    target 112
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa72"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 258
    source 252
    target 31
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa223"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 259
    source 252
    target 38
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa136"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 260
    source 34
    target 252
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR17"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa132"
    ]
  ]
  edge [
    id 262
    source 13
    target 261
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa35"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 263
    source 46
    target 261
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa55"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 264
    source 261
    target 158
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa7"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 266
    source 109
    target 265
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa2"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 267
    source 5
    target 265
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa150"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 268
    source 265
    target 22
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa4"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 269
    source 265
    target 9
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa228"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 270
    source 138
    target 265
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR18"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa5"
    ]
  ]
  edge [
    id 272
    source 28
    target 271
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa351"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 273
    source 4
    target 271
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa315"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 274
    source 26
    target 271
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa218"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 275
    source 271
    target 41
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa153"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 276
    source 271
    target 8
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa154"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 277
    source 271
    target 31
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa223"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 278
    source 162
    target 271
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR19"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa158"
    ]
  ]
  edge [
    id 280
    source 21
    target 279
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa70"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 281
    source 4
    target 279
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa315"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 282
    source 279
    target 3
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa66"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 283
    source 279
    target 1
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa103"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 284
    source 279
    target 130
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa124"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 285
    source 65
    target 279
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR20"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa144"
    ]
  ]
  edge [
    id 287
    source 75
    target 286
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa204"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 288
    source 4
    target 286
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa315"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 289
    source 5
    target 286
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa150"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 290
    source 26
    target 286
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa218"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 291
    source 286
    target 63
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa215"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 292
    source 286
    target 31
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa223"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 293
    source 286
    target 27
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa152"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 294
    source 286
    target 8
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa154"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 295
    source 286
    target 1
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa103"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 297
    source 125
    target 286
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR21"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa216"
    ]
  ]
  edge [
    id 299
    source 150
    target 298
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa403"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 300
    source 298
    target 12
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa412"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 302
    source 86
    target 301
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa195"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 303
    source 117
    target 301
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa167"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 304
    source 301
    target 104
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa198"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 305
    source 301
    target 72
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa200"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 306
    source 42
    target 301
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR22"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa199"
    ]
  ]
  edge [
    id 308
    source 133
    target 307
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa137"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 309
    source 36
    target 307
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa106"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 310
    source 307
    target 3
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa66"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 311
    source 307
    target 25
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa107"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 312
    source 65
    target 307
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR23"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa144"
    ]
  ]
  edge [
    id 314
    source 70
    target 313
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa170"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 315
    source 5
    target 313
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa150"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 316
    source 313
    target 20
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa179"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 317
    source 313
    target 23
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa347"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 318
    source 313
    target 1
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa103"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 319
    source 313
    target 9
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa228"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 320
    source 30
    target 313
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR24"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa160"
    ]
  ]
  edge [
    id 322
    source 92
    target 321
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa317"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 323
    source 124
    target 321
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa357"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 324
    source 321
    target 57
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa355"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 325
    source 321
    target 23
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa347"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 326
    source 2
    target 321
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR25"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa358"
    ]
  ]
  edge [
    id 328
    source 13
    target 327
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa35"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 329
    source 121
    target 327
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa209"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 330
    source 327
    target 153
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa8"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 332
    source 27
    target 331
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa152"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 333
    source 5
    target 331
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa150"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 334
    source 331
    target 9
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa228"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 336
    source 81
    target 331
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR26"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa370"
    ]
  ]
  edge [
    id 337
    source 40
    target 331
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR27"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa391"
    ]
  ]
  edge [
    id 338
    source 145
    target 331
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR28"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa392"
    ]
  ]
  edge [
    id 339
    source 17
    target 331
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR29"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa390"
    ]
  ]
  edge [
    id 341
    source 75
    target 340
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa204"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 342
    source 4
    target 340
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa315"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 343
    source 340
    target 134
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa308"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 344
    source 340
    target 23
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa347"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 345
    source 14
    target 340
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR30"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa275"
    ]
  ]
  edge [
    id 347
    source 83
    target 346
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa251"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 348
    source 5
    target 346
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa150"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 349
    source 346
    target 93
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa248"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 350
    source 346
    target 9
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa228"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 351
    source 346
    target 1
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa103"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 352
    source 24
    target 346
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR31"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa255"
    ]
  ]
  edge [
    id 354
    source 39
    target 353
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa71"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 355
    source 5
    target 353
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa150"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 356
    source 26
    target 353
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa218"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 357
    source 4
    target 353
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa315"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 358
    source 353
    target 21
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa70"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 359
    source 353
    target 9
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa228"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 360
    source 353
    target 1
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa103"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 361
    source 353
    target 8
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa154"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 362
    source 16
    target 353
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR32"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa84"
    ]
  ]
  edge [
    id 364
    source 27
    target 363
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa152"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 365
    source 1
    target 363
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa103"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 366
    source 4
    target 363
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa315"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 367
    source 363
    target 143
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa202"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 368
    source 363
    target 6
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa307"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 369
    source 107
    target 363
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR33"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa305"
    ]
  ]
  edge [
    id 370
    source 149
    target 363
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR34"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa304"
    ]
  ]
  edge [
    id 371
    source 114
    target 363
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR35"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa306"
    ]
  ]
  edge [
    id 373
    source 49
    target 372
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa364"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 374
    source 5
    target 372
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa150"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 375
    source 372
    target 18
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa369"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 376
    source 372
    target 9
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa228"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 377
    source 81
    target 372
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR36"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa370"
    ]
  ]
  edge [
    id 379
    source 118
    target 378
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa10"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 380
    source 31
    target 378
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa223"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 381
    source 378
    target 146
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa11"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 382
    source 378
    target 102
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa13"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 383
    source 44
    target 378
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR37"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa12"
    ]
  ]
  edge [
    id 385
    source 161
    target 384
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa333"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 386
    source 23
    target 384
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa347"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 387
    source 384
    target 111
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa346"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 388
    source 384
    target 43
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa271"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 389
    source 2
    target 384
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR38"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa358"
    ]
  ]
  edge [
    id 391
    source 92
    target 390
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa317"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 392
    source 21
    target 390
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa70"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 393
    source 4
    target 390
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa315"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 394
    source 390
    target 152
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa309"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 395
    source 390
    target 1
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa103"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 396
    source 390
    target 129
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa79"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 397
    source 53
    target 390
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR39"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa321"
    ]
  ]
  edge [
    id 399
    source 79
    target 398
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa225"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 400
    source 5
    target 398
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa150"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 401
    source 398
    target 11
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa229"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 402
    source 398
    target 9
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa228"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 403
    source 90
    target 398
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR40"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa232"
    ]
  ]
  edge [
    id 404
    source 19
    target 398
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR41"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa2"
    ]
  ]
  edge [
    id 405
    source 59
    target 398
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR42"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa235"
    ]
  ]
  edge [
    id 406
    source 45
    target 398
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR43"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa382"
    ]
  ]
  edge [
    id 407
    source 89
    target 398
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR44"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa383"
    ]
  ]
  edge [
    id 409
    source 41
    target 408
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa153"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 410
    source 5
    target 408
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa150"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 411
    source 60
    target 408
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa162"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 412
    source 408
    target 123
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa159"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 413
    source 408
    target 9
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa228"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 414
    source 408
    target 1
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa103"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 415
    source 408
    target 23
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa347"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 416
    source 30
    target 408
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR45"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa160"
    ]
  ]
  edge [
    id 418
    source 163
    target 417
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 419
    source 4
    target 417
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa315"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 420
    source 417
    target 76
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa21"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 421
    source 417
    target 137
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa1"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 422
    source 46
    target 417
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR46"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa55"
    ]
  ]
  edge [
    id 423
    source 158
    target 417
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR47"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa7"
    ]
  ]
  edge [
    id 424
    source 97
    target 417
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR48"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa58"
    ]
  ]
  edge [
    id 426
    source 118
    target 425
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa10"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 427
    source 425
    target 101
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa8"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 428
    source 140
    target 425
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR49"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa19"
    ]
  ]
  edge [
    id 430
    source 21
    target 429
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa70"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 431
    source 119
    target 429
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa115"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 432
    source 142
    target 429
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa119"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 433
    source 429
    target 3
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa66"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 434
    source 429
    target 48
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa117"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 435
    source 429
    target 110
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa116"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 436
    source 150
    target 429
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR50"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa403"
    ]
  ]
  edge [
    id 437
    source 120
    target 429
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR51"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa6"
    ]
  ]
  edge [
    id 439
    source 11
    target 438
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa229"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 440
    source 4
    target 438
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa315"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 441
    source 438
    target 63
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa215"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 442
    source 438
    target 23
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa347"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 444
    source 438
    target 1
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa103"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 446
    source 7
    target 438
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR52"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa277"
    ]
  ]
  edge [
    id 448
    source 99
    target 447
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa146"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 449
    source 5
    target 447
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa150"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 450
    source 447
    target 28
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa351"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 451
    source 447
    target 27
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa152"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 452
    source 141
    target 447
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR53"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa148"
    ]
  ]
  edge [
    id 453
    source 122
    target 447
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR54"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa149"
    ]
  ]
  edge [
    id 454
    source 128
    target 447
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR55"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa151"
    ]
  ]
  edge [
    id 456
    source 161
    target 455
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa333"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 457
    source 4
    target 455
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa315"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 458
    source 1
    target 455
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa103"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 459
    source 455
    target 151
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa313"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 460
    source 455
    target 6
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa307"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 461
    source 132
    target 455
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR56"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa363"
    ]
  ]
  edge [
    id 463
    source 33
    target 462
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa69"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 464
    source 4
    target 462
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa315"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 465
    source 462
    target 50
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa68"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 466
    source 462
    target 23
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa347"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 467
    source 14
    target 462
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR57"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa275"
    ]
  ]
  edge [
    id 469
    source 137
    target 468
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa1"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 470
    source 468
    target 109
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa2"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 471
    source 74
    target 468
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR58"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa3"
    ]
  ]
  edge [
    id 473
    source 52
    target 472
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa256"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 474
    source 4
    target 472
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa315"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 475
    source 1
    target 472
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa103"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 476
    source 472
    target 152
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa309"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 477
    source 472
    target 6
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa307"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 478
    source 108
    target 472
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR59"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa326"
    ]
  ]
  edge [
    id 480
    source 101
    target 479
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa8"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 481
    source 76
    target 479
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa21"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 482
    source 479
    target 164
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa20"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 483
    source 479
    target 103
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa22"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 484
    source 67
    target 479
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR60"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa1"
    ]
  ]
  edge [
    id 486
    source 83
    target 485
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa251"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 487
    source 23
    target 485
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa347"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 488
    source 485
    target 52
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa256"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 489
    source 485
    target 124
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa357"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 490
    source 2
    target 485
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR61"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa358"
    ]
  ]
  edge [
    id 492
    source 33
    target 491
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa69"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 493
    source 5
    target 491
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa150"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 494
    source 1
    target 491
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa103"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 495
    source 491
    target 21
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa70"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 496
    source 491
    target 31
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa223"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 497
    source 82
    target 491
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR62"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa91"
    ]
  ]
  edge [
    id 498
    source 54
    target 491
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR63"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa92"
    ]
  ]
  edge [
    id 499
    source 35
    target 491
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR64"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa93"
    ]
  ]
  edge [
    id 501
    source 133
    target 500
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa137"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 502
    source 500
    target 3
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa66"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 504
    source 164
    target 503
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa20"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 505
    source 4
    target 503
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa315"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 506
    source 503
    target 137
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa1"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 507
    source 503
    target 76
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa21"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 508
    source 84
    target 503
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR65"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa24"
    ]
  ]
  edge [
    id 509
    source 144
    target 503
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR66"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa57"
    ]
  ]
  edge [
    id 511
    source 165
    target 510
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa184"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 512
    source 5
    target 510
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa150"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 513
    source 154
    target 510
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa194"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 514
    source 510
    target 29
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa189"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 515
    source 510
    target 9
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa228"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 516
    source 510
    target 1
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa103"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 517
    source 510
    target 23
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa347"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 518
    source 91
    target 510
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR67"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa185"
    ]
  ]
  edge [
    id 520
    source 161
    target 519
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa333"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 521
    source 5
    target 519
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa150"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 522
    source 519
    target 27
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa152"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 523
    source 519
    target 9
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa228"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 524
    source 519
    target 1
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa103"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 525
    source 69
    target 519
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR68"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa342"
    ]
  ]
  edge [
    id 527
    source 21
    target 526
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa70"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 528
    source 61
    target 526
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa295"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 529
    source 526
    target 129
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa79"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 530
    source 526
    target 36
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa106"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 531
    source 106
    target 526
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR69"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa80"
    ]
  ]
  edge [
    id 533
    source 56
    target 532
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa27"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 534
    source 36
    target 532
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa106"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 535
    source 532
    target 137
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa1"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 536
    source 532
    target 61
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa295"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 537
    source 532
    target 1
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa103"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 538
    source 66
    target 532
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR70"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa28"
    ]
  ]
  edge [
    id 540
    source 79
    target 539
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa225"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 541
    source 4
    target 539
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa315"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 542
    source 539
    target 63
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa215"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 543
    source 539
    target 1
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa103"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 544
    source 539
    target 23
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa347"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 545
    source 7
    target 539
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR71"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa277"
    ]
  ]
  edge [
    id 546
    source 131
    target 539
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR72"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa290"
    ]
  ]
  edge [
    id 547
    source 73
    target 539
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR73"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa291"
    ]
  ]
  edge [
    id 548
    source 85
    target 539
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR74"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa292"
    ]
  ]
  edge [
    id 549
    source 78
    target 539
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR75"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa293"
    ]
  ]
  edge [
    id 551
    source 143
    target 550
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa202"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 552
    source 6
    target 550
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa307"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 553
    source 36
    target 550
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa106"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 554
    source 550
    target 63
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa215"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 555
    source 550
    target 61
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa295"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 556
    source 550
    target 1
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa103"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 558
    source 159
    target 550
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR76"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa299"
    ]
  ]
  edge [
    id 559
    source 10
    target 550
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR77"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa300"
    ]
  ]
  edge [
    id 561
    source 121
    target 560
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa209"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 562
    source 560
    target 156
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa411"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 564
    source 50
    target 563
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa68"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 565
    source 5
    target 563
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa150"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 566
    source 563
    target 33
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa69"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 567
    source 563
    target 9
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa228"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 568
    source 563
    target 1
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa103"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 569
    source 116
    target 563
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR78"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa104"
    ]
  ]
  edge [
    id 571
    source 3
    target 570
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa66"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 572
    source 28
    target 570
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa351"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 573
    source 570
    target 33
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa69"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 574
    source 570
    target 31
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa223"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 575
    source 148
    target 570
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR79"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa108"
    ]
  ]
  edge [
    id 577
    source 150
    target 576
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa403"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 578
    source 68
    target 576
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa120"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 579
    source 576
    target 120
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa6"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 581
    source 143
    target 580
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa202"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 582
    source 4
    target 580
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa315"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 583
    source 580
    target 151
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa313"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 584
    source 580
    target 23
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa347"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 585
    source 14
    target 580
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR80"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa275"
    ]
  ]
  edge [
    id 587
    source 133
    target 586
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa137"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 588
    source 28
    target 586
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa351"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 589
    source 4
    target 586
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa315"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 590
    source 5
    target 586
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa150"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 591
    source 586
    target 112
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa72"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 592
    source 586
    target 9
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa228"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 593
    source 586
    target 31
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa223"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 594
    source 586
    target 23
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa347"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 595
    source 88
    target 586
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR81"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa138"
    ]
  ]
  edge [
    id 597
    source 113
    target 596
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa238"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 598
    source 5
    target 596
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa150"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 599
    source 596
    target 96
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa245"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 600
    source 596
    target 9
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa228"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 601
    source 90
    target 596
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR82"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa232"
    ]
  ]
  edge [
    id 602
    source 19
    target 596
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR83"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa2"
    ]
  ]
  edge [
    id 603
    source 89
    target 596
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR84"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa383"
    ]
  ]
  edge [
    id 604
    source 45
    target 596
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR85"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa382"
    ]
  ]
  edge [
    id 605
    source 59
    target 596
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR86"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa235"
    ]
  ]
  edge [
    id 607
    source 160
    target 606
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa53"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 608
    source 4
    target 606
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa315"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 609
    source 606
    target 32
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa49"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 610
    source 606
    target 137
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa1"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 611
    source 46
    target 606
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR87"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa55"
    ]
  ]
  edge [
    id 612
    source 158
    target 606
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR88"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa7"
    ]
  ]
  edge [
    id 613
    source 97
    target 606
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR89"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa58"
    ]
  ]
  edge [
    id 615
    source 62
    target 614
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa145"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 616
    source 614
    target 99
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa146"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 617
    source 115
    target 614
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR90"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa147"
    ]
  ]
  edge [
    id 619
    source 96
    target 618
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa245"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 620
    source 4
    target 618
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa315"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 621
    source 618
    target 93
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa248"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 622
    source 618
    target 31
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa223"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 623
    source 618
    target 1
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa103"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 624
    source 51
    target 618
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR91"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa94"
    ]
  ]
  edge [
    id 625
    source 80
    target 618
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR92"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa261"
    ]
  ]
  edge [
    id 626
    source 77
    target 618
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR93"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa264"
    ]
  ]
  edge [
    id 628
    source 32
    target 627
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa49"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 629
    source 4
    target 627
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa315"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 630
    source 627
    target 137
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa1"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 631
    source 627
    target 71
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa51"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 632
    source 46
    target 627
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR94"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa55"
    ]
  ]
  edge [
    id 633
    source 158
    target 627
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR95"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa7"
    ]
  ]
  edge [
    id 634
    source 97
    target 627
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR96"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa58"
    ]
  ]
  edge [
    id 636
    source 104
    target 635
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa198"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 637
    source 635
    target 143
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa202"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 638
    source 635
    target 4
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa315"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 639
    source 42
    target 635
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR97"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa199"
    ]
  ]
  edge [
    id 641
    source 18
    target 640
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa369"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 642
    source 5
    target 640
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa150"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 643
    source 640
    target 157
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa373"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 644
    source 640
    target 9
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa228"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 645
    source 19
    target 640
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR98"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa2"
    ]
  ]
  edge [
    id 646
    source 59
    target 640
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR99"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa235"
    ]
  ]
  edge [
    id 647
    source 90
    target 640
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR100"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa232"
    ]
  ]
  edge [
    id 648
    source 45
    target 640
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR101"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa382"
    ]
  ]
  edge [
    id 649
    source 89
    target 640
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR102"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa383"
    ]
  ]
  edge [
    id 651
    source 52
    target 650
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa256"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 652
    source 28
    target 650
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa351"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 653
    source 650
    target 63
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa215"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 654
    source 650
    target 31
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa223"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 655
    source 105
    target 650
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR103"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa332"
    ]
  ]
  edge [
    id 657
    source 22
    target 656
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa4"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 658
    source 118
    target 656
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa10"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 659
    source 656
    target 101
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa8"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 660
    source 656
    target 146
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa11"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 661
    source 55
    target 656
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR104"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa9"
    ]
  ]
  edge [
    id 663
    source 143
    target 662
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa202"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 664
    source 21
    target 662
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa70"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 665
    source 4
    target 662
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa315"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 666
    source 662
    target 75
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa204"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 667
    source 662
    target 129
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa79"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 668
    source 662
    target 1
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa103"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 669
    source 100
    target 662
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR105"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa210"
    ]
  ]
  edge [
    id 670
    source 121
    target 662
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR106"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa209"
    ]
  ]
  edge [
    id 671
    source 153
    target 662
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR107"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa8"
    ]
  ]
  edge [
    id 672
    source 166
    target 662
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR108"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa213"
    ]
  ]
  edge [
    id 673
    source 147
    target 662
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR109"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa212"
    ]
  ]
  edge [
    id 674
    source 126
    target 662
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR110"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa214"
    ]
  ]
  edge [
    id 676
    source 79
    target 675
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa225"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 677
    source 127
    target 675
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa376"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 678
    source 675
    target 113
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa238"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 679
    source 675
    target 64
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa241"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 680
    source 675
    target 4
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa315"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 681
    source 95
    target 675
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR111"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa3"
    ]
  ]
  edge [
    id 683
    source 92
    target 682
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa317"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 684
    source 28
    target 682
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa351"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 685
    source 682
    target 143
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa202"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 686
    source 682
    target 31
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa223"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 687
    source 105
    target 682
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR112"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa332"
    ]
  ]
  edge [
    id 689
    source 20
    target 688
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa179"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 690
    source 38
    target 688
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa136"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 691
    source 688
    target 165
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa184"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 692
    source 688
    target 1
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa103"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 694
    source 91
    target 688
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR113"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa185"
    ]
  ]
  edge [
    id 696
    source 139
    target 695
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa359"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 697
    source 5
    target 695
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa150"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 698
    source 695
    target 49
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa364"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 699
    source 695
    target 9
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa228"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 700
    source 695
    target 1
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa103"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 701
    source 24
    target 695
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR114"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa255"
    ]
  ]
  edge [
    id 703
    source 63
    target 702
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa215"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 704
    source 5
    target 702
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa150"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 705
    source 702
    target 79
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa225"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 706
    source 702
    target 9
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "both"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa228"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 707
    source 58
    target 702
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR115"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa226"
    ]
  ]
  edge [
    id 709
    source 123
    target 708
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa159"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 710
    source 117
    target 708
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa167"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 711
    source 708
    target 37
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa166"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 712
    source 708
    target 72
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa200"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 713
    source 708
    target 1
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa103"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 714
    source 30
    target 708
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR116"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa160"
    ]
  ]
  edge [
    id 716
    source 37
    target 715
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa166"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 717
    source 26
    target 715
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa218"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 718
    source 5
    target 715
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa150"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 719
    source 4
    target 715
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa315"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 720
    source 715
    target 70
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa170"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 721
    source 715
    target 8
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa154"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 722
    source 715
    target 23
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa347"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 723
    source 715
    target 1
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa103"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 724
    source 715
    target 9
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa228"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 725
    source 135
    target 715
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR117"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa171"
    ]
  ]
  edge [
    id 727
    source 136
    target 726
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa257"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 728
    source 23
    target 726
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa347"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 729
    source 726
    target 52
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa256"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 730
    source 726
    target 43
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa271"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 731
    source 2
    target 726
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR118"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa358"
    ]
  ]
  edge [
    id 733
    source 9
    target 732
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa228"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 734
    source 127
    target 732
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa376"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 735
    source 732
    target 18
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa369"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 736
    source 732
    target 64
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa241"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 737
    source 732
    target 4
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa315"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 738
    source 95
    target 732
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR119"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa3"
    ]
  ]
]
