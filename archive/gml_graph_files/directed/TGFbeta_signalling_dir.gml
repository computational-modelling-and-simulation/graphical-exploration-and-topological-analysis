# generated with VANTED V2.8.0 at Tue Apr 27 20:00:51 CEST 2021
graph [
  background_coloring "true"
  cluster_colors "255,128,128,255:0,0,0,255;128,255,128,255:0,0,0,255;128,128,255,255:0,0,0,255"
  clusterbackground_fill_outer_region "false"
  clusterbackground_grid 50.0
  clusterbackground_low_alpha 0.2
  clusterbackground_radius 200.0
  clusterbackground_space_fill "true"
  graphbackgroundcolor "#ffffff"
  sbml [
    model_meta_id "untitled"
    model_non_rdf_annotation ""
    model_notes ""
    model_sbml_id "untitled"
    namespace "xmlns:celldesigner=\"http://www.sbml.org/2001/ns/celldesigner\" "
    sbml_level "2"
    sbml_version "4"
  ]
  sbml_compartment_default [
    sbml_compartment_default_id "default"
    sbml_compartment_default_meta_id "default"
    sbml_compartment_default_non_rdf_annotation ""
    sbml_compartment_default_notes ""
    sbml_compartment_default_size "1.0"
    sbml_compartment_default_units "volume"
  ]
  sbml_compartment_s_id_ca1 [
    sbml_compartment_s_id_ca1_annotation ""
    sbml_compartment_s_id_ca1_id "s_id_ca1"
    sbml_compartment_s_id_ca1_meta_id "s_id_ca1"
    sbml_compartment_s_id_ca1_name "cell"
    sbml_compartment_s_id_ca1_non_rdf_annotation ""
    sbml_compartment_s_id_ca1_notes ""
    sbml_compartment_s_id_ca1_outside "default"
    sbml_compartment_s_id_ca1_size "1.0"
    sbml_compartment_s_id_ca1_units "volume"
  ]
  sbml_compartment_s_id_ca2 [
    sbml_compartment_s_id_ca2_annotation ""
    sbml_compartment_s_id_ca2_id "s_id_ca2"
    sbml_compartment_s_id_ca2_meta_id "s_id_ca2"
    sbml_compartment_s_id_ca2_name "nucleus"
    sbml_compartment_s_id_ca2_non_rdf_annotation ""
    sbml_compartment_s_id_ca2_notes ""
    sbml_compartment_s_id_ca2_outside "s_id_ca1"
    sbml_compartment_s_id_ca2_size "1.0"
    sbml_compartment_s_id_ca2_units "volume"
  ]
  sbml_unit_definition_1 [
    sbml_unit_definition_1_id "volume"
    sbml_unit_definition_1_name "volume"
    sbml_unit_definition_1_sub_unit_1_ "(1.0 * 10^0 * litre)^1.0"
    sbml_unit_definition_1unit "(1.0 * 10^0 * litre)^1.0"
  ]
  sbml_unit_definition_2 [
    sbml_unit_definition_2_id "substance"
    sbml_unit_definition_2_name "substance"
    sbml_unit_definition_2_sub_unit_1_ "(1.0 * 10^0 * mole)^1.0"
    sbml_unit_definition_2unit "(1.0 * 10^0 * mole)^1.0"
  ]
  sbml_unit_definition_3 [
    sbml_unit_definition_3_id "area"
    sbml_unit_definition_3_name "area"
    sbml_unit_definition_3_sub_unit_1_ "(1.0 * 10^0 * metre)^2.0"
    sbml_unit_definition_3unit "(1.0 * 10^0 * metre)^2.0"
  ]
  sbml_unit_definition_4 [
    sbml_unit_definition_4_id "time"
    sbml_unit_definition_4_name "time"
    sbml_unit_definition_4_sub_unit_1_ "(1.0 * 10^0 * second)^1.0"
    sbml_unit_definition_4unit "(1.0 * 10^0 * second)^1.0"
  ]
  sbml_unit_definition_5 [
    sbml_unit_definition_5_id "length"
    sbml_unit_definition_5_name "length"
    sbml_unit_definition_5_sub_unit_1_ "(1.0 * 10^0 * metre)^1.0"
    sbml_unit_definition_5unit "(1.0 * 10^0 * metre)^1.0"
  ]
  directed 1
  node [
    id 1
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "rhoa__cell__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa38"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa38"
      minerva_former_symbols "ARH12; ARHA"
      minerva_name "RHOA"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/667"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=RHOA"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_001664"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000067560"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.6.5.2"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/387"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/387"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P61586"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P61586"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=RHOA"
      minerva_ref_type__resource1 "HGNC__667"
      minerva_ref_type__resource10 "HGNC_SYMBOL__RHOA"
      minerva_ref_type__resource2 "REFSEQ__NM_001664"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000067560"
      minerva_ref_type__resource4 "EC__3.6.5.2"
      minerva_ref_type__resource5 "ENTREZ__387"
      minerva_ref_type__resource6 "ENTREZ__387"
      minerva_ref_type__resource7 "UNIPROT__P61586"
      minerva_ref_type__resource8 "UNIPROT__P61586"
      minerva_ref_type__resource9 "HGNC_SYMBOL__RHOA"
      minerva_structuralState "Active"
      minerva_type "Protein"
      minerva_x 658.6666666666667
      minerva_y 956.1666666666667
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa38"
      species_meta_id "s_id_sa38"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 2
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "pp2a__cell__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa16"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa16"
      minerva_name "PP2A"
      minerva_state1 "Active"
      minerva_type "Complex"
      minerva_x 835.0
      minerva_y 810.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa16"
      species_meta_id "s_id_csa16"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 3
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "bambi__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa33"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa33"
      minerva_name "BAMBI"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/Q13145"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q13145"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_012342"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000095739"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BAMBI"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/25805"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BAMBI"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/25805"
      minerva_ref_link9 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/30251"
      minerva_ref_type__resource1 "UNIPROT__Q13145"
      minerva_ref_type__resource2 "UNIPROT__Q13145"
      minerva_ref_type__resource3 "REFSEQ__NM_012342"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000095739"
      minerva_ref_type__resource5 "HGNC_SYMBOL__BAMBI"
      minerva_ref_type__resource6 "ENTREZ__25805"
      minerva_ref_type__resource7 "HGNC_SYMBOL__BAMBI"
      minerva_ref_type__resource8 "ENTREZ__25805"
      minerva_ref_type__resource9 "HGNC__30251"
      minerva_type "Protein"
      minerva_x 284.0
      minerva_y 565.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa33"
      species_meta_id "s_id_sa33"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 4
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf3a__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa65"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa65"
      minerva_name "Orf3a"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/P59632"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/1489669"
      minerva_ref_type__resource1 "UNIPROT__P59632"
      minerva_ref_type__resource2 "ENTREZ__1489669"
      minerva_type "Protein"
      minerva_x 380.0
      minerva_y 400.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa65"
      species_meta_id "s_id_sa65"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 5
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "rhoa__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa1"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa1"
      minerva_former_symbols "ARH12; ARHA"
      minerva_name "RHOA"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/667"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=RHOA"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_001664"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000067560"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.6.5.2"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/387"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/387"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P61586"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P61586"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=RHOA"
      minerva_ref_type__resource1 "HGNC__667"
      minerva_ref_type__resource10 "HGNC_SYMBOL__RHOA"
      minerva_ref_type__resource2 "REFSEQ__NM_001664"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000067560"
      minerva_ref_type__resource4 "EC__3.6.5.2"
      minerva_ref_type__resource5 "ENTREZ__387"
      minerva_ref_type__resource6 "ENTREZ__387"
      minerva_ref_type__resource7 "UNIPROT__P61586"
      minerva_ref_type__resource8 "UNIPROT__P61586"
      minerva_ref_type__resource9 "HGNC_SYMBOL__RHOA"
      minerva_type "Protein"
      minerva_x 659.0
      minerva_y 1072.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa1"
      species_meta_id "s_id_sa1"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 6
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "e__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa69"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa69"
      minerva_name "E"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P59637"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/1489671"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=E"
      minerva_ref_type__resource1 "UNIPROT__P59637"
      minerva_ref_type__resource2 "ENTREZ__1489671"
      minerva_ref_type__resource3 "HGNC_SYMBOL__E"
      minerva_type "Protein"
      minerva_x 755.0
      minerva_y 195.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa69"
      species_meta_id "s_id_sa69"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 8
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "rps6kb1__cell__empty__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa40"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa40"
      minerva_former_symbols "STK14A"
      minerva_name "RPS6KB1"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/10436"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_003161"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P23443"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P23443"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.1"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/6198"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/6198"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000108443"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=RPS6KB1"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=RPS6KB1"
      minerva_ref_type__resource1 "HGNC__10436"
      minerva_ref_type__resource10 "REFSEQ__NM_003161"
      minerva_ref_type__resource2 "UNIPROT__P23443"
      minerva_ref_type__resource3 "UNIPROT__P23443"
      minerva_ref_type__resource4 "EC__2.7.11.1"
      minerva_ref_type__resource5 "ENTREZ__6198"
      minerva_ref_type__resource6 "ENTREZ__6198"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000108443"
      minerva_ref_type__resource8 "HGNC_SYMBOL__RPS6KB1"
      minerva_ref_type__resource9 "HGNC_SYMBOL__RPS6KB1"
      minerva_state1 "EMPTY"
      minerva_type "Protein"
      minerva_x 945.0
      minerva_y 828.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa40"
      species_meta_id "s_id_sa40"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 9
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1000.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tgfbr__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa13"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa13"
      minerva_name "TGFBR"
      minerva_type "Complex"
      minerva_x 299.0
      minerva_y 675.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa13"
      species_meta_id "s_id_csa13"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 10
    zlevel -1

    cluster [
      cluster "default"
    ]
    graphics [
      x 1100.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ltbp1__default__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa7"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa7"
      minerva_name "LTBP1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_206943"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=LTBP1"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/4052"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=LTBP1"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/4052"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/Q14766"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/Q14766"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6714"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000049323"
      minerva_ref_type__resource1 "REFSEQ__NM_206943"
      minerva_ref_type__resource2 "HGNC_SYMBOL__LTBP1"
      minerva_ref_type__resource3 "ENTREZ__4052"
      minerva_ref_type__resource4 "HGNC_SYMBOL__LTBP1"
      minerva_ref_type__resource5 "ENTREZ__4052"
      minerva_ref_type__resource6 "UNIPROT__Q14766"
      minerva_ref_type__resource7 "UNIPROT__Q14766"
      minerva_ref_type__resource8 "HGNC__6714"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000049323"
      minerva_type "Protein"
      minerva_x 100.0
      minerva_y 794.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "default"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa7"
      species_meta_id "s_id_sa7"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 11
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1200.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp7b__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa70"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa70"
      minerva_name "Nsp7b"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/Q7TFA1"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/1489675"
      minerva_ref_type__resource1 "UNIPROT__Q7TFA1"
      minerva_ref_type__resource2 "ENTREZ__1489675"
      minerva_type "Protein"
      minerva_x 755.0
      minerva_y 150.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa70"
      species_meta_id "s_id_sa70"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 12
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "pp2a__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa15"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa15"
      minerva_name "PP2A"
      minerva_type "Complex"
      minerva_x 655.0
      minerva_y 810.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa15"
      species_meta_id "s_id_csa15"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 14
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "rps6kb1__cell__phosphorylated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa41"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa41"
      minerva_former_symbols "STK14A"
      minerva_name "RPS6KB1"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/10436"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_003161"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P23443"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P23443"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.1"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/6198"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/6198"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000108443"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=RPS6KB1"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=RPS6KB1"
      minerva_ref_type__resource1 "HGNC__10436"
      minerva_ref_type__resource10 "REFSEQ__NM_003161"
      minerva_ref_type__resource2 "UNIPROT__P23443"
      minerva_ref_type__resource3 "UNIPROT__P23443"
      minerva_ref_type__resource4 "EC__2.7.11.1"
      minerva_ref_type__resource5 "ENTREZ__6198"
      minerva_ref_type__resource6 "ENTREZ__6198"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000108443"
      minerva_ref_type__resource8 "HGNC_SYMBOL__RPS6KB1"
      minerva_ref_type__resource9 "HGNC_SYMBOL__RPS6KB1"
      minerva_state1 "PHOSPHORYLATED"
      minerva_type "Protein"
      minerva_x 945.0
      minerva_y 733.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa41"
      species_meta_id "s_id_sa41"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 16
    zlevel -1

    cluster [
      cluster "default"
    ]
    graphics [
      x 600.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp7b__default__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa64"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa64"
      minerva_name "Nsp7b"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/Q7TFA1"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/1489675"
      minerva_ref_type__resource1 "UNIPROT__Q7TFA1"
      minerva_ref_type__resource2 "ENTREZ__1489675"
      minerva_type "Protein"
      minerva_x 215.0
      minerva_y 1330.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "default"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa64"
      species_meta_id "s_id_sa64"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 17
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "acvr1b__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa45"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa45"
      minerva_former_symbols "ACVRLK4"
      minerva_name "ACVR1B"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ACVR1B"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_020328"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P36896"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P36896"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ACVR1B"
      minerva_ref_link5 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.30"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/91"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/91"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/172"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000135503"
      minerva_ref_type__resource1 "HGNC_SYMBOL__ACVR1B"
      minerva_ref_type__resource10 "REFSEQ__NM_020328"
      minerva_ref_type__resource2 "UNIPROT__P36896"
      minerva_ref_type__resource3 "UNIPROT__P36896"
      minerva_ref_type__resource4 "HGNC_SYMBOL__ACVR1B"
      minerva_ref_type__resource5 "EC__2.7.11.30"
      minerva_ref_type__resource6 "ENTREZ__91"
      minerva_ref_type__resource7 "ENTREZ__91"
      minerva_ref_type__resource8 "HGNC__172"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000135503"
      minerva_type "Protein"
      minerva_x 279.0
      minerva_y 1125.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa45"
      species_meta_id "s_id_sa45"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 20
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1000.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "modulation_space_of_space_cell_space_cycle__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa55"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa55"
      minerva_fullName "regulation of cell cycle"
      minerva_name "Modulation of cell cycle"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0000074"
      minerva_ref_type__resource1 "GO__GO:0000074"
      minerva_type "Phenotype"
      minerva_x 1425.0
      minerva_y 798.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa55"
      species_meta_id "s_id_sa55"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 21
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf7a__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa84"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa84"
      minerva_name "Orf7a"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/P59635"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/1489674"
      minerva_ref_type__resource1 "UNIPROT__P59635"
      minerva_ref_type__resource2 "ENTREZ__1489674"
      minerva_type "Protein"
      minerva_x 745.0
      minerva_y 700.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa84"
      species_meta_id "s_id_sa84"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 22
    zlevel -1

    cluster [
      cluster "default"
    ]
    graphics [
      x 1200.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tgfb1__default__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa10"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa10"
      minerva_former_symbols "DPD1; TGFB"
      minerva_name "TGFB1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_000660"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/7040"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7040"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11766"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TGFB1"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P01137"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P01137"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TGFB1"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000105329"
      minerva_ref_type__resource1 "REFSEQ__NM_000660"
      minerva_ref_type__resource2 "ENTREZ__7040"
      minerva_ref_type__resource3 "ENTREZ__7040"
      minerva_ref_type__resource4 "HGNC__11766"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TGFB1"
      minerva_ref_type__resource6 "UNIPROT__P01137"
      minerva_ref_type__resource7 "UNIPROT__P01137"
      minerva_ref_type__resource8 "HGNC_SYMBOL__TGFB1"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000105329"
      minerva_type "Protein"
      minerva_x 175.0
      minerva_y 638.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "default"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa10"
      species_meta_id "s_id_sa10"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 23
    zlevel -1

    cluster [
      cluster "default"
    ]
    graphics [
      x 200.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf8__default__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa72"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa72"
      minerva_name "Orf8"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/Q80H93"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q7TFA0"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/1489677"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/1489676"
      minerva_ref_type__resource1 "UNIPROT__Q80H93"
      minerva_ref_type__resource2 "UNIPROT__Q7TFA0"
      minerva_ref_type__resource3 "ENTREZ__1489677"
      minerva_ref_type__resource4 "ENTREZ__1489676"
      minerva_type "Protein"
      minerva_x 125.0
      minerva_y 735.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "default"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa72"
      species_meta_id "s_id_sa72"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 24
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "bmpr1_slash_2_slash_actr2__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa7"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa7"
      minerva_name "BMPR1/2/ACTR2"
      minerva_type "Complex"
      minerva_x 301.0
      minerva_y 196.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa7"
      species_meta_id "s_id_csa7"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 25
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "acvr1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa44"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa44"
      minerva_former_symbols "ACVRLK2"
      minerva_name "ACVR1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/90"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000115170"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/90"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/171"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.30"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/Q04771"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/Q04771"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ACVR1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_001105"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ACVR1"
      minerva_ref_type__resource1 "ENTREZ__90"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000115170"
      minerva_ref_type__resource2 "ENTREZ__90"
      minerva_ref_type__resource3 "HGNC__171"
      minerva_ref_type__resource4 "EC__2.7.11.30"
      minerva_ref_type__resource5 "UNIPROT__Q04771"
      minerva_ref_type__resource6 "UNIPROT__Q04771"
      minerva_ref_type__resource7 "HGNC_SYMBOL__ACVR1"
      minerva_ref_type__resource8 "REFSEQ__NM_001105"
      minerva_ref_type__resource9 "HGNC_SYMBOL__ACVR1"
      minerva_type "Protein"
      minerva_x 279.0
      minerva_y 1180.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa44"
      species_meta_id "s_id_sa44"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 26
    zlevel -1

    cluster [
      cluster "default"
    ]
    graphics [
      x 500.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sa44_underscore_degraded__default__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa66"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa66"
      minerva_name "sa44_degraded"
      minerva_type "Degraded"
      minerva_x 125.0
      minerva_y 565.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "default"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa66"
      species_meta_id "s_id_sa66"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 27
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp7__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa76"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa76"
      minerva_name "Nsp7"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/Q7TFA1"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/1489675"
      minerva_ref_type__resource1 "UNIPROT__Q7TFA1"
      minerva_ref_type__resource2 "ENTREZ__1489675"
      minerva_type "Protein"
      minerva_x 740.0
      minerva_y 1014.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa76"
      species_meta_id "s_id_sa76"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 28
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf10__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa92"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa92"
      minerva_name "Orf10"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/BCD58762"
      minerva_ref_type__resource1 "NCBI_PROTEIN__BCD58762"
      minerva_type "Protein"
      minerva_x 1031.0
      minerva_y 1381.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa92"
      species_meta_id "s_id_sa92"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 29
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 800.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tgfb_slash_tgfbr__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa11"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa11"
      minerva_name "TGFB/TGFBR"
      minerva_type "Complex"
      minerva_x 264.0
      minerva_y 930.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa11"
      species_meta_id "s_id_csa11"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 30
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mapk3__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa18"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa18"
      minerva_former_symbols "PRKM3"
      minerva_name "MAPK3"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.24"
      minerva_ref_link10 "http://purl.uniprot.org/uniprot/P27361"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000102882"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6877"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAPK3"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAPK3"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_001040056"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/5595"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/5595"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P27361"
      minerva_ref_type__resource1 "EC__2.7.11.24"
      minerva_ref_type__resource10 "UNIPROT__P27361"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000102882"
      minerva_ref_type__resource3 "HGNC__6877"
      minerva_ref_type__resource4 "HGNC_SYMBOL__MAPK3"
      minerva_ref_type__resource5 "HGNC_SYMBOL__MAPK3"
      minerva_ref_type__resource6 "REFSEQ__NM_001040056"
      minerva_ref_type__resource7 "ENTREZ__5595"
      minerva_ref_type__resource8 "ENTREZ__5595"
      minerva_ref_type__resource9 "UNIPROT__P27361"
      minerva_type "Protein"
      minerva_x 560.0
      minerva_y 500.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa18"
      species_meta_id "s_id_sa18"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 32
    zlevel -1

    graphics [
      x 1100.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re3"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_000660"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/7040"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7040"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11766"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TGFB1"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P01137"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P01137"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TGFB1"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000105329"
      minerva_ref_type__resource1 "REFSEQ__NM_000660"
      minerva_ref_type__resource2 "ENTREZ__7040"
      minerva_ref_type__resource3 "ENTREZ__7040"
      minerva_ref_type__resource4 "HGNC__11766"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TGFB1"
      minerva_ref_type__resource6 "UNIPROT__P01137"
      minerva_ref_type__resource7 "UNIPROT__P01137"
      minerva_ref_type__resource8 "HGNC_SYMBOL__TGFB1"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000105329"
      minerva_type "State transition"
      minerva_x 627.5
      minerva_y 315.03125
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re3"
      reaction_meta_id "re3"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 41
    zlevel -1

    graphics [
      x 1200.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re20"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_000660"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/7040"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7040"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11766"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TGFB1"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P01137"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P01137"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TGFB1"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000105329"
      minerva_ref_type__resource1 "REFSEQ__NM_000660"
      minerva_ref_type__resource2 "ENTREZ__7040"
      minerva_ref_type__resource3 "ENTREZ__7040"
      minerva_ref_type__resource4 "HGNC__11766"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TGFB1"
      minerva_ref_type__resource6 "UNIPROT__P01137"
      minerva_ref_type__resource7 "UNIPROT__P01137"
      minerva_ref_type__resource8 "HGNC_SYMBOL__TGFB1"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000105329"
      minerva_type "State transition"
      minerva_x 188.5
      minerva_y 565.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re20"
      reaction_meta_id "re20"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 45
    zlevel -1

    graphics [
      x 200.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re14"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_000660"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/7040"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7040"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11766"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TGFB1"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P01137"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P01137"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TGFB1"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000105329"
      minerva_ref_type__resource1 "REFSEQ__NM_000660"
      minerva_ref_type__resource2 "ENTREZ__7040"
      minerva_ref_type__resource3 "ENTREZ__7040"
      minerva_ref_type__resource4 "HGNC__11766"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TGFB1"
      minerva_ref_type__resource6 "UNIPROT__P01137"
      minerva_ref_type__resource7 "UNIPROT__P01137"
      minerva_ref_type__resource8 "HGNC_SYMBOL__TGFB1"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000105329"
      minerva_type "Physical stimulation"
      minerva_x 1013.3333333333334
      minerva_y 1052.0
    ]
    sbml [
      reaction_id "re14"
      reaction_meta_id "re14"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 48
    zlevel -1

    graphics [
      x 300.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re8"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_000660"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/7040"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7040"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11766"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TGFB1"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P01137"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P01137"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TGFB1"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000105329"
      minerva_ref_type__resource1 "REFSEQ__NM_000660"
      minerva_ref_type__resource2 "ENTREZ__7040"
      minerva_ref_type__resource3 "ENTREZ__7040"
      minerva_ref_type__resource4 "HGNC__11766"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TGFB1"
      minerva_ref_type__resource6 "UNIPROT__P01137"
      minerva_ref_type__resource7 "UNIPROT__P01137"
      minerva_ref_type__resource8 "HGNC_SYMBOL__TGFB1"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000105329"
      minerva_type "State transition"
      minerva_x 745.0
      minerva_y 780.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re8"
      reaction_meta_id "re8"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 53
    zlevel -1

    graphics [
      x 400.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re33"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_000660"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/7040"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7040"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11766"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TGFB1"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P01137"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P01137"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TGFB1"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000105329"
      minerva_ref_type__resource1 "REFSEQ__NM_000660"
      minerva_ref_type__resource2 "ENTREZ__7040"
      minerva_ref_type__resource3 "ENTREZ__7040"
      minerva_ref_type__resource4 "HGNC__11766"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TGFB1"
      minerva_ref_type__resource6 "UNIPROT__P01137"
      minerva_ref_type__resource7 "UNIPROT__P01137"
      minerva_ref_type__resource8 "HGNC_SYMBOL__TGFB1"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000105329"
      minerva_type "Heterodimer association"
      minerva_x 888.5
      minerva_y 1440.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re33"
      reaction_meta_id "re33"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 57
    zlevel -1

    graphics [
      x 500.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re7"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_000660"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/7040"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7040"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11766"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TGFB1"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P01137"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P01137"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TGFB1"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000105329"
      minerva_ref_type__resource1 "REFSEQ__NM_000660"
      minerva_ref_type__resource2 "ENTREZ__7040"
      minerva_ref_type__resource3 "ENTREZ__7040"
      minerva_ref_type__resource4 "HGNC__11766"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TGFB1"
      minerva_ref_type__resource6 "UNIPROT__P01137"
      minerva_ref_type__resource7 "UNIPROT__P01137"
      minerva_ref_type__resource8 "HGNC_SYMBOL__TGFB1"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000105329"
      minerva_type "State transition"
      minerva_x 658.8333333333334
      minerva_y 1014.0833333333334
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re7"
      reaction_meta_id "re7"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 62
    zlevel -1

    graphics [
      x 600.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re6"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_000660"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/7040"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7040"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11766"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TGFB1"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P01137"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P01137"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TGFB1"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000105329"
      minerva_ref_type__resource1 "REFSEQ__NM_000660"
      minerva_ref_type__resource2 "ENTREZ__7040"
      minerva_ref_type__resource3 "ENTREZ__7040"
      minerva_ref_type__resource4 "HGNC__11766"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TGFB1"
      minerva_ref_type__resource6 "UNIPROT__P01137"
      minerva_ref_type__resource7 "UNIPROT__P01137"
      minerva_ref_type__resource8 "HGNC_SYMBOL__TGFB1"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000105329"
      minerva_type "State transition"
      minerva_x 554.0
      minerva_y 1250.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re6"
      reaction_meta_id "re6"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 73
    zlevel -1

    graphics [
      x 700.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re13"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_000660"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/7040"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7040"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11766"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TGFB1"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P01137"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P01137"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TGFB1"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000105329"
      minerva_ref_type__resource1 "REFSEQ__NM_000660"
      minerva_ref_type__resource2 "ENTREZ__7040"
      minerva_ref_type__resource3 "ENTREZ__7040"
      minerva_ref_type__resource4 "HGNC__11766"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TGFB1"
      minerva_ref_type__resource6 "UNIPROT__P01137"
      minerva_ref_type__resource7 "UNIPROT__P01137"
      minerva_ref_type__resource8 "HGNC_SYMBOL__TGFB1"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000105329"
      minerva_type "Physical stimulation"
      minerva_x 1063.5520833333335
      minerva_y 529.03125
    ]
    sbml [
      reaction_id "re13"
      reaction_meta_id "re13"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 76
    zlevel -1

    graphics [
      x 800.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re10"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_000660"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/7040"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7040"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11766"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TGFB1"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P01137"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P01137"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TGFB1"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000105329"
      minerva_ref_type__resource1 "REFSEQ__NM_000660"
      minerva_ref_type__resource2 "ENTREZ__7040"
      minerva_ref_type__resource3 "ENTREZ__7040"
      minerva_ref_type__resource4 "HGNC__11766"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TGFB1"
      minerva_ref_type__resource6 "UNIPROT__P01137"
      minerva_ref_type__resource7 "UNIPROT__P01137"
      minerva_ref_type__resource8 "HGNC_SYMBOL__TGFB1"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000105329"
      minerva_type "State transition"
      minerva_x 945.0
      minerva_y 780.5
    ]
    sbml [
      reaction_id "re10"
      reaction_meta_id "re10"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 80
    zlevel -1

    graphics [
      x 900.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re32"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_000660"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/7040"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7040"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11766"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TGFB1"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P01137"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P01137"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TGFB1"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000105329"
      minerva_ref_type__resource1 "REFSEQ__NM_000660"
      minerva_ref_type__resource2 "ENTREZ__7040"
      minerva_ref_type__resource3 "ENTREZ__7040"
      minerva_ref_type__resource4 "HGNC__11766"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TGFB1"
      minerva_ref_type__resource6 "UNIPROT__P01137"
      minerva_ref_type__resource7 "UNIPROT__P01137"
      minerva_ref_type__resource8 "HGNC_SYMBOL__TGFB1"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000105329"
      minerva_type "Heterodimer association"
      minerva_x 213.75000000000006
      minerva_y 788.7500000000001
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re32"
      reaction_meta_id "re32"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 87
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 250.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "smad2_slash_3__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa5"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa5"
      minerva_name "SMAD2/3"
      minerva_type "Complex"
      minerva_x 654.0
      minerva_y 1250.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa5"
      species_meta_id "s_id_csa5"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
    ]
  ]
  node [
    id 91
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "smad1_slash_5_slash_8__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa2"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa2"
      minerva_name "SMAD1/5/8"
      minerva_type "Complex"
      minerva_x 500.0
      minerva_y 315.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa2"
      species_meta_id "s_id_csa2"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
    ]
  ]
  node [
    id 95
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 550.0
      y 150.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "e3_space_ubiquitin_space_ligase_space_complex__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa12"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa12"
      minerva_name "E3 ubiquitin ligase complex"
      minerva_type "Complex"
      minerva_x 1069.0
      minerva_y 1495.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa12"
      species_meta_id "s_id_csa12"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
    ]
  ]
  edge [
    id 35
    source 3
    target 32
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR0"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa33"
    ]
  ]
  edge [
    id 36
    source 24
    target 32
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR1"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa7"
    ]
  ]
  edge [
    id 37
    source 30
    target 32
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR2"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa18"
    ]
  ]
  edge [
    id 38
    source 4
    target 32
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR3"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa65"
    ]
  ]
  edge [
    id 39
    source 11
    target 32
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR4"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa70"
    ]
  ]
  edge [
    id 40
    source 6
    target 32
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR5"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa69"
    ]
  ]
  edge [
    id 42
    source 3
    target 41
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 43
    source 41
    target 26
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa66"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 44
    source 4
    target 41
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR6"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa65"
    ]
  ]
  edge [
    id 47
    source 45
    target 20
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa55"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 49
    source 12
    target 48
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa15"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 50
    source 48
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa16"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 51
    source 29
    target 48
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR7"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa11"
    ]
  ]
  edge [
    id 52
    source 21
    target 48
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR8"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa84"
    ]
  ]
  edge [
    id 54
    source 28
    target 53
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa92"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 58
    source 5
    target 57
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa1"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 59
    source 57
    target 1
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa38"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 60
    source 29
    target 57
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR9"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa11"
    ]
  ]
  edge [
    id 61
    source 27
    target 57
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR10"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa76"
    ]
  ]
  edge [
    id 65
    source 3
    target 62
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR11"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa33"
    ]
  ]
  edge [
    id 67
    source 29
    target 62
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR13"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa11"
    ]
  ]
  edge [
    id 68
    source 25
    target 62
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR14"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa44"
    ]
  ]
  edge [
    id 69
    source 17
    target 62
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR15"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa45"
    ]
  ]
  edge [
    id 70
    source 30
    target 62
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR16"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa18"
    ]
  ]
  edge [
    id 71
    source 4
    target 62
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR17"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa65"
    ]
  ]
  edge [
    id 72
    source 16
    target 62
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR18"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa64"
    ]
  ]
  edge [
    id 75
    source 73
    target 20
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa55"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 77
    source 14
    target 76
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa41"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 78
    source 76
    target 8
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa40"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 79
    source 2
    target 76
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR19"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa16"
    ]
  ]
  edge [
    id 81
    source 22
    target 80
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa10"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 82
    source 9
    target 80
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa13"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 83
    source 80
    target 29
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa11"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 84
    source 10
    target 80
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR20"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa7"
    ]
  ]
  edge [
    id 85
    source 3
    target 80
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR21"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa33"
    ]
  ]
  edge [
    id 86
    source 23
    target 80
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR22"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa72"
    ]
  ]
  edge [
    id 88
    source 62
    target 87
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa5"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 89
    source 87
    target 45
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa5"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 90
    source 87
    target 62
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa4"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 92
    source 91
    target 32
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa2"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 93
    source 32
    target 91
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa3"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 94
    source 91
    target 73
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa3"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 96
    source 95
    target 53
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa12"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 97
    source 95
    target 62
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR12"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa9"
    ]
  ]
  edge [
    id 98
    source 53
    target 95
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa9"
      stoichiometry "1.0"
    ]
  ]
]
