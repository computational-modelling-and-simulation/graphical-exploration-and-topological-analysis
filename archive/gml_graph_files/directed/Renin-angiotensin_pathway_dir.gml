# generated with VANTED V2.8.0 at Tue Apr 27 20:00:46 CEST 2021
graph [
  background_coloring "true"
  cluster_colors "255,128,128,255:0,0,0,255;255,237,128,255:0,0,0,255;164,255,128,255:0,0,0,255;128,255,200,255:0,0,0,255;128,200,255,255:0,0,0,255;164,128,255,255:0,0,0,255;255,128,237,255:0,0,0,255"
  clusterbackground_fill_outer_region "false"
  clusterbackground_grid 50.0
  clusterbackground_low_alpha 0.2
  clusterbackground_radius 200.0
  clusterbackground_space_fill "true"
  graphbackgroundcolor "#ffffff"
  sbml [
    model_annotation ""
    model_meta_id "RAS_complete2"
    model_non_rdf_annotation ""
    model_notes ""
    model_sbml_id "RAS_complete2"
    namespace "xmlns:celldesigner=\"http://www.sbml.org/2001/ns/celldesigner\" "
    sbml_level "2"
    sbml_version "4"
  ]
  sbml_compartment_default [
    sbml_compartment_default_id "default"
    sbml_compartment_default_meta_id "default"
    sbml_compartment_default_non_rdf_annotation ""
    sbml_compartment_default_notes ""
    sbml_compartment_default_size "1.0"
    sbml_compartment_default_units "volume"
  ]
  sbml_compartment_s_id_ca1 [
    sbml_compartment_s_id_ca1_annotation ""
    sbml_compartment_s_id_ca1_id "s_id_ca1"
    sbml_compartment_s_id_ca1_meta_id "s_id_ca1"
    sbml_compartment_s_id_ca1_name "cell"
    sbml_compartment_s_id_ca1_non_rdf_annotation ""
    sbml_compartment_s_id_ca1_notes ""
    sbml_compartment_s_id_ca1_outside "s_id_ca9"
    sbml_compartment_s_id_ca1_size "1.0"
    sbml_compartment_s_id_ca1_units "volume"
  ]
  sbml_compartment_s_id_ca3 [
    sbml_compartment_s_id_ca3_annotation ""
    sbml_compartment_s_id_ca3_id "s_id_ca3"
    sbml_compartment_s_id_ca3_meta_id "s_id_ca3"
    sbml_compartment_s_id_ca3_name "lysosome"
    sbml_compartment_s_id_ca3_non_rdf_annotation ""
    sbml_compartment_s_id_ca3_notes ""
    sbml_compartment_s_id_ca3_outside "s_id_ca1"
    sbml_compartment_s_id_ca3_size "1.0"
    sbml_compartment_s_id_ca3_units "volume"
  ]
  sbml_compartment_s_id_ca5 [
    sbml_compartment_s_id_ca5_annotation ""
    sbml_compartment_s_id_ca5_id "s_id_ca5"
    sbml_compartment_s_id_ca5_meta_id "s_id_ca5"
    sbml_compartment_s_id_ca5_name "Kidney_space__slash__space_renin_minus_producing_space_organs"
    sbml_compartment_s_id_ca5_non_rdf_annotation ""
    sbml_compartment_s_id_ca5_notes ""
    sbml_compartment_s_id_ca5_outside "s_id_ca9"
    sbml_compartment_s_id_ca5_size "1.0"
    sbml_compartment_s_id_ca5_units "volume"
  ]
  sbml_compartment_s_id_ca6 [
    sbml_compartment_s_id_ca6_annotation ""
    sbml_compartment_s_id_ca6_id "s_id_ca6"
    sbml_compartment_s_id_ca6_meta_id "s_id_ca6"
    sbml_compartment_s_id_ca6_name "cell"
    sbml_compartment_s_id_ca6_non_rdf_annotation ""
    sbml_compartment_s_id_ca6_notes ""
    sbml_compartment_s_id_ca6_outside "s_id_ca5"
    sbml_compartment_s_id_ca6_size "1.0"
    sbml_compartment_s_id_ca6_units "volume"
  ]
  sbml_compartment_s_id_ca8 [
    sbml_compartment_s_id_ca8_annotation ""
    sbml_compartment_s_id_ca8_id "s_id_ca8"
    sbml_compartment_s_id_ca8_meta_id "s_id_ca8"
    sbml_compartment_s_id_ca8_name "nucleus"
    sbml_compartment_s_id_ca8_non_rdf_annotation ""
    sbml_compartment_s_id_ca8_notes ""
    sbml_compartment_s_id_ca8_outside "s_id_ca1"
    sbml_compartment_s_id_ca8_size "1.0"
    sbml_compartment_s_id_ca8_units "volume"
  ]
  sbml_compartment_s_id_ca9 [
    sbml_compartment_s_id_ca9_id "s_id_ca9"
    sbml_compartment_s_id_ca9_meta_id "s_id_ca9"
    sbml_compartment_s_id_ca9_name "human_space_host"
    sbml_compartment_s_id_ca9_non_rdf_annotation ""
    sbml_compartment_s_id_ca9_notes ""
    sbml_compartment_s_id_ca9_outside "default"
    sbml_compartment_s_id_ca9_size "1.0"
    sbml_compartment_s_id_ca9_units "volume"
  ]
  sbml_unit_definition_1 [
    sbml_unit_definition_1_id "area"
    sbml_unit_definition_1_name "area"
    sbml_unit_definition_1_sub_unit_1_ "(1.0 * 10^0 * metre)^2.0"
    sbml_unit_definition_1unit "(1.0 * 10^0 * metre)^2.0"
  ]
  sbml_unit_definition_2 [
    sbml_unit_definition_2_id "substance"
    sbml_unit_definition_2_name "substance"
    sbml_unit_definition_2_sub_unit_1_ "(1.0 * 10^0 * mole)^1.0"
    sbml_unit_definition_2unit "(1.0 * 10^0 * mole)^1.0"
  ]
  sbml_unit_definition_3 [
    sbml_unit_definition_3_id "volume"
    sbml_unit_definition_3_name "volume"
    sbml_unit_definition_3_sub_unit_1_ "(1.0 * 10^0 * litre)^1.0"
    sbml_unit_definition_3unit "(1.0 * 10^0 * litre)^1.0"
  ]
  sbml_unit_definition_4 [
    sbml_unit_definition_4_id "length"
    sbml_unit_definition_4_name "length"
    sbml_unit_definition_4_sub_unit_1_ "(1.0 * 10^0 * metre)^1.0"
    sbml_unit_definition_4unit "(1.0 * 10^0 * metre)^1.0"
  ]
  sbml_unit_definition_5 [
    sbml_unit_definition_5_id "time"
    sbml_unit_definition_5_name "time"
    sbml_unit_definition_5_sub_unit_1_ "(1.0 * 10^0 * second)^1.0"
    sbml_unit_definition_5unit "(1.0 * 10^0 * second)^1.0"
  ]
  directed 1
  node [
    id 1
    zlevel -1

    cluster [
      cluster "s_id_ca8"
    ]
    graphics [
      x 200.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "calcitriol__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa37"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa37"
      minerva_fullName "calcitriol"
      minerva_name "Calcitriol"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17823"
      minerva_ref_type__resource1 "CHEBI__CHEBI:17823"
      minerva_synonyms "(1S,3R,5Z,7E)-9,10-secocholesta-5,7,10-triene-1,3,25-triol; (1alpha,3beta,5Z,7E)-9,10-secocholesta-5,7,10(19)-triene-1,3,25-triol; (5Z,7E)-(1S,3R)-9,10-secocholesta-5,7,10(19)-triene-1,3,25-triol; 1,25-DHCC; 1-alpha-25-Dihydroxyvitamin D3; 1alpha,25(OH)2D3; 1alpha,25-dihydroxycholecalciferol; 1alpha,25-dihydroxyvitamin D3; 5-{2-[1-(5-HYDROXY-1,5-DIMETHYL-HEXYL)-7A-METHYL-OCTAHYDRO-INDEN-4-YLIDENE]-ETHYLIDENE}-4-METHYLENE-CYCLOHEXANE-1,3-DIOL; Calcijex; Calcitriol; Decostriol; Rocaltrol; calcitriol; calcitriol; calcitriol; calcitriol; calcitriolum"
      minerva_type "Simple molecule"
      minerva_x 299.0
      minerva_y 426.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca8"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa37"
      species_meta_id "s_id_sa37"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 2
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "angiotensin_space_1_minus_9__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa22"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa22"
      minerva_fullName "angiotensin (1-9)"
      minerva_name "angiotensin 1-9"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:80128"
      minerva_ref_type__resource1 "CHEBI__CHEBI:80128"
      minerva_synonyms "Asp-Arg-Val-Tyr-Ile-His-Pro-Phe-His; D-R-V-Y-I-H-P-F-H; DRVYIHPFH; L-Asp-L-Arg-L-Val-L-Tyr-L-Ile-L-His-L-Pro-L-Phe-L-His; [des-Leu(10)]-angiotensin I; ang-(1-9); angiotensin I (1-9); angiotensin-(1-9); des-Leu angiotensin I"
      minerva_type "Simple molecule"
      minerva_x 1669.5
      minerva_y 766.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa22"
      species_meta_id "s_id_sa22"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 3
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cma1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa122"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa122"
      minerva_name "CMA1"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/2097"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.21.39"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/1215"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/1215"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CMA1"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CMA1"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000092009"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_001836"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P23946"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P23946"
      minerva_ref_type__resource1 "HGNC__2097"
      minerva_ref_type__resource10 "EC__3.4.21.39"
      minerva_ref_type__resource2 "ENTREZ__1215"
      minerva_ref_type__resource3 "ENTREZ__1215"
      minerva_ref_type__resource4 "HGNC_SYMBOL__CMA1"
      minerva_ref_type__resource5 "HGNC_SYMBOL__CMA1"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000092009"
      minerva_ref_type__resource7 "REFSEQ__NM_001836"
      minerva_ref_type__resource8 "UNIPROT__P23946"
      minerva_ref_type__resource9 "UNIPROT__P23946"
      minerva_type "Protein"
      minerva_x 838.0
      minerva_y 950.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa122"
      species_meta_id "s_id_sa122"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 4
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ace__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa29"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa28"
      minerva_elementId2 "sa29"
      minerva_former_symbols "DCP1"
      minerva_name "ACE"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/2707"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.2.1.-"
      minerva_ref_link11 "https://www.ensembl.org/id/ENSG00000159640"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P12821"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P12821"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.15.1"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/1636"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/1636"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ACE"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_000789"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ACE"
      minerva_ref_type__resource1 "HGNC__2707"
      minerva_ref_type__resource10 "EC__3.2.1.-"
      minerva_ref_type__resource11 "ENSEMBL__ENSG00000159640"
      minerva_ref_type__resource2 "UNIPROT__P12821"
      minerva_ref_type__resource3 "UNIPROT__P12821"
      minerva_ref_type__resource4 "EC__3.4.15.1"
      minerva_ref_type__resource5 "ENTREZ__1636"
      minerva_ref_type__resource6 "ENTREZ__1636"
      minerva_ref_type__resource7 "HGNC_SYMBOL__ACE"
      minerva_ref_type__resource8 "REFSEQ__NM_000789"
      minerva_ref_type__resource9 "HGNC_SYMBOL__ACE"
      minerva_type "Protein"
      minerva_x 2230.0
      minerva_x2 570.0
      minerva_y 970.0
      minerva_y2 925.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa29"
      species_meta_id "s_id_sa29"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 5
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 600.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ctsg__lysosome__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa137"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa137"
      minerva_name "CTSG"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/1511"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000100448"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P08311"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P08311"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/1511"
      minerva_ref_link5 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.21.20"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/2532"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CTSG"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CTSG"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_001911"
      minerva_ref_type__resource1 "ENTREZ__1511"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000100448"
      minerva_ref_type__resource2 "UNIPROT__P08311"
      minerva_ref_type__resource3 "UNIPROT__P08311"
      minerva_ref_type__resource4 "ENTREZ__1511"
      minerva_ref_type__resource5 "EC__3.4.21.20"
      minerva_ref_type__resource6 "HGNC__2532"
      minerva_ref_type__resource7 "HGNC_SYMBOL__CTSG"
      minerva_ref_type__resource8 "HGNC_SYMBOL__CTSG"
      minerva_ref_type__resource9 "REFSEQ__NM_001911"
      minerva_type "Protein"
      minerva_x 940.0
      minerva_y 655.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "lysosome"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa137"
      species_meta_id "s_id_sa137"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 7
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 800.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sex,_space_male__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa193"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa44"
      minerva_elementId2 "sa193"
      minerva_name "sex, male"
      minerva_ref_link1 "http://id.nlm.nih.gov/mesh/D008297"
      minerva_ref_type__resource1 "MESH_2012__D008297"
      minerva_type "Phenotype"
      minerva_x 1884.0
      minerva_x2 430.0
      minerva_y 147.0
      minerva_y2 1399.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa193"
      species_meta_id "s_id_sa193"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 8
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "agtr2__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa79"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa79"
      minerva_name "AGTR2"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000180772"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR2"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR2"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/338"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_000686"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P50052"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/P50052"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/186"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/186"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000180772"
      minerva_ref_type__resource2 "HGNC_SYMBOL__AGTR2"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR2"
      minerva_ref_type__resource4 "HGNC__338"
      minerva_ref_type__resource5 "REFSEQ__NM_000686"
      minerva_ref_type__resource6 "UNIPROT__P50052"
      minerva_ref_type__resource7 "UNIPROT__P50052"
      minerva_ref_type__resource8 "ENTREZ__186"
      minerva_ref_type__resource9 "ENTREZ__186"
      minerva_type "Protein"
      minerva_x 1022.7894736842104
      minerva_y 1675.2894736842104
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa79"
      species_meta_id "s_id_sa79"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 9
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1000.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "estradiol__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa200"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa200"
      minerva_elementId2 "sa169"
      minerva_fullName "17beta-estradiol"
      minerva_name "estradiol"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16469"
      minerva_ref_type__resource1 "CHEBI__CHEBI:16469"
      minerva_synonyms "(17beta)-estra-1,3,5(10)-triene-3,17-diol; 17beta oestradiol; 17beta-estra-1,3,5(10)-triene-3,17-diol; 17beta-estradiol; 17beta-estradiol; 17beta-oestradiol; ESTRADIOL; Estradiol; Estradiol-17beta; Estradiol-17beta; beta-Estradiol; cis-estradiol"
      minerva_type "Simple molecule"
      minerva_x 620.0
      minerva_x2 1200.0
      minerva_y 440.0
      minerva_y2 401.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa200"
      species_meta_id "s_id_sa200"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 10
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ace__cell__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa146"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa100"
      minerva_elementId2 "sa146"
      minerva_former_symbols "DCP1"
      minerva_name "ACE"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/2707"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.2.1.-"
      minerva_ref_link11 "https://www.ensembl.org/id/ENSG00000159640"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P12821"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P12821"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.15.1"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/1636"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/1636"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ACE"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_000789"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ACE"
      minerva_ref_type__resource1 "HGNC__2707"
      minerva_ref_type__resource10 "EC__3.2.1.-"
      minerva_ref_type__resource11 "ENSEMBL__ENSG00000159640"
      minerva_ref_type__resource2 "UNIPROT__P12821"
      minerva_ref_type__resource3 "UNIPROT__P12821"
      minerva_ref_type__resource4 "EC__3.4.15.1"
      minerva_ref_type__resource5 "ENTREZ__1636"
      minerva_ref_type__resource6 "ENTREZ__1636"
      minerva_ref_type__resource7 "HGNC_SYMBOL__ACE"
      minerva_ref_type__resource8 "REFSEQ__NM_000789"
      minerva_ref_type__resource9 "HGNC_SYMBOL__ACE"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 2230.0
      minerva_x2 570.0
      minerva_y 1075.0
      minerva_y2 830.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa146"
      species_meta_id "s_id_sa146"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 11
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 1200.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "hypertension__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa202"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa202"
      minerva_name "hypertension"
      minerva_ref_link1 "http://id.nlm.nih.gov/mesh/D006973"
      minerva_ref_type__resource1 "MESH_2012__D006973"
      minerva_type "Phenotype"
      minerva_x 2106.0
      minerva_y 219.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa202"
      species_meta_id "s_id_sa202"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 12
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "agtr1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa26"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa26"
      minerva_elementId2 "sa139"
      minerva_former_symbols "AGTR1B"
      minerva_name "AGTR1"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "Protein"
      minerva_x 700.0
      minerva_x2 570.0
      minerva_y 1678.0
      minerva_y2 980.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa26"
      species_meta_id "s_id_sa26"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 13
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "enpep__cell__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa64"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa64"
      minerva_name "ENPEP"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000138792"
      minerva_ref_link10 "http://purl.uniprot.org/uniprot/Q07075"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/2028"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/2028"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/3355"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ENPEP"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ENPEP"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_001379611"
      minerva_ref_link8 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.11.7"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q07075"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000138792"
      minerva_ref_type__resource10 "UNIPROT__Q07075"
      minerva_ref_type__resource2 "ENTREZ__2028"
      minerva_ref_type__resource3 "ENTREZ__2028"
      minerva_ref_type__resource4 "HGNC__3355"
      minerva_ref_type__resource5 "HGNC_SYMBOL__ENPEP"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ENPEP"
      minerva_ref_type__resource7 "REFSEQ__NM_001379611"
      minerva_ref_type__resource8 "EC__3.4.11.7"
      minerva_ref_type__resource9 "UNIPROT__Q07075"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 566.0
      minerva_y 1343.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa64"
      species_meta_id "s_id_sa64"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 14
    zlevel -1

    cluster [
      cluster "s_id_ca8"
    ]
    graphics [
      x 400.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ace__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa199"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa199"
      minerva_former_symbols "DCP1"
      minerva_name "ACE"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/2707"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P12821"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/1636"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ACE"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_000789"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000159640"
      minerva_ref_type__resource1 "HGNC__2707"
      minerva_ref_type__resource2 "UNIPROT__P12821"
      minerva_ref_type__resource3 "ENTREZ__1636"
      minerva_ref_type__resource4 "HGNC_SYMBOL__ACE"
      minerva_ref_type__resource5 "REFSEQ__NM_000789"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000159640"
      minerva_type "Gene"
      minerva_x 860.0
      minerva_y 490.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca8"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa199"
      species_meta_id "s_id_sa199"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 15
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 500.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "angiotensin_space_1_minus_7__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa96"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa96"
      minerva_fullName "Ile(5)-angiotensin II (1-7)"
      minerva_name "angiotensin 1-7"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:55438"
      minerva_ref_type__resource1 "CHEBI__CHEBI:55438"
      minerva_synonyms "8-Des-phe-angiotensin II; Angiotensin (1-7); Angiotensin I (1-7); Angiotensin II (1-7); Angiotensin II (1-7) heptapeptide"
      minerva_type "Simple molecule"
      minerva_x 1650.0
      minerva_y 226.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa96"
      species_meta_id "s_id_sa96"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 16
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "calcitriol__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa201"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa201"
      minerva_fullName "calcitriol"
      minerva_name "Calcitriol"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17823"
      minerva_ref_type__resource1 "CHEBI__CHEBI:17823"
      minerva_synonyms "(1S,3R,5Z,7E)-9,10-secocholesta-5,7,10-triene-1,3,25-triol; (1alpha,3beta,5Z,7E)-9,10-secocholesta-5,7,10(19)-triene-1,3,25-triol; (5Z,7E)-(1S,3R)-9,10-secocholesta-5,7,10(19)-triene-1,3,25-triol; 1,25-DHCC; 1-alpha-25-Dihydroxyvitamin D3; 1alpha,25(OH)2D3; 1alpha,25-dihydroxycholecalciferol; 1alpha,25-dihydroxyvitamin D3; 5-{2-[1-(5-HYDROXY-1,5-DIMETHYL-HEXYL)-7A-METHYL-OCTAHYDRO-INDEN-4-YLIDENE]-ETHYLIDENE}-4-METHYLENE-CYCLOHEXANE-1,3-DIOL; Calcijex; Calcitriol; Decostriol; Rocaltrol; calcitriol; calcitriol; calcitriol; calcitriol; calcitriolum"
      minerva_type "Simple molecule"
      minerva_x 1180.0
      minerva_y 440.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa201"
      species_meta_id "s_id_sa201"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 17
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 700.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ren__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa71"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa71"
      minerva_name "REN"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=REN"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.23.15"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=REN"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P00797"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P00797"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/9958"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000143839"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/5972"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_000537"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/5972"
      minerva_ref_type__resource1 "HGNC_SYMBOL__REN"
      minerva_ref_type__resource10 "EC__3.4.23.15"
      minerva_ref_type__resource2 "HGNC_SYMBOL__REN"
      minerva_ref_type__resource3 "UNIPROT__P00797"
      minerva_ref_type__resource4 "UNIPROT__P00797"
      minerva_ref_type__resource5 "HGNC__9958"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000143839"
      minerva_ref_type__resource7 "ENTREZ__5972"
      minerva_ref_type__resource8 "REFSEQ__NM_000537"
      minerva_ref_type__resource9 "ENTREZ__5972"
      minerva_type "Protein"
      minerva_x 485.0
      minerva_y 510.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa71"
      species_meta_id "s_id_sa71"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 18
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 800.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "oxidative_space_stress__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa157"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa157"
      minerva_fullName "response to oxidative stress"
      minerva_name "oxidative stress"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0006979"
      minerva_ref_type__resource1 "GO__GO:0006979"
      minerva_type "Phenotype"
      minerva_x 1188.0
      minerva_y 2000.25
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa157"
      species_meta_id "s_id_sa157"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 19
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "angiotensin_space_1_minus_7__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa24"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa24"
      minerva_fullName "Ile(5)-angiotensin II (1-7)"
      minerva_name "angiotensin 1-7"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:55438"
      minerva_ref_type__resource1 "CHEBI__CHEBI:55438"
      minerva_synonyms "8-Des-phe-angiotensin II; Angiotensin (1-7); Angiotensin I (1-7); Angiotensin II (1-7); Angiotensin II (1-7) heptapeptide"
      minerva_type "Simple molecule"
      minerva_x 1669.5
      minerva_y 1021.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa24"
      species_meta_id "s_id_sa24"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 20
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 1000.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "inflammatory_space_response__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa61"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa61"
      minerva_fullName "inflammatory response"
      minerva_name "inflammatory response"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0006954"
      minerva_ref_type__resource1 "GO__GO:0006954"
      minerva_type "Phenotype"
      minerva_x 1188.0
      minerva_y 2059.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa61"
      species_meta_id "s_id_sa61"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 21
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 1100.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "platelet_space_aggregation__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa209"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa209"
      minerva_fullName "platelet aggregation"
      minerva_name "platelet aggregation"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0070527"
      minerva_ref_type__resource1 "GO__GO:0070527"
      minerva_type "Phenotype"
      minerva_x 424.0
      minerva_y 1630.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa209"
      species_meta_id "s_id_sa209"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 22
    zlevel -1

    cluster [
      cluster "s_id_ca8"
    ]
    graphics [
      x 1200.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ace2__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa168"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa168"
      minerva_name "ACE2"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000130234"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/13557"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/Q9BYF1"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/59272"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_001371415"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ACE2"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000130234"
      minerva_ref_type__resource2 "HGNC__13557"
      minerva_ref_type__resource3 "UNIPROT__Q9BYF1"
      minerva_ref_type__resource4 "ENTREZ__59272"
      minerva_ref_type__resource5 "REFSEQ__NM_001371415"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ACE2"
      minerva_type "Gene"
      minerva_x 1020.0
      minerva_y 491.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca8"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa168"
      species_meta_id "s_id_sa168"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 23
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 200.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ethynylestradiol__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa134"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa134"
      minerva_fullName "17alpha-ethynylestradiol"
      minerva_name "ethynylestradiol"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:4903"
      minerva_ref_type__resource1 "CHEBI__CHEBI:4903"
      minerva_synonyms "17-ethinyl-3,17-estradiol; 17-ethinyl-3,17-oestradiol; 17-ethinylestradiol; 17alpha-Ethinyl estradiol; 17alpha-ethynylestradiol; Ethinyl estradiol; Ethinylestradiol; Ethynyl estradiol; ethinyloestradiol"
      minerva_type "Simple molecule"
      minerva_x 855.0
      minerva_y 235.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa134"
      species_meta_id "s_id_sa134"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 24
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "angiotensin_space_i__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa21"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa21"
      minerva_fullName "angiotensin I"
      minerva_name "angiotensin I"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:2718"
      minerva_ref_type__resource1 "CHEBI__CHEBI:2718"
      minerva_synonyms "Asp-Arg-Val-Tyr-Ile-His-Pro-Phe-His-Leu; D-R-V-Y-I-H-P-F-H-L; DRVYIHPFHL; L-Asp-L-Arg-L-Val-L-Tyr-L-Ile-L-His-L-Pro-L-Phe-L-His-L-Leu; angiotensin I; pepsitensin; proangiotensin"
      minerva_type "Simple molecule"
      minerva_x 752.0
      minerva_y 765.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa21"
      species_meta_id "s_id_sa21"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 25
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 400.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "prcp__lysosome__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa47"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa47"
      minerva_name "PRCP"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000137509"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/5547"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PRCP"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PRCP"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.16.2"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P42785"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P42785"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/9344"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/5547"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_005040"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000137509"
      minerva_ref_type__resource10 "ENTREZ__5547"
      minerva_ref_type__resource2 "HGNC_SYMBOL__PRCP"
      minerva_ref_type__resource3 "HGNC_SYMBOL__PRCP"
      minerva_ref_type__resource4 "EC__3.4.16.2"
      minerva_ref_type__resource5 "UNIPROT__P42785"
      minerva_ref_type__resource6 "UNIPROT__P42785"
      minerva_ref_type__resource7 "HGNC__9344"
      minerva_ref_type__resource8 "ENTREZ__5547"
      minerva_ref_type__resource9 "REFSEQ__NM_005040"
      minerva_type "Protein"
      minerva_x 1030.0
      minerva_y 690.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "lysosome"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa47"
      species_meta_id "s_id_sa47"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 26
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 500.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "vasoconstriction__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa56"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa56"
      minerva_name "vasoconstriction"
      minerva_ref_link1 "http://id.nlm.nih.gov/mesh/D014661"
      minerva_ref_type__resource1 "MESH_2012__D014661"
      minerva_type "Phenotype"
      minerva_x 1188.0
      minerva_y 1823.25
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa56"
      species_meta_id "s_id_sa56"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 27
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ace2__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa30"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa30"
      minerva_elementId2 "sa31"
      minerva_name "ACE2"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000130234"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ACE2"
      minerva_ref_link11 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ACE2"
      minerva_ref_link2 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.17.23"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.17.-"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/13557"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/Q9BYF1"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/Q9BYF1"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/59272"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_001371415"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/59272"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000130234"
      minerva_ref_type__resource10 "HGNC_SYMBOL__ACE2"
      minerva_ref_type__resource11 "HGNC_SYMBOL__ACE2"
      minerva_ref_type__resource2 "EC__3.4.17.23"
      minerva_ref_type__resource3 "EC__3.4.17.-"
      minerva_ref_type__resource4 "HGNC__13557"
      minerva_ref_type__resource5 "UNIPROT__Q9BYF1"
      minerva_ref_type__resource6 "UNIPROT__Q9BYF1"
      minerva_ref_type__resource7 "ENTREZ__59272"
      minerva_ref_type__resource8 "REFSEQ__NM_001371415"
      minerva_ref_type__resource9 "ENTREZ__59272"
      minerva_type "Protein"
      minerva_x 1330.0
      minerva_x2 795.0
      minerva_y 300.0
      minerva_y2 1678.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa30"
      species_meta_id "s_id_sa30"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 28
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "prep__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa120"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa120"
      minerva_name "PREP"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/9358"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.21.26"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PREP"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PREP"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/5550"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/5550"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_002726"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000085377"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P48147"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P48147"
      minerva_ref_type__resource1 "HGNC__9358"
      minerva_ref_type__resource10 "EC__3.4.21.26"
      minerva_ref_type__resource2 "HGNC_SYMBOL__PREP"
      minerva_ref_type__resource3 "HGNC_SYMBOL__PREP"
      minerva_ref_type__resource4 "ENTREZ__5550"
      minerva_ref_type__resource5 "ENTREZ__5550"
      minerva_ref_type__resource6 "REFSEQ__NM_002726"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000085377"
      minerva_ref_type__resource8 "UNIPROT__P48147"
      minerva_ref_type__resource9 "UNIPROT__P48147"
      minerva_type "Protein"
      minerva_x 1415.0
      minerva_y 982.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa120"
      species_meta_id "s_id_sa120"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 29
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 800.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mrgprd__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa94"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa94"
      minerva_name "MRGPRD"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_198923"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/116512"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/116512"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MRGPRD"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MRGPRD"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/Q8TDS7"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/Q8TDS7"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/29626"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000172938"
      minerva_ref_type__resource1 "REFSEQ__NM_198923"
      minerva_ref_type__resource2 "ENTREZ__116512"
      minerva_ref_type__resource3 "ENTREZ__116512"
      minerva_ref_type__resource4 "HGNC_SYMBOL__MRGPRD"
      minerva_ref_type__resource5 "HGNC_SYMBOL__MRGPRD"
      minerva_ref_type__resource6 "UNIPROT__Q8TDS7"
      minerva_ref_type__resource7 "UNIPROT__Q8TDS7"
      minerva_ref_type__resource8 "HGNC__29626"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000172938"
      minerva_type "Protein"
      minerva_x 1738.0
      minerva_y 1675.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa94"
      species_meta_id "s_id_sa94"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 30
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "thop1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa48"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa48"
      minerva_name "THOP1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7064"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_003249"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/7064"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=THOP1"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000172009"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=THOP1"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P52888"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P52888"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11793"
      minerva_ref_link9 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.24.15"
      minerva_ref_type__resource1 "ENTREZ__7064"
      minerva_ref_type__resource10 "REFSEQ__NM_003249"
      minerva_ref_type__resource2 "ENTREZ__7064"
      minerva_ref_type__resource3 "HGNC_SYMBOL__THOP1"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000172009"
      minerva_ref_type__resource5 "HGNC_SYMBOL__THOP1"
      minerva_ref_type__resource6 "UNIPROT__P52888"
      minerva_ref_type__resource7 "UNIPROT__P52888"
      minerva_ref_type__resource8 "HGNC__11793"
      minerva_ref_type__resource9 "EC__3.4.24.15"
      minerva_type "Protein"
      minerva_x 1435.0
      minerva_y 905.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa48"
      species_meta_id "s_id_sa48"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 31
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1000.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "alamandine__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa49"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa49"
      minerva_name "alamandine"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/44192273"
      minerva_ref_type__resource1 "PUBCHEM__44192273"
      minerva_type "Simple molecule"
      minerva_x 1500.0
      minerva_y 1180.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa49"
      species_meta_id "s_id_sa49"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 32
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 1100.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "_space_diabetes_space_mellitus,_space_type_space_ii__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa203"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa203"
      minerva_name " Diabetes mellitus, type II"
      minerva_ref_link1 "http://id.nlm.nih.gov/mesh/D003924"
      minerva_ref_type__resource1 "MESH_2012__D003924"
      minerva_type "Phenotype"
      minerva_x 2422.5
      minerva_y 451.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa203"
      species_meta_id "s_id_sa203"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 33
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1200.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "agtr1__cell__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa102"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa140"
      minerva_elementId2 "sa204"
      minerva_elementId3 "sa102"
      minerva_elementId4 "sa167"
      minerva_former_symbols "AGTR1B"
      minerva_name "AGTR1"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 570.0
      minerva_x2 2230.0
      minerva_x3 575.0
      minerva_x4 1751.5
      minerva_y 1080.0
      minerva_y2 1577.0
      minerva_y3 1610.0
      minerva_y4 299.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa102"
      species_meta_id "s_id_sa102"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 34
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "lnpep__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa152"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa152"
      minerva_name "LNPEP"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.11.3"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/4012"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_005575"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113441"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6656"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=LNPEP"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=LNPEP"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4012"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q9UIQ6"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q9UIQ6"
      minerva_ref_type__resource1 "EC__3.4.11.3"
      minerva_ref_type__resource10 "ENTREZ__4012"
      minerva_ref_type__resource2 "REFSEQ__NM_005575"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113441"
      minerva_ref_type__resource4 "HGNC__6656"
      minerva_ref_type__resource5 "HGNC_SYMBOL__LNPEP"
      minerva_ref_type__resource6 "HGNC_SYMBOL__LNPEP"
      minerva_ref_type__resource7 "ENTREZ__4012"
      minerva_ref_type__resource8 "UNIPROT__Q9UIQ6"
      minerva_ref_type__resource9 "UNIPROT__Q9UIQ6"
      minerva_type "Protein"
      minerva_x 1302.0
      minerva_y 1670.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa152"
      species_meta_id "s_id_sa152"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 35
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tmprss2__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa40"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa40"
      minerva_name "TMPRSS2"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11876"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_001135099"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TMPRSS2"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/O15393"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/O15393"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TMPRSS2"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/7113"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/7113"
      minerva_ref_link8 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.21.-"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000184012"
      minerva_ref_type__resource1 "HGNC__11876"
      minerva_ref_type__resource10 "REFSEQ__NM_001135099"
      minerva_ref_type__resource2 "HGNC_SYMBOL__TMPRSS2"
      minerva_ref_type__resource3 "UNIPROT__O15393"
      minerva_ref_type__resource4 "UNIPROT__O15393"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TMPRSS2"
      minerva_ref_type__resource6 "ENTREZ__7113"
      minerva_ref_type__resource7 "ENTREZ__7113"
      minerva_ref_type__resource8 "EC__3.4.21.-"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000184012"
      minerva_type "Protein"
      minerva_x 940.0
      minerva_y 290.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa40"
      species_meta_id "s_id_sa40"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 36
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "androgen__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa132"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa132"
      minerva_fullName "androgen"
      minerva_name "androgen"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:50113"
      minerva_ref_type__resource1 "CHEBI__CHEBI:50113"
      minerva_synonyms "Androgen; Androgene; androgene; androgenes; androgeno; androgenos; androgens"
      minerva_type "Simple molecule"
      minerva_x 1110.0
      minerva_y 379.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa132"
      species_meta_id "s_id_sa132"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 37
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mas1:agtr1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa9"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa9"
      minerva_name "MAS1:AGTR1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/15809376"
      minerva_ref_type__resource1 "PUBMED__15809376"
      minerva_type "Complex"
      minerva_x 570.0
      minerva_y 1230.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa9"
      species_meta_id "s_id_csa9"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 38
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "angiotensin_space_1_minus_5__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa32"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa32"
      minerva_fullName "Angiotensin (1-5)"
      minerva_name "angiotensin 1-5"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:80129"
      minerva_ref_type__resource1 "CHEBI__CHEBI:80129"
      minerva_type "Simple molecule"
      minerva_x 1952.0
      minerva_y 1241.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa32"
      species_meta_id "s_id_sa32"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 39
    zlevel -1

    cluster [
      cluster "s_id_ca8"
    ]
    graphics [
      x 700.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ren__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa36"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa36"
      minerva_name "REN"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=REN"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P00797"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/9958"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000143839"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/5972"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000537"
      minerva_ref_type__resource1 "HGNC_SYMBOL__REN"
      minerva_ref_type__resource2 "UNIPROT__P00797"
      minerva_ref_type__resource3 "HGNC__9958"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000143839"
      minerva_ref_type__resource5 "ENTREZ__5972"
      minerva_ref_type__resource6 "REFSEQ__NM_000537"
      minerva_type "Gene"
      minerva_x 192.0
      minerva_y 509.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca8"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa36"
      species_meta_id "s_id_sa36"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 40
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 800.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ctsd__lysosome__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa136"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa136"
      minerva_former_symbols "CPSD"
      minerva_name "CTSD"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_001909"
      minerva_ref_link10 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/2529"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000117984"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.23.5"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P07339"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P07339"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/1509"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/1509"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CTSD"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CTSD"
      minerva_ref_type__resource1 "REFSEQ__NM_001909"
      minerva_ref_type__resource10 "HGNC__2529"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000117984"
      minerva_ref_type__resource3 "EC__3.4.23.5"
      minerva_ref_type__resource4 "UNIPROT__P07339"
      minerva_ref_type__resource5 "UNIPROT__P07339"
      minerva_ref_type__resource6 "ENTREZ__1509"
      minerva_ref_type__resource7 "ENTREZ__1509"
      minerva_ref_type__resource8 "HGNC_SYMBOL__CTSD"
      minerva_ref_type__resource9 "HGNC_SYMBOL__CTSD"
      minerva_type "Protein"
      minerva_x 990.0
      minerva_y 605.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "lysosome"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa136"
      species_meta_id "s_id_sa136"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 41
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 900.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "pulmonary_space_edema__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa208"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa208"
      minerva_name "pulmonary edema"
      minerva_ref_link1 "http://id.nlm.nih.gov/mesh/D011654"
      minerva_ref_type__resource1 "MESH_2012__D011654"
      minerva_type "Phenotype"
      minerva_x 424.0
      minerva_y 1581.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa208"
      species_meta_id "s_id_sa208"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 42
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 1000.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cgp42112a__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa70"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa70"
      minerva_name "CGP42112A"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:147302"
      minerva_ref_type__resource1 "CHEBI__CHEBI:147302"
      minerva_type "Drug"
      minerva_x 1105.0
      minerva_y 1740.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa70"
      species_meta_id "s_id_sa70"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 43
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "enpep__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa162"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa162"
      minerva_name "ENPEP"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000138792"
      minerva_ref_link10 "http://purl.uniprot.org/uniprot/Q07075"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/2028"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/2028"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/3355"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ENPEP"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ENPEP"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_001379611"
      minerva_ref_link8 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.11.7"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q07075"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000138792"
      minerva_ref_type__resource10 "UNIPROT__Q07075"
      minerva_ref_type__resource2 "ENTREZ__2028"
      minerva_ref_type__resource3 "ENTREZ__2028"
      minerva_ref_type__resource4 "HGNC__3355"
      minerva_ref_type__resource5 "HGNC_SYMBOL__ENPEP"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ENPEP"
      minerva_ref_type__resource7 "REFSEQ__NM_001379611"
      minerva_ref_type__resource8 "EC__3.4.11.7"
      minerva_ref_type__resource9 "UNIPROT__Q07075"
      minerva_type "Protein"
      minerva_x 566.4615384615386
      minerva_y 1435.1538461538462
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa162"
      species_meta_id "s_id_sa162"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 44
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 1200.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "angiotensin_space_ii__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa95"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa95"
      minerva_fullName "angiotensin II"
      minerva_name "angiotensin II"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:48432"
      minerva_ref_type__resource1 "CHEBI__CHEBI:48432"
      minerva_synonyms "1-8-angiotensin I; ang II; angiotensina II; angiotensinum II"
      minerva_type "Simple molecule"
      minerva_x 1650.0
      minerva_y 146.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa95"
      species_meta_id "s_id_sa95"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 45
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 200.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa38"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa38"
      minerva_name "S"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P0DTC2"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/1489668"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P59594"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/43740568"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=S"
      minerva_ref_type__resource1 "UNIPROT__P0DTC2"
      minerva_ref_type__resource2 "ENTREZ__1489668"
      minerva_ref_type__resource3 "UNIPROT__P59594"
      minerva_ref_type__resource4 "ENTREZ__43740568"
      minerva_ref_type__resource5 "HGNC_SYMBOL__S"
      minerva_type "Protein"
      minerva_x 1050.0
      minerva_y 90.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa38"
      species_meta_id "s_id_sa38"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 46
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 300.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cognition__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa197"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa197"
      minerva_name "cognition"
      minerva_ref_link1 "http://id.nlm.nih.gov/mesh/D003071"
      minerva_ref_type__resource1 "MESH_2012__D003071"
      minerva_type "Phenotype"
      minerva_x 1188.0
      minerva_y 2119.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa197"
      species_meta_id "s_id_sa197"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 47
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 400.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ar234960__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa159"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa159"
      minerva_name "AR234960"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/146025955"
      minerva_ref_type__resource1 "PUBCHEM__146025955"
      minerva_type "Drug"
      minerva_x 1995.0
      minerva_y 1745.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa159"
      species_meta_id "s_id_sa159"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 48
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "anpep_space___cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa65"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa65"
      minerva_former_symbols "CD13; PEPN"
      minerva_name "ANPEP "
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/290"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.11.2"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/290"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/500"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ANPEP"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_001150"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P15144"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P15144"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ANPEP"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000166825"
      minerva_ref_type__resource1 "ENTREZ__290"
      minerva_ref_type__resource10 "EC__3.4.11.2"
      minerva_ref_type__resource2 "ENTREZ__290"
      minerva_ref_type__resource3 "HGNC__500"
      minerva_ref_type__resource4 "HGNC_SYMBOL__ANPEP"
      minerva_ref_type__resource5 "REFSEQ__NM_001150"
      minerva_ref_type__resource6 "UNIPROT__P15144"
      minerva_ref_type__resource7 "UNIPROT__P15144"
      minerva_ref_type__resource8 "HGNC_SYMBOL__ANPEP"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000166825"
      minerva_type "Protein"
      minerva_x 565.0
      minerva_y 1490.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa65"
      species_meta_id "s_id_sa65"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 49
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 600.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "abo_space_blood_space_group_space_system__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa192"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa192"
      minerva_name "ABO blood group system"
      minerva_ref_link1 "http://id.nlm.nih.gov/mesh/D000017"
      minerva_ref_type__resource1 "MESH_2012__D000017"
      minerva_type "Phenotype"
      minerva_x 360.0
      minerva_y 892.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa192"
      species_meta_id "s_id_sa192"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 50
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 700.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "camostat_space_mesilate__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa45"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa45"
      minerva_name "Camostat mesilate"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:135632"
      minerva_ref_type__resource1 "CHEBI__CHEBI:135632"
      minerva_type "Drug"
      minerva_x 935.0
      minerva_y 177.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa45"
      species_meta_id "s_id_sa45"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 51
    zlevel -1

    cluster [
      cluster "s_id_ca6"
    ]
    graphics [
      x 800.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ren__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa35"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa35"
      minerva_name "REN"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=REN"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.23.15"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=REN"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P00797"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P00797"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/9958"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000143839"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/5972"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_000537"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/5972"
      minerva_ref_type__resource1 "HGNC_SYMBOL__REN"
      minerva_ref_type__resource10 "EC__3.4.23.15"
      minerva_ref_type__resource2 "HGNC_SYMBOL__REN"
      minerva_ref_type__resource3 "UNIPROT__P00797"
      minerva_ref_type__resource4 "UNIPROT__P00797"
      minerva_ref_type__resource5 "HGNC__9958"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000143839"
      minerva_ref_type__resource7 "ENTREZ__5972"
      minerva_ref_type__resource8 "REFSEQ__NM_000537"
      minerva_ref_type__resource9 "ENTREZ__5972"
      minerva_type "Protein"
      minerva_x 322.0
      minerva_y 509.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca6"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa35"
      species_meta_id "s_id_sa35"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 52
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "angiotensin_space_1_minus_12__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa121"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa121"
      minerva_name "angiotensin 1-12"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/27465904"
      minerva_ref_type__resource1 "PUBMED__27465904"
      minerva_type "Simple molecule"
      minerva_x 975.0
      minerva_y 980.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa121"
      species_meta_id "s_id_sa121"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 53
    zlevel -1

    cluster [
      cluster "s_id_ca8"
    ]
    graphics [
      x 1000.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tmprss2__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa130"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa130"
      minerva_name "TMPRSS2"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11876"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TMPRSS2"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/O15393"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/7113"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000184012"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_001135099"
      minerva_ref_type__resource1 "HGNC__11876"
      minerva_ref_type__resource2 "HGNC_SYMBOL__TMPRSS2"
      minerva_ref_type__resource3 "UNIPROT__O15393"
      minerva_ref_type__resource4 "ENTREZ__7113"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000184012"
      minerva_ref_type__resource6 "REFSEQ__NM_001135099"
      minerva_type "Gene"
      minerva_x 940.0
      minerva_y 460.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca8"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa130"
      species_meta_id "s_id_sa130"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 54
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "viral_space_replication_space_cycle__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa166"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa166"
      minerva_name "viral replication cycle"
      minerva_ref_link1 "http://id.nlm.nih.gov/mesh/D014661"
      minerva_ref_type__resource1 "MESH_2012__D014661"
      minerva_type "Phenotype"
      minerva_x 1195.0
      minerva_y 340.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa166"
      species_meta_id "s_id_sa166"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 55
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 1200.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ace2,_space_soluble__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa73"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa73"
      minerva_name "ACE2, soluble"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000130234"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ACE2"
      minerva_ref_link2 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.17.23"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.17.-"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/13557"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/Q9BYF1"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/Q9BYF1"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/59272"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_001371415"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/59272"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000130234"
      minerva_ref_type__resource10 "HGNC_SYMBOL__ACE2"
      minerva_ref_type__resource2 "EC__3.4.17.23"
      minerva_ref_type__resource3 "EC__3.4.17.-"
      minerva_ref_type__resource4 "HGNC__13557"
      minerva_ref_type__resource5 "UNIPROT__Q9BYF1"
      minerva_ref_type__resource6 "UNIPROT__Q9BYF1"
      minerva_ref_type__resource7 "ENTREZ__59272"
      minerva_ref_type__resource8 "REFSEQ__NM_001371415"
      minerva_ref_type__resource9 "ENTREZ__59272"
      minerva_type "Protein"
      minerva_x 1515.0
      minerva_y 186.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa73"
      species_meta_id "s_id_sa73"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 56
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ace2:agtr1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa4"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa4"
      minerva_name "ACE2:AGTR1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/25225202"
      minerva_ref_type__resource1 "PUBMED__25225202"
      minerva_type "Complex"
      minerva_x 915.0
      minerva_y 1660.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa4"
      species_meta_id "s_id_csa4"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 57
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 300.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "aging__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa54"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa54"
      minerva_fullName "aging"
      minerva_name "aging"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0007568"
      minerva_ref_type__resource1 "GO__GO:0007568"
      minerva_type "Phenotype"
      minerva_x 2000.0
      minerva_y 182.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa54"
      species_meta_id "s_id_sa54"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 58
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "adam17__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa43"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa43"
      minerva_former_symbols "TACE"
      minerva_name "ADAM17"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000151694"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ADAM17"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/6868"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/6868"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_001382777"
      minerva_ref_link5 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.24.86"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/195"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P78536"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P78536"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ADAM17"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000151694"
      minerva_ref_type__resource10 "HGNC_SYMBOL__ADAM17"
      minerva_ref_type__resource2 "ENTREZ__6868"
      minerva_ref_type__resource3 "ENTREZ__6868"
      minerva_ref_type__resource4 "REFSEQ__NM_001382777"
      minerva_ref_type__resource5 "EC__3.4.24.86"
      minerva_ref_type__resource6 "HGNC__195"
      minerva_ref_type__resource7 "UNIPROT__P78536"
      minerva_ref_type__resource8 "UNIPROT__P78536"
      minerva_ref_type__resource9 "HGNC_SYMBOL__ADAM17"
      minerva_type "Protein"
      minerva_x 1600.0
      minerva_y 300.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa43"
      species_meta_id "s_id_sa43"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 59
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mme__cell__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa194"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa194"
      minerva_name "MME"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000196549"
      minerva_ref_link10 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7154"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/4311"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/4311"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.24.11"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MME"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MME"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_000902"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P08473"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P08473"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000196549"
      minerva_ref_type__resource10 "HGNC__7154"
      minerva_ref_type__resource2 "ENTREZ__4311"
      minerva_ref_type__resource3 "ENTREZ__4311"
      minerva_ref_type__resource4 "EC__3.4.24.11"
      minerva_ref_type__resource5 "HGNC_SYMBOL__MME"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MME"
      minerva_ref_type__resource7 "REFSEQ__NM_000902"
      minerva_ref_type__resource8 "UNIPROT__P08473"
      minerva_ref_type__resource9 "UNIPROT__P08473"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 2220.0
      minerva_y 696.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa194"
      species_meta_id "s_id_sa194"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 60
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "angiotensin_space_a__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa51"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa51"
      minerva_name "angiotensin A"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/91691124"
      minerva_ref_link2 "https://www.kegg.jp/entry/C20970"
      minerva_ref_type__resource1 "PUBCHEM__91691124"
      minerva_ref_type__resource2 "KEGG_COMPOUND__C20970"
      minerva_type "Simple molecule"
      minerva_x 1125.0
      minerva_y 1180.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa51"
      species_meta_id "s_id_sa51"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 62
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 800.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s134__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa113"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa113"
      minerva_name "s134"
      minerva_type "Degraded"
      minerva_x 1650.0
      minerva_y 70.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa113"
      species_meta_id "s_id_sa113"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 63
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "angiotensin_space_iii__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa62"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa62"
      minerva_fullName "Angiotensin III"
      minerva_name "angiotensin III"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:89666"
      minerva_ref_type__resource1 "CHEBI__CHEBI:89666"
      minerva_synonyms "(2S)-2-{[(2S)-1-[(2S)-2-[(2S,3S)-2-[(2S)-2-[(2S)-2-[(2S)-2-amino-5-[(diaminomethylidene)amino]pentanamido]-3-methylbutanamido]-3-(4-hydroxyphenyl)propanamido]-3-methylpentanamido]-3-(1H-imidazol-5-yl)propanoyl]pyrrolidin-2-yl]formamido}-3-phenylpropanoic acid; Des-Asp-1-Angiotensin II"
      minerva_type "Simple molecule"
      minerva_x 750.25
      minerva_y 1314.25
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa62"
      species_meta_id "s_id_sa62"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 64
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 1000.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "qgc001__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa161"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa161"
      minerva_name "QGC001"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/24337978"
      minerva_ref_type__resource1 "PUBMED__24337978"
      minerva_type "Drug"
      minerva_x 430.0
      minerva_y 1350.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa161"
      species_meta_id "s_id_sa161"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 65
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 1100.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "losartan__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa69"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa69"
      minerva_name "Losartan"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:6541"
      minerva_ref_type__resource1 "CHEBI__CHEBI:6541"
      minerva_type "Drug"
      minerva_x 450.0
      minerva_y 1700.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa69"
      species_meta_id "s_id_sa69"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 66
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1200.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "angiotensin_space_1_minus_4__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa138"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa138"
      minerva_name "angiotensin 1-4"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/22490446"
      minerva_ref_type__resource1 "PUBMED__22490446"
      minerva_type "Simple molecule"
      minerva_x 975.0
      minerva_y 873.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa138"
      species_meta_id "s_id_sa138"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 67
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "angiotensin_space_iv__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa63"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa63"
      minerva_fullName "Angiotensin IV"
      minerva_name "angiotensin IV"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:80127"
      minerva_ref_type__resource1 "CHEBI__CHEBI:80127"
      minerva_synonyms "Angiotensin II, 1-des-asn-2-arg"
      minerva_type "Simple molecule"
      minerva_x 750.5
      minerva_y 1453.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa63"
      species_meta_id "s_id_sa63"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 68
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 300.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sars_minus_cov_minus_2_space_infection__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa165"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa165"
      minerva_name "SARS-CoV-2 infection"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=2697049"
      minerva_ref_link2 "http://id.nlm.nih.gov/mesh/C000657245"
      minerva_ref_type__resource1 "TAXONOMY__2697049"
      minerva_ref_type__resource2 "MESH_2012__C000657245"
      minerva_type "Phenotype"
      minerva_x 1515.0
      minerva_y 105.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa165"
      species_meta_id "s_id_sa165"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 70
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 500.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ace2__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa175"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa175"
      minerva_name "ACE2"
      minerva_type "Degraded"
      minerva_x 1326.0
      minerva_y 210.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa175"
      species_meta_id "s_id_sa175"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 72
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ace2__cell__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa98"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa98"
      minerva_name "ACE2"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000130234"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ACE2"
      minerva_ref_link11 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ACE2"
      minerva_ref_link2 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.17.23"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.17.-"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/13557"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/Q9BYF1"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/Q9BYF1"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/59272"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_001371415"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/59272"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000130234"
      minerva_ref_type__resource10 "HGNC_SYMBOL__ACE2"
      minerva_ref_type__resource11 "HGNC_SYMBOL__ACE2"
      minerva_ref_type__resource2 "EC__3.4.17.23"
      minerva_ref_type__resource3 "EC__3.4.17.-"
      minerva_ref_type__resource4 "HGNC__13557"
      minerva_ref_type__resource5 "UNIPROT__Q9BYF1"
      minerva_ref_type__resource6 "UNIPROT__Q9BYF1"
      minerva_ref_type__resource7 "ENTREZ__59272"
      minerva_ref_type__resource8 "REFSEQ__NM_001371415"
      minerva_ref_type__resource9 "ENTREZ__59272"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 1470.0
      minerva_y 300.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa98"
      species_meta_id "s_id_sa98"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 73
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 800.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "agt__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa34"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa34"
      minerva_former_symbols "SERPINA8"
      minerva_name "AGT"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/P01019"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P01019"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGT"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGT"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000135744"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/333"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/183"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/183"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_000029"
      minerva_ref_type__resource1 "UNIPROT__P01019"
      minerva_ref_type__resource2 "UNIPROT__P01019"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGT"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGT"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000135744"
      minerva_ref_type__resource6 "HGNC__333"
      minerva_ref_type__resource7 "ENTREZ__183"
      minerva_ref_type__resource8 "ENTREZ__183"
      minerva_ref_type__resource9 "REFSEQ__NM_000029"
      minerva_type "Protein"
      minerva_x 750.0
      minerva_y 250.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa34"
      species_meta_id "s_id_sa34"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 74
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mas1__cell__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa27"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa27"
      minerva_name "MAS1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_002377"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000130368"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6899"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4142"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/4142"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAS1"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/P04201"
      minerva_ref_link8 "https://purl.uniprot.org/uniprot/P04201"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAS1"
      minerva_ref_type__resource1 "REFSEQ__NM_002377"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000130368"
      minerva_ref_type__resource3 "HGNC__6899"
      minerva_ref_type__resource4 "ENTREZ__4142"
      minerva_ref_type__resource5 "ENTREZ__4142"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MAS1"
      minerva_ref_type__resource7 "UNIPROT__P04201"
      minerva_ref_type__resource8 "UNIPROT__P04201"
      minerva_ref_type__resource9 "HGNC_SYMBOL__MAS1"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 1845.0
      minerva_y 1675.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa27"
      species_meta_id "s_id_sa27"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 75
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1000.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "klk1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa144"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa144"
      minerva_name "KLK1"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6357"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000167748"
      minerva_ref_link2 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.21.35"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_002257"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P06870"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P06870"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/3816"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/3816"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=KLK1"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=KLK1"
      minerva_ref_type__resource1 "HGNC__6357"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000167748"
      minerva_ref_type__resource2 "EC__3.4.21.35"
      minerva_ref_type__resource3 "REFSEQ__NM_002257"
      minerva_ref_type__resource4 "UNIPROT__P06870"
      minerva_ref_type__resource5 "UNIPROT__P06870"
      minerva_ref_type__resource6 "ENTREZ__3816"
      minerva_ref_type__resource7 "ENTREZ__3816"
      minerva_ref_type__resource8 "HGNC_SYMBOL__KLK1"
      minerva_ref_type__resource9 "HGNC_SYMBOL__KLK1"
      minerva_type "Protein"
      minerva_x 695.0
      minerva_y 810.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa144"
      species_meta_id "s_id_sa144"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 76
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ace2_minus_spike_space_complex__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa5"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa5"
      minerva_name "ACE2-Spike complex"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/32275855"
      minerva_ref_type__resource1 "PUBMED__32275855"
      minerva_type "Complex"
      minerva_x 1050.0
      minerva_y 290.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa5"
      species_meta_id "s_id_csa5"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 77
    zlevel -1

    cluster [
      cluster "s_id_ca8"
    ]
    graphics [
      x 1200.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "agt__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa174"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa174"
      minerva_former_symbols "SERPINA8"
      minerva_name "AGT"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/P01019"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGT"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000135744"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/333"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/183"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000029"
      minerva_ref_type__resource1 "UNIPROT__P01019"
      minerva_ref_type__resource2 "HGNC_SYMBOL__AGT"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000135744"
      minerva_ref_type__resource4 "HGNC__333"
      minerva_ref_type__resource5 "ENTREZ__183"
      minerva_ref_type__resource6 "REFSEQ__NM_000029"
      minerva_type "Gene"
      minerva_x 860.0
      minerva_y 430.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca8"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa174"
      species_meta_id "s_id_sa174"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 78
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 200.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "neurodegeneration__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa190"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa190"
      minerva_name "neurodegeneration"
      minerva_ref_link1 "http://id.nlm.nih.gov/mesh/D009410"
      minerva_ref_type__resource1 "MESH_2012__D009410"
      minerva_type "Phenotype"
      minerva_x 1400.0
      minerva_y 1756.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa190"
      species_meta_id "s_id_sa190"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 79
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mme__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa39"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa39"
      minerva_name "MME"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000196549"
      minerva_ref_link10 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7154"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/4311"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/4311"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.24.11"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MME"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MME"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_000902"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P08473"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P08473"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000196549"
      minerva_ref_type__resource10 "HGNC__7154"
      minerva_ref_type__resource2 "ENTREZ__4311"
      minerva_ref_type__resource3 "ENTREZ__4311"
      minerva_ref_type__resource4 "EC__3.4.24.11"
      minerva_ref_type__resource5 "HGNC_SYMBOL__MME"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MME"
      minerva_ref_type__resource7 "REFSEQ__NM_000902"
      minerva_ref_type__resource8 "UNIPROT__P08473"
      minerva_ref_type__resource9 "UNIPROT__P08473"
      minerva_type "Protein"
      minerva_x 2220.0
      minerva_y 800.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa39"
      species_meta_id "s_id_sa39"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 80
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s115__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa99"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa99"
      minerva_name "s115"
      minerva_type "Degraded"
      minerva_x 635.0
      minerva_y 960.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa99"
      species_meta_id "s_id_sa99"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 81
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 500.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "fibrosis__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa171"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa171"
      minerva_name "fibrosis"
      minerva_ref_link1 "http://id.nlm.nih.gov/mesh/D005355"
      minerva_ref_type__resource1 "MESH_2012__D005355"
      minerva_type "Phenotype"
      minerva_x 1188.0
      minerva_y 1880.25
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa171"
      species_meta_id "s_id_sa171"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 82
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "lnpep__cell__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa156"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa156"
      minerva_name "LNPEP"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.11.3"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/4012"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_005575"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113441"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6656"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=LNPEP"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=LNPEP"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4012"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q9UIQ6"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q9UIQ6"
      minerva_ref_type__resource1 "EC__3.4.11.3"
      minerva_ref_type__resource10 "ENTREZ__4012"
      minerva_ref_type__resource2 "REFSEQ__NM_005575"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113441"
      minerva_ref_type__resource4 "HGNC__6656"
      minerva_ref_type__resource5 "HGNC_SYMBOL__LNPEP"
      minerva_ref_type__resource6 "HGNC_SYMBOL__LNPEP"
      minerva_ref_type__resource7 "ENTREZ__4012"
      minerva_ref_type__resource8 "UNIPROT__Q9UIQ6"
      minerva_ref_type__resource9 "UNIPROT__Q9UIQ6"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 1460.0
      minerva_y 1670.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa156"
      species_meta_id "s_id_sa156"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 83
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 700.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "thrombosis__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa172"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa172"
      minerva_name "thrombosis"
      minerva_ref_link1 "http://id.nlm.nih.gov/mesh/D013927"
      minerva_ref_type__resource1 "MESH_2012__D013927"
      minerva_type "Phenotype"
      minerva_x 1189.0
      minerva_y 1940.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa172"
      species_meta_id "s_id_sa172"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 84
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 800.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "agtr2__cell__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa25"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa25"
      minerva_name "AGTR2"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000180772"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR2"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR2"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/338"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_000686"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P50052"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/P50052"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/186"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/186"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000180772"
      minerva_ref_type__resource2 "HGNC_SYMBOL__AGTR2"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR2"
      minerva_ref_type__resource4 "HGNC__338"
      minerva_ref_type__resource5 "REFSEQ__NM_000686"
      minerva_ref_type__resource6 "UNIPROT__P50052"
      minerva_ref_type__resource7 "UNIPROT__P50052"
      minerva_ref_type__resource8 "ENTREZ__186"
      minerva_ref_type__resource9 "ENTREZ__186"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 1188.0
      minerva_y 1675.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa25"
      species_meta_id "s_id_sa25"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 85
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "angiotensin_space_3_minus_7__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa160"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa160"
      minerva_name "angiotensin 3-7"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/30934934"
      minerva_ref_type__resource1 "PUBMED__30934934"
      minerva_type "Simple molecule"
      minerva_x 1445.0
      minerva_y 1447.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa160"
      species_meta_id "s_id_sa160"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 86
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 1000.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "lisinopril__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa66"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa66"
      minerva_name "Lisinopril"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:43755"
      minerva_ref_type__resource1 "CHEBI__CHEBI:43755"
      minerva_type "Drug"
      minerva_x 440.0
      minerva_y 830.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa66"
      species_meta_id "s_id_sa66"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 87
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mrgprd__cell__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa50"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa50"
      minerva_name "MRGPRD"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_198923"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/116512"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/116512"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MRGPRD"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MRGPRD"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/Q8TDS7"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/Q8TDS7"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/29626"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000172938"
      minerva_ref_type__resource1 "REFSEQ__NM_198923"
      minerva_ref_type__resource2 "ENTREZ__116512"
      minerva_ref_type__resource3 "ENTREZ__116512"
      minerva_ref_type__resource4 "HGNC_SYMBOL__MRGPRD"
      minerva_ref_type__resource5 "HGNC_SYMBOL__MRGPRD"
      minerva_ref_type__resource6 "UNIPROT__Q8TDS7"
      minerva_ref_type__resource7 "UNIPROT__Q8TDS7"
      minerva_ref_type__resource8 "HGNC__29626"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000172938"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 1575.0
      minerva_y 1675.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa50"
      species_meta_id "s_id_sa50"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 88
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1200.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "angiotensin_space_ii__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa23"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa23"
      minerva_fullName "angiotensin II"
      minerva_name "angiotensin II"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:48432"
      minerva_ref_type__resource1 "CHEBI__CHEBI:48432"
      minerva_synonyms "1-8-angiotensin I; ang II; angiotensina II; angiotensinum II"
      minerva_type "Simple molecule"
      minerva_x 750.0
      minerva_y 1020.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa23"
      species_meta_id "s_id_sa23"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 89
    zlevel -1

    graphics [
      x 200.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re67"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 1105.3947368421052
      minerva_y 1687.1447368421052
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re67"
      reaction_meta_id "re67"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 93
    zlevel -1

    graphics [
      x 300.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re186"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re186"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 2230.0
      minerva_y 1022.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re186"
      reaction_meta_id "re186"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 98
    zlevel -1

    graphics [
      x 400.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re193"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re193"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 2210.0
      minerva_y 1022.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re193"
      reaction_meta_id "re193"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 102
    zlevel -1

    graphics [
      x 500.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re145"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re145"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "Negative influence"
      minerva_x 1371.6690121155636
      minerva_y 1822.9813140726935
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re145"
      reaction_meta_id "re145"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 105
    zlevel -1

    graphics [
      x 600.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re112"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "Known transition omitted"
      minerva_x 940.0
      minerva_y 378.75
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re112"
      reaction_meta_id "re112"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 109
    zlevel -1

    graphics [
      x 700.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re153"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re153"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "Known transition omitted"
      minerva_x 1280.000000000001
      minerva_y 403.0000000000007
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re153"
      reaction_meta_id "re153"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 115
    zlevel -1

    graphics [
      x 800.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re81"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 1035.3279123450875
      minerva_y 1639.772201514505
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re81"
      reaction_meta_id "re81"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 120
    zlevel -1

    graphics [
      x 900.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re192"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re192"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 2220.0
      minerva_y 748.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re192"
      reaction_meta_id "re192"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 124
    zlevel -1

    graphics [
      x 1000.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re129"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re129"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 768.8387315186709
      minerva_y 509.87799385678187
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re129"
      reaction_meta_id "re129"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 129
    zlevel -1

    graphics [
      x 1100.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re203"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re203"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "Trigger"
      minerva_x 512.0
      minerva_y 1626.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re203"
      reaction_meta_id "re203"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 132
    zlevel -1

    graphics [
      x 1200.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re150"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re150"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "Known transition omitted"
      minerva_x 1025.0
      minerva_y 177.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re150"
      reaction_meta_id "re150"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 137
    zlevel -1

    graphics [
      x 200.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re98"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "Heterodimer association"
      minerva_x 830.0000000000005
      minerva_y 1630.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re98"
      reaction_meta_id "re98"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 142
    zlevel -1

    graphics [
      x 300.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re181"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re181"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "Trigger"
      minerva_x 843.8781682939398
      minerva_y 2058.847035104176
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re181"
      reaction_meta_id "re181"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 145
    zlevel -1

    graphics [
      x 400.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re149"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re149"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 1095.6566871440605
      minerva_y 1446.9015199668056
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re149"
      reaction_meta_id "re149"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 148
    zlevel -1

    graphics [
      x 500.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re144"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re144"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "Negative influence"
      minerva_x 1364.0083844598034
      minerva_y 2051.020569644534
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re144"
      reaction_meta_id "re144"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 151
    zlevel -1

    graphics [
      x 600.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re132"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 751.2501014445704
      minerva_y 864.7387396526537
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re132"
      reaction_meta_id "re132"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 155
    zlevel -1

    graphics [
      x 700.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re30"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 1492.5000000000002
      minerva_y 967.4999999999997
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re30"
      reaction_meta_id "re30"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 159
    zlevel -1

    graphics [
      x 800.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re134"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re134"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 1475.0
      minerva_y 974.9999999999997
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re134"
      reaction_meta_id "re134"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 163
    zlevel -1

    graphics [
      x 900.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re103"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 1650.0
      minerva_y 103.75
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re103"
      reaction_meta_id "re103"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 167
    zlevel -1

    graphics [
      x 1000.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re138"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re138"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 1105.3947368421052
      minerva_y 1663.1447368421052
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re138"
      reaction_meta_id "re138"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 171
    zlevel -1

    graphics [
      x 1100.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re43"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 1572.3249850304153
      minerva_y 1098.8058535285863
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re43"
      reaction_meta_id "re43"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 174
    zlevel -1

    graphics [
      x 1200.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re167"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re167"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "Known transition omitted"
      minerva_x 813.75
      minerva_y 343.75
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re167"
      reaction_meta_id "re167"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 178
    zlevel -1

    graphics [
      x 200.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re62"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 750.375
      minerva_y 1383.875
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re62"
      reaction_meta_id "re62"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 182
    zlevel -1

    graphics [
      x 300.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re135"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re135"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 554.5397923875435
      minerva_y 1653.9878892733573
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re135"
      reaction_meta_id "re135"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 186
    zlevel -1

    graphics [
      x 400.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re25"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 1819.67164209259
      minerva_y 1129.1032384259086
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re25"
      reaction_meta_id "re25"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 190
    zlevel -1

    graphics [
      x 500.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re195"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re195"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "Negative influence"
      minerva_x 843.8755799943663
      minerva_y 2118.86297244006
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re195"
      reaction_meta_id "re195"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 193
    zlevel -1

    graphics [
      x 600.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re182"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re182"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "Trigger"
      minerva_x 843.8568739039663
      minerva_y 1879.9413753921197
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re182"
      reaction_meta_id "re182"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 196
    zlevel -1

    graphics [
      x 700.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re161"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re161"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "Negative influence"
      minerva_x 1554.5893781478767
      minerva_y 1939.787612662919
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re161"
      reaction_meta_id "re161"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 199
    zlevel -1

    graphics [
      x 800.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re142"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re142"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "Negative influence"
      minerva_x 1547.737419428397
      minerva_y 1832.0545465884368
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re142"
      reaction_meta_id "re142"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 202
    zlevel -1

    graphics [
      x 900.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re139"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re139"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 1116.894570843678
      minerva_y 1629.1591145284974
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re139"
      reaction_meta_id "re139"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 206
    zlevel -1

    graphics [
      x 1000.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re94"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 661.6069995808243
      minerva_y 981.0285192279152
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re94"
      reaction_meta_id "re94"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 210
    zlevel -1

    graphics [
      x 1100.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re46"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 1331.25
      minerva_y 1180.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re46"
      reaction_meta_id "re46"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 214
    zlevel -1

    graphics [
      x 1200.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re109"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 859.0354517530827
      minerva_y 1000.1913417161826
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re109"
      reaction_meta_id "re109"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 218
    zlevel -1

    graphics [
      x 200.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re196"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re196"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "Trigger"
      minerva_x 1371.6002075634676
      minerva_y 2118.949722447925
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re196"
      reaction_meta_id "re196"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 221
    zlevel -1

    graphics [
      x 300.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re127"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re127"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 1936.0000000000005
      minerva_y 1700.0000000000002
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re127"
      reaction_meta_id "re127"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 225
    zlevel -1

    graphics [
      x 400.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re133"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re133"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 1482.5
      minerva_y 949.9999999999993
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re133"
      reaction_meta_id "re133"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 229
    zlevel -1

    graphics [
      x 500.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re191"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re191"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 586.3325311728204
      minerva_y 1400.5760225287631
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re191"
      reaction_meta_id "re191"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 233
    zlevel -1

    graphics [
      x 600.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re141"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re141"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 1669.5
      minerva_y 1679.0000000000014
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re141"
      reaction_meta_id "re141"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 237
    zlevel -1

    graphics [
      x 700.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re151"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re151"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 1381.0
      minerva_y 1674.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re151"
      reaction_meta_id "re151"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 241
    zlevel -1

    graphics [
      x 800.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re184"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re184"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "Trigger"
      minerva_x 843.824997344323
      minerva_y 1999.995192344373
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re184"
      reaction_meta_id "re184"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 244
    zlevel -1

    graphics [
      x 900.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re117"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 550.0
      minerva_y 877.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re117"
      reaction_meta_id "re117"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 248
    zlevel -1

    graphics [
      x 1000.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:10969042"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re17"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 1394.749365134112
      minerva_y 765.7005442671784
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re17"
      reaction_meta_id "re17"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 252
    zlevel -1

    graphics [
      x 1100.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re160"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re160"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "Negative influence"
      minerva_x 1554.1540340959464
      minerva_y 1999.9555900038072
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re160"
      reaction_meta_id "re160"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 255
    zlevel -1

    graphics [
      x 1200.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re148"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re148"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 839.9999999999999
      minerva_y 1302.4999999999993
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re148"
      reaction_meta_id "re148"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 258
    zlevel -1

    graphics [
      x 200.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re123"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re123"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 1129.0
      minerva_y 979.9999999999998
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re123"
      reaction_meta_id "re123"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 262
    zlevel -1

    graphics [
      x 300.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re90"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 1656.5
      minerva_y 1655.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re90"
      reaction_meta_id "re90"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 266
    zlevel -1

    graphics [
      x 400.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re198"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re198"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 2158.132022937758
      minerva_y 1572.4934787983457
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re198"
      reaction_meta_id "re198"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 270
    zlevel -1

    graphics [
      x 500.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re18"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 1669.5
      minerva_y 893.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re18"
      reaction_meta_id "re18"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 275
    zlevel -1

    graphics [
      x 600.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re136"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re136"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 627.5
      minerva_y 1650.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re136"
      reaction_meta_id "re136"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 279
    zlevel -1

    graphics [
      x 700.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re200"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re200"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "Known transition omitted"
      minerva_x 1365.0
      minerva_y 444.99999999999966
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re200"
      reaction_meta_id "re200"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 284
    zlevel -1

    graphics [
      x 800.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re188"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re188"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 570.0
      minerva_y 887.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re188"
      reaction_meta_id "re188"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 288
    zlevel -1

    graphics [
      x 900.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re68"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "Transport"
      minerva_x 403.5
      minerva_y 509.7499999999997
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re68"
      reaction_meta_id "re68"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 291
    zlevel -1

    graphics [
      x 1000.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re102"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "Heterodimer association"
      minerva_x 1050.0
      minerva_y 195.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re102"
      reaction_meta_id "re102"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 295
    zlevel -1

    graphics [
      x 1100.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re128"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re128"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 546.2307692307693
      minerva_y 1389.076923076923
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re128"
      reaction_meta_id "re128"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 299
    zlevel -1

    graphics [
      x 1200.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re131"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re131"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 769.0887315186709
      minerva_y 927.3779938567818
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re131"
      reaction_meta_id "re131"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 303
    zlevel -1

    graphics [
      x 200.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re114"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 975.0
      minerva_y 943.75
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re114"
      reaction_meta_id "re114"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 307
    zlevel -1

    graphics [
      x 300.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re146"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re146"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "Negative influence"
      minerva_x 1412.6460778730086
      minerva_y 1814.7806991345901
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re146"
      reaction_meta_id "re146"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 310
    zlevel -1

    graphics [
      x 400.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re140"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re140"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 1935.0000000000005
      minerva_y 1650.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re140"
      reaction_meta_id "re140"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 314
    zlevel -1

    graphics [
      x 500.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re197"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re197"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "Known transition omitted"
      minerva_x 648.5091392368182
      minerva_y 490.1611200116509
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re197"
      reaction_meta_id "re197"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 319
    zlevel -1

    graphics [
      x 600.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re122"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re122"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 1371.0
      minerva_y 1650.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re122"
      reaction_meta_id "re122"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 323
    zlevel -1

    graphics [
      x 700.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re168"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re168"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 1329.5307337294603
      minerva_y 252.69551813004514
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re168"
      reaction_meta_id "re168"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 327
    zlevel -1

    graphics [
      x 800.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re190"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re190"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 1380.0000000000007
      minerva_y 312.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re190"
      reaction_meta_id "re190"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 332
    zlevel -1

    graphics [
      x 900.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re183"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re183"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "Trigger"
      minerva_x 843.8810510099148
      minerva_y 1822.9175607445936
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re183"
      reaction_meta_id "re183"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 335
    zlevel -1

    graphics [
      x 1000.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re27"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "Known transition omitted"
      minerva_x 258.0
      minerva_y 509.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re27"
      reaction_meta_id "re27"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 339
    zlevel -1

    graphics [
      x 1100.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:22536270"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re19"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 1205.9998683201443
      minerva_y 1020.5274723751534
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re19"
      reaction_meta_id "re19"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 344
    zlevel -1

    graphics [
      x 1200.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re115"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "Heterodimer association"
      minerva_x 594.8225152129822
      minerva_y 1078.8995943204782
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re115"
      reaction_meta_id "re115"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 348
    zlevel -1

    graphics [
      x 200.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re119"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 853.1432950360409
      minerva_y 873.9348218078358
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re119"
      reaction_meta_id "re119"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 352
    zlevel -1

    graphics [
      x 300.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re185"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re185"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "Negative influence"
      minerva_x 1410.0
      minerva_y 1710.25
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re185"
      reaction_meta_id "re185"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 355
    zlevel -1

    graphics [
      x 400.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re77"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "Negative influence"
      minerva_x 1188.0
      minerva_y 1750.375
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re77"
      reaction_meta_id "re77"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 358
    zlevel -1

    graphics [
      x 500.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re202"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re202"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "Trigger"
      minerva_x 512.0
      minerva_y 1585.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re202"
      reaction_meta_id "re202"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 361
    zlevel -1

    graphics [
      x 600.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re42"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 1952.5000000000002
      minerva_y 1679.0000000000005
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re42"
      reaction_meta_id "re42"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 365
    zlevel -1

    graphics [
      x 700.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re152"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re152"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "Trigger"
      minerva_x 1123.0
      minerva_y 305.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re152"
      reaction_meta_id "re152"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 368
    zlevel -1

    graphics [
      x 800.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re189"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re189"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "Known transition omitted"
      minerva_x 1349.9999999999998
      minerva_y 376.50000000000006
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re189"
      reaction_meta_id "re189"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 372
    zlevel -1

    graphics [
      x 900.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re45"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 942.6483528439626
      minerva_y 1097.323507372223
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re45"
      reaction_meta_id "re45"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 375
    zlevel -1

    graphics [
      x 1000.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re61"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 750.125
      minerva_y 1166.625
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re61"
      reaction_meta_id "re61"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 379
    zlevel -1

    graphics [
      x 1100.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "PMID:190881"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re16"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 730.9112684813291
      minerva_y 924.8779938567818
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re16"
      reaction_meta_id "re16"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 383
    zlevel -1

    graphics [
      x 1200.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re194"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re194"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 2025.0
      minerva_y 1620.203125
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re194"
      reaction_meta_id "re194"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 387
    zlevel -1

    graphics [
      x 200.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re165"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re165"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "Negative influence"
      minerva_x 1265.8577612638928
      minerva_y 1871.8163224392492
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re165"
      reaction_meta_id "re165"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 390
    zlevel -1

    graphics [
      x 300.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re28"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 733.1612684813291
      minerva_y 509.87799385678187
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re28"
      reaction_meta_id "re28"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 394
    zlevel -1

    graphics [
      x 400.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re177"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re177"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "Trigger"
      minerva_x 844.3920426857876
      minerva_y 1939.81199033163
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re177"
      reaction_meta_id "re177"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 397
    zlevel -1

    graphics [
      x 500.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re91"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 1650.0
      minerva_y 186.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re91"
      reaction_meta_id "re91"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 401
    zlevel -1

    graphics [
      x 600.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re147"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re147"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 1548.32835790741
      minerva_y 1232.1032384259086
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re147"
      reaction_meta_id "re147"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 404
    zlevel -1

    graphics [
      x 700.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re116"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 589.9999999999998
      minerva_y 1019.4999999999997
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re116"
      reaction_meta_id "re116"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 409
    zlevel -1

    graphics [
      x 800.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re159"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re159"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "Negative influence"
      minerva_x 1547.6795629736666
      minerva_y 1889.2153204310807
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re159"
      reaction_meta_id "re159"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 412
    zlevel -1

    graphics [
      x 900.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re75"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "Negative influence"
      minerva_x 1554.1123256832634
      minerva_y 2058.8201553021754
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re75"
      reaction_meta_id "re75"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 415
    zlevel -1

    graphics [
      x 1000.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re93"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 1400.0
      minerva_y 296.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re93"
      reaction_meta_id "re93"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 419
    zlevel -1

    graphics [
      x 1100.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re74"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "Negative influence"
      minerva_x 1411.7975523916639
      minerva_y 2068.5706784966387
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re74"
      reaction_meta_id "re74"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 422
    zlevel -1

    graphics [
      x 1200.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re96"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 647.6652628718934
      minerva_y 1606.388534166554
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re96"
      reaction_meta_id "re96"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 428
    zlevel -1

    graphics [
      x 200.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re201"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re201"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 2250.0000000000014
      minerva_y 1004.5000000000014
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re201"
      reaction_meta_id "re201"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 432
    zlevel -1

    graphics [
      x 300.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re69"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/336"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_000685"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=AGTR1"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P30556"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000144891"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/185"
      minerva_ref_type__resource1 "HGNC__336"
      minerva_ref_type__resource2 "REFSEQ__NM_000685"
      minerva_ref_type__resource3 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__AGTR1"
      minerva_ref_type__resource5 "UNIPROT__P30556"
      minerva_ref_type__resource6 "UNIPROT__P30556"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000144891"
      minerva_ref_type__resource8 "ENTREZ__185"
      minerva_ref_type__resource9 "ENTREZ__185"
      minerva_type "State transition"
      minerva_x 1428.2142857142858
      minerva_y 247.25
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re69"
      reaction_meta_id "re69"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 437
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "aldosterone__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa195"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa195"
      minerva_fullName "aldosterone"
      minerva_name "aldosterone"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:27584"
      minerva_ref_type__resource1 "CHEBI__CHEBI:27584"
      minerva_synonyms "(+)-aldosterone; (11beta)-11,21-dihydroxy-3,20-dioxopregn-4-en-18-al; 11beta,21-Dihydroxy-3,20-dioxo-4-pregnen-18-al; ALDOSTERONE; Aldosterone; aldosterone"
      minerva_type "Simple molecule"
      minerva_x 2140.0
      minerva_y 1622.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation "Annotation [about=\"#s_id_sa195\", history=]"
      species_id "s_id_sa195"
      species_meta_id "s_id_sa195"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
      species_notes "XMLNode 'notes' [childElements size=1]"
    ]
  ]
  node [
    id 444
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 350.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mas1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa141"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa141"
      minerva_elementId2 "sa77"
      minerva_name "MAS1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_002377"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000130368"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6899"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4142"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/4142"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAS1"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/P04201"
      minerva_ref_link8 "https://purl.uniprot.org/uniprot/P04201"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAS1"
      minerva_ref_type__resource1 "REFSEQ__NM_002377"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000130368"
      minerva_ref_type__resource3 "HGNC__6899"
      minerva_ref_type__resource4 "ENTREZ__4142"
      minerva_ref_type__resource5 "ENTREZ__4142"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MAS1"
      minerva_ref_type__resource7 "UNIPROT__P04201"
      minerva_ref_type__resource8 "UNIPROT__P04201"
      minerva_ref_type__resource9 "HGNC_SYMBOL__MAS1"
      minerva_type "Protein"
      minerva_x 570.0
      minerva_x2 2025.0
      minerva_y 1130.0
      minerva_y2 1675.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation "Annotation [about=\"#s_id_sa141\", history=]"
      species_id "s_id_sa141"
      species_meta_id "s_id_sa141"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
      species_notes "XMLNode 'notes' [childElements size=1]"
    ]
  ]
  edge [
    id 90
    source 8
    target 89
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa79"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 91
    source 89
    target 84
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa25"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 92
    source 42
    target 89
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR0"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa70"
    ]
  ]
  edge [
    id 94
    source 4
    target 93
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa29"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 95
    source 93
    target 10
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa146"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 96
    source 57
    target 93
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR1"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa54"
    ]
  ]
  edge [
    id 97
    source 7
    target 93
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR2"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa193"
    ]
  ]
  edge [
    id 99
    source 4
    target 98
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa29"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 100
    source 98
    target 10
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa146"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 103
    source 82
    target 102
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa156"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 104
    source 102
    target 26
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa56"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 106
    source 53
    target 105
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa130"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 107
    source 105
    target 35
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa40"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 108
    source 36
    target 105
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR4"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa132"
    ]
  ]
  edge [
    id 110
    source 22
    target 109
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa168"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 111
    source 109
    target 27
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa30"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 112
    source 9
    target 109
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR5"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa200"
    ]
  ]
  edge [
    id 113
    source 45
    target 109
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR6"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa38"
    ]
  ]
  edge [
    id 114
    source 16
    target 109
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR7"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa201"
    ]
  ]
  edge [
    id 116
    source 8
    target 115
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa79"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 117
    source 115
    target 84
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa25"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 118
    source 88
    target 115
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR8"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa23"
    ]
  ]
  edge [
    id 119
    source 60
    target 115
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR9"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa51"
    ]
  ]
  edge [
    id 121
    source 79
    target 120
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa39"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 122
    source 120
    target 59
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa194"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 123
    source 7
    target 120
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR10"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa193"
    ]
  ]
  edge [
    id 125
    source 73
    target 124
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa34"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 126
    source 124
    target 24
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa21"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 127
    source 40
    target 124
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR11"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa136"
    ]
  ]
  edge [
    id 128
    source 5
    target 124
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR12"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa137"
    ]
  ]
  edge [
    id 130
    source 33
    target 129
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa102"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 131
    source 129
    target 21
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa209"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 133
    source 45
    target 132
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa38"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 134
    source 132
    target 76
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa5"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 135
    source 50
    target 132
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR13"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa45"
    ]
  ]
  edge [
    id 136
    source 35
    target 132
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR14"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa40"
    ]
  ]
  edge [
    id 138
    source 12
    target 137
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa26"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 139
    source 27
    target 137
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa30"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 140
    source 137
    target 56
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa4"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 141
    source 88
    target 137
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR15"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa23"
    ]
  ]
  edge [
    id 143
    source 33
    target 142
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa102"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 144
    source 142
    target 20
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa61"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 146
    source 67
    target 145
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa63"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 147
    source 145
    target 85
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa160"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 149
    source 82
    target 148
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa156"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 150
    source 148
    target 20
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa61"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 152
    source 24
    target 151
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa21"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 153
    source 151
    target 88
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa23"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 154
    source 75
    target 151
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR16"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa144"
    ]
  ]
  edge [
    id 156
    source 24
    target 155
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa21"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 157
    source 155
    target 19
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa24"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 158
    source 79
    target 155
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR17"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa39"
    ]
  ]
  edge [
    id 160
    source 24
    target 159
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa21"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 161
    source 159
    target 19
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa24"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 162
    source 28
    target 159
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR18"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa120"
    ]
  ]
  edge [
    id 164
    source 62
    target 163
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa113"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 165
    source 163
    target 44
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa95"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 166
    source 68
    target 163
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR19"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa165"
    ]
  ]
  edge [
    id 168
    source 8
    target 167
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa79"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 169
    source 167
    target 84
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa25"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 170
    source 19
    target 167
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR20"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa24"
    ]
  ]
  edge [
    id 172
    source 19
    target 171
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa24"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 173
    source 171
    target 31
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa49"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 175
    source 77
    target 174
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa174"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 176
    source 174
    target 73
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa34"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 177
    source 23
    target 174
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR21"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa134"
    ]
  ]
  edge [
    id 179
    source 63
    target 178
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa62"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 180
    source 178
    target 67
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa63"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 181
    source 48
    target 178
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR22"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa65"
    ]
  ]
  edge [
    id 183
    source 12
    target 182
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa26"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 184
    source 182
    target 33
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa102"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 185
    source 65
    target 182
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR23"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa69"
    ]
  ]
  edge [
    id 187
    source 19
    target 186
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa24"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 188
    source 186
    target 38
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa32"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 189
    source 10
    target 186
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR24"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa146"
    ]
  ]
  edge [
    id 191
    source 33
    target 190
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa102"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 192
    source 190
    target 46
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa197"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 194
    source 33
    target 193
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa102"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 195
    source 193
    target 81
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa171"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 197
    source 74
    target 196
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa27"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 198
    source 196
    target 83
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa172"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 200
    source 74
    target 199
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa27"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 201
    source 199
    target 26
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa56"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 203
    source 8
    target 202
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa79"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 204
    source 202
    target 84
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa25"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 205
    source 2
    target 202
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR25"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa22"
    ]
  ]
  edge [
    id 207
    source 80
    target 206
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa99"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 208
    source 206
    target 88
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa23"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 209
    source 45
    target 206
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR26"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa38"
    ]
  ]
  edge [
    id 211
    source 60
    target 210
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa51"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 212
    source 210
    target 31
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa49"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 213
    source 27
    target 210
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR27"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa30"
    ]
  ]
  edge [
    id 215
    source 52
    target 214
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa121"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 216
    source 214
    target 88
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa23"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 217
    source 3
    target 214
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR28"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa122"
    ]
  ]
  edge [
    id 219
    source 82
    target 218
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa156"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 220
    source 218
    target 46
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa197"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 223
    source 221
    target 74
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa27"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 224
    source 47
    target 221
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR29"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa159"
    ]
  ]
  edge [
    id 226
    source 24
    target 225
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa21"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 227
    source 225
    target 19
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa24"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 228
    source 30
    target 225
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR30"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa48"
    ]
  ]
  edge [
    id 230
    source 43
    target 229
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa162"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 231
    source 229
    target 13
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa64"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 232
    source 7
    target 229
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR31"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa193"
    ]
  ]
  edge [
    id 234
    source 29
    target 233
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa94"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 235
    source 233
    target 87
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa50"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 236
    source 19
    target 233
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR32"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa24"
    ]
  ]
  edge [
    id 238
    source 34
    target 237
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa152"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 239
    source 237
    target 82
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa156"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 240
    source 85
    target 237
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR33"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa160"
    ]
  ]
  edge [
    id 242
    source 33
    target 241
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa102"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 243
    source 241
    target 18
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa157"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 245
    source 4
    target 244
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa29"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 246
    source 244
    target 10
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa146"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 247
    source 86
    target 244
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR34"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa66"
    ]
  ]
  edge [
    id 249
    source 24
    target 248
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa21"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 250
    source 248
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa22"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 251
    source 27
    target 248
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR35"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa30"
    ]
  ]
  edge [
    id 253
    source 74
    target 252
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa27"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 254
    source 252
    target 18
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa157"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 256
    source 88
    target 255
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa23"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 257
    source 255
    target 85
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa160"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 259
    source 52
    target 258
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa121"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 260
    source 258
    target 19
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa24"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 261
    source 79
    target 258
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR36"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa39"
    ]
  ]
  edge [
    id 263
    source 29
    target 262
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa94"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 264
    source 262
    target 87
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa50"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 265
    source 31
    target 262
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR37"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa49"
    ]
  ]
  edge [
    id 269
    source 33
    target 266
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR38"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa102"
    ]
  ]
  edge [
    id 271
    source 2
    target 270
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa22"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 272
    source 270
    target 19
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa24"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 273
    source 10
    target 270
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR39"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa146"
    ]
  ]
  edge [
    id 274
    source 79
    target 270
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR40"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa39"
    ]
  ]
  edge [
    id 276
    source 12
    target 275
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa26"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 277
    source 275
    target 33
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa102"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 278
    source 19
    target 275
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR41"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa24"
    ]
  ]
  edge [
    id 280
    source 22
    target 279
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa168"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 281
    source 279
    target 27
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa30"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 282
    source 11
    target 279
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR42"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa202"
    ]
  ]
  edge [
    id 283
    source 32
    target 279
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR43"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa203"
    ]
  ]
  edge [
    id 285
    source 4
    target 284
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa29"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 286
    source 284
    target 10
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa146"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 287
    source 49
    target 284
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR44"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa192"
    ]
  ]
  edge [
    id 289
    source 51
    target 288
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa35"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 290
    source 288
    target 17
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa71"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 292
    source 27
    target 291
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa30"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 293
    source 45
    target 291
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa38"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 294
    source 291
    target 76
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa5"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 296
    source 43
    target 295
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa162"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 297
    source 295
    target 13
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa64"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 298
    source 64
    target 295
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR45"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa161"
    ]
  ]
  edge [
    id 300
    source 24
    target 299
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa21"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 301
    source 299
    target 88
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa23"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 302
    source 3
    target 299
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR46"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa122"
    ]
  ]
  edge [
    id 304
    source 52
    target 303
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa121"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 305
    source 303
    target 66
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa138"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 306
    source 79
    target 303
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR47"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa39"
    ]
  ]
  edge [
    id 308
    source 87
    target 307
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa50"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 309
    source 307
    target 26
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa56"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 312
    source 310
    target 74
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa27"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 313
    source 19
    target 310
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR48"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa24"
    ]
  ]
  edge [
    id 315
    source 14
    target 314
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa199"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 316
    source 314
    target 4
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa29"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 317
    source 9
    target 314
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR49"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa200"
    ]
  ]
  edge [
    id 318
    source 1
    target 314
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR50"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa37"
    ]
  ]
  edge [
    id 320
    source 34
    target 319
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa152"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 321
    source 319
    target 82
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa156"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 322
    source 67
    target 319
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR51"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa63"
    ]
  ]
  edge [
    id 324
    source 70
    target 323
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa175"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 325
    source 323
    target 27
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa30"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 326
    source 68
    target 323
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR52"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa165"
    ]
  ]
  edge [
    id 328
    source 27
    target 327
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa30"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 329
    source 327
    target 72
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa98"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 330
    source 57
    target 327
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR53"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa54"
    ]
  ]
  edge [
    id 331
    source 11
    target 327
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR54"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa202"
    ]
  ]
  edge [
    id 333
    source 33
    target 332
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa102"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 334
    source 332
    target 26
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa56"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 336
    source 39
    target 335
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa36"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 337
    source 335
    target 51
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa35"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 338
    source 1
    target 335
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR55"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa37"
    ]
  ]
  edge [
    id 340
    source 88
    target 339
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa23"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 341
    source 339
    target 19
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa24"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 342
    source 27
    target 339
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR56"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa30"
    ]
  ]
  edge [
    id 343
    source 25
    target 339
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR57"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa47"
    ]
  ]
  edge [
    id 345
    source 33
    target 344
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa102"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 347
    source 344
    target 37
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa9"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 349
    source 52
    target 348
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa121"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 350
    source 348
    target 24
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa21"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 351
    source 4
    target 348
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR58"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa29"
    ]
  ]
  edge [
    id 353
    source 82
    target 352
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa156"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 354
    source 352
    target 78
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa190"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 356
    source 84
    target 355
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa25"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 357
    source 355
    target 26
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa56"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 359
    source 33
    target 358
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa102"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 360
    source 358
    target 41
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa208"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 363
    source 361
    target 74
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa27"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 364
    source 38
    target 361
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR59"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa32"
    ]
  ]
  edge [
    id 366
    source 76
    target 365
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa5"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 367
    source 365
    target 54
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa166"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 369
    source 22
    target 368
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa168"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 370
    source 368
    target 27
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa30"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 371
    source 57
    target 368
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR60"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa54"
    ]
  ]
  edge [
    id 373
    source 88
    target 372
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa23"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 374
    source 372
    target 60
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa51"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 376
    source 88
    target 375
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa23"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 377
    source 375
    target 63
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa62"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 378
    source 13
    target 375
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR61"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa64"
    ]
  ]
  edge [
    id 380
    source 24
    target 379
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa21"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 381
    source 379
    target 88
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa23"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 382
    source 4
    target 379
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR62"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa29"
    ]
  ]
  edge [
    id 388
    source 84
    target 387
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa25"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 389
    source 387
    target 81
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa171"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 391
    source 73
    target 390
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa34"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 392
    source 390
    target 24
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa21"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 393
    source 17
    target 390
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR64"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa71"
    ]
  ]
  edge [
    id 395
    source 33
    target 394
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa102"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 396
    source 394
    target 83
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa172"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 398
    source 44
    target 397
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa95"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 399
    source 397
    target 15
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa96"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 400
    source 55
    target 397
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR65"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa73"
    ]
  ]
  edge [
    id 402
    source 19
    target 401
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa24"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 403
    source 401
    target 85
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa160"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 405
    source 12
    target 404
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa26"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 406
    source 404
    target 33
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa102"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 407
    source 37
    target 404
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR66"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa9"
    ]
  ]
  edge [
    id 408
    source 88
    target 404
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR67"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa23"
    ]
  ]
  edge [
    id 410
    source 74
    target 409
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa27"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 411
    source 409
    target 81
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa171"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 413
    source 74
    target 412
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa27"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 414
    source 412
    target 20
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa61"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 416
    source 27
    target 415
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa30"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 417
    source 415
    target 72
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa98"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 418
    source 7
    target 415
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR68"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa193"
    ]
  ]
  edge [
    id 420
    source 87
    target 419
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa50"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 421
    source 419
    target 20
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa61"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 423
    source 12
    target 422
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa26"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 424
    source 422
    target 33
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa102"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 425
    source 88
    target 422
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR69"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa23"
    ]
  ]
  edge [
    id 426
    source 67
    target 422
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR70"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa63"
    ]
  ]
  edge [
    id 427
    source 60
    target 422
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR71"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa51"
    ]
  ]
  edge [
    id 429
    source 4
    target 428
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa29"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 430
    source 428
    target 10
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa146"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 431
    source 11
    target 428
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR72"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa202"
    ]
  ]
  edge [
    id 433
    source 27
    target 432
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa30"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 434
    source 432
    target 55
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa73"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 435
    source 58
    target 432
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR73"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa43"
    ]
  ]
  edge [
    id 436
    source 33
    target 432
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR74"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa102"
    ]
  ]
  edge [
    id 438
    source 437
    target 98
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR3"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa195"
    ]
  ]
  edge [
    id 439
    source 437
    target 383
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR63"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa195"
    ]
  ]
  edge [
    id 440
    source 266
    target 437
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa195"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 443
    source 437
    target 266
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa205"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 445
    source 383
    target 444
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa141"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 446
    source 444
    target 221
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa141"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 447
    source 444
    target 310
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa141"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 448
    source 444
    target 344
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa141"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 449
    source 444
    target 361
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa141"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 450
    source 444
    target 383
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa196"
      stoichiometry "1.0"
    ]
  ]
]
