# generated with VANTED V2.8.0 at Tue Apr 27 20:00:54 CEST 2021
graph [
  background_coloring "true"
  cluster_colors "255,128,128,255:0,0,0,255;255,223,128,255:0,0,0,255;191,255,128,255:0,0,0,255;128,255,159,255:0,0,0,255;128,255,255,255:0,0,0,255;128,159,255,255:0,0,0,255;191,128,255,255:0,0,0,255;255,128,223,255:0,0,0,255"
  clusterbackground_fill_outer_region "false"
  clusterbackground_grid 50.0
  clusterbackground_low_alpha 0.2
  clusterbackground_radius 200.0
  clusterbackground_space_fill "true"
  graphbackgroundcolor "#ffffff"
  sbml [
    model_meta_id "untitled"
    model_non_rdf_annotation ""
    model_notes ""
    model_sbml_id "untitled"
    namespace "xmlns:celldesigner=\"http://www.sbml.org/2001/ns/celldesigner\" "
    sbml_level "2"
    sbml_version "4"
  ]
  sbml_compartment_default [
    sbml_compartment_default_id "default"
    sbml_compartment_default_meta_id "default"
    sbml_compartment_default_non_rdf_annotation ""
    sbml_compartment_default_notes ""
    sbml_compartment_default_size "1.0"
    sbml_compartment_default_units "volume"
  ]
  sbml_compartment_s_id_ca1 [
    sbml_compartment_s_id_ca1_id "s_id_ca1"
    sbml_compartment_s_id_ca1_meta_id "s_id_ca1"
    sbml_compartment_s_id_ca1_name "cell_space_membrane"
    sbml_compartment_s_id_ca1_non_rdf_annotation ""
    sbml_compartment_s_id_ca1_notes ""
    sbml_compartment_s_id_ca1_outside "s_id_ca2"
    sbml_compartment_s_id_ca1_size "1.0"
    sbml_compartment_s_id_ca1_units "volume"
  ]
  sbml_compartment_s_id_ca10 [
    sbml_compartment_s_id_ca10_id "s_id_ca10"
    sbml_compartment_s_id_ca10_meta_id "s_id_ca10"
    sbml_compartment_s_id_ca10_name "endosome"
    sbml_compartment_s_id_ca10_non_rdf_annotation ""
    sbml_compartment_s_id_ca10_notes ""
    sbml_compartment_s_id_ca10_outside "s_id_ca2"
    sbml_compartment_s_id_ca10_size "1.0"
    sbml_compartment_s_id_ca10_units "volume"
  ]
  sbml_compartment_s_id_ca2 [
    sbml_compartment_s_id_ca2_id "s_id_ca2"
    sbml_compartment_s_id_ca2_meta_id "s_id_ca2"
    sbml_compartment_s_id_ca2_name "cell"
    sbml_compartment_s_id_ca2_non_rdf_annotation ""
    sbml_compartment_s_id_ca2_notes ""
    sbml_compartment_s_id_ca2_outside "s_id_ca8"
    sbml_compartment_s_id_ca2_size "1.0"
    sbml_compartment_s_id_ca2_units "volume"
  ]
  sbml_compartment_s_id_ca3 [
    sbml_compartment_s_id_ca3_id "s_id_ca3"
    sbml_compartment_s_id_ca3_meta_id "s_id_ca3"
    sbml_compartment_s_id_ca3_name "nucleus"
    sbml_compartment_s_id_ca3_non_rdf_annotation ""
    sbml_compartment_s_id_ca3_notes ""
    sbml_compartment_s_id_ca3_outside "s_id_ca2"
    sbml_compartment_s_id_ca3_size "1.0"
    sbml_compartment_s_id_ca3_units "volume"
  ]
  sbml_compartment_s_id_ca6 [
    sbml_compartment_s_id_ca6_id "s_id_ca6"
    sbml_compartment_s_id_ca6_meta_id "s_id_ca6"
    sbml_compartment_s_id_ca6_name "mitochondrion"
    sbml_compartment_s_id_ca6_non_rdf_annotation ""
    sbml_compartment_s_id_ca6_notes ""
    sbml_compartment_s_id_ca6_outside "s_id_ca2"
    sbml_compartment_s_id_ca6_size "1.0"
    sbml_compartment_s_id_ca6_units "volume"
  ]
  sbml_compartment_s_id_ca8 [
    sbml_compartment_s_id_ca8_id "s_id_ca8"
    sbml_compartment_s_id_ca8_meta_id "s_id_ca8"
    sbml_compartment_s_id_ca8_name "extracellular_space_space"
    sbml_compartment_s_id_ca8_non_rdf_annotation ""
    sbml_compartment_s_id_ca8_notes ""
    sbml_compartment_s_id_ca8_outside "default"
    sbml_compartment_s_id_ca8_size "1.0"
    sbml_compartment_s_id_ca8_units "volume"
  ]
  sbml_compartment_s_id_ca9 [
    sbml_compartment_s_id_ca9_id "s_id_ca9"
    sbml_compartment_s_id_ca9_meta_id "s_id_ca9"
    sbml_compartment_s_id_ca9_name "endoplasmic_space_reticulum"
    sbml_compartment_s_id_ca9_non_rdf_annotation ""
    sbml_compartment_s_id_ca9_notes ""
    sbml_compartment_s_id_ca9_outside "s_id_ca2"
    sbml_compartment_s_id_ca9_size "1.0"
    sbml_compartment_s_id_ca9_units "volume"
  ]
  sbml_unit_definition_1 [
    sbml_unit_definition_1_id "volume"
    sbml_unit_definition_1_name "volume"
    sbml_unit_definition_1_sub_unit_1_ "(1.0 * 10^0 * litre)^1.0"
    sbml_unit_definition_1unit "(1.0 * 10^0 * litre)^1.0"
  ]
  sbml_unit_definition_2 [
    sbml_unit_definition_2_id "time"
    sbml_unit_definition_2_name "time"
    sbml_unit_definition_2_sub_unit_1_ "(1.0 * 10^0 * second)^1.0"
    sbml_unit_definition_2unit "(1.0 * 10^0 * second)^1.0"
  ]
  sbml_unit_definition_3 [
    sbml_unit_definition_3_id "length"
    sbml_unit_definition_3_name "length"
    sbml_unit_definition_3_sub_unit_1_ "(1.0 * 10^0 * metre)^1.0"
    sbml_unit_definition_3unit "(1.0 * 10^0 * metre)^1.0"
  ]
  sbml_unit_definition_4 [
    sbml_unit_definition_4_id "area"
    sbml_unit_definition_4_name "area"
    sbml_unit_definition_4_sub_unit_1_ "(1.0 * 10^0 * metre)^2.0"
    sbml_unit_definition_4unit "(1.0 * 10^0 * metre)^2.0"
  ]
  sbml_unit_definition_5 [
    sbml_unit_definition_5_id "substance"
    sbml_unit_definition_5_name "substance"
    sbml_unit_definition_5_sub_unit_1_ "(1.0 * 10^0 * mole)^1.0"
    sbml_unit_definition_5unit "(1.0 * 10^0 * mole)^1.0"
  ]
  directed 1
  node [
    id 1
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 200.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "eif2ak__cell__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa51"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa51"
      minerva_name "EIF2AK"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link2 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource2 "WIKIPATHWAYS__WP4868"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 1362.318181818182
      minerva_y 2304.1515151515155
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa51"
      species_meta_id "s_id_sa51"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 2
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ifnar__cell_space_membrane__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa1"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa1"
      minerva_name "IFNAR"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link2 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource2 "WIKIPATHWAYS__WP4868"
      minerva_type "Complex"
      minerva_x 1490.272727272727
      minerva_y 382.6590909090909
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell_space_membrane"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa1"
      species_meta_id "s_id_csa1"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 3
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 400.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp13__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa309"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa309"
      minerva_elementId2 "sa167"
      minerva_name "Nsp13"
      minerva_ref_link1 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.22.-"
      minerva_ref_link11 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.6.4.13"
      minerva_ref_link12 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.6.4.12"
      minerva_ref_link13 "https://www.ncbi.nlm.nih.gov/gene/43740578"
      minerva_ref_link14 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.1.13.-"
      minerva_ref_link2 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.7.48"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.1.1.-"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.19.12"
      minerva_ref_link5 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.22.69"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P0DTD1"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=rep"
      minerva_ref_link8 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.1.-.-"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_009725308"
      minerva_ref_type__resource1 "WIKIPATHWAYS__WP4868"
      minerva_ref_type__resource10 "EC__3.4.22.-"
      minerva_ref_type__resource11 "EC__3.6.4.13"
      minerva_ref_type__resource12 "EC__3.6.4.12"
      minerva_ref_type__resource13 "ENTREZ__43740578"
      minerva_ref_type__resource14 "EC__3.1.13.-"
      minerva_ref_type__resource2 "EC__2.7.7.48"
      minerva_ref_type__resource3 "EC__2.1.1.-"
      minerva_ref_type__resource4 "EC__3.4.19.12"
      minerva_ref_type__resource5 "EC__3.4.22.69"
      minerva_ref_type__resource6 "UNIPROT__P0DTD1"
      minerva_ref_type__resource7 "HGNC_SYMBOL__rep"
      minerva_ref_type__resource8 "EC__3.1.-.-"
      minerva_ref_type__resource9 "NCBI_PROTEIN__YP_009725308"
      minerva_type "Protein"
      minerva_x 1980.0
      minerva_x2 4140.0
      minerva_y 910.0
      minerva_y2 710.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa309"
      species_meta_id "s_id_sa309"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 4
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 500.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "p50_underscore_p65__nucleus__translocated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa34"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa34"
      minerva_name "p50_p65"
      minerva_state1 "translocated"
      minerva_type "Complex"
      minerva_x 3512.5
      minerva_y 3017.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa34"
      species_meta_id "s_id_csa34"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 5
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 600.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "traf6__cell__ubiquitinated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa59"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa59"
      minerva_name "TRAF6"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_145803"
      minerva_ref_link11 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRAF6"
      minerva_ref_link12 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRAF6"
      minerva_ref_link2 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/7189"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/7189"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000175104"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/Q9Y4K3"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q9Y4K3"
      minerva_ref_link9 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12036"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource10 "REFSEQ__NM_145803"
      minerva_ref_type__resource11 "HGNC_SYMBOL__TRAF6"
      minerva_ref_type__resource12 "HGNC_SYMBOL__TRAF6"
      minerva_ref_type__resource2 "WIKIPATHWAYS__WP4868"
      minerva_ref_type__resource3 "EC__2.3.2.27"
      minerva_ref_type__resource4 "ENTREZ__7189"
      minerva_ref_type__resource5 "ENTREZ__7189"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000175104"
      minerva_ref_type__resource7 "UNIPROT__Q9Y4K3"
      minerva_ref_type__resource8 "UNIPROT__Q9Y4K3"
      minerva_ref_type__resource9 "HGNC__12036"
      minerva_state1 "UBIQUITINATED"
      minerva_type "Protein"
      minerva_x 3147.5
      minerva_y 897.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa59"
      species_meta_id "s_id_sa59"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 6
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 700.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "stat2__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa306"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa306"
      minerva_name "STAT2"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005419"
      minerva_ref_link11 "https://www.ensembl.org/id/ENSG00000170581"
      minerva_ref_link2 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P52630"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P52630"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=STAT2"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=STAT2"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11363"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/6773"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/6773"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource10 "REFSEQ__NM_005419"
      minerva_ref_type__resource11 "ENSEMBL__ENSG00000170581"
      minerva_ref_type__resource2 "WIKIPATHWAYS__WP4868"
      minerva_ref_type__resource3 "UNIPROT__P52630"
      minerva_ref_type__resource4 "UNIPROT__P52630"
      minerva_ref_type__resource5 "HGNC_SYMBOL__STAT2"
      minerva_ref_type__resource6 "HGNC_SYMBOL__STAT2"
      minerva_ref_type__resource7 "HGNC__11363"
      minerva_ref_type__resource8 "ENTREZ__6773"
      minerva_ref_type__resource9 "ENTREZ__6773"
      minerva_type "Protein"
      minerva_x 1680.0
      minerva_y 870.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa306"
      species_meta_id "s_id_sa306"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 7
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 800.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "irf3__cell__active_phosphorylated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa286"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa286"
      minerva_elementId2 "sa121"
      minerva_name "IRF3"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IRF3"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/gene/3661"
      minerva_ref_link12 "https://www.ncbi.nlm.nih.gov/gene/3661"
      minerva_ref_link2 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/pubmed/24622840"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6118"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/Q14653"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/Q14653"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000126456"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_001571"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IRF3"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource10 "HGNC_SYMBOL__IRF3"
      minerva_ref_type__resource11 "ENTREZ__3661"
      minerva_ref_type__resource12 "ENTREZ__3661"
      minerva_ref_type__resource2 "WIKIPATHWAYS__WP4868"
      minerva_ref_type__resource3 "PUBMED__24622840"
      minerva_ref_type__resource4 "HGNC__6118"
      minerva_ref_type__resource5 "UNIPROT__Q14653"
      minerva_ref_type__resource6 "UNIPROT__Q14653"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000126456"
      minerva_ref_type__resource8 "REFSEQ__NM_001571"
      minerva_ref_type__resource9 "HGNC_SYMBOL__IRF3"
      minerva_state1 "PHOSPHORYLATED"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 4830.0
      minerva_x2 4662.0
      minerva_y 1320.0
      minerva_y2 1326.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa286"
      species_meta_id "s_id_sa286"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 8
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "treml4__cell_space_membrane__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa166"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa166"
      minerva_name "TREML4"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000188056"
      minerva_ref_link10 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/30807"
      minerva_ref_link2 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/285852"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/285852"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_198153"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TREML4"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TREML4"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q6UXN2"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q6UXN2"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000188056"
      minerva_ref_type__resource10 "HGNC__30807"
      minerva_ref_type__resource2 "WIKIPATHWAYS__WP4868"
      minerva_ref_type__resource3 "ENTREZ__285852"
      minerva_ref_type__resource4 "ENTREZ__285852"
      minerva_ref_type__resource5 "REFSEQ__NM_198153"
      minerva_ref_type__resource6 "HGNC_SYMBOL__TREML4"
      minerva_ref_type__resource7 "HGNC_SYMBOL__TREML4"
      minerva_ref_type__resource8 "UNIPROT__Q6UXN2"
      minerva_ref_type__resource9 "UNIPROT__Q6UXN2"
      minerva_type "Protein"
      minerva_x 2684.5
      minerva_y 438.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell_space_membrane"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa166"
      species_meta_id "s_id_sa166"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 9
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1000.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tbk1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa60"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa60"
      minerva_name "TBK1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link10 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11584"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/gene/29110"
      minerva_ref_link12 "https://www.ncbi.nlm.nih.gov/gene/29110"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TBK1"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000183735"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TBK1"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/pubmed/24622840"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/Q9UHD2"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/Q9UHD2"
      minerva_ref_link8 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.1"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_013254"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource10 "HGNC__11584"
      minerva_ref_type__resource11 "ENTREZ__29110"
      minerva_ref_type__resource12 "ENTREZ__29110"
      minerva_ref_type__resource2 "HGNC_SYMBOL__TBK1"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000183735"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TBK1"
      minerva_ref_type__resource5 "PUBMED__24622840"
      minerva_ref_type__resource6 "UNIPROT__Q9UHD2"
      minerva_ref_type__resource7 "UNIPROT__Q9UHD2"
      minerva_ref_type__resource8 "EC__2.7.11.1"
      minerva_ref_type__resource9 "REFSEQ__NM_013254"
      minerva_type "Protein"
      minerva_x 4011.5
      minerva_y 997.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa60"
      species_meta_id "s_id_sa60"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 10
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1100.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ikbke__cell__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa116"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa116"
      minerva_name "IKBKE"
      minerva_ref_link1 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.10"
      minerva_ref_link11 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IKBKE"
      minerva_ref_link12 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IKBKE"
      minerva_ref_link13 "https://www.ensembl.org/id/ENSG00000263528"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/9641"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/9641"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/14552"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_001193321"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/Q14164"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14164"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/pubmed/24622840"
      minerva_ref_type__resource1 "WIKIPATHWAYS__WP4868"
      minerva_ref_type__resource10 "EC__2.7.11.10"
      minerva_ref_type__resource11 "HGNC_SYMBOL__IKBKE"
      minerva_ref_type__resource12 "HGNC_SYMBOL__IKBKE"
      minerva_ref_type__resource13 "ENSEMBL__ENSG00000263528"
      minerva_ref_type__resource2 "ENTREZ__9641"
      minerva_ref_type__resource3 "ENTREZ__9641"
      minerva_ref_type__resource4 "HGNC__14552"
      minerva_ref_type__resource5 "REFSEQ__NM_001193321"
      minerva_ref_type__resource6 "PUBMED__31226023"
      minerva_ref_type__resource7 "UNIPROT__Q14164"
      minerva_ref_type__resource8 "UNIPROT__Q14164"
      minerva_ref_type__resource9 "PUBMED__24622840"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 4312.5
      minerva_y 1149.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa116"
      species_meta_id "s_id_sa116"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 11
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1200.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nap1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa296"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa296"
      minerva_name "NAP1"
      minerva_type "Protein"
      minerva_x 1390.0
      minerva_y 700.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa296"
      species_meta_id "s_id_sa296"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 12
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 200.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ifih1__cell__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa154"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa154"
      minerva_name "IFIH1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.6.4.13"
      minerva_ref_link11 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IFIH1"
      minerva_ref_link12 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IFIH1"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/18873"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/64135"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/64135"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_022168"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000115267"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/pubmed/19052324"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q9BYX4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q9BYX4"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource10 "EC__3.6.4.13"
      minerva_ref_type__resource11 "HGNC_SYMBOL__IFIH1"
      minerva_ref_type__resource12 "HGNC_SYMBOL__IFIH1"
      minerva_ref_type__resource2 "HGNC__18873"
      minerva_ref_type__resource3 "ENTREZ__64135"
      minerva_ref_type__resource4 "ENTREZ__64135"
      minerva_ref_type__resource5 "REFSEQ__NM_022168"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000115267"
      minerva_ref_type__resource7 "PUBMED__19052324"
      minerva_ref_type__resource8 "UNIPROT__Q9BYX4"
      minerva_ref_type__resource9 "UNIPROT__Q9BYX4"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 5968.14935064935
      minerva_y 2037.5649350649348
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa154"
      species_meta_id "s_id_sa154"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 13
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 300.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp15__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa227"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa227"
      minerva_elementId2 "sa316"
      minerva_elementId3 "sa129"
      minerva_name "Nsp15"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/YP_009725310"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource2 "NCBI_PROTEIN__YP_009725310"
      minerva_type "Protein"
      minerva_x 6130.0
      minerva_x2 4270.0
      minerva_x3 1268.454545454546
      minerva_y 2130.0
      minerva_y2 1620.0
      minerva_y3 2067.318181818182
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa227"
      species_meta_id "s_id_sa227"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 15
    zlevel -1

    cluster [
      cluster "s_id_ca8"
    ]
    graphics [
      x 500.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ifna1__extracellular_space_space__secreted__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa31"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa31"
      minerva_name "IFNA1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000197919"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IFNA1"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IFNA1"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/3439"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/3439"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_024013"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/P01562"
      minerva_ref_link8 "https://purl.uniprot.org/uniprot/P01562"
      minerva_ref_link9 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/5417"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000197919"
      minerva_ref_type__resource2 "HGNC_SYMBOL__IFNA1"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IFNA1"
      minerva_ref_type__resource4 "ENTREZ__3439"
      minerva_ref_type__resource5 "ENTREZ__3439"
      minerva_ref_type__resource6 "REFSEQ__NM_024013"
      minerva_ref_type__resource7 "UNIPROT__P01562"
      minerva_ref_type__resource8 "UNIPROT__P01562"
      minerva_ref_type__resource9 "HGNC__5417"
      minerva_structuralState "secreted"
      minerva_type "Protein"
      minerva_x 2530.0
      minerva_y 3720.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca8"
      compartment_name "extracellular_space_space"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa31"
      species_meta_id "s_id_sa31"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 16
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 600.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ikka__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa266"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa266"
      minerva_name "IKKa"
      minerva_type "Protein"
      minerva_x 2440.0
      minerva_y 1920.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa266"
      species_meta_id "s_id_sa266"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 17
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 700.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "eif2ak__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa160"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa160"
      minerva_name "EIF2AK"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link2 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource2 "WIKIPATHWAYS__WP4868"
      minerva_type "Protein"
      minerva_x 1173.5
      minerva_y 2304.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa160"
      species_meta_id "s_id_sa160"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 18
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 800.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp5__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa251"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa251"
      minerva_name "Nsp5"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/YP_009725301"
      minerva_ref_type__resource1 "NCBI_PROTEIN__YP_009725301"
      minerva_type "Protein"
      minerva_x 6110.0
      minerva_y 1890.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa251"
      species_meta_id "s_id_sa251"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 19
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 900.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "traf6__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa58"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa58"
      minerva_name "TRAF6"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_145803"
      minerva_ref_link11 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRAF6"
      minerva_ref_link12 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRAF6"
      minerva_ref_link2 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/7189"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/7189"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000175104"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/Q9Y4K3"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q9Y4K3"
      minerva_ref_link9 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12036"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource10 "REFSEQ__NM_145803"
      minerva_ref_type__resource11 "HGNC_SYMBOL__TRAF6"
      minerva_ref_type__resource12 "HGNC_SYMBOL__TRAF6"
      minerva_ref_type__resource2 "WIKIPATHWAYS__WP4868"
      minerva_ref_type__resource3 "EC__2.3.2.27"
      minerva_ref_type__resource4 "ENTREZ__7189"
      minerva_ref_type__resource5 "ENTREZ__7189"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000175104"
      minerva_ref_type__resource7 "UNIPROT__Q9Y4K3"
      minerva_ref_type__resource8 "UNIPROT__Q9Y4K3"
      minerva_ref_type__resource9 "HGNC__12036"
      minerva_type "Protein"
      minerva_x 3512.5
      minerva_y 897.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa58"
      species_meta_id "s_id_sa58"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 20
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1000.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "viral_space_dsrna__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa186"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa186"
      minerva_elementId2 "sa187"
      minerva_name "Viral dsRNA"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/pubmed/19052324"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=694009"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource2 "PUBMED__19052324"
      minerva_ref_type__resource3 "TAXONOMY__694009"
      minerva_type "RNA"
      minerva_x 5539.0
      minerva_x2 5546.454545454545
      minerva_y 1643.0
      minerva_y2 1449.7727272727273
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa186"
      species_meta_id "s_id_sa186"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 21
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1100.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "isg15_underscore_nsp3__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa48"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa48"
      minerva_name "ISG15_Nsp3"
      minerva_type "Complex"
      minerva_x 2530.0
      minerva_y 970.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa48"
      species_meta_id "s_id_csa48"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 22
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1200.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "p50_underscore_p65__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa21"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa21"
      minerva_name "p50_p65"
      minerva_type "Complex"
      minerva_x 3520.0
      minerva_y 2480.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa21"
      species_meta_id "s_id_csa21"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 23
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 200.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf7b__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa308"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa308"
      minerva_name "Orf7b"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/YP_009725318.1"
      minerva_ref_type__resource1 "NCBI_PROTEIN__YP_009725318.1"
      minerva_type "Protein"
      minerva_x 1980.0
      minerva_y 1070.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa308"
      species_meta_id "s_id_sa308"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 24
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 300.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "oas2__cell__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa76"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa76"
      minerva_name "OAS2"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000111335"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/protein/NM_001032731"
      minerva_ref_link12 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.7.84"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=OAS2"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=OAS2"
      minerva_ref_link4 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P29728"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P29728"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4939"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4939"
      minerva_ref_link9 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/8087"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000111335"
      minerva_ref_type__resource11 "REFSEQ__NM_001032731"
      minerva_ref_type__resource12 "EC__2.7.7.84"
      minerva_ref_type__resource2 "HGNC_SYMBOL__OAS2"
      minerva_ref_type__resource3 "HGNC_SYMBOL__OAS2"
      minerva_ref_type__resource4 "WIKIPATHWAYS__WP4868"
      minerva_ref_type__resource5 "UNIPROT__P29728"
      minerva_ref_type__resource6 "UNIPROT__P29728"
      minerva_ref_type__resource7 "ENTREZ__4939"
      minerva_ref_type__resource8 "ENTREZ__4939"
      minerva_ref_type__resource9 "HGNC__8087"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 963.954545454546
      minerva_y 2215.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa76"
      species_meta_id "s_id_sa76"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 25
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 400.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp3__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa169"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa169"
      minerva_name "Nsp3"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link2 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/YP_009725299"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource2 "WIKIPATHWAYS__WP4868"
      minerva_ref_type__resource3 "NCBI_PROTEIN__YP_009725299"
      minerva_ref_type__resource4 "UNIPROT__Nsp3"
      minerva_type "Protein"
      minerva_x 3500.0
      minerva_y 1140.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa169"
      species_meta_id "s_id_sa169"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 26
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 500.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "m__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa134"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa134"
      minerva_elementId2 "sa232"
      minerva_elementId3 "sa226"
      minerva_name "M"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/YP_009724393.1"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource2 "NCBI_PROTEIN__YP_009724393.1"
      minerva_ref_type__resource3 "UNIPROT__M"
      minerva_type "Protein"
      minerva_x 1210.0
      minerva_x2 5860.0
      minerva_x3 5516.5
      minerva_y 1090.0
      minerva_y2 1450.0
      minerva_y3 668.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa134"
      species_meta_id "s_id_sa134"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 27
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 600.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "itch__cell__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa204"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa204"
      minerva_name "ITCH"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/Q96J02"
      minerva_ref_link10 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/13890"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q96J02"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.26"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ITCH"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ITCH"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000078747"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_001257137"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/83737"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/83737"
      minerva_ref_type__resource1 "UNIPROT__Q96J02"
      minerva_ref_type__resource10 "HGNC__13890"
      minerva_ref_type__resource2 "UNIPROT__Q96J02"
      minerva_ref_type__resource3 "EC__2.3.2.26"
      minerva_ref_type__resource4 "HGNC_SYMBOL__ITCH"
      minerva_ref_type__resource5 "HGNC_SYMBOL__ITCH"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000078747"
      minerva_ref_type__resource7 "REFSEQ__NM_001257137"
      minerva_ref_type__resource8 "ENTREZ__83737"
      minerva_ref_type__resource9 "ENTREZ__83737"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 4886.5
      minerva_y 2187.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa204"
      species_meta_id "s_id_sa204"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 28
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 700.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ifnb1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa26"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa26"
      minerva_former_symbols "IFNB"
      minerva_name "IFNB1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/3456"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000171855"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IFNB1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IFNB1"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/5434"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P01574"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/P01574"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_002176"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/3456"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource10 "ENTREZ__3456"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000171855"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IFNB1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__IFNB1"
      minerva_ref_type__resource5 "HGNC__5434"
      minerva_ref_type__resource6 "UNIPROT__P01574"
      minerva_ref_type__resource7 "UNIPROT__P01574"
      minerva_ref_type__resource8 "REFSEQ__NM_002176"
      minerva_ref_type__resource9 "ENTREZ__3456"
      minerva_type "Protein"
      minerva_x 2710.0
      minerva_y 3400.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa26"
      species_meta_id "s_id_sa26"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 29
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 800.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp6__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa304"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa307"
      minerva_elementId2 "sa304"
      minerva_name "Nsp6"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/32979938"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=2697049"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/YP_009742613.1"
      minerva_ref_type__resource1 "PUBMED__32979938"
      minerva_ref_type__resource2 "TAXONOMY__2697049"
      minerva_ref_type__resource3 "NCBI_PROTEIN__YP_009742613.1"
      minerva_type "Protein"
      minerva_x 1210.0
      minerva_x2 4600.0
      minerva_y 850.0
      minerva_y2 1140.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa304"
      species_meta_id "s_id_sa304"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 30
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 900.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "irf9__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa70"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa70"
      minerva_former_symbols "ISGF3G"
      minerva_name "IRF9"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/10379"
      minerva_ref_link11 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6131"
      minerva_ref_link2 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/Q00978"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/Q00978"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000213928"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_006084"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IRF9"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/10379"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IRF9"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource10 "ENTREZ__10379"
      minerva_ref_type__resource11 "HGNC__6131"
      minerva_ref_type__resource2 "WIKIPATHWAYS__WP4868"
      minerva_ref_type__resource3 "UNIPROT__Q00978"
      minerva_ref_type__resource4 "UNIPROT__Q00978"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000213928"
      minerva_ref_type__resource6 "REFSEQ__NM_006084"
      minerva_ref_type__resource7 "HGNC_SYMBOL__IRF9"
      minerva_ref_type__resource8 "ENTREZ__10379"
      minerva_ref_type__resource9 "HGNC_SYMBOL__IRF9"
      minerva_type "Protein"
      minerva_x 1380.0
      minerva_y 1090.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa70"
      species_meta_id "s_id_sa70"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 31
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1000.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "grl0617__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa168"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa168"
      minerva_name "GRL0617"
      minerva_ref_link1 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_type__resource1 "WIKIPATHWAYS__WP4868"
      minerva_type "Drug"
      minerva_x 2890.0
      minerva_y 960.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa168"
      species_meta_id "s_id_sa168"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 32
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1100.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mapk8__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa63"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa63"
      minerva_former_symbols "PRKM8"
      minerva_name "MAPK8"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link10 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6881"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/gene/5599"
      minerva_ref_link12 "https://www.ncbi.nlm.nih.gov/gene/5599"
      minerva_ref_link2 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.24"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_001278547"
      minerva_ref_link4 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000107643"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAPK8"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P45983"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P45983"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAPK8"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource10 "HGNC__6881"
      minerva_ref_type__resource11 "ENTREZ__5599"
      minerva_ref_type__resource12 "ENTREZ__5599"
      minerva_ref_type__resource2 "EC__2.7.11.24"
      minerva_ref_type__resource3 "REFSEQ__NM_001278547"
      minerva_ref_type__resource4 "WIKIPATHWAYS__WP4868"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000107643"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MAPK8"
      minerva_ref_type__resource7 "UNIPROT__P45983"
      minerva_ref_type__resource8 "UNIPROT__P45983"
      minerva_ref_type__resource9 "HGNC_SYMBOL__MAPK8"
      minerva_type "Protein"
      minerva_x 2611.5
      minerva_y 1390.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa63"
      species_meta_id "s_id_sa63"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 33
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1200.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "pp1ab__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa184"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa184"
      minerva_name "pp1ab"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/24622840"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/YP_009724389"
      minerva_ref_type__resource1 "PUBMED__24622840"
      minerva_ref_type__resource2 "NCBI_PROTEIN__YP_009724389"
      minerva_type "Protein"
      minerva_x 5138.5
      minerva_y 1152.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa184"
      species_meta_id "s_id_sa184"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 34
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 200.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tab2__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa255"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa255"
      minerva_former_symbols "MAP3K7IP2"
      minerva_name "TAB2"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TAB2"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000055208"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TAB2"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/Q9NYJ8"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/Q9NYJ8"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/17075"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/23118"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_001292034"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/23118"
      minerva_ref_type__resource1 "HGNC_SYMBOL__TAB2"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000055208"
      minerva_ref_type__resource3 "HGNC_SYMBOL__TAB2"
      minerva_ref_type__resource4 "UNIPROT__Q9NYJ8"
      minerva_ref_type__resource5 "UNIPROT__Q9NYJ8"
      minerva_ref_type__resource6 "HGNC__17075"
      minerva_ref_type__resource7 "ENTREZ__23118"
      minerva_ref_type__resource8 "REFSEQ__NM_001292034"
      minerva_ref_type__resource9 "ENTREZ__23118"
      minerva_type "Protein"
      minerva_x 620.0
      minerva_y 1500.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa255"
      species_meta_id "s_id_sa255"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 35
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 300.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s2661__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa300"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa300"
      minerva_name "s2661"
      minerva_type "Degraded"
      minerva_x 1980.0
      minerva_y 2880.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa300"
      species_meta_id "s_id_sa300"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 36
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 400.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "irak1_slash_4__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa314"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa314"
      minerva_elementId2 "sa292"
      minerva_name "IRAK1/4"
      minerva_type "Protein"
      minerva_x 3560.25
      minerva_x2 1200.0
      minerva_y 577.0
      minerva_y2 750.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa314"
      species_meta_id "s_id_sa314"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 37
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 500.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mns__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa237"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa237"
      minerva_name "MNS"
      minerva_type "Drug"
      minerva_x 2470.0
      minerva_y 2220.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa237"
      species_meta_id "s_id_sa237"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 38
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 600.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tab1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa254"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa254"
      minerva_former_symbols "MAP3K7IP1"
      minerva_name "TAB1"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/18157"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_153497"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/Q15750"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/Q15750"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000100324"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TAB1"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TAB1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/10454"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/10454"
      minerva_ref_type__resource1 "HGNC__18157"
      minerva_ref_type__resource2 "REFSEQ__NM_153497"
      minerva_ref_type__resource3 "UNIPROT__Q15750"
      minerva_ref_type__resource4 "UNIPROT__Q15750"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000100324"
      minerva_ref_type__resource6 "HGNC_SYMBOL__TAB1"
      minerva_ref_type__resource7 "HGNC_SYMBOL__TAB1"
      minerva_ref_type__resource8 "ENTREZ__10454"
      minerva_ref_type__resource9 "ENTREZ__10454"
      minerva_type "Protein"
      minerva_x 480.0
      minerva_y 1500.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa254"
      species_meta_id "s_id_sa254"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 39
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 700.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ikb__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa270"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa270"
      minerva_name "IkB"
      minerva_type "Protein"
      minerva_x 3390.0
      minerva_y 1840.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa270"
      species_meta_id "s_id_sa270"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 40
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 800.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tbk1__cell__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa61"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa61"
      minerva_name "TBK1"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TBK1"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/pubmed/24622840"
      minerva_ref_link11 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.1"
      minerva_ref_link12 "https://www.ncbi.nlm.nih.gov/gene/29110"
      minerva_ref_link13 "https://www.ncbi.nlm.nih.gov/gene/29110"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000183735"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TBK1"
      minerva_ref_link4 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/Q9UHD2"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/Q9UHD2"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_013254"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11584"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_type__resource1 "HGNC_SYMBOL__TBK1"
      minerva_ref_type__resource10 "PUBMED__24622840"
      minerva_ref_type__resource11 "EC__2.7.11.1"
      minerva_ref_type__resource12 "ENTREZ__29110"
      minerva_ref_type__resource13 "ENTREZ__29110"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000183735"
      minerva_ref_type__resource3 "HGNC_SYMBOL__TBK1"
      minerva_ref_type__resource4 "WIKIPATHWAYS__WP4868"
      minerva_ref_type__resource5 "UNIPROT__Q9UHD2"
      minerva_ref_type__resource6 "UNIPROT__Q9UHD2"
      minerva_ref_type__resource7 "REFSEQ__NM_013254"
      minerva_ref_type__resource8 "HGNC__11584"
      minerva_ref_type__resource9 "PUBMED__31226023"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 4263.833333333335
      minerva_y 996.9285714285716
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa61"
      species_meta_id "s_id_sa61"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 41
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 900.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tank__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa209"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa209"
      minerva_former_symbols "TRAF2"
      minerva_name "TANK"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/10010"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/10010"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000136560"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_133484"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/Q92844"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/Q92844"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11562"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TANK"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TANK"
      minerva_ref_type__resource1 "ENTREZ__10010"
      minerva_ref_type__resource2 "ENTREZ__10010"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000136560"
      minerva_ref_type__resource4 "REFSEQ__NM_133484"
      minerva_ref_type__resource5 "UNIPROT__Q92844"
      minerva_ref_type__resource6 "UNIPROT__Q92844"
      minerva_ref_type__resource7 "HGNC__11562"
      minerva_ref_type__resource8 "HGNC_SYMBOL__TANK"
      minerva_ref_type__resource9 "HGNC_SYMBOL__TANK"
      minerva_type "Protein"
      minerva_x 5570.5
      minerva_y 819.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa209"
      species_meta_id "s_id_sa209"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 42
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 1000.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "isre__nucleus__translocated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa6"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa6"
      minerva_name "ISRE"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link2 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource2 "WIKIPATHWAYS__WP4868"
      minerva_state1 "translocated"
      minerva_type "Complex"
      minerva_x 1470.0
      minerva_y 2910.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa6"
      species_meta_id "s_id_csa6"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 43
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1100.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "irf3_underscore_homodimer__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa45"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa45"
      minerva_name "IRF3_homodimer"
      minerva_type "Complex"
      minerva_x 4650.0
      minerva_y 1610.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa45"
      species_meta_id "s_id_csa45"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 44
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1200.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "azithromycin__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa321"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa320"
      minerva_elementId2 "sa321"
      minerva_elementId3 "sa170"
      minerva_name "Azithromycin"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:2955"
      minerva_ref_type__resource1 "CHEBI__CHEBI:2955"
      minerva_type "Drug"
      minerva_x 6340.0
      minerva_x2 3220.0
      minerva_x3 3940.0
      minerva_y 1900.0
      minerva_y2 2470.0
      minerva_y3 1260.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa321"
      species_meta_id "s_id_sa321"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 45
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 200.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "rip1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa247"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa247"
      minerva_name "RIP1"
      minerva_type "Protein"
      minerva_x 840.0
      minerva_y 1370.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa247"
      species_meta_id "s_id_sa247"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 46
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 300.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "oas1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa159"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa159"
      minerva_former_symbols "OIAS"
      minerva_name "OAS1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link10 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/8086"
      minerva_ref_link11 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.7.84"
      minerva_ref_link12 "https://www.ensembl.org/id/ENSG00000089127"
      minerva_ref_link2 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P00973"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P00973"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/4938"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/4938"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_001032409"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=OAS1"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=OAS1"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource10 "HGNC__8086"
      minerva_ref_type__resource11 "EC__2.7.7.84"
      minerva_ref_type__resource12 "ENSEMBL__ENSG00000089127"
      minerva_ref_type__resource2 "WIKIPATHWAYS__WP4868"
      minerva_ref_type__resource3 "UNIPROT__P00973"
      minerva_ref_type__resource4 "UNIPROT__P00973"
      minerva_ref_type__resource5 "ENTREZ__4938"
      minerva_ref_type__resource6 "ENTREZ__4938"
      minerva_ref_type__resource7 "REFSEQ__NM_001032409"
      minerva_ref_type__resource8 "HGNC_SYMBOL__OAS1"
      minerva_ref_type__resource9 "HGNC_SYMBOL__OAS1"
      minerva_type "Protein"
      minerva_x 700.0
      minerva_y 2300.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa159"
      species_meta_id "s_id_sa159"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 47
    zlevel -1

    cluster [
      cluster "s_id_ca8"
    ]
    graphics [
      x 400.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "type_space_i_space_ifn_space_response__extracellular_space_space__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa260"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa260"
      minerva_name "type I IFN response"
      minerva_type "Phenotype"
      minerva_x 2620.0
      minerva_y 3890.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca8"
      compartment_name "extracellular_space_space"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa260"
      species_meta_id "s_id_sa260"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 48
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 500.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s36__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa46"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa46"
      minerva_name "s36"
      minerva_type "Degraded"
      minerva_x 1043.5
      minerva_y 2858.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa46"
      species_meta_id "s_id_sa46"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 49
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 600.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf9b__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa139"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa139"
      minerva_name "Orf9b"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P0DTD2"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/ABI96969"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource2 "UNIPROT__P0DTD2"
      minerva_ref_type__resource3 "NCBI_PROTEIN__ABI96969"
      minerva_type "Protein"
      minerva_x 5209.0
      minerva_y 2097.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa139"
      species_meta_id "s_id_sa139"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 50
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 700.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "traf3__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa82"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa82"
      minerva_name "TRAF3"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link10 "http://purl.uniprot.org/uniprot/Q13114"
      minerva_ref_link11 "http://purl.uniprot.org/uniprot/Q13114"
      minerva_ref_link12 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12033"
      minerva_ref_link2 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRAF3"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000131323"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRAF3"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/7187"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/7187"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_145725"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource10 "UNIPROT__Q13114"
      minerva_ref_type__resource11 "UNIPROT__Q13114"
      minerva_ref_type__resource12 "HGNC__12033"
      minerva_ref_type__resource2 "WIKIPATHWAYS__WP4868"
      minerva_ref_type__resource3 "HGNC_SYMBOL__TRAF3"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000131323"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRAF3"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "ENTREZ__7187"
      minerva_ref_type__resource8 "ENTREZ__7187"
      minerva_ref_type__resource9 "REFSEQ__NM_145725"
      minerva_type "Protein"
      minerva_x 3747.5
      minerva_y 891.3333333333334
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa82"
      species_meta_id "s_id_sa82"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 51
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 800.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nlrp3_underscore_inflammasome__cell__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa30"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa30"
      minerva_name "NLRP3_inflammasome"
      minerva_state1 "active"
      minerva_type "Complex"
      minerva_x 2867.5
      minerva_y 2437.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa30"
      species_meta_id "s_id_csa30"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 52
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 900.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "jun__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa36"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa36"
      minerva_name "JUN"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000177606"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/protein/NM_002228"
      minerva_ref_link2 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/3725"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/3725"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6204"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P05412"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P05412"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=JUN"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=JUN"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000177606"
      minerva_ref_type__resource11 "REFSEQ__NM_002228"
      minerva_ref_type__resource2 "WIKIPATHWAYS__WP4868"
      minerva_ref_type__resource3 "ENTREZ__3725"
      minerva_ref_type__resource4 "ENTREZ__3725"
      minerva_ref_type__resource5 "HGNC__6204"
      minerva_ref_type__resource6 "UNIPROT__P05412"
      minerva_ref_type__resource7 "UNIPROT__P05412"
      minerva_ref_type__resource8 "HGNC_SYMBOL__JUN"
      minerva_ref_type__resource9 "HGNC_SYMBOL__JUN"
      minerva_type "Protein"
      minerva_x 3580.0
      minerva_y 1430.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa36"
      species_meta_id "s_id_sa36"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 53
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1000.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tbk1_underscore_ikbke__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa19"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa19"
      minerva_name "TBK1_IKBKE"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link2 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/pubmed/24622840"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource2 "WIKIPATHWAYS__WP4868"
      minerva_ref_type__resource3 "PUBMED__24622840"
      minerva_type "Complex"
      minerva_x 4750.0
      minerva_y 995.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa19"
      species_meta_id "s_id_csa19"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 54
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 1100.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "irf3_underscore_homodimer__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa46"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa46"
      minerva_name "IRF3_homodimer"
      minerva_type "Complex"
      minerva_x 4640.0
      minerva_y 2920.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa46"
      species_meta_id "s_id_csa46"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 55
    zlevel -1

    cluster [
      cluster "s_id_ca8"
    ]
    graphics [
      x 1200.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ifna1__extracellular_space_space__extracellular space__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa5"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa5"
      minerva_name "IFNA1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link10 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/5417"
      minerva_ref_link11 "https://www.ensembl.org/id/ENSG00000197919"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IFNA1"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IFNA1"
      minerva_ref_link4 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/3439"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/3439"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_024013"
      minerva_ref_link8 "https://purl.uniprot.org/uniprot/P01562"
      minerva_ref_link9 "https://purl.uniprot.org/uniprot/P01562"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource10 "HGNC__5417"
      minerva_ref_type__resource11 "ENSEMBL__ENSG00000197919"
      minerva_ref_type__resource2 "HGNC_SYMBOL__IFNA1"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IFNA1"
      minerva_ref_type__resource4 "WIKIPATHWAYS__WP4868"
      minerva_ref_type__resource5 "ENTREZ__3439"
      minerva_ref_type__resource6 "ENTREZ__3439"
      minerva_ref_type__resource7 "REFSEQ__NM_024013"
      minerva_ref_type__resource8 "UNIPROT__P01562"
      minerva_ref_type__resource9 "UNIPROT__P01562"
      minerva_structuralState "Extracellular Space"
      minerva_type "Protein"
      minerva_x 1590.772727272727
      minerva_y 180.65909090909088
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca8"
      compartment_name "extracellular_space_space"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa5"
      species_meta_id "s_id_sa5"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 56
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 200.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "irf7__cell__active_phosphorylated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa281"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa281"
      minerva_elementId2 "sa211"
      minerva_name "IRF7"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/Q92985"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q92985"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000185507"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_001572"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IRF7"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IRF7"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6122"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/3665"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/3665"
      minerva_ref_type__resource1 "UNIPROT__Q92985"
      minerva_ref_type__resource2 "UNIPROT__Q92985"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000185507"
      minerva_ref_type__resource4 "REFSEQ__NM_001572"
      minerva_ref_type__resource5 "HGNC_SYMBOL__IRF7"
      minerva_ref_type__resource6 "HGNC_SYMBOL__IRF7"
      minerva_ref_type__resource7 "HGNC__6122"
      minerva_ref_type__resource8 "ENTREZ__3665"
      minerva_ref_type__resource9 "ENTREZ__3665"
      minerva_state1 "PHOSPHORYLATED"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 4200.0
      minerva_x2 4346.5
      minerva_y 2020.0
      minerva_y2 2021.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa281"
      species_meta_id "s_id_sa281"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 57
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 300.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ikb__cell__phosphorylated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa278"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa278"
      minerva_name "IkB"
      minerva_state1 "PHOSPHORYLATED"
      minerva_type "Protein"
      minerva_x 4080.0
      minerva_y 2460.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa278"
      species_meta_id "s_id_sa278"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 58
    zlevel -1

    cluster [
      cluster "s_id_ca6"
    ]
    graphics [
      x 400.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mavs__mitochondrion__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa156"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa156"
      minerva_name "MAVS"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/57506"
      minerva_ref_link11 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/29233"
      minerva_ref_link12 "https://www.ncbi.nlm.nih.gov/protein/NM_020746"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q7Z434"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/Q7Z434"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/pubmed/24622840"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAVS"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAVS"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/pubmed/19052324"
      minerva_ref_link8 "https://www.ensembl.org/id/ENSG00000088888"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/57506"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource10 "ENTREZ__57506"
      minerva_ref_type__resource11 "HGNC__29233"
      minerva_ref_type__resource12 "REFSEQ__NM_020746"
      minerva_ref_type__resource2 "UNIPROT__Q7Z434"
      minerva_ref_type__resource3 "UNIPROT__Q7Z434"
      minerva_ref_type__resource4 "PUBMED__24622840"
      minerva_ref_type__resource5 "HGNC_SYMBOL__MAVS"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MAVS"
      minerva_ref_type__resource7 "PUBMED__19052324"
      minerva_ref_type__resource8 "ENSEMBL__ENSG00000088888"
      minerva_ref_type__resource9 "ENTREZ__57506"
      minerva_type "Protein"
      minerva_x 5131.363636363636
      minerva_y 1928.8636363636363
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca6"
      compartment_name "mitochondrion"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa156"
      species_meta_id "s_id_sa156"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 60
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 600.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf6__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa311"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa311"
      minerva_elementId2 "sa312"
      minerva_elementId3 "sa127"
      minerva_name "Orf6"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/YP_009724394.1"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource2 "NCBI_PROTEIN__YP_009724394.1"
      minerva_type "Protein"
      minerva_x 1320.0
      minerva_x2 4880.0
      minerva_x3 2750.0
      minerva_y 1870.0
      minerva_y2 2270.0
      minerva_y3 790.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa311"
      species_meta_id "s_id_sa311"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 61
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tlr4__cell_space_membrane__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa291"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa291"
      minerva_name "TLR4"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_138554"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000136869"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/7099"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7099"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.2.2.6"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11850"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/O00206"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/O00206"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TLR4"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TLR4"
      minerva_ref_type__resource1 "REFSEQ__NM_138554"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000136869"
      minerva_ref_type__resource2 "ENTREZ__7099"
      minerva_ref_type__resource3 "ENTREZ__7099"
      minerva_ref_type__resource4 "EC__3.2.2.6"
      minerva_ref_type__resource5 "HGNC__11850"
      minerva_ref_type__resource6 "UNIPROT__O00206"
      minerva_ref_type__resource7 "UNIPROT__O00206"
      minerva_ref_type__resource8 "HGNC_SYMBOL__TLR4"
      minerva_ref_type__resource9 "HGNC_SYMBOL__TLR4"
      minerva_type "Protein"
      minerva_x 820.0
      minerva_y 480.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell_space_membrane"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa291"
      species_meta_id "s_id_sa291"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 62
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 800.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nap1__cell__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa297"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa297"
      minerva_name "NAP1"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 1600.0
      minerva_y 690.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa297"
      species_meta_id "s_id_sa297"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 63
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 900.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ddx58__cell__ubiquitinated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa155"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa155"
      minerva_name "DDX58"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link10 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/19102"
      minerva_ref_link11 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DDX58"
      minerva_ref_link12 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DDX58"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/23586"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/23586"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_014314"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000107201"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/pubmed/19052324"
      minerva_ref_link7 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.6.4.13"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/O95786"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/O95786"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource10 "HGNC__19102"
      minerva_ref_type__resource11 "HGNC_SYMBOL__DDX58"
      minerva_ref_type__resource12 "HGNC_SYMBOL__DDX58"
      minerva_ref_type__resource2 "ENTREZ__23586"
      minerva_ref_type__resource3 "ENTREZ__23586"
      minerva_ref_type__resource4 "REFSEQ__NM_014314"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000107201"
      minerva_ref_type__resource6 "PUBMED__19052324"
      minerva_ref_type__resource7 "EC__3.6.4.13"
      minerva_ref_type__resource8 "UNIPROT__O95786"
      minerva_ref_type__resource9 "UNIPROT__O95786"
      minerva_state1 "UBIQUITINATED"
      minerva_type "Protein"
      minerva_x 5966.363636363636
      minerva_y 1734.1363636363635
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa155"
      species_meta_id "s_id_sa155"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 65
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1100.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "rnf135__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa202"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa202"
      minerva_name "RNF135"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000181481"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q8IUD6"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/Q8IUD6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/21158"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_032322"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=RNF135"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=RNF135"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/84282"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/84282"
      minerva_ref_type__resource1 "EC__2.3.2.27"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000181481"
      minerva_ref_type__resource2 "UNIPROT__Q8IUD6"
      minerva_ref_type__resource3 "UNIPROT__Q8IUD6"
      minerva_ref_type__resource4 "HGNC__21158"
      minerva_ref_type__resource5 "REFSEQ__NM_032322"
      minerva_ref_type__resource6 "HGNC_SYMBOL__RNF135"
      minerva_ref_type__resource7 "HGNC_SYMBOL__RNF135"
      minerva_ref_type__resource8 "ENTREZ__84282"
      minerva_ref_type__resource9 "ENTREZ__84282"
      minerva_type "Protein"
      minerva_x 6310.5
      minerva_y 1596.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa202"
      species_meta_id "s_id_sa202"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 66
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1200.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mapk14__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa64"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa64"
      minerva_former_symbols "CSBP1; CSBP2; CSPB1"
      minerva_name "MAPK14"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAPK14"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/gene/1432"
      minerva_ref_link12 "https://www.ncbi.nlm.nih.gov/protein/NM_001315"
      minerva_ref_link2 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.24"
      minerva_ref_link3 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000112062"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6876"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/Q16539"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/Q16539"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAPK14"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/1432"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource10 "HGNC_SYMBOL__MAPK14"
      minerva_ref_type__resource11 "ENTREZ__1432"
      minerva_ref_type__resource12 "REFSEQ__NM_001315"
      minerva_ref_type__resource2 "EC__2.7.11.24"
      minerva_ref_type__resource3 "WIKIPATHWAYS__WP4868"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000112062"
      minerva_ref_type__resource5 "HGNC__6876"
      minerva_ref_type__resource6 "UNIPROT__Q16539"
      minerva_ref_type__resource7 "UNIPROT__Q16539"
      minerva_ref_type__resource8 "HGNC_SYMBOL__MAPK14"
      minerva_ref_type__resource9 "ENTREZ__1432"
      minerva_type "Protein"
      minerva_x 2610.0
      minerva_y 1300.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa64"
      species_meta_id "s_id_sa64"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 67
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 200.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tlr9__endosome__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa238"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa238"
      minerva_name "TLR9"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/Q9NR96"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q9NR96"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/54106"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/54106"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000239732"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TLR9"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TLR9"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_017442"
      minerva_ref_link9 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/15633"
      minerva_ref_type__resource1 "UNIPROT__Q9NR96"
      minerva_ref_type__resource2 "UNIPROT__Q9NR96"
      minerva_ref_type__resource3 "ENTREZ__54106"
      minerva_ref_type__resource4 "ENTREZ__54106"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000239732"
      minerva_ref_type__resource6 "HGNC_SYMBOL__TLR9"
      minerva_ref_type__resource7 "HGNC_SYMBOL__TLR9"
      minerva_ref_type__resource8 "REFSEQ__NM_017442"
      minerva_ref_type__resource9 "HGNC__15633"
      minerva_type "Protein"
      minerva_x 980.0
      minerva_y 730.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "endosome"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa238"
      species_meta_id "s_id_sa238"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 68
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tlr4_underscore_trif_underscore_tram__cell_space_membrane__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa47"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa47"
      minerva_name "TLR4_TRIF_TRAM"
      minerva_type "Complex"
      minerva_x 1170.0
      minerva_y 465.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell_space_membrane"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa47"
      species_meta_id "s_id_csa47"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 70
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 500.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s39__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa49"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa49"
      minerva_name "s39"
      minerva_type "Degraded"
      minerva_x 1660.0
      minerva_y 2890.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa49"
      species_meta_id "s_id_sa49"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 71
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 600.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "isg15__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa301"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa301"
      minerva_former_symbols "G1P2"
      minerva_name "ISG15"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000187608"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_005101"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/9636"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/9636"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ISG15"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ISG15"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P05161"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P05161"
      minerva_ref_link9 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/4053"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000187608"
      minerva_ref_type__resource2 "REFSEQ__NM_005101"
      minerva_ref_type__resource3 "ENTREZ__9636"
      minerva_ref_type__resource4 "ENTREZ__9636"
      minerva_ref_type__resource5 "HGNC_SYMBOL__ISG15"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ISG15"
      minerva_ref_type__resource7 "UNIPROT__P05161"
      minerva_ref_type__resource8 "UNIPROT__P05161"
      minerva_ref_type__resource9 "HGNC__4053"
      minerva_type "Protein"
      minerva_x 2110.0
      minerva_y 1080.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa301"
      species_meta_id "s_id_sa301"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 72
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 700.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sting1__endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa171"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa171"
      minerva_former_symbols "TMEM173"
      minerva_name "STING1"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=STING1"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000184584"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=STING1"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/pubmed/24622840"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/Q86WV6"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/Q86WV6"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/340061"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/340061"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/27962"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_198282"
      minerva_ref_type__resource1 "HGNC_SYMBOL__STING1"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000184584"
      minerva_ref_type__resource2 "HGNC_SYMBOL__STING1"
      minerva_ref_type__resource3 "PUBMED__24622840"
      minerva_ref_type__resource4 "UNIPROT__Q86WV6"
      minerva_ref_type__resource5 "UNIPROT__Q86WV6"
      minerva_ref_type__resource6 "ENTREZ__340061"
      minerva_ref_type__resource7 "ENTREZ__340061"
      minerva_ref_type__resource8 "HGNC__27962"
      minerva_ref_type__resource9 "REFSEQ__NM_198282"
      minerva_type "Protein"
      minerva_x 5217.5
      minerva_y 1551.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa171"
      species_meta_id "s_id_sa171"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 73
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 800.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "stat1_slash_2_underscore_irf9__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa4"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa4"
      minerva_name "STAT1/2_IRF9"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link2 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource2 "WIKIPATHWAYS__WP4868"
      minerva_type "Complex"
      minerva_x 1460.0
      minerva_y 1420.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa4"
      species_meta_id "s_id_csa4"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 74
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 900.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "oas2__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa158"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa158"
      minerva_name "OAS2"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000111335"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/protein/NM_001032731"
      minerva_ref_link12 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.7.84"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=OAS2"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=OAS2"
      minerva_ref_link4 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P29728"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P29728"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4939"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4939"
      minerva_ref_link9 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/8087"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000111335"
      minerva_ref_type__resource11 "REFSEQ__NM_001032731"
      minerva_ref_type__resource12 "EC__2.7.7.84"
      minerva_ref_type__resource2 "HGNC_SYMBOL__OAS2"
      minerva_ref_type__resource3 "HGNC_SYMBOL__OAS2"
      minerva_ref_type__resource4 "WIKIPATHWAYS__WP4868"
      minerva_ref_type__resource5 "UNIPROT__P29728"
      minerva_ref_type__resource6 "UNIPROT__P29728"
      minerva_ref_type__resource7 "ENTREZ__4939"
      minerva_ref_type__resource8 "ENTREZ__4939"
      minerva_ref_type__resource9 "HGNC__8087"
      minerva_type "Protein"
      minerva_x 699.454545454546
      minerva_y 2214.318181818182
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa158"
      species_meta_id "s_id_sa158"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 75
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1000.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp3__cell__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa123"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa123"
      minerva_name "Nsp3"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link2 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/YP_009725299"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource2 "WIKIPATHWAYS__WP4868"
      minerva_ref_type__resource3 "NCBI_PROTEIN__YP_009725299"
      minerva_ref_type__resource4 "UNIPROT__Nsp3"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 3796.7307692307695
      minerva_y 1139.8461538461538
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa123"
      species_meta_id "s_id_sa123"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 77
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1200.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf7a__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa195"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa195"
      minerva_name "Orf7a"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/YP_009724395.1"
      minerva_ref_type__resource1 "NCBI_PROTEIN__YP_009724395.1"
      minerva_type "Protein"
      minerva_x 1980.0
      minerva_y 990.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa195"
      species_meta_id "s_id_sa195"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 78
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 200.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "rig1_underscore_mda5__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa23"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa23"
      minerva_name "RIG1_MDA5"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/pubmed/19052324"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource2 "PUBMED__19052324"
      minerva_type "Complex"
      minerva_x 5655.5
      minerva_y 1771.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa23"
      species_meta_id "s_id_csa23"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 79
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 300.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s22__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa35"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa35"
      minerva_name "s22"
      minerva_type "Degraded"
      minerva_x 2626.5
      minerva_y 2752.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa35"
      species_meta_id "s_id_sa35"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 80
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 400.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "irf3__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa120"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa120"
      minerva_name "IRF3"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IRF3"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/gene/3661"
      minerva_ref_link12 "https://www.ncbi.nlm.nih.gov/gene/3661"
      minerva_ref_link2 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/pubmed/24622840"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6118"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/Q14653"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/Q14653"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000126456"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_001571"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IRF3"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource10 "HGNC_SYMBOL__IRF3"
      minerva_ref_type__resource11 "ENTREZ__3661"
      minerva_ref_type__resource12 "ENTREZ__3661"
      minerva_ref_type__resource2 "WIKIPATHWAYS__WP4868"
      minerva_ref_type__resource3 "PUBMED__24622840"
      minerva_ref_type__resource4 "HGNC__6118"
      minerva_ref_type__resource5 "UNIPROT__Q14653"
      minerva_ref_type__resource6 "UNIPROT__Q14653"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000126456"
      minerva_ref_type__resource8 "REFSEQ__NM_001571"
      minerva_ref_type__resource9 "HGNC_SYMBOL__IRF3"
      minerva_type "Protein"
      minerva_x 4340.0
      minerva_y 1330.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa120"
      species_meta_id "s_id_sa120"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 81
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 500.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mapk8_slash_14__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa8"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa8"
      minerva_name "MAPK8/14"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link2 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource2 "WIKIPATHWAYS__WP4868"
      minerva_type "Complex"
      minerva_x 2960.0
      minerva_y 1420.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa8"
      species_meta_id "s_id_csa8"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 82
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 600.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "traf3__cell__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa83"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa83"
      minerva_name "TRAF3"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link10 "http://purl.uniprot.org/uniprot/Q13114"
      minerva_ref_link11 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12033"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRAF3"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000131323"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRAF3"
      minerva_ref_link5 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/7187"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/7187"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_145725"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q13114"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource10 "UNIPROT__Q13114"
      minerva_ref_type__resource11 "HGNC__12033"
      minerva_ref_type__resource2 "HGNC_SYMBOL__TRAF3"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000131323"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRAF3"
      minerva_ref_type__resource5 "EC__2.3.2.27"
      minerva_ref_type__resource6 "ENTREZ__7187"
      minerva_ref_type__resource7 "ENTREZ__7187"
      minerva_ref_type__resource8 "REFSEQ__NM_145725"
      minerva_ref_type__resource9 "UNIPROT__Q13114"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 4008.5
      minerva_y 891.3333333333334
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa83"
      species_meta_id "s_id_sa83"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 83
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 700.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "irf7__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa213"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa213"
      minerva_name "IRF7"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/Q92985"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q92985"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000185507"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_001572"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IRF7"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IRF7"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6122"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/3665"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/3665"
      minerva_ref_type__resource1 "UNIPROT__Q92985"
      minerva_ref_type__resource2 "UNIPROT__Q92985"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000185507"
      minerva_ref_type__resource4 "REFSEQ__NM_001572"
      minerva_ref_type__resource5 "HGNC_SYMBOL__IRF7"
      minerva_ref_type__resource6 "HGNC_SYMBOL__IRF7"
      minerva_ref_type__resource7 "HGNC__6122"
      minerva_ref_type__resource8 "ENTREZ__3665"
      minerva_ref_type__resource9 "ENTREZ__3665"
      minerva_type "Protein"
      minerva_x 4570.5
      minerva_y 2021.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa213"
      species_meta_id "s_id_sa213"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 84
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 800.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s67__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa79"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa79"
      minerva_name "s67"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_type "Degraded"
      minerva_x 513.5
      minerva_y 2858.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa79"
      species_meta_id "s_id_sa79"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 86
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1000.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa132"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa132"
      minerva_name "Nsp1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/YP_009725297"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource2 "NCBI_PROTEIN__YP_009725297"
      minerva_type "Protein"
      minerva_x 1210.0
      minerva_y 930.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa132"
      species_meta_id "s_id_sa132"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 87
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 1100.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tlr3_underscore_trif__endosome__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa37"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa37"
      minerva_name "TLR3_TRIF"
      minerva_type "Complex"
      minerva_x 900.0
      minerva_y 1125.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "endosome"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa37"
      species_meta_id "s_id_csa37"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 88
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1200.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nlrp3_underscore_inflammasome__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa31"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa31"
      minerva_name "NLRP3_inflammasome"
      minerva_type "Complex"
      minerva_x 2297.5
      minerva_y 2437.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa31"
      species_meta_id "s_id_csa31"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 89
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 200.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "traf3_underscore_tbk1_underscore_ikbke__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa25"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa25"
      minerva_name "TRAF3_TBK1_IKBKE"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/24622840"
      minerva_ref_type__resource1 "PUBMED__24622840"
      minerva_type "Complex"
      minerva_x 5147.0
      minerva_y 977.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa25"
      species_meta_id "s_id_csa25"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 90
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 300.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nfkb1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa269"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa269"
      minerva_name "NFKB1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4790"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/4790"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000109320"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKB1"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKB1"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7794"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P19838"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P19838"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_003998"
      minerva_ref_type__resource1 "ENTREZ__4790"
      minerva_ref_type__resource2 "ENTREZ__4790"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000109320"
      minerva_ref_type__resource4 "HGNC_SYMBOL__NFKB1"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NFKB1"
      minerva_ref_type__resource6 "HGNC__7794"
      minerva_ref_type__resource7 "UNIPROT__P19838"
      minerva_ref_type__resource8 "UNIPROT__P19838"
      minerva_ref_type__resource9 "REFSEQ__NM_003998"
      minerva_type "Protein"
      minerva_x 3240.0
      minerva_y 1840.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa269"
      species_meta_id "s_id_sa269"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 91
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 400.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "rela__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa268"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa268"
      minerva_former_symbols "NFKB3"
      minerva_name "RELA"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/9955"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000173039"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/5970"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/5970"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_021975"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=RELA"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=RELA"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q04206"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q04206"
      minerva_ref_type__resource1 "HGNC__9955"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000173039"
      minerva_ref_type__resource3 "ENTREZ__5970"
      minerva_ref_type__resource4 "ENTREZ__5970"
      minerva_ref_type__resource5 "REFSEQ__NM_021975"
      minerva_ref_type__resource6 "HGNC_SYMBOL__RELA"
      minerva_ref_type__resource7 "HGNC_SYMBOL__RELA"
      minerva_ref_type__resource8 "UNIPROT__Q04206"
      minerva_ref_type__resource9 "UNIPROT__Q04206"
      minerva_type "Protein"
      minerva_x 3100.0
      minerva_y 1840.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa268"
      species_meta_id "s_id_sa268"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 92
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 500.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp16__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa319"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa319"
      minerva_name "Nsp16"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/YP_009725309"
      minerva_ref_type__resource1 "NCBI_PROTEIN__YP_009725309"
      minerva_type "Protein"
      minerva_x 5590.0
      minerva_y 1970.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa319"
      species_meta_id "s_id_sa319"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 93
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 600.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "traf3_underscore_tbk1_underscore_ikbke__cell__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa26"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa26"
      minerva_name "TRAF3_TBK1_IKBKE"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/24622840"
      minerva_ref_type__resource1 "PUBMED__24622840"
      minerva_state1 "active"
      minerva_type "Complex"
      minerva_x 5631.454545454544
      minerva_y 977.9545454545453
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa26"
      species_meta_id "s_id_csa26"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 94
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 700.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "map3k7__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa85"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa85"
      minerva_former_symbols "TAK1"
      minerva_name "MAP3K7"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP3K7"
      minerva_ref_link11 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP3K7"
      minerva_ref_link12 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.25"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/O43318"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/O43318"
      minerva_ref_link4 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000135341"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_145331"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6859"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/6885"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/6885"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource10 "HGNC_SYMBOL__MAP3K7"
      minerva_ref_type__resource11 "HGNC_SYMBOL__MAP3K7"
      minerva_ref_type__resource12 "EC__2.7.11.25"
      minerva_ref_type__resource2 "UNIPROT__O43318"
      minerva_ref_type__resource3 "UNIPROT__O43318"
      minerva_ref_type__resource4 "WIKIPATHWAYS__WP4868"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000135341"
      minerva_ref_type__resource6 "REFSEQ__NM_145331"
      minerva_ref_type__resource7 "HGNC__6859"
      minerva_ref_type__resource8 "ENTREZ__6885"
      minerva_ref_type__resource9 "ENTREZ__6885"
      minerva_type "Protein"
      minerva_x 3252.5
      minerva_y 1037.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa85"
      species_meta_id "s_id_sa85"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 95
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 800.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tlr7__endosome__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa55"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa55"
      minerva_name "TLR7"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/51284"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q9NYK1"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/Q9NYK1"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/15631"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_016562"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TLR7"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TLR7"
      minerva_ref_link8 "https://www.ensembl.org/id/ENSG00000196664"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/51284"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource10 "ENTREZ__51284"
      minerva_ref_type__resource2 "UNIPROT__Q9NYK1"
      minerva_ref_type__resource3 "UNIPROT__Q9NYK1"
      minerva_ref_type__resource4 "HGNC__15631"
      minerva_ref_type__resource5 "REFSEQ__NM_016562"
      minerva_ref_type__resource6 "HGNC_SYMBOL__TLR7"
      minerva_ref_type__resource7 "HGNC_SYMBOL__TLR7"
      minerva_ref_type__resource8 "ENSEMBL__ENSG00000196664"
      minerva_ref_type__resource9 "ENTREZ__51284"
      minerva_type "Protein"
      minerva_x 670.0
      minerva_y 580.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "endosome"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa55"
      species_meta_id "s_id_sa55"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 96
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 900.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ikkb__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa267"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa267"
      minerva_name "IKKb"
      minerva_type "Protein"
      minerva_x 2440.0
      minerva_y 2030.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa267"
      species_meta_id "s_id_sa267"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 97
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1000.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "trif__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa240"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa240"
      minerva_name "TRIF"
      minerva_type "Protein"
      minerva_x 1280.0
      minerva_y 1210.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa240"
      species_meta_id "s_id_sa240"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 98
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1100.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "jak1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa9"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa9"
      minerva_former_symbols "JAK1B"
      minerva_name "JAK1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link10 "http://purl.uniprot.org/uniprot/P23458"
      minerva_ref_link11 "http://purl.uniprot.org/uniprot/P23458"
      minerva_ref_link12 "https://www.ncbi.nlm.nih.gov/protein/NM_002227"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=JAK1"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=JAK1"
      minerva_ref_link4 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_link5 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.10.2"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6190"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/3716"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/3716"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000162434"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource10 "UNIPROT__P23458"
      minerva_ref_type__resource11 "UNIPROT__P23458"
      minerva_ref_type__resource12 "REFSEQ__NM_002227"
      minerva_ref_type__resource2 "HGNC_SYMBOL__JAK1"
      minerva_ref_type__resource3 "HGNC_SYMBOL__JAK1"
      minerva_ref_type__resource4 "WIKIPATHWAYS__WP4868"
      minerva_ref_type__resource5 "EC__2.7.10.2"
      minerva_ref_type__resource6 "HGNC__6190"
      minerva_ref_type__resource7 "ENTREZ__3716"
      minerva_ref_type__resource8 "ENTREZ__3716"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000162434"
      minerva_type "Protein"
      minerva_x 2230.0
      minerva_y 680.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa9"
      species_meta_id "s_id_sa9"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 99
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1200.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ifna1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa30"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa30"
      minerva_name "IFNA1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000197919"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IFNA1"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IFNA1"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/3439"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/3439"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_024013"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/P01562"
      minerva_ref_link8 "https://purl.uniprot.org/uniprot/P01562"
      minerva_ref_link9 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/5417"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000197919"
      minerva_ref_type__resource2 "HGNC_SYMBOL__IFNA1"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IFNA1"
      minerva_ref_type__resource4 "ENTREZ__3439"
      minerva_ref_type__resource5 "ENTREZ__3439"
      minerva_ref_type__resource6 "REFSEQ__NM_024013"
      minerva_ref_type__resource7 "UNIPROT__P01562"
      minerva_ref_type__resource8 "UNIPROT__P01562"
      minerva_ref_type__resource9 "HGNC__5417"
      minerva_type "Protein"
      minerva_x 2530.0
      minerva_y 3400.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa30"
      species_meta_id "s_id_sa30"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 101
    zlevel -1

    cluster [
      cluster "s_id_ca6"
    ]
    graphics [
      x 300.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mavs__mitochondrion__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa151"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa151"
      minerva_name "MAVS"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/57506"
      minerva_ref_link11 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/29233"
      minerva_ref_link12 "https://www.ncbi.nlm.nih.gov/protein/NM_020746"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q7Z434"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/Q7Z434"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/pubmed/24622840"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAVS"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAVS"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/pubmed/19052324"
      minerva_ref_link8 "https://www.ensembl.org/id/ENSG00000088888"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/57506"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource10 "ENTREZ__57506"
      minerva_ref_type__resource11 "HGNC__29233"
      minerva_ref_type__resource12 "REFSEQ__NM_020746"
      minerva_ref_type__resource2 "UNIPROT__Q7Z434"
      minerva_ref_type__resource3 "UNIPROT__Q7Z434"
      minerva_ref_type__resource4 "PUBMED__24622840"
      minerva_ref_type__resource5 "HGNC_SYMBOL__MAVS"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MAVS"
      minerva_ref_type__resource7 "PUBMED__19052324"
      minerva_ref_type__resource8 "ENSEMBL__ENSG00000088888"
      minerva_ref_type__resource9 "ENTREZ__57506"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 4883.5
      minerva_y 1929.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca6"
      compartment_name "mitochondrion"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa151"
      species_meta_id "s_id_sa151"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 102
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 400.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "traf3_underscore_tank_underscore_tbk1_underscore_ikkepsilon__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa29"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa29"
      minerva_name "TRAF3_TANK_TBK1_IKKepsilon"
      minerva_type "Complex"
      minerva_x 5107.5
      minerva_y 687.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa29"
      species_meta_id "s_id_csa29"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 103
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 500.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "irf7_underscore_homodimer__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa43"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa43"
      minerva_name "IRF7_homodimer"
      minerva_type "Complex"
      minerva_x 4330.0
      minerva_y 2300.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa43"
      species_meta_id "s_id_csa43"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 104
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 600.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp10__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa317"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa317"
      minerva_name "Nsp10"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/YP_009725306"
      minerva_ref_type__resource1 "NCBI_PROTEIN__YP_009725306"
      minerva_type "Protein"
      minerva_x 5760.0
      minerva_y 2080.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa317"
      species_meta_id "s_id_sa317"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 105
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 700.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tab1_slash_2_underscore_traf6_underscore_tak1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa39"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa39"
      minerva_name "TAB1/2_TRAF6_TAK1"
      minerva_type "Complex"
      minerva_x 480.0
      minerva_y 1850.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa39"
      species_meta_id "s_id_csa39"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 106
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 800.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "oas3__cell__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa80"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa80"
      minerva_name "OAS3"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link10 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/8088"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/gene/4940"
      minerva_ref_link12 "https://www.ncbi.nlm.nih.gov/gene/4940"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=OAS3"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=OAS3"
      minerva_ref_link4 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/Q9Y6K5"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/Q9Y6K5"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000111331"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_006187"
      minerva_ref_link9 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.7.84"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource10 "HGNC__8088"
      minerva_ref_type__resource11 "ENTREZ__4940"
      minerva_ref_type__resource12 "ENTREZ__4940"
      minerva_ref_type__resource2 "HGNC_SYMBOL__OAS3"
      minerva_ref_type__resource3 "HGNC_SYMBOL__OAS3"
      minerva_ref_type__resource4 "WIKIPATHWAYS__WP4868"
      minerva_ref_type__resource5 "UNIPROT__Q9Y6K5"
      minerva_ref_type__resource6 "UNIPROT__Q9Y6K5"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000111331"
      minerva_ref_type__resource8 "REFSEQ__NM_006187"
      minerva_ref_type__resource9 "EC__2.7.7.84"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 1016.954545454546
      minerva_y 2130.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa80"
      species_meta_id "s_id_sa80"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 107
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 900.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "itch__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa203"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa203"
      minerva_name "ITCH"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/Q96J02"
      minerva_ref_link10 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/13890"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q96J02"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.26"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ITCH"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ITCH"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000078747"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_001257137"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/83737"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/83737"
      minerva_ref_type__resource1 "UNIPROT__Q96J02"
      minerva_ref_type__resource10 "HGNC__13890"
      minerva_ref_type__resource2 "UNIPROT__Q96J02"
      minerva_ref_type__resource3 "EC__2.3.2.26"
      minerva_ref_type__resource4 "HGNC_SYMBOL__ITCH"
      minerva_ref_type__resource5 "HGNC_SYMBOL__ITCH"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000078747"
      minerva_ref_type__resource7 "REFSEQ__NM_001257137"
      minerva_ref_type__resource8 "ENTREZ__83737"
      minerva_ref_type__resource9 "ENTREZ__83737"
      minerva_type "Protein"
      minerva_x 5188.5
      minerva_y 2187.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa203"
      species_meta_id "s_id_sa203"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 108
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1000.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "viral_space_replication__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa162"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa162"
      minerva_name "Viral replication"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/pubmed/19052324"
      minerva_ref_link3 "http://id.nlm.nih.gov/mesh/D014779"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource2 "PUBMED__19052324"
      minerva_ref_type__resource3 "MESH_2012__D014779"
      minerva_type "Phenotype"
      minerva_x 5703.40909090909
      minerva_y 1546.1363636363635
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa162"
      species_meta_id "s_id_sa162"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 110
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1200.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ikbke__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa119"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa119"
      minerva_name "IKBKE"
      minerva_ref_link1 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.10"
      minerva_ref_link11 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IKBKE"
      minerva_ref_link12 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IKBKE"
      minerva_ref_link13 "https://www.ensembl.org/id/ENSG00000263528"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/9641"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/9641"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/14552"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_001193321"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/Q14164"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14164"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/pubmed/24622840"
      minerva_ref_type__resource1 "WIKIPATHWAYS__WP4868"
      minerva_ref_type__resource10 "EC__2.7.11.10"
      minerva_ref_type__resource11 "HGNC_SYMBOL__IKBKE"
      minerva_ref_type__resource12 "HGNC_SYMBOL__IKBKE"
      minerva_ref_type__resource13 "ENSEMBL__ENSG00000263528"
      minerva_ref_type__resource2 "ENTREZ__9641"
      minerva_ref_type__resource3 "ENTREZ__9641"
      minerva_ref_type__resource4 "HGNC__14552"
      minerva_ref_type__resource5 "REFSEQ__NM_001193321"
      minerva_ref_type__resource6 "PUBMED__31226023"
      minerva_ref_type__resource7 "UNIPROT__Q14164"
      minerva_ref_type__resource8 "UNIPROT__Q14164"
      minerva_ref_type__resource9 "PUBMED__24622840"
      minerva_type "Protein"
      minerva_x 3996.0
      minerva_y 1149.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa119"
      species_meta_id "s_id_sa119"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 111
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 200.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ifih1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa198"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa198"
      minerva_name "IFIH1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.6.4.13"
      minerva_ref_link11 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IFIH1"
      minerva_ref_link12 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IFIH1"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/18873"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/64135"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/64135"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_022168"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000115267"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/pubmed/19052324"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q9BYX4"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q9BYX4"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource10 "EC__3.6.4.13"
      minerva_ref_type__resource11 "HGNC_SYMBOL__IFIH1"
      minerva_ref_type__resource12 "HGNC_SYMBOL__IFIH1"
      minerva_ref_type__resource2 "HGNC__18873"
      minerva_ref_type__resource3 "ENTREZ__64135"
      minerva_ref_type__resource4 "ENTREZ__64135"
      minerva_ref_type__resource5 "REFSEQ__NM_022168"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000115267"
      minerva_ref_type__resource7 "PUBMED__19052324"
      minerva_ref_type__resource8 "UNIPROT__Q9BYX4"
      minerva_ref_type__resource9 "UNIPROT__Q9BYX4"
      minerva_type "Protein"
      minerva_x 6290.785714285714
      minerva_y 2036.9285714285713
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa198"
      species_meta_id "s_id_sa198"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 113
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ifna1_underscore_ifnar__cell_space_membrane__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa2"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa2"
      minerva_name "IFNA1_IFNAR"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link2 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource2 "WIKIPATHWAYS__WP4868"
      minerva_type "Complex"
      minerva_x 2035.772727272727
      minerva_y 381.1590909090909
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell_space_membrane"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa2"
      species_meta_id "s_id_csa2"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 114
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 500.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tlr3_underscore_trif_underscore_ripk1__endosome__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa38"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa38"
      minerva_name "TLR3_TRIF_RIPK1"
      minerva_type "Complex"
      minerva_x 530.0
      minerva_y 1255.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "endosome"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa38"
      species_meta_id "s_id_csa38"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 116
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 700.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ikka_underscore_ikkb_underscore_nemo__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa40"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa40"
      minerva_name "IKKa_IKKb_NEMO"
      minerva_type "Complex"
      minerva_x 2765.0
      minerva_y 1932.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa40"
      species_meta_id "s_id_csa40"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 117
    zlevel -1

    cluster [
      cluster "s_id_ca8"
    ]
    graphics [
      x 800.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ifnb1__extracellular_space_space__extracellular space__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa3"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa3"
      minerva_former_symbols "IFNB"
      minerva_name "IFNB1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/3456"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/gene/3456"
      minerva_ref_link2 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000171855"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IFNB1"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IFNB1"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/5434"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/P01574"
      minerva_ref_link8 "https://purl.uniprot.org/uniprot/P01574"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_002176"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource10 "ENTREZ__3456"
      minerva_ref_type__resource11 "ENTREZ__3456"
      minerva_ref_type__resource2 "WIKIPATHWAYS__WP4868"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000171855"
      minerva_ref_type__resource4 "HGNC_SYMBOL__IFNB1"
      minerva_ref_type__resource5 "HGNC_SYMBOL__IFNB1"
      minerva_ref_type__resource6 "HGNC__5434"
      minerva_ref_type__resource7 "UNIPROT__P01574"
      minerva_ref_type__resource8 "UNIPROT__P01574"
      minerva_ref_type__resource9 "REFSEQ__NM_002176"
      minerva_structuralState "Extracellular Space"
      minerva_type "Protein"
      minerva_x 2460.0
      minerva_y 180.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca8"
      compartment_name "extracellular_space_space"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa3"
      species_meta_id "s_id_sa3"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 119
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1000.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ap_minus_1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa10"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa10"
      minerva_name "AP-1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link2 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource2 "WIKIPATHWAYS__WP4868"
      minerva_type "Complex"
      minerva_x 3570.0
      minerva_y 1690.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa10"
      species_meta_id "s_id_csa10"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 120
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1100.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "myd88__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa57"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa57"
      minerva_name "MYD88"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link10 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7562"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/protein/NM_002468"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/4615"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000172936"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4615"
      minerva_ref_link5 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/Q99836"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/Q99836"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MYD88"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MYD88"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource10 "HGNC__7562"
      minerva_ref_type__resource11 "REFSEQ__NM_002468"
      minerva_ref_type__resource2 "ENTREZ__4615"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000172936"
      minerva_ref_type__resource4 "ENTREZ__4615"
      minerva_ref_type__resource5 "WIKIPATHWAYS__WP4868"
      minerva_ref_type__resource6 "UNIPROT__Q99836"
      minerva_ref_type__resource7 "UNIPROT__Q99836"
      minerva_ref_type__resource8 "HGNC_SYMBOL__MYD88"
      minerva_ref_type__resource9 "HGNC_SYMBOL__MYD88"
      minerva_type "Protein"
      minerva_x 3390.0
      minerva_y 540.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa57"
      species_meta_id "s_id_sa57"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 121
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1200.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "stat2__cell__phosphorylated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa69"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa69"
      minerva_name "STAT2"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005419"
      minerva_ref_link11 "https://www.ensembl.org/id/ENSG00000170581"
      minerva_ref_link2 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P52630"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P52630"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=STAT2"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=STAT2"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11363"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/6773"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/6773"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource10 "REFSEQ__NM_005419"
      minerva_ref_type__resource11 "ENSEMBL__ENSG00000170581"
      minerva_ref_type__resource2 "WIKIPATHWAYS__WP4868"
      minerva_ref_type__resource3 "UNIPROT__P52630"
      minerva_ref_type__resource4 "UNIPROT__P52630"
      minerva_ref_type__resource5 "HGNC_SYMBOL__STAT2"
      minerva_ref_type__resource6 "HGNC_SYMBOL__STAT2"
      minerva_ref_type__resource7 "HGNC__11363"
      minerva_ref_type__resource8 "ENTREZ__6773"
      minerva_ref_type__resource9 "ENTREZ__6773"
      minerva_state1 "PHOSPHORYLATED"
      minerva_type "Protein"
      minerva_x 1680.0
      minerva_y 1090.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa69"
      species_meta_id "s_id_sa69"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 122
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 200.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "n__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa140"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa140"
      minerva_name "N"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/BCD58761"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/YP_009724397.2"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource2 "NCBI_PROTEIN__BCD58761"
      minerva_ref_type__resource3 "NCBI_PROTEIN__YP_009724397.2"
      minerva_type "Protein"
      minerva_x 6060.0
      minerva_y 1453.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa140"
      species_meta_id "s_id_sa140"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 123
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 300.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "irf7_underscore_homodimer__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa44"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa44"
      minerva_name "IRF7_homodimer"
      minerva_type "Complex"
      minerva_x 4330.0
      minerva_y 2910.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa44"
      species_meta_id "s_id_csa44"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 124
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 400.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s63__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa75"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa75"
      minerva_name "s63"
      minerva_type "Degraded"
      minerva_x 778.5
      minerva_y 2858.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa75"
      species_meta_id "s_id_sa75"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 125
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 500.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tak1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa257"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa257"
      minerva_name "TAK1"
      minerva_type "Protein"
      minerva_x 330.0
      minerva_y 1500.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa257"
      species_meta_id "s_id_sa257"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 126
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 600.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "oas3__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa157"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa157"
      minerva_name "OAS3"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link10 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/8088"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/gene/4940"
      minerva_ref_link12 "https://www.ncbi.nlm.nih.gov/gene/4940"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=OAS3"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=OAS3"
      minerva_ref_link4 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/Q9Y6K5"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/Q9Y6K5"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000111331"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_006187"
      minerva_ref_link9 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.7.84"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource10 "HGNC__8088"
      minerva_ref_type__resource11 "ENTREZ__4940"
      minerva_ref_type__resource12 "ENTREZ__4940"
      minerva_ref_type__resource2 "HGNC_SYMBOL__OAS3"
      minerva_ref_type__resource3 "HGNC_SYMBOL__OAS3"
      minerva_ref_type__resource4 "WIKIPATHWAYS__WP4868"
      minerva_ref_type__resource5 "UNIPROT__Q9Y6K5"
      minerva_ref_type__resource6 "UNIPROT__Q9Y6K5"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000111331"
      minerva_ref_type__resource8 "REFSEQ__NM_006187"
      minerva_ref_type__resource9 "EC__2.7.7.84"
      minerva_type "Protein"
      minerva_x 700.0
      minerva_y 2130.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa157"
      species_meta_id "s_id_sa157"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 127
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 700.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tlr3__endosome__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa239"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa239"
      minerva_name "TLR3"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/O15455"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/O15455"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000164342"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_003265"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/7098"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/7098"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TLR3"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TLR3"
      minerva_ref_link9 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11849"
      minerva_ref_type__resource1 "UNIPROT__O15455"
      minerva_ref_type__resource2 "UNIPROT__O15455"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000164342"
      minerva_ref_type__resource4 "REFSEQ__NM_003265"
      minerva_ref_type__resource5 "ENTREZ__7098"
      minerva_ref_type__resource6 "ENTREZ__7098"
      minerva_ref_type__resource7 "HGNC_SYMBOL__TLR3"
      minerva_ref_type__resource8 "HGNC_SYMBOL__TLR3"
      minerva_ref_type__resource9 "HGNC__11849"
      minerva_type "Protein"
      minerva_x 1080.0
      minerva_y 890.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "endosome"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa239"
      species_meta_id "s_id_sa239"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 128
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 800.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ap_minus_1__nucleus__translocated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa35"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa35"
      minerva_name "AP-1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link2 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource2 "WIKIPATHWAYS__WP4868"
      minerva_state1 "translocated"
      minerva_type "Complex"
      minerva_x 3697.5
      minerva_y 2837.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa35"
      species_meta_id "s_id_csa35"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 129
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 900.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "isg_space_expression_underscore_antiviral_space_response__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa81"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa81"
      minerva_name "ISG expression_antiviral response"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link2 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_link3 "http://id.nlm.nih.gov/mesh/D007113"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource2 "WIKIPATHWAYS__WP4868"
      minerva_ref_type__resource3 "MESH_2012__D007113"
      minerva_type "Phenotype"
      minerva_x 1530.0
      minerva_y 2560.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa81"
      species_meta_id "s_id_sa81"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 130
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 1000.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sting1__endoplasmic_space_reticulum__ubiquitinated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa172"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa172"
      minerva_former_symbols "TMEM173"
      minerva_name "STING1"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=STING1"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000184584"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=STING1"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/pubmed/24622840"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/Q86WV6"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/Q86WV6"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/340061"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/340061"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/27962"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_198282"
      minerva_ref_type__resource1 "HGNC_SYMBOL__STING1"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000184584"
      minerva_ref_type__resource2 "HGNC_SYMBOL__STING1"
      minerva_ref_type__resource3 "PUBMED__24622840"
      minerva_ref_type__resource4 "UNIPROT__Q86WV6"
      minerva_ref_type__resource5 "UNIPROT__Q86WV6"
      minerva_ref_type__resource6 "ENTREZ__340061"
      minerva_ref_type__resource7 "ENTREZ__340061"
      minerva_ref_type__resource8 "HGNC__27962"
      minerva_ref_type__resource9 "REFSEQ__NM_198282"
      minerva_state1 "UBIQUITINATED"
      minerva_type "Protein"
      minerva_x 4978.153846153846
      minerva_y 1551.2692307692307
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa172"
      species_meta_id "s_id_sa172"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 131
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 1100.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "viral_space_dsrna__endosome__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa259"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa259"
      minerva_name "Viral dsRNA"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/pubmed/19052324"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=694009"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource2 "PUBMED__19052324"
      minerva_ref_type__resource3 "TAXONOMY__694009"
      minerva_type "RNA"
      minerva_x 690.0
      minerva_y 810.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "endosome"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa259"
      species_meta_id "s_id_sa259"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 132
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1200.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf8__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa27"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa27"
      minerva_name "Orf8"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/YP_009724396.1"
      minerva_ref_type__resource1 "NCBI_PROTEIN__YP_009724396.1"
      minerva_type "Complex"
      minerva_x 4268.5
      minerva_y 1457.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa27"
      species_meta_id "s_id_csa27"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 133
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 200.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "trim25__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa201"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa201"
      minerva_former_symbols "ZNF147"
      minerva_name "TRIM25"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "Protein"
      minerva_x 6182.5
      minerva_y 1596.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa201"
      species_meta_id "s_id_sa201"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 135
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 400.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "oas1_underscore_eif2ak__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa32"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa32"
      minerva_name "OAS1_EIF2AK"
      minerva_type "Complex"
      minerva_x 909.454545454546
      minerva_y 2509.318181818182
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa32"
      species_meta_id "s_id_csa32"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 136
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 500.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "fos__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa87"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa87"
      minerva_name "FOS"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=FOS"
      minerva_ref_link11 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=FOS"
      minerva_ref_link2 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/3796"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000170345"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_005252"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P01100"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P01100"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/2353"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/2353"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource10 "HGNC_SYMBOL__FOS"
      minerva_ref_type__resource11 "HGNC_SYMBOL__FOS"
      minerva_ref_type__resource2 "WIKIPATHWAYS__WP4868"
      minerva_ref_type__resource3 "HGNC__3796"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000170345"
      minerva_ref_type__resource5 "REFSEQ__NM_005252"
      minerva_ref_type__resource6 "UNIPROT__P01100"
      minerva_ref_type__resource7 "UNIPROT__P01100"
      minerva_ref_type__resource8 "ENTREZ__2353"
      minerva_ref_type__resource9 "ENTREZ__2353"
      minerva_type "Protein"
      minerva_x 3690.0
      minerva_y 1510.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa87"
      species_meta_id "s_id_sa87"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 137
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 600.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "oas1__cell__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa50"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa50"
      minerva_former_symbols "OIAS"
      minerva_name "OAS1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link10 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/8086"
      minerva_ref_link11 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.7.84"
      minerva_ref_link12 "https://www.ensembl.org/id/ENSG00000089127"
      minerva_ref_link2 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P00973"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P00973"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/4938"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/4938"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_001032409"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=OAS1"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=OAS1"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource10 "HGNC__8086"
      minerva_ref_type__resource11 "EC__2.7.7.84"
      minerva_ref_type__resource12 "ENSEMBL__ENSG00000089127"
      minerva_ref_type__resource2 "WIKIPATHWAYS__WP4868"
      minerva_ref_type__resource3 "UNIPROT__P00973"
      minerva_ref_type__resource4 "UNIPROT__P00973"
      minerva_ref_type__resource5 "ENTREZ__4938"
      minerva_ref_type__resource6 "ENTREZ__4938"
      minerva_ref_type__resource7 "REFSEQ__NM_001032409"
      minerva_ref_type__resource8 "HGNC_SYMBOL__OAS1"
      minerva_ref_type__resource9 "HGNC_SYMBOL__OAS1"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 908.318181818182
      minerva_y 2300.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa50"
      species_meta_id "s_id_sa50"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 138
    zlevel -1

    cluster [
      cluster "s_id_ca8"
    ]
    graphics [
      x 700.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "proinflammatory_space_cytokine_space_expression_underscore_inflammation__extracellular_space_space__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa196"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa196"
      minerva_name "Proinflammatory cytokine expression_Inflammation"
      minerva_ref_link1 "http://id.nlm.nih.gov/mesh/D007249"
      minerva_ref_type__resource1 "MESH_2012__D007249"
      minerva_type "Phenotype"
      minerva_x 3270.0
      minerva_y 3890.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca8"
      compartment_name "extracellular_space_space"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa196"
      species_meta_id "s_id_sa196"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 139
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 800.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s27__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa40"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa40"
      minerva_name "s27"
      minerva_type "Degraded"
      minerva_x 2257.5
      minerva_y 2752.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa40"
      species_meta_id "s_id_sa40"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 140
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 900.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "jak1_underscore_tyk2__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa9"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa9"
      minerva_name "JAK1_TYK2"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link2 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource2 "WIKIPATHWAYS__WP4868"
      minerva_type "Complex"
      minerva_x 1933.25
      minerva_y 735.25
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa9"
      species_meta_id "s_id_csa9"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 141
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1000.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "stat1__cell__phosphorylated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa68"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa68"
      minerva_name "STAT1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link10 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11362"
      minerva_ref_link11 "https://www.ensembl.org/id/ENSG00000115415"
      minerva_ref_link2 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_007315"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=STAT1"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=STAT1"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P42224"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P42224"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/6772"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/6772"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource10 "HGNC__11362"
      minerva_ref_type__resource11 "ENSEMBL__ENSG00000115415"
      minerva_ref_type__resource2 "WIKIPATHWAYS__WP4868"
      minerva_ref_type__resource3 "REFSEQ__NM_007315"
      minerva_ref_type__resource4 "HGNC_SYMBOL__STAT1"
      minerva_ref_type__resource5 "HGNC_SYMBOL__STAT1"
      minerva_ref_type__resource6 "UNIPROT__P42224"
      minerva_ref_type__resource7 "UNIPROT__P42224"
      minerva_ref_type__resource8 "ENTREZ__6772"
      minerva_ref_type__resource9 "ENTREZ__6772"
      minerva_state1 "PHOSPHORYLATED"
      minerva_type "Protein"
      minerva_x 1550.0
      minerva_y 1040.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa68"
      species_meta_id "s_id_sa68"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 142
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1100.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "myd88_underscore_tram__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa20"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa20"
      minerva_name "MYD88_TRAM"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_type "Complex"
      minerva_x 3390.25
      minerva_y 717.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa20"
      species_meta_id "s_id_csa20"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 143
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1200.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp14__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa318"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa318"
      minerva_name "Nsp14"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/YP_009725309"
      minerva_ref_type__resource1 "NCBI_PROTEIN__YP_009725309"
      minerva_type "Protein"
      minerva_x 5750.0
      minerva_y 1970.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa318"
      species_meta_id "s_id_sa318"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 144
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ifnb1_underscore_ifnar__cell_space_membrane__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa3"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa3"
      minerva_name "IFNB1_IFNAR"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link2 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource2 "WIKIPATHWAYS__WP4868"
      minerva_type "Complex"
      minerva_x 2395.272727272727
      minerva_y 388.6590909090909
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell_space_membrane"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa3"
      species_meta_id "s_id_csa3"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 145
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 300.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf3a__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa194"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa194"
      minerva_name "Orf3a"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/YP_009724391.1"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/pubmed/32979938"
      minerva_ref_type__resource1 "NCBI_PROTEIN__YP_009724391.1"
      minerva_ref_type__resource2 "PUBMED__32979938"
      minerva_type "Protein"
      minerva_x 1210.0
      minerva_y 1010.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa194"
      species_meta_id "s_id_sa194"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 148
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 600.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ddx58__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa200"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa200"
      minerva_name "DDX58"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link10 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/19102"
      minerva_ref_link11 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DDX58"
      minerva_ref_link12 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DDX58"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/23586"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/23586"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_014314"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000107201"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/pubmed/19052324"
      minerva_ref_link7 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.6.4.13"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/O95786"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/O95786"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource10 "HGNC__19102"
      minerva_ref_type__resource11 "HGNC_SYMBOL__DDX58"
      minerva_ref_type__resource12 "HGNC_SYMBOL__DDX58"
      minerva_ref_type__resource2 "ENTREZ__23586"
      minerva_ref_type__resource3 "ENTREZ__23586"
      minerva_ref_type__resource4 "REFSEQ__NM_014314"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000107201"
      minerva_ref_type__resource6 "PUBMED__19052324"
      minerva_ref_type__resource7 "EC__3.6.4.13"
      minerva_ref_type__resource8 "UNIPROT__O95786"
      minerva_ref_type__resource9 "UNIPROT__O95786"
      minerva_type "Protein"
      minerva_x 6246.5
      minerva_y 1732.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa200"
      species_meta_id "s_id_sa200"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 149
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 700.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "e__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa131"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa131"
      minerva_name "E"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/BCD58755"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource2 "NCBI_PROTEIN__BCD58755"
      minerva_ref_type__resource3 "UNIPROT__E"
      minerva_type "Protein"
      minerva_x 2620.0
      minerva_y 1510.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa131"
      species_meta_id "s_id_sa131"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 150
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 800.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tyk2__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa19"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa19"
      minerva_name "TYK2"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link10 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12440"
      minerva_ref_link11 "http://purl.uniprot.org/uniprot/P29597"
      minerva_ref_link12 "http://purl.uniprot.org/uniprot/P29597"
      minerva_ref_link2 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.10.2"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_003331"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/7297"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/7297"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000105397"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TYK2"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TYK2"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource10 "HGNC__12440"
      minerva_ref_type__resource11 "UNIPROT__P29597"
      minerva_ref_type__resource12 "UNIPROT__P29597"
      minerva_ref_type__resource2 "WIKIPATHWAYS__WP4868"
      minerva_ref_type__resource3 "EC__2.7.10.2"
      minerva_ref_type__resource4 "REFSEQ__NM_003331"
      minerva_ref_type__resource5 "ENTREZ__7297"
      minerva_ref_type__resource6 "ENTREZ__7297"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000105397"
      minerva_ref_type__resource8 "HGNC_SYMBOL__TYK2"
      minerva_ref_type__resource9 "HGNC_SYMBOL__TYK2"
      minerva_type "Protein"
      minerva_x 2230.0
      minerva_y 780.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa19"
      species_meta_id "s_id_sa19"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 152
    zlevel -1

    cluster [
      cluster "s_id_ca8"
    ]
    graphics [
      x 1000.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ifnb1__extracellular_space_space__secreted__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa27"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa27"
      minerva_former_symbols "IFNB"
      minerva_name "IFNB1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/3456"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000171855"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IFNB1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IFNB1"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/5434"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P01574"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/P01574"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_002176"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/3456"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource10 "ENTREZ__3456"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000171855"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IFNB1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__IFNB1"
      minerva_ref_type__resource5 "HGNC__5434"
      minerva_ref_type__resource6 "UNIPROT__P01574"
      minerva_ref_type__resource7 "UNIPROT__P01574"
      minerva_ref_type__resource8 "REFSEQ__NM_002176"
      minerva_ref_type__resource9 "ENTREZ__3456"
      minerva_structuralState "secreted"
      minerva_type "Protein"
      minerva_x 2710.0
      minerva_y 3720.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca8"
      compartment_name "extracellular_space_space"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa27"
      species_meta_id "s_id_sa27"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 153
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1100.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "map3k7__cell__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa86"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa86"
      minerva_former_symbols "TAK1"
      minerva_name "MAP3K7"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP3K7"
      minerva_ref_link11 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP3K7"
      minerva_ref_link12 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.25"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/O43318"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/O43318"
      minerva_ref_link4 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000135341"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_145331"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6859"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/6885"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/6885"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource10 "HGNC_SYMBOL__MAP3K7"
      minerva_ref_type__resource11 "HGNC_SYMBOL__MAP3K7"
      minerva_ref_type__resource12 "EC__2.7.11.25"
      minerva_ref_type__resource2 "UNIPROT__O43318"
      minerva_ref_type__resource3 "UNIPROT__O43318"
      minerva_ref_type__resource4 "WIKIPATHWAYS__WP4868"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000135341"
      minerva_ref_type__resource6 "REFSEQ__NM_145331"
      minerva_ref_type__resource7 "HGNC__6859"
      minerva_ref_type__resource8 "ENTREZ__6885"
      minerva_ref_type__resource9 "ENTREZ__6885"
      minerva_structuralState "active"
      minerva_type "Protein"
      minerva_x 3042.5
      minerva_y 1037.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa86"
      species_meta_id "s_id_sa86"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 155
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 200.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nemo__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa265"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa265"
      minerva_name "NEMO"
      minerva_type "Protein"
      minerva_x 2430.0
      minerva_y 1810.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa265"
      species_meta_id "s_id_sa265"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 156
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 300.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "stat1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa305"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa305"
      minerva_name "STAT1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link10 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11362"
      minerva_ref_link11 "https://www.ensembl.org/id/ENSG00000115415"
      minerva_ref_link2 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_007315"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=STAT1"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=STAT1"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P42224"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P42224"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/6772"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/6772"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource10 "HGNC__11362"
      minerva_ref_type__resource11 "ENSEMBL__ENSG00000115415"
      minerva_ref_type__resource2 "WIKIPATHWAYS__WP4868"
      minerva_ref_type__resource3 "REFSEQ__NM_007315"
      minerva_ref_type__resource4 "HGNC_SYMBOL__STAT1"
      minerva_ref_type__resource5 "HGNC_SYMBOL__STAT1"
      minerva_ref_type__resource6 "UNIPROT__P42224"
      minerva_ref_type__resource7 "UNIPROT__P42224"
      minerva_ref_type__resource8 "ENTREZ__6772"
      minerva_ref_type__resource9 "ENTREZ__6772"
      minerva_type "Protein"
      minerva_x 1490.0
      minerva_y 840.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa305"
      species_meta_id "s_id_sa305"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 157
    zlevel -1

    graphics [
      x 400.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re105"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 3574.122807017545
      minerva_y 1547.25
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re105"
      reaction_meta_id "re105"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 163
    zlevel -1

    graphics [
      x 500.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re133"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re133"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 2017.75
      minerva_y 2878.75
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re133"
      reaction_meta_id "re133"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 168
    zlevel -1

    graphics [
      x 600.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re132"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 1495.0
      minerva_y 696.9306930693067
    ]
    sbml [
      reaction_id "re132"
      reaction_meta_id "re132"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 172
    zlevel -1

    graphics [
      x 700.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re97"
      type "text"
    ]
    minerva [
      minerva_type "Trigger"
      minerva_x 1319.4056830038348
      minerva_y 2591.5088144825813
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re97"
      reaction_meta_id "re97"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 175
    zlevel -1

    graphics [
      x 800.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re81"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 5037.5
      minerva_y 2187.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re81"
      reaction_meta_id "re81"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 179
    zlevel -1

    graphics [
      x 900.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re69"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 5518.258522727272
      minerva_y 1546.3863636363635
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re69"
      reaction_meta_id "re69"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 185
    zlevel -1

    graphics [
      x 1000.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re17"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 1729.9993198164334
      minerva_y 2889.1393526252077
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re17"
      reaction_meta_id "re17"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 191
    zlevel -1

    graphics [
      x 1100.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re139"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re139"
      type "text"
    ]
    minerva [
      minerva_type "Trigger"
      minerva_x 1962.5000000000002
      minerva_y 2560.0
    ]
    sbml [
      reaction_id "re139"
      reaction_meta_id "re139"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 194
    zlevel -1

    graphics [
      x 1200.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re134"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re134"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 2101.875
      minerva_y 1981.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re134"
      reaction_meta_id "re134"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 197
    zlevel -1

    graphics [
      x 200.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re115"
      type "text"
    ]
    minerva [
      minerva_type "Heterodimer association"
      minerva_x 479.75
      minerva_y 1656.5390625
    ]
    sbml [
      reaction_id "re115"
      reaction_meta_id "re115"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 204
    zlevel -1

    graphics [
      x 300.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re80"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 6106.431818181818
      minerva_y 1733.3181818181818
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re80"
      reaction_meta_id "re80"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 212
    zlevel -1

    graphics [
      x 400.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re24"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 4137.666666666668
      minerva_y 997.2142857142858
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re24"
      reaction_meta_id "re24"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 219
    zlevel -1

    graphics [
      x 500.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re131"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re131"
      type "text"
    ]
    minerva [
      minerva_type "Heterodimer association"
      minerva_x 1170.0
      minerva_y 606.625
    ]
    sbml [
      reaction_id "re131"
      reaction_meta_id "re131"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 224
    zlevel -1

    graphics [
      x 600.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re121"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re121"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 3615.0
      minerva_y 2066.25
    ]
    sbml [
      reaction_id "re121"
      reaction_meta_id "re121"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 228
    zlevel -1

    graphics [
      x 700.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re34"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 487.2333696161935
      minerva_y 2126.742342844441
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re34"
      reaction_meta_id "re34"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 231
    zlevel -1

    graphics [
      x 800.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re40"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 3147.5
      minerva_y 1037.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re40"
      reaction_meta_id "re40"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 235
    zlevel -1

    graphics [
      x 900.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re32"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 513.0806443209974
      minerva_y 2210.254837183628
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re32"
      reaction_meta_id "re32"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 238
    zlevel -1

    graphics [
      x 1000.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re119"
      type "text"
    ]
    minerva [
      minerva_type "Heterodimer association"
      minerva_x 3275.0
      minerva_y 1923.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re119"
      reaction_meta_id "re119"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 243
    zlevel -1

    graphics [
      x 1100.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re118"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re118"
      type "text"
    ]
    minerva [
      minerva_type "Heterodimer association"
      minerva_x 2568.0
      minerva_y 1926.25
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re118"
      reaction_meta_id "re118"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 249
    zlevel -1

    graphics [
      x 1200.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re86"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 2582.5
      minerva_y 2437.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re86"
      reaction_meta_id "re86"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 255
    zlevel -1

    graphics [
      x 200.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re138"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re138"
      type "text"
    ]
    minerva [
      minerva_type "Transport"
      minerva_x 1465.0
      minerva_y 2166.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re138"
      reaction_meta_id "re138"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 259
    zlevel -1

    graphics [
      x 300.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re42"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 3696.75
      minerva_y 2263.75
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re42"
      reaction_meta_id "re42"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 262
    zlevel -1

    graphics [
      x 400.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re33"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 565.75
      minerva_y 2858.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re33"
      reaction_meta_id "re33"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 268
    zlevel -1

    graphics [
      x 500.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re122"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re122"
      type "text"
    ]
    minerva [
      minerva_type "Dissociation"
      minerva_x 3940.0
      minerva_y 2231.875
    ]
    sbml [
      reaction_id "re122"
      reaction_meta_id "re122"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 272
    zlevel -1

    graphics [
      x 600.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re107"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 4393.958742081261
      minerva_y 997.2708062282461
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re107"
      reaction_meta_id "re107"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 280
    zlevel -1

    graphics [
      x 700.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re13"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 2327.75
      minerva_y 2752.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re13"
      reaction_meta_id "re13"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 287
    zlevel -1

    graphics [
      x 800.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re117"
      type "text"
    ]
    minerva [
      minerva_type "Trigger"
      minerva_x 2703.3333333333335
      minerva_y 3797.9375
    ]
    sbml [
      reaction_id "re117"
      reaction_meta_id "re117"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 290
    zlevel -1

    graphics [
      x 900.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re18"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 538.4940540258357
      minerva_y 2303.8647028264973
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re18"
      reaction_meta_id "re18"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 293
    zlevel -1

    graphics [
      x 1000.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re99"
      type "text"
    ]
    minerva [
      minerva_type "Trigger"
      minerva_x 3427.8125
      minerva_y 3457.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re99"
      reaction_meta_id "re99"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 296
    zlevel -1

    graphics [
      x 1100.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re14"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 2491.612186480014
      minerva_y 3212.122055498913
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re14"
      reaction_meta_id "re14"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 299
    zlevel -1

    graphics [
      x 1200.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re64"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 5007.431818181818
      minerva_y 1929.181818181818
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re64"
      reaction_meta_id "re64"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 305
    zlevel -1

    graphics [
      x 200.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re140"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re140"
      type "text"
    ]
    minerva [
      minerva_type "Trigger"
      minerva_x 2485.3807581786027
      minerva_y 2588.046606984969
    ]
    sbml [
      reaction_id "re140"
      reaction_meta_id "re140"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 308
    zlevel -1

    graphics [
      x 300.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re73"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 5389.227272727272
      minerva_y 1028.7272727272725
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re73"
      reaction_meta_id "re73"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 313
    zlevel -1

    graphics [
      x 400.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re136"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re136"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 1520.0
      minerva_y 940.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re136"
      reaction_meta_id "re136"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 322
    zlevel -1

    graphics [
      x 500.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re113"
      type "text"
    ]
    minerva [
      minerva_type "Heterodimer association"
      minerva_x 1065.0
      minerva_y 1152.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re113"
      reaction_meta_id "re113"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 328
    zlevel -1

    graphics [
      x 600.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re98"
      type "text"
    ]
    minerva [
      minerva_type "Negative influence"
      minerva_x 1529.9999999999995
      minerva_y 2405.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re98"
      reaction_meta_id "re98"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 331
    zlevel -1

    graphics [
      x 700.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re116"
      type "text"
    ]
    minerva [
      minerva_type "Trigger"
      minerva_x 2536.6666666666665
      minerva_y 3797.4375
    ]
    sbml [
      reaction_id "re116"
      reaction_meta_id "re116"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 334
    zlevel -1

    graphics [
      x 800.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re10"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 2697.25
      minerva_y 2752.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re10"
      reaction_meta_id "re10"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 343
    zlevel -1

    graphics [
      x 900.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re102"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 1741.272727272727
      minerva_y 381.9090909090909
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re102"
      reaction_meta_id "re102"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 348
    zlevel -1

    graphics [
      x 1000.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re16"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 1098.25
      minerva_y 2858.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re16"
      reaction_meta_id "re16"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 354
    zlevel -1

    graphics [
      x 1100.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re114"
      type "text"
    ]
    minerva [
      minerva_type "Heterodimer association"
      minerva_x 734.9999999999998
      minerva_y 1247.5000000000002
    ]
    sbml [
      reaction_id "re114"
      reaction_meta_id "re114"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 358
    zlevel -1

    graphics [
      x 1200.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re95"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 2124.0
      minerva_y 718.1530944625408
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re95"
      reaction_meta_id "re95"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 364
    zlevel -1

    graphics [
      x 200.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re72"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 5097.826923076923
      minerva_y 1551.3846153846152
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re72"
      reaction_meta_id "re72"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 369
    zlevel -1

    graphics [
      x 300.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re137"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re137"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 1680.0
      minerva_y 980.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re137"
      reaction_meta_id "re137"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 376
    zlevel -1

    graphics [
      x 400.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re84"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 4458.5
      minerva_y 2021.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re84"
      reaction_meta_id "re84"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 380
    zlevel -1

    graphics [
      x 500.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re135"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re135"
      type "text"
    ]
    minerva [
      minerva_type "Heterodimer association"
      minerva_x 2702.5
      minerva_y 1025.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re135"
      reaction_meta_id "re135"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 385
    zlevel -1

    graphics [
      x 600.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re54"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 4501.0
      minerva_y 1328.25
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re54"
      reaction_meta_id "re54"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 397
    zlevel -1

    graphics [
      x 700.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re94"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 908.9868724713815
      minerva_y 2389.9154065798184
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re94"
      reaction_meta_id "re94"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 403
    zlevel -1

    graphics [
      x 800.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re12"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 2766.625404474232
      minerva_y 3212.97596920231
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re12"
      reaction_meta_id "re12"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 406
    zlevel -1

    graphics [
      x 900.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re130"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re130"
      type "text"
    ]
    minerva [
      minerva_type "Transport"
      minerva_x 4645.0
      minerva_y 2265.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re130"
      reaction_meta_id "re130"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 410
    zlevel -1

    graphics [
      x 1000.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re82"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 5415.227272727272
      minerva_y 839.4772727272726
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re82"
      reaction_meta_id "re82"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 417
    zlevel -1

    graphics [
      x 1100.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re103"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 2313.022727272726
      minerva_y 212.23863636363023
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re103"
      reaction_meta_id "re103"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 422
    zlevel -1

    graphics [
      x 1200.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re129"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re129"
      type "text"
    ]
    minerva [
      minerva_type "Heterodimer association"
      minerva_x 4690.0
      minerva_y 1447.5
    ]
    sbml [
      reaction_id "re129"
      reaction_meta_id "re129"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 426
    zlevel -1

    graphics [
      x 200.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re96"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 3390.164237668161
      minerva_y 613.25
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re96"
      reaction_meta_id "re96"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 434
    zlevel -1

    graphics [
      x 300.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re111"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 4951.799137935039
      minerva_y 1028.0500018578987
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re111"
      reaction_meta_id "re111"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 439
    zlevel -1

    graphics [
      x 400.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re67"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 804.159090909091
      minerva_y 2300.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re67"
      reaction_meta_id "re67"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 443
    zlevel -1

    graphics [
      x 500.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re110"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 5848.681818181818
      minerva_y 1734.3181818181818
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re110"
      reaction_meta_id "re110"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 455
    zlevel -1

    graphics [
      x 600.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re71"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 3648.1153846153848
      minerva_y 1139.923076923077
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re71"
      reaction_meta_id "re71"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 458
    zlevel -1

    graphics [
      x 700.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re8"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 2530.0
      minerva_y 3561.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re8"
      reaction_meta_id "re8"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 461
    zlevel -1

    graphics [
      x 800.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re19"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 1841.714920066089
      minerva_y 2584.026393281917
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re19"
      reaction_meta_id "re19"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 464
    zlevel -1

    graphics [
      x 900.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re31"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 835.75
      minerva_y 2858.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re31"
      reaction_meta_id "re31"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 470
    zlevel -1

    graphics [
      x 1000.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re65"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 858.477272727273
      minerva_y 2130.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re65"
      reaction_meta_id "re65"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 474
    zlevel -1

    graphics [
      x 1100.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re66"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 831.704545454546
      minerva_y 2214.659090909091
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re66"
      reaction_meta_id "re66"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 478
    zlevel -1

    graphics [
      x 1200.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re39"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 3878.0
      minerva_y 891.3333333333334
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re39"
      reaction_meta_id "re39"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 483
    zlevel -1

    graphics [
      x 200.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re128"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re128"
      type "text"
    ]
    minerva [
      minerva_type "Transport"
      minerva_x 4330.0
      minerva_y 2605.0
    ]
    sbml [
      reaction_id "re128"
      reaction_meta_id "re128"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 486
    zlevel -1

    graphics [
      x 300.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re68"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 1267.909090909091
      minerva_y 2304.075757575758
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re68"
      reaction_meta_id "re68"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 490
    zlevel -1

    graphics [
      x 400.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re92"
      type "text"
    ]
    minerva [
      minerva_type "Trigger"
      minerva_x 2710.92231533629
      minerva_y 2559.9488477719588
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re92"
      reaction_meta_id "re92"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 493
    zlevel -1

    graphics [
      x 500.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re62"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 3516.25
      minerva_y 2748.75
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re62"
      reaction_meta_id "re62"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 496
    zlevel -1

    graphics [
      x 600.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re9"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 2710.0
      minerva_y 3560.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re9"
      reaction_meta_id "re9"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 499
    zlevel -1

    graphics [
      x 700.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re52"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 4109.0
      minerva_y 1149.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re52"
      reaction_meta_id "re52"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 504
    zlevel -1

    graphics [
      x 800.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re100"
      type "text"
    ]
    minerva [
      minerva_type "Trigger"
      minerva_x 3111.847770657226
      minerva_y 3193.9876158089546
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re100"
      reaction_meta_id "re100"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 507
    zlevel -1

    graphics [
      x 900.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re104"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 2759.75
      minerva_y 1386.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re104"
      reaction_meta_id "re104"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 513
    zlevel -1

    graphics [
      x 1000.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re76"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 6129.467532467532
      minerva_y 2037.246753246753
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re76"
      reaction_meta_id "re76"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 518
    zlevel -1

    graphics [
      x 1100.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re127"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re127"
      type "text"
    ]
    minerva [
      minerva_type "Heterodimer association"
      minerva_x 4325.0
      minerva_y 2172.5
    ]
    sbml [
      reaction_id "re127"
      reaction_meta_id "re127"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 522
    zlevel -1

    graphics [
      x 1200.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re89"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 1498.9943609022557
      minerva_y 1190.75
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re89"
      reaction_meta_id "re89"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 531
    zlevel -1

    graphics [
      x 200.0
      y 2200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re23"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 3330.0
      minerva_y 897.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re23"
      reaction_meta_id "re23"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 537
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 650.0
      y 950.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ikb_underscore_p50_underscore_p65__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa42"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa42"
      minerva_name "IkB_p50_p65"
      minerva_type "Complex"
      minerva_x 3940.0
      minerva_y 2070.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa42"
      species_meta_id "s_id_csa42"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
    ]
  ]
  node [
    id 542
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 250.0
      y 1050.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "eif2ak__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa47"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa47"
      minerva_name "EIF2AK"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link2 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource2 "WIKIPATHWAYS__WP4868"
      minerva_type "Gene"
      minerva_x 1835.5
      minerva_y 2974.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation "Annotation [about=\"#s_id_sa47\", history=]"
      species_id "s_id_sa47"
      species_meta_id "s_id_sa47"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
    ]
  ]
  node [
    id 547
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 900.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ifna1__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa38"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa38"
      minerva_name "IFNA1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IFNA1"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/3439"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_024013"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P01562"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/5417"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000197919"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource2 "HGNC_SYMBOL__IFNA1"
      minerva_ref_type__resource3 "ENTREZ__3439"
      minerva_ref_type__resource4 "REFSEQ__NM_024013"
      minerva_ref_type__resource5 "UNIPROT__P01562"
      minerva_ref_type__resource6 "HGNC__5417"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000197919"
      minerva_type "Gene"
      minerva_x 2240.5
      minerva_y 2900.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation "Annotation [about=\"#s_id_sa38\", history=]"
      species_id "s_id_sa38"
      species_meta_id "s_id_sa38"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
      species_notes "XMLNode 'notes' [childElements size=1]"
    ]
  ]
  node [
    id 552
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 800.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "oas2__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa74"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa74"
      minerva_name "OAS2"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=OAS2"
      minerva_ref_link3 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P29728"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/4939"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/8087"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000111335"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_001032731"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource2 "HGNC_SYMBOL__OAS2"
      minerva_ref_type__resource3 "WIKIPATHWAYS__WP4868"
      minerva_ref_type__resource4 "UNIPROT__P29728"
      minerva_ref_type__resource5 "ENTREZ__4939"
      minerva_ref_type__resource6 "HGNC__8087"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000111335"
      minerva_ref_type__resource8 "REFSEQ__NM_001032731"
      minerva_type "RNA"
      minerva_x 942.5
      minerva_y 2858.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation "Annotation [about=\"#s_id_sa74\", history=]"
      species_id "s_id_sa74"
      species_meta_id "s_id_sa74"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
      species_notes "XMLNode 'notes' [childElements size=1]"
    ]
  ]
  node [
    id 558
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 850.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "oas3__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa78"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa78"
      minerva_name "OAS3"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=OAS3"
      minerva_ref_link3 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/Q9Y6K5"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000111331"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_006187"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/8088"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4940"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource2 "HGNC_SYMBOL__OAS3"
      minerva_ref_type__resource3 "WIKIPATHWAYS__WP4868"
      minerva_ref_type__resource4 "UNIPROT__Q9Y6K5"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000111331"
      minerva_ref_type__resource6 "REFSEQ__NM_006187"
      minerva_ref_type__resource7 "HGNC__8088"
      minerva_ref_type__resource8 "ENTREZ__4940"
      minerva_type "RNA"
      minerva_x 667.5
      minerva_y 2858.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation "Annotation [about=\"#s_id_sa78\", history=]"
      species_id "s_id_sa78"
      species_meta_id "s_id_sa78"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
      species_notes "XMLNode 'notes' [childElements size=1]"
    ]
  ]
  node [
    id 564
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 1050.0
      y 1250.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "isg15__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa298"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa298"
      minerva_former_symbols "G1P2"
      minerva_name "ISG15"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000187608"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_005101"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/9636"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ISG15"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P05161"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/4053"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000187608"
      minerva_ref_type__resource2 "REFSEQ__NM_005101"
      minerva_ref_type__resource3 "ENTREZ__9636"
      minerva_ref_type__resource4 "HGNC_SYMBOL__ISG15"
      minerva_ref_type__resource5 "UNIPROT__P05161"
      minerva_ref_type__resource6 "HGNC__4053"
      minerva_type "Gene"
      minerva_x 2090.0
      minerva_y 2970.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation "Annotation [about=\"#s_id_sa298\", history=]"
      species_id "s_id_sa298"
      species_meta_id "s_id_sa298"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
      species_notes "XMLNode 'notes' [childElements size=1]"
    ]
  ]
  node [
    id 569
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 450.0
      y 1050.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ifnb1__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa34"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa34"
      minerva_former_symbols "IFNB"
      minerva_name "IFNB1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000171855"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IFNB1"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/5434"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P01574"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_002176"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/3456"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000171855"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IFNB1"
      minerva_ref_type__resource4 "HGNC__5434"
      minerva_ref_type__resource5 "UNIPROT__P01574"
      minerva_ref_type__resource6 "REFSEQ__NM_002176"
      minerva_ref_type__resource7 "ENTREZ__3456"
      minerva_type "RNA"
      minerva_x 2817.5
      minerva_y 2752.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation "Annotation [about=\"#s_id_sa34\", history=]"
      species_id "s_id_sa34"
      species_meta_id "s_id_sa34"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
      species_notes "XMLNode 'notes' [childElements size=1]"
    ]
  ]
  node [
    id 575
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 400.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "oas1__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa45"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa45"
      minerva_former_symbols "OIAS"
      minerva_name "OAS1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link2 "http://www.wikipathways.org/instance/WP4868"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P00973"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4938"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_001032409"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=OAS1"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/8086"
      minerva_ref_link8 "https://www.ensembl.org/id/ENSG00000089127"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource2 "WIKIPATHWAYS__WP4868"
      minerva_ref_type__resource3 "UNIPROT__P00973"
      minerva_ref_type__resource4 "ENTREZ__4938"
      minerva_ref_type__resource5 "REFSEQ__NM_001032409"
      minerva_ref_type__resource6 "HGNC_SYMBOL__OAS1"
      minerva_ref_type__resource7 "HGNC__8086"
      minerva_ref_type__resource8 "ENSEMBL__ENSG00000089127"
      minerva_type "RNA"
      minerva_x 1202.5
      minerva_y 2858.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation "Annotation [about=\"#s_id_sa45\", history=]"
      species_id "s_id_sa45"
      species_meta_id "s_id_sa45"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
      species_notes "XMLNode 'notes' [childElements size=1]"
    ]
  ]
  edge [
    id 158
    source 52
    target 157
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa36"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 159
    source 136
    target 157
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa87"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 160
    source 157
    target 119
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa10"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 161
    source 149
    target 157
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR0"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa131"
    ]
  ]
  edge [
    id 162
    source 81
    target 157
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR1"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa8"
    ]
  ]
  edge [
    id 164
    source 35
    target 163
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa300"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 167
    source 42
    target 163
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR3"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa6"
    ]
  ]
  edge [
    id 169
    source 11
    target 168
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa296"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 170
    source 168
    target 62
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa297"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 171
    source 68
    target 168
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR4"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa47"
    ]
  ]
  edge [
    id 173
    source 135
    target 172
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa32"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 174
    source 172
    target 129
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa81"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 176
    source 107
    target 175
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa203"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 177
    source 175
    target 27
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa204"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 178
    source 49
    target 175
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR5"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa139"
    ]
  ]
  edge [
    id 180
    source 20
    target 179
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa186"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 181
    source 179
    target 20
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa186"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 182
    source 108
    target 179
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR6"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa162"
    ]
  ]
  edge [
    id 183
    source 15
    target 179
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR7"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa31"
    ]
  ]
  edge [
    id 184
    source 152
    target 179
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR8"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa27"
    ]
  ]
  edge [
    id 186
    source 70
    target 185
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa49"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 188
    source 42
    target 185
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR9"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa6"
    ]
  ]
  edge [
    id 190
    source 54
    target 185
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR11"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa46"
    ]
  ]
  edge [
    id 192
    source 71
    target 191
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa301"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 193
    source 191
    target 129
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa81"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 196
    source 194
    target 71
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa301"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 198
    source 38
    target 197
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa254"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 199
    source 34
    target 197
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa255"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 200
    source 5
    target 197
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa59"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 201
    source 125
    target 197
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa257"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 202
    source 197
    target 105
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa39"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 203
    source 114
    target 197
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR12"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa38"
    ]
  ]
  edge [
    id 205
    source 148
    target 204
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa200"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 206
    source 204
    target 63
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa155"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 207
    source 122
    target 204
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR13"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa140"
    ]
  ]
  edge [
    id 208
    source 133
    target 204
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR14"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa201"
    ]
  ]
  edge [
    id 209
    source 65
    target 204
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR15"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa202"
    ]
  ]
  edge [
    id 210
    source 18
    target 204
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR16"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa251"
    ]
  ]
  edge [
    id 211
    source 44
    target 204
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR17"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa321"
    ]
  ]
  edge [
    id 213
    source 9
    target 212
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa60"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 214
    source 212
    target 40
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa61"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 215
    source 10
    target 212
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR18"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa116"
    ]
  ]
  edge [
    id 216
    source 130
    target 212
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR19"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa172"
    ]
  ]
  edge [
    id 217
    source 101
    target 212
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR20"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa151"
    ]
  ]
  edge [
    id 218
    source 3
    target 212
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR21"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa309"
    ]
  ]
  edge [
    id 220
    source 61
    target 219
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa291"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 221
    source 36
    target 219
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa314"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 222
    source 97
    target 219
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa240"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 223
    source 219
    target 68
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa47"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 227
    source 116
    target 224
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR22"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa40"
    ]
  ]
  edge [
    id 230
    source 228
    target 126
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa157"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 232
    source 94
    target 231
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa85"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 233
    source 231
    target 153
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa86"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 234
    source 5
    target 231
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR23"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa59"
    ]
  ]
  edge [
    id 237
    source 235
    target 74
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa158"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 239
    source 91
    target 238
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa268"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 240
    source 90
    target 238
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa269"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 241
    source 39
    target 238
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa270"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 244
    source 155
    target 243
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa265"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 245
    source 16
    target 243
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa266"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 246
    source 96
    target 243
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa267"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 247
    source 243
    target 116
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa40"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 248
    source 105
    target 243
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR24"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa39"
    ]
  ]
  edge [
    id 250
    source 88
    target 249
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa31"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 251
    source 249
    target 51
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa30"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 252
    source 149
    target 249
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR25"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa131"
    ]
  ]
  edge [
    id 253
    source 4
    target 249
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR26"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa34"
    ]
  ]
  edge [
    id 254
    source 37
    target 249
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR27"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa237"
    ]
  ]
  edge [
    id 256
    source 73
    target 255
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa4"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 257
    source 255
    target 42
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa6"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 258
    source 60
    target 255
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR28"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa311"
    ]
  ]
  edge [
    id 260
    source 119
    target 259
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa10"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 261
    source 259
    target 128
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa35"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 263
    source 84
    target 262
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa79"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 265
    source 42
    target 262
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR29"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa6"
    ]
  ]
  edge [
    id 267
    source 54
    target 262
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR31"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa46"
    ]
  ]
  edge [
    id 270
    source 268
    target 22
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa21"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 271
    source 268
    target 57
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa278"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 273
    source 40
    target 272
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa61"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 274
    source 10
    target 272
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa116"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 275
    source 272
    target 53
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa19"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 276
    source 82
    target 272
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR32"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa83"
    ]
  ]
  edge [
    id 277
    source 75
    target 272
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR33"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa123"
    ]
  ]
  edge [
    id 278
    source 62
    target 272
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR34"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa297"
    ]
  ]
  edge [
    id 279
    source 44
    target 272
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR35"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa321"
    ]
  ]
  edge [
    id 281
    source 139
    target 280
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa40"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 283
    source 128
    target 280
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR36"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa35"
    ]
  ]
  edge [
    id 285
    source 4
    target 280
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR38"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa34"
    ]
  ]
  edge [
    id 286
    source 123
    target 280
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR39"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa44"
    ]
  ]
  edge [
    id 288
    source 152
    target 287
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa27"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 289
    source 287
    target 47
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa260"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 292
    source 290
    target 46
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa159"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 294
    source 4
    target 293
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa34"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 295
    source 293
    target 138
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa196"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 298
    source 296
    target 99
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa30"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 300
    source 58
    target 299
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa156"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 301
    source 299
    target 101
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa151"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 302
    source 78
    target 299
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR40"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa23"
    ]
  ]
  edge [
    id 303
    source 49
    target 299
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR41"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa139"
    ]
  ]
  edge [
    id 304
    source 27
    target 299
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR42"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa204"
    ]
  ]
  edge [
    id 306
    source 44
    target 305
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa321"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 307
    source 305
    target 129
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa81"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 309
    source 89
    target 308
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa25"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 310
    source 308
    target 93
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa26"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 311
    source 130
    target 308
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR43"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa172"
    ]
  ]
  edge [
    id 312
    source 33
    target 308
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR44"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa184"
    ]
  ]
  edge [
    id 314
    source 156
    target 313
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa305"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 315
    source 313
    target 141
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa68"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 316
    source 86
    target 313
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR45"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa132"
    ]
  ]
  edge [
    id 317
    source 29
    target 313
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR46"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa304"
    ]
  ]
  edge [
    id 318
    source 3
    target 313
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR47"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa309"
    ]
  ]
  edge [
    id 319
    source 23
    target 313
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR48"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa308"
    ]
  ]
  edge [
    id 320
    source 145
    target 313
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR49"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa194"
    ]
  ]
  edge [
    id 321
    source 26
    target 313
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR50"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa134"
    ]
  ]
  edge [
    id 323
    source 127
    target 322
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa239"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 324
    source 97
    target 322
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa240"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 325
    source 322
    target 87
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa37"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 326
    source 131
    target 322
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR51"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa259"
    ]
  ]
  edge [
    id 327
    source 44
    target 322
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR52"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa321"
    ]
  ]
  edge [
    id 329
    source 75
    target 328
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa123"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 330
    source 328
    target 129
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa81"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 332
    source 15
    target 331
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa31"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 333
    source 331
    target 47
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa260"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 335
    source 79
    target 334
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa35"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 337
    source 128
    target 334
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR53"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa35"
    ]
  ]
  edge [
    id 339
    source 4
    target 334
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR55"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa34"
    ]
  ]
  edge [
    id 340
    source 54
    target 334
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR56"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa46"
    ]
  ]
  edge [
    id 341
    source 123
    target 334
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR57"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa44"
    ]
  ]
  edge [
    id 342
    source 44
    target 334
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR58"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa321"
    ]
  ]
  edge [
    id 344
    source 2
    target 343
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa1"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 345
    source 55
    target 343
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa5"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 346
    source 343
    target 113
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa2"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 347
    source 15
    target 343
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR59"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa31"
    ]
  ]
  edge [
    id 349
    source 48
    target 348
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa46"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 351
    source 42
    target 348
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR60"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa6"
    ]
  ]
  edge [
    id 353
    source 54
    target 348
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR62"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa46"
    ]
  ]
  edge [
    id 355
    source 87
    target 354
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa37"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 356
    source 45
    target 354
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa247"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 357
    source 354
    target 114
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa38"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 359
    source 98
    target 358
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa9"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 360
    source 150
    target 358
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa19"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 361
    source 358
    target 140
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa9"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 362
    source 113
    target 358
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR63"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa2"
    ]
  ]
  edge [
    id 363
    source 144
    target 358
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR64"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa3"
    ]
  ]
  edge [
    id 365
    source 72
    target 364
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa171"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 366
    source 364
    target 130
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa172"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 367
    source 101
    target 364
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR65"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa151"
    ]
  ]
  edge [
    id 368
    source 33
    target 364
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR66"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa184"
    ]
  ]
  edge [
    id 370
    source 6
    target 369
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa306"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 371
    source 369
    target 121
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa69"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 372
    source 77
    target 369
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR67"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa195"
    ]
  ]
  edge [
    id 373
    source 23
    target 369
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR68"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa308"
    ]
  ]
  edge [
    id 374
    source 29
    target 369
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR69"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa304"
    ]
  ]
  edge [
    id 375
    source 3
    target 369
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR70"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa309"
    ]
  ]
  edge [
    id 377
    source 83
    target 376
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa213"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 378
    source 376
    target 56
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa281"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 379
    source 102
    target 376
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR71"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa29"
    ]
  ]
  edge [
    id 381
    source 71
    target 380
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa301"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 382
    source 75
    target 380
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa123"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 383
    source 380
    target 21
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa48"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 384
    source 31
    target 380
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR72"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa168"
    ]
  ]
  edge [
    id 386
    source 80
    target 385
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa120"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 387
    source 385
    target 7
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa286"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 388
    source 75
    target 385
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR73"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa123"
    ]
  ]
  edge [
    id 389
    source 122
    target 385
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR74"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa140"
    ]
  ]
  edge [
    id 390
    source 132
    target 385
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR75"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa27"
    ]
  ]
  edge [
    id 391
    source 60
    target 385
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR76"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa311"
    ]
  ]
  edge [
    id 392
    source 33
    target 385
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR77"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa184"
    ]
  ]
  edge [
    id 393
    source 101
    target 385
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR78"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa151"
    ]
  ]
  edge [
    id 394
    source 102
    target 385
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR79"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa29"
    ]
  ]
  edge [
    id 395
    source 29
    target 385
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR80"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa304"
    ]
  ]
  edge [
    id 396
    source 13
    target 385
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR81"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa227"
    ]
  ]
  edge [
    id 398
    source 137
    target 397
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa50"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 399
    source 106
    target 397
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa80"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 400
    source 1
    target 397
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa51"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 401
    source 24
    target 397
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa76"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 402
    source 397
    target 135
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa32"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 405
    source 403
    target 28
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa26"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 407
    source 43
    target 406
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa45"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 408
    source 406
    target 54
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa46"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 409
    source 60
    target 406
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR82"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa311"
    ]
  ]
  edge [
    id 411
    source 93
    target 410
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa26"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 412
    source 41
    target 410
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa209"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 413
    source 410
    target 102
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa29"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 414
    source 87
    target 410
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR83"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa37"
    ]
  ]
  edge [
    id 415
    source 26
    target 410
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR84"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa134"
    ]
  ]
  edge [
    id 416
    source 101
    target 410
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR85"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa151"
    ]
  ]
  edge [
    id 418
    source 2
    target 417
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa1"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 419
    source 117
    target 417
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa3"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 420
    source 417
    target 144
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa3"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 421
    source 152
    target 417
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR86"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa27"
    ]
  ]
  edge [
    id 423
    source 7
    target 422
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa286"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 425
    source 422
    target 43
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa45"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 427
    source 120
    target 426
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa57"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 428
    source 36
    target 426
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa314"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 429
    source 426
    target 142
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa20"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 430
    source 8
    target 426
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR87"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa166"
    ]
  ]
  edge [
    id 431
    source 95
    target 426
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR88"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa55"
    ]
  ]
  edge [
    id 432
    source 67
    target 426
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR89"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa238"
    ]
  ]
  edge [
    id 433
    source 131
    target 426
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR90"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa259"
    ]
  ]
  edge [
    id 435
    source 53
    target 434
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa19"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 436
    source 82
    target 434
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa83"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 437
    source 434
    target 89
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa25"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 438
    source 33
    target 434
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR91"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa184"
    ]
  ]
  edge [
    id 440
    source 46
    target 439
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa159"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 441
    source 439
    target 137
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa50"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 442
    source 13
    target 439
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR92"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa227"
    ]
  ]
  edge [
    id 444
    source 63
    target 443
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa155"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 445
    source 12
    target 443
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa154"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 446
    source 443
    target 78
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa23"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 447
    source 20
    target 443
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR93"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa186"
    ]
  ]
  edge [
    id 448
    source 122
    target 443
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR94"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa140"
    ]
  ]
  edge [
    id 449
    source 13
    target 443
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR95"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa227"
    ]
  ]
  edge [
    id 450
    source 26
    target 443
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR96"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa134"
    ]
  ]
  edge [
    id 452
    source 104
    target 443
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR98"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa317"
    ]
  ]
  edge [
    id 453
    source 143
    target 443
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR99"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa318"
    ]
  ]
  edge [
    id 454
    source 92
    target 443
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR100"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa319"
    ]
  ]
  edge [
    id 456
    source 25
    target 455
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa169"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 457
    source 455
    target 75
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa123"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 459
    source 99
    target 458
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa30"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 460
    source 458
    target 15
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa31"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 463
    source 461
    target 1
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa51"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 465
    source 124
    target 464
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa75"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 467
    source 42
    target 464
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR101"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa6"
    ]
  ]
  edge [
    id 469
    source 54
    target 464
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR103"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa46"
    ]
  ]
  edge [
    id 471
    source 126
    target 470
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa157"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 472
    source 470
    target 106
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa80"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 473
    source 13
    target 470
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR104"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa227"
    ]
  ]
  edge [
    id 475
    source 74
    target 474
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa158"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 476
    source 474
    target 24
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa76"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 477
    source 13
    target 474
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR105"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa227"
    ]
  ]
  edge [
    id 479
    source 50
    target 478
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa82"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 480
    source 478
    target 82
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa83"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 481
    source 142
    target 478
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR106"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa20"
    ]
  ]
  edge [
    id 482
    source 75
    target 478
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR107"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa123"
    ]
  ]
  edge [
    id 484
    source 103
    target 483
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa43"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 485
    source 483
    target 123
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa44"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 487
    source 17
    target 486
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa160"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 488
    source 486
    target 1
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa51"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 489
    source 13
    target 486
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR108"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa227"
    ]
  ]
  edge [
    id 491
    source 4
    target 490
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa34"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 492
    source 490
    target 129
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa81"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 494
    source 22
    target 493
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa21"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 495
    source 493
    target 4
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa34"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 497
    source 28
    target 496
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa26"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 498
    source 496
    target 152
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa27"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 500
    source 110
    target 499
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa119"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 501
    source 499
    target 10
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa116"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 502
    source 75
    target 499
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR109"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa123"
    ]
  ]
  edge [
    id 503
    source 40
    target 499
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR110"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa61"
    ]
  ]
  edge [
    id 505
    source 51
    target 504
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_csa30"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 506
    source 504
    target 138
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa196"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 508
    source 66
    target 507
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa64"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 509
    source 32
    target 507
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa63"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 510
    source 507
    target 81
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa8"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 511
    source 40
    target 507
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR111"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa61"
    ]
  ]
  edge [
    id 512
    source 153
    target 507
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR112"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa86"
    ]
  ]
  edge [
    id 514
    source 111
    target 513
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa198"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 515
    source 513
    target 12
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa154"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 516
    source 13
    target 513
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR113"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa227"
    ]
  ]
  edge [
    id 517
    source 44
    target 513
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR114"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa321"
    ]
  ]
  edge [
    id 519
    source 56
    target 518
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa281"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 521
    source 518
    target 103
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa43"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 523
    source 141
    target 522
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa68"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 524
    source 30
    target 522
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa70"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 525
    source 121
    target 522
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa69"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 526
    source 522
    target 73
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_csa4"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 527
    source 60
    target 522
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR115"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa311"
    ]
  ]
  edge [
    id 528
    source 8
    target 522
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR116"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa166"
    ]
  ]
  edge [
    id 529
    source 140
    target 522
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR117"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa9"
    ]
  ]
  edge [
    id 530
    source 18
    target 522
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR118"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa251"
    ]
  ]
  edge [
    id 532
    source 19
    target 531
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation ""
      sbmlRole "reactant"
      species "s_id_sa58"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 533
    source 531
    target 5
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation ""
      sbmlRole "product"
      species "s_id_sa59"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 534
    source 142
    target 531
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR119"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_csa20"
    ]
  ]
  edge [
    id 535
    source 75
    target 531
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR120"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa123"
    ]
  ]
  edge [
    id 536
    source 27
    target 531
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR121"
      modifier_non_rdf_annotation ""
      sbmlRole "modifier"
      species "s_id_sa204"
    ]
  ]
  edge [
    id 538
    source 224
    target 537
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa42"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 539
    source 537
    target 268
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa42"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 540
    source 238
    target 537
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa41"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 541
    source 537
    target 224
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa41"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 543
    source 542
    target 185
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR10"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa47"
    ]
  ]
  edge [
    id 546
    source 542
    target 461
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa48"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 548
    source 547
    target 280
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR37"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa38"
    ]
  ]
  edge [
    id 551
    source 547
    target 296
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa39"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 553
    source 464
    target 552
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa74"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 554
    source 552
    target 235
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa74"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 555
    source 552
    target 464
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR102"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa73"
    ]
  ]
  edge [
    id 559
    source 262
    target 558
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa78"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 560
    source 558
    target 228
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa78"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 561
    source 558
    target 262
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR30"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa77"
    ]
  ]
  edge [
    id 565
    source 564
    target 163
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR2"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa298"
    ]
  ]
  edge [
    id 568
    source 564
    target 194
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa299"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 570
    source 334
    target 569
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa34"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 571
    source 569
    target 403
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa34"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 572
    source 569
    target 334
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR54"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa33"
    ]
  ]
  edge [
    id 576
    source 348
    target 575
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa45"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 577
    source 575
    target 290
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa45"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 578
    source 575
    target 348
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR61"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa44"
    ]
  ]
]
