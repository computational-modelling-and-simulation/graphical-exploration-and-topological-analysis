# generated with VANTED V2.8.0 at Tue Apr 27 20:00:46 CEST 2021
graph [
  background_coloring "true"
  cluster_colors "255,128,128,255:0,0,0,255;255,213,128,255:0,0,0,255;213,255,128,255:0,0,0,255;128,255,128,255:0,0,0,255;128,255,213,255:0,0,0,255;128,212,255,255:0,0,0,255;128,128,255,255:0,0,0,255;213,128,255,255:0,0,0,255;255,128,212,255:0,0,0,255"
  clusterbackground_fill_outer_region "false"
  clusterbackground_grid 50.0
  clusterbackground_low_alpha 0.2
  clusterbackground_radius 200.0
  clusterbackground_space_fill "true"
  graphbackgroundcolor "#ffffff"
  sbml [
    model_annotation ""
    model_meta_id "Nsp4_Nsp6"
    model_non_rdf_annotation ""
    model_notes ""
    model_sbml_id "Nsp4_Nsp6"
    namespace "xmlns:celldesigner=\"http://www.sbml.org/2001/ns/celldesigner\" "
    sbml_level "2"
    sbml_version "4"
  ]
  sbml_compartment_default [
    sbml_compartment_default_id "default"
    sbml_compartment_default_meta_id "default"
    sbml_compartment_default_non_rdf_annotation ""
    sbml_compartment_default_notes ""
    sbml_compartment_default_size "1.0"
    sbml_compartment_default_units "volume"
  ]
  sbml_compartment_s_id_ca10 [
    sbml_compartment_s_id_ca10_id "s_id_ca10"
    sbml_compartment_s_id_ca10_meta_id "s_id_ca10"
    sbml_compartment_s_id_ca10_name "human_space_host"
    sbml_compartment_s_id_ca10_non_rdf_annotation ""
    sbml_compartment_s_id_ca10_notes ""
    sbml_compartment_s_id_ca10_outside "default"
    sbml_compartment_s_id_ca10_size "1.0"
    sbml_compartment_s_id_ca10_units "volume"
  ]
  sbml_compartment_s_id_ca2 [
    sbml_compartment_s_id_ca2_annotation ""
    sbml_compartment_s_id_ca2_id "s_id_ca2"
    sbml_compartment_s_id_ca2_meta_id "s_id_ca2"
    sbml_compartment_s_id_ca2_name "endoplasmic_space_reticulum"
    sbml_compartment_s_id_ca2_non_rdf_annotation ""
    sbml_compartment_s_id_ca2_notes ""
    sbml_compartment_s_id_ca2_outside "s_id_ca9"
    sbml_compartment_s_id_ca2_size "1.0"
    sbml_compartment_s_id_ca2_units "volume"
  ]
  sbml_compartment_s_id_ca3 [
    sbml_compartment_s_id_ca3_annotation ""
    sbml_compartment_s_id_ca3_id "s_id_ca3"
    sbml_compartment_s_id_ca3_meta_id "s_id_ca3"
    sbml_compartment_s_id_ca3_name "mitochondrial_space_matrix"
    sbml_compartment_s_id_ca3_non_rdf_annotation ""
    sbml_compartment_s_id_ca3_notes ""
    sbml_compartment_s_id_ca3_outside "s_id_ca4"
    sbml_compartment_s_id_ca3_size "1.0"
    sbml_compartment_s_id_ca3_units "volume"
  ]
  sbml_compartment_s_id_ca4 [
    sbml_compartment_s_id_ca4_id "s_id_ca4"
    sbml_compartment_s_id_ca4_meta_id "s_id_ca4"
    sbml_compartment_s_id_ca4_name "mitochondrion"
    sbml_compartment_s_id_ca4_non_rdf_annotation ""
    sbml_compartment_s_id_ca4_notes ""
    sbml_compartment_s_id_ca4_outside "s_id_ca9"
    sbml_compartment_s_id_ca4_size "1.0"
    sbml_compartment_s_id_ca4_units "volume"
  ]
  sbml_compartment_s_id_ca5 [
    sbml_compartment_s_id_ca5_annotation ""
    sbml_compartment_s_id_ca5_id "s_id_ca5"
    sbml_compartment_s_id_ca5_meta_id "s_id_ca5"
    sbml_compartment_s_id_ca5_name "nucleus"
    sbml_compartment_s_id_ca5_non_rdf_annotation ""
    sbml_compartment_s_id_ca5_notes ""
    sbml_compartment_s_id_ca5_outside "s_id_ca9"
    sbml_compartment_s_id_ca5_size "1.0"
    sbml_compartment_s_id_ca5_units "volume"
  ]
  sbml_compartment_s_id_ca7 [
    sbml_compartment_s_id_ca7_annotation ""
    sbml_compartment_s_id_ca7_id "s_id_ca7"
    sbml_compartment_s_id_ca7_meta_id "s_id_ca7"
    sbml_compartment_s_id_ca7_name "endosome"
    sbml_compartment_s_id_ca7_non_rdf_annotation ""
    sbml_compartment_s_id_ca7_notes ""
    sbml_compartment_s_id_ca7_outside "s_id_ca9"
    sbml_compartment_s_id_ca7_size "1.0"
    sbml_compartment_s_id_ca7_units "volume"
  ]
  sbml_compartment_s_id_ca8 [
    sbml_compartment_s_id_ca8_annotation ""
    sbml_compartment_s_id_ca8_id "s_id_ca8"
    sbml_compartment_s_id_ca8_meta_id "s_id_ca8"
    sbml_compartment_s_id_ca8_name "double_space_membrane_space_vesicle_space_viral_space_factory"
    sbml_compartment_s_id_ca8_non_rdf_annotation ""
    sbml_compartment_s_id_ca8_notes ""
    sbml_compartment_s_id_ca8_outside "s_id_ca9"
    sbml_compartment_s_id_ca8_size "1.0"
    sbml_compartment_s_id_ca8_units "volume"
  ]
  sbml_compartment_s_id_ca9 [
    sbml_compartment_s_id_ca9_annotation ""
    sbml_compartment_s_id_ca9_id "s_id_ca9"
    sbml_compartment_s_id_ca9_meta_id "s_id_ca9"
    sbml_compartment_s_id_ca9_name "cell"
    sbml_compartment_s_id_ca9_non_rdf_annotation ""
    sbml_compartment_s_id_ca9_notes ""
    sbml_compartment_s_id_ca9_outside "s_id_ca10"
    sbml_compartment_s_id_ca9_size "1.0"
    sbml_compartment_s_id_ca9_units "volume"
  ]
  sbml_unit_definition_1 [
    sbml_unit_definition_1_id "length"
    sbml_unit_definition_1_name "length"
    sbml_unit_definition_1_sub_unit_1_ "(1.0 * 10^0 * metre)^1.0"
    sbml_unit_definition_1unit "(1.0 * 10^0 * metre)^1.0"
  ]
  sbml_unit_definition_2 [
    sbml_unit_definition_2_id "area"
    sbml_unit_definition_2_name "area"
    sbml_unit_definition_2_sub_unit_1_ "(1.0 * 10^0 * metre)^2.0"
    sbml_unit_definition_2unit "(1.0 * 10^0 * metre)^2.0"
  ]
  sbml_unit_definition_3 [
    sbml_unit_definition_3_id "substance"
    sbml_unit_definition_3_name "substance"
    sbml_unit_definition_3_sub_unit_1_ "(1.0 * 10^0 * mole)^1.0"
    sbml_unit_definition_3unit "(1.0 * 10^0 * mole)^1.0"
  ]
  sbml_unit_definition_4 [
    sbml_unit_definition_4_id "time"
    sbml_unit_definition_4_name "time"
    sbml_unit_definition_4_sub_unit_1_ "(1.0 * 10^0 * second)^1.0"
    sbml_unit_definition_4unit "(1.0 * 10^0 * second)^1.0"
  ]
  sbml_unit_definition_5 [
    sbml_unit_definition_5_id "volume"
    sbml_unit_definition_5_name "volume"
    sbml_unit_definition_5_sub_unit_1_ "(1.0 * 10^0 * litre)^1.0"
    sbml_unit_definition_5unit "(1.0 * 10^0 * litre)^1.0"
  ]
  directed 0
  node [
    id 1
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 200.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp3_space_(_plus_)__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa160"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa160"
      minerva_name "Nsp3 (+)"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/32353859"
      minerva_ref_type__resource1 "PUBMED__32353859"
      minerva_type "RNA"
      minerva_x 1113.75
      minerva_y 381.83333333333326
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa160"
      species_meta_id "s_id_sa160"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 2
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 300.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sa3_underscore_degraded__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa16"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa16"
      minerva_elementId2 "sa58"
      minerva_name "sa3_degraded"
      minerva_type "Degraded"
      minerva_x 661.0
      minerva_x2 1347.166666666667
      minerva_y 954.5
      minerva_y2 641.6666666666667
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa16"
      species_meta_id "s_id_sa16"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 3
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 400.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp4_space_(_minus_)__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa220"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa220"
      minerva_name "Nsp4 (-)"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/32353859"
      minerva_ref_type__resource1 "PUBMED__32353859"
      minerva_type "RNA"
      minerva_x 746.0
      minerva_y 931.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa220"
      species_meta_id "s_id_sa220"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 4
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 500.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp4_underscore_alg11__endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa15"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa93"
      minerva_elementId2 "csa15"
      minerva_name "Nsp4_ALG11"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/20080937"
      minerva_ref_type__resource1 "PUBMED__20080937"
      minerva_type "Complex"
      minerva_x 164.5
      minerva_x2 160.0
      minerva_y 328.5
      minerva_y2 483.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa15"
      species_meta_id "s_id_csa15"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 5
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 600.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sa5_underscore_degraded__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa49"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa5"
      minerva_elementId2 "sa49"
      minerva_name "sa5_degraded"
      minerva_type "Degraded"
      minerva_x 655.0
      minerva_x2 1643.166666666667
      minerva_y 799.0
      minerva_y2 539.1666666666667
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa49"
      species_meta_id "s_id_sa49"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 6
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 700.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "p1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa84"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa84"
      minerva_name "P1"
      minerva_type "Complex"
      minerva_x 3011.5
      minerva_y 419.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa84"
      species_meta_id "s_id_csa84"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 7
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 800.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "selinexor__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa187"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa187"
      minerva_name "Selinexor"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/32353859"
      minerva_ref_type__resource1 "PUBMED__32353859"
      minerva_type "Drug"
      minerva_x 552.0
      minerva_y 740.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa187"
      species_meta_id "s_id_sa187"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 8
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 900.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "h_plus___cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa234"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa234"
      minerva_fullName "hydron"
      minerva_name "H+"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15378"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15378"
      minerva_synonyms "H(+); H+; Hydron"
      minerva_type "Ion"
      minerva_x 535.0
      minerva_y 1529.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa234"
      species_meta_id "s_id_sa234"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 9
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 1000.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "slc6a15:nsp6__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa45"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa45"
      minerva_name "SLC6A15:Nsp6"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/32353859"
      minerva_ref_type__resource1 "PUBMED__32353859"
      minerva_type "Complex"
      minerva_x 2304.0
      minerva_y 489.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa45"
      species_meta_id "s_id_csa45"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 10
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1100.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "v_minus_type_space_proton_space_atpase__endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa94"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa94"
      minerva_fullName "lysosomal proton-transporting V-type ATPase complex"
      minerva_name "V-type proton ATPase"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0046611"
      minerva_ref_type__resource1 "GO__GO:0046611"
      minerva_type "Complex"
      minerva_x 377.0
      minerva_y 1386.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa94"
      species_meta_id "s_id_csa94"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 11
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 1200.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ide__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa225"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa225"
      minerva_elementId2 "sa8"
      minerva_name "IDE"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/P14735"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000119912"
      minerva_ref_link11 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IDE"
      minerva_ref_link12 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IDE"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/3416"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/3416"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/pubmed/32353859"
      minerva_ref_link5 "https://doi.org/10.1210/mend-4-8-1125"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=10116"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/5381"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_004969"
      minerva_ref_link9 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.24.56"
      minerva_ref_type__resource1 "UNIPROT__P14735"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000119912"
      minerva_ref_type__resource11 "HGNC_SYMBOL__IDE"
      minerva_ref_type__resource12 "HGNC_SYMBOL__IDE"
      minerva_ref_type__resource2 "ENTREZ__3416"
      minerva_ref_type__resource3 "ENTREZ__3416"
      minerva_ref_type__resource4 "PUBMED__32353859"
      minerva_ref_type__resource5 "DOI__10.1210/mend-4-8-1125"
      minerva_ref_type__resource6 "TAXONOMY__10116"
      minerva_ref_type__resource7 "HGNC__5381"
      minerva_ref_type__resource8 "REFSEQ__NM_004969"
      minerva_ref_type__resource9 "EC__3.4.24.56"
      minerva_type "Protein"
      minerva_x 759.5
      minerva_x2 702.0
      minerva_y 251.5
      minerva_y2 683.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa225"
      species_meta_id "s_id_sa225"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 12
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 200.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp3_space_(_minus_)__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa219"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa219"
      minerva_name "Nsp3 (-)"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/32353859"
      minerva_ref_type__resource1 "PUBMED__32353859"
      minerva_type "RNA"
      minerva_x 992.0
      minerva_y 383.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa219"
      species_meta_id "s_id_sa219"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 14
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 400.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp6_space_(_minus_)__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa218"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa218"
      minerva_name "Nsp6 (-)"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/32353859"
      minerva_ref_type__resource1 "PUBMED__32353859"
      minerva_type "RNA"
      minerva_x 1403.0
      minerva_y 587.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa218"
      species_meta_id "s_id_sa218"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 15
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 500.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tim_space_complex__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa29"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa29"
      minerva_name "TIM complex"
      minerva_type "Complex"
      minerva_x 1319.1875
      minerva_y 1301.125
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa29"
      species_meta_id "s_id_csa29"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 16
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 600.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "slc6a15:loratadine__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa49"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa49"
      minerva_name "SLC6A15:Loratadine"
      minerva_type "Complex"
      minerva_x 2135.0
      minerva_y 741.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa49"
      species_meta_id "s_id_csa49"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 17
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 700.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "p_minus_atpase:nsp6__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa40"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa40"
      minerva_name "P-ATPase:Nsp6"
      minerva_type "Complex"
      minerva_x 2318.916666666667
      minerva_y 205.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa40"
      species_meta_id "s_id_csa40"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 19
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 900.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "p0__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa83"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa83"
      minerva_name "P0"
      minerva_type "Complex"
      minerva_x 2958.0
      minerva_y 207.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa83"
      species_meta_id "s_id_csa83"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 20
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 1000.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp6__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa48"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa48"
      minerva_name "Nsp6"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/32353859"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=2697049"
      minerva_ref_type__resource1 "PUBMED__32353859"
      minerva_ref_type__resource2 "TAXONOMY__2697049"
      minerva_type "Protein"
      minerva_x 1553.0
      minerva_y 444.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa48"
      species_meta_id "s_id_sa48"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 21
    zlevel -1

    cluster [
      cluster "s_id_ca8"
    ]
    graphics [
      x 1100.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "plasma_space_membrane_space_organization__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa164"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa164"
      minerva_fullName "plasma membrane organization"
      minerva_name "Plasma membrane organization"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31226023"
      minerva_ref_link2 "http://amigo.geneontology.org/amigo/term/GO:0007009"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=694009"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/pubmed/23943763"
      minerva_ref_type__resource1 "PUBMED__31226023"
      minerva_ref_type__resource2 "GO__GO:0007009"
      minerva_ref_type__resource3 "TAXONOMY__694009"
      minerva_ref_type__resource4 "PUBMED__23943763"
      minerva_type "Phenotype"
      minerva_x 872.75
      minerva_y 449.33333333333326
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca8"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa164"
      species_meta_id "s_id_sa164"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 22
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 1200.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp6:sigmar1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa92"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa92"
      minerva_name "Nsp6:SIGMAR1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/10406945"
      minerva_ref_type__resource1 "PUBMED__10406945"
      minerva_type "Complex"
      minerva_x 94.5
      minerva_y 1760.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa92"
      species_meta_id "s_id_csa92"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 23
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 200.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp6:sigmar1__endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa18"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa18"
      minerva_name "Nsp6:SIGMAR1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/32353859"
      minerva_ref_type__resource1 "PUBMED__32353859"
      minerva_type "Complex"
      minerva_x 162.66666666666674
      minerva_y 1187.1666666666667
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa18"
      species_meta_id "s_id_csa18"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 24
    zlevel -1

    cluster [
      cluster "s_id_ca7"
    ]
    graphics [
      x 300.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "v_minus_atpase:nsp6__endosome__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa34"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa34"
      minerva_name "V-ATPase:Nsp6"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/Q15904"
      minerva_ref_type__resource1 "UNIPROT__Q15904"
      minerva_type "Complex"
      minerva_x 762.25
      minerva_y 1086.75
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca7"
      compartment_name "endosome"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa34"
      species_meta_id "s_id_csa34"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 25
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 400.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "atp6ap1:bafilomycin_space_a1__endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa50"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa50"
      minerva_name "ATP6AP1:Bafilomycin A1"
      minerva_type "Complex"
      minerva_x 279.0
      minerva_y 1580.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa50"
      species_meta_id "s_id_csa50"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 26
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 500.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sa171_underscore_degraded__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa163"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa163"
      minerva_name "sa171_degraded"
      minerva_type "Degraded"
      minerva_x 1219.75
      minerva_y 401.83333333333326
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa163"
      species_meta_id "s_id_sa163"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 27
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 600.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp6_space_(_plus_)__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa47"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa47"
      minerva_name "Nsp6 (+)"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/32353859"
      minerva_ref_type__resource1 "PUBMED__32353859"
      minerva_type "RNA"
      minerva_x 1536.166666666667
      minerva_y 587.6666666666667
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa47"
      species_meta_id "s_id_sa47"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 28
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 700.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "slc6a15:m__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa48"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa48"
      minerva_name "SLC6A15:M"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/32353859"
      minerva_ref_type__resource1 "PUBMED__32353859"
      minerva_type "Complex"
      minerva_x 2531.0
      minerva_y 687.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa48"
      species_meta_id "s_id_csa48"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 29
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 800.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sigmar1:drugs__endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa53"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa53"
      minerva_name "SIGMAR1:Drugs"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/32353859"
      minerva_ref_type__resource1 "PUBMED__32353859"
      minerva_type "Complex"
      minerva_x 155.0
      minerva_y 922.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa53"
      species_meta_id "s_id_csa53"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 30
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 900.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nup210__endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa9"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa9"
      minerva_name "NUP210"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NUP210"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NUP210"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/pubmed/14517331"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/Q8TEM1"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/23225"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000132182"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/23225"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/30052"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_024923"
      minerva_ref_type__resource1 "HGNC_SYMBOL__NUP210"
      minerva_ref_type__resource2 "HGNC_SYMBOL__NUP210"
      minerva_ref_type__resource3 "PUBMED__14517331"
      minerva_ref_type__resource4 "UNIPROT__Q8TEM1"
      minerva_ref_type__resource5 "ENTREZ__23225"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000132182"
      minerva_ref_type__resource7 "ENTREZ__23225"
      minerva_ref_type__resource8 "HGNC__30052"
      minerva_ref_type__resource9 "REFSEQ__NM_024923"
      minerva_type "Protein"
      minerva_x 254.5
      minerva_y 852.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa9"
      species_meta_id "s_id_sa9"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 31
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 1000.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp4_space_(_plus_)__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa3"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa3"
      minerva_name "Nsp4 (+)"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/32353859"
      minerva_ref_type__resource1 "PUBMED__32353859"
      minerva_type "RNA"
      minerva_x 757.0
      minerva_y 842.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa3"
      species_meta_id "s_id_sa3"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 32
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 1100.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sigmar1__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa226"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa226"
      minerva_former_symbols "OPRS1"
      minerva_name "SIGMAR1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/10406945"
      minerva_ref_link10 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/8157"
      minerva_ref_link11 "https://purl.uniprot.org/uniprot/Q99720"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_005866"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SIGMAR1"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000147955"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SIGMAR1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/pubmed/32353859"
      minerva_ref_link7 "https://doi.org/10.1074/jbc.272.43.27107"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/10280"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/10280"
      minerva_ref_type__resource1 "PUBMED__10406945"
      minerva_ref_type__resource10 "HGNC__8157"
      minerva_ref_type__resource11 "UNIPROT__Q99720"
      minerva_ref_type__resource2 "REFSEQ__NM_005866"
      minerva_ref_type__resource3 "HGNC_SYMBOL__SIGMAR1"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000147955"
      minerva_ref_type__resource5 "HGNC_SYMBOL__SIGMAR1"
      minerva_ref_type__resource6 "PUBMED__32353859"
      minerva_ref_type__resource7 "DOI__10.1074/jbc.272.43.27107"
      minerva_ref_type__resource8 "ENTREZ__10280"
      minerva_ref_type__resource9 "ENTREZ__10280"
      minerva_type "Protein"
      minerva_x 408.5
      minerva_y 1867.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa226"
      species_meta_id "s_id_sa226"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 33
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 1200.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "m__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa193"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa193"
      minerva_name "M"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=2697049"
      minerva_ref_type__resource1 "TAXONOMY__2697049"
      minerva_ref_type__resource2 "UNIPROT__M"
      minerva_type "Protein"
      minerva_x 2274.0
      minerva_y 773.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa193"
      species_meta_id "s_id_sa193"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 34
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 200.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp4:ide__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa4"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa2"
      minerva_elementId2 "csa4"
      minerva_name "Nsp4:IDE"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/32353859"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/pubmed/9830016"
      minerva_ref_type__resource1 "PUBMED__32353859"
      minerva_ref_type__resource2 "PUBMED__9830016"
      minerva_type "Complex"
      minerva_x 642.5
      minerva_x2 630.5
      minerva_y 540.0
      minerva_y2 266.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa4"
      species_meta_id "s_id_csa4"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 35
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 300.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp4_underscore_ide__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa5"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa5"
      minerva_name "Nsp4_IDE"
      minerva_type "Complex"
      minerva_x 399.5
      minerva_y 127.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa5"
      species_meta_id "s_id_csa5"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 36
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 400.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "f0__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa100"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa100"
      minerva_name "F0"
      minerva_type "Complex"
      minerva_x 1516.8970588235293
      minerva_y 1102.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa100"
      species_meta_id "s_id_csa100"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 37
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 500.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "slc6a15__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa191"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa191"
      minerva_name "SLC6A15"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/Q8IY34"
      minerva_ref_link10 "https://purl.uniprot.org/uniprot/Q9H2J7"
      minerva_ref_link11 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SLC6A15"
      minerva_ref_link12 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SLC6A15"
      minerva_ref_link13 "https://www.ncbi.nlm.nih.gov/protein/NM_018057"
      minerva_ref_link14 "https://www.ensembl.org/id/ENSG00000072041"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/pubmed/32353859"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/55117"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/55117"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SLC15A3"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/13621"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_182767"
      minerva_ref_link8 "https://purl.uniprot.org/uniprot/Q9H2J7"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/51296"
      minerva_ref_type__resource1 "UNIPROT__Q8IY34"
      minerva_ref_type__resource10 "UNIPROT__Q9H2J7"
      minerva_ref_type__resource11 "HGNC_SYMBOL__SLC6A15"
      minerva_ref_type__resource12 "HGNC_SYMBOL__SLC6A15"
      minerva_ref_type__resource13 "REFSEQ__NM_018057"
      minerva_ref_type__resource14 "ENSEMBL__ENSG00000072041"
      minerva_ref_type__resource2 "PUBMED__32353859"
      minerva_ref_type__resource3 "ENTREZ__55117"
      minerva_ref_type__resource4 "ENTREZ__55117"
      minerva_ref_type__resource5 "HGNC_SYMBOL__SLC15A3"
      minerva_ref_type__resource6 "HGNC__13621"
      minerva_ref_type__resource7 "REFSEQ__NM_182767"
      minerva_ref_type__resource8 "UNIPROT__Q9H2J7"
      minerva_ref_type__resource9 "ENTREZ__51296"
      minerva_type "Protein"
      minerva_x 2135.0
      minerva_y 556.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa191"
      species_meta_id "s_id_sa191"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 38
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 600.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sigmar1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa227"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa227"
      minerva_former_symbols "OPRS1"
      minerva_name "SIGMAR1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/10406945"
      minerva_ref_link10 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/8157"
      minerva_ref_link11 "https://purl.uniprot.org/uniprot/Q99720"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_005866"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SIGMAR1"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000147955"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SIGMAR1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/pubmed/32353859"
      minerva_ref_link7 "https://doi.org/10.1074/jbc.272.43.27107"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/10280"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/10280"
      minerva_ref_type__resource1 "PUBMED__10406945"
      minerva_ref_type__resource10 "HGNC__8157"
      minerva_ref_type__resource11 "UNIPROT__Q99720"
      minerva_ref_type__resource2 "REFSEQ__NM_005866"
      minerva_ref_type__resource3 "HGNC_SYMBOL__SIGMAR1"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000147955"
      minerva_ref_type__resource5 "HGNC_SYMBOL__SIGMAR1"
      minerva_ref_type__resource6 "PUBMED__32353859"
      minerva_ref_type__resource7 "DOI__10.1074/jbc.272.43.27107"
      minerva_ref_type__resource8 "ENTREZ__10280"
      minerva_ref_type__resource9 "ENTREZ__10280"
      minerva_type "Protein"
      minerva_x 88.5
      minerva_y 1643.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa227"
      species_meta_id "s_id_sa227"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 39
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 700.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sa169_underscore_degraded__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa159"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa159"
      minerva_name "sa169_degraded"
      minerva_type "Degraded"
      minerva_x 1022.75
      minerva_y 325.83333333333326
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa159"
      species_meta_id "s_id_sa159"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 40
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 800.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp4__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa4"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa4"
      minerva_name "Nsp4"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=2697049"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/BCD58761"
      minerva_ref_type__resource1 "TAXONOMY__2697049"
      minerva_ref_type__resource2 "NCBI_PROTEIN__BCD58761"
      minerva_type "Protein"
      minerva_x 754.0
      minerva_y 774.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa4"
      species_meta_id "s_id_sa4"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 41
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 900.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "loratadine__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa200"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa200"
      minerva_name "Loratadine"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/123134323"
      minerva_ref_type__resource1 "PUBMED__123134323"
      minerva_type "Drug"
      minerva_x 2162.0
      minerva_y 653.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa200"
      species_meta_id "s_id_sa200"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 42
    zlevel -1

    cluster [
      cluster "s_id_ca8"
    ]
    graphics [
      x 1000.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp3__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa162"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa162"
      minerva_name "Nsp3"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/32353859"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=2697049"
      minerva_ref_type__resource1 "PUBMED__32353859"
      minerva_ref_type__resource2 "TAXONOMY__2697049"
      minerva_ref_type__resource3 "UNIPROT__Nsp3"
      minerva_type "Protein"
      minerva_x 1175.75
      minerva_y 473.83333333333326
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca8"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa162"
      species_meta_id "s_id_sa162"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 43
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 1100.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "f1__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa101"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa101"
      minerva_name "F1"
      minerva_type "Complex"
      minerva_x 1516.8970588235293
      minerva_y 986.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa101"
      species_meta_id "s_id_csa101"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 44
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 1200.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sa2_underscore_degraded__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa50"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa6"
      minerva_elementId2 "sa50"
      minerva_name "sa2_degraded"
      minerva_type "Degraded"
      minerva_x 654.0
      minerva_x2 1492.166666666667
      minerva_y 861.5
      minerva_y2 653.6666666666667
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa50"
      species_meta_id "s_id_sa50"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 45
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 200.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "several_space_drugs__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa212"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa212"
      minerva_name "Several drugs"
      minerva_type "Drug"
      minerva_x 541.0
      minerva_y 846.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa212"
      species_meta_id "s_id_sa212"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 46
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 300.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "v_minus_atpase:nsp6__endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa32"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa32"
      minerva_name "V-ATPase:Nsp6"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/Q15904"
      minerva_ref_type__resource1 "UNIPROT__Q15904"
      minerva_type "Complex"
      minerva_x 412.0
      minerva_y 964.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa32"
      species_meta_id "s_id_csa32"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 47
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 400.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sa170_underscore_degraded__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa161"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa161"
      minerva_name "sa170_degraded"
      minerva_type "Degraded"
      minerva_x 1165.75
      minerva_y 326.83333333333326
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa161"
      species_meta_id "s_id_sa161"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 48
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 500.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "v_minus_atpase__endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa30"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa30"
      minerva_name "V-ATPase"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/Q15904"
      minerva_ref_type__resource1 "UNIPROT__Q15904"
      minerva_type "Complex"
      minerva_x 417.0
      minerva_y 1122.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa30"
      species_meta_id "s_id_csa30"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 49
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 600.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nup210:selinexor__endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa44"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa44"
      minerva_name "NUP210:Selinexor"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/32353859"
      minerva_ref_type__resource1 "PUBMED__32353859"
      minerva_type "Complex"
      minerva_x 179.0
      minerva_y 636.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa44"
      species_meta_id "s_id_csa44"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 50
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 700.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "atp6ap1__endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa223"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa223"
      minerva_former_symbols "ATP6IP1; ATP6S1"
      minerva_name "ATP6AP1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/537"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000071553"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/537"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/868"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_001183"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/Q15904"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/pubmed/27231034"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6AP1"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6AP1"
      minerva_ref_type__resource1 "ENTREZ__537"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000071553"
      minerva_ref_type__resource3 "ENTREZ__537"
      minerva_ref_type__resource4 "HGNC__868"
      minerva_ref_type__resource5 "REFSEQ__NM_001183"
      minerva_ref_type__resource6 "UNIPROT__Q15904"
      minerva_ref_type__resource7 "PUBMED__27231034"
      minerva_ref_type__resource8 "HGNC_SYMBOL__ATP6AP1"
      minerva_ref_type__resource9 "HGNC_SYMBOL__ATP6AP1"
      minerva_type "Protein"
      minerva_x 462.0
      minerva_y 1226.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa223"
      species_meta_id "s_id_sa223"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 51
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 800.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "slc6a15:orf9c__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa47"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa47"
      minerva_name "SLC6A15:Orf9c"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/32353859"
      minerva_ref_type__resource1 "PUBMED__32353859"
      minerva_type "Complex"
      minerva_x 2412.0
      minerva_y 588.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa47"
      species_meta_id "s_id_csa47"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 52
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 900.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp4:nup210__endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa14"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa14"
      minerva_name "Nsp4:NUP210"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/14517331"
      minerva_ref_type__resource1 "PUBMED__14517331"
      minerva_type "Complex"
      minerva_x 148.5
      minerva_y 771.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa14"
      species_meta_id "s_id_csa14"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 54
    zlevel -1

    cluster [
      cluster "s_id_ca7"
    ]
    graphics [
      x 1100.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "v_minus_atpase__endosome__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa33"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa33"
      minerva_name "V-ATPase"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/Q15904"
      minerva_ref_type__resource1 "UNIPROT__Q15904"
      minerva_type "Complex"
      minerva_x 759.25
      minerva_y 1239.75
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca7"
      compartment_name "endosome"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa33"
      species_meta_id "s_id_csa33"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 55
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 1200.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "atp5mg__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa255"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa255"
      minerva_former_symbols "ATP5L"
      minerva_name "ATP5MG"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/14247"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_006476"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP5MG"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP5MG"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000167283"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/10632"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/10632"
      minerva_ref_link8 "https://purl.uniprot.org/uniprot/O75964"
      minerva_ref_type__resource1 "HGNC__14247"
      minerva_ref_type__resource2 "REFSEQ__NM_006476"
      minerva_ref_type__resource3 "HGNC_SYMBOL__ATP5MG"
      minerva_ref_type__resource4 "HGNC_SYMBOL__ATP5MG"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000167283"
      minerva_ref_type__resource6 "ENTREZ__10632"
      minerva_ref_type__resource7 "ENTREZ__10632"
      minerva_ref_type__resource8 "UNIPROT__O75964"
      minerva_type "Protein"
      minerva_x 1585.5294117647059
      minerva_y 1342.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa255"
      species_meta_id "s_id_sa255"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 56
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 200.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "atp13a3__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa222"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa222"
      minerva_name "ATP13A3"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000133657"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/Q9H7F0"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP13A3"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP13A3"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/24113"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_024524"
      minerva_ref_link7 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.2.2.-"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/79572"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/79572"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000133657"
      minerva_ref_type__resource2 "UNIPROT__Q9H7F0"
      minerva_ref_type__resource3 "HGNC_SYMBOL__ATP13A3"
      minerva_ref_type__resource4 "HGNC_SYMBOL__ATP13A3"
      minerva_ref_type__resource5 "HGNC__24113"
      minerva_ref_type__resource6 "REFSEQ__NM_024524"
      minerva_ref_type__resource7 "EC__7.2.2.-"
      minerva_ref_type__resource8 "ENTREZ__79572"
      minerva_ref_type__resource9 "ENTREZ__79572"
      minerva_type "Protein"
      minerva_x 2659.5
      minerva_y 247.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa222"
      species_meta_id "s_id_sa222"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 58
    zlevel -1

    cluster [
      cluster "s_id_ca4"
    ]
    graphics [
      x 400.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp4__mitochondrion__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa217"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa217"
      minerva_name "Nsp4"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=2697049"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/BCD58761"
      minerva_ref_type__resource1 "TAXONOMY__2697049"
      minerva_ref_type__resource2 "NCBI_PROTEIN__BCD58761"
      minerva_type "Protein"
      minerva_x 1136.5882352941178
      minerva_y 1236.8235294117649
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca4"
      compartment_name "mitochondrion"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa217"
      species_meta_id "s_id_sa217"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 59
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 500.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nup210__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa224"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa224"
      minerva_name "NUP210"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NUP210"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NUP210"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/pubmed/14517331"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/Q8TEM1"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/23225"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000132182"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/23225"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/30052"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_024923"
      minerva_ref_type__resource1 "HGNC_SYMBOL__NUP210"
      minerva_ref_type__resource2 "HGNC_SYMBOL__NUP210"
      minerva_ref_type__resource3 "PUBMED__14517331"
      minerva_ref_type__resource4 "UNIPROT__Q8TEM1"
      minerva_ref_type__resource5 "ENTREZ__23225"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000132182"
      minerva_ref_type__resource7 "ENTREZ__23225"
      minerva_ref_type__resource8 "HGNC__30052"
      minerva_ref_type__resource9 "REFSEQ__NM_024923"
      minerva_type "Protein"
      minerva_x 495.5
      minerva_y 1720.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa224"
      species_meta_id "s_id_sa224"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 60
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 600.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp4:nup210__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa13"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa13"
      minerva_name "Nsp4:NUP210"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/14517331"
      minerva_ref_type__resource1 "PUBMED__14517331"
      minerva_type "Complex"
      minerva_x 386.5
      minerva_y 1730.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa13"
      species_meta_id "s_id_csa13"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 61
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 700.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tim_space_complex:nsp4__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa27"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa27"
      minerva_name "TIM complex:Nsp4"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/32353859"
      minerva_ref_type__resource1 "PUBMED__32353859"
      minerva_type "Complex"
      minerva_x 1284.125
      minerva_y 1557.625
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa27"
      species_meta_id "s_id_csa27"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 62
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 800.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "alg11__endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa14"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa230"
      minerva_elementId2 "sa14"
      minerva_name "ALG11"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/32456"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000253710"
      minerva_ref_link2 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.4.1.131"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/440138"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/440138"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/Q2TAA5"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ALG11"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ALG11"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/pubmed/20080937"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_001004127"
      minerva_ref_type__resource1 "HGNC__32456"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000253710"
      minerva_ref_type__resource2 "EC__2.4.1.131"
      minerva_ref_type__resource3 "ENTREZ__440138"
      minerva_ref_type__resource4 "ENTREZ__440138"
      minerva_ref_type__resource5 "UNIPROT__Q2TAA5"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ALG11"
      minerva_ref_type__resource7 "HGNC_SYMBOL__ALG11"
      minerva_ref_type__resource8 "PUBMED__20080937"
      minerva_ref_type__resource9 "REFSEQ__NM_001004127"
      minerva_type "Protein"
      minerva_x 321.5
      minerva_x2 316.0
      minerva_y 321.5
      minerva_y2 448.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa14"
      species_meta_id "s_id_sa14"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 63
    zlevel -1

    cluster [
      cluster "s_id_ca8"
    ]
    graphics [
      x 900.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp3:nsp4:nsp6__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa39"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa39"
      minerva_name "Nsp3:Nsp4:Nsp6"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/32353859"
      minerva_ref_link2 "https://doi.org/10.1016/j.virol.2017.07.019"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=694009"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/pubmed/29128390"
      minerva_ref_type__resource1 "PUBMED__32353859"
      minerva_ref_type__resource2 "DOI__10.1016/j.virol.2017.07.019"
      minerva_ref_type__resource3 "TAXONOMY__694009"
      minerva_ref_type__resource4 "PUBMED__29128390"
      minerva_type "Complex"
      minerva_x 985.25
      minerva_y 567.3333333333333
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca8"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa39"
      species_meta_id "s_id_csa39"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 64
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1000.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sigmar1__endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa53"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa53"
      minerva_former_symbols "OPRS1"
      minerva_name "SIGMAR1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/10406945"
      minerva_ref_link10 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/8157"
      minerva_ref_link11 "https://purl.uniprot.org/uniprot/Q99720"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_005866"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SIGMAR1"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000147955"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SIGMAR1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/pubmed/32353859"
      minerva_ref_link7 "https://doi.org/10.1074/jbc.272.43.27107"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/10280"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/10280"
      minerva_ref_type__resource1 "PUBMED__10406945"
      minerva_ref_type__resource10 "HGNC__8157"
      minerva_ref_type__resource11 "UNIPROT__Q99720"
      minerva_ref_type__resource2 "REFSEQ__NM_005866"
      minerva_ref_type__resource3 "HGNC_SYMBOL__SIGMAR1"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000147955"
      minerva_ref_type__resource5 "HGNC_SYMBOL__SIGMAR1"
      minerva_ref_type__resource6 "PUBMED__32353859"
      minerva_ref_type__resource7 "DOI__10.1074/jbc.272.43.27107"
      minerva_ref_type__resource8 "ENTREZ__10280"
      minerva_ref_type__resource9 "ENTREZ__10280"
      minerva_type "Protein"
      minerva_x 149.16666666666674
      minerva_y 1047.1666666666667
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa53"
      species_meta_id "s_id_sa53"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 65
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 1100.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ide__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa154"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa154"
      minerva_name "IDE"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/P14735"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IDE"
      minerva_ref_link11 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IDE"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/3416"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/3416"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/pubmed/32353859"
      minerva_ref_link5 "https://doi.org/10.1210/mend-4-8-1125"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/5381"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_004969"
      minerva_ref_link8 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.24.56"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000119912"
      minerva_ref_type__resource1 "UNIPROT__P14735"
      minerva_ref_type__resource10 "HGNC_SYMBOL__IDE"
      minerva_ref_type__resource11 "HGNC_SYMBOL__IDE"
      minerva_ref_type__resource2 "ENTREZ__3416"
      minerva_ref_type__resource3 "ENTREZ__3416"
      minerva_ref_type__resource4 "PUBMED__32353859"
      minerva_ref_type__resource5 "DOI__10.1210/mend-4-8-1125"
      minerva_ref_type__resource6 "HGNC__5381"
      minerva_ref_type__resource7 "REFSEQ__NM_004969"
      minerva_ref_type__resource8 "EC__3.4.24.56"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000119912"
      minerva_type "Protein"
      minerva_x 706.5163398692812
      minerva_y 137.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa154"
      species_meta_id "s_id_sa154"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 66
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 1200.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "bafilomycin_space_a1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa188"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa188"
      minerva_name "Bafilomycin A1"
      minerva_type "Drug"
      minerva_x 416.0
      minerva_y 1624.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa188"
      species_meta_id "s_id_sa188"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 67
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 200.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp4:dnajc11__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa12"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa12"
      minerva_name "Nsp4:DNAJC11"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/32353859"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/pubmed/25997101"
      minerva_ref_type__resource1 "PUBMED__32353859"
      minerva_ref_type__resource2 "PUBMED__25997101"
      minerva_type "Complex"
      minerva_x 1337.0
      minerva_y 830.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa12"
      species_meta_id "s_id_csa12"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 68
    zlevel -1

    cluster [
      cluster "s_id_ca4"
    ]
    graphics [
      x 300.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tom_space_complex__mitochondrion__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa24"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa24"
      minerva_name "TOM complex"
      minerva_type "Complex"
      minerva_x 1055.0
      minerva_y 1554.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca4"
      compartment_name "mitochondrion"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa24"
      species_meta_id "s_id_csa24"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 69
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 400.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "f_minus_atpase:nsp6__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa43"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa43"
      minerva_name "F-ATPase:Nsp6"
      minerva_type "Complex"
      minerva_x 1539.7777777777778
      minerva_y 1611.4444444444443
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa43"
      species_meta_id "s_id_csa43"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 70
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 500.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "dnajc11__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa13"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa13"
      minerva_name "DNAJC11"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/Q9NVH1"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DNAJC11"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/25570"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/55735"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/55735"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/pubmed/32353859"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/pubmed/25997101"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000007923"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DNAJC11"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_018198"
      minerva_ref_type__resource1 "UNIPROT__Q9NVH1"
      minerva_ref_type__resource10 "HGNC_SYMBOL__DNAJC11"
      minerva_ref_type__resource2 "HGNC__25570"
      minerva_ref_type__resource3 "ENTREZ__55735"
      minerva_ref_type__resource4 "ENTREZ__55735"
      minerva_ref_type__resource5 "PUBMED__32353859"
      minerva_ref_type__resource6 "PUBMED__25997101"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000007923"
      minerva_ref_type__resource8 "HGNC_SYMBOL__DNAJC11"
      minerva_ref_type__resource9 "REFSEQ__NM_018198"
      minerva_type "Protein"
      minerva_x 1303.0
      minerva_y 937.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa13"
      species_meta_id "s_id_sa13"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 71
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 600.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "h_plus___endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa233"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa233"
      minerva_fullName "hydron"
      minerva_name "H+"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15378"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15378"
      minerva_synonyms "H(+); H+; Hydron"
      minerva_type "Ion"
      minerva_x 387.0
      minerva_y 1528.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa233"
      species_meta_id "s_id_sa233"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 72
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 700.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf9c__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa192"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa192"
      minerva_name "Orf9c"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?mode=Info&id=2697049"
      minerva_ref_type__resource1 "TAXONOMY__2697049"
      minerva_type "Protein"
      minerva_x 2305.0
      minerva_y 623.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa192"
      species_meta_id "s_id_sa192"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 73
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 800.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp6:sigmar1__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa19"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa19"
      minerva_name "Nsp6:SIGMAR1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/32353859"
      minerva_ref_type__resource1 "PUBMED__32353859"
      minerva_type "Complex"
      minerva_x 265.66666666666697
      minerva_y 1811.1666666666667
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa19"
      species_meta_id "s_id_csa19"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 74
    zlevel -1

    graphics [
      x 900.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re6"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "Heterodimer association"
      minerva_x 606.5161290322571
      minerva_y 631.9999999999985
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re6"
      reaction_meta_id "re6"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 78
    zlevel -1

    graphics [
      x 1000.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re40"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "State transition"
      minerva_x 704.2581699346406
      minerva_y 410.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re40"
      reaction_meta_id "re40"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 81
    zlevel -1

    graphics [
      x 1100.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re71"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "State transition"
      minerva_x 374.9999999999996
      minerva_y 1286.25
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re71"
      reaction_meta_id "re71"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 84
    zlevel -1

    graphics [
      x 1200.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re80"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "State transition"
      minerva_x 588.125
      minerva_y 1180.875
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re80"
      reaction_meta_id "re80"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 87
    zlevel -1

    graphics [
      x 200.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re77"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "State transition"
      minerva_x 318.75
      minerva_y 384.75
    ]
    sbml [
      reaction_id "re77"
      reaction_meta_id "re77"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 90
    zlevel -1

    graphics [
      x 300.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re22"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "State transition"
      minerva_x 1544.143946990613
      minerva_y 519.5833435058594
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re22"
      reaction_meta_id "re22"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 93
    zlevel -1

    graphics [
      x 400.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re32"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "State transition"
      minerva_x 1458.3333333333335
      minerva_y 599.8333333333334
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re32"
      reaction_meta_id "re32"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 96
    zlevel -1

    graphics [
      x 500.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re21"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "State transition"
      minerva_x 1480.8333333333335
      minerva_y 574.8333333333334
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re21"
      reaction_meta_id "re21"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 99
    zlevel -1

    graphics [
      x 600.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re24"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "State transition"
      minerva_x 1512.2187940032168
      minerva_y 623.5884756618414
    ]
    sbml [
      reaction_id "re24"
      reaction_meta_id "re24"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 102
    zlevel -1

    graphics [
      x 700.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re57"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "Heterodimer association"
      minerva_x 2084.576923076923
      minerva_y 645.9999999999992
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re57"
      reaction_meta_id "re57"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 106
    zlevel -1

    graphics [
      x 800.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re75"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "State transition"
      minerva_x 128.5833357522654
      minerva_y 1473.8333129882812
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re75"
      reaction_meta_id "re75"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 109
    zlevel -1

    graphics [
      x 900.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re83"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "Heterodimer association"
      minerva_x 1656.7132388844207
      minerva_y 1158.9177938514222
    ]
    sbml [
      reaction_id "re83"
      reaction_meta_id "re83"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 113
    zlevel -1

    graphics [
      x 1000.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re76"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "State transition"
      minerva_x 162.25
      minerva_y 405.75
    ]
    sbml [
      reaction_id "re76"
      reaction_meta_id "re76"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 116
    zlevel -1

    graphics [
      x 1100.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re48"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "State transition"
      minerva_x 1008.8420124743461
      minerva_y 351.6893751831941
    ]
    sbml [
      reaction_id "re48"
      reaction_meta_id "re48"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 119
    zlevel -1

    graphics [
      x 1200.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re18"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "Heterodimer association"
      minerva_x 318.00000000000006
      minerva_y 478.84210526315763
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re18"
      reaction_meta_id "re18"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 123
    zlevel -1

    graphics [
      x 200.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re79"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "Heterodimer association"
      minerva_x 393.1255656108594
      minerva_y 1205.7500000000007
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re79"
      reaction_meta_id "re79"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 127
    zlevel -1

    graphics [
      x 300.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re51"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "State transition"
      minerva_x 1086.8036971831011
      minerva_y 517.4893927211549
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re51"
      reaction_meta_id "re51"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 132
    zlevel -1

    graphics [
      x 400.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re64"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "Positive influence"
      minerva_x 914.4989455271576
      minerva_y 493.12333841959634
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re64"
      reaction_meta_id "re64"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 135
    zlevel -1

    graphics [
      x 500.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re28"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "State transition"
      minerva_x 214.16666666666686
      minerva_y 1499.1666666666667
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re28"
      reaction_meta_id "re28"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 138
    zlevel -1

    graphics [
      x 600.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re17"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "State transition"
      minerva_x 267.5
      minerva_y 1250.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re17"
      reaction_meta_id "re17"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 141
    zlevel -1

    graphics [
      x 700.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re19"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "State transition"
      minerva_x 726.1875
      minerva_y 886.75
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re19"
      reaction_meta_id "re19"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 144
    zlevel -1

    graphics [
      x 800.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re33"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "State transition"
      minerva_x 1371.558079744052
      minerva_y 617.7849249073166
    ]
    sbml [
      reaction_id "re33"
      reaction_meta_id "re33"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 147
    zlevel -1

    graphics [
      x 900.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re4"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "State transition"
      minerva_x 689.1993495245545
      minerva_y 855.006916107121
    ]
    sbml [
      reaction_id "re4"
      reaction_meta_id "re4"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 150
    zlevel -1

    graphics [
      x 1000.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re50"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "State transition"
      minerva_x 1201.7753110732951
      minerva_y 431.24646066794094
    ]
    sbml [
      reaction_id "re50"
      reaction_meta_id "re50"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 153
    zlevel -1

    graphics [
      x 1100.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re37"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "Heterodimer association"
      minerva_x 1188.7024833003463
      minerva_y 1377.8124999999993
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re37"
      reaction_meta_id "re37"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 157
    zlevel -1

    graphics [
      x 1200.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re7"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "State transition"
      minerva_x 636.5
      minerva_y 403.0
    ]
    sbml [
      reaction_id "re7"
      reaction_meta_id "re7"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 160
    zlevel -1

    graphics [
      x 200.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re2"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "State transition"
      minerva_x 755.6642335766423
      minerva_y 812.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re2"
      reaction_meta_id "re2"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 163
    zlevel -1

    graphics [
      x 300.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re78"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "Transport"
      minerva_x 461.0
      minerva_y 1528.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re78"
      reaction_meta_id "re78"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 167
    zlevel -1

    graphics [
      x 400.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re62"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "Heterodimer association"
      minerva_x 218.3636363636371
      minerva_y 1007.0000000000002
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re62"
      reaction_meta_id "re62"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 171
    zlevel -1

    graphics [
      x 500.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re72"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "State transition"
      minerva_x 730.7500000000002
      minerva_y 467.24999999999994
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re72"
      reaction_meta_id "re72"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 174
    zlevel -1

    graphics [
      x 600.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re45"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "State transition"
      minerva_x 1064.125
      minerva_y 369.91666666666663
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re45"
      reaction_meta_id "re45"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 177
    zlevel -1

    graphics [
      x 700.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re67"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "Heterodimer association"
      minerva_x 2846.2423245614045
      minerva_y 351.7500000000005
    ]
    sbml [
      reaction_id "re67"
      reaction_meta_id "re67"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 181
    zlevel -1

    graphics [
      x 800.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re53"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "Heterodimer association"
      minerva_x 305.5
      minerva_y 749.7643165024633
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re53"
      reaction_meta_id "re53"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 185
    zlevel -1

    graphics [
      x 900.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re63"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "State transition"
      minerva_x 945.2941176470595
      minerva_y 1005.411764705883
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re63"
      reaction_meta_id "re63"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 189
    zlevel -1

    graphics [
      x 1000.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re66"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "Heterodimer association"
      minerva_x 2627.739542643229
      minerva_y 315.4795626028268
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re66"
      reaction_meta_id "re66"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 193
    zlevel -1

    graphics [
      x 1100.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re16"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "Heterodimer association"
      minerva_x 1199.0000000000032
      minerva_y 859.309734513281
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re16"
      reaction_meta_id "re16"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 197
    zlevel -1

    graphics [
      x 1200.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re81"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "State transition"
      minerva_x 587.125
      minerva_y 1025.375
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re81"
      reaction_meta_id "re81"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 200
    zlevel -1

    graphics [
      x 200.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re31"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "Heterodimer association"
      minerva_x 516.5833333333378
      minerva_y 1039.2724780701724
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re31"
      reaction_meta_id "re31"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 204
    zlevel -1

    graphics [
      x 300.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re27"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "Heterodimer association"
      minerva_x 238.9166692097932
      minerva_y 1132.8617868066801
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re27"
      reaction_meta_id "re27"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 208
    zlevel -1

    graphics [
      x 400.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re10"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "State transition"
      minerva_x 500.3484419263457
      minerva_y 333.5
    ]
    sbml [
      reaction_id "re10"
      reaction_meta_id "re10"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 211
    zlevel -1

    graphics [
      x 500.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re20"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "State transition"
      minerva_x 684.8906072750901
      minerva_y 947.8949497533577
    ]
    sbml [
      reaction_id "re20"
      reaction_meta_id "re20"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 214
    zlevel -1

    graphics [
      x 600.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re82"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "Heterodimer association"
      minerva_x 1669.7779416265134
      minerva_y 1366.1161769206315
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re82"
      reaction_meta_id "re82"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 218
    zlevel -1

    graphics [
      x 700.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re3"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "State transition"
      minerva_x 688.3782546834867
      minerva_y 790.5711478072005
    ]
    sbml [
      reaction_id "re3"
      reaction_meta_id "re3"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 221
    zlevel -1

    graphics [
      x 800.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re25"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "Heterodimer association"
      minerva_x 1706.4722493489528
      minerva_y 1511.3975700648307
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re25"
      reaction_meta_id "re25"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 225
    zlevel -1

    graphics [
      x 900.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re55"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "Heterodimer association"
      minerva_x 2293.500000000001
      minerva_y 589.9010695187168
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re55"
      reaction_meta_id "re55"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 229
    zlevel -1

    graphics [
      x 1000.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re60"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "Heterodimer association"
      minerva_x 375.1363636363601
      minerva_y 1582.0522772757554
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re60"
      reaction_meta_id "re60"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 233
    zlevel -1

    graphics [
      x 1100.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re74"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "State transition"
      minerva_x 118.8333333333334
      minerva_y 1345.3333333333335
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re74"
      reaction_meta_id "re74"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 236
    zlevel -1

    graphics [
      x 1200.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re23"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "State transition"
      minerva_x 1604.8068196487031
      minerva_y 498.6796562281134
    ]
    sbml [
      reaction_id "re23"
      reaction_meta_id "re23"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 239
    zlevel -1

    graphics [
      x 200.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re56"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "Heterodimer association"
      minerva_x 2369.4999999999995
      minerva_y 684.0421245421246
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re56"
      reaction_meta_id "re56"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 243
    zlevel -1

    graphics [
      x 300.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re54"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "Heterodimer association"
      minerva_x 2166.9999999999995
      minerva_y 490.22321428571405
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re54"
      reaction_meta_id "re54"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 247
    zlevel -1

    graphics [
      x 400.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re9"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "Heterodimer association"
      minerva_x 298.4999999999979
      minerva_y 814.7999999999997
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re9"
      reaction_meta_id "re9"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 251
    zlevel -1

    graphics [
      x 500.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re46"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "State transition"
      minerva_x 1041.625
      minerva_y 394.91666666666663
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re46"
      reaction_meta_id "re46"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 254
    zlevel -1

    graphics [
      x 600.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re47"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "State transition"
      minerva_x 1142.2228295146551
      minerva_y 424.0833384195962
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re47"
      reaction_meta_id "re47"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 257
    zlevel -1

    graphics [
      x 700.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re49"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "State transition"
      minerva_x 1142.9110405706429
      minerva_y 350.9899250374609
    ]
    sbml [
      reaction_id "re49"
      reaction_meta_id "re49"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 260
    zlevel -1

    graphics [
      x 800.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re26"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "Heterodimer association"
      minerva_x 2208.2663128930785
      minerva_y 340.0833333333345
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re26"
      reaction_meta_id "re26"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 264
    zlevel -1

    graphics [
      x 900.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re73"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "State transition"
      minerva_x 278.83333333333354
      minerva_y 1457.3333333333335
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re73"
      reaction_meta_id "re73"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 267
    zlevel -1

    graphics [
      x 1000.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/863"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8992"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000113732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15342"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003945"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP6V0E1"
      minerva_ref_type__resource1 "HGNC__863"
      minerva_ref_type__resource2 "ENTREZ__8992"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000113732"
      minerva_ref_type__resource4 "UNIPROT__O15342"
      minerva_ref_type__resource5 "REFSEQ__NM_003945"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATP6V0E1"
      minerva_type "State transition"
      minerva_x 776.8125
      minerva_y 886.75
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1"
      reaction_meta_id "re1"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 270
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 550.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "p_minus_atpase__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa41"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa41"
      minerva_name "P-ATPase"
      minerva_type "Complex"
      minerva_x 2529.0833333333335
      minerva_y 212.33333333333326
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa41"
      species_meta_id "s_id_csa41"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
      species_notes "XMLNode 'notes' [childElements size=1]"
    ]
  ]
  node [
    id 275
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 650.0
      y 350.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "f_minus_atpase__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa97"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa97"
      minerva_name "F-ATPase"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/O75964"
      minerva_ref_type__resource1 "UNIPROT__O75964"
      minerva_type "Complex"
      minerva_x 1525.5294117647059
      minerva_y 1227.2058823529412
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation "Annotation [about=\"#s_id_csa97\", history=]"
      species_id "s_id_csa97"
      species_meta_id "s_id_csa97"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
    ]
  ]
  edge [
    id 280
    source 40
    target 74
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa4"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 281
    source 11
    target 74
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa225"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 282
    source 74
    target 34
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa4"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 283
    source 11
    target 78
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa225"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 284
    source 78
    target 65
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa154"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 285
    source 30
    target 81
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa9"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 286
    source 81
    target 59
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa224"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 287
    source 48
    target 84
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa30"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 288
    source 84
    target 54
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 289
    source 62
    target 87
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa14"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 291
    source 27
    target 90
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa47"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 292
    source 90
    target 20
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa48"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 293
    source 27
    target 93
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa47"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 294
    source 93
    target 14
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa218"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 295
    source 14
    target 96
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa218"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 296
    source 96
    target 27
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa47"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 297
    source 27
    target 99
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa47"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 298
    source 99
    target 44
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa50"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 299
    source 37
    target 102
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa191"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 300
    source 41
    target 102
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa200"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 301
    source 102
    target 16
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa49"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 302
    source 23
    target 106
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa18"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 303
    source 106
    target 22
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa92"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 304
    source 43
    target 109
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa101"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 305
    source 36
    target 109
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa100"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 306
    source 4
    target 113
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa15"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 308
    source 12
    target 116
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa219"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 309
    source 116
    target 39
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa159"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 310
    source 40
    target 119
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa4"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 311
    source 62
    target 119
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa14"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 312
    source 119
    target 4
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa15"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 313
    source 10
    target 123
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa94"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 314
    source 50
    target 123
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa223"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 315
    source 123
    target 48
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa30"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 316
    source 42
    target 127
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa162"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 317
    source 20
    target 127
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa48"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 318
    source 40
    target 127
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa4"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 319
    source 127
    target 63
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa39"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 320
    source 63
    target 132
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa39"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 321
    source 132
    target 21
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa164"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 322
    source 23
    target 135
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa18"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 323
    source 135
    target 73
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa19"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 324
    source 52
    target 138
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa14"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 325
    source 138
    target 60
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa13"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 326
    source 31
    target 141
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa3"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 327
    source 141
    target 3
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa220"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 328
    source 14
    target 144
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa218"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 329
    source 144
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa16"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 330
    source 31
    target 147
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa3"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 331
    source 147
    target 44
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa50"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 332
    source 42
    target 150
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa162"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 333
    source 150
    target 26
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa163"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 334
    source 58
    target 153
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa217"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 335
    source 15
    target 153
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa29"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 336
    source 153
    target 61
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa27"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 337
    source 34
    target 157
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa4"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 339
    source 31
    target 160
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa3"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 340
    source 160
    target 40
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa4"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 341
    source 8
    target 163
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa234"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 342
    source 163
    target 71
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa233"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 343
    source 10
    target 163
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR0"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa94"
    ]
  ]
  edge [
    id 344
    source 45
    target 167
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa212"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 345
    source 64
    target 167
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa53"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 346
    source 167
    target 29
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa53"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 347
    source 11
    target 171
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa225"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 349
    source 12
    target 174
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa219"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 350
    source 174
    target 1
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa160"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 351
    source 6
    target 177
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa84"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 352
    source 19
    target 177
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa83"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 353
    source 30
    target 181
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa9"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 354
    source 7
    target 181
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa187"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 355
    source 181
    target 49
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa44"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 356
    source 40
    target 185
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa4"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 357
    source 185
    target 58
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa217"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 358
    source 68
    target 185
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR1"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa24"
    ]
  ]
  edge [
    id 359
    source 56
    target 189
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa222"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 360
    source 40
    target 193
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa4"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 361
    source 70
    target 193
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa13"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 362
    source 193
    target 67
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa12"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 363
    source 46
    target 197
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa32"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 364
    source 197
    target 24
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa34"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 365
    source 20
    target 200
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa48"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 366
    source 48
    target 200
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa30"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 367
    source 200
    target 46
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa32"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 368
    source 20
    target 204
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa48"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 369
    source 64
    target 204
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa53"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 370
    source 204
    target 23
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa18"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 371
    source 34
    target 208
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa4"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 372
    source 208
    target 35
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa5"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 373
    source 3
    target 211
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa220"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 374
    source 211
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa16"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 375
    source 55
    target 214
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa255"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 376
    source 40
    target 218
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa4"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 377
    source 218
    target 5
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa49"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 378
    source 20
    target 221
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa48"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 379
    source 221
    target 69
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa43"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 380
    source 37
    target 225
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa191"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 381
    source 72
    target 225
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa192"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 382
    source 225
    target 51
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa47"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 383
    source 50
    target 229
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa223"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 384
    source 66
    target 229
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa188"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 385
    source 229
    target 25
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa50"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 386
    source 64
    target 233
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa53"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 387
    source 233
    target 38
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa227"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 388
    source 20
    target 236
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa48"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 389
    source 236
    target 5
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa49"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 390
    source 37
    target 239
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa191"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 391
    source 33
    target 239
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa193"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 392
    source 239
    target 28
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa48"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 393
    source 20
    target 243
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa48"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 394
    source 37
    target 243
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa191"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 395
    source 243
    target 9
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa45"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 396
    source 40
    target 247
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa4"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 397
    source 30
    target 247
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa9"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 398
    source 247
    target 52
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa14"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 399
    source 1
    target 251
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa160"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 400
    source 251
    target 12
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa219"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 401
    source 1
    target 254
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa160"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 402
    source 254
    target 42
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa162"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 403
    source 1
    target 257
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa160"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 404
    source 257
    target 47
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa161"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 405
    source 20
    target 260
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa48"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 406
    source 260
    target 17
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa40"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 407
    source 64
    target 264
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa53"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 408
    source 264
    target 32
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa226"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 409
    source 3
    target 267
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa220"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 410
    source 267
    target 31
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa3"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 411
    source 189
    target 270
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa41"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 412
    source 270
    target 260
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa41"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 413
    source 177
    target 270
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa80"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 415
    source 109
    target 275
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa97"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 416
    source 275
    target 214
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa97"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 418
    source 275
    target 221
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa42"
      stoichiometry "1.0"
    ]
  ]
]
