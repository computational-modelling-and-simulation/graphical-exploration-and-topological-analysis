# generated with VANTED V2.8.0 at Tue Apr 27 20:00:52 CEST 2021
graph [
  background_coloring "true"
  cluster_colors "255,128,128,255:0,0,0,255;230,255,128,255:0,0,0,255;128,255,179,255:0,0,0,255;128,178,255,255:0,0,0,255;230,128,255,255:0,0,0,255"
  clusterbackground_fill_outer_region "false"
  clusterbackground_grid 50.0
  clusterbackground_low_alpha 0.2
  clusterbackground_radius 200.0
  clusterbackground_space_fill "true"
  graphbackgroundcolor "#ffffff"
  sbml [
    model_annotation ""
    model_meta_id "mitochondrium"
    model_non_rdf_annotation ""
    model_notes ""
    model_sbml_id "mitochondrium"
    namespace "xmlns:celldesigner=\"http://www.sbml.org/2001/ns/celldesigner\" "
    sbml_level "2"
    sbml_version "4"
  ]
  sbml_compartment_default [
    sbml_compartment_default_id "default"
    sbml_compartment_default_meta_id "default"
    sbml_compartment_default_non_rdf_annotation ""
    sbml_compartment_default_notes ""
    sbml_compartment_default_size "1.0"
    sbml_compartment_default_units "volume"
  ]
  sbml_compartment_s_id_ca1 [
    sbml_compartment_s_id_ca1_annotation ""
    sbml_compartment_s_id_ca1_id "s_id_ca1"
    sbml_compartment_s_id_ca1_meta_id "s_id_ca1"
    sbml_compartment_s_id_ca1_name "mitochondrion"
    sbml_compartment_s_id_ca1_non_rdf_annotation ""
    sbml_compartment_s_id_ca1_notes ""
    sbml_compartment_s_id_ca1_outside "s_id_ca4"
    sbml_compartment_s_id_ca1_size "1.0"
    sbml_compartment_s_id_ca1_units "volume"
  ]
  sbml_compartment_s_id_ca2 [
    sbml_compartment_s_id_ca2_annotation ""
    sbml_compartment_s_id_ca2_id "s_id_ca2"
    sbml_compartment_s_id_ca2_meta_id "s_id_ca2"
    sbml_compartment_s_id_ca2_name "mitochondrial_space_matrix"
    sbml_compartment_s_id_ca2_non_rdf_annotation ""
    sbml_compartment_s_id_ca2_notes ""
    sbml_compartment_s_id_ca2_outside "s_id_ca1"
    sbml_compartment_s_id_ca2_size "1.0"
    sbml_compartment_s_id_ca2_units "volume"
  ]
  sbml_compartment_s_id_ca3 [
    sbml_compartment_s_id_ca3_annotation ""
    sbml_compartment_s_id_ca3_id "s_id_ca3"
    sbml_compartment_s_id_ca3_meta_id "s_id_ca3"
    sbml_compartment_s_id_ca3_name "mitochondrial_space_nucleoid"
    sbml_compartment_s_id_ca3_non_rdf_annotation ""
    sbml_compartment_s_id_ca3_notes ""
    sbml_compartment_s_id_ca3_outside "s_id_ca2"
    sbml_compartment_s_id_ca3_size "1.0"
    sbml_compartment_s_id_ca3_units "volume"
  ]
  sbml_compartment_s_id_ca4 [
    sbml_compartment_s_id_ca4_annotation ""
    sbml_compartment_s_id_ca4_id "s_id_ca4"
    sbml_compartment_s_id_ca4_meta_id "s_id_ca4"
    sbml_compartment_s_id_ca4_name "cell"
    sbml_compartment_s_id_ca4_non_rdf_annotation ""
    sbml_compartment_s_id_ca4_notes ""
    sbml_compartment_s_id_ca4_outside "default"
    sbml_compartment_s_id_ca4_size "1.0"
    sbml_compartment_s_id_ca4_units "volume"
  ]
  sbml_unit_definition_1 [
    sbml_unit_definition_1_id "area"
    sbml_unit_definition_1_name "area"
    sbml_unit_definition_1_sub_unit_1_ "(1.0 * 10^0 * metre)^2.0"
    sbml_unit_definition_1unit "(1.0 * 10^0 * metre)^2.0"
  ]
  sbml_unit_definition_2 [
    sbml_unit_definition_2_id "volume"
    sbml_unit_definition_2_name "volume"
    sbml_unit_definition_2_sub_unit_1_ "(1.0 * 10^0 * litre)^1.0"
    sbml_unit_definition_2unit "(1.0 * 10^0 * litre)^1.0"
  ]
  sbml_unit_definition_3 [
    sbml_unit_definition_3_id "length"
    sbml_unit_definition_3_name "length"
    sbml_unit_definition_3_sub_unit_1_ "(1.0 * 10^0 * metre)^1.0"
    sbml_unit_definition_3unit "(1.0 * 10^0 * metre)^1.0"
  ]
  sbml_unit_definition_4 [
    sbml_unit_definition_4_id "substance"
    sbml_unit_definition_4_name "substance"
    sbml_unit_definition_4_sub_unit_1_ "(1.0 * 10^0 * mole)^1.0"
    sbml_unit_definition_4unit "(1.0 * 10^0 * mole)^1.0"
  ]
  sbml_unit_definition_5 [
    sbml_unit_definition_5_id "time"
    sbml_unit_definition_5_name "time"
    sbml_unit_definition_5_sub_unit_1_ "(1.0 * 10^0 * second)^1.0"
    sbml_unit_definition_5unit "(1.0 * 10^0 * second)^1.0"
  ]
  directed 0
  node [
    id 1
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 200.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "o_underscore_sub_underscore_2_underscore_endsub_underscore___mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa374"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa374"
      minerva_elementId2 "sa24"
      minerva_elementId3 "sa361"
      minerva_fullName "dioxygen"
      minerva_name "O_sub_2_endsub_"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15379"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15379"
      minerva_synonyms "Disauerstoff; E 948; E-948; E948; O2; O2; O2; OXYGEN MOLECULE; Oxygen; [OO]; dioxygene; molecular oxygen"
      minerva_type "Simple molecule"
      minerva_x 820.0
      minerva_x2 1671.0
      minerva_x3 400.0
      minerva_y 939.0
      minerva_y2 363.0
      minerva_y3 1518.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa374"
      species_meta_id "s_id_sa374"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 2
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 300.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "prdx__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa97"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa97"
      minerva_name "PRDX"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/26067716"
      minerva_ref_type__resource1 "PUBMED__26067716"
      minerva_type "Complex"
      minerva_x 933.0
      minerva_y 1459.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa97"
      species_meta_id "s_id_csa97"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 3
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 400.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s1190__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa712"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa712"
      minerva_name "s1190"
      minerva_type "Degraded"
      minerva_x 2353.0
      minerva_y 648.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa712"
      species_meta_id "s_id_sa712"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 4
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 500.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "glutathione_space_disulfide__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa380"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa380"
      minerva_fullName "glutathione disulfide(2-)"
      minerva_name "glutathione disulfide"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:58297"
      minerva_ref_type__resource1 "CHEBI__CHEBI:58297"
      minerva_synonyms "glutathione disulfide; glutathione disulfide dianion"
      minerva_type "Simple molecule"
      minerva_x 630.0
      minerva_y 1683.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa380"
      species_meta_id "s_id_sa380"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 5
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 600.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sirt3__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa205"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa359"
      minerva_elementId2 "sa205"
      minerva_name "SIRT3"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/23410"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/23410"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/Q9NTG7"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SIRT3"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SIRT3"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/14931"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_001017524"
      minerva_ref_link8 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.1.286"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000142082"
      minerva_ref_type__resource1 "ENTREZ__23410"
      minerva_ref_type__resource2 "ENTREZ__23410"
      minerva_ref_type__resource3 "UNIPROT__Q9NTG7"
      minerva_ref_type__resource4 "HGNC_SYMBOL__SIRT3"
      minerva_ref_type__resource5 "HGNC_SYMBOL__SIRT3"
      minerva_ref_type__resource6 "HGNC__14931"
      minerva_ref_type__resource7 "REFSEQ__NM_001017524"
      minerva_ref_type__resource8 "EC__2.3.1.286"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000142082"
      minerva_type "Protein"
      minerva_x 480.0
      minerva_x2 723.5
      minerva_y 1488.0
      minerva_y2 443.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa205"
      species_meta_id "s_id_sa205"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 6
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 700.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "superoxide__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa354"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa360"
      minerva_elementId2 "sa354"
      minerva_fullName "superoxide"
      minerva_name "superoxide"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18421"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:1842"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18421"
      minerva_ref_type__resource2 "CHEBI__CHEBI:1842"
      minerva_synonyms "(O2)(.-); Hyperoxid; O2(-); O2(.-); O2-; O2.-; Superoxide anion; dioxide(1-); hyperoxide; superoxide; superoxide; superoxide anion radical; superoxide radical; superoxide radical anion; superoxyde"
      minerva_type "Simple molecule"
      minerva_x 400.0
      minerva_x2 820.0
      minerva_y 1338.0
      minerva_y2 1103.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa354"
      species_meta_id "s_id_sa354"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 7
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 800.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ho__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa363"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa363"
      minerva_fullName "holmium atom"
      minerva_name "HO"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:49648"
      minerva_ref_type__resource1 "CHEBI__CHEBI:49648"
      minerva_synonyms "67Ho; Ho; holmio; holmium"
      minerva_type "Simple molecule"
      minerva_x 645.0
      minerva_y 1268.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa363"
      species_meta_id "s_id_sa363"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 8
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 900.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "atp_space_synthase__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa47"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa47"
      minerva_fullName "mitochondrial proton-transporting ATP synthase complex"
      minerva_name "ATP Synthase"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0005753"
      minerva_ref_type__resource1 "GO__GO:0005753"
      minerva_type "Complex"
      minerva_x 2353.0
      minerva_y 383.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa47"
      species_meta_id "s_id_csa47"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 9
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1000.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "o_underscore_super_underscore_2_minus__underscore_endsuper_underscore___mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa23"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa23"
      minerva_fullName "oxide(2-)"
      minerva_name "O_super_2-_endsuper_"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:29356"
      minerva_ref_type__resource1 "CHEBI__CHEBI:29356"
      minerva_synonyms "O(2-); oxide"
      minerva_type "Simple molecule"
      minerva_x 1803.0
      minerva_y 363.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa23"
      species_meta_id "s_id_sa23"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 10
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1100.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "q__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa219"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa219"
      minerva_elementId2 "sa6"
      minerva_fullName "ubiquinones"
      minerva_name "Q"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16389"
      minerva_ref_type__resource1 "CHEBI__CHEBI:16389"
      minerva_synonyms "CoQ; Coenzym Q; Coenzyme Q; Koenzym Q; Q; Ubichinon; Ubiquinone; Ubiquinones; a ubiquinone; coenzyme Q; coenzymes Q; mitochondrial ubiquinone; mitochondrial ubiquinones; mitoquinones"
      minerva_type "Simple molecule"
      minerva_x 1154.0
      minerva_x2 1778.0
      minerva_y 293.0
      minerva_y2 283.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa219"
      species_meta_id "s_id_sa219"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 11
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1200.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "timm29__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa700"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa700"
      minerva_former_symbols "C19orf52"
      minerva_name "TIMM29"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000142444"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/25152"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TIMM29"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TIMM29"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_138358"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/Q9BSF4"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/Q9BSF4"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/90580"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/90580"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000142444"
      minerva_ref_type__resource2 "HGNC__25152"
      minerva_ref_type__resource3 "HGNC_SYMBOL__TIMM29"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TIMM29"
      minerva_ref_type__resource5 "REFSEQ__NM_138358"
      minerva_ref_type__resource6 "UNIPROT__Q9BSF4"
      minerva_ref_type__resource7 "UNIPROT__Q9BSF4"
      minerva_ref_type__resource8 "ENTREZ__90580"
      minerva_ref_type__resource9 "ENTREZ__90580"
      minerva_type "Protein"
      minerva_x 353.0
      minerva_y 1782.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa700"
      species_meta_id "s_id_sa700"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 12
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 200.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "txnrd2__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa393"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa393"
      minerva_name "TXNRD2"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=1.8.1.9"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000184470"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TXNRD2"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TXNRD2"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_006440"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/18155"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/10587"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/10587"
      minerva_ref_link9 "https://purl.uniprot.org/uniprot/Q9NNW7"
      minerva_ref_type__resource1 "EC__1.8.1.9"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000184470"
      minerva_ref_type__resource3 "HGNC_SYMBOL__TXNRD2"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TXNRD2"
      minerva_ref_type__resource5 "REFSEQ__NM_006440"
      minerva_ref_type__resource6 "HGNC__18155"
      minerva_ref_type__resource7 "ENTREZ__10587"
      minerva_ref_type__resource8 "ENTREZ__10587"
      minerva_ref_type__resource9 "UNIPROT__Q9NNW7"
      minerva_type "Protein"
      minerva_x 1095.0
      minerva_y 1453.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa393"
      species_meta_id "s_id_sa393"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 13
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 300.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "h_plus___mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa19"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa377"
      minerva_elementId2 "sa373"
      minerva_elementId3 "sa370"
      minerva_elementId4 "sa335"
      minerva_elementId5 "sa715"
      minerva_elementId6 "sa371"
      minerva_elementId7 "sa649"
      minerva_elementId8 "sa19"
      minerva_fullName "hydrogen(.)"
      minerva_name "H+"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:29235"
      minerva_ref_type__resource1 "CHEBI__CHEBI:29235"
      minerva_synonyms "H(.)"
      minerva_type "Ion"
      minerva_x 882.0
      minerva_x2 882.0
      minerva_x3 758.0
      minerva_x4 2158.0
      minerva_x5 570.5
      minerva_x6 478.0
      minerva_x7 476.75
      minerva_x8 1658.0
      minerva_y 993.0
      minerva_y2 1148.0
      minerva_y3 1128.0
      minerva_y4 323.0
      minerva_y5 420.5
      minerva_y6 1373.0
      minerva_y7 607.75
      minerva_y8 328.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa19"
      species_meta_id "s_id_sa19"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 14
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 400.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s1139__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa671"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa672"
      minerva_elementId2 "sa670"
      minerva_elementId3 "sa671"
      minerva_name "s1139"
      minerva_type "Degraded"
      minerva_x 968.0
      minerva_x2 963.0
      minerva_x3 943.0
      minerva_y 945.0
      minerva_y2 1002.0
      minerva_y3 973.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa671"
      species_meta_id "s_id_sa671"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 15
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 500.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "pi__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa32"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa32"
      minerva_fullName "phosphate(3-)"
      minerva_name "Pi"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18367"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18367"
      minerva_synonyms "Orthophosphate; PHOSPHATE ION; PO4(3-); Phosphate; [PO4](3-)"
      minerva_type "Simple molecule"
      minerva_x 2121.0
      minerva_y 563.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa32"
      species_meta_id "s_id_sa32"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 16
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 600.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "complex_space_4__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa35"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa35"
      minerva_fullName "respiratory chain complex IV"
      minerva_name "complex 4"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0045277"
      minerva_ref_type__resource1 "GO__GO:0045277"
      minerva_type "Complex"
      minerva_x 1997.0
      minerva_y 333.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa35"
      species_meta_id "s_id_csa35"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 17
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 700.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s1127__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa659"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa659"
      minerva_name "s1127"
      minerva_type "Degraded"
      minerva_x 237.0
      minerva_y 878.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa659"
      species_meta_id "s_id_sa659"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 18
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 800.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tca__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa500"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa500"
      minerva_name "TCA"
      minerva_type "Phenotype"
      minerva_x 1113.0
      minerva_y 333.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa500"
      species_meta_id "s_id_sa500"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 20
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 1000.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mt_space_transcription__mitochondrial_space_nucleoid__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa56"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa56"
      minerva_name "MT transcription"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/18391175"
      minerva_ref_type__resource1 "PUBMED__18391175"
      minerva_type "Complex"
      minerva_x 2133.0
      minerva_y 1188.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "mitochondrial_space_nucleoid"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa56"
      species_meta_id "s_id_csa56"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 21
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1100.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ndufaf7__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa669"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa669"
      minerva_former_symbols "C2orf56"
      minerva_name "NDUFAF7"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000003509"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_144736"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NDUFAF7"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NDUFAF7"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.1.1.320"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/28816"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/55471"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/Q7L592"
      minerva_ref_link8 "https://purl.uniprot.org/uniprot/Q7L592"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/55471"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000003509"
      minerva_ref_type__resource10 "REFSEQ__NM_144736"
      minerva_ref_type__resource2 "HGNC_SYMBOL__NDUFAF7"
      minerva_ref_type__resource3 "HGNC_SYMBOL__NDUFAF7"
      minerva_ref_type__resource4 "EC__2.1.1.320"
      minerva_ref_type__resource5 "HGNC__28816"
      minerva_ref_type__resource6 "ENTREZ__55471"
      minerva_ref_type__resource7 "UNIPROT__Q7L592"
      minerva_ref_type__resource8 "UNIPROT__Q7L592"
      minerva_ref_type__resource9 "ENTREZ__55471"
      minerva_type "Protein"
      minerva_x 1103.0
      minerva_y 1002.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa669"
      species_meta_id "s_id_sa669"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 22
    zlevel -1

    cluster [
      cluster "s_id_ca4"
    ]
    graphics [
      x 1200.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf9b__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa691"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa691"
      minerva_name "Orf9b"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/APO40587"
      minerva_ref_type__resource1 "NCBI_PROTEIN__APO40587"
      minerva_type "Protein"
      minerva_x 578.0
      minerva_y 2178.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca4"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa691"
      species_meta_id "s_id_sa691"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 23
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 200.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "paraquat_space_dication__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa50"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa50"
      minerva_name "paraquat dication"
      minerva_type "Complex"
      minerva_x 285.5
      minerva_y 1390.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa50"
      species_meta_id "s_id_csa50"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 24
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 300.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp8__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa494"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa494"
      minerva_name "Nsp8"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/YP_009742615"
      minerva_ref_type__resource1 "NCBI_PROTEIN__YP_009742615"
      minerva_type "Protein"
      minerva_x 1533.0
      minerva_y 1493.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa494"
      species_meta_id "s_id_sa494"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 25
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 400.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "fe2_plus___mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa375"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa367"
      minerva_elementId2 "sa375"
      minerva_elementId3 "sa365"
      minerva_fullName "iron(2+)"
      minerva_name "Fe2+"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:29033"
      minerva_ref_type__resource1 "CHEBI__CHEBI:29033"
      minerva_synonyms "FE (II) ION; Fe(2+); Fe(II); Fe2+; Ferrous ion; Iron(2+); iron ion(2+)"
      minerva_type "Ion"
      minerva_x 528.0
      minerva_x2 882.0
      minerva_x3 768.0
      minerva_y 1301.5
      minerva_y2 952.0
      minerva_y3 1301.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa375"
      species_meta_id "s_id_sa375"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 26
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 500.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp8_minus_affected_space_mt_space_ribosomal_space_proteins__mitochondrial_space_nucleoid__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa91"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa91"
      minerva_name "Nsp8-affected Mt ribosomal proteins"
      minerva_type "Complex"
      minerva_x 1737.625
      minerva_y 1569.125
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "mitochondrial_space_nucleoid"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa91"
      species_meta_id "s_id_csa91"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 27
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 600.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "paraquat__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa397"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa397"
      minerva_fullName "paraquat"
      minerva_name "paraquat"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:34905"
      minerva_ref_type__resource1 "CHEBI__CHEBI:34905"
      minerva_synonyms "1,1'-Dimethyl-4,4'-bipyridinium; 1,1'-dimethyl-4,4'-bipyridyldiylium; N,N'-dimethyl-4,4'-bipyridinium; N,N'-dimethyl-4,4'-bipyridinium dication; Paraquat; dimethyl viologen; methyl viologen ion(2+); paraquat dication; paraquat ion"
      minerva_type "Simple molecule"
      minerva_x 285.0
      minerva_y 1518.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa397"
      species_meta_id "s_id_sa397"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 28
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 700.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ecsit__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa667"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa667"
      minerva_name "ECSIT"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/Q9BQ95"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/Q9BQ95"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000130159"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/29548"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ECSIT"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_016581"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ECSIT"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/51295"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/51295"
      minerva_ref_type__resource1 "UNIPROT__Q9BQ95"
      minerva_ref_type__resource2 "UNIPROT__Q9BQ95"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000130159"
      minerva_ref_type__resource4 "HGNC__29548"
      minerva_ref_type__resource5 "HGNC_SYMBOL__ECSIT"
      minerva_ref_type__resource6 "REFSEQ__NM_016581"
      minerva_ref_type__resource7 "HGNC_SYMBOL__ECSIT"
      minerva_ref_type__resource8 "ENTREZ__51295"
      minerva_ref_type__resource9 "ENTREZ__51295"
      minerva_type "Protein"
      minerva_x 1103.0
      minerva_y 973.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa667"
      species_meta_id "s_id_sa667"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 29
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 800.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "precursor_space_protein_space_n_minus_terminus_space_binding__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa690"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa690"
      minerva_name "precursor protein N-terminus binding"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0047485"
      minerva_ref_type__resource1 "GO__GO:0047485"
      minerva_type "Protein"
      minerva_x 757.0
      minerva_y 1917.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa690"
      species_meta_id "s_id_sa690"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 30
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 900.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "qh_underscore_sub_underscore_2_underscore_endsub_underscore___mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa255"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa220"
      minerva_elementId2 "sa255"
      minerva_fullName "ubiquinol"
      minerva_name "QH_sub_2_endsub_"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17976"
      minerva_ref_type__resource1 "CHEBI__CHEBI:17976"
      minerva_synonyms "CoQH2; QH(2); QH2; Ubiquinol; a ubiquinol; coenzymes QH2; reduced ubiquinone; ubiquinols"
      minerva_type "Simple molecule"
      minerva_x 1076.0
      minerva_x2 1651.0
      minerva_y 293.0
      minerva_y2 283.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa255"
      species_meta_id "s_id_sa255"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 31
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1000.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "glutathione__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa381"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa381"
      minerva_fullName "glutathione"
      minerva_name "glutathione"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16856"
      minerva_ref_type__resource1 "CHEBI__CHEBI:16856"
      minerva_synonyms "5-L-Glutamyl-L-cysteinylglycine; GSH; Glutathione; Glutathione-SH; N-(N-gamma-L-Glutamyl-L-cysteinyl)glycine; Reduced glutathione; gamma-L-Glutamyl-L-cysteinyl-glycine"
      minerva_type "Simple molecule"
      minerva_x 630.0
      minerva_y 1428.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa381"
      species_meta_id "s_id_sa381"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 32
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1100.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "gpx1__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa378"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa378"
      minerva_name "GPX1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/2876"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/2876"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/4553"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000233276"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P07203"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=GPX1"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=GPX1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_000581"
      minerva_ref_link9 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=1.11.1.9"
      minerva_ref_type__resource1 "ENTREZ__2876"
      minerva_ref_type__resource2 "ENTREZ__2876"
      minerva_ref_type__resource3 "HGNC__4553"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000233276"
      minerva_ref_type__resource5 "UNIPROT__P07203"
      minerva_ref_type__resource6 "HGNC_SYMBOL__GPX1"
      minerva_ref_type__resource7 "HGNC_SYMBOL__GPX1"
      minerva_ref_type__resource8 "REFSEQ__NM_000581"
      minerva_ref_type__resource9 "EC__1.11.1.9"
      minerva_type "Protein"
      minerva_x 715.0
      minerva_y 1453.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa378"
      species_meta_id "s_id_sa378"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 33
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1200.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "txn2__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa392"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa391"
      minerva_elementId2 "sa392"
      minerva_name "TXN2"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_012473"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/17772"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/Q99757"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/25828"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/25828"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TXN2"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TXN2"
      minerva_ref_link8 "https://www.ensembl.org/id/ENSG00000100348"
      minerva_ref_type__resource1 "REFSEQ__NM_012473"
      minerva_ref_type__resource2 "HGNC__17772"
      minerva_ref_type__resource3 "UNIPROT__Q99757"
      minerva_ref_type__resource4 "ENTREZ__25828"
      minerva_ref_type__resource5 "ENTREZ__25828"
      minerva_ref_type__resource6 "HGNC_SYMBOL__TXN2"
      minerva_ref_type__resource7 "HGNC_SYMBOL__TXN2"
      minerva_ref_type__resource8 "ENSEMBL__ENSG00000100348"
      minerva_type "Protein"
      minerva_x 1015.0
      minerva_x2 1015.0
      minerva_y 1578.0
      minerva_y2 1333.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa392"
      species_meta_id "s_id_sa392"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 34
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 200.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp7__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa499"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa499"
      minerva_name "Nsp7"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/YP_009725303"
      minerva_ref_type__resource1 "NCBI_PROTEIN__YP_009725303"
      minerva_type "Protein"
      minerva_x 1017.0
      minerva_y 1061.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa499"
      species_meta_id "s_id_sa499"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 35
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 300.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ndufb9__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa661"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa661"
      minerva_name "NDUFB9"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4715"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/4715"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_005005"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000147684"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7704"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NDUFB9"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/Q9Y6M9"
      minerva_ref_link8 "https://purl.uniprot.org/uniprot/Q9Y6M9"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NDUFB9"
      minerva_ref_type__resource1 "ENTREZ__4715"
      minerva_ref_type__resource2 "ENTREZ__4715"
      minerva_ref_type__resource3 "REFSEQ__NM_005005"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000147684"
      minerva_ref_type__resource5 "HGNC__7704"
      minerva_ref_type__resource6 "HGNC_SYMBOL__NDUFB9"
      minerva_ref_type__resource7 "UNIPROT__Q9Y6M9"
      minerva_ref_type__resource8 "UNIPROT__Q9Y6M9"
      minerva_ref_type__resource9 "HGNC_SYMBOL__NDUFB9"
      minerva_type "Protein"
      minerva_x 383.0
      minerva_y 758.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa661"
      species_meta_id "s_id_sa661"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 36
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 400.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "atp5mg__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa710"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa710"
      minerva_former_symbols "ATP5L"
      minerva_name "ATP5MG"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/14247"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_006476"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP5MG"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATP5MG"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000167283"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/10632"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/10632"
      minerva_ref_link8 "https://purl.uniprot.org/uniprot/O75964"
      minerva_ref_link9 "https://purl.uniprot.org/uniprot/O75964"
      minerva_ref_type__resource1 "HGNC__14247"
      minerva_ref_type__resource2 "REFSEQ__NM_006476"
      minerva_ref_type__resource3 "HGNC_SYMBOL__ATP5MG"
      minerva_ref_type__resource4 "HGNC_SYMBOL__ATP5MG"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000167283"
      minerva_ref_type__resource6 "ENTREZ__10632"
      minerva_ref_type__resource7 "ENTREZ__10632"
      minerva_ref_type__resource8 "UNIPROT__O75964"
      minerva_ref_type__resource9 "UNIPROT__O75964"
      minerva_type "Protein"
      minerva_x 2393.0
      minerva_y 678.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa710"
      species_meta_id "s_id_sa710"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 37
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 500.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mt_space_ribosomal_space_proteins__mitochondrial_space_nucleoid__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa51"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa51"
      minerva_name "Mt ribosomal proteins"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/23149385"
      minerva_ref_type__resource1 "PUBMED__23149385"
      minerva_type "Complex"
      minerva_x 1980.5
      minerva_y 1438.75
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "mitochondrial_space_nucleoid"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa51"
      species_meta_id "s_id_csa51"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 38
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 600.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mt_minus_trna_space_synthetase__mitochondrial_space_nucleoid__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa53"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa53"
      minerva_name "Mt-tRNA synthetase"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/23149385"
      minerva_ref_type__resource1 "PUBMED__23149385"
      minerva_type "Complex"
      minerva_x 1914.0
      minerva_y 1312.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "mitochondrial_space_nucleoid"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa53"
      species_meta_id "s_id_csa53"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 39
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 700.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "h_underscore_sub_underscore_2_underscore_endsub_underscore_o__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa369"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa356"
      minerva_elementId2 "sa369"
      minerva_fullName "water"
      minerva_name "H_sub_2_endsub_O"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15377"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15377"
      minerva_synonyms "BOUND WATER; H2O; H2O; HOH; WATER; Wasser; Water; [OH2]; acqua; agua; aqua; dihydridooxygen; dihydrogen oxide; eau; hydrogen hydroxide"
      minerva_type "Simple molecule"
      minerva_x 820.0
      minerva_x2 743.0
      minerva_y 1573.0
      minerva_y2 1168.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa369"
      species_meta_id "s_id_sa369"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 40
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 800.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s1195__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa714"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa714"
      minerva_name "s1195"
      minerva_type "Degraded"
      minerva_x 1263.0
      minerva_y 1061.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa714"
      species_meta_id "s_id_sa714"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 41
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 900.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mt_space_trnas__mitochondrial_space_nucleoid__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa55"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa55"
      minerva_name "MT tRNAs"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/23149385"
      minerva_ref_type__resource1 "PUBMED__23149385"
      minerva_type "Complex"
      minerva_x 1755.0
      minerva_y 1343.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "mitochondrial_space_nucleoid"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa55"
      species_meta_id "s_id_csa55"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 42
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1000.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "acad9__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa668"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa668"
      minerva_name "ACAD9"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/28976"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ACAD9"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/28976"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_014049"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=1.3.8.-"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000177646"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/21497"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ACAD9"
      minerva_ref_link8 "https://purl.uniprot.org/uniprot/Q9H845"
      minerva_ref_link9 "https://purl.uniprot.org/uniprot/Q9H845"
      minerva_ref_type__resource1 "ENTREZ__28976"
      minerva_ref_type__resource10 "HGNC_SYMBOL__ACAD9"
      minerva_ref_type__resource2 "ENTREZ__28976"
      minerva_ref_type__resource3 "REFSEQ__NM_014049"
      minerva_ref_type__resource4 "EC__1.3.8.-"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000177646"
      minerva_ref_type__resource6 "HGNC__21497"
      minerva_ref_type__resource7 "HGNC_SYMBOL__ACAD9"
      minerva_ref_type__resource8 "UNIPROT__Q9H845"
      minerva_ref_type__resource9 "UNIPROT__Q9H845"
      minerva_type "Protein"
      minerva_x 1103.0
      minerva_y 945.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa668"
      species_meta_id "s_id_sa668"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 43
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "h_plus___mitochondrion__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa18"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa20"
      minerva_elementId2 "sa644"
      minerva_elementId3 "sa18"
      minerva_fullName "hydrogen(.)"
      minerva_name "H+"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:29235"
      minerva_ref_type__resource1 "CHEBI__CHEBI:29235"
      minerva_synonyms "H(.)"
      minerva_type "Ion"
      minerva_x 1803.0
      minerva_x2 1518.0
      minerva_x3 568.0
      minerva_y 178.0
      minerva_y2 178.0
      minerva_y3 178.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "mitochondrion"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa18"
      species_meta_id "s_id_sa18"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 44
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1200.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ndufa1__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa665"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa665"
      minerva_name "NDUFA1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_004541"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000125356"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/O15239"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/O15239"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NDUFA1"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NDUFA1"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4694"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4694"
      minerva_ref_link9 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7683"
      minerva_ref_type__resource1 "REFSEQ__NM_004541"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000125356"
      minerva_ref_type__resource3 "UNIPROT__O15239"
      minerva_ref_type__resource4 "UNIPROT__O15239"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NDUFA1"
      minerva_ref_type__resource6 "HGNC_SYMBOL__NDUFA1"
      minerva_ref_type__resource7 "ENTREZ__4694"
      minerva_ref_type__resource8 "ENTREZ__4694"
      minerva_ref_type__resource9 "HGNC__7683"
      minerva_type "Protein"
      minerva_x 383.0
      minerva_y 788.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa665"
      species_meta_id "s_id_sa665"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 45
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 200.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tim23_space_complex__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa94"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa94"
      minerva_fullName "TIM23 mitochondrial import inner membrane translocase complex"
      minerva_name "TIM23 complex"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0005744"
      minerva_ref_type__resource1 "GO__GO:0005744"
      minerva_type "Complex"
      minerva_x 571.0
      minerva_y 2015.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa94"
      species_meta_id "s_id_csa94"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 46
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 300.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mt_minus_dna_space_repair__mitochondrial_space_nucleoid__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa54"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa54"
      minerva_name "Mt-DNA repair"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/23149385"
      minerva_ref_type__resource1 "PUBMED__23149385"
      minerva_type "Complex"
      minerva_x 2138.0
      minerva_y 833.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "mitochondrial_space_nucleoid"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa54"
      species_meta_id "s_id_csa54"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 47
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 400.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s1196__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa716"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa716"
      minerva_name "s1196"
      minerva_type "Degraded"
      minerva_x 1518.0
      minerva_y 583.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa716"
      species_meta_id "s_id_sa716"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 48
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 500.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "trmt1__mitochondrial_space_nucleoid__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa692"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa692"
      minerva_name "TRMT1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/28752201"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.1.1.216"
      minerva_ref_link11 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/25980"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_017722"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRMT1"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/55621"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRMT1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/55621"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000104907"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q9NXH9"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q9NXH9"
      minerva_ref_type__resource1 "PUBMED__28752201"
      minerva_ref_type__resource10 "EC__2.1.1.216"
      minerva_ref_type__resource11 "HGNC__25980"
      minerva_ref_type__resource2 "REFSEQ__NM_017722"
      minerva_ref_type__resource3 "HGNC_SYMBOL__TRMT1"
      minerva_ref_type__resource4 "ENTREZ__55621"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRMT1"
      minerva_ref_type__resource6 "ENTREZ__55621"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000104907"
      minerva_ref_type__resource8 "UNIPROT__Q9NXH9"
      minerva_ref_type__resource9 "UNIPROT__Q9NXH9"
      minerva_type "Protein"
      minerva_x 1913.0
      minerva_y 1134.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "mitochondrial_space_nucleoid"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa692"
      species_meta_id "s_id_sa692"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 49
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 600.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nadph__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa395"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa382"
      minerva_elementId2 "sa395"
      minerva_fullName "NADPH"
      minerva_name "NADPH"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16474"
      minerva_ref_type__resource1 "CHEBI__CHEBI:16474"
      minerva_synonyms "NADPH; NADPH DIHYDRO-NICOTINAMIDE-ADENINE-DINUCLEOTIDE PHOSPHATE; Reduced nicotinamide adenine dinucleotide phosphate; TPNH; dihydronicotinamide-adenine dinucleotide phosphate; reduced nicotinamide-adenine dinucleotide phosphate"
      minerva_type "Simple molecule"
      minerva_x 703.0
      minerva_x2 1095.0
      minerva_y 1619.0
      minerva_y2 1528.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa395"
      species_meta_id "s_id_sa395"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 50
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 700.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "gpx4__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa379"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa379"
      minerva_name "GPX4"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000167468"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/2879"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/2879"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/4556"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=GPX4"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=GPX4"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_002085"
      minerva_ref_link8 "https://purl.uniprot.org/uniprot/P36969"
      minerva_ref_link9 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=1.11.1.12"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000167468"
      minerva_ref_type__resource2 "ENTREZ__2879"
      minerva_ref_type__resource3 "ENTREZ__2879"
      minerva_ref_type__resource4 "HGNC__4556"
      minerva_ref_type__resource5 "HGNC_SYMBOL__GPX4"
      minerva_ref_type__resource6 "HGNC_SYMBOL__GPX4"
      minerva_ref_type__resource7 "REFSEQ__NM_002085"
      minerva_ref_type__resource8 "UNIPROT__P36969"
      minerva_ref_type__resource9 "EC__1.11.1.12"
      minerva_type "Protein"
      minerva_x 715.0
      minerva_y 1500.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa379"
      species_meta_id "s_id_sa379"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 51
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 800.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cyt_space_c__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa13"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa13"
      minerva_name "Cyt C"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/P99999"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/54205"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CYCS"
      minerva_ref_type__resource1 "UNIPROT__P99999"
      minerva_ref_type__resource2 "ENTREZ__54205"
      minerva_ref_type__resource3 "HGNC_SYMBOL__CYCS"
      minerva_type "Protein"
      minerva_x 1711.0
      minerva_y 283.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa13"
      species_meta_id "s_id_sa13"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 52
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 900.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mt_minus_dntp_space_pool__mitochondrial_space_nucleoid__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa52"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa52"
      minerva_name "Mt-dNTP pool"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/23149385"
      minerva_ref_type__resource1 "PUBMED__23149385"
      minerva_type "Complex"
      minerva_x 1713.0
      minerva_y 793.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "mitochondrial_space_nucleoid"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa52"
      species_meta_id "s_id_csa52"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 53
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1000.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cat__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa385"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa385"
      minerva_name "CAT"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_001752"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/847"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/847"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=1.11.1.6"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000121691"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CAT"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CAT"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1516"
      minerva_ref_link9 "https://purl.uniprot.org/uniprot/P04040"
      minerva_ref_type__resource1 "REFSEQ__NM_001752"
      minerva_ref_type__resource2 "ENTREZ__847"
      minerva_ref_type__resource3 "ENTREZ__847"
      minerva_ref_type__resource4 "EC__1.11.1.6"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000121691"
      minerva_ref_type__resource6 "HGNC_SYMBOL__CAT"
      minerva_ref_type__resource7 "HGNC_SYMBOL__CAT"
      minerva_ref_type__resource8 "HGNC__1516"
      minerva_ref_type__resource9 "UNIPROT__P04040"
      minerva_type "Protein"
      minerva_x 745.0
      minerva_y 1393.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa385"
      species_meta_id "s_id_sa385"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 54
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 1100.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "damaged_space_mt_space_dna__mitochondrial_space_nucleoid__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa653"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa653"
      minerva_name "damaged mt DNA"
      minerva_type "Gene"
      minerva_x 1993.0
      minerva_y 768.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "mitochondrial_space_nucleoid"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa653"
      species_meta_id "s_id_sa653"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 55
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1200.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf9c__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa498"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa498"
      minerva_name "Orf9c"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/AIA62288"
      minerva_ref_type__resource1 "NCBI_PROTEIN__AIA62288"
      minerva_type "Protein"
      minerva_x 743.0
      minerva_y 858.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa498"
      species_meta_id "s_id_sa498"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 56
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 200.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sod1__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa372"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa372"
      minerva_former_symbols "ALS; ALS1"
      minerva_name "SOD1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_000454"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P00441"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000142168"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11179"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SOD1"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SOD1"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/6647"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/6647"
      minerva_ref_link9 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=1.15.1.1"
      minerva_ref_type__resource1 "REFSEQ__NM_000454"
      minerva_ref_type__resource2 "UNIPROT__P00441"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000142168"
      minerva_ref_type__resource4 "HGNC__11179"
      minerva_ref_type__resource5 "HGNC_SYMBOL__SOD1"
      minerva_ref_type__resource6 "HGNC_SYMBOL__SOD1"
      minerva_ref_type__resource7 "ENTREZ__6647"
      minerva_ref_type__resource8 "ENTREZ__6647"
      minerva_ref_type__resource9 "EC__1.15.1.1"
      minerva_type "Protein"
      minerva_x 983.0
      minerva_y 1221.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa372"
      species_meta_id "s_id_sa372"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 57
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 300.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mt_space_translation__mitochondrial_space_nucleoid__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa58"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa58"
      minerva_name "Mt translation"
      minerva_ref_link1 "https://doi.org/10.1155/2010/737385"
      minerva_ref_type__resource1 "DOI__10.1155/2010/737385"
      minerva_type "Complex"
      minerva_x 2215.0
      minerva_y 1548.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "mitochondrial_space_nucleoid"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa58"
      species_meta_id "s_id_csa58"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 58
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 400.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cyt_space_c__mitochondrial_space_matrix__reduced__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa253"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa253"
      minerva_name "Cyt C"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/P99999"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/54205"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CYCS"
      minerva_ref_type__resource1 "UNIPROT__P99999"
      minerva_ref_type__resource2 "ENTREZ__54205"
      minerva_ref_type__resource3 "HGNC_SYMBOL__CYCS"
      minerva_structuralState "reduced"
      minerva_type "Protein"
      minerva_x 1838.0
      minerva_y 283.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa253"
      species_meta_id "s_id_sa253"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 59
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 500.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "fe3_plus___mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa376"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa368"
      minerva_elementId2 "sa366"
      minerva_elementId3 "sa376"
      minerva_fullName "iron(3+)"
      minerva_name "Fe3+"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:29034"
      minerva_ref_type__resource1 "CHEBI__CHEBI:29034"
      minerva_synonyms "FE (III) ION; Fe(3+); Fe(3+); Fe(III); Fe3+; Ferric ion; Iron(3+); ferric iron; iron, ion (Fe(3+))"
      minerva_type "Ion"
      minerva_x 623.0
      minerva_x2 683.0
      minerva_x3 882.0
      minerva_y 1301.5
      minerva_y2 1301.5
      minerva_y3 1085.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa376"
      species_meta_id "s_id_sa376"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 60
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 600.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "gsr__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa384"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa384"
      minerva_name "GSR"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000104687"
      minerva_ref_link2 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=1.8.1.7"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/4623"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/2936"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/2936"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000637"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=GSR"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=GSR"
      minerva_ref_link9 "https://purl.uniprot.org/uniprot/P00390"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000104687"
      minerva_ref_type__resource2 "EC__1.8.1.7"
      minerva_ref_type__resource3 "HGNC__4623"
      minerva_ref_type__resource4 "ENTREZ__2936"
      minerva_ref_type__resource5 "ENTREZ__2936"
      minerva_ref_type__resource6 "REFSEQ__NM_000637"
      minerva_ref_type__resource7 "HGNC_SYMBOL__GSR"
      minerva_ref_type__resource8 "HGNC_SYMBOL__GSR"
      minerva_ref_type__resource9 "UNIPROT__P00390"
      minerva_type "Protein"
      minerva_x 543.0
      minerva_y 1603.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa384"
      species_meta_id "s_id_sa384"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 61
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 700.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "hydroxide__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa364"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa364"
      minerva_fullName "hydroxide"
      minerva_name "hydroxide"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16234"
      minerva_ref_type__resource1 "CHEBI__CHEBI:16234"
      minerva_synonyms "HO-; HYDROXIDE ION; Hydroxide ion; OH(-); OH-"
      minerva_type "Simple molecule"
      minerva_x 520.0
      minerva_y 1223.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa364"
      species_meta_id "s_id_sa364"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 62
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 800.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp5__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa693"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa693"
      minerva_name "Nsp5"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/YP_009742612"
      minerva_ref_type__resource1 "NCBI_PROTEIN__YP_009742612"
      minerva_type "Protein"
      minerva_x 1533.0
      minerva_y 1193.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa693"
      species_meta_id "s_id_sa693"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 63
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 900.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nad_plus___mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa3"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa3"
      minerva_fullName "NAD(+)"
      minerva_name "NAD+"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15846"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15846"
      minerva_synonyms "DPN; Diphosphopyridine nucleotide; NAD; NAD+; Nadide; Nicotinamide adenine dinucleotide; beta-NAD"
      minerva_type "Simple molecule"
      minerva_x 482.0
      minerva_y 653.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa3"
      species_meta_id "s_id_sa3"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 64
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1000.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp4__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa695"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa695"
      minerva_name "Nsp4"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/YP_009742611"
      minerva_ref_type__resource1 "NCBI_PROTEIN__YP_009742611"
      minerva_type "Protein"
      minerva_x 928.0
      minerva_y 1848.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa695"
      species_meta_id "s_id_sa695"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 65
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1100.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "atp__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa33"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa33"
      minerva_fullName "ATP"
      minerva_name "ATP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15422"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15422"
      minerva_synonyms "ADENOSINE-5'-TRIPHOSPHATE; ATP; Adenosine 5'-triphosphate; Adenosine triphosphate; Adephos; Adetol; Adynol; Atipi; Atriphos; Cardenosine; Fosfobion; Glucobasin; H4atp; Myotriphos; Triadenyl; Triphosphaden"
      minerva_type "Simple molecule"
      minerva_x 2216.0
      minerva_y 523.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa33"
      species_meta_id "s_id_sa33"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 66
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 1200.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mt_space_dna_space_replication__mitochondrial_space_nucleoid__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa463"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa463"
      minerva_name "mt DNA replication"
      minerva_type "Phenotype"
      minerva_x 1873.0
      minerva_y 1033.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "mitochondrial_space_nucleoid"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa463"
      species_meta_id "s_id_sa463"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 67
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 200.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "adp__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa30"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa30"
      minerva_fullName "ADP"
      minerva_name "ADP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "CHEBI__CHEBI:16761"
      minerva_synonyms "5'-adenylphosphoric acid; ADENOSINE-5'-DIPHOSPHATE; ADP; Adenosine 5'-diphosphate; H3adp"
      minerva_type "Simple molecule"
      minerva_x 2095.0
      minerva_y 523.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa30"
      species_meta_id "s_id_sa30"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 68
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 300.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tim9_minus_tim10_space_complex__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa96"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa96"
      minerva_fullName "mitochondrial intermembrane space protein transporter complex"
      minerva_name "TIM9-TIM10 complex"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0042719"
      minerva_ref_type__resource1 "GO__GO:0042719"
      minerva_type "Complex"
      minerva_x 1033.0
      minerva_y 1983.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa96"
      species_meta_id "s_id_csa96"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 69
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 400.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ros__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa362"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa362"
      minerva_fullName "reactive oxygen species"
      minerva_name "ROS"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:26523"
      minerva_ref_type__resource1 "CHEBI__CHEBI:26523"
      minerva_synonyms "ROS"
      minerva_type "Simple molecule"
      minerva_x 425.0
      minerva_y 1108.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa362"
      species_meta_id "s_id_sa362"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 70
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 500.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s1185__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa706"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa706"
      minerva_name "s1185"
      minerva_type "Degraded"
      minerva_x 403.0
      minerva_y 1757.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa706"
      species_meta_id "s_id_sa706"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 71
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 600.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nadp(_plus_)__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa383"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa394"
      minerva_elementId2 "sa383"
      minerva_fullName "NADP(+)"
      minerva_name "NADP(+)"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18009"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18009"
      minerva_synonyms "NADP; NADP+; Nicotinamide adenine dinucleotide phosphate; TPN; Triphosphopyridine nucleotide; beta-Nicotinamide adenine dinucleotide phosphate; beta-nicotinamide adenine dinucleotide phosphate; oxidized nicotinamide-adenine dinucleotide phosphate"
      minerva_type "Simple molecule"
      minerva_x 1095.0
      minerva_x2 703.0
      minerva_y 1368.0
      minerva_y2 1583.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa383"
      species_meta_id "s_id_sa383"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 72
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 700.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp8_minus_affected_space_mt_space_ribosomal_space_proteins__mitochondrial_space_nucleoid__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa90"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa90"
      minerva_name "Nsp8-affected Mt ribosomal proteins"
      minerva_state1 "active"
      minerva_type "Complex"
      minerva_x 1985.0
      minerva_y 1569.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "mitochondrial_space_nucleoid"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa90"
      species_meta_id "s_id_csa90"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 73
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 800.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s1132__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa664"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa664"
      minerva_name "s1132"
      minerva_type "Degraded"
      minerva_x 518.0
      minerva_y 757.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa664"
      species_meta_id "s_id_sa664"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 74
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 900.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mt_space_dna__mitochondrial_space_nucleoid__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa652"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa652"
      minerva_name "mt DNA"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0000262"
      minerva_ref_type__resource1 "GO__GO:0000262"
      minerva_type "Gene"
      minerva_x 1993.0
      minerva_y 953.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "mitochondrial_space_nucleoid"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa652"
      species_meta_id "s_id_sa652"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 75
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 1000.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s1119__mitochondrial_space_nucleoid__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa655"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa655"
      minerva_name "s1119"
      minerva_type "Degraded"
      minerva_x 1810.0
      minerva_y 953.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "mitochondrial_space_nucleoid"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa655"
      species_meta_id "s_id_sa655"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 77
    zlevel -1

    cluster [
      cluster "s_id_ca4"
    ]
    graphics [
      x 1200.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "precursor_space_protein_space_n_minus_terminus_space_binding__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa688"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa688"
      minerva_name "precursor protein N-terminus binding"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0047485"
      minerva_ref_type__resource1 "GO__GO:0047485"
      minerva_type "Protein"
      minerva_x 758.0
      minerva_y 2208.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca4"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa688"
      species_meta_id "s_id_sa688"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 78
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 200.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tim9_minus_tim10_space_complex__mitochondrial_space_matrix__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa95"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa95"
      minerva_fullName "mitochondrial intermembrane space protein transporter complex"
      minerva_name "TIM9-TIM10 complex"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0042719"
      minerva_ref_type__resource1 "GO__GO:0042719"
      minerva_state1 "active"
      minerva_type "Complex"
      minerva_x 823.0
      minerva_y 1983.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa95"
      species_meta_id "s_id_csa95"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 80
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 400.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mt_space_dna_space_damage__mitochondrial_space_nucleoid__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa461"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa461"
      minerva_name "mt DNA damage"
      minerva_type "Phenotype"
      minerva_x 1873.0
      minerva_y 860.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "mitochondrial_space_nucleoid"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa461"
      species_meta_id "s_id_sa461"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 81
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 500.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mt_space_replication__mitochondrial_space_nucleoid__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa57"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa57"
      minerva_fullName "mitochondrial DNA replication"
      minerva_name "Mt replication"
      minerva_ref_link1 "https://doi.org/10.1155/2010/737385"
      minerva_ref_link2 "https://doi.org/10.1042/EBC20170103"
      minerva_ref_link3 "http://amigo.geneontology.org/amigo/term/GO:0006264"
      minerva_ref_type__resource1 "DOI__10.1155/2010/737385"
      minerva_ref_type__resource2 "DOI__10.1042/EBC20170103"
      minerva_ref_type__resource3 "GO__GO:0006264"
      minerva_type "Complex"
      minerva_x 1678.0
      minerva_y 1033.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "mitochondrial_space_nucleoid"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa57"
      species_meta_id "s_id_csa57"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 82
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 600.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "complex_space_1__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa20"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa20"
      minerva_fullName "respiratory chain complex I"
      minerva_name "Complex 1"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0045271"
      minerva_ref_link2 "https://doi.org/10.1016/j.bbabio.2011.08.010"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/pubmed/19355884"
      minerva_ref_type__resource1 "GO__GO:0045271"
      minerva_ref_type__resource2 "DOI__10.1016/j.bbabio.2011.08.010"
      minerva_ref_type__resource3 "PUBMED__19355884"
      minerva_type "Complex"
      minerva_x 331.0
      minerva_y 413.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa20"
      species_meta_id "s_id_csa20"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 83
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 700.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp6__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa713"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa713"
      minerva_name "Nsp6"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/YP_009742613"
      minerva_ref_type__resource1 "NCBI_PROTEIN__YP_009742613"
      minerva_type "Protein"
      minerva_x 2263.0
      minerva_y 591.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa713"
      species_meta_id "s_id_sa713"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 85
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 900.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tim22_space_complex__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa93"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa93"
      minerva_fullName "TIM22 mitochondrial import inner membrane insertion complex"
      minerva_name "TIM22 complex"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0042721"
      minerva_ref_type__resource1 "GO__GO:0042721"
      minerva_type "Complex"
      minerva_x 400.5
      minerva_y 1977.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa93"
      species_meta_id "s_id_csa93"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 86
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1000.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "oxphos_space_factors__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa44"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa44"
      minerva_name "OXPHOS factors"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/23149385"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/pubmed/30030361"
      minerva_ref_type__resource1 "PUBMED__23149385"
      minerva_ref_type__resource2 "PUBMED__30030361"
      minerva_type "Complex"
      minerva_x 1263.0
      minerva_y 723.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa44"
      species_meta_id "s_id_csa44"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 87
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 1100.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tfam__mitochondrial_space_nucleoid__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa460"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa460"
      minerva_former_symbols "TCF6; TCF6L2"
      minerva_name "TFAM"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_003201"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11741"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TFAM"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TFAM"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/Q00059"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000108064"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/7019"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/7019"
      minerva_ref_type__resource1 "REFSEQ__NM_003201"
      minerva_ref_type__resource2 "HGNC__11741"
      minerva_ref_type__resource3 "HGNC_SYMBOL__TFAM"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TFAM"
      minerva_ref_type__resource5 "UNIPROT__Q00059"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000108064"
      minerva_ref_type__resource7 "ENTREZ__7019"
      minerva_ref_type__resource8 "ENTREZ__7019"
      minerva_type "Protein"
      minerva_x 2078.0
      minerva_y 1033.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "mitochondrial_space_nucleoid"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa460"
      species_meta_id "s_id_sa460"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 88
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 1200.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s1167__mitochondrial_space_nucleoid__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa694"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa694"
      minerva_name "s1167"
      minerva_type "Degraded"
      minerva_x 1773.0
      minerva_y 1133.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "mitochondrial_space_nucleoid"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa694"
      species_meta_id "s_id_sa694"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 89
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 200.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mt_space_mrna__mitochondrial_space_nucleoid__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa459"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa459"
      minerva_name "mt mRNA"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:33699"
      minerva_ref_type__resource1 "CHEBI__CHEBI:33699"
      minerva_type "RNA"
      minerva_x 2088.0
      minerva_y 1298.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "mitochondrial_space_nucleoid"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa459"
      species_meta_id "s_id_sa459"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 90
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 300.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s1131__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa663"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa663"
      minerva_name "s1131"
      minerva_type "Degraded"
      minerva_x 493.0
      minerva_y 788.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa663"
      species_meta_id "s_id_sa663"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 91
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 400.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s1197__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa717"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa717"
      minerva_name "s1197"
      minerva_type "Degraded"
      minerva_x 1997.166666666667
      minerva_y 583.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa717"
      species_meta_id "s_id_sa717"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 92
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 500.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "h_underscore_sub_underscore_2_underscore_endsub_underscore_o_underscore_sub_underscore_2_underscore_endsub_underscore___mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa355"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa355"
      minerva_fullName "hydrogen peroxide"
      minerva_name "H_sub_2_endsub_O_sub_2_endsub_"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16240"
      minerva_ref_type__resource1 "CHEBI__CHEBI:16240"
      minerva_synonyms "H2O2; H2O2; H2O2; HOOH; HYDROGEN PEROXIDE; Hydrogen peroxide; Oxydol; [OH(OH)]; dihydrogen dioxide; perhydrol"
      minerva_type "Simple molecule"
      minerva_x 820.0
      minerva_y 1338.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa355"
      species_meta_id "s_id_sa355"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 93
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 600.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sod2__mitochondrial_space_matrix__acetylated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa357"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa357"
      minerva_name "SOD2"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000112096"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11180"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/6648"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/6648"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P04179"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000636"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SOD2"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SOD2"
      minerva_ref_link9 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=1.15.1.1"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000112096"
      minerva_ref_type__resource2 "HGNC__11180"
      minerva_ref_type__resource3 "ENTREZ__6648"
      minerva_ref_type__resource4 "ENTREZ__6648"
      minerva_ref_type__resource5 "UNIPROT__P04179"
      minerva_ref_type__resource6 "REFSEQ__NM_000636"
      minerva_ref_type__resource7 "HGNC_SYMBOL__SOD2"
      minerva_ref_type__resource8 "HGNC_SYMBOL__SOD2"
      minerva_ref_type__resource9 "EC__1.15.1.1"
      minerva_state1 "ACETYLATED"
      minerva_type "Protein"
      minerva_x 545.0
      minerva_y 1553.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa357"
      species_meta_id "s_id_sa357"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 94
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tom_space_complex__mitochondrion__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa92"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa92"
      minerva_fullName "mitochondrial outer membrane translocase complex"
      minerva_name "TOM complex"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0005742"
      minerva_ref_type__resource1 "GO__GO:0005742"
      minerva_type "Complex"
      minerva_x 798.0
      minerva_y 2128.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "mitochondrion"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa92"
      species_meta_id "s_id_csa92"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 95
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 800.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nadh__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2"
      minerva_fullName "NADH"
      minerva_name "NADH"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16908"
      minerva_ref_type__resource1 "CHEBI__CHEBI:16908"
      minerva_synonyms "1,4-DIHYDRONICOTINAMIDE ADENINE DINUCLEOTIDE; DPNH; NADH; Reduced nicotinamide adenine dinucleotide; nicotinamide adenine dinucleotide (reduced)"
      minerva_type "Simple molecule"
      minerva_x 359.0
      minerva_y 652.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2"
      species_meta_id "s_id_sa2"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 97
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1000.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "complex_space_3__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa34"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa34"
      minerva_fullName "respiratory chain complex III"
      minerva_name "complex 3"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0045275"
      minerva_ref_type__resource1 "GO__GO:0045275"
      minerva_type "Complex"
      minerva_x 1519.0
      minerva_y 339.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa34"
      species_meta_id "s_id_csa34"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 98
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1100.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sod2__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa358"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa358"
      minerva_name "SOD2"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000112096"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11180"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/6648"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/6648"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P04179"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000636"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SOD2"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SOD2"
      minerva_ref_link9 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=1.15.1.1"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000112096"
      minerva_ref_type__resource2 "HGNC__11180"
      minerva_ref_type__resource3 "ENTREZ__6648"
      minerva_ref_type__resource4 "ENTREZ__6648"
      minerva_ref_type__resource5 "UNIPROT__P04179"
      minerva_ref_type__resource6 "REFSEQ__NM_000636"
      minerva_ref_type__resource7 "HGNC_SYMBOL__SOD2"
      minerva_ref_type__resource8 "HGNC_SYMBOL__SOD2"
      minerva_ref_type__resource9 "EC__1.15.1.1"
      minerva_type "Protein"
      minerva_x 545.0
      minerva_y 1423.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa358"
      species_meta_id "s_id_sa358"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 99
    zlevel -1

    graphics [
      x 1200.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re139"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re139"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "Transport"
      minerva_x 1132.5
      minerva_y 512.9999999999998
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re139"
      reaction_meta_id "re139"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 103
    zlevel -1

    graphics [
      x 200.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re135"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re135"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "Transport"
      minerva_x 1950.5000000000005
      minerva_y 142.9999999999993
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re135"
      reaction_meta_id "re135"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 106
    zlevel -1

    graphics [
      x 300.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re138"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re138"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 422.4990698600228
      minerva_y 652.516252600488
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re138"
      reaction_meta_id "re138"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 112
    zlevel -1

    graphics [
      x 400.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re69"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 488.6678918189475
      minerva_y 1112.7835429045635
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re69"
      reaction_meta_id "re69"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 115
    zlevel -1

    graphics [
      x 500.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re136"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re136"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "Transport"
      minerva_x 1475.4999999999995
      minerva_y 143.00000000000125
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re136"
      reaction_meta_id "re136"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 118
    zlevel -1

    graphics [
      x 600.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re85"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "Physical stimulation"
      minerva_x 1983.0
      minerva_y 1033.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re85"
      reaction_meta_id "re85"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 121
    zlevel -1

    graphics [
      x 700.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re132"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "Transport"
      minerva_x 2093.0000000000005
      minerva_y 143.00000000000006
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re132"
      reaction_meta_id "re132"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 124
    zlevel -1

    graphics [
      x 800.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re65"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 400.12563132923543
      minerva_y 1221.1694173824158
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re65"
      reaction_meta_id "re65"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 127
    zlevel -1

    graphics [
      x 900.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re63"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 546.1678918189475
      minerva_y 1342.8917714522818
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re63"
      reaction_meta_id "re63"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 132
    zlevel -1

    graphics [
      x 1000.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re16"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "Heterodimer association"
      minerva_x 2161.5
      minerva_y 523.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re16"
      reaction_meta_id "re16"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 138
    zlevel -1

    graphics [
      x 1100.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re159"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re159"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "Transport"
      minerva_x 705.0
      minerva_y 2062.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re159"
      reaction_meta_id "re159"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 146
    zlevel -1

    graphics [
      x 1200.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re38"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 1114.0
      minerva_y 276.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re38"
      reaction_meta_id "re38"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 150
    zlevel -1

    graphics [
      x 200.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re92"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "Modulation"
      minerva_x 1836.0211179604787
      minerva_y 998.3700532170673
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re92"
      reaction_meta_id "re92"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 153
    zlevel -1

    graphics [
      x 300.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re91"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "Transcription"
      minerva_x 1915.6880811857366
      minerva_y 1180.511581486514
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re91"
      reaction_meta_id "re91"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 158
    zlevel -1

    graphics [
      x 400.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re161"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re161"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 1831.9998979630882
      minerva_y 1133.4214278425943
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re161"
      reaction_meta_id "re161"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 162
    zlevel -1

    graphics [
      x 500.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re93"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "Translation"
      minerva_x 2089.8124971055363
      minerva_y 1548.0006093612535
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re93"
      reaction_meta_id "re93"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 168
    zlevel -1

    graphics [
      x 600.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re75"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 630.0000000000007
      minerva_y 1603.0000000000005
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re75"
      reaction_meta_id "re75"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 174
    zlevel -1

    graphics [
      x 700.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re60"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 844.8743686707527
      minerva_y 1452.9999999999975
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re60"
      reaction_meta_id "re60"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 180
    zlevel -1

    graphics [
      x 800.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re43"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 1518.5278254136842
      minerva_y 507.1250967828767
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re43"
      reaction_meta_id "re43"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 185
    zlevel -1

    graphics [
      x 900.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re154"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re154"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 454.0
      minerva_y 788.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re154"
      reaction_meta_id "re154"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 189
    zlevel -1

    graphics [
      x 1000.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re147"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re147"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 1860.0
      minerva_y 1569.0625
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re147"
      reaction_meta_id "re147"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 193
    zlevel -1

    graphics [
      x 1100.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re56"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 833.3939201327781
      minerva_y 1220.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re56"
      reaction_meta_id "re56"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 198
    zlevel -1

    graphics [
      x 1200.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re88"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "Transcription"
      minerva_x 1993.0
      minerva_y 1132.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re88"
      reaction_meta_id "re88"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 204
    zlevel -1

    graphics [
      x 200.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re41"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "Transport"
      minerva_x 569.1675873934379
      minerva_y 299.2504528028501
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re41"
      reaction_meta_id "re41"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 208
    zlevel -1

    graphics [
      x 300.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re105"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 238.04277558589726
      minerva_y 724.3750921453507
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re105"
      reaction_meta_id "re105"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 215
    zlevel -1

    graphics [
      x 400.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re73"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 806.6060798672219
      minerva_y 1021.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re73"
      reaction_meta_id "re73"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 218
    zlevel -1

    graphics [
      x 500.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re164"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re164"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 2353.0
      minerva_y 591.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re164"
      reaction_meta_id "re164"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 223
    zlevel -1

    graphics [
      x 600.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re72"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 833.3939201327781
      minerva_y 1021.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re72"
      reaction_meta_id "re72"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 229
    zlevel -1

    graphics [
      x 700.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re157"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re157"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 1007.0
      minerva_y 973.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re157"
      reaction_meta_id "re157"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 233
    zlevel -1

    graphics [
      x 800.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re58"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 805.8030399336109
      minerva_y 1198.7742470781955
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re58"
      reaction_meta_id "re58"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 239
    zlevel -1

    graphics [
      x 900.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re163"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re163"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 401.69220256632536
      minerva_y 1847.4995824102848
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re163"
      reaction_meta_id "re163"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 244
    zlevel -1

    graphics [
      x 1000.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re67"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 730.3756313292354
      minerva_y 1268.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re67"
      reaction_meta_id "re67"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 250
    zlevel -1

    graphics [
      x 1100.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re66"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 621.1679162564853
      minerva_y 1103.1186734543553
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re66"
      reaction_meta_id "re66"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 253
    zlevel -1

    graphics [
      x 1200.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re155"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re155"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 1263.0
      minerva_y 976.25
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re155"
      reaction_meta_id "re155"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 259
    zlevel -1

    graphics [
      x 200.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re162"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re162"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 928.0
      minerva_y 1970.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re162"
      reaction_meta_id "re162"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 263
    zlevel -1

    graphics [
      x 300.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re77"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 307.37714868049636
      minerva_y 1463.0918899835337
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re77"
      reaction_meta_id "re77"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 268
    zlevel -1

    graphics [
      x 400.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re76"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 1015.0
      minerva_y 1455.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re76"
      reaction_meta_id "re76"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 274
    zlevel -1

    graphics [
      x 500.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re134"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re134"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "Transport"
      minerva_x 1518.6234400675091
      minerva_y 215.87398410117885
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re134"
      reaction_meta_id "re134"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 277
    zlevel -1

    graphics [
      x 600.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re89"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "Trigger"
      minerva_x 1771.0
      minerva_y 1033.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re89"
      reaction_meta_id "re89"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 280
    zlevel -1

    graphics [
      x 700.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re78"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 263.12285131950364
      minerva_y 1463.0918899835337
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re78"
      reaction_meta_id "re78"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 285
    zlevel -1

    graphics [
      x 800.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re70"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 572.25
      minerva_y 1268.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re70"
      reaction_meta_id "re70"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 290
    zlevel -1

    graphics [
      x 900.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re40"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 1114.0
      minerva_y 401.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re40"
      reaction_meta_id "re40"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 296
    zlevel -1

    graphics [
      x 1000.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re71"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 618.7064768518171
      minerva_y 1333.2164570954365
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re71"
      reaction_meta_id "re71"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 299
    zlevel -1

    graphics [
      x 1100.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re120"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 820.0000000000001
      minerva_y 1433.000000000001
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re120"
      reaction_meta_id "re120"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 303
    zlevel -1

    graphics [
      x 1200.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re87"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 1872.25
      minerva_y 953.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re87"
      reaction_meta_id "re87"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 308
    zlevel -1

    graphics [
      x 200.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re141"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re141"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 1959.75
      minerva_y 860.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re141"
      reaction_meta_id "re141"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 312
    zlevel -1

    graphics [
      x 300.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re156"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re156"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 1019.5
      minerva_y 945.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re156"
      reaction_meta_id "re156"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 316
    zlevel -1

    graphics [
      x 400.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re74"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 795.11839723497
      minerva_y 1452.9999999999955
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re74"
      reaction_meta_id "re74"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 323
    zlevel -1

    graphics [
      x 500.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re44"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 1997.0792929313543
      minerva_y 496.50000204060655
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re44"
      reaction_meta_id "re44"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 328
    zlevel -1

    graphics [
      x 600.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re34"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 818.5
      minerva_y 339.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re34"
      reaction_meta_id "re34"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 333
    zlevel -1

    graphics [
      x 700.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re133"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re133"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "Transport"
      minerva_x 2227.5
      minerva_y 303.0000000000002
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re133"
      reaction_meta_id "re133"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 336
    zlevel -1

    graphics [
      x 800.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re158"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re158"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 1017.0
      minerva_y 1002.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re158"
      reaction_meta_id "re158"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 340
    zlevel -1

    graphics [
      x 900.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re9"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 1737.25
      minerva_y 363.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re9"
      reaction_meta_id "re9"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 350
    zlevel -1

    graphics [
      x 1000.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re152"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re152"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 466.50022158823384
      minerva_y 757.5421029306501
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re152"
      reaction_meta_id "re152"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 354
    zlevel -1

    graphics [
      x 1100.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re64"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 400.0
      minerva_y 1428.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re64"
      reaction_meta_id "re64"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 357
    zlevel -1

    graphics [
      x 1200.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re61"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 545.0
      minerva_y 1488.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re61"
      reaction_meta_id "re61"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 361
    zlevel -1

    graphics [
      x 200.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re142"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re142"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "State transition"
      minerva_x 2026.25
      minerva_y 860.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re142"
      reaction_meta_id "re142"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 367
    zlevel -1

    graphics [
      x 300.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re140"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re140"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=7.1.1.2"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P03901"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/4539"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000212907"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MT-ND4L"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7460"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/YP_003024034"
      minerva_ref_type__resource1 "ENTREZ__4539"
      minerva_ref_type__resource10 "EC__7.1.1.2"
      minerva_ref_type__resource2 "UNIPROT__P03901"
      minerva_ref_type__resource3 "UNIPROT__P03901"
      minerva_ref_type__resource4 "ENTREZ__4539"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000212907"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MT-ND4L"
      minerva_ref_type__resource8 "HGNC__7460"
      minerva_ref_type__resource9 "REFSEQ__YP_003024034"
      minerva_type "Transport"
      minerva_x 1750.5000000000055
      minerva_y 455.00000000000114
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re140"
      reaction_meta_id "re140"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 371
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 666.6666666666666
      y 833.3333333333334
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "complex_space_2__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa84"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "true"
      minerva_elementId "csa84"
      minerva_fullName "respiratory chain complex II"
      minerva_name "complex 2"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0045273"
      minerva_ref_link2 "https://doi.org/10.1021/bi901627u"
      minerva_ref_type__resource1 "GO__GO:0045273"
      minerva_ref_type__resource2 "DOI__10.1021/bi901627u"
      minerva_type "Complex"
      minerva_x 1279.0
      minerva_y 339.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation "Annotation [about=\"#s_id_csa84\", history=]"
      species_id "s_id_csa84"
      species_meta_id "s_id_csa84"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
      species_notes "XMLNode 'notes' [childElements size=1]"
    ]
  ]
  node [
    id 378
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1000.0
      y 450.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mtdna_space_encoded_space_oxphos_space_units__mitochondrial_space_matrix__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa103"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa98"
      minerva_elementId2 "csa103"
      minerva_name "mtDNA encoded OXPHOS units"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/23149385"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/pubmed/30030361"
      minerva_ref_type__resource1 "PUBMED__23149385"
      minerva_ref_type__resource2 "PUBMED__30030361"
      minerva_type "Complex"
      minerva_x 388.0
      minerva_x2 1743.0
      minerva_y 908.0
      minerva_y2 568.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrial_space_matrix"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation "Annotation [about=\"#s_id_csa103\", history=]"
      species_id "s_id_csa103"
      species_meta_id "s_id_csa103"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
    ]
  ]
  edge [
    id 383
    source 82
    target 99
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa20"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 384
    source 99
    target 97
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa34"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 385
    source 10
    target 99
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR0"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa219"
    ]
  ]
  edge [
    id 386
    source 43
    target 103
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa18"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 387
    source 103
    target 8
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa47"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 388
    source 95
    target 106
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 389
    source 106
    target 63
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa3"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 390
    source 106
    target 13
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa19"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 391
    source 106
    target 82
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa20"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 393
    source 61
    target 112
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa364"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 394
    source 112
    target 69
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa362"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 395
    source 43
    target 115
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa18"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 396
    source 115
    target 8
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa47"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 397
    source 87
    target 118
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa460"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 398
    source 118
    target 66
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa463"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 399
    source 43
    target 121
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa18"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 400
    source 121
    target 8
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa47"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 401
    source 6
    target 124
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa354"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 402
    source 124
    target 69
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa362"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 403
    source 6
    target 127
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa354"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 404
    source 13
    target 127
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa19"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 405
    source 127
    target 92
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa355"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 406
    source 98
    target 127
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR2"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa358"
    ]
  ]
  edge [
    id 407
    source 67
    target 132
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa30"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 408
    source 15
    target 132
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa32"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 409
    source 132
    target 65
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 410
    source 8
    target 132
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR3"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa47"
    ]
  ]
  edge [
    id 411
    source 13
    target 132
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR4"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa19"
    ]
  ]
  edge [
    id 412
    source 77
    target 138
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa688"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 413
    source 138
    target 29
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa690"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 414
    source 94
    target 138
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR5"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa92"
    ]
  ]
  edge [
    id 415
    source 85
    target 138
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR6"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa93"
    ]
  ]
  edge [
    id 416
    source 22
    target 138
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR7"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa691"
    ]
  ]
  edge [
    id 417
    source 45
    target 138
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR8"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa94"
    ]
  ]
  edge [
    id 418
    source 78
    target 138
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR9"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa95"
    ]
  ]
  edge [
    id 419
    source 18
    target 146
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR10"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa500"
    ]
  ]
  edge [
    id 420
    source 52
    target 150
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa52"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 421
    source 150
    target 66
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa463"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 422
    source 74
    target 153
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa652"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 423
    source 153
    target 41
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa55"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 424
    source 38
    target 153
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR11"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa53"
    ]
  ]
  edge [
    id 425
    source 48
    target 153
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR12"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa692"
    ]
  ]
  edge [
    id 426
    source 48
    target 158
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa692"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 427
    source 158
    target 88
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa694"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 428
    source 62
    target 158
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR13"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa693"
    ]
  ]
  edge [
    id 429
    source 89
    target 162
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa459"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 430
    source 57
    target 162
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR14"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa58"
    ]
  ]
  edge [
    id 431
    source 37
    target 162
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR15"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa51"
    ]
  ]
  edge [
    id 432
    source 72
    target 162
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR16"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa90"
    ]
  ]
  edge [
    id 433
    source 4
    target 168
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa380"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 434
    source 49
    target 168
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa395"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 435
    source 168
    target 31
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa381"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 436
    source 168
    target 71
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa383"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 437
    source 60
    target 168
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR17"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa384"
    ]
  ]
  edge [
    id 438
    source 92
    target 174
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa355"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 439
    source 33
    target 174
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa392"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 440
    source 174
    target 39
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa369"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 442
    source 2
    target 174
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR18"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa97"
    ]
  ]
  edge [
    id 443
    source 47
    target 180
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa716"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 444
    source 180
    target 97
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa34"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 445
    source 86
    target 180
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR19"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa44"
    ]
  ]
  edge [
    id 446
    source 90
    target 185
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa663"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 447
    source 185
    target 44
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa665"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 448
    source 55
    target 185
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR20"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa498"
    ]
  ]
  edge [
    id 449
    source 26
    target 189
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa91"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 450
    source 189
    target 72
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa90"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 451
    source 24
    target 189
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR21"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa494"
    ]
  ]
  edge [
    id 452
    source 6
    target 193
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa354"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 453
    source 13
    target 193
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa19"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 454
    source 193
    target 92
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa355"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 455
    source 56
    target 193
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR22"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa372"
    ]
  ]
  edge [
    id 456
    source 74
    target 198
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa652"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 457
    source 198
    target 89
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa459"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 458
    source 87
    target 198
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR23"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa460"
    ]
  ]
  edge [
    id 459
    source 20
    target 198
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR24"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa56"
    ]
  ]
  edge [
    id 460
    source 54
    target 198
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR25"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa653"
    ]
  ]
  edge [
    id 461
    source 13
    target 204
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa19"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 462
    source 204
    target 43
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa18"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 463
    source 82
    target 204
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR26"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa20"
    ]
  ]
  edge [
    id 464
    source 17
    target 208
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa659"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 465
    source 44
    target 208
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa665"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 466
    source 35
    target 208
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa661"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 467
    source 208
    target 82
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa20"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 468
    source 86
    target 208
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR27"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa44"
    ]
  ]
  edge [
    id 469
    source 1
    target 215
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa374"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 470
    source 215
    target 6
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa354"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 471
    source 3
    target 218
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa712"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 472
    source 36
    target 218
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa710"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 473
    source 218
    target 8
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa47"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 474
    source 83
    target 218
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR28"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa713"
    ]
  ]
  edge [
    id 475
    source 6
    target 223
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa354"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 476
    source 59
    target 223
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa376"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 477
    source 223
    target 1
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa374"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 478
    source 223
    target 13
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa19"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 479
    source 223
    target 25
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa375"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 480
    source 14
    target 229
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa671"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 481
    source 229
    target 28
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa667"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 482
    source 55
    target 229
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR29"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa498"
    ]
  ]
  edge [
    id 483
    source 92
    target 233
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa355"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 484
    source 61
    target 233
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa364"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 485
    source 233
    target 6
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa354"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 486
    source 233
    target 13
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa19"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 487
    source 233
    target 39
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa369"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 488
    source 70
    target 239
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa706"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 489
    source 11
    target 239
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa700"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 490
    source 239
    target 85
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa93"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 491
    source 64
    target 239
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR30"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa695"
    ]
  ]
  edge [
    id 492
    source 92
    target 244
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa355"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 493
    source 25
    target 244
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa375"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 494
    source 244
    target 7
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa363"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 495
    source 244
    target 59
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa376"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 496
    source 244
    target 61
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa364"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 497
    source 6
    target 250
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa354"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 498
    source 250
    target 69
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa362"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 499
    source 40
    target 253
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa714"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 500
    source 42
    target 253
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa668"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 501
    source 28
    target 253
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa667"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 502
    source 21
    target 253
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa669"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 503
    source 253
    target 86
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa44"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 504
    source 68
    target 259
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa96"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 505
    source 259
    target 78
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa95"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 506
    source 64
    target 259
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR31"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa695"
    ]
  ]
  edge [
    id 507
    source 27
    target 263
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa397"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 508
    source 1
    target 263
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa374"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 509
    source 263
    target 23
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa50"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 510
    source 263
    target 6
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa354"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 511
    source 33
    target 268
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa392"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 512
    source 49
    target 268
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa395"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 514
    source 268
    target 71
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa383"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 515
    source 12
    target 268
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR32"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa393"
    ]
  ]
  edge [
    id 516
    source 97
    target 274
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa34"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 517
    source 274
    target 43
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa18"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 518
    source 81
    target 277
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa57"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 519
    source 277
    target 66
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa463"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 520
    source 23
    target 280
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa50"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 521
    source 280
    target 27
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa397"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 522
    source 6
    target 280
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR33"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa354"
    ]
  ]
  edge [
    id 523
    source 82
    target 280
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR34"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa20"
    ]
  ]
  edge [
    id 524
    source 61
    target 285
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa364"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 525
    source 25
    target 285
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa375"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 526
    source 285
    target 7
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa363"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 527
    source 285
    target 59
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa376"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 528
    source 10
    target 290
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa219"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 529
    source 290
    target 30
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa255"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 530
    source 82
    target 290
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR35"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa20"
    ]
  ]
  edge [
    id 531
    source 92
    target 296
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa355"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 532
    source 296
    target 69
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa362"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 533
    source 92
    target 299
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa355"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 534
    source 299
    target 39
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa369"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 535
    source 53
    target 299
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR36"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa385"
    ]
  ]
  edge [
    id 536
    source 75
    target 303
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa655"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 537
    source 303
    target 74
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa652"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 538
    source 80
    target 303
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR37"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa461"
    ]
  ]
  edge [
    id 539
    source 66
    target 303
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR38"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa463"
    ]
  ]
  edge [
    id 540
    source 74
    target 308
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa652"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 541
    source 308
    target 54
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa653"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 542
    source 80
    target 308
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR39"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa461"
    ]
  ]
  edge [
    id 543
    source 14
    target 312
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa671"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 544
    source 312
    target 42
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa668"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 545
    source 55
    target 312
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR40"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa498"
    ]
  ]
  edge [
    id 546
    source 92
    target 316
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa355"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 547
    source 31
    target 316
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa381"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 548
    source 316
    target 39
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa369"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 549
    source 316
    target 4
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa380"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 550
    source 50
    target 316
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR41"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa379"
    ]
  ]
  edge [
    id 551
    source 32
    target 316
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR42"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa378"
    ]
  ]
  edge [
    id 552
    source 91
    target 323
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa717"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 553
    source 323
    target 16
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa35"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 554
    source 86
    target 323
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR43"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa44"
    ]
  ]
  edge [
    id 555
    source 5
    target 328
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR44"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa205"
    ]
  ]
  edge [
    id 556
    source 86
    target 328
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR45"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa44"
    ]
  ]
  edge [
    id 557
    source 8
    target 333
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa47"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 558
    source 333
    target 13
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa19"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 559
    source 14
    target 336
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa671"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 560
    source 336
    target 21
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa669"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 561
    source 34
    target 336
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR46"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa499"
    ]
  ]
  edge [
    id 562
    source 1
    target 340
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa374"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 563
    source 51
    target 340
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa13"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 564
    source 13
    target 340
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa19"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 565
    source 30
    target 340
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa255"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 566
    source 340
    target 9
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa23"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 567
    source 340
    target 58
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa253"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 568
    source 340
    target 10
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa219"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 569
    source 340
    target 43
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa18"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 570
    source 97
    target 340
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR47"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa34"
    ]
  ]
  edge [
    id 571
    source 73
    target 350
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa664"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 572
    source 350
    target 35
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa661"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 573
    source 55
    target 350
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR48"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa498"
    ]
  ]
  edge [
    id 574
    source 1
    target 354
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa374"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 575
    source 354
    target 6
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa354"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 576
    source 93
    target 357
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa357"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 577
    source 357
    target 98
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa358"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 578
    source 5
    target 357
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR49"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa205"
    ]
  ]
  edge [
    id 579
    source 54
    target 361
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa653"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 580
    source 361
    target 74
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa652"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 581
    source 87
    target 361
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR50"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa460"
    ]
  ]
  edge [
    id 582
    source 46
    target 361
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR51"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa54"
    ]
  ]
  edge [
    id 583
    source 52
    target 361
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR52"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa52"
    ]
  ]
  edge [
    id 584
    source 97
    target 367
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa34"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 585
    source 367
    target 16
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa35"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 586
    source 58
    target 367
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR53"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa253"
    ]
  ]
  edge [
    id 587
    source 146
    target 371
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa84"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 588
    source 371
    target 290
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa84"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 590
    source 328
    target 371
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa24"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 593
    source 378
    target 180
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa103"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 594
    source 378
    target 208
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa103"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 595
    source 378
    target 323
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa103"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 596
    source 162
    target 378
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa60"
      stoichiometry "1.0"
    ]
  ]
]
