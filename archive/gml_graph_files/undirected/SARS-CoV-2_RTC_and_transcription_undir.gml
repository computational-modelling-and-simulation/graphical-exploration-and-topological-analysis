# generated with VANTED V2.8.0 at Tue Apr 27 20:00:51 CEST 2021
graph [
  background_coloring "true"
  cluster_colors "255,128,128,255:0,0,0,255;128,255,128,255:0,0,0,255;128,128,255,255:0,0,0,255"
  clusterbackground_fill_outer_region "false"
  clusterbackground_grid 50.0
  clusterbackground_low_alpha 0.2
  clusterbackground_radius 200.0
  clusterbackground_space_fill "true"
  graphbackgroundcolor "#ffffff"
  sbml [
    model_non_rdf_annotation ""
    model_notes ""
    namespace "xmlns:celldesigner=\"http://www.sbml.org/2001/ns/celldesigner\" "
    sbml_level "2"
    sbml_version "4"
  ]
  sbml_compartment_default [
    sbml_compartment_default_id "default"
    sbml_compartment_default_meta_id "default"
    sbml_compartment_default_non_rdf_annotation ""
    sbml_compartment_default_notes ""
    sbml_compartment_default_size "1.0"
    sbml_compartment_default_units "volume"
  ]
  sbml_compartment_s_id_ca2 [
    sbml_compartment_s_id_ca2_annotation ""
    sbml_compartment_s_id_ca2_id "s_id_ca2"
    sbml_compartment_s_id_ca2_meta_id "s_id_ca2"
    sbml_compartment_s_id_ca2_name "cell"
    sbml_compartment_s_id_ca2_non_rdf_annotation ""
    sbml_compartment_s_id_ca2_notes ""
    sbml_compartment_s_id_ca2_outside "default"
    sbml_compartment_s_id_ca2_size "1.0"
    sbml_compartment_s_id_ca2_units "volume"
  ]
  sbml_compartment_s_id_glyph1 [
    sbml_compartment_s_id_glyph1_annotation ""
    sbml_compartment_s_id_glyph1_id "s_id_glyph1"
    sbml_compartment_s_id_glyph1_meta_id "s_id_glyph1"
    sbml_compartment_s_id_glyph1_name "double_space_membrane_space_vesicle_space_viral_space_factory"
    sbml_compartment_s_id_glyph1_non_rdf_annotation ""
    sbml_compartment_s_id_glyph1_notes ""
    sbml_compartment_s_id_glyph1_outside "s_id_ca2"
    sbml_compartment_s_id_glyph1_size "1.0"
    sbml_compartment_s_id_glyph1_units "volume"
  ]
  sbml_unit_definition_1 [
    sbml_unit_definition_1_id "time"
    sbml_unit_definition_1_name "time"
    sbml_unit_definition_1_sub_unit_1_ "(1.0 * 10^0 * second)^1.0"
    sbml_unit_definition_1unit "(1.0 * 10^0 * second)^1.0"
  ]
  sbml_unit_definition_2 [
    sbml_unit_definition_2_id "length"
    sbml_unit_definition_2_name "length"
    sbml_unit_definition_2_sub_unit_1_ "(1.0 * 10^0 * metre)^1.0"
    sbml_unit_definition_2unit "(1.0 * 10^0 * metre)^1.0"
  ]
  sbml_unit_definition_3 [
    sbml_unit_definition_3_id "area"
    sbml_unit_definition_3_name "area"
    sbml_unit_definition_3_sub_unit_1_ "(1.0 * 10^0 * metre)^2.0"
    sbml_unit_definition_3unit "(1.0 * 10^0 * metre)^2.0"
  ]
  sbml_unit_definition_4 [
    sbml_unit_definition_4_id "substance"
    sbml_unit_definition_4_name "substance"
    sbml_unit_definition_4_sub_unit_1_ "(1.0 * 10^0 * mole)^1.0"
    sbml_unit_definition_4unit "(1.0 * 10^0 * mole)^1.0"
  ]
  sbml_unit_definition_5 [
    sbml_unit_definition_5_id "volume"
    sbml_unit_definition_5_name "volume"
    sbml_unit_definition_5_sub_unit_1_ "(1.0 * 10^0 * litre)^1.0"
    sbml_unit_definition_5unit "(1.0 * 10^0 * litre)^1.0"
  ]
  directed 0
  node [
    id 1
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 200.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp8_space_(ii)__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_glyph50"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "glyph50"
      minerva_name "nsp8 (II)"
      minerva_type "Protein"
      minerva_x 485.0
      minerva_y 855.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_glyph50"
      species_meta_id "s_id_glyph50"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 2
    zlevel -1

    cluster [
      cluster "s_id_glyph1"
    ]
    graphics [
      x 300.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "deg__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_glyph75"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "glyph69"
      minerva_elementId2 "glyph71"
      minerva_elementId3 "glyph74"
      minerva_elementId4 "glyph72"
      minerva_elementId5 "glyph75"
      minerva_name "deg"
      minerva_type "Degraded"
      minerva_x 2055.0
      minerva_x2 1405.0
      minerva_x3 1760.0
      minerva_x4 2250.0
      minerva_x5 1915.0
      minerva_y 960.0
      minerva_y2 800.0
      minerva_y3 465.0
      minerva_y4 955.0
      minerva_y5 455.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_glyph1"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_glyph75"
      species_meta_id "s_id_glyph75"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 3
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 400.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp11__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_glyph66"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "glyph66"
      minerva_name "nsp11"
      minerva_type "Protein"
      minerva_x 285.0
      minerva_y 1145.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_glyph66"
      species_meta_id "s_id_glyph66"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 4
    zlevel -1

    cluster [
      cluster "s_id_glyph1"
    ]
    graphics [
      x 500.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "pp1a__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_glyph46"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "glyph46"
      minerva_name "pp1a"
      minerva_type "Protein"
      minerva_x 2140.0
      minerva_y 1085.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_glyph1"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_glyph46"
      species_meta_id "s_id_glyph46"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 5
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 600.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "(_plus_)grna_space_(including_space_orf1a_space_orf1b)__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_glyph13"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "glyph13"
      minerva_name "(+)gRNA (including ORF1a ORF1b)"
      minerva_type "Gene"
      minerva_x 212.0
      minerva_y 60.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_glyph13"
      species_meta_id "s_id_glyph13"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 6
    zlevel -1

    cluster [
      cluster "s_id_glyph1"
    ]
    graphics [
      x 700.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "(_plus_)grna_space_(including_space_orf1a_space_orf1b)__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_glyph17"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "glyph17"
      minerva_name "(+)gRNA (including ORF1a ORF1b)"
      minerva_type "Gene"
      minerva_x 2195.0
      minerva_y 810.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_glyph1"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_glyph17"
      species_meta_id "s_id_glyph17"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 7
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 800.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp6__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_glyph56"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "glyph56"
      minerva_name "nsp6"
      minerva_type "Protein"
      minerva_x 545.0
      minerva_y 1065.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_glyph56"
      species_meta_id "s_id_glyph56"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 8
    zlevel -1

    cluster [
      cluster "s_id_glyph1"
    ]
    graphics [
      x 900.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "pp1ab__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_glyph53"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "glyph53"
      minerva_name "pp1ab"
      minerva_type "Protein"
      minerva_x 2340.0
      minerva_y 1085.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_glyph1"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_glyph53"
      species_meta_id "s_id_glyph53"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 10
    zlevel -1

    cluster [
      cluster "s_id_glyph1"
    ]
    graphics [
      x 1100.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "viral_space_accessory__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_glyph19"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "glyph19"
      minerva_name "viral accessory"
      minerva_type "Protein"
      minerva_x 2175.0
      minerva_y 410.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_glyph1"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_glyph19"
      species_meta_id "s_id_glyph19"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 11
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1200.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp2__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_glyph37"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "glyph37"
      minerva_name "nsp2"
      minerva_type "Protein"
      minerva_x 415.0
      minerva_y 980.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_glyph37"
      species_meta_id "s_id_glyph37"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 12
    zlevel -1

    cluster [
      cluster "s_id_glyph1"
    ]
    graphics [
      x 200.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "structural_space_proteins__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_glyph18"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "glyph18"
      minerva_name "structural proteins"
      minerva_type "Protein"
      minerva_x 2175.0
      minerva_y 515.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_glyph1"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_glyph18"
      species_meta_id "s_id_glyph18"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 13
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 300.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp9__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_glyph51"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "glyph51"
      minerva_name "nsp9"
      minerva_type "Protein"
      minerva_x 755.0
      minerva_y 950.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_glyph51"
      species_meta_id "s_id_glyph51"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 14
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 400.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "complex__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_glyph8"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "glyph8"
      minerva_name "complex"
      minerva_type "Complex"
      minerva_x 755.0
      minerva_y 770.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_glyph8"
      species_meta_id "s_id_glyph8"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 15
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 500.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp16__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_glyph36"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "glyph36"
      minerva_name "nsp16"
      minerva_type "Protein"
      minerva_x 490.0
      minerva_y 175.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_glyph36"
      species_meta_id "s_id_glyph36"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 16
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 600.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp14__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_glyph54"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "glyph54"
      minerva_name "nsp14"
      minerva_type "Protein"
      minerva_x 490.0
      minerva_y 495.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_glyph54"
      species_meta_id "s_id_glyph54"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 17
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 700.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp15__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_glyph52"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "glyph52"
      minerva_name "nsp15"
      minerva_type "Protein"
      minerva_x 490.0
      minerva_y 255.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_glyph52"
      species_meta_id "s_id_glyph52"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 18
    zlevel -1

    cluster [
      cluster "s_id_glyph1"
    ]
    graphics [
      x 800.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "complex__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_glyph6"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "glyph6"
      minerva_name "complex"
      minerva_type "Complex"
      minerva_x 1905.0
      minerva_y 1265.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_glyph1"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_glyph6"
      species_meta_id "s_id_glyph6"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 19
    zlevel -1

    cluster [
      cluster "s_id_glyph1"
    ]
    graphics [
      x 900.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cov_space_poly_minus__br_merase_space_complex__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_glyph4"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "glyph4"
      minerva_name "CoV poly-
merase complex"
      minerva_type "Complex"
      minerva_x 1905.0
      minerva_y 995.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_glyph1"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_glyph4"
      species_meta_id "s_id_glyph4"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 20
    zlevel -1

    cluster [
      cluster "s_id_glyph1"
    ]
    graphics [
      x 1000.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "(_minus_)grna_space_(including_space_orf1a_space_orf1b)__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_glyph16"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "glyph16"
      minerva_name "(-)gRNA (including ORF1a ORF1b)"
      minerva_type "Gene"
      minerva_x 2196.0
      minerva_y 720.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_glyph1"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_glyph16"
      species_meta_id "s_id_glyph16"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 21
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1100.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp3__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_glyph45"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "glyph45"
      minerva_name "nsp3"
      minerva_type "Protein"
      minerva_x 545.0
      minerva_y 980.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_glyph45"
      species_meta_id "s_id_glyph45"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 22
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1200.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "pp1a__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_glyph63"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "glyph63"
      minerva_name "pp1a"
      minerva_type "Protein"
      minerva_x 160.0
      minerva_y 330.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_glyph63"
      species_meta_id "s_id_glyph63"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 23
    zlevel -1

    cluster [
      cluster "s_id_glyph1"
    ]
    graphics [
      x 200.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "(_plus_)sgrna_space_(2_minus_9)__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_glyph20"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "glyph20"
      minerva_name "(+)sgRNA (2-9)"
      minerva_type "Gene"
      minerva_x 1985.0
      minerva_y 565.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_glyph1"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_glyph20"
      species_meta_id "s_id_glyph20"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 24
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 300.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "deg__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_glyph73"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "glyph73"
      minerva_elementId2 "glyph70"
      minerva_name "deg"
      minerva_type "Degraded"
      minerva_x 50.0
      minerva_x2 235.0
      minerva_y 205.0
      minerva_y2 210.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_glyph73"
      species_meta_id "s_id_glyph73"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 26
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 500.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_glyph39"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "glyph39"
      minerva_name "nsp1"
      minerva_type "Protein"
      minerva_x 285.0
      minerva_y 980.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_glyph39"
      species_meta_id "s_id_glyph39"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 27
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 600.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp13__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_glyph34"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "glyph34"
      minerva_name "nsp13"
      minerva_type "Protein"
      minerva_x 490.0
      minerva_y 335.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_glyph34"
      species_meta_id "s_id_glyph34"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 28
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 700.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cov_space_poly_minus__br_merase_space_complex__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_glyph5"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "glyph5"
      minerva_name "CoV poly-
merase complex"
      minerva_type "Complex"
      minerva_x 755.0
      minerva_y 495.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_glyph5"
      species_meta_id "s_id_glyph5"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 29
    zlevel -1

    cluster [
      cluster "s_id_glyph1"
    ]
    graphics [
      x 800.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp9__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_glyph42"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "glyph42"
      minerva_name "nsp9"
      minerva_type "Protein"
      minerva_x 1735.0
      minerva_y 1260.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_glyph1"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_glyph42"
      species_meta_id "s_id_glyph42"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 30
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 900.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp7__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_glyph33"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "glyph33"
      minerva_name "nsp7"
      minerva_type "Protein"
      minerva_x 485.0
      minerva_y 685.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_glyph33"
      species_meta_id "s_id_glyph33"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 31
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1000.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp5__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_glyph67"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "glyph67"
      minerva_name "nsp5"
      minerva_type "Protein"
      minerva_x 415.0
      minerva_y 1065.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_glyph67"
      species_meta_id "s_id_glyph67"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 32
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1100.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp10__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_glyph61"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "glyph61"
      minerva_name "nsp10"
      minerva_type "Protein"
      minerva_x 490.0
      minerva_y 575.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_glyph61"
      species_meta_id "s_id_glyph61"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 33
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1200.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp12__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_glyph62"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "glyph62"
      minerva_name "nsp12"
      minerva_type "Protein"
      minerva_x 490.0
      minerva_y 415.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_glyph62"
      species_meta_id "s_id_glyph62"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 34
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 200.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "pp1ab__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_glyph40"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "glyph40"
      minerva_name "pp1ab"
      minerva_type "Protein"
      minerva_x 335.0
      minerva_y 330.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_glyph40"
      species_meta_id "s_id_glyph40"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 35
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 300.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp8_space_(i)__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_glyph49"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "glyph49"
      minerva_name "nsp8 (I)"
      minerva_type "Protein"
      minerva_x 485.0
      minerva_y 770.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_glyph49"
      species_meta_id "s_id_glyph49"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 36
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 400.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp4__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_glyph41"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "glyph41"
      minerva_name "nsp4"
      minerva_type "Protein"
      minerva_x 285.0
      minerva_y 1065.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_glyph41"
      species_meta_id "s_id_glyph41"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 37
    zlevel -1

    graphics [
      x 500.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "glyph82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "glyph82"
      type "text"
    ]
    minerva [
      minerva_type "Known transition omitted"
      minerva_x 295.50000000000006
      minerva_y 770.0
    ]
    sbml [
      reaction_id "glyph82"
      reaction_meta_id "glyph82"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 51
    zlevel -1

    graphics [
      x 600.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "glyph81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "glyph81"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 1760.0
      minerva_y 520.0
    ]
    sbml [
      reaction_id "glyph81"
      reaction_meta_id "glyph81"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 55
    zlevel -1

    graphics [
      x 700.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "glyph87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "glyph87"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 293.76791466482024
      minerva_y 207.3450903094619
    ]
    sbml [
      reaction_id "glyph87"
      reaction_meta_id "glyph87"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 59
    zlevel -1

    graphics [
      x 800.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "glyph76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "glyph76"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 612.0
      minerva_y 770.0
    ]
    sbml [
      reaction_id "glyph76"
      reaction_meta_id "glyph76"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 64
    zlevel -1

    graphics [
      x 900.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "glyph77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "glyph77"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 880.0000000000007
      minerva_y 543.6111111111112
    ]
    sbml [
      reaction_id "glyph77"
      reaction_meta_id "glyph77"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 70
    zlevel -1

    graphics [
      x 1000.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "glyph84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "glyph84"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 614.5
      minerva_y 495.0
    ]
    sbml [
      reaction_id "glyph84"
      reaction_meta_id "glyph84"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 75
    zlevel -1

    graphics [
      x 1100.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "glyph86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "glyph86"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 1335.0
      minerva_y 837.5
    ]
    sbml [
      reaction_id "glyph86"
      reaction_meta_id "glyph86"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 79
    zlevel -1

    graphics [
      x 1200.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "glyph85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "glyph85"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 115.00000000000001
      minerva_y 200.00000000000003
    ]
    sbml [
      reaction_id "glyph85"
      reaction_meta_id "glyph85"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 83
    zlevel -1

    graphics [
      x 200.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "glyph88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "glyph88"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 1985.0
      minerva_y 450.0
    ]
    sbml [
      reaction_id "glyph88"
      reaction_meta_id "glyph88"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 88
    zlevel -1

    graphics [
      x 300.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "glyph80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "glyph80"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 1606.0
      minerva_y 1144.9999999999998
    ]
    sbml [
      reaction_id "glyph80"
      reaction_meta_id "glyph80"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 95
    zlevel -1

    graphics [
      x 400.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "glyph79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "glyph79"
      type "text"
    ]
    minerva [
      minerva_type "Known transition omitted"
      minerva_x 385.5
      minerva_y 415.0
    ]
    sbml [
      reaction_id "glyph79"
      reaction_meta_id "glyph79"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 102
    zlevel -1

    graphics [
      x 500.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "glyph78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "glyph78"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 2305.0
      minerva_y 949.9999999999994
    ]
    sbml [
      reaction_id "glyph78"
      reaction_meta_id "glyph78"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 106
    zlevel -1

    graphics [
      x 600.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "glyph83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "glyph83"
      type "text"
    ]
    minerva [
      minerva_type "State transition"
      minerva_x 2107.5
      minerva_y 955.0000000000001
    ]
    sbml [
      reaction_id "glyph83"
      reaction_meta_id "glyph83"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 110
    zlevel -1

    cluster [
      cluster "s_id_glyph1"
    ]
    graphics [
      x 700.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "replication_space_transcription_space_complex_space___double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_glyph2"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "glyph2"
      minerva_name "Replication transcription complex "
      minerva_type "Complex"
      minerva_x 1335.0
      minerva_y 1145.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_glyph1"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_glyph2"
      species_meta_id "s_id_glyph2"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
    ]
  ]
  edge [
    id 118
    source 22
    target 37
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_glyph63"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 119
    source 37
    target 35
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_glyph49"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 120
    source 37
    target 1
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_glyph50"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 121
    source 37
    target 30
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_glyph33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 122
    source 37
    target 13
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_glyph51"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 123
    source 37
    target 26
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_glyph39"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 124
    source 37
    target 11
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_glyph37"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 125
    source 37
    target 21
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_glyph45"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 126
    source 37
    target 3
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_glyph66"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 127
    source 37
    target 7
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_glyph56"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 128
    source 37
    target 31
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_glyph67"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 129
    source 37
    target 36
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_glyph41"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 130
    source 37
    target 32
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_glyph61"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 131
    source 2
    target 51
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_glyph75"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 132
    source 51
    target 23
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_glyph20"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 133
    source 24
    target 55
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_glyph73"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 134
    source 55
    target 34
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_glyph40"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 135
    source 5
    target 55
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR1"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_glyph13"
    ]
  ]
  edge [
    id 136
    source 35
    target 59
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_glyph49"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 137
    source 1
    target 59
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_glyph50"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 138
    source 30
    target 59
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_glyph33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 139
    source 59
    target 14
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_glyph8"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 140
    source 28
    target 64
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_glyph5"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 141
    source 13
    target 64
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_glyph51"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 142
    source 14
    target 64
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_glyph8"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 143
    source 5
    target 64
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_glyph13"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 144
    source 16
    target 70
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_glyph54"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 145
    source 32
    target 70
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_glyph61"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 146
    source 33
    target 70
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_glyph62"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 147
    source 70
    target 28
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_glyph5"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 148
    source 2
    target 75
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_glyph75"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 149
    source 24
    target 79
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_glyph73"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 150
    source 79
    target 22
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_glyph63"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 151
    source 5
    target 79
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR2"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_glyph13"
    ]
  ]
  edge [
    id 152
    source 2
    target 83
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_glyph75"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 153
    source 83
    target 10
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_glyph19"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 154
    source 83
    target 12
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_glyph18"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 155
    source 23
    target 83
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR3"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_glyph20"
    ]
  ]
  edge [
    id 156
    source 88
    target 19
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_glyph4"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 157
    source 88
    target 18
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_glyph6"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 158
    source 88
    target 29
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_glyph42"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 159
    source 88
    target 6
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_glyph17"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 160
    source 88
    target 20
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_glyph16"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 161
    source 34
    target 95
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_glyph40"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 162
    source 95
    target 33
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_glyph62"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 163
    source 95
    target 16
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_glyph54"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 164
    source 95
    target 27
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_glyph34"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 165
    source 95
    target 15
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_glyph36"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 166
    source 95
    target 17
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_glyph52"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 167
    source 2
    target 102
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_glyph75"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 168
    source 102
    target 8
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_glyph53"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 169
    source 6
    target 102
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR4"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_glyph17"
    ]
  ]
  edge [
    id 170
    source 2
    target 106
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_glyph75"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 171
    source 106
    target 4
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_glyph46"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 172
    source 6
    target 106
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR5"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_glyph17"
    ]
  ]
  edge [
    id 173
    source 75
    target 110
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_glyph2"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 174
    source 110
    target 88
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_glyph2"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 175
    source 110
    target 51
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR0"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_glyph3"
    ]
  ]
  edge [
    id 176
    source 64
    target 110
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_glyph3"
      stoichiometry "1.0"
    ]
  ]
]
