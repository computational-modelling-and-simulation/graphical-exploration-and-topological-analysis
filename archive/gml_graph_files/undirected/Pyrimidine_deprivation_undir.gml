# generated with VANTED V2.8.0 at Tue Apr 27 20:00:49 CEST 2021
graph [
  background_coloring "true"
  cluster_colors "255,128,128,255:0,0,0,255;255,197,128,255:0,0,0,255;243,255,128,255:0,0,0,255;174,255,128,255:0,0,0,255;128,255,151,255:0,0,0,255;128,255,220,255:0,0,0,255;128,220,255,255:0,0,0,255;128,151,255,255:0,0,0,255;174,128,255,255:0,0,0,255;243,128,255,255:0,0,0,255;255,128,197,255:0,0,0,255"
  clusterbackground_fill_outer_region "false"
  clusterbackground_grid 50.0
  clusterbackground_low_alpha 0.2
  clusterbackground_radius 200.0
  clusterbackground_space_fill "true"
  graphbackgroundcolor "#ffffff"
  sbml [
    model_meta_id "pyrimidine_deprivation"
    model_non_rdf_annotation ""
    model_notes ""
    model_sbml_id "pyrimidine_deprivation"
    namespace "xmlns:celldesigner=\"http://www.sbml.org/2001/ns/celldesigner\" "
    sbml_level "2"
    sbml_version "4"
  ]
  sbml_compartment_default [
    sbml_compartment_default_id "default"
    sbml_compartment_default_meta_id "default"
    sbml_compartment_default_non_rdf_annotation ""
    sbml_compartment_default_notes ""
    sbml_compartment_default_size "1.0"
    sbml_compartment_default_units "volume"
  ]
  sbml_compartment_s_id_ca10 [
    sbml_compartment_s_id_ca10_annotation ""
    sbml_compartment_s_id_ca10_id "s_id_ca10"
    sbml_compartment_s_id_ca10_meta_id "s_id_ca10"
    sbml_compartment_s_id_ca10_name "pulmonary_space_endothelial_space_cell"
    sbml_compartment_s_id_ca10_non_rdf_annotation ""
    sbml_compartment_s_id_ca10_notes ""
    sbml_compartment_s_id_ca10_outside "s_id_ca11"
    sbml_compartment_s_id_ca10_size "1.0"
    sbml_compartment_s_id_ca10_units "volume"
  ]
  sbml_compartment_s_id_ca11 [
    sbml_compartment_s_id_ca11_id "s_id_ca11"
    sbml_compartment_s_id_ca11_meta_id "s_id_ca11"
    sbml_compartment_s_id_ca11_name "human_space_host"
    sbml_compartment_s_id_ca11_non_rdf_annotation ""
    sbml_compartment_s_id_ca11_notes ""
    sbml_compartment_s_id_ca11_outside "default"
    sbml_compartment_s_id_ca11_size "1.0"
    sbml_compartment_s_id_ca11_units "volume"
  ]
  sbml_compartment_s_id_ca2 [
    sbml_compartment_s_id_ca2_annotation ""
    sbml_compartment_s_id_ca2_id "s_id_ca2"
    sbml_compartment_s_id_ca2_meta_id "s_id_ca2"
    sbml_compartment_s_id_ca2_name "mitochondrion"
    sbml_compartment_s_id_ca2_non_rdf_annotation ""
    sbml_compartment_s_id_ca2_notes ""
    sbml_compartment_s_id_ca2_outside "s_id_ca10"
    sbml_compartment_s_id_ca2_size "1.0"
    sbml_compartment_s_id_ca2_units "volume"
  ]
  sbml_compartment_s_id_ca3 [
    sbml_compartment_s_id_ca3_annotation ""
    sbml_compartment_s_id_ca3_id "s_id_ca3"
    sbml_compartment_s_id_ca3_meta_id "s_id_ca3"
    sbml_compartment_s_id_ca3_name "Golgi_space_apparatus"
    sbml_compartment_s_id_ca3_non_rdf_annotation ""
    sbml_compartment_s_id_ca3_notes ""
    sbml_compartment_s_id_ca3_outside "s_id_ca10"
    sbml_compartment_s_id_ca3_size "1.0"
    sbml_compartment_s_id_ca3_units "volume"
  ]
  sbml_compartment_s_id_ca4 [
    sbml_compartment_s_id_ca4_annotation ""
    sbml_compartment_s_id_ca4_id "s_id_ca4"
    sbml_compartment_s_id_ca4_meta_id "s_id_ca4"
    sbml_compartment_s_id_ca4_name "endoplasmic_space_reticulum"
    sbml_compartment_s_id_ca4_non_rdf_annotation ""
    sbml_compartment_s_id_ca4_notes ""
    sbml_compartment_s_id_ca4_outside "s_id_ca10"
    sbml_compartment_s_id_ca4_size "1.0"
    sbml_compartment_s_id_ca4_units "volume"
  ]
  sbml_compartment_s_id_ca5 [
    sbml_compartment_s_id_ca5_annotation ""
    sbml_compartment_s_id_ca5_id "s_id_ca5"
    sbml_compartment_s_id_ca5_meta_id "s_id_ca5"
    sbml_compartment_s_id_ca5_name "endolysosome"
    sbml_compartment_s_id_ca5_non_rdf_annotation ""
    sbml_compartment_s_id_ca5_notes ""
    sbml_compartment_s_id_ca5_outside "s_id_ca10"
    sbml_compartment_s_id_ca5_size "1.0"
    sbml_compartment_s_id_ca5_units "volume"
  ]
  sbml_compartment_s_id_ca6 [
    sbml_compartment_s_id_ca6_annotation ""
    sbml_compartment_s_id_ca6_id "s_id_ca6"
    sbml_compartment_s_id_ca6_meta_id "s_id_ca6"
    sbml_compartment_s_id_ca6_name "nucleus"
    sbml_compartment_s_id_ca6_non_rdf_annotation ""
    sbml_compartment_s_id_ca6_notes ""
    sbml_compartment_s_id_ca6_outside "s_id_ca10"
    sbml_compartment_s_id_ca6_size "1.0"
    sbml_compartment_s_id_ca6_units "volume"
  ]
  sbml_compartment_s_id_ca7 [
    sbml_compartment_s_id_ca7_annotation ""
    sbml_compartment_s_id_ca7_id "s_id_ca7"
    sbml_compartment_s_id_ca7_meta_id "s_id_ca7"
    sbml_compartment_s_id_ca7_name "endoplasmic_space_reticulum_minus_Golgi_space_intermediate_space_compartment"
    sbml_compartment_s_id_ca7_non_rdf_annotation ""
    sbml_compartment_s_id_ca7_notes ""
    sbml_compartment_s_id_ca7_outside "s_id_ca10"
    sbml_compartment_s_id_ca7_size "1.0"
    sbml_compartment_s_id_ca7_units "volume"
  ]
  sbml_compartment_s_id_ca8 [
    sbml_compartment_s_id_ca8_annotation ""
    sbml_compartment_s_id_ca8_id "s_id_ca8"
    sbml_compartment_s_id_ca8_meta_id "s_id_ca8"
    sbml_compartment_s_id_ca8_name "COPII_minus_coated_space_vesicle"
    sbml_compartment_s_id_ca8_non_rdf_annotation ""
    sbml_compartment_s_id_ca8_notes ""
    sbml_compartment_s_id_ca8_outside "s_id_ca10"
    sbml_compartment_s_id_ca8_size "1.0"
    sbml_compartment_s_id_ca8_units "volume"
  ]
  sbml_compartment_s_id_ca9 [
    sbml_compartment_s_id_ca9_annotation ""
    sbml_compartment_s_id_ca9_id "s_id_ca9"
    sbml_compartment_s_id_ca9_meta_id "s_id_ca9"
    sbml_compartment_s_id_ca9_name "autophagosome"
    sbml_compartment_s_id_ca9_non_rdf_annotation ""
    sbml_compartment_s_id_ca9_notes ""
    sbml_compartment_s_id_ca9_outside "s_id_ca10"
    sbml_compartment_s_id_ca9_size "1.0"
    sbml_compartment_s_id_ca9_units "volume"
  ]
  sbml_unit_definition_1 [
    sbml_unit_definition_1_id "substance"
    sbml_unit_definition_1_name "substance"
    sbml_unit_definition_1_sub_unit_1_ "(1.0 * 10^0 * mole)^1.0"
    sbml_unit_definition_1unit "(1.0 * 10^0 * mole)^1.0"
  ]
  sbml_unit_definition_2 [
    sbml_unit_definition_2_id "volume"
    sbml_unit_definition_2_name "volume"
    sbml_unit_definition_2_sub_unit_1_ "(1.0 * 10^0 * litre)^1.0"
    sbml_unit_definition_2unit "(1.0 * 10^0 * litre)^1.0"
  ]
  sbml_unit_definition_3 [
    sbml_unit_definition_3_id "time"
    sbml_unit_definition_3_name "time"
    sbml_unit_definition_3_sub_unit_1_ "(1.0 * 10^0 * second)^1.0"
    sbml_unit_definition_3unit "(1.0 * 10^0 * second)^1.0"
  ]
  sbml_unit_definition_4 [
    sbml_unit_definition_4_id "area"
    sbml_unit_definition_4_name "area"
    sbml_unit_definition_4_sub_unit_1_ "(1.0 * 10^0 * metre)^2.0"
    sbml_unit_definition_4unit "(1.0 * 10^0 * metre)^2.0"
  ]
  sbml_unit_definition_5 [
    sbml_unit_definition_5_id "length"
    sbml_unit_definition_5_name "length"
    sbml_unit_definition_5_sub_unit_1_ "(1.0 * 10^0 * metre)^1.0"
    sbml_unit_definition_5unit "(1.0 * 10^0 * metre)^1.0"
  ]
  directed 0
  node [
    id 1
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 200.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nf_minus_kb__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa19"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa19"
      minerva_fullName "NF-kappaB complex"
      minerva_name "NF-kB"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0071159"
      minerva_ref_type__resource1 "GO__GO:0071159"
      minerva_type "Complex"
      minerva_x 2704.571428571429
      minerva_y 526.4285714285714
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa19"
      species_meta_id "s_id_csa19"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 2
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 300.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "pi__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa15"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa14"
      minerva_elementId2 "sa15"
      minerva_fullName "phosphate(3-)"
      minerva_name "Pi"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18367"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18367"
      minerva_synonyms "Orthophosphate; PHOSPHATE ION; PO4(3-); Phosphate; [PO4](3-)"
      minerva_type "Simple molecule"
      minerva_x 609.0
      minerva_x2 722.0
      minerva_y 1105.0
      minerva_y2 882.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa15"
      species_meta_id "s_id_sa15"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 3
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 400.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cgamp:sting__golgi_space_apparatus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa4"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa4"
      minerva_fullName "STING complex"
      minerva_name "cGAMP:STING"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:1990231"
      minerva_ref_type__resource1 "GO__GO:1990231"
      minerva_type "Complex"
      minerva_x 2701.5
      minerva_y 823.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "Golgi_space_apparatus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa4"
      species_meta_id "s_id_csa4"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 4
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 500.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sting__golgi_space_apparatus__none__2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa100"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa100"
      minerva_homodimer "2"
      minerva_name "STING"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=STING1"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q86WV6"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/340061"
      minerva_ref_type__resource1 "HGNC_SYMBOL__STING1"
      minerva_ref_type__resource2 "UNIPROT__Q86WV6"
      minerva_ref_type__resource3 "ENTREZ__340061"
      minerva_type "Protein"
      minerva_x 2605.0
      minerva_y 748.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "Golgi_space_apparatus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa100"
      species_meta_id "s_id_sa100"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 5
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 600.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ifnb1__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa91"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa91"
      minerva_former_symbols "IFNB"
      minerva_fullName "interferon beta 1"
      minerva_name "IFNB1"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000171855"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IFNB1"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/5434"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P01574"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_002176"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/3456"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000171855"
      minerva_ref_type__resource2 "HGNC_SYMBOL__IFNB1"
      minerva_ref_type__resource3 "HGNC__5434"
      minerva_ref_type__resource4 "UNIPROT__P01574"
      minerva_ref_type__resource5 "REFSEQ__NM_002176"
      minerva_ref_type__resource6 "ENTREZ__3456"
      minerva_synonyms "IFB; IFF"
      minerva_type "Protein"
      minerva_x 2032.5
      minerva_y 291.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa91"
      species_meta_id "s_id_sa91"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 6
    zlevel -1

    cluster [
      cluster "s_id_ca11"
    ]
    graphics [
      x 700.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "uridine__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa37"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa37"
      minerva_fullName "uridine"
      minerva_name "uridine"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16704"
      minerva_ref_type__resource1 "CHEBI__CHEBI:16704"
      minerva_synonyms "1-beta-D-ribofuranosylpyrimidine-2; 4(1H; 3H)-dione; 1-beta-D-ribofuranosyluracil; URIDINE; Urd; Uridin; Uridine; beta-Uridine; u; uridine"
      minerva_type "Simple molecule"
      minerva_x 1255.0
      minerva_y 125.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca11"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa37"
      species_meta_id "s_id_sa37"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 7
    zlevel -1

    cluster [
      cluster "s_id_ca11"
    ]
    graphics [
      x 800.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ifna1__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa86"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa86"
      minerva_fullName "interferon alpha 1"
      minerva_name "IFNA1"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IFNA1"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/3439"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_024013"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P01562"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/5417"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000197919"
      minerva_ref_type__resource1 "HGNC_SYMBOL__IFNA1"
      minerva_ref_type__resource2 "ENTREZ__3439"
      minerva_ref_type__resource3 "REFSEQ__NM_024013"
      minerva_ref_type__resource4 "UNIPROT__P01562"
      minerva_ref_type__resource5 "HGNC__5417"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000197919"
      minerva_synonyms "IFL; IFN; IFN-ALPHA; IFN-alphaD; IFNA13; IFNA@"
      minerva_type "Protein"
      minerva_x 2165.0
      minerva_y 130.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca11"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa86"
      species_meta_id "s_id_sa86"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 8
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 900.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "udp__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa35"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa35"
      minerva_fullName "UDP"
      minerva_name "UDP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17659"
      minerva_ref_type__resource1 "CHEBI__CHEBI:17659"
      minerva_synonyms "UDP; Uridine 5'-diphosphate; Uridine diphosphate"
      minerva_type "Simple molecule"
      minerva_x 1254.0
      minerva_y 505.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa35"
      species_meta_id "s_id_sa35"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 9
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 1000.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "gtp__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa82"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa82"
      minerva_fullName "GTP"
      minerva_name "GTP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15996"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15996"
      minerva_synonyms "5'-GTP; GTP; GUANOSINE-5'-TRIPHOSPHATE; Guanosine 5'-triphosphate; H4gtp; guanosine 5'-triphosphoric acid; guanosine triphosphate"
      minerva_type "Simple molecule"
      minerva_x 1865.0
      minerva_y 855.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa82"
      species_meta_id "s_id_sa82"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 10
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 1100.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "atp__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa9"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa81"
      minerva_elementId2 "sa9"
      minerva_fullName "ATP"
      minerva_name "ATP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15422"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15422"
      minerva_synonyms "ADENOSINE-5'-TRIPHOSPHATE; ATP; Adenosine 5'-triphosphate; Adenosine triphosphate; Adephos; Adetol; Adynol; Atipi; Atriphos; Cardenosine; Fosfobion; Glucobasin; H4atp; Myotriphos; Triadenyl; Triphosphaden"
      minerva_type "Simple molecule"
      minerva_x 1866.0
      minerva_x2 769.0
      minerva_y 794.5
      minerva_y2 1257.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa9"
      species_meta_id "s_id_sa9"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 11
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 1200.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "(s)_minus_dihydroorotate__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa4"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa4"
      minerva_fullName "(S)-dihydroorotate"
      minerva_name "(S)-dihydroorotate"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:30864"
      minerva_ref_type__resource1 "CHEBI__CHEBI:30864"
      minerva_synonyms "(S)-4; 5-dihydroorotate; (S)-dihydroorotate; 4; 5-dihydro-L-orotate"
      minerva_type "Simple molecule"
      minerva_x 666.25
      minerva_y 594.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa4"
      species_meta_id "s_id_sa4"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 12
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 200.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "dna__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa142"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa142"
      minerva_name "DNA"
      minerva_type "Gene"
      minerva_x 1505.0
      minerva_y 935.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa142"
      species_meta_id "s_id_sa142"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 13
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 300.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ctps1__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa43"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa43"
      minerva_former_symbols "CTPS"
      minerva_name "CTPS1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/1503"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/1503"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=6.3.4.2"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_001905"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P17812"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CTPS1"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CTPS1"
      minerva_ref_link8 "https://www.ensembl.org/id/ENSG00000171793"
      minerva_ref_link9 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/2519"
      minerva_ref_type__resource1 "ENTREZ__1503"
      minerva_ref_type__resource2 "ENTREZ__1503"
      minerva_ref_type__resource3 "EC__6.3.4.2"
      minerva_ref_type__resource4 "REFSEQ__NM_001905"
      minerva_ref_type__resource5 "UNIPROT__P17812"
      minerva_ref_type__resource6 "HGNC_SYMBOL__CTPS1"
      minerva_ref_type__resource7 "HGNC_SYMBOL__CTPS1"
      minerva_ref_type__resource8 "ENSEMBL__ENSG00000171793"
      minerva_ref_type__resource9 "HGNC__2519"
      minerva_type "Protein"
      minerva_x 1735.0
      minerva_y 563.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa43"
      species_meta_id "s_id_sa43"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 14
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 400.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "l_minus_asp__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa16"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa16"
      minerva_fullName "L-aspartate(1-)"
      minerva_name "L-Asp"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:29991"
      minerva_ref_type__resource1 "CHEBI__CHEBI:29991"
      minerva_synonyms "(2S)-2-ammoniobutanedioate; (2S)-2-ammoniosuccinate; L-aspartate; L-aspartate; L-aspartate(1-); L-aspartic acid monoanion"
      minerva_type "Simple molecule"
      minerva_x 719.0
      minerva_y 1036.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa16"
      species_meta_id "s_id_sa16"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 15
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 500.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "l_minus_glu__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa10"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa10"
      minerva_fullName "L-glutamic acid"
      minerva_name "L-Glu"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16015"
      minerva_ref_type__resource1 "CHEBI__CHEBI:16015"
      minerva_synonyms "(S)-2-aminopentanedioic acid; (S)-glutamic acid; E; GLUTAMIC ACID; Glu; Glutamate; L-Glu; L-Glutamic acid; L-Glutaminic acid; L-Glutaminsaeure; acide glutamique; acido glutamico; acidum glutamicum; glutamic acid"
      minerva_type "Simple molecule"
      minerva_x 722.0
      minerva_y 1102.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa10"
      species_meta_id "s_id_sa10"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 16
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 600.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "umps__pulmonary_space_endothelial_space_cell__none__2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa25"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa25"
      minerva_fullName "uridine monophosphate synthetase"
      minerva_homodimer "2"
      minerva_name "UMPS"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7372"
      minerva_ref_link2 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.4.2.10"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_000373"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=UMPS"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12563"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000114491"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/P11172"
      minerva_ref_link8 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=4.1.1.23"
      minerva_ref_type__resource1 "ENTREZ__7372"
      minerva_ref_type__resource2 "EC__2.4.2.10"
      minerva_ref_type__resource3 "REFSEQ__NM_000373"
      minerva_ref_type__resource4 "HGNC_SYMBOL__UMPS"
      minerva_ref_type__resource5 "HGNC__12563"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000114491"
      minerva_ref_type__resource7 "UNIPROT__P11172"
      minerva_ref_type__resource8 "EC__4.1.1.23"
      minerva_type "Protein"
      minerva_x 991.0
      minerva_y 327.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa25"
      species_meta_id "s_id_sa25"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 17
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 700.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cmp__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa34"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa34"
      minerva_fullName "cytidine 5'-monophosphate"
      minerva_name "CMP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17361"
      minerva_ref_type__resource1 "CHEBI__CHEBI:17361"
      minerva_synonyms "CMP; Cytidine-5'-monophosphate; Cytidylic acid; cytidine 5'-monophosphate; cytidylate; pC"
      minerva_type "Simple molecule"
      minerva_x 1466.0
      minerva_y 396.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa34"
      species_meta_id "s_id_sa34"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 18
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 800.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cda__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa32"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa32"
      minerva_fullName "cytidine deaminase"
      minerva_name "CDA"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/P32320"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/978"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_001785"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000158825"
      minerva_ref_link5 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.5.4.5"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CDA"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1712"
      minerva_ref_type__resource1 "UNIPROT__P32320"
      minerva_ref_type__resource2 "ENTREZ__978"
      minerva_ref_type__resource3 "REFSEQ__NM_001785"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000158825"
      minerva_ref_type__resource5 "EC__3.5.4.5"
      minerva_ref_type__resource6 "HGNC_SYMBOL__CDA"
      minerva_ref_type__resource7 "HGNC__1712"
      minerva_synonyms "CDD"
      minerva_type "Protein"
      minerva_x 1361.0
      minerva_y 205.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa32"
      species_meta_id "s_id_sa32"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 19
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 900.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "dna_space_biosynthesis__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa141"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa141"
      minerva_fullName "DNA biosynthetic process"
      minerva_name "DNA biosynthesis"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0071897"
      minerva_ref_type__resource1 "GO__GO:0071897"
      minerva_type "Phenotype"
      minerva_x 1533.0
      minerva_y 995.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa141"
      species_meta_id "s_id_sa141"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 20
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 1000.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "dutp__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa47"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa47"
      minerva_fullName "dUTP"
      minerva_name "dUTP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17625"
      minerva_ref_type__resource1 "CHEBI__CHEBI:17625"
      minerva_synonyms "2'-Deoxyuridine 5'-triphosphate; 2'-deoxyuridine 5'-triphosphate; dUTP; deoxyuridine triphosphate"
      minerva_type "Simple molecule"
      minerva_x 1255.0
      minerva_y 715.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa47"
      species_meta_id "s_id_sa47"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 21
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 1100.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "rad50__pulmonary_space_endothelial_space_cell__none__2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa97"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa97"
      minerva_fullName "RAD50 double strand break repair protein"
      minerva_homodimer "2"
      minerva_name "RAD50"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.6.-.-"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/10111"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_005732"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/Q92878"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=RAD50"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000113522"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/9816"
      minerva_ref_type__resource1 "EC__3.6.-.-"
      minerva_ref_type__resource2 "ENTREZ__10111"
      minerva_ref_type__resource3 "REFSEQ__NM_005732"
      minerva_ref_type__resource4 "UNIPROT__Q92878"
      minerva_ref_type__resource5 "HGNC_SYMBOL__RAD50"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000113522"
      minerva_ref_type__resource7 "HGNC__9816"
      minerva_synonyms "RAD50-2; hRad50"
      minerva_type "Protein"
      minerva_x 2007.0
      minerva_y 1262.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa97"
      species_meta_id "s_id_sa97"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 22
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 1200.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "co2__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa26"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa26"
      minerva_fullName "carbon dioxide"
      minerva_name "CO2"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16526"
      minerva_ref_type__resource1 "CHEBI__CHEBI:16526"
      minerva_synonyms "CARBON DIOXIDE; CO2; CO2; Carbon dioxide; E 290; E-290; E290; R-744; [CO2]; carbonic anhydride"
      minerva_type "Simple molecule"
      minerva_x 1157.0
      minerva_y 443.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa26"
      species_meta_id "s_id_sa26"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 23
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 200.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "lc3__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa94"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa94"
      minerva_name "LC3"
      minerva_type "Protein"
      minerva_x 2829.0
      minerva_y 976.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa94"
      species_meta_id "s_id_sa94"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 24
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 300.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "dtdp__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa60"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa60"
      minerva_fullName "dTDP"
      minerva_name "dTDP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18075"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18075"
      minerva_synonyms "2'-deoxyribosylthymine 5'-(trihydrogen diphosphate); Deoxythymidine 5'-diphosphate; THYMIDINE-5'- DIPHOSPHATE; dTDP; deoxy-TDP; thymidine 5'-diphosphate; thymidine 5'-pyrophosphate"
      minerva_type "Simple molecule"
      minerva_x 1445.0
      minerva_y 1062.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa60"
      species_meta_id "s_id_sa60"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 25
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 400.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "dcdp__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa46"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa46"
      minerva_fullName "dCDP"
      minerva_name "dCDP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:28846"
      minerva_ref_type__resource1 "CHEBI__CHEBI:28846"
      minerva_synonyms "2'-Deoxycytidine 5'-diphosphate; 2'-Deoxycytidine diphosphate; D-1beta-Ribofuranosylcytosine diphosphate; dCDP; deoxycytidine diphosphate"
      minerva_type "Simple molecule"
      minerva_x 1462.0
      minerva_y 621.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa46"
      species_meta_id "s_id_sa46"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 26
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 500.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "dudp__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa45"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa45"
      minerva_fullName "dUDP"
      minerva_name "dUDP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:28850"
      minerva_ref_type__resource1 "CHEBI__CHEBI:28850"
      minerva_synonyms "2'-Deoxyuridine 5'-diphosphate; dUDP"
      minerva_type "Simple molecule"
      minerva_x 1254.0
      minerva_y 623.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa45"
      species_meta_id "s_id_sa45"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 27
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 600.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nme1__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa51"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa42"
      minerva_elementId2 "sa51"
      minerva_name "NME1"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "Protein"
      minerva_x 1565.0
      minerva_x2 1363.0
      minerva_y 572.0
      minerva_y2 669.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa51"
      species_meta_id "s_id_sa51"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 28
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 700.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "p1788__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa64"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa64"
      minerva_name "P1788"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/31740051"
      minerva_ref_type__resource1 "PUBMED__31740051"
      minerva_type "Simple molecule"
      minerva_x 779.0
      minerva_y 557.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa64"
      species_meta_id "s_id_sa64"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 29
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 800.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ribonucleoside_minus_diphosphate_space_reductase__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa25"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa25"
      minerva_fullName "ribonucleoside-diphosphate reductase complex"
      minerva_name "ribonucleoside-diphosphate reductase"
      minerva_ref_link1 "https://www.brenda-enzymes.org/php/result_flat.php4?ecno=1.17.4.1"
      minerva_ref_link2 "http://amigo.geneontology.org/amigo/term/GO:0005971"
      minerva_ref_type__resource1 "BRENDA__1.17.4.1"
      minerva_ref_type__resource2 "GO__GO:0005971"
      minerva_type "Complex"
      minerva_x 1364.0
      minerva_y 577.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa25"
      species_meta_id "s_id_csa25"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 30
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 900.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "dctpp1__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa56"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa56"
      minerva_name "DCTPP1"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DCTPP1"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DCTPP1"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/28777"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/Q9H773"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000179958"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.6.1.12"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_024096"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/79077"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/79077"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DCTPP1"
      minerva_ref_type__resource2 "HGNC_SYMBOL__DCTPP1"
      minerva_ref_type__resource3 "HGNC__28777"
      minerva_ref_type__resource4 "UNIPROT__Q9H773"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000179958"
      minerva_ref_type__resource6 "EC__3.6.1.12"
      minerva_ref_type__resource7 "REFSEQ__NM_024096"
      minerva_ref_type__resource8 "ENTREZ__79077"
      minerva_ref_type__resource9 "ENTREZ__79077"
      minerva_type "Protein"
      minerva_x 1355.0
      minerva_y 772.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa56"
      species_meta_id "s_id_sa56"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 31
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 1000.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "dttp__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa62"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa62"
      minerva_fullName "dTTP"
      minerva_name "dTTP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18077"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18077"
      minerva_synonyms "2'-deoxythymidine triphosphate; 5'-TTP; Deoxythymidine 5'-triphosphate; Deoxythymidine triphosphate; THYMIDINE-5'-TRIPHOSPHATE; TTP; dTTP; dThd5'PPP; deoxy-TTP; pppdT; thymidine 5'-triphosphate"
      minerva_type "Simple molecule"
      minerva_x 1648.0
      minerva_y 1062.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa62"
      species_meta_id "s_id_sa62"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 32
    zlevel -1

    cluster [
      cluster "s_id_ca11"
    ]
    graphics [
      x 1100.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cytidine__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa38"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa38"
      minerva_fullName "cytidine"
      minerva_name "cytidine"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17562"
      minerva_ref_type__resource1 "CHEBI__CHEBI:17562"
      minerva_synonyms "1-beta-D-Ribofuranosylcytosine; 1beta-D-ribofuranosylcytosine; 4-AMINO-1-BETA-D-RIBOFURANOSYL-2(1H)-PYRIMIDINONE; 4-amino-1-beta-D-ribofuranosylpyrimidin-2(1H)-one; 4-amino-1beta-D-ribofuranosyl-2(1H)-pyrimidinone; Cyd; Cytidin; Cytidine; Cytosine riboside; Zytidin; cytidine; cytosine-1beta-D-Ribofuranoside"
      minerva_type "Simple molecule"
      minerva_x 1460.0
      minerva_y 127.25
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca11"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa38"
      species_meta_id "s_id_sa38"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 33
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 1200.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mre11__pulmonary_space_endothelial_space_cell__none__2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa96"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa96"
      minerva_former_symbols "MRE11A"
      minerva_fullName "MRE11 homolog, double strand break repair nuclease"
      minerva_homodimer "2"
      minerva_name "MRE11"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MRE11"
      minerva_ref_link2 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.1.-.-"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/4361"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P49959"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000020922"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_005591"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7230"
      minerva_ref_type__resource1 "HGNC_SYMBOL__MRE11"
      minerva_ref_type__resource2 "EC__3.1.-.-"
      minerva_ref_type__resource3 "ENTREZ__4361"
      minerva_ref_type__resource4 "UNIPROT__P49959"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000020922"
      minerva_ref_type__resource6 "REFSEQ__NM_005591"
      minerva_ref_type__resource7 "HGNC__7230"
      minerva_synonyms "ATLD"
      minerva_type "Protein"
      minerva_x 1821.0
      minerva_y 1167.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa96"
      species_meta_id "s_id_sa96"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 34
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 200.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "n_minus_carbamoyl_minus_l_minus_aspartate__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa3"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa3"
      minerva_fullName "N-carbamoyl-L-aspartate(2-)"
      minerva_name "N-carbamoyl-L-aspartate"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:32814"
      minerva_ref_type__resource1 "CHEBI__CHEBI:32814"
      minerva_synonyms "L-ureidosuccinate; N-carbamoyl-L-aspartate; N-carbamoyl-L-aspartate; N-carbamoyl-L-aspartate dianion"
      minerva_type "Simple molecule"
      minerva_x 666.0
      minerva_y 841.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa3"
      species_meta_id "s_id_sa3"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 35
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 300.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cad__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa19"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa19"
      minerva_fullName "carbamoyl-phosphate synthetase 2, aspartate transcarbamylase, and dihydroorotase"
      minerva_name "CAD"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_001306079"
      minerva_ref_link2 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.5.2.3"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1424"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CAD"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P27708"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/790"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000084774"
      minerva_ref_link8 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=6.3.5.5"
      minerva_ref_link9 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.1.3.2"
      minerva_ref_type__resource1 "REFSEQ__NM_001306079"
      minerva_ref_type__resource2 "EC__3.5.2.3"
      minerva_ref_type__resource3 "HGNC__1424"
      minerva_ref_type__resource4 "HGNC_SYMBOL__CAD"
      minerva_ref_type__resource5 "UNIPROT__P27708"
      minerva_ref_type__resource6 "ENTREZ__790"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000084774"
      minerva_ref_type__resource8 "EC__6.3.5.5"
      minerva_ref_type__resource9 "EC__2.1.3.2"
      minerva_synonyms "GATD4"
      minerva_type "Protein"
      minerva_x 481.0
      minerva_y 957.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa19"
      species_meta_id "s_id_sa19"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 36
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 400.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "coq__mitochondrion__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa21"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa21"
      minerva_fullName "ubiquinones"
      minerva_name "CoQ"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16389"
      minerva_ref_type__resource1 "CHEBI__CHEBI:16389"
      minerva_synonyms "CoQ; Coenzym Q; Coenzyme Q; Koenzym Q; Q; Ubichinon; Ubiquinone; Ubiquinones; a ubiquinone; coenzyme Q; coenzymes Q; mitochondrial ubiquinone; mitochondrial ubiquinones; mitoquinones"
      minerva_type "Simple molecule"
      minerva_x 483.5
      minerva_y 553.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrion"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa21"
      species_meta_id "s_id_sa21"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 37
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 500.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "dcmp__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa53"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa53"
      minerva_fullName "2'-deoxycytosine 5'-monophosphate"
      minerva_name "dCMP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15918"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15918"
      minerva_synonyms "2'-DEOXYCYTIDINE-5'-MONOPHOSPHATE; 2'-Deoxycytidine 5'-monophosphate; 2'-deoxycytosine 5'-monophosphate; Deoxycytidine monophosphate; Deoxycytidylate; Deoxycytidylic acid; dCMP"
      minerva_type "Simple molecule"
      minerva_x 1466.0
      minerva_y 828.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa53"
      species_meta_id "s_id_sa53"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 38
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 600.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "uridine__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa30"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa30"
      minerva_fullName "uridine"
      minerva_name "uridine"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16704"
      minerva_ref_type__resource1 "CHEBI__CHEBI:16704"
      minerva_synonyms "1-beta-D-ribofuranosylpyrimidine-2; 4(1H; 3H)-dione; 1-beta-D-ribofuranosyluracil; URIDINE; Urd; Uridin; Uridine; beta-Uridine; u; uridine"
      minerva_type "Simple molecule"
      minerva_x 1256.0
      minerva_y 263.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa30"
      species_meta_id "s_id_sa30"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 39
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 700.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tbk1__golgi_space_apparatus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa80"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa80"
      minerva_fullName "TANK binding kinase 1"
      minerva_name "TBK1"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TBK1"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000183735"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/Q9UHD2"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.1"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_013254"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11584"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/29110"
      minerva_ref_type__resource1 "HGNC_SYMBOL__TBK1"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000183735"
      minerva_ref_type__resource3 "UNIPROT__Q9UHD2"
      minerva_ref_type__resource4 "EC__2.7.11.1"
      minerva_ref_type__resource5 "REFSEQ__NM_013254"
      minerva_ref_type__resource6 "HGNC__11584"
      minerva_ref_type__resource7 "ENTREZ__29110"
      minerva_synonyms "NAK"
      minerva_type "Protein"
      minerva_x 2735.0
      minerva_y 689.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "Golgi_space_apparatus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa80"
      species_meta_id "s_id_sa80"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 40
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 800.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "h_underscore_plus__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa17"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa17"
      minerva_fullName "hydron"
      minerva_name "H_plus"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15378"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15378"
      minerva_synonyms "H(+); H+; Hydron"
      minerva_type "Simple molecule"
      minerva_x 727.0
      minerva_y 801.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa17"
      species_meta_id "s_id_sa17"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 41
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 900.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "h2o__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa18"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa8"
      minerva_elementId2 "sa18"
      minerva_fullName "water"
      minerva_name "H2O"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15377"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15377"
      minerva_synonyms "BOUND WATER; H2O; H2O; HOH; WATER; Wasser; Water; [OH2]; acqua; agua; aqua; dihydridooxygen; dihydrogen oxide; eau; hydrogen hydroxide"
      minerva_type "Simple molecule"
      minerva_x 715.0
      minerva_x2 611.0
      minerva_y 1288.0
      minerva_y2 637.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa18"
      species_meta_id "s_id_sa18"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 42
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 1000.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orotate__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa6"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa6"
      minerva_fullName "orotate"
      minerva_name "orotate"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:30839"
      minerva_ref_type__resource1 "CHEBI__CHEBI:30839"
      minerva_synonyms "orotate"
      minerva_type "Simple molecule"
      minerva_x 664.0
      minerva_y 396.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa6"
      species_meta_id "s_id_sa6"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 43
    zlevel -1

    cluster [
      cluster "s_id_ca6"
    ]
    graphics [
      x 1100.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ifnb1_space_expression_space_complex__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa8"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa8"
      minerva_name "IFNB1 expression complex"
      minerva_type "Complex"
      minerva_x 2415.0
      minerva_y 525.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca6"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa8"
      species_meta_id "s_id_csa8"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 44
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 1200.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cgas__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa78"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa78"
      minerva_former_symbols "C6orf150; MB21D1"
      minerva_fullName "cyclic GMP-AMP synthase"
      minerva_name "cGAS"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000164430"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/21367"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/115004"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/Q8N884"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CGAS"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_138441"
      minerva_ref_link7 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.7.86"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000164430"
      minerva_ref_type__resource2 "HGNC__21367"
      minerva_ref_type__resource3 "ENTREZ__115004"
      minerva_ref_type__resource4 "UNIPROT__Q8N884"
      minerva_ref_type__resource5 "HGNC_SYMBOL__CGAS"
      minerva_ref_type__resource6 "REFSEQ__NM_138441"
      minerva_ref_type__resource7 "EC__2.7.7.86"
      minerva_type "Protein"
      minerva_x 1773.0
      minerva_y 877.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa78"
      species_meta_id "s_id_sa78"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 45
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 200.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "dtymk__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa61"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa61"
      minerva_name "dTYMK"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/1841"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/1841"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_012145"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DTYMK"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000168393"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DTYMK"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P23919"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/3061"
      minerva_ref_link9 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.9"
      minerva_ref_type__resource1 "ENTREZ__1841"
      minerva_ref_type__resource2 "ENTREZ__1841"
      minerva_ref_type__resource3 "REFSEQ__NM_012145"
      minerva_ref_type__resource4 "HGNC_SYMBOL__DTYMK"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000168393"
      minerva_ref_type__resource6 "HGNC_SYMBOL__DTYMK"
      minerva_ref_type__resource7 "UNIPROT__P23919"
      minerva_ref_type__resource8 "HGNC__3061"
      minerva_ref_type__resource9 "EC__2.7.4.9"
      minerva_type "Protein"
      minerva_x 1380.0
      minerva_y 965.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa61"
      species_meta_id "s_id_sa61"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 46
    zlevel -1

    cluster [
      cluster "s_id_ca4"
    ]
    graphics [
      x 300.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cgamp:sting__endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa3"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa3"
      minerva_fullName "STING complex"
      minerva_name "cGAMP:STING"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:1990231"
      minerva_ref_type__resource1 "GO__GO:1990231"
      minerva_type "Complex"
      minerva_x 2494.5
      minerva_y 823.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca4"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa3"
      species_meta_id "s_id_csa3"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 47
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 400.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "virus_space_replication__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa143"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa143"
      minerva_fullName "viral genome replication"
      minerva_name "virus replication"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0019079"
      minerva_ref_type__resource1 "GO__GO:0019079"
      minerva_type "Phenotype"
      minerva_x 1607.0
      minerva_y 851.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa143"
      species_meta_id "s_id_sa143"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 48
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 500.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "prpp__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa29"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa29"
      minerva_fullName "5-O-phosphono-alpha-D-ribofuranosyl diphosphate"
      minerva_name "PRPP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17111"
      minerva_ref_type__resource1 "CHEBI__CHEBI:17111"
      minerva_synonyms "5-Phospho-alpha-D-ribose 1-diphosphate; 5-Phosphoribosyl 1-pyrophosphate; 5-Phosphoribosyl diphosphate; ALPHA-PHOSPHORIBOSYLPYROPHOSPHORIC ACID; PRPP; PRib-PP; alpha-D-ribofuranose 5-(dihydrogen phosphate) 1-(trihydrogen diphosphate); phosphoribosyl pyrophosphate; phosphoribosylpyrophosphate"
      minerva_type "Simple molecule"
      minerva_x 710.0
      minerva_y 450.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa29"
      species_meta_id "s_id_sa29"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 49
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 600.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "irf3__pulmonary_space_endothelial_space_cell__phosphorylated__2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa85"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa85"
      minerva_fullName "interferon regulatory factor 3"
      minerva_homodimer "2"
      minerva_name "IRF3"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6118"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14653"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000126456"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_001571"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IRF3"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/3661"
      minerva_ref_type__resource1 "HGNC__6118"
      minerva_ref_type__resource2 "UNIPROT__Q14653"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000126456"
      minerva_ref_type__resource4 "REFSEQ__NM_001571"
      minerva_ref_type__resource5 "HGNC_SYMBOL__IRF3"
      minerva_ref_type__resource6 "ENTREZ__3661"
      minerva_state1 "PHOSPHORYLATED"
      minerva_type "Protein"
      minerva_x 2700.0
      minerva_y 435.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa85"
      species_meta_id "s_id_sa85"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 50
    zlevel -1

    cluster [
      cluster "s_id_ca4"
    ]
    graphics [
      x 700.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mre11:rad50:dna__endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa24"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa24"
      minerva_name "MRE11:RAD50:DNA"
      minerva_type "Complex"
      minerva_x 2282.285714285714
      minerva_y 950.1428571428571
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca4"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa24"
      species_meta_id "s_id_csa24"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 51
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 800.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orotidine_space_5'_minus_monophosphate__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa23"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa23"
      minerva_fullName "orotidine 5'-phosphate"
      minerva_name "orotidine 5'-monophosphate"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15842"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15842"
      minerva_synonyms "6-carboxy-5'-uridylic acid; Orotidine 5'-phosphate; Orotidylic acid; orotidine 5'-(dihydrogen phosphate)"
      minerva_type "Simple molecule"
      minerva_x 989.0
      minerva_y 396.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa23"
      species_meta_id "s_id_sa23"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 52
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 900.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cap__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2"
      minerva_fullName "carbamoyl phosphate"
      minerva_name "CAP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17672"
      minerva_ref_type__resource1 "CHEBI__CHEBI:17672"
      minerva_synonyms "Carbamoyl phosphate; Carbamyl phosphate; PHOSPHORIC ACID MONO(FORMAMIDE)ESTER; aminocarbonyl dihydrogen phosphate; carbamic phosphoric monoanhydride; monocarbamoyl phosphate"
      minerva_type "Simple molecule"
      minerva_x 666.0
      minerva_y 1070.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2"
      species_meta_id "s_id_sa2"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 53
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 1000.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ifna1__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa90"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa90"
      minerva_fullName "interferon alpha 1"
      minerva_name "IFNA1"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IFNA1"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/3439"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_024013"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P01562"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/5417"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000197919"
      minerva_ref_type__resource1 "HGNC_SYMBOL__IFNA1"
      minerva_ref_type__resource2 "ENTREZ__3439"
      minerva_ref_type__resource3 "REFSEQ__NM_024013"
      minerva_ref_type__resource4 "UNIPROT__P01562"
      minerva_ref_type__resource5 "HGNC__5417"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000197919"
      minerva_synonyms "IFL; IFN; IFN-ALPHA; IFN-alphaD; IFNA13; IFNA@"
      minerva_type "Protein"
      minerva_x 2132.75
      minerva_y 294.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa90"
      species_meta_id "s_id_sa90"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 54
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 1100.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nf_minus_kb__endolysosome__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa6"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa6"
      minerva_fullName "NF-kappaB complex"
      minerva_name "NF-kB"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0071159"
      minerva_ref_type__resource1 "GO__GO:0071159"
      minerva_type "Complex"
      minerva_x 2893.0
      minerva_y 526.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "endolysosome"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa6"
      species_meta_id "s_id_csa6"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 55
    zlevel -1

    cluster [
      cluster "s_id_ca4"
    ]
    graphics [
      x 1200.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sting__endoplasmic_space_reticulum__none__2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa98"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa98"
      minerva_former_symbols "TMEM173"
      minerva_homodimer "2"
      minerva_name "STING"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=STING1"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=STING1"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/Q86WV6"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/340061"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/340061"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/27962"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_198282"
      minerva_ref_link8 "https://www.ensembl.org/id/ENSG00000184584"
      minerva_ref_type__resource1 "HGNC_SYMBOL__STING1"
      minerva_ref_type__resource2 "HGNC_SYMBOL__STING1"
      minerva_ref_type__resource3 "UNIPROT__Q86WV6"
      minerva_ref_type__resource4 "ENTREZ__340061"
      minerva_ref_type__resource5 "ENTREZ__340061"
      minerva_ref_type__resource6 "HGNC__27962"
      minerva_ref_type__resource7 "REFSEQ__NM_198282"
      minerva_ref_type__resource8 "ENSEMBL__ENSG00000184584"
      minerva_type "Protein"
      minerva_x 2255.0
      minerva_y 745.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca4"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "true"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa98"
      species_meta_id "s_id_sa98"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 57
    zlevel -1

    cluster [
      cluster "s_id_ca11"
    ]
    graphics [
      x 300.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "hcovs_space_ifn_space_induction_space_wp4880__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa178"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa178"
      minerva_name "HCoVs IFN induction WP4880"
      minerva_type "Phenotype"
      minerva_x 2400.0
      minerva_y 93.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca11"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa178"
      species_meta_id "s_id_sa178"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 58
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 400.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "uck2__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa33"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa33"
      minerva_former_symbols "UMPK"
      minerva_name "UCK2"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7371"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=UCK2"
      minerva_ref_link11 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=UCK2"
      minerva_ref_link12 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.1.48"
      minerva_ref_link13 "https://www.ensembl.org/id/ENSG00000143179"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=UCK1"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7371"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/Q9HA47"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_012474"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12562"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/Q9BZX2"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/83549"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q9BZX2"
      minerva_ref_type__resource1 "ENTREZ__7371"
      minerva_ref_type__resource10 "HGNC_SYMBOL__UCK2"
      minerva_ref_type__resource11 "HGNC_SYMBOL__UCK2"
      minerva_ref_type__resource12 "EC__2.7.1.48"
      minerva_ref_type__resource13 "ENSEMBL__ENSG00000143179"
      minerva_ref_type__resource2 "HGNC_SYMBOL__UCK1"
      minerva_ref_type__resource3 "ENTREZ__7371"
      minerva_ref_type__resource4 "UNIPROT__Q9HA47"
      minerva_ref_type__resource5 "REFSEQ__NM_012474"
      minerva_ref_type__resource6 "HGNC__12562"
      minerva_ref_type__resource7 "UNIPROT__Q9BZX2"
      minerva_ref_type__resource8 "ENTREZ__83549"
      minerva_ref_type__resource9 "UNIPROT__Q9BZX2"
      minerva_type "Protein"
      minerva_x 1358.5625
      minerva_y 328.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa33"
      species_meta_id "s_id_sa33"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 59
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 500.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "irf3__endolysosome__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa84"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa84"
      minerva_fullName "interferon regulatory factor 3"
      minerva_name "IRF3"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6118"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14653"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000126456"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_001571"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IRF3"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/3661"
      minerva_ref_type__resource1 "HGNC__6118"
      minerva_ref_type__resource2 "UNIPROT__Q14653"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000126456"
      minerva_ref_type__resource4 "REFSEQ__NM_001571"
      minerva_ref_type__resource5 "HGNC_SYMBOL__IRF3"
      minerva_ref_type__resource6 "ENTREZ__3661"
      minerva_type "Protein"
      minerva_x 3045.0
      minerva_y 430.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "endolysosome"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa84"
      species_meta_id "s_id_sa84"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 60
    zlevel -1

    cluster [
      cluster "s_id_ca11"
    ]
    graphics [
      x 600.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "interferon_space_1_space_pathway__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa176"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa176"
      minerva_name "Interferon 1 pathway"
      minerva_type "Phenotype"
      minerva_x 2085.0
      minerva_y 55.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca11"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa176"
      species_meta_id "s_id_sa176"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 61
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 700.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "rna_space_biosynthesis__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa140"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa140"
      minerva_fullName "RNA biosynthetic process"
      minerva_name "RNA biosynthesis"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0032774"
      minerva_ref_type__resource1 "GO__GO:0032774"
      minerva_type "Phenotype"
      minerva_x 1885.0
      minerva_y 610.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa140"
      species_meta_id "s_id_sa140"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 62
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 800.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cmpk__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa39"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa39"
      minerva_former_symbols "CMPK"
      minerva_name "CMPK"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CMPK1"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_016308"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CMPK1"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/18170"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000162368"
      minerva_ref_link5 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.14"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/51727"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/51727"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P30085"
      minerva_ref_type__resource1 "HGNC_SYMBOL__CMPK1"
      minerva_ref_type__resource10 "REFSEQ__NM_016308"
      minerva_ref_type__resource2 "HGNC_SYMBOL__CMPK1"
      minerva_ref_type__resource3 "HGNC__18170"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000162368"
      minerva_ref_type__resource5 "EC__2.7.4.14"
      minerva_ref_type__resource6 "EC__2.7.4.6"
      minerva_ref_type__resource7 "ENTREZ__51727"
      minerva_ref_type__resource8 "ENTREZ__51727"
      minerva_ref_type__resource9 "UNIPROT__P30085"
      minerva_type "Protein"
      minerva_x 1357.0
      minerva_y 451.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa39"
      species_meta_id "s_id_sa39"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 63
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 900.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "hco3_underscore_minus__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa7"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa7"
      minerva_fullName "hydrogencarbonate"
      minerva_name "HCO3_minus"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17544"
      minerva_ref_type__resource1 "CHEBI__CHEBI:17544"
      minerva_synonyms "Acid carbonate; BICARBONATE ION; Bicarbonate; HCO3(-); HCO3-; Hydrogencarbonate; [CO2(OH)](-); hydrogen carbonate; hydrogencarbonate"
      minerva_type "Simple molecule"
      minerva_x 621.0
      minerva_y 1287.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa7"
      species_meta_id "s_id_sa7"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 64
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 1000.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ctp__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa41"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa41"
      minerva_fullName "CTP"
      minerva_name "CTP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17677"
      minerva_ref_type__resource1 "CHEBI__CHEBI:17677"
      minerva_synonyms "5'-CTP; CTP; CYTIDINE-5'-TRIPHOSPHATE; Cytidine 5'-triphosphate; Cytidine triphosphate; H4ctp"
      minerva_type "Simple molecule"
      minerva_x 1663.0
      minerva_y 505.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa41"
      species_meta_id "s_id_sa41"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 65
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 1100.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mre11:dsdna__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa23"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa23"
      minerva_fullName "double-stranded DNA binding"
      minerva_name "MRE11:dsDNA"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0003690"
      minerva_ref_type__resource1 "GO__GO:0003690"
      minerva_type "Complex"
      minerva_x 2003.321428571429
      minerva_y 1115.142857142857
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa23"
      species_meta_id "s_id_csa23"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 66
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 1200.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ppi__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa28"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa28"
      minerva_fullName "diphosphate(4-)"
      minerva_name "PPi"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18361"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18361"
      minerva_synonyms "DIPHOSPHATE; Diphosphat; Diphosphate; P2O7(4-); PPi; Pyrophosphat; Pyrophosphate; [O3POPO3](4-); pyrophosphate ion"
      minerva_type "Simple molecule"
      minerva_x 905.0
      minerva_y 450.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa28"
      species_meta_id "s_id_sa28"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 67
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 200.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "dsdna__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa77"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa77"
      minerva_fullName "double-stranded DNA fragment"
      minerva_name "dsDNA"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:75909"
      minerva_ref_type__resource1 "CHEBI__CHEBI:75909"
      minerva_synonyms "double-strand DNA fragment; dsDNA fragment; duplex DNA fragment"
      minerva_type "Simple molecule"
      minerva_x 1735.0
      minerva_y 935.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa77"
      species_meta_id "s_id_sa77"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 68
    zlevel -1

    cluster [
      cluster "s_id_ca11"
    ]
    graphics [
      x 300.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "hcovs_space_type_space_i_space_ifn_space_signalling(wp4868)__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa175"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa175"
      minerva_name "HCoVs Type I Ifn signalling(WP4868)"
      minerva_type "Phenotype"
      minerva_x 1755.0
      minerva_y 85.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca11"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa175"
      species_meta_id "s_id_sa175"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 69
    zlevel -1

    cluster [
      cluster "s_id_ca11"
    ]
    graphics [
      x 400.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ifnb1__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa87"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa87"
      minerva_former_symbols "IFNB"
      minerva_fullName "interferon beta 1"
      minerva_name "IFNB1"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000171855"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IFNB1"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/5434"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P01574"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_002176"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/3456"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000171855"
      minerva_ref_type__resource2 "HGNC_SYMBOL__IFNB1"
      minerva_ref_type__resource3 "HGNC__5434"
      minerva_ref_type__resource4 "UNIPROT__P01574"
      minerva_ref_type__resource5 "REFSEQ__NM_002176"
      minerva_ref_type__resource6 "ENTREZ__3456"
      minerva_synonyms "IFB; IFF"
      minerva_type "Protein"
      minerva_x 2000.0
      minerva_y 130.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca11"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa87"
      species_meta_id "s_id_sa87"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 70
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 500.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ump__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa27"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa27"
      minerva_fullName "uridine 5'-monophosphate(2-)"
      minerva_name "UMP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:57865"
      minerva_ref_type__resource1 "CHEBI__CHEBI:57865"
      minerva_synonyms "5'-uridylate; UMP; UMP dianion; UMP(2-); uridine 5'-phosphate"
      minerva_type "Simple molecule"
      minerva_x 1256.0
      minerva_y 398.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa27"
      species_meta_id "s_id_sa27"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 71
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 600.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "adp__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa13"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa13"
      minerva_fullName "ADP"
      minerva_name "ADP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16761"
      minerva_ref_type__resource1 "CHEBI__CHEBI:16761"
      minerva_synonyms "5'-adenylphosphoric acid; ADENOSINE-5'-DIPHOSPHATE; ADP; Adenosine 5'-diphosphate; H3adp"
      minerva_type "Simple molecule"
      minerva_x 756.0
      minerva_y 1139.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa13"
      species_meta_id "s_id_sa13"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 72
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 700.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "dump__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa52"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa52"
      minerva_fullName "dUMP"
      minerva_name "dUMP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17622"
      minerva_ref_type__resource1 "CHEBI__CHEBI:17622"
      minerva_synonyms "2'-Deoxyuridine 5'-phosphate; 2'-deoxyuridine 5'-monophosphate; Deoxyuridine 5'-phosphate; Deoxyuridine monophosphate; Deoxyuridylic acid; dUMP; deoxyuridylate"
      minerva_type "Simple molecule"
      minerva_x 1253.0
      minerva_y 830.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa52"
      species_meta_id "s_id_sa52"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 73
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 800.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cdp__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa36"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa36"
      minerva_fullName "CDP"
      minerva_name "CDP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17239"
      minerva_ref_type__resource1 "CHEBI__CHEBI:17239"
      minerva_synonyms "5'-CDP; CDP; CYTIDINE-5'-DIPHOSPHATE; Cytidine 5'-diphosphate; Cytidine 5'-diphosphoric acid; Cytidine 5'-pyrophosphate; Cytidine diphosphate; Cytidine; 5'-(trihydrogen pyrophosphate)"
      minerva_type "Simple molecule"
      minerva_x 1464.0
      minerva_y 505.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa36"
      species_meta_id "s_id_sa36"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 74
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 900.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sting:tbk1__golgi_space_apparatus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa20"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa20"
      minerva_name "STING:TBK1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/30842653"
      minerva_ref_type__resource1 "PUBMED__30842653"
      minerva_type "Complex"
      minerva_x 2944.071428571429
      minerva_y 772.4285714285714
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "Golgi_space_apparatus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa20"
      species_meta_id "s_id_csa20"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 75
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 1000.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "dna_space_damage__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa163"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa163"
      minerva_name "DNA damage"
      minerva_type "Phenotype"
      minerva_x 1406.0
      minerva_y 1169.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa163"
      species_meta_id "s_id_sa163"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 76
    zlevel -1

    cluster [
      cluster "s_id_ca7"
    ]
    graphics [
      x 1100.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cgamp_minus_sting__endoplasmic_space_reticulum_minus_golgi_space_intermediate_space_compartment__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa16"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa16"
      minerva_name "cGAMP-STING"
      minerva_type "Complex"
      minerva_x 2790.5
      minerva_y 1067.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca7"
      compartment_name "endoplasmic_space_reticulum_minus_Golgi_space_intermediate_space_compartment"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa16"
      species_meta_id "s_id_csa16"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 77
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 1200.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s152__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa179"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa179"
      minerva_name "s152"
      minerva_type "Degraded"
      minerva_x 1735.0
      minerva_y 1025.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa179"
      species_meta_id "s_id_sa179"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 78
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 200.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cgamp__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa83"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa83"
      minerva_fullName "2'-3'-cGAMP"
      minerva_name "cGAMP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:75947"
      minerva_ref_type__resource1 "CHEBI__CHEBI:75947"
      minerva_synonyms "2',5'-3',5'-cGAMP; 2'-3'-cyclic GMP-AMP; 2'-O,5'-O-[(adenosine-3'-O,5'-O-diyl)bisphosphinico]guanosine; c[G(2',5')pA(3',5')p]; cyclic (guanosine-(2'->5')-monophosphate-adenosine-(3'->5')-monophosphate); cyclic guanosine monophosphate-adenosine monophosphate"
      minerva_type "Simple molecule"
      minerva_x 2097.0
      minerva_y 822.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa83"
      species_meta_id "s_id_sa83"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 79
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 300.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "dctp__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa48"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa48"
      minerva_fullName "dCTP"
      minerva_name "dCTP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16311"
      minerva_ref_type__resource1 "CHEBI__CHEBI:16311"
      minerva_synonyms "2'-Deoxycytidine 5'-triphosphate; Deoxycytidine 5'-triphosphate; Deoxycytidine triphosphate; dCTP"
      minerva_type "Simple molecule"
      minerva_x 1465.0
      minerva_y 715.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa48"
      species_meta_id "s_id_sa48"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 80
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 400.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "teriflunomide__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa76"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa76"
      minerva_name "Teriflunomide"
      minerva_ref_link1 "http://www.drugbank.ca/drugs/DB08880"
      minerva_ref_type__resource1 "DRUGBANK__DB08880"
      minerva_type "Drug"
      minerva_x 840.0
      minerva_y 493.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa76"
      species_meta_id "s_id_sa76"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 81
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 500.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "l_minus_gln__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa1"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa1"
      minerva_fullName "L-glutamine"
      minerva_name "L-Gln"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:18050"
      minerva_ref_type__resource1 "CHEBI__CHEBI:18050"
      minerva_synonyms "(2S)-2,5-diamino-5-oxopentanoic acid; (2S)-2-amino-4-carbamoylbutanoic acid; (S)-2,5-diamino-5-oxopentanoic acid; GLUTAMINE; Glutamic acid 5-amide; Glutamic acid amide; L-(+)-glutamine; L-2-Aminoglutaramic acid; L-2-aminoglutaramic acid; L-2-aminoglutaramic acid; L-Glutamin; L-Glutamine; L-Glutaminsaeure-5-amid; L-glutamic acid gamma-amide; Levoglutamide; Q"
      minerva_type "Simple molecule"
      minerva_x 671.0
      minerva_y 1327.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa1"
      species_meta_id "s_id_sa1"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 82
    zlevel -1

    cluster [
      cluster "s_id_ca9"
    ]
    graphics [
      x 600.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cgamp:sting:lc3__autophagosome__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa13"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa13"
      minerva_name "cGAMP:STING:LC3"
      minerva_type "Complex"
      minerva_x 3039.5
      minerva_y 1041.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca9"
      compartment_name "autophagosome"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa13"
      species_meta_id "s_id_csa13"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 83
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 700.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sting:tbk1__endolysosome__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa5"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa5"
      minerva_name "STING:TBK1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/30842653"
      minerva_ref_type__resource1 "PUBMED__30842653"
      minerva_type "Complex"
      minerva_x 3035.5
      minerva_y 530.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "endolysosome"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa5"
      species_meta_id "s_id_csa5"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 85
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 900.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "dctd__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa57"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa57"
      minerva_name "DCTD"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_001012732"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P32321"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/1635"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/1635"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/2710"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000129187"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DCTD"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DCTD"
      minerva_ref_link9 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.5.4.12"
      minerva_ref_type__resource1 "REFSEQ__NM_001012732"
      minerva_ref_type__resource2 "UNIPROT__P32321"
      minerva_ref_type__resource3 "ENTREZ__1635"
      minerva_ref_type__resource4 "ENTREZ__1635"
      minerva_ref_type__resource5 "HGNC__2710"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000129187"
      minerva_ref_type__resource7 "HGNC_SYMBOL__DCTD"
      minerva_ref_type__resource8 "HGNC_SYMBOL__DCTD"
      minerva_ref_type__resource9 "EC__3.5.4.12"
      minerva_type "Protein"
      minerva_x 1360.0
      minerva_y 895.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa57"
      species_meta_id "s_id_sa57"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 86
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 1000.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "dtmp__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa58"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa58"
      minerva_fullName "dTMP"
      minerva_name "dTMP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17013"
      minerva_ref_type__resource1 "CHEBI__CHEBI:17013"
      minerva_synonyms "(dT)1; 5'-TMP; 5'-Thymidylic acid; 5-methyl-dUMP; THYMIDINE-5'-PHOSPHATE; TMP; Thymidine 5'-phosphate; Thymidine monophosphate; Thymidylate; Thymidylic acid; deoxyribosylthymine monophosphate; ribothymidine 5'-monophosphate; thymidine 5'-(dihydrogen phosphate); thymidine 5'-phosphoric acid; thymidine-5'-monophosphoric acid"
      minerva_type "Simple molecule"
      minerva_x 1255.0
      minerva_y 977.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa58"
      species_meta_id "s_id_sa58"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 87
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1100.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "qh2__mitochondrion__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa20"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa20"
      minerva_fullName "ubiquinol"
      minerva_name "QH2"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17976"
      minerva_ref_type__resource1 "CHEBI__CHEBI:17976"
      minerva_synonyms "CoQH2; QH(2); QH2; Ubiquinol; a ubiquinol; coenzymes QH2; reduced ubiquinone; ubiquinols"
      minerva_type "Simple molecule"
      minerva_x 479.5
      minerva_y 401.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrion"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa20"
      species_meta_id "s_id_sa20"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 88
    zlevel -1

    cluster [
      cluster "s_id_ca8"
    ]
    graphics [
      x 1200.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cgamp_minus_sting__copii_minus_coated_space_vesicle__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa11"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa11"
      minerva_name "cGAMP-STING"
      minerva_type "Complex"
      minerva_x 2584.0
      minerva_y 1066.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca8"
      compartment_name "COPII_minus_coated_space_vesicle"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa11"
      species_meta_id "s_id_csa11"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 91
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 400.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "dut__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa54"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa54"
      minerva_name "DUT"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_001025248"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/1854"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/1854"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/3078"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P33316"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000128951"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUT"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUT"
      minerva_ref_link9 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.6.1.23"
      minerva_ref_type__resource1 "REFSEQ__NM_001025248"
      minerva_ref_type__resource2 "ENTREZ__1854"
      minerva_ref_type__resource3 "ENTREZ__1854"
      minerva_ref_type__resource4 "HGNC__3078"
      minerva_ref_type__resource5 "UNIPROT__P33316"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000128951"
      minerva_ref_type__resource7 "HGNC_SYMBOL__DUT"
      minerva_ref_type__resource8 "HGNC_SYMBOL__DUT"
      minerva_ref_type__resource9 "EC__3.6.1.23"
      minerva_type "Protein"
      minerva_x 1175.0
      minerva_y 775.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa54"
      species_meta_id "s_id_sa54"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 92
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 500.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mre11:rad50:dna__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa10"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa10"
      minerva_name "MRE11:RAD50:DNA"
      minerva_type "Complex"
      minerva_x 2282.2857142857147
      minerva_y 1114.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa10"
      species_meta_id "s_id_csa10"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 93
    zlevel -1

    cluster [
      cluster "s_id_ca4"
    ]
    graphics [
      x 600.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sar1a__endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa92"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa92"
      minerva_former_symbols "SARA1"
      minerva_fullName "secretion associated Ras related GTPase 1A"
      minerva_name "SAR1A"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/Q9NR31"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000079332"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/10534"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SAR1A"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_001142648"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/56681"
      minerva_ref_type__resource1 "UNIPROT__Q9NR31"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000079332"
      minerva_ref_type__resource3 "HGNC__10534"
      minerva_ref_type__resource4 "HGNC_SYMBOL__SAR1A"
      minerva_ref_type__resource5 "REFSEQ__NM_001142648"
      minerva_ref_type__resource6 "ENTREZ__56681"
      minerva_synonyms "SAR1; Sara"
      minerva_type "Protein"
      minerva_x 2450.0
      minerva_y 980.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca4"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa92"
      species_meta_id "s_id_sa92"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 94
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 700.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "dihydroorotate_space_dehydrogenase_space_holoenzyme__mitochondrion__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa22"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa22"
      minerva_name "dihydroorotate dehydrogenase holoenzyme"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/1723"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DHODH"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/2867"
      minerva_ref_type__resource1 "ENTREZ__1723"
      minerva_ref_type__resource2 "HGNC_SYMBOL__DHODH"
      minerva_ref_type__resource3 "HGNC__2867"
      minerva_type "Simple molecule"
      minerva_x 276.5
      minerva_y 493.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "mitochondrion"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa22"
      species_meta_id "s_id_sa22"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 95
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 800.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tyms__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa59"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa59"
      minerva_former_symbols "TS"
      minerva_name "TYMS"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P04818"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/7298"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7298"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TYMS"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_001071"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TYMS"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12441"
      minerva_ref_link8 "https://www.ensembl.org/id/ENSG00000176890"
      minerva_ref_link9 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.1.1.45"
      minerva_ref_type__resource1 "UNIPROT__P04818"
      minerva_ref_type__resource2 "ENTREZ__7298"
      minerva_ref_type__resource3 "ENTREZ__7298"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TYMS"
      minerva_ref_type__resource5 "REFSEQ__NM_001071"
      minerva_ref_type__resource6 "HGNC_SYMBOL__TYMS"
      minerva_ref_type__resource7 "HGNC__12441"
      minerva_ref_type__resource8 "ENSEMBL__ENSG00000176890"
      minerva_ref_type__resource9 "EC__2.1.1.45"
      minerva_type "Protein"
      minerva_x 1175.0
      minerva_y 905.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa59"
      species_meta_id "s_id_sa59"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 96
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 900.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "rtc_space_and_space_transcription_space_pw__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa162"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa162"
      minerva_name "RTC and transcription pw"
      minerva_type "Phenotype"
      minerva_x 1885.0
      minerva_y 504.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa162"
      species_meta_id "s_id_sa162"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 97
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 1000.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "utp__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa40"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa40"
      minerva_fullName "UTP"
      minerva_name "UTP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15713"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15713"
      minerva_synonyms "5'-UTP; H4utp; UTP; Uridine 5'-triphosphate; Uridine triphosphate; uridine 5'-triphosphoric acid"
      minerva_type "Simple molecule"
      minerva_x 1662.0
      minerva_y 623.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa40"
      species_meta_id "s_id_sa40"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 98
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 1100.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "wipi2__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa93"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa93"
      minerva_fullName "WD repeat domain, phosphoinositide interacting 2"
      minerva_name "WIPI2"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_015610"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/32225"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=WIPI2"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000157954"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/26100"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/Q9Y4P8"
      minerva_ref_type__resource1 "REFSEQ__NM_015610"
      minerva_ref_type__resource2 "HGNC__32225"
      minerva_ref_type__resource3 "HGNC_SYMBOL__WIPI2"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000157954"
      minerva_ref_type__resource5 "ENTREZ__26100"
      minerva_ref_type__resource6 "UNIPROT__Q9Y4P8"
      minerva_synonyms "ATG18B; ATG21; CGI-50; DKFZP434J154; DKFZp686P02188; FLJ12979; FLJ14217; FLJ42984"
      minerva_type "Protein"
      minerva_x 2934.0
      minerva_y 934.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa93"
      species_meta_id "s_id_sa93"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 99
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 1200.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cgas:dsdna__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa1"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa1"
      minerva_name "cGAS:dsDNA"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/pubmed/28363908"
      minerva_ref_type__resource1 "PUBMED__28363908"
      minerva_type "Complex"
      minerva_x 1999.0
      minerva_y 933.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa1"
      species_meta_id "s_id_csa1"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 100
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 200.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "hcovs_space_autophagy_space_(wp4863)__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa177"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa177"
      minerva_name "HCoVs autophagy (WP4863)"
      minerva_type "Phenotype"
      minerva_x 3040.0
      minerva_y 1252.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa177"
      species_meta_id "s_id_sa177"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 101
    zlevel -1

    cluster [
      cluster "s_id_ca10"
    ]
    graphics [
      x 300.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cytidine__pulmonary_space_endothelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa31"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa31"
      minerva_fullName "cytidine"
      minerva_name "cytidine"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:17562"
      minerva_ref_type__resource1 "CHEBI__CHEBI:17562"
      minerva_synonyms "1-beta-D-Ribofuranosylcytosine; 1beta-D-ribofuranosylcytosine; 4-AMINO-1-BETA-D-RIBOFURANOSYL-2(1H)-PYRIMIDINONE; 4-amino-1-beta-D-ribofuranosylpyrimidin-2(1H)-one; 4-amino-1beta-D-ribofuranosyl-2(1H)-pyrimidinone; Cyd; Cytidin; Cytidine; Cytosine riboside; Zytidin; cytidine; cytosine-1beta-D-Ribofuranoside"
      minerva_type "Simple molecule"
      minerva_x 1466.0
      minerva_y 261.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca10"
      compartment_name "pulmonary_space_endothelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa31"
      species_meta_id "s_id_sa31"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 102
    zlevel -1

    graphics [
      x 400.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re43"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 3016.0357142857147
      minerva_y 651.2142857142858
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re43"
      reaction_meta_id "re43"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 105
    zlevel -1

    graphics [
      x 500.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re54"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "Heterodimer association"
      minerva_x 1895.5141952983727
      minerva_y 1077.799909584087
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re54"
      reaction_meta_id "re54"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 109
    zlevel -1

    graphics [
      x 600.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re16"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 1463.0
      minerva_y 193.625
    ]
    sbml [
      reaction_id "re16"
      reaction_meta_id "re16"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 112
    zlevel -1

    graphics [
      x 700.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re62"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 2427.0
      minerva_y 743.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re62"
      reaction_meta_id "re62"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 116
    zlevel -1

    graphics [
      x 800.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re48"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 2132.8632707856614
      minerva_y 374.3571472167969
    ]
    sbml [
      reaction_id "re48"
      reaction_meta_id "re48"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 119
    zlevel -1

    graphics [
      x 900.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re25"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 1254.5
      minerva_y 669.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re25"
      reaction_meta_id "re25"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 123
    zlevel -1

    graphics [
      x 1000.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re15"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 1255.5
      minerva_y 193.5
    ]
    sbml [
      reaction_id "re15"
      reaction_meta_id "re15"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 126
    zlevel -1

    graphics [
      x 1100.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re33"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 1546.5
      minerva_y 1062.0
    ]
    sbml [
      reaction_id "re33"
      reaction_meta_id "re33"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 129
    zlevel -1

    graphics [
      x 1200.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re51"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 2190.6607142857147
      minerva_y 611.0892857142858
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re51"
      reaction_meta_id "re51"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 133
    zlevel -1

    graphics [
      x 200.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re4"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 666.125
      minerva_y 718.75
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re4"
      reaction_meta_id "re4"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 139
    zlevel -1

    graphics [
      x 300.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re44"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 2798.7857142857147
      minerva_y 526.2142857142858
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re44"
      reaction_meta_id "re44"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 142
    zlevel -1

    graphics [
      x 400.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re5"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 665.125
      minerva_y 492.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re5"
      reaction_meta_id "re5"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 150
    zlevel -1

    graphics [
      x 500.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re39"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "Heterodimer association"
      minerva_x 1999.5
      minerva_y 822.5000000000001
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re39"
      reaction_meta_id "re39"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 155
    zlevel -1

    graphics [
      x 600.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re86"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "Trigger"
      minerva_x 1749.30674258726
      minerva_y 504.6112308892466
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re86"
      reaction_meta_id "re86"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 158
    zlevel -1

    graphics [
      x 700.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re70"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 2205.6607142857147
      minerva_y 446.0892857142857
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re70"
      reaction_meta_id "re70"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 162
    zlevel -1

    graphics [
      x 800.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re11"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 1361.0
      minerva_y 262.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re11"
      reaction_meta_id "re11"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 166
    zlevel -1

    graphics [
      x 900.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re63"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "Heterodimer association"
      minerva_x 2847.7857142857147
      minerva_y 771.7142857142857
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re63"
      reaction_meta_id "re63"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 170
    zlevel -1

    graphics [
      x 1000.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re24"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 1254.0
      minerva_y 564.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re24"
      reaction_meta_id "re24"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 174
    zlevel -1

    graphics [
      x 1100.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re76"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 1735.0
      minerva_y 983.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re76"
      reaction_meta_id "re76"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 178
    zlevel -1

    graphics [
      x 1200.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re31"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 1254.0
      minerva_y 903.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re31"
      reaction_meta_id "re31"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 182
    zlevel -1

    graphics [
      x 200.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re88"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "Trigger"
      minerva_x 3039.7265917602995
      minerva_y 1179.5
    ]
    sbml [
      reaction_id "re88"
      reaction_meta_id "re88"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 185
    zlevel -1

    graphics [
      x 300.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re87"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "Trigger"
      minerva_x 1885.0
      minerva_y 558.25
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re87"
      reaction_meta_id "re87"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 188
    zlevel -1

    graphics [
      x 400.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re89"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "Trigger"
      minerva_x 2286.5835074593333
      minerva_y 101.07183540037758
    ]
    sbml [
      reaction_id "re89"
      reaction_meta_id "re89"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 191
    zlevel -1

    graphics [
      x 500.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re42"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 2596.5
      minerva_y 823.25
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re42"
      reaction_meta_id "re42"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 194
    zlevel -1

    graphics [
      x 600.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re47"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 2000.0
      minerva_y 180.25000000000117
    ]
    sbml [
      reaction_id "re47"
      reaction_meta_id "re47"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 197
    zlevel -1

    graphics [
      x 700.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re18"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 1465.0
      minerva_y 450.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re18"
      reaction_meta_id "re18"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 201
    zlevel -1

    graphics [
      x 800.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re27"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 1463.0
      minerva_y 563.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re27"
      reaction_meta_id "re27"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 205
    zlevel -1

    graphics [
      x 900.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re29"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 1465.5
      minerva_y 771.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re29"
      reaction_meta_id "re29"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 209
    zlevel -1

    graphics [
      x 1000.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re90"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "Trigger"
      minerva_x 1883.2439832279297
      minerva_y 92.58669190798477
    ]
    sbml [
      reaction_id "re90"
      reaction_meta_id "re90"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 212
    zlevel -1

    graphics [
      x 1100.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re7"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 801.5
      minerva_y 396.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re7"
      reaction_meta_id "re7"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 218
    zlevel -1

    graphics [
      x 1200.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re19"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 1573.5
      minerva_y 622.6791706585304
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re19"
      reaction_meta_id "re19"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 222
    zlevel -1

    graphics [
      x 200.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re55"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "Heterodimer association"
      minerva_x 2145.1514585344794
      minerva_y 1113.8836013916214
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re55"
      reaction_meta_id "re55"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 226
    zlevel -1

    graphics [
      x 300.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re85"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "Trigger"
      minerva_x 1265.5410434625967
      minerva_y 1168.6436506530886
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re85"
      reaction_meta_id "re85"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 229
    zlevel -1

    graphics [
      x 400.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re40"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "Heterodimer association"
      minerva_x 2364.5
      minerva_y 823.2499999999998
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re40"
      reaction_meta_id "re40"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 233
    zlevel -1

    graphics [
      x 500.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re53"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 2687.75
      minerva_y 1066.75
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re53"
      reaction_meta_id "re53"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 236
    zlevel -1

    graphics [
      x 600.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re95"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "Trigger"
      minerva_x 2140.0
      minerva_y 91.25
    ]
    sbml [
      reaction_id "re95"
      reaction_meta_id "re95"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 239
    zlevel -1

    graphics [
      x 700.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re41"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "Heterodimer association"
      minerva_x 2836.285714285714
      minerva_y 801.2142857142853
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re41"
      reaction_meta_id "re41"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 243
    zlevel -1

    graphics [
      x 800.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re93"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "Trigger"
      minerva_x 1982.0937887351172
      minerva_y 92.97496580226365
    ]
    sbml [
      reaction_id "re93"
      reaction_meta_id "re93"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 246
    zlevel -1

    graphics [
      x 900.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re26"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 1463.5
      minerva_y 668.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re26"
      reaction_meta_id "re26"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 250
    zlevel -1

    graphics [
      x 1000.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re49"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 2031.9989712490924
      minerva_y 419.4983335307798
    ]
    sbml [
      reaction_id "re49"
      reaction_meta_id "re49"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 253
    zlevel -1

    graphics [
      x 1100.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re21"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 1662.5
      minerva_y 564.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re21"
      reaction_meta_id "re21"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 257
    zlevel -1

    graphics [
      x 1200.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re14"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 1466.0
      minerva_y 327.25
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re14"
      reaction_meta_id "re14"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 261
    zlevel -1

    graphics [
      x 200.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re12"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 1256.0
      minerva_y 329.25
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re12"
      reaction_meta_id "re12"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 265
    zlevel -1

    graphics [
      x 300.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re32"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 1350.0
      minerva_y 1019.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re32"
      reaction_meta_id "re32"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 269
    zlevel -1

    graphics [
      x 400.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re9"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 1152.5
      minerva_y 397.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re9"
      reaction_meta_id "re9"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 274
    zlevel -1

    graphics [
      x 500.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re58"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 2552.125
      minerva_y 948.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re58"
      reaction_meta_id "re58"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 278
    zlevel -1

    graphics [
      x 600.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re2"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 668.5
      minerva_y 1198.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re2"
      reaction_meta_id "re2"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 288
    zlevel -1

    graphics [
      x 700.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re57"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 2869.5
      minerva_y 431.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re57"
      reaction_meta_id "re57"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 292
    zlevel -1

    graphics [
      x 800.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re71"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 2605.5357142857147
      minerva_y 525.7142857142858
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re71"
      reaction_meta_id "re71"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 296
    zlevel -1

    graphics [
      x 900.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re38"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "Heterodimer association"
      minerva_x 1896.3749999999995
      minerva_y 933.0000000000002
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re38"
      reaction_meta_id "re38"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 300
    zlevel -1

    graphics [
      x 1000.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re20"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 1563.5
      minerva_y 505.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re20"
      reaction_meta_id "re20"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 304
    zlevel -1

    graphics [
      x 1100.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re64"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 2282.2857142857147
      minerva_y 1032.0714285714284
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re64"
      reaction_meta_id "re64"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 307
    zlevel -1

    graphics [
      x 1200.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re59"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "Heterodimer association"
      minerva_x 2935.0
      minerva_y 1041.056279160746
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re59"
      reaction_meta_id "re59"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 312
    zlevel -1

    graphics [
      x 200.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re91"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "Trigger"
      minerva_x 2186.088755830642
      minerva_y 101.25561087190528
    ]
    sbml [
      reaction_id "re91"
      reaction_meta_id "re91"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 315
    zlevel -1

    graphics [
      x 300.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re17"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 1255.0
      minerva_y 451.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re17"
      reaction_meta_id "re17"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 319
    zlevel -1

    graphics [
      x 400.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re94"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "Trigger"
      minerva_x 2027.5
      minerva_y 91.25
    ]
    sbml [
      reaction_id "re94"
      reaction_meta_id "re94"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 322
    zlevel -1

    graphics [
      x 500.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re28"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 1254.0
      minerva_y 772.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re28"
      reaction_meta_id "re28"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 326
    zlevel -1

    graphics [
      x 600.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re3"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 666.0
      minerva_y 956.75
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re3"
      reaction_meta_id "re3"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 332
    zlevel -1

    graphics [
      x 700.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re69"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 1607.75
      minerva_y 935.0
    ]
    sbml [
      reaction_id "re69"
      reaction_meta_id "re69"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 338
    zlevel -1

    graphics [
      x 800.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re77"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 1359.5
      minerva_y 829.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re77"
      reaction_meta_id "re77"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 342
    zlevel -1

    graphics [
      x 900.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re46"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000239672"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15531"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.4.6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7849"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000269"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NME1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/4830"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000239672"
      minerva_ref_type__resource2 "UNIPROT__P15531"
      minerva_ref_type__resource3 "EC__2.7.4.6"
      minerva_ref_type__resource4 "HGNC__7849"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource6 "REFSEQ__NM_000269"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NME1"
      minerva_ref_type__resource8 "ENTREZ__4830"
      minerva_ref_type__resource9 "ENTREZ__4830"
      minerva_type "State transition"
      minerva_x 2165.0
      minerva_y 179.75000000000065
    ]
    sbml [
      reaction_id "re46"
      reaction_meta_id "re46"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 345
    zlevel -1

    cluster [
      cluster "s_id_ca6"
    ]
    graphics [
      x 250.0
      y 750.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ifna1__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa88"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa88"
      minerva_fullName "interferon alpha 1"
      minerva_name "IFNA1"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IFNA1"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/3439"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_024013"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P01562"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/5417"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000197919"
      minerva_ref_type__resource1 "HGNC_SYMBOL__IFNA1"
      minerva_ref_type__resource2 "ENTREZ__3439"
      minerva_ref_type__resource3 "REFSEQ__NM_024013"
      minerva_ref_type__resource4 "UNIPROT__P01562"
      minerva_ref_type__resource5 "HGNC__5417"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000197919"
      minerva_synonyms "IFL; IFN; IFN-ALPHA; IFN-alphaD; IFNA13; IFNA@"
      minerva_type "RNA"
      minerva_x 2133.0
      minerva_y 447.2142857142858
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca6"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation "Annotation [about=\"#s_id_sa88\", history=]"
      species_id "s_id_sa88"
      species_meta_id "s_id_sa88"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
      species_notes "XMLNode 'notes' [childElements size=1]"
    ]
  ]
  node [
    id 349
    zlevel -1

    cluster [
      cluster "s_id_ca6"
    ]
    graphics [
      x 500.0
      y 850.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ifnb1__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa89"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa89"
      minerva_former_symbols "IFNB"
      minerva_fullName "interferon beta 1"
      minerva_name "IFNB1"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000171855"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IFNB1"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/5434"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P01574"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_002176"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/3456"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000171855"
      minerva_ref_type__resource2 "HGNC_SYMBOL__IFNB1"
      minerva_ref_type__resource3 "HGNC__5434"
      minerva_ref_type__resource4 "UNIPROT__P01574"
      minerva_ref_type__resource5 "REFSEQ__NM_002176"
      minerva_ref_type__resource6 "ENTREZ__3456"
      minerva_synonyms "IFB; IFF"
      minerva_type "RNA"
      minerva_x 2103.0
      minerva_y 610.9642857142858
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca6"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation "Annotation [about=\"#s_id_sa89\", history=]"
      species_id "s_id_sa89"
      species_meta_id "s_id_sa89"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
      species_notes "XMLNode 'notes' [childElements size=1]"
    ]
  ]
  edge [
    id 353
    source 74
    target 102
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa20"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 354
    source 102
    target 83
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa5"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 355
    source 67
    target 105
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa77"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 356
    source 33
    target 105
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa96"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 357
    source 105
    target 65
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa23"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 358
    source 32
    target 109
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa38"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 359
    source 109
    target 101
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa31"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 360
    source 55
    target 112
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa98"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 361
    source 112
    target 4
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa100"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 362
    source 50
    target 112
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR0"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa24"
    ]
  ]
  edge [
    id 363
    source 116
    target 53
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa90"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 364
    source 26
    target 119
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa45"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 365
    source 119
    target 20
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa47"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 366
    source 27
    target 119
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR1"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa51"
    ]
  ]
  edge [
    id 367
    source 6
    target 123
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa37"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 368
    source 123
    target 38
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa30"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 369
    source 24
    target 126
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa60"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 370
    source 126
    target 31
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa62"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 371
    source 43
    target 129
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR2"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa8"
    ]
  ]
  edge [
    id 372
    source 34
    target 133
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa3"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 373
    source 40
    target 133
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa17"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 374
    source 133
    target 11
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa4"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 375
    source 133
    target 41
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa18"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 376
    source 35
    target 133
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR3"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa19"
    ]
  ]
  edge [
    id 377
    source 54
    target 139
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa6"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 378
    source 139
    target 1
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa19"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 379
    source 11
    target 142
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa4"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 380
    source 36
    target 142
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa21"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 381
    source 142
    target 42
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa6"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 382
    source 142
    target 87
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa20"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 383
    source 94
    target 142
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR4"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa22"
    ]
  ]
  edge [
    id 384
    source 28
    target 142
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR5"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa64"
    ]
  ]
  edge [
    id 385
    source 80
    target 142
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR6"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa76"
    ]
  ]
  edge [
    id 386
    source 10
    target 150
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa9"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 387
    source 9
    target 150
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa82"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 388
    source 150
    target 78
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa83"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 389
    source 99
    target 150
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR7"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa1"
    ]
  ]
  edge [
    id 390
    source 64
    target 155
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa41"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 391
    source 155
    target 96
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa162"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 392
    source 43
    target 158
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR8"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa8"
    ]
  ]
  edge [
    id 393
    source 101
    target 162
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa31"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 394
    source 162
    target 38
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa30"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 395
    source 18
    target 162
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR9"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa32"
    ]
  ]
  edge [
    id 396
    source 39
    target 166
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa80"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 397
    source 4
    target 166
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa100"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 398
    source 166
    target 74
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa20"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 399
    source 8
    target 170
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa35"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 400
    source 170
    target 26
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa45"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 401
    source 29
    target 170
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR10"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa25"
    ]
  ]
  edge [
    id 402
    source 77
    target 174
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa179"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 403
    source 174
    target 67
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa77"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 404
    source 75
    target 174
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR11"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa163"
    ]
  ]
  edge [
    id 405
    source 72
    target 178
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa52"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 406
    source 178
    target 86
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa58"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 407
    source 95
    target 178
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR12"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa59"
    ]
  ]
  edge [
    id 408
    source 82
    target 182
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa13"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 409
    source 182
    target 100
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa177"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 410
    source 96
    target 185
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa162"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 411
    source 185
    target 61
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa140"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 412
    source 7
    target 188
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa86"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 413
    source 188
    target 57
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa178"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 414
    source 46
    target 191
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa3"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 415
    source 191
    target 3
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa4"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 416
    source 5
    target 194
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa91"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 417
    source 194
    target 69
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa87"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 418
    source 17
    target 197
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa34"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 419
    source 197
    target 73
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa36"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 420
    source 62
    target 197
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR13"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa39"
    ]
  ]
  edge [
    id 421
    source 73
    target 201
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa36"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 422
    source 201
    target 25
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa46"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 423
    source 29
    target 201
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR14"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa25"
    ]
  ]
  edge [
    id 424
    source 79
    target 205
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa48"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 425
    source 205
    target 37
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa53"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 426
    source 30
    target 205
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR15"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa56"
    ]
  ]
  edge [
    id 427
    source 69
    target 209
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa87"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 428
    source 209
    target 68
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa175"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 429
    source 42
    target 212
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa6"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 430
    source 48
    target 212
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa29"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 431
    source 212
    target 51
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa23"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 432
    source 212
    target 66
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa28"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 433
    source 16
    target 212
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR16"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa25"
    ]
  ]
  edge [
    id 434
    source 8
    target 218
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa35"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 435
    source 218
    target 97
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa40"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 436
    source 27
    target 218
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR17"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa51"
    ]
  ]
  edge [
    id 437
    source 21
    target 222
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa97"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 438
    source 65
    target 222
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa23"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 439
    source 222
    target 92
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa10"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 440
    source 80
    target 226
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa76"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 441
    source 226
    target 75
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa163"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 442
    source 78
    target 229
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa83"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 443
    source 55
    target 229
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa98"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 444
    source 229
    target 46
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa3"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 445
    source 88
    target 233
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa11"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 446
    source 233
    target 76
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa16"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 447
    source 7
    target 236
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa86"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 448
    source 236
    target 60
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa176"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 449
    source 39
    target 239
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa80"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 450
    source 3
    target 239
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa4"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 451
    source 239
    target 74
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa20"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 452
    source 7
    target 243
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa86"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 453
    source 243
    target 68
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa175"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 454
    source 25
    target 246
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa46"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 455
    source 246
    target 79
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa48"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 456
    source 27
    target 246
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR18"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa51"
    ]
  ]
  edge [
    id 457
    source 250
    target 5
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa91"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 458
    source 97
    target 253
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa40"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 459
    source 253
    target 64
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa41"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 460
    source 13
    target 253
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR19"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa43"
    ]
  ]
  edge [
    id 461
    source 101
    target 257
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa31"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 462
    source 257
    target 17
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa34"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 463
    source 58
    target 257
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR20"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa33"
    ]
  ]
  edge [
    id 464
    source 38
    target 261
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa30"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 465
    source 261
    target 70
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa27"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 466
    source 58
    target 261
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR21"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa33"
    ]
  ]
  edge [
    id 467
    source 86
    target 265
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa58"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 468
    source 265
    target 24
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa60"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 469
    source 45
    target 265
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR22"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa61"
    ]
  ]
  edge [
    id 470
    source 51
    target 269
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa23"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 471
    source 269
    target 70
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa27"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 472
    source 269
    target 22
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa26"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 473
    source 16
    target 269
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR23"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa25"
    ]
  ]
  edge [
    id 474
    source 46
    target 274
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa3"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 475
    source 274
    target 88
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa11"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 476
    source 93
    target 274
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR24"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa92"
    ]
  ]
  edge [
    id 477
    source 81
    target 278
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa1"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 478
    source 63
    target 278
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa7"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 479
    source 41
    target 278
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa18"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 480
    source 10
    target 278
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa9"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 481
    source 278
    target 52
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 482
    source 278
    target 15
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa10"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 483
    source 278
    target 71
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa13"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 484
    source 278
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa15"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 485
    source 35
    target 278
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR25"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa19"
    ]
  ]
  edge [
    id 486
    source 59
    target 288
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa84"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 487
    source 288
    target 49
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa85"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 488
    source 83
    target 288
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR26"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa5"
    ]
  ]
  edge [
    id 489
    source 1
    target 292
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa19"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 490
    source 49
    target 292
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa85"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 491
    source 292
    target 43
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa8"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 492
    source 67
    target 296
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa77"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 493
    source 44
    target 296
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa78"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 494
    source 296
    target 99
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa1"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 495
    source 73
    target 300
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa36"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 496
    source 300
    target 64
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa41"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 497
    source 27
    target 300
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR27"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa51"
    ]
  ]
  edge [
    id 498
    source 92
    target 304
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa10"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 499
    source 304
    target 50
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa24"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 500
    source 76
    target 307
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa16"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 501
    source 23
    target 307
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa94"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 502
    source 307
    target 82
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa13"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 503
    source 98
    target 307
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR28"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa93"
    ]
  ]
  edge [
    id 504
    source 69
    target 312
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa87"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 505
    source 312
    target 57
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa178"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 506
    source 70
    target 315
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa27"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 507
    source 315
    target 8
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa35"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 508
    source 62
    target 315
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR29"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa39"
    ]
  ]
  edge [
    id 509
    source 69
    target 319
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa87"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 510
    source 319
    target 60
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa176"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 511
    source 20
    target 322
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa47"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 512
    source 322
    target 72
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa52"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 513
    source 91
    target 322
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR30"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa54"
    ]
  ]
  edge [
    id 514
    source 52
    target 326
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 515
    source 14
    target 326
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa16"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 516
    source 326
    target 34
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa3"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 517
    source 326
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa15"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 518
    source 35
    target 326
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR31"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa19"
    ]
  ]
  edge [
    id 519
    source 67
    target 332
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa77"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 520
    source 31
    target 332
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa62"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 521
    source 332
    target 12
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa142"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 522
    source 19
    target 332
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR32"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa141"
    ]
  ]
  edge [
    id 523
    source 47
    target 332
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR33"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa143"
    ]
  ]
  edge [
    id 524
    source 37
    target 338
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa53"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 525
    source 338
    target 72
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa52"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 526
    source 85
    target 338
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR34"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa57"
    ]
  ]
  edge [
    id 527
    source 53
    target 342
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa90"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 528
    source 342
    target 7
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa86"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 529
    source 158
    target 345
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa88"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 530
    source 345
    target 116
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa88"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 532
    source 129
    target 349
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa89"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 533
    source 349
    target 250
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa89"
      stoichiometry "1.0"
    ]
  ]
]
