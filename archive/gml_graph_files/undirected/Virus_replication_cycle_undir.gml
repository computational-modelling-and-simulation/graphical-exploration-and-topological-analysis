# generated with VANTED V2.8.0 at Tue Apr 27 20:00:44 CEST 2021
graph [
  background_coloring "true"
  cluster_colors "255,128,128,255:0,0,0,255;255,191,128,255:0,0,0,255;255,255,128,255:0,0,0,255;191,255,128,255:0,0,0,255;128,255,128,255:0,0,0,255;128,255,191,255:0,0,0,255;128,255,255,255:0,0,0,255;128,191,255,255:0,0,0,255;128,128,255,255:0,0,0,255;191,128,255,255:0,0,0,255;255,128,255,255:0,0,0,255;255,128,191,255:0,0,0,255"
  clusterbackground_fill_outer_region "false"
  clusterbackground_grid 50.0
  clusterbackground_low_alpha 0.2
  clusterbackground_radius 200.0
  clusterbackground_space_fill "true"
  graphbackgroundcolor "#ffffff"
  sbml [
    model_meta_id "PMID_27820809"
    model_non_rdf_annotation ""
    model_notes ""
    model_sbml_id "PMID_27820809"
    namespace "xmlns:celldesigner=\"http://www.sbml.org/2001/ns/celldesigner\" "
    sbml_level "2"
    sbml_version "4"
  ]
  sbml_compartment_default [
    sbml_compartment_default_id "default"
    sbml_compartment_default_meta_id "default"
    sbml_compartment_default_non_rdf_annotation ""
    sbml_compartment_default_notes ""
    sbml_compartment_default_size "1.0"
    sbml_compartment_default_units "volume"
  ]
  sbml_compartment_s_id_ca189 [
    sbml_compartment_s_id_ca189_annotation ""
    sbml_compartment_s_id_ca189_id "s_id_ca189"
    sbml_compartment_s_id_ca189_meta_id "s_id_ca189"
    sbml_compartment_s_id_ca189_name "epithelial_space_cell"
    sbml_compartment_s_id_ca189_non_rdf_annotation ""
    sbml_compartment_s_id_ca189_notes ""
    sbml_compartment_s_id_ca189_outside "default"
    sbml_compartment_s_id_ca189_size "1.0"
    sbml_compartment_s_id_ca189_units "volume"
  ]
  sbml_compartment_s_id_ca206 [
    sbml_compartment_s_id_ca206_annotation ""
    sbml_compartment_s_id_ca206_id "s_id_ca206"
    sbml_compartment_s_id_ca206_meta_id "s_id_ca206"
    sbml_compartment_s_id_ca206_name "endosome"
    sbml_compartment_s_id_ca206_non_rdf_annotation ""
    sbml_compartment_s_id_ca206_notes ""
    sbml_compartment_s_id_ca206_outside "s_id_ca189"
    sbml_compartment_s_id_ca206_size "1.0"
    sbml_compartment_s_id_ca206_units "volume"
  ]
  sbml_compartment_s_id_ca218 [
    sbml_compartment_s_id_ca218_annotation ""
    sbml_compartment_s_id_ca218_id "s_id_ca218"
    sbml_compartment_s_id_ca218_meta_id "s_id_ca218"
    sbml_compartment_s_id_ca218_name "SARS_minus_CoV_minus_2_space_virion"
    sbml_compartment_s_id_ca218_non_rdf_annotation ""
    sbml_compartment_s_id_ca218_notes ""
    sbml_compartment_s_id_ca218_outside "default"
    sbml_compartment_s_id_ca218_size "1.0"
    sbml_compartment_s_id_ca218_units "volume"
  ]
  sbml_compartment_s_id_ca221 [
    sbml_compartment_s_id_ca221_annotation ""
    sbml_compartment_s_id_ca221_id "s_id_ca221"
    sbml_compartment_s_id_ca221_meta_id "s_id_ca221"
    sbml_compartment_s_id_ca221_name "endoplasmic_space_reticulum_minus_Golgi_space_intermediate_space_compartment"
    sbml_compartment_s_id_ca221_non_rdf_annotation ""
    sbml_compartment_s_id_ca221_notes ""
    sbml_compartment_s_id_ca221_outside "s_id_ca189"
    sbml_compartment_s_id_ca221_size "1.0"
    sbml_compartment_s_id_ca221_units "volume"
  ]
  sbml_compartment_s_id_ca222 [
    sbml_compartment_s_id_ca222_annotation ""
    sbml_compartment_s_id_ca222_id "s_id_ca222"
    sbml_compartment_s_id_ca222_meta_id "s_id_ca222"
    sbml_compartment_s_id_ca222_name "rough_space_endoplasmic_space_reticulum"
    sbml_compartment_s_id_ca222_non_rdf_annotation ""
    sbml_compartment_s_id_ca222_notes ""
    sbml_compartment_s_id_ca222_outside "s_id_ca189"
    sbml_compartment_s_id_ca222_size "1.0"
    sbml_compartment_s_id_ca222_units "volume"
  ]
  sbml_compartment_s_id_ca230 [
    sbml_compartment_s_id_ca230_annotation ""
    sbml_compartment_s_id_ca230_id "s_id_ca230"
    sbml_compartment_s_id_ca230_meta_id "s_id_ca230"
    sbml_compartment_s_id_ca230_name "SARS_minus_CoV_minus_2_space_virion"
    sbml_compartment_s_id_ca230_non_rdf_annotation ""
    sbml_compartment_s_id_ca230_notes ""
    sbml_compartment_s_id_ca230_outside "s_id_ca233"
    sbml_compartment_s_id_ca230_size "1.0"
    sbml_compartment_s_id_ca230_units "volume"
  ]
  sbml_compartment_s_id_ca232 [
    sbml_compartment_s_id_ca232_annotation ""
    sbml_compartment_s_id_ca232_id "s_id_ca232"
    sbml_compartment_s_id_ca232_meta_id "s_id_ca232"
    sbml_compartment_s_id_ca232_name "SARS_minus_CoV_minus_2_space_virion"
    sbml_compartment_s_id_ca232_non_rdf_annotation ""
    sbml_compartment_s_id_ca232_notes ""
    sbml_compartment_s_id_ca232_outside "s_id_ca221"
    sbml_compartment_s_id_ca232_size "1.0"
    sbml_compartment_s_id_ca232_units "volume"
  ]
  sbml_compartment_s_id_ca233 [
    sbml_compartment_s_id_ca233_annotation ""
    sbml_compartment_s_id_ca233_id "s_id_ca233"
    sbml_compartment_s_id_ca233_meta_id "s_id_ca233"
    sbml_compartment_s_id_ca233_name "smooth_minus_wall_space_vesicle"
    sbml_compartment_s_id_ca233_non_rdf_annotation ""
    sbml_compartment_s_id_ca233_notes ""
    sbml_compartment_s_id_ca233_outside "s_id_ca189"
    sbml_compartment_s_id_ca233_size "1.0"
    sbml_compartment_s_id_ca233_units "volume"
  ]
  sbml_compartment_s_id_ca237 [
    sbml_compartment_s_id_ca237_annotation ""
    sbml_compartment_s_id_ca237_id "s_id_ca237"
    sbml_compartment_s_id_ca237_meta_id "s_id_ca237"
    sbml_compartment_s_id_ca237_name "double_space_membrane_space_vesicle_space_viral_space_factory"
    sbml_compartment_s_id_ca237_non_rdf_annotation ""
    sbml_compartment_s_id_ca237_notes ""
    sbml_compartment_s_id_ca237_outside "s_id_ca189"
    sbml_compartment_s_id_ca237_size "1.0"
    sbml_compartment_s_id_ca237_units "volume"
  ]
  sbml_compartment_s_id_ca238 [
    sbml_compartment_s_id_ca238_annotation ""
    sbml_compartment_s_id_ca238_id "s_id_ca238"
    sbml_compartment_s_id_ca238_meta_id "s_id_ca238"
    sbml_compartment_s_id_ca238_name "SARS_minus_CoV_minus_2_space_virion"
    sbml_compartment_s_id_ca238_non_rdf_annotation ""
    sbml_compartment_s_id_ca238_notes ""
    sbml_compartment_s_id_ca238_outside "s_id_ca206"
    sbml_compartment_s_id_ca238_size "1.0"
    sbml_compartment_s_id_ca238_units "volume"
  ]
  sbml_compartment_s_id_ca242 [
    sbml_compartment_s_id_ca242_annotation ""
    sbml_compartment_s_id_ca242_id "s_id_ca242"
    sbml_compartment_s_id_ca242_meta_id "s_id_ca242"
    sbml_compartment_s_id_ca242_name "endoplasmic_space_reticulum"
    sbml_compartment_s_id_ca242_non_rdf_annotation ""
    sbml_compartment_s_id_ca242_notes ""
    sbml_compartment_s_id_ca242_outside "s_id_ca189"
    sbml_compartment_s_id_ca242_size "1.0"
    sbml_compartment_s_id_ca242_units "volume"
  ]
  sbml_unit_definition_1 [
    sbml_unit_definition_1_id "length"
    sbml_unit_definition_1_name "length"
    sbml_unit_definition_1_sub_unit_1_ "(1.0 * 10^0 * metre)^1.0"
    sbml_unit_definition_1unit "(1.0 * 10^0 * metre)^1.0"
  ]
  sbml_unit_definition_2 [
    sbml_unit_definition_2_id "area"
    sbml_unit_definition_2_name "area"
    sbml_unit_definition_2_sub_unit_1_ "(1.0 * 10^0 * metre)^2.0"
    sbml_unit_definition_2unit "(1.0 * 10^0 * metre)^2.0"
  ]
  sbml_unit_definition_3 [
    sbml_unit_definition_3_id "time"
    sbml_unit_definition_3_name "time"
    sbml_unit_definition_3_sub_unit_1_ "(1.0 * 10^0 * second)^1.0"
    sbml_unit_definition_3unit "(1.0 * 10^0 * second)^1.0"
  ]
  sbml_unit_definition_4 [
    sbml_unit_definition_4_id "substance"
    sbml_unit_definition_4_name "substance"
    sbml_unit_definition_4_sub_unit_1_ "(1.0 * 10^0 * mole)^1.0"
    sbml_unit_definition_4unit "(1.0 * 10^0 * mole)^1.0"
  ]
  sbml_unit_definition_5 [
    sbml_unit_definition_5_id "volume"
    sbml_unit_definition_5_name "volume"
    sbml_unit_definition_5_sub_unit_1_ "(1.0 * 10^0 * litre)^1.0"
    sbml_unit_definition_5unit "(1.0 * 10^0 * litre)^1.0"
  ]
  directed 0
  node [
    id 1
    zlevel -1

    cluster [
      cluster "s_id_ca242"
    ]
    graphics [
      x 200.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp6__endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2204"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2367"
      minerva_elementId2 "sa2204"
      minerva_name "Nsp6"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/YP_009725302"
      minerva_ref_type__resource1 "NCBI_PROTEIN__YP_009725302"
      minerva_type "Protein"
      minerva_x 3177.1764705882356
      minerva_x2 2829.8479532163756
      minerva_y 1597.2318452380964
      minerva_y2 1602.924759816207
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca242"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2204"
      species_meta_id "s_id_sa2204"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 2
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 300.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "host_space_translation__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2358"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2358"
      minerva_fullName "translation"
      minerva_name "Host translation"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0006412"
      minerva_ref_type__resource1 "GO__GO:0006412"
      minerva_type "Phenotype"
      minerva_x 3330.5
      minerva_y 566.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2358"
      species_meta_id "s_id_sa2358"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 3
    zlevel -1

    cluster [
      cluster "s_id_ca218"
    ]
    graphics [
      x 400.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nucleocapsid__sars_minus_cov_minus_2_space_virion__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa366"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa357"
      minerva_elementId10 "csa371"
      minerva_elementId11 "csa366"
      minerva_elementId12 "csa374"
      minerva_elementId2 "csa365"
      minerva_elementId3 "csa432"
      minerva_elementId4 "csa389"
      minerva_elementId5 "csa391"
      minerva_elementId6 "csa370"
      minerva_elementId7 "csa353"
      minerva_elementId8 "csa352"
      minerva_elementId9 "csa387"
      minerva_fullName "viral nucleocapsid"
      minerva_name "Nucleocapsid"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0019013"
      minerva_ref_type__resource1 "GO__GO:0019013"
      minerva_type "Complex"
      minerva_x 554.8461538461538
      minerva_x10 3555.423076923077
      minerva_x11 3980.0
      minerva_x12 2541.880808080811
      minerva_x2 1420.6666666666665
      minerva_x3 1115.5195780741446
      minerva_x4 5143.625
      minerva_x5 5197.2307692307695
      minerva_x6 2907.666666666668
      minerva_x7 634.5
      minerva_x8 211.0
      minerva_x9 5134.5
      minerva_y 916.1634615384619
      minerva_y10 278.4807692307693
      minerva_y11 286.0
      minerva_y12 330.15429292929343
      minerva_y2 894.4166666666667
      minerva_y3 910.3165237755024
      minerva_y4 999.9567307692307
      minerva_y5 2296.766826923077
      minerva_y6 311.41666666666697
      minerva_y7 252.75000000000045
      minerva_y8 252.0
      minerva_y9 228.16346153846143
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca218"
      compartment_name "SARS_minus_CoV_minus_2_space_virion"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa366"
      species_meta_id "s_id_csa366"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 4
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 500.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "n_space_(_minus_)ss_space_sgmrna__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2355"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2355"
      minerva_name "N (-)ss sgmRNA"
      minerva_type "RNA"
      minerva_x 3209.0
      minerva_y 2310.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2355"
      species_meta_id "s_id_sa2355"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 5
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 600.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "m_space_ds_space_sgmrna__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2343"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2343"
      minerva_name "M ds sgmRNA"
      minerva_type "RNA"
      minerva_x 2548.0
      minerva_y 2340.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2343"
      species_meta_id "s_id_sa2343"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 6
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 700.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp16__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2199"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2199"
      minerva_name "Nsp16"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/YP_009725311"
      minerva_ref_type__resource1 "NCBI_PROTEIN__YP_009725311"
      minerva_type "Protein"
      minerva_x 2043.7837301587306
      minerva_y 1608.9247598162071
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2199"
      species_meta_id "s_id_sa2199"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 7
    zlevel -1

    cluster [
      cluster "s_id_ca218"
    ]
    graphics [
      x 800.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "e__sars_minus_cov_minus_2_space_virion__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2003"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2050"
      minerva_elementId10 "sa2003"
      minerva_elementId11 "sa2013"
      minerva_elementId12 "sa2008"
      minerva_elementId13 "sa2023"
      minerva_elementId2 "sa2062"
      minerva_elementId3 "sa2184"
      minerva_elementId4 "sa1998"
      minerva_elementId5 "sa1858"
      minerva_elementId6 "sa2065"
      minerva_elementId7 "sa2019"
      minerva_elementId8 "sa2046"
      minerva_elementId9 "sa2039"
      minerva_name "E"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/43740570"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=E"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P0DTC4"
      minerva_ref_type__resource1 "ENTREZ__43740570"
      minerva_ref_type__resource2 "HGNC_SYMBOL__E"
      minerva_ref_type__resource3 "UNIPROT__P0DTC4"
      minerva_type "Protein"
      minerva_x 417.66025641025635
      minerva_x10 4979.698717948718
      minerva_x11 3405.0320512820517
      minerva_x12 3840.3653846153848
      minerva_x13 2378.3653846153848
      minerva_x2 1511.429487179487
      minerva_x3 976.2823985869652
      minerva_x4 4981.448717948718
      minerva_x5 5055.2307692307695
      minerva_x6 2384.583333333333
      minerva_x7 2761.0320512820517
      minerva_x8 480.66025641025635
      minerva_x9 60.48443223443246
      minerva_y 975.5858426113358
      minerva_y10 261.15315030364354
      minerva_y11 323.15315030364354
      minerva_y12 270.15315030364354
      minerva_y13 353.27815030364354
      minerva_y2 788.3550733805666
      minerva_y3 955.2549304894023
      minerva_y4 1043.9031503036435
      minerva_y5 2307.766826923077
      minerva_y6 470.96696149245474
      minerva_y7 360.27815030364354
      minerva_y8 307.5858426113358
      minerva_y9 277.8674360179293
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca218"
      compartment_name "SARS_minus_CoV_minus_2_space_virion"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2003"
      species_meta_id "s_id_sa2003"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 8
    zlevel -1

    cluster [
      cluster "s_id_ca232"
    ]
    graphics [
      x 900.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "m__sars_minus_cov_minus_2_space_virion__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa1857"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2049"
      minerva_elementId10 "sa2171"
      minerva_elementId11 "sa2007"
      minerva_elementId12 "sa2024"
      minerva_elementId2 "sa2061"
      minerva_elementId3 "sa2185"
      minerva_elementId4 "sa1997"
      minerva_elementId5 "sa1857"
      minerva_elementId6 "sa2020"
      minerva_elementId7 "sa2045"
      minerva_elementId8 "sa2038"
      minerva_elementId9 "sa2002"
      minerva_name "M"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/43740571"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P0DTC5"
      minerva_ref_type__resource1 "ENTREZ__43740571"
      minerva_ref_type__resource2 "UNIPROT__P0DTC5"
      minerva_type "Protein"
      minerva_x 409.66025641025635
      minerva_x10 3403.842105263158
      minerva_x11 3856.3653846153848
      minerva_x12 2401.3653846153848
      minerva_x2 1537.429487179487
      minerva_x3 973.2823985869652
      minerva_x4 4975.448717948718
      minerva_x5 5050.2307692307695
      minerva_x6 2755.0320512820517
      minerva_x7 474.66025641025635
      minerva_x8 67.48443223443246
      minerva_x9 4988.698717948718
      minerva_y 920.5858426113358
      minerva_y10 267.7368421052631
      minerva_y11 221.15315030364354
      minerva_y12 282.27815030364354
      minerva_y2 981.3550733805666
      minerva_y3 889.2549304894023
      minerva_y4 984.9031503036435
      minerva_y5 2243.766826923077
      minerva_y6 301.27815030364354
      minerva_y7 251.5858426113358
      minerva_y8 220.86743601792932
      minerva_y9 199.15315030364354
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca232"
      compartment_name "SARS_minus_CoV_minus_2_space_virion"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa1857"
      species_meta_id "s_id_sa1857"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 9
    zlevel -1

    cluster [
      cluster "s_id_ca222"
    ]
    graphics [
      x 1000.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf7a__rough_space_endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa1875"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa1875"
      minerva_name "Orf7a"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/43740573"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P0DTC7"
      minerva_ref_type__resource1 "ENTREZ__43740573"
      minerva_ref_type__resource2 "UNIPROT__P0DTC7"
      minerva_type "Protein"
      minerva_x 4422.583333333333
      minerva_y 3030.6749999999993
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca222"
      compartment_name "rough_space_endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa1875"
      species_meta_id "s_id_sa1875"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 10
    zlevel -1

    cluster [
      cluster "s_id_ca218"
    ]
    graphics [
      x 1100.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s2__sars_minus_cov_minus_2_space_virion__6-hb conformation__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa1601"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2052"
      minerva_elementId2 "sa1601"
      minerva_name "S2"
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002552"
      minerva_ref_type__resource1 "INTERPRO__IPR002552"
      minerva_structuralState "6-HB conformation"
      minerva_type "Protein"
      minerva_x 1415.2980769230771
      minerva_x2 2542.833333333332
      minerva_y 1031.3846153846152
      minerva_y2 472.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca218"
      compartment_name "SARS_minus_CoV_minus_2_space_virion"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa1601"
      species_meta_id "s_id_sa1601"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 11
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 1200.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf9b_space_(_minus_)ss_space_sgmrna__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2320"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2320"
      minerva_name "Orf9b (-)ss sgmRNA"
      minerva_type "RNA"
      minerva_x 1753.0555555555557
      minerva_y 2508.8888888888887
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2320"
      species_meta_id "s_id_sa2320"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 12
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 200.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sa2298_underscore_degraded__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2335"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2335"
      minerva_name "sa2298_degraded"
      minerva_type "Degraded"
      minerva_x 3495.0
      minerva_y 2548.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2335"
      species_meta_id "s_id_sa2335"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 13
    zlevel -1

    cluster [
      cluster "s_id_ca218"
    ]
    graphics [
      x 300.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nrp1__sars_minus_cov_minus_2_space_virion__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2371"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2371"
      minerva_name "NRP1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/8829"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8829"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/8004"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/O14786"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/O14786"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000099250"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_001024628"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NRP1"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NRP1"
      minerva_ref_type__resource1 "ENTREZ__8829"
      minerva_ref_type__resource2 "ENTREZ__8829"
      minerva_ref_type__resource3 "HGNC__8004"
      minerva_ref_type__resource4 "UNIPROT__O14786"
      minerva_ref_type__resource5 "UNIPROT__O14786"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000099250"
      minerva_ref_type__resource7 "REFSEQ__NM_001024628"
      minerva_ref_type__resource8 "HGNC_SYMBOL__NRP1"
      minerva_ref_type__resource9 "HGNC_SYMBOL__NRP1"
      minerva_type "Protein"
      minerva_x 2705.0
      minerva_y 466.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca218"
      compartment_name "SARS_minus_CoV_minus_2_space_virion"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2371"
      species_meta_id "s_id_sa2371"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 14
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 400.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf7a_space_(_minus_)ss_space_sgmrna__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2324"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2324"
      minerva_name "Orf7a (-)ss sgmRNA"
      minerva_type "RNA"
      minerva_x 1753.0555555555557
      minerva_y 2353.8888888888887
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2324"
      species_meta_id "s_id_sa2324"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 15
    zlevel -1

    cluster [
      cluster "s_id_ca222"
    ]
    graphics [
      x 500.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf3a__rough_space_endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa1873"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa1873"
      minerva_name "Orf3a"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P0DTC3"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/43740569"
      minerva_ref_type__resource1 "UNIPROT__P0DTC3"
      minerva_ref_type__resource2 "ENTREZ__43740569"
      minerva_type "Protein"
      minerva_x 4423.916666666667
      minerva_y 3128.6749999999993
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca222"
      compartment_name "rough_space_endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa1873"
      species_meta_id "s_id_sa1873"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 16
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 600.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp9__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2361"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2201"
      minerva_elementId2 "sa2361"
      minerva_name "Nsp9"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/YP_009725305"
      minerva_ref_type__resource1 "NCBI_PROTEIN__YP_009725305"
      minerva_type "Protein"
      minerva_x 2563.8479532163756
      minerva_x2 3512.423976608188
      minerva_y 1606.924759816207
      minerva_y2 1597.2318452380962
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2361"
      species_meta_id "s_id_sa2361"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 17
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 700.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s2919__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa1920"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa1920"
      minerva_name "s2919"
      minerva_type "Degraded"
      minerva_x 1548.0
      minerva_y 1274.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa1920"
      species_meta_id "s_id_sa1920"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 18
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 800.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp12__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2197"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2197"
      minerva_name "Nsp12"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/YP_009725307"
      minerva_ref_type__resource1 "NCBI_PROTEIN__YP_009725307"
      minerva_type "Protein"
      minerva_x 2388.111111111112
      minerva_y 1604.924759816207
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2197"
      species_meta_id "s_id_sa2197"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 19
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 900.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf3a_space_ds_space_sgmrna__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2348"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2348"
      minerva_name "Orf3a ds sgmRNA"
      minerva_type "RNA"
      minerva_x 2548.0
      minerva_y 2545.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2348"
      species_meta_id "s_id_sa2348"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 20
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 1000.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp2__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2217"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2217"
      minerva_elementId2 "sa2357"
      minerva_name "Nsp2"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/YP_009725298"
      minerva_ref_type__resource1 "NCBI_PROTEIN__YP_009725298"
      minerva_type "Protein"
      minerva_x 2483.65
      minerva_x2 3381.5
      minerva_y 659.5941176470587
      minerva_y2 659.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2217"
      species_meta_id "s_id_sa2217"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 21
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 1100.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf9b_space_(_minus_)ss_space_sgmrna__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2300"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2300"
      minerva_name "Orf9b (-)ss sgmRNA"
      minerva_type "RNA"
      minerva_x 3215.0
      minerva_y 2632.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2300"
      species_meta_id "s_id_sa2300"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 22
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 1200.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "n_space_(_plus_)ss_space_sgmrna__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa1962"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa1962"
      minerva_name "N (+)ss sgmRNA"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/43740575"
      minerva_ref_type__resource1 "ENTREZ__43740575"
      minerva_type "RNA"
      minerva_x 3401.9493251846206
      minerva_y 3363.0588235294117
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa1962"
      species_meta_id "s_id_sa1962"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 23
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 200.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sa2302_underscore_degraded__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2340"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2340"
      minerva_name "sa2302_degraded"
      minerva_type "Degraded"
      minerva_x 3495.0
      minerva_y 2706.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2340"
      species_meta_id "s_id_sa2340"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 24
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 300.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "heparan_space_sulfate__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2369"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2370"
      minerva_elementId2 "sa2369"
      minerva_fullName "heparan sulfate"
      minerva_name "Heparan sulfate"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:28815"
      minerva_ref_type__resource1 "CHEBI__CHEBI:28815"
      minerva_synonyms "Heparan N-sulfate; Heparan sulfate; Heparan sulphate; Heparatan sulfate; Heparin monosulfate; Heparin sulfate; Heparitin monosulfate; Heparitin sulfate; N-Acetylheparan sulfate"
      minerva_type "Simple molecule"
      minerva_x 3735.0
      minerva_x2 413.5
      minerva_y 467.0
      minerva_y2 465.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2369"
      species_meta_id "s_id_sa2369"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 25
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 400.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf8_space_(_minus_)ss_space_sgmrna__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2321"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2321"
      minerva_name "Orf8 (-)ss sgmRNA"
      minerva_type "RNA"
      minerva_x 1753.0555555555557
      minerva_y 2471.8888888888887
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2321"
      species_meta_id "s_id_sa2321"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 26
    zlevel -1

    cluster [
      cluster "s_id_ca221"
    ]
    graphics [
      x 500.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf14__endoplasmic_space_reticulum_minus_golgi_space_intermediate_space_compartment__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2250"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2250"
      minerva_name "Orf14"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P0DTD3"
      minerva_ref_type__resource1 "UNIPROT__P0DTD3"
      minerva_type "Protein"
      minerva_x 5272.229864433812
      minerva_y 2645.3181818181815
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca221"
      compartment_name "endoplasmic_space_reticulum_minus_Golgi_space_intermediate_space_compartment"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2250"
      species_meta_id "s_id_sa2250"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 27
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 600.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf7b_space_(_minus_)ss_space_sgmrna__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2318"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2318"
      minerva_name "Orf7b (-)ss sgmRNA"
      minerva_type "RNA"
      minerva_x 1753.0555555555557
      minerva_y 2587.8888888888887
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2318"
      species_meta_id "s_id_sa2318"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 28
    zlevel -1

    cluster [
      cluster "s_id_ca238"
    ]
    graphics [
      x 700.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf7a__sars_minus_cov_minus_2_space_virion__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2060"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2048"
      minerva_elementId10 "sa2011"
      minerva_elementId11 "sa2006"
      minerva_elementId12 "sa2025"
      minerva_elementId2 "sa2060"
      minerva_elementId3 "sa2183"
      minerva_elementId4 "sa1996"
      minerva_elementId5 "sa1986"
      minerva_elementId6 "sa2021"
      minerva_elementId7 "sa2044"
      minerva_elementId8 "sa2037"
      minerva_elementId9 "sa2001"
      minerva_name "Orf7a"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/43740573"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P0DTC7"
      minerva_ref_type__resource1 "ENTREZ__43740573"
      minerva_ref_type__resource2 "UNIPROT__P0DTC7"
      minerva_type "Protein"
      minerva_x 410.53475033738187
      minerva_x10 3419.9065452091772
      minerva_x11 3901.2398785425103
      minerva_x12 2472.2398785425103
      minerva_x2 1367.3039811066126
      minerva_x3 1012.1568925140907
      minerva_x4 4996.323211875843
      minerva_x5 5228.105263157895
      minerva_x6 2775.9065452091772
      minerva_x7 495.53475033738187
      minerva_x8 106.35892616155797
      minerva_x9 5016.573211875843
      minerva_y 864.1348051619428
      minerva_y10 208.70211285425057
      minerva_y11 171.70211285425057
      minerva_y12 207.82711285425057
      minerva_y2 783.9040359311737
      minerva_y3 817.8038930400094
      minerva_y4 929.4521128542506
      minerva_y5 2413.315789473684
      minerva_y6 245.82711285425057
      minerva_y7 196.13480516194284
      minerva_y8 165.41639856853635
      minerva_y9 140.70211285425057
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca238"
      compartment_name "SARS_minus_CoV_minus_2_space_virion"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2060"
      species_meta_id "s_id_sa2060"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 29
    zlevel -1

    cluster [
      cluster "s_id_ca218"
    ]
    graphics [
      x 800.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s__sars_minus_cov_minus_2_space_virion__n:3__2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2173"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa1999"
      minerva_elementId2 "sa1859"
      minerva_elementId3 "sa2178"
      minerva_elementId4 "sa2040"
      minerva_elementId5 "sa2004"
      minerva_elementId6 "sa2009"
      minerva_elementId7 "sa2173"
      minerva_homodimer "2"
      minerva_name "S"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P0DTC2"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/43740568"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=S"
      minerva_ref_type__resource1 "UNIPROT__P0DTC2"
      minerva_ref_type__resource2 "ENTREZ__43740568"
      minerva_ref_type__resource3 "HGNC_SYMBOL__S"
      minerva_structuralState "N:3"
      minerva_type "Protein"
      minerva_x 5020.641393694022
      minerva_x2 5079.423444976073
      minerva_x3 260.1771079797363
      minerva_x4 127.67710797973632
      minerva_x5 5018.891393694022
      minerva_x6 3889.5580603606886
      minerva_x7 3842.0580603606886
      minerva_y 1120.6531503036435
      minerva_y2 2379.516826923077
      minerva_y3 373.75
      minerva_y4 367.6174360179293
      minerva_y5 326.90315030364354
      minerva_y6 392.90315030364354
      minerva_y7 328.75
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca218"
      compartment_name "SARS_minus_CoV_minus_2_space_virion"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2173"
      species_meta_id "s_id_sa2173"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 30
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 900.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp1__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2216"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2216"
      minerva_name "Nsp1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/YP_009725297"
      minerva_ref_type__resource1 "NCBI_PROTEIN__YP_009725297"
      minerva_type "Protein"
      minerva_x 2608.65
      minerva_y 643.5941176470587
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2216"
      species_meta_id "s_id_sa2216"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 31
    zlevel -1

    cluster [
      cluster "s_id_ca242"
    ]
    graphics [
      x 1000.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp8__endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2202"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2202"
      minerva_name "Nsp8"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/YP_009725304"
      minerva_ref_type__resource1 "NCBI_PROTEIN__YP_009725304"
      minerva_type "Protein"
      minerva_x 2651.8479532163756
      minerva_y 1606.924759816207
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca242"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2202"
      species_meta_id "s_id_sa2202"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 32
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 1100.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tmprss2__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa1537"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa1537"
      minerva_name "TMPRSS2"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11876"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_001135099"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TMPRSS2"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/O15393"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/O15393"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TMPRSS2"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/7113"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/7113"
      minerva_ref_link8 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.21.-"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000184012"
      minerva_ref_type__resource1 "HGNC__11876"
      minerva_ref_type__resource10 "REFSEQ__NM_001135099"
      minerva_ref_type__resource2 "HGNC_SYMBOL__TMPRSS2"
      minerva_ref_type__resource3 "UNIPROT__O15393"
      minerva_ref_type__resource4 "UNIPROT__O15393"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TMPRSS2"
      minerva_ref_type__resource6 "ENTREZ__7113"
      minerva_ref_type__resource7 "ENTREZ__7113"
      minerva_ref_type__resource8 "EC__3.4.21.-"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000184012"
      minerva_type "Protein"
      minerva_x 3379.166666666668
      minerva_y 469.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa1537"
      species_meta_id "s_id_sa1537"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 33
    zlevel -1

    cluster [
      cluster "s_id_ca221"
    ]
    graphics [
      x 1200.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf6__endoplasmic_space_reticulum_minus_golgi_space_intermediate_space_compartment__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2246"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2246"
      minerva_name "Orf6"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/43740572"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P0DTC6"
      minerva_ref_type__resource1 "ENTREZ__43740572"
      minerva_ref_type__resource2 "UNIPROT__P0DTC6"
      minerva_type "Protein"
      minerva_x 4912.6048644338125
      minerva_y 2645.3181818181815
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca221"
      compartment_name "endoplasmic_space_reticulum_minus_Golgi_space_intermediate_space_compartment"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2246"
      species_meta_id "s_id_sa2246"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 34
    zlevel -1

    cluster [
      cluster "s_id_ca206"
    ]
    graphics [
      x 200.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ctsb__endosome__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa1524"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa1524"
      minerva_name "CTSB"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000164733"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CTSB"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/1508"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/1508"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_147780"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P07858"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P07858"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/2527"
      minerva_ref_link8 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.22.1"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CTSB"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000164733"
      minerva_ref_type__resource10 "HGNC_SYMBOL__CTSB"
      minerva_ref_type__resource2 "ENTREZ__1508"
      minerva_ref_type__resource3 "ENTREZ__1508"
      minerva_ref_type__resource4 "REFSEQ__NM_147780"
      minerva_ref_type__resource5 "UNIPROT__P07858"
      minerva_ref_type__resource6 "UNIPROT__P07858"
      minerva_ref_type__resource7 "HGNC__2527"
      minerva_ref_type__resource8 "EC__3.4.22.1"
      minerva_ref_type__resource9 "HGNC_SYMBOL__CTSB"
      minerva_type "Protein"
      minerva_x 746.75
      minerva_y 1057.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca206"
      compartment_name "endosome"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa1524"
      species_meta_id "s_id_sa1524"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 35
    zlevel -1

    cluster [
      cluster "s_id_ca222"
    ]
    graphics [
      x 300.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf14__rough_space_endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa1877"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa1877"
      minerva_name "Orf14"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P0DTD3"
      minerva_ref_type__resource1 "UNIPROT__P0DTD3"
      minerva_type "Protein"
      minerva_x 4424.874999999999
      minerva_y 3272.925
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca222"
      compartment_name "rough_space_endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa1877"
      species_meta_id "s_id_sa1877"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 36
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 400.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf8_space_(_minus_)ss_space_sgmrna__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2299"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2299"
      minerva_name "Orf8 (-)ss sgmRNA"
      minerva_type "RNA"
      minerva_x 3212.0
      minerva_y 2588.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2299"
      species_meta_id "s_id_sa2299"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 37
    zlevel -1

    cluster [
      cluster "s_id_ca242"
    ]
    graphics [
      x 500.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "pp1a_space_nsp3_minus_11__endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2220"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2220"
      minerva_name "pp1a Nsp3-11"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.19.12"
      minerva_ref_link2 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.22.69"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P0DTC1"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.22.-"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/43740578"
      minerva_ref_type__resource1 "EC__3.4.19.12"
      minerva_ref_type__resource2 "EC__3.4.22.69"
      minerva_ref_type__resource3 "UNIPROT__P0DTC1"
      minerva_ref_type__resource4 "EC__3.4.22.-"
      minerva_ref_type__resource5 "ENTREZ__43740578"
      minerva_type "Protein"
      minerva_x 3186.5
      minerva_y 751.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca242"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2220"
      species_meta_id "s_id_sa2220"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 38
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 600.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf9b_space_(_plus_)ss_space_sgmrna__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2109"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2109"
      minerva_name "Orf9b (+)ss sgmRNA"
      minerva_type "RNA"
      minerva_x 3248.9493251846206
      minerva_y 3661.3297258297252
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_sa2109"
      species_meta_id "s_id_sa2109"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 39
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 700.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp11__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2364"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2364"
      minerva_name "Nsp11"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/YP_009725312"
      minerva_ref_type__resource1 "NCBI_PROTEIN__YP_009725312"
      minerva_type "Protein"
      minerva_x 3716.576023391812
      minerva_y 1597.2318452380962
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2364"
      species_meta_id "s_id_sa2364"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 40
    zlevel -1

    cluster [
      cluster "s_id_ca221"
    ]
    graphics [
      x 800.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s__endoplasmic_space_reticulum_minus_golgi_space_intermediate_space_compartment__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa1893"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa1893"
      minerva_name "S"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P0DTC2"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/43740568"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=S"
      minerva_ref_type__resource1 "UNIPROT__P0DTC2"
      minerva_ref_type__resource2 "ENTREZ__43740568"
      minerva_ref_type__resource3 "HGNC_SYMBOL__S"
      minerva_type "Protein"
      minerva_x 4723.218137254905
      minerva_y 2647.318181818181
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca221"
      compartment_name "endoplasmic_space_reticulum_minus_Golgi_space_intermediate_space_compartment"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa1893"
      species_meta_id "s_id_sa1893"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 41
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 900.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sa2297_underscore_degraded__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2334"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2334"
      minerva_name "sa2297_degraded"
      minerva_type "Degraded"
      minerva_x 3495.0
      minerva_y 2509.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2334"
      species_meta_id "s_id_sa2334"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 42
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 1000.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf7b_space_(_minus_)ss_space_sgmrna__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2302"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2302"
      minerva_name "Orf7b (-)ss sgmRNA"
      minerva_type "RNA"
      minerva_x 3214.0
      minerva_y 2705.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2302"
      species_meta_id "s_id_sa2302"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 43
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 1100.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp5__epithelial_space_cell__n:2__2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2241"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2241"
      minerva_elementId2 "sa2366"
      minerva_homodimer "2"
      minerva_name "Nsp5"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/YP_009725301"
      minerva_ref_type__resource1 "NCBI_PROTEIN__YP_009725301"
      minerva_structuralState "N:2"
      minerva_type "Protein"
      minerva_x 2408.25
      minerva_x2 3622.5
      minerva_y 1054.5
      minerva_y2 1098.75
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2241"
      species_meta_id "s_id_sa2241"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 44
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 1200.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "replication_space_transcription_space_complex:n_space_oligomer__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa398"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa398"
      minerva_name "Replication transcription complex:N oligomer"
      minerva_type "Complex"
      minerva_x 3718.5
      minerva_y 2376.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa398"
      species_meta_id "s_id_csa398"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 45
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 200.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "(_plus_)ss_space_grna__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa1675"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa1675"
      minerva_name "(+)ss gRNA"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NC_045512"
      minerva_ref_type__resource1 "REFSEQ__NC_045512"
      minerva_type "RNA"
      minerva_x 1889.0
      minerva_y 1710.875
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa1675"
      species_meta_id "s_id_sa1675"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 46
    zlevel -1

    cluster [
      cluster "s_id_ca222"
    ]
    graphics [
      x 300.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "m__rough_space_endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa1686"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa1686"
      minerva_name "M"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/43740571"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P0DTC5"
      minerva_ref_type__resource1 "ENTREZ__43740571"
      minerva_ref_type__resource2 "UNIPROT__P0DTC5"
      minerva_type "Protein"
      minerva_x 4426.708333333334
      minerva_y 2879.083333333333
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca222"
      compartment_name "rough_space_endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa1686"
      species_meta_id "s_id_sa1686"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 47
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 400.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "e_space_(_minus_)ss_space_sgmrna__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2326"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2326"
      minerva_name "E (-)ss sgmRNA"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/43740570"
      minerva_ref_type__resource1 "ENTREZ__43740570"
      minerva_type "RNA"
      minerva_x 1753.0555555555557
      minerva_y 2272.8888888888887
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2326"
      species_meta_id "s_id_sa2326"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 48
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 500.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nucleocapsid__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa369"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa369"
      minerva_fullName "viral nucleocapsid"
      minerva_name "Nucleocapsid"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0019013"
      minerva_ref_type__resource1 "GO__GO:0019013"
      minerva_type "Complex"
      minerva_x 1905.0
      minerva_y 916.291666666667
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa369"
      species_meta_id "s_id_csa369"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 49
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 600.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf14_space_(_minus_)ss_space_sgmrna__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2301"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2301"
      minerva_name "Orf14 (-)ss sgmRNA"
      minerva_type "RNA"
      minerva_x 3210.0
      minerva_y 2669.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2301"
      species_meta_id "s_id_sa2301"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 50
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 700.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "furin__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa1921"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa1921"
      minerva_former_symbols "FUR; PACE; PCSK3"
      minerva_name "FURIN"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/8568"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_002569"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000140564"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.21.75"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P09958"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P09958"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/5045"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/5045"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=FURIN"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=FURIN"
      minerva_ref_type__resource1 "HGNC__8568"
      minerva_ref_type__resource10 "REFSEQ__NM_002569"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000140564"
      minerva_ref_type__resource3 "EC__3.4.21.75"
      minerva_ref_type__resource4 "UNIPROT__P09958"
      minerva_ref_type__resource5 "UNIPROT__P09958"
      minerva_ref_type__resource6 "ENTREZ__5045"
      minerva_ref_type__resource7 "ENTREZ__5045"
      minerva_ref_type__resource8 "HGNC_SYMBOL__FURIN"
      minerva_ref_type__resource9 "HGNC_SYMBOL__FURIN"
      minerva_type "Protein"
      minerva_x 3227.0
      minerva_y 471.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa1921"
      species_meta_id "s_id_sa1921"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 51
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 800.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp10__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2200"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2360"
      minerva_elementId2 "sa2200"
      minerva_name "Nsp10"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/YP_009725306"
      minerva_ref_type__resource1 "NCBI_PROTEIN__YP_009725306"
      minerva_type "Protein"
      minerva_x 3615.423976608188
      minerva_x2 2473.8479532163756
      minerva_y 1597.2318452380962
      minerva_y2 1604.924759816207
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2200"
      species_meta_id "s_id_sa2200"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 52
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 900.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sa2300_underscore_degraded__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2338"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2338"
      minerva_name "sa2300_degraded"
      minerva_type "Degraded"
      minerva_x 3495.0
      minerva_y 2632.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2338"
      species_meta_id "s_id_sa2338"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 53
    zlevel -1

    cluster [
      cluster "s_id_ca222"
    ]
    graphics [
      x 1000.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf6__rough_space_endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa1874"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa1874"
      minerva_name "Orf6"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/43740572"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P0DTC6"
      minerva_ref_type__resource1 "ENTREZ__43740572"
      minerva_ref_type__resource2 "UNIPROT__P0DTC6"
      minerva_type "Protein"
      minerva_x 4421.25
      minerva_y 3079.6749999999993
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca222"
      compartment_name "rough_space_endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa1874"
      species_meta_id "s_id_sa1874"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 54
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 1100.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sa2292_underscore_degraded__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2329"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2329"
      minerva_name "sa2292_degraded"
      minerva_type "Degraded"
      minerva_x 3495.0
      minerva_y 2339.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2329"
      species_meta_id "s_id_sa2329"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 55
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 1200.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s_space_(_plus_)ss_space_sgmrna__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2112"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2112"
      minerva_name "S (+)ss sgmRNA"
      minerva_type "RNA"
      minerva_x 3346.9493251846206
      minerva_y 3478.650432900433
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_sa2112"
      species_meta_id "s_id_sa2112"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 56
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 200.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "host_space_translation_space_complex__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa427"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa427"
      minerva_fullName "translation initiation complex"
      minerva_name "Host translation complex"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0070992"
      minerva_ref_type__resource1 "GO__GO:0070992"
      minerva_type "Complex"
      minerva_x 2371.0
      minerva_y 570.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa427"
      species_meta_id "s_id_csa427"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 57
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 300.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp14__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2206"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2206"
      minerva_name "Nsp14"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/YP_009725309"
      minerva_ref_type__resource1 "NCBI_PROTEIN__YP_009725309"
      minerva_type "Protein"
      minerva_x 2216.777777777779
      minerva_y 1606.924759816207
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2206"
      species_meta_id "s_id_sa2206"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 58
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 400.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "(_plus_)ss_space_grna__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2153"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2153"
      minerva_name "(+)ss gRNA"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NC_045512"
      minerva_ref_type__resource1 "REFSEQ__NC_045512"
      minerva_type "RNA"
      minerva_x 3438.500000000001
      minerva_y 3134.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2153"
      species_meta_id "s_id_sa2153"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 59
    zlevel -1

    cluster [
      cluster "s_id_ca221"
    ]
    graphics [
      x 500.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf7a__endoplasmic_space_reticulum_minus_golgi_space_intermediate_space_compartment__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2245"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2245"
      minerva_name "Orf7a"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/43740573"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P0DTC7"
      minerva_ref_type__resource1 "ENTREZ__43740573"
      minerva_ref_type__resource2 "UNIPROT__P0DTC7"
      minerva_type "Protein"
      minerva_x 4823.9381977671455
      minerva_y 2645.3181818181815
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca221"
      compartment_name "endoplasmic_space_reticulum_minus_Golgi_space_intermediate_space_compartment"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2245"
      species_meta_id "s_id_sa2245"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 60
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 600.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cellular_space_response_space_to_space_exogenous_space_dsrna__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2291"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2291"
      minerva_fullName "cellular response to exogenous dsRNA"
      minerva_name "cellular response to exogenous dsRNA"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0071360"
      minerva_ref_type__resource1 "GO__GO:0071360"
      minerva_type "Phenotype"
      minerva_x 2164.0
      minerva_y 1381.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2291"
      species_meta_id "s_id_sa2291"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 61
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 700.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "e_space_ds_space_sgmrna__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2344"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2344"
      minerva_name "E ds sgmRNA"
      minerva_type "RNA"
      minerva_x 2548.0
      minerva_y 2379.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2344"
      species_meta_id "s_id_sa2344"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 62
    zlevel -1

    cluster [
      cluster "s_id_ca242"
    ]
    graphics [
      x 800.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp3_minus_4__endoplasmic_space_reticulum__glycosylated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2365"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2243"
      minerva_elementId2 "sa2365"
      minerva_name "Nsp3-4"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/YP_009725300"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/YP_009725299"
      minerva_ref_type__resource1 "NCBI_PROTEIN__YP_009725300"
      minerva_ref_type__resource2 "NCBI_PROTEIN__YP_009725299"
      minerva_state1 "GLYCOSYLATED"
      minerva_type "Protein"
      minerva_x 2642.25
      minerva_x2 3273.5
      minerva_y 1211.25
      minerva_y2 1278.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca242"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2365"
      species_meta_id "s_id_sa2365"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 63
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 900.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sa2295_underscore_degraded__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2332"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2332"
      minerva_name "sa2295_degraded"
      minerva_type "Degraded"
      minerva_x 3495.0
      minerva_y 2422.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2332"
      species_meta_id "s_id_sa2332"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 64
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 1000.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf6_space_(_minus_)ss_space_sgmrna__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2297"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2297"
      minerva_name "Orf6 (-)ss sgmRNA"
      minerva_type "RNA"
      minerva_x 3210.0
      minerva_y 2507.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2297"
      species_meta_id "s_id_sa2297"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 65
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 1100.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "n_space__space_(_minus_)ss_space_sgmrna__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2353"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2353"
      minerva_name "N  (-)ss sgmRNA"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/43740575"
      minerva_ref_type__resource1 "ENTREZ__43740575"
      minerva_type "RNA"
      minerva_x 1753.0555555555557
      minerva_y 2195.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2353"
      species_meta_id "s_id_sa2353"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 66
    zlevel -1

    cluster [
      cluster "s_id_ca242"
    ]
    graphics [
      x 1200.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "endoplasmic_space_reticulum__endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa439"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa439"
      minerva_fullName "endoplasmic reticulum"
      minerva_name "Endoplasmic reticulum"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0005783"
      minerva_ref_type__resource1 "GO__GO:0005783"
      minerva_type "Complex"
      minerva_x 3103.5
      minerva_y 1520.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca242"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa439"
      species_meta_id "s_id_csa439"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 67
    zlevel -1

    cluster [
      cluster "s_id_ca242"
    ]
    graphics [
      x 200.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp3_minus_4__endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2359"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2359"
      minerva_elementId2 "sa2242"
      minerva_name "Nsp3-4"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/YP_009725300"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/YP_009725299"
      minerva_ref_type__resource1 "NCBI_PROTEIN__YP_009725300"
      minerva_ref_type__resource2 "NCBI_PROTEIN__YP_009725299"
      minerva_type "Protein"
      minerva_x 3294.5
      minerva_x2 2640.0
      minerva_y 1155.5
      minerva_y2 1101.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca242"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2359"
      species_meta_id "s_id_sa2359"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 68
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 300.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s1:nrp1_space_complex__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa441"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa441"
      minerva_name "S1:NRP1 complex"
      minerva_type "Complex"
      minerva_x 1863.5
      minerva_y 410.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa441"
      species_meta_id "s_id_csa441"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 69
    zlevel -1

    cluster [
      cluster "s_id_ca221"
    ]
    graphics [
      x 400.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf9b__endoplasmic_space_reticulum_minus_golgi_space_intermediate_space_compartment__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2249"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2249"
      minerva_name "Orf9b"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P0DTD2"
      minerva_ref_type__resource1 "UNIPROT__P0DTD2"
      minerva_type "Protein"
      minerva_x 5178.6048644338125
      minerva_y 2645.3181818181815
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca221"
      compartment_name "endoplasmic_space_reticulum_minus_Golgi_space_intermediate_space_compartment"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2249"
      species_meta_id "s_id_sa2249"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 70
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 500.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "e_space_(_plus_)ss_space_sgmrna__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2111"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2111"
      minerva_name "E (+)ss sgmRNA"
      minerva_type "RNA"
      minerva_x 3360.9493251846206
      minerva_y 3439.650432900433
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_sa2111"
      species_meta_id "s_id_sa2111"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 71
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 600.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sa2314_underscore_degraded__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2315"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2315"
      minerva_name "sa2314_degraded"
      minerva_type "Degraded"
      minerva_x 3494.0
      minerva_y 2257.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2315"
      species_meta_id "s_id_sa2315"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 72
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 700.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf7a__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2067"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2067"
      minerva_name "Orf7a"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/43740573"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P0DTC7"
      minerva_ref_type__resource1 "ENTREZ__43740573"
      minerva_ref_type__resource2 "UNIPROT__P0DTC7"
      minerva_type "Protein"
      minerva_x 2179.4578272604585
      minerva_y 465.5159240430618
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2067"
      species_meta_id "s_id_sa2067"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 73
    zlevel -1

    cluster [
      cluster "s_id_ca222"
    ]
    graphics [
      x 800.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf9b__rough_space_endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa1878"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa1878"
      minerva_name "Orf9b"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P0DTD2"
      minerva_ref_type__resource1 "UNIPROT__P0DTD2"
      minerva_type "Protein"
      minerva_x 4423.25
      minerva_y 3225.6749999999993
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca222"
      compartment_name "rough_space_endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa1878"
      species_meta_id "s_id_sa1878"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 74
    zlevel -1

    cluster [
      cluster "s_id_ca222"
    ]
    graphics [
      x 900.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf7b__rough_space_endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa1876"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa1876"
      minerva_name "Orf7b"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/43740574"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P0DTD8"
      minerva_ref_type__resource1 "ENTREZ__43740574"
      minerva_ref_type__resource2 "UNIPROT__P0DTD8"
      minerva_type "Protein"
      minerva_x 4417.916666666667
      minerva_y 3322.6749999999993
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca222"
      compartment_name "rough_space_endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa1876"
      species_meta_id "s_id_sa1876"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 75
    zlevel -1

    cluster [
      cluster "s_id_ca238"
    ]
    graphics [
      x 1000.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s2__sars_minus_cov_minus_2_space_virion__exposed hr1-hr2-fp __none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2188"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2188"
      minerva_elementId2 "sa2063"
      minerva_name "S2"
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002552"
      minerva_ref_type__resource1 "INTERPRO__IPR002552"
      minerva_structuralState "exposed HR1-HR2-FP "
      minerva_type "Protein"
      minerva_x 1105.3116515837107
      minerva_x2 2913.1057692307695
      minerva_y 1031.1764705882354
      minerva_y2 460.8461538461538
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca238"
      compartment_name "SARS_minus_CoV_minus_2_space_virion"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2188"
      species_meta_id "s_id_sa2188"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 76
    zlevel -1

    cluster [
      cluster "s_id_ca218"
    ]
    graphics [
      x 1100.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ace2:spike_space_complex__sars_minus_cov_minus_2_space_virion__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa430"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa430"
      minerva_elementId2 "csa431"
      minerva_elementId3 "csa368"
      minerva_name "ACE2:SPIKE complex"
      minerva_type "Complex"
      minerva_x 629.5
      minerva_x2 566.5
      minerva_x3 3540.3461538461524
      minerva_y 434.5
      minerva_y2 1091.5
      minerva_y3 462.41346153846143
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca218"
      compartment_name "SARS_minus_CoV_minus_2_space_virion"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa430"
      species_meta_id "s_id_csa430"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 77
    zlevel -1

    cluster [
      cluster "s_id_ca242"
    ]
    graphics [
      x 1200.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "host_space_translation__endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2237"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2237"
      minerva_fullName "translation"
      minerva_name "Host translation"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0006412"
      minerva_ref_type__resource1 "GO__GO:0006412"
      minerva_type "Phenotype"
      minerva_x 2747.0
      minerva_y 645.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca242"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2237"
      species_meta_id "s_id_sa2237"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 78
    zlevel -1

    cluster [
      cluster "s_id_ca221"
    ]
    graphics [
      x 200.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nucleocapsid__endoplasmic_space_reticulum_minus_golgi_space_intermediate_space_compartment__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa397"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa397"
      minerva_fullName "viral nucleocapsid"
      minerva_name "Nucleocapsid"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0019013"
      minerva_ref_type__resource1 "GO__GO:0019013"
      minerva_type "Complex"
      minerva_x 4689.0
      minerva_y 2147.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca221"
      compartment_name "endoplasmic_space_reticulum_minus_Golgi_space_intermediate_space_compartment"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa397"
      species_meta_id "s_id_csa397"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 79
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 300.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "n__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa1667"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa1685"
      minerva_elementId2 "sa1667"
      minerva_name "N"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=N"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/43740575"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P0DTC9"
      minerva_ref_type__resource1 "HGNC_SYMBOL__N"
      minerva_ref_type__resource2 "ENTREZ__43740575"
      minerva_ref_type__resource3 "UNIPROT__P0DTC9"
      minerva_type "Protein"
      minerva_x 3762.041666666668
      minerva_x2 1745.8181818181815
      minerva_y 3055.083333333333
      minerva_y2 1274.454545454545
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa1667"
      species_meta_id "s_id_sa1667"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 80
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 400.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "m_space_(_plus_)ss_space_sgmrna__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2113"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2113"
      minerva_name "M (+)ss sgmRNA"
      minerva_type "RNA"
      minerva_x 3382.9493251846206
      minerva_y 3402.650432900433
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2113"
      species_meta_id "s_id_sa2113"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 81
    zlevel -1

    cluster [
      cluster "s_id_ca221"
    ]
    graphics [
      x 500.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf7b__endoplasmic_space_reticulum_minus_golgi_space_intermediate_space_compartment__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2251"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2251"
      minerva_name "Orf7b"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/43740574"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P0DTD8"
      minerva_ref_type__resource1 "ENTREZ__43740574"
      minerva_ref_type__resource2 "UNIPROT__P0DTD8"
      minerva_type "Protein"
      minerva_x 5365.2715311004795
      minerva_y 2645.3181818181815
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca221"
      compartment_name "endoplasmic_space_reticulum_minus_Golgi_space_intermediate_space_compartment"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2251"
      species_meta_id "s_id_sa2251"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 82
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 600.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf8_space_ds_space_sgmrna__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2349"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2349"
      minerva_name "Orf8 ds sgmRNA"
      minerva_type "RNA"
      minerva_x 2548.0
      minerva_y 2583.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2349"
      species_meta_id "s_id_sa2349"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 83
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 700.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "(_minus_)ss_space_grna__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2328"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2314"
      minerva_elementId2 "sa2328"
      minerva_name "(-)ss gRNA"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NC_045512"
      minerva_ref_type__resource1 "REFSEQ__NC_045512"
      minerva_type "RNA"
      minerva_x 3251.5
      minerva_x2 1838.0555555555557
      minerva_y 2255.5
      minerva_y2 2145.3888888888887
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2328"
      species_meta_id "s_id_sa2328"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 84
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 800.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s_space_(_minus_)ss_space_sgmrna__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2295"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2295"
      minerva_name "S (-)ss sgmRNA"
      minerva_type "RNA"
      minerva_x 3217.0
      minerva_y 2422.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2295"
      species_meta_id "s_id_sa2295"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 85
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 900.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf6_space_ds_space_sgmrna__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2347"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2347"
      minerva_name "Orf6 ds sgmRNA"
      minerva_type "RNA"
      minerva_x 2548.0
      minerva_y 2505.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2347"
      species_meta_id "s_id_sa2347"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 86
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 1000.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "replication_space_transcription_space_complex__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa440"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa440"
      minerva_elementId2 "csa410"
      minerva_name "Replication transcription complex"
      minerva_type "Complex"
      minerva_x 3478.1098901098903
      minerva_x2 2346.7142857142862
      minerva_y 1935.1428571428573
      minerva_y2 1943.857142857143
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa440"
      species_meta_id "s_id_csa440"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 87
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 1100.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sa2355_underscore_degraded__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2368"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2368"
      minerva_name "sa2355_degraded"
      minerva_type "Degraded"
      minerva_x 3495.0
      minerva_y 2310.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2368"
      species_meta_id "s_id_sa2368"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 88
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 1200.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "m_space_(_minus_)ss_space_sgmrna__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2292"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2292"
      minerva_name "M (-)ss sgmRNA"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/43740571"
      minerva_ref_type__resource1 "ENTREZ__43740571"
      minerva_type "RNA"
      minerva_x 3212.0
      minerva_y 2343.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2292"
      species_meta_id "s_id_sa2292"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 89
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 200.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf9b_space_ds_space_sgmrna__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2350"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2350"
      minerva_name "Orf9b ds sgmRNA"
      minerva_type "RNA"
      minerva_x 2548.0
      minerva_y 2625.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2350"
      species_meta_id "s_id_sa2350"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 90
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 300.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "n__epithelial_space_cell__oligomer__2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa1887"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa1887"
      minerva_homodimer "2"
      minerva_name "N"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=N"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/43740575"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P0DTC9"
      minerva_ref_type__resource1 "HGNC_SYMBOL__N"
      minerva_ref_type__resource2 "ENTREZ__43740575"
      minerva_ref_type__resource3 "UNIPROT__P0DTC9"
      minerva_structuralState "oligomer"
      minerva_type "Protein"
      minerva_x 3773.2222222222226
      minerva_y 2942.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa1887"
      species_meta_id "s_id_sa1887"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 91
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 400.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf6_space__space_(_plus_)ss_space_sgmrna__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2108"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2108"
      minerva_name "Orf6  (+)ss sgmRNA"
      minerva_type "RNA"
      minerva_x 3310.9493251846206
      minerva_y 3554.6075036075035
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_sa2108"
      species_meta_id "s_id_sa2108"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 92
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 500.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf8_space_(_plus_)ss_space_sgmrna__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2107"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2107"
      minerva_name "Orf8 (+)ss sgmRNA"
      minerva_type "RNA"
      minerva_x 3273.9493251846206
      minerva_y 3624.3297258297252
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_sa2107"
      species_meta_id "s_id_sa2107"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 93
    zlevel -1

    cluster [
      cluster "s_id_ca221"
    ]
    graphics [
      x 600.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "e__endoplasmic_space_reticulum_minus_golgi_space_intermediate_space_compartment__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa1892"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa1892"
      minerva_name "E"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/43740570"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=E"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P0DTC4"
      minerva_ref_type__resource1 "ENTREZ__43740570"
      minerva_ref_type__resource2 "HGNC_SYMBOL__E"
      minerva_ref_type__resource3 "UNIPROT__P0DTC4"
      minerva_type "Protein"
      minerva_x 4624.551470588238
      minerva_y 2644.318181818181
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca221"
      compartment_name "endoplasmic_space_reticulum_minus_Golgi_space_intermediate_space_compartment"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa1892"
      species_meta_id "s_id_sa1892"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 94
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 700.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sa2296_underscore_degraded__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2333"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2333"
      minerva_name "sa2296_degraded"
      minerva_type "Degraded"
      minerva_x 3495.0
      minerva_y 2469.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2333"
      species_meta_id "s_id_sa2333"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 95
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 800.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ace2__epithelial_space_cell__n:2__2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa1462"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa1462"
      minerva_elementId2 "sa1545"
      minerva_homodimer "2"
      minerva_name "ACE2"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000130234"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ACE2"
      minerva_ref_link11 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ACE2"
      minerva_ref_link2 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.17.23"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.17.-"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/13557"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/Q9BYF1"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/Q9BYF1"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/59272"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_001371415"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/59272"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000130234"
      minerva_ref_type__resource10 "HGNC_SYMBOL__ACE2"
      minerva_ref_type__resource11 "HGNC_SYMBOL__ACE2"
      minerva_ref_type__resource2 "EC__3.4.17.23"
      minerva_ref_type__resource3 "EC__3.4.17.-"
      minerva_ref_type__resource4 "HGNC__13557"
      minerva_ref_type__resource5 "UNIPROT__Q9BYF1"
      minerva_ref_type__resource6 "UNIPROT__Q9BYF1"
      minerva_ref_type__resource7 "ENTREZ__59272"
      minerva_ref_type__resource8 "REFSEQ__NM_001371415"
      minerva_ref_type__resource9 "ENTREZ__59272"
      minerva_structuralState "N:2"
      minerva_type "Protein"
      minerva_x 223.5
      minerva_x2 3869.0
      minerva_y 472.5
      minerva_y2 476.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa1462"
      species_meta_id "s_id_sa1462"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 96
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 900.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s_space_ds_space_sgmrna__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2345"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2345"
      minerva_name "S ds sgmRNA"
      minerva_type "RNA"
      minerva_x 2548.0
      minerva_y 2419.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2345"
      species_meta_id "s_id_sa2345"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 97
    zlevel -1

    cluster [
      cluster "s_id_ca242"
    ]
    graphics [
      x 1000.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "pp1a_space_nsp3_minus_11__endoplasmic_space_reticulum__n:2__2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2240"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2240"
      minerva_homodimer "2"
      minerva_name "pp1a Nsp3-11"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.19.12"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.6.4.12"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/gene/43740578"
      minerva_ref_link12 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.1.13.-"
      minerva_ref_link2 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.22.69"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P0DTD1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=rep"
      minerva_ref_link5 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.1.-.-"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.7.48"
      minerva_ref_link7 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.1.1.-"
      minerva_ref_link8 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.22.-"
      minerva_ref_link9 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.6.4.13"
      minerva_ref_type__resource1 "EC__3.4.19.12"
      minerva_ref_type__resource10 "EC__3.6.4.12"
      minerva_ref_type__resource11 "ENTREZ__43740578"
      minerva_ref_type__resource12 "EC__3.1.13.-"
      minerva_ref_type__resource2 "EC__3.4.22.69"
      minerva_ref_type__resource3 "UNIPROT__P0DTD1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__rep"
      minerva_ref_type__resource5 "EC__3.1.-.-"
      minerva_ref_type__resource6 "EC__2.7.7.48"
      minerva_ref_type__resource7 "EC__2.1.1.-"
      minerva_ref_type__resource8 "EC__3.4.22.-"
      minerva_ref_type__resource9 "EC__3.6.4.13"
      minerva_structuralState "N:2"
      minerva_type "Protein"
      minerva_x 3249.5
      minerva_y 864.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca242"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2240"
      species_meta_id "s_id_sa2240"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 98
    zlevel -1

    cluster [
      cluster "s_id_ca206"
    ]
    graphics [
      x 1100.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "e__endosome__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2115"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2115"
      minerva_name "E"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/43740570"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=E"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P0DTC4"
      minerva_ref_type__resource1 "ENTREZ__43740570"
      minerva_ref_type__resource2 "HGNC_SYMBOL__E"
      minerva_ref_type__resource3 "UNIPROT__P0DTC4"
      minerva_type "Protein"
      minerva_x 1790.5627530364372
      minerva_y 816.7255187246965
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca206"
      compartment_name "endosome"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2115"
      species_meta_id "s_id_sa2115"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 99
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 1200.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "double_minus_membrane_space_vesicle__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa438"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa438"
      minerva_fullName "double membrane vesicle viral factory"
      minerva_name "Double-membrane vesicle"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0039718"
      minerva_ref_type__resource1 "GO__GO:0039718"
      minerva_type "Complex"
      minerva_x 3094.0
      minerva_y 1892.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa438"
      species_meta_id "s_id_csa438"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 100
    zlevel -1

    cluster [
      cluster "s_id_ca242"
    ]
    graphics [
      x 200.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "pp1ab_space_nsp6_minus_16__endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2229"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2229"
      minerva_name "pp1ab nsp6-16"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.19.12"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.6.4.12"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/gene/43740578"
      minerva_ref_link12 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.1.13.-"
      minerva_ref_link2 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.22.69"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P0DTD1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=rep"
      minerva_ref_link5 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.1.-.-"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.7.48"
      minerva_ref_link7 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.1.1.-"
      minerva_ref_link8 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.22.-"
      minerva_ref_link9 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.6.4.13"
      minerva_ref_type__resource1 "EC__3.4.19.12"
      minerva_ref_type__resource10 "EC__3.6.4.12"
      minerva_ref_type__resource11 "ENTREZ__43740578"
      minerva_ref_type__resource12 "EC__3.1.13.-"
      minerva_ref_type__resource2 "EC__3.4.22.69"
      minerva_ref_type__resource3 "UNIPROT__P0DTD1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__rep"
      minerva_ref_type__resource5 "EC__3.1.-.-"
      minerva_ref_type__resource6 "EC__2.7.7.48"
      minerva_ref_type__resource7 "EC__2.1.1.-"
      minerva_ref_type__resource8 "EC__3.4.22.-"
      minerva_ref_type__resource9 "EC__3.6.4.13"
      minerva_type "Protein"
      minerva_x 2629.0
      minerva_y 1021.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca242"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2229"
      species_meta_id "s_id_sa2229"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 101
    zlevel -1

    cluster [
      cluster "s_id_ca221"
    ]
    graphics [
      x 300.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf3a__endoplasmic_space_reticulum_minus_golgi_space_intermediate_space_compartment__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2247"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2247"
      minerva_name "Orf3a"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P0DTC3"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/43740569"
      minerva_ref_type__resource1 "UNIPROT__P0DTC3"
      minerva_ref_type__resource2 "ENTREZ__43740569"
      minerva_type "Protein"
      minerva_x 5003.2715311004795
      minerva_y 2645.3181818181815
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca221"
      compartment_name "endoplasmic_space_reticulum_minus_Golgi_space_intermediate_space_compartment"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2247"
      species_meta_id "s_id_sa2247"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 102
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 400.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf3a_space_(_minus_)ss_space_sgmrna__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2322"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2322"
      minerva_name "Orf3a (-)ss sgmRNA"
      minerva_type "RNA"
      minerva_x 1753.0555555555557
      minerva_y 2435.8888888888887
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2322"
      species_meta_id "s_id_sa2322"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 103
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 500.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf7b_space_(_plus_)ss_space_sgmrna__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2106"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2106"
      minerva_name "Orf7b (+)ss sgmRNA"
      minerva_type "RNA"
      minerva_x 3197.9493251846206
      minerva_y 3736.6075036075035
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_sa2106"
      species_meta_id "s_id_sa2106"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 104
    zlevel -1

    cluster [
      cluster "default"
    ]
    graphics [
      x 600.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s1__default__cendr__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa1539"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa1539"
      minerva_name "S1"
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_structuralState "CendR"
      minerva_type "Protein"
      minerva_x 3103.0
      minerva_y 275.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "default"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa1539"
      species_meta_id "s_id_sa1539"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 105
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 700.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sa2299_underscore_degraded__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2337"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2337"
      minerva_name "sa2299_degraded"
      minerva_type "Degraded"
      minerva_x 3495.0
      minerva_y 2588.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2337"
      species_meta_id "s_id_sa2337"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 106
    zlevel -1

    cluster [
      cluster "s_id_ca222"
    ]
    graphics [
      x 800.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf8__rough_space_endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa1879"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa1879"
      minerva_name "Orf8"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P0DTC8"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/43740577"
      minerva_ref_type__resource1 "UNIPROT__P0DTC8"
      minerva_ref_type__resource2 "ENTREZ__43740577"
      minerva_type "Protein"
      minerva_x 4421.916666666667
      minerva_y 3176.6749999999993
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca222"
      compartment_name "rough_space_endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa1879"
      species_meta_id "s_id_sa1879"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 107
    zlevel -1

    cluster [
      cluster "s_id_ca242"
    ]
    graphics [
      x 900.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "pp1ab_space_nsp3_minus_16__endoplasmic_space_reticulum__n:2__2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2244"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2244"
      minerva_homodimer "2"
      minerva_name "pp1ab Nsp3-16"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.19.12"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.6.4.12"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/gene/43740578"
      minerva_ref_link12 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.1.13.-"
      minerva_ref_link2 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.22.69"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P0DTD1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=rep"
      minerva_ref_link5 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.1.-.-"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.7.48"
      minerva_ref_link7 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.1.1.-"
      minerva_ref_link8 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.22.-"
      minerva_ref_link9 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.6.4.13"
      minerva_ref_type__resource1 "EC__3.4.19.12"
      minerva_ref_type__resource10 "EC__3.6.4.12"
      minerva_ref_type__resource11 "ENTREZ__43740578"
      minerva_ref_type__resource12 "EC__3.1.13.-"
      minerva_ref_type__resource2 "EC__3.4.22.69"
      minerva_ref_type__resource3 "UNIPROT__P0DTD1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__rep"
      minerva_ref_type__resource5 "EC__3.1.-.-"
      minerva_ref_type__resource6 "EC__2.7.7.48"
      minerva_ref_type__resource7 "EC__2.1.1.-"
      minerva_ref_type__resource8 "EC__3.4.22.-"
      minerva_ref_type__resource9 "EC__3.6.4.13"
      minerva_structuralState "N:2"
      minerva_type "Protein"
      minerva_x 2664.5
      minerva_y 878.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca242"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2244"
      species_meta_id "s_id_sa2244"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 108
    zlevel -1

    cluster [
      cluster "s_id_ca206"
    ]
    graphics [
      x 1000.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "m__endosome__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2116"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2116"
      minerva_name "M"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/43740571"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P0DTC5"
      minerva_ref_type__resource1 "ENTREZ__43740571"
      minerva_ref_type__resource2 "UNIPROT__P0DTC5"
      minerva_type "Protein"
      minerva_x 1778.5627530364372
      minerva_y 1010.7255187246965
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca206"
      compartment_name "endosome"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2116"
      species_meta_id "s_id_sa2116"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 109
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 1100.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "host_space_translation_space_complex__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa429"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa429"
      minerva_fullName "translation initiation complex"
      minerva_name "Host translation complex"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0070992"
      minerva_ref_type__resource1 "GO__GO:0070992"
      minerva_type "Complex"
      minerva_x 3779.1111111111113
      minerva_y 3153.333333333333
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa429"
      species_meta_id "s_id_csa429"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 110
    zlevel -1

    cluster [
      cluster "s_id_ca242"
    ]
    graphics [
      x 1200.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp7__endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2203"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2363"
      minerva_elementId2 "sa2203"
      minerva_name "Nsp7"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/YP_009725303"
      minerva_ref_type__resource1 "NCBI_PROTEIN__YP_009725303"
      minerva_type "Protein"
      minerva_x 3300.423976608188
      minerva_x2 2738.8479532163756
      minerva_y 1597.2318452380962
      minerva_y2 1604.924759816207
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca242"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2203"
      species_meta_id "s_id_sa2203"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 111
    zlevel -1

    cluster [
      cluster "s_id_ca242"
    ]
    graphics [
      x 200.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp3__endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2222"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2222"
      minerva_name "Nsp3"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/YP_009725299"
      minerva_ref_type__resource1 "NCBI_PROTEIN__YP_009725299"
      minerva_ref_type__resource2 "UNIPROT__Nsp3"
      minerva_type "Protein"
      minerva_x 2919.5
      minerva_y 1614.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca242"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2222"
      species_meta_id "s_id_sa2222"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 112
    zlevel -1

    cluster [
      cluster "default"
    ]
    graphics [
      x 300.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ace2__default__100 kda fragment__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2239"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2239"
      minerva_name "ACE2"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000130234"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ACE2"
      minerva_ref_link11 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ACE2"
      minerva_ref_link2 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.17.23"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.17.-"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/13557"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/Q9BYF1"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/Q9BYF1"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/59272"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_001371415"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/59272"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000130234"
      minerva_ref_type__resource10 "HGNC_SYMBOL__ACE2"
      minerva_ref_type__resource11 "HGNC_SYMBOL__ACE2"
      minerva_ref_type__resource2 "EC__3.4.17.23"
      minerva_ref_type__resource3 "EC__3.4.17.-"
      minerva_ref_type__resource4 "HGNC__13557"
      minerva_ref_type__resource5 "UNIPROT__Q9BYF1"
      minerva_ref_type__resource6 "UNIPROT__Q9BYF1"
      minerva_ref_type__resource7 "ENTREZ__59272"
      minerva_ref_type__resource8 "REFSEQ__NM_001371415"
      minerva_ref_type__resource9 "ENTREZ__59272"
      minerva_structuralState "100 Kda fragment"
      minerva_type "Protein"
      minerva_x 3200.0
      minerva_y 196.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "default"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2239"
      species_meta_id "s_id_sa2239"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 113
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 400.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ace2__epithelial_space_cell__15 kda fragment__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2238"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2238"
      minerva_name "ACE2"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000130234"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ACE2"
      minerva_ref_link11 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ACE2"
      minerva_ref_link2 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.17.23"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.17.-"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/13557"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/Q9BYF1"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/Q9BYF1"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/59272"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_001371415"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/59272"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000130234"
      minerva_ref_type__resource10 "HGNC_SYMBOL__ACE2"
      minerva_ref_type__resource11 "HGNC_SYMBOL__ACE2"
      minerva_ref_type__resource2 "EC__3.4.17.23"
      minerva_ref_type__resource3 "EC__3.4.17.-"
      minerva_ref_type__resource4 "HGNC__13557"
      minerva_ref_type__resource5 "UNIPROT__Q9BYF1"
      minerva_ref_type__resource6 "UNIPROT__Q9BYF1"
      minerva_ref_type__resource7 "ENTREZ__59272"
      minerva_ref_type__resource8 "REFSEQ__NM_001371415"
      minerva_ref_type__resource9 "ENTREZ__59272"
      minerva_structuralState "15 Kda fragment"
      minerva_type "Protein"
      minerva_x 3119.0
      minerva_y 470.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2238"
      species_meta_id "s_id_sa2238"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 114
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 500.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp13__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2198"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2198"
      minerva_name "Nsp13"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/YP_009725308"
      minerva_ref_type__resource1 "NCBI_PROTEIN__YP_009725308"
      minerva_type "Protein"
      minerva_x 2302.444444444446
      minerva_y 1606.924759816207
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2198"
      species_meta_id "s_id_sa2198"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 115
    zlevel -1

    cluster [
      cluster "s_id_ca242"
    ]
    graphics [
      x 600.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "pp1ab_space_nsp3_minus_16__endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2221"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2221"
      minerva_name "pp1ab Nsp3-16"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.19.12"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.6.4.12"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/gene/43740578"
      minerva_ref_link12 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.1.13.-"
      minerva_ref_link2 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.22.69"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P0DTD1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=rep"
      minerva_ref_link5 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.1.-.-"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.7.48"
      minerva_ref_link7 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.1.1.-"
      minerva_ref_link8 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.22.-"
      minerva_ref_link9 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.6.4.13"
      minerva_ref_type__resource1 "EC__3.4.19.12"
      minerva_ref_type__resource10 "EC__3.6.4.12"
      minerva_ref_type__resource11 "ENTREZ__43740578"
      minerva_ref_type__resource12 "EC__3.1.13.-"
      minerva_ref_type__resource2 "EC__3.4.22.69"
      minerva_ref_type__resource3 "UNIPROT__P0DTD1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__rep"
      minerva_ref_type__resource5 "EC__3.1.-.-"
      minerva_ref_type__resource6 "EC__2.7.7.48"
      minerva_ref_type__resource7 "EC__2.1.1.-"
      minerva_ref_type__resource8 "EC__3.4.22.-"
      minerva_ref_type__resource9 "EC__3.6.4.13"
      minerva_type "Protein"
      minerva_x 2711.5
      minerva_y 755.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca242"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2221"
      species_meta_id "s_id_sa2221"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 116
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 700.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf14_space_(_minus_)ss_space_sgmrna__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2319"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2319"
      minerva_name "Orf14 (-)ss sgmRNA"
      minerva_type "RNA"
      minerva_x 1753.0555555555557
      minerva_y 2551.8888888888887
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2319"
      species_meta_id "s_id_sa2319"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 117
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 800.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp8__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2362"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2362"
      minerva_name "Nsp8"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/YP_009725304"
      minerva_ref_type__resource1 "NCBI_PROTEIN__YP_009725304"
      minerva_type "Protein"
      minerva_x 3409.423976608188
      minerva_y 1597.2318452380962
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2362"
      species_meta_id "s_id_sa2362"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 118
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 900.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf7a_space_ds_space_sgmrna__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2346"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2346"
      minerva_name "Orf7a ds sgmRNA"
      minerva_type "RNA"
      minerva_x 2548.0
      minerva_y 2462.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2346"
      species_meta_id "s_id_sa2346"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 119
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 1000.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ds_space_grna__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2342"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2342"
      minerva_name "ds gRNA"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NC_045512"
      minerva_ref_type__resource1 "REFSEQ__NC_045512"
      minerva_type "RNA"
      minerva_x 2577.0
      minerva_y 2252.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2342"
      species_meta_id "s_id_sa2342"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 120
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 1100.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cholesterol__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2372"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2372"
      minerva_fullName "cholesterol"
      minerva_name "cholesterol"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:16113"
      minerva_ref_type__resource1 "CHEBI__CHEBI:16113"
      minerva_synonyms "(3beta,14beta,17alpha)-cholest-5-en-3-ol; CHOLESTEROL; Cholest-5-en-3beta-ol; Cholesterin; Cholesterol; cholesterol"
      minerva_type "Simple molecule"
      minerva_x 2026.5
      minerva_y 465.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2372"
      species_meta_id "s_id_sa2372"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 121
    zlevel -1

    cluster [
      cluster "s_id_ca221"
    ]
    graphics [
      x 1200.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "m__endoplasmic_space_reticulum_minus_golgi_space_intermediate_space_compartment__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa1891"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa1891"
      minerva_name "M"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/43740571"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P0DTC5"
      minerva_ref_type__resource1 "ENTREZ__43740571"
      minerva_ref_type__resource2 "UNIPROT__P0DTC5"
      minerva_type "Protein"
      minerva_x 4526.884803921571
      minerva_y 2645.318181818181
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca221"
      compartment_name "endoplasmic_space_reticulum_minus_Golgi_space_intermediate_space_compartment"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa1891"
      species_meta_id "s_id_sa1891"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 122
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 200.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf3a_space_(_plus_)ss_space_sgmrna__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2104"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2104"
      minerva_name "Orf3a (+)ss sgmRNA"
      minerva_type "RNA"
      minerva_x 3293.9493251846206
      minerva_y 3589.6075036075035
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_sa2104"
      species_meta_id "s_id_sa2104"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 123
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 300.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf7a_space_(_plus_)ss_space_sgmrna__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2105"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2105"
      minerva_name "Orf7a (+)ss sgmRNA"
      minerva_type "RNA"
      minerva_x 3328.9493251846206
      minerva_y 3518.6075036075035
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_sa2105"
      species_meta_id "s_id_sa2105"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 124
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 400.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "n_space_ds_space_sgmrna__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2354"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2354"
      minerva_name "N ds sgmRNA"
      minerva_type "RNA"
      minerva_x 2548.0
      minerva_y 2308.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2354"
      species_meta_id "s_id_sa2354"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 125
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 500.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf7a_space_(_minus_)ss_space_sgmrna__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2296"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2296"
      minerva_name "Orf7a (-)ss sgmRNA"
      minerva_type "RNA"
      minerva_x 3212.0
      minerva_y 2470.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2296"
      species_meta_id "s_id_sa2296"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 126
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 600.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "e_space_(_minus_)ss_space_sgmrna__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2294"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2294"
      minerva_name "E (-)ss sgmRNA"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/43740570"
      minerva_ref_type__resource1 "ENTREZ__43740570"
      minerva_type "RNA"
      minerva_x 3209.0
      minerva_y 2381.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2294"
      species_meta_id "s_id_sa2294"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 127
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 700.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "m__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2066"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2066"
      minerva_name "M"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/43740571"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P0DTC5"
      minerva_ref_type__resource1 "ENTREZ__43740571"
      minerva_ref_type__resource2 "UNIPROT__P0DTC5"
      minerva_type "Protein"
      minerva_x 2280.583333333333
      minerva_y 471.96696149245474
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2066"
      species_meta_id "s_id_sa2066"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 128
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 800.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s_space_(_minus_)ss_space_sgmrna__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2325"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2325"
      minerva_name "S (-)ss sgmRNA"
      minerva_type "RNA"
      minerva_x 1753.0555555555557
      minerva_y 2311.8888888888887
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2325"
      species_meta_id "s_id_sa2325"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 129
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 900.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp15__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2205"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2205"
      minerva_name "Nsp15"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/YP_009725310"
      minerva_ref_type__resource1 "NCBI_PROTEIN__YP_009725310"
      minerva_type "Protein"
      minerva_x 2130.111111111112
      minerva_y 1609.924759816207
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2205"
      species_meta_id "s_id_sa2205"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 130
    zlevel -1

    cluster [
      cluster "s_id_ca222"
    ]
    graphics [
      x 1000.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "e__rough_space_endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa1687"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa1687"
      minerva_name "E"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/43740570"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=E"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P0DTC4"
      minerva_ref_type__resource1 "ENTREZ__43740570"
      minerva_ref_type__resource2 "HGNC_SYMBOL__E"
      minerva_ref_type__resource3 "UNIPROT__P0DTC4"
      minerva_type "Protein"
      minerva_x 4425.375000000001
      minerva_y 2929.083333333333
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca222"
      compartment_name "rough_space_endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa1687"
      species_meta_id "s_id_sa1687"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 131
    zlevel -1

    cluster [
      cluster "s_id_ca242"
    ]
    graphics [
      x 1100.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "pp1ab__endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa1790"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa1790"
      minerva_name "pp1ab"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.19.12"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.6.4.12"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/gene/43740578"
      minerva_ref_link12 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.1.13.-"
      minerva_ref_link2 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.22.69"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P0DTD1"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=rep"
      minerva_ref_link5 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.1.-.-"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.7.48"
      minerva_ref_link7 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.1.1.-"
      minerva_ref_link8 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.22.-"
      minerva_ref_link9 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.6.4.13"
      minerva_ref_type__resource1 "EC__3.4.19.12"
      minerva_ref_type__resource10 "EC__3.6.4.12"
      minerva_ref_type__resource11 "ENTREZ__43740578"
      minerva_ref_type__resource12 "EC__3.1.13.-"
      minerva_ref_type__resource2 "EC__3.4.22.69"
      minerva_ref_type__resource3 "UNIPROT__P0DTD1"
      minerva_ref_type__resource4 "HGNC_SYMBOL__rep"
      minerva_ref_type__resource5 "EC__3.1.-.-"
      minerva_ref_type__resource6 "EC__2.7.7.48"
      minerva_ref_type__resource7 "EC__2.1.1.-"
      minerva_ref_type__resource8 "EC__3.4.22.-"
      minerva_ref_type__resource9 "EC__3.6.4.13"
      minerva_type "Protein"
      minerva_x 2859.7916666666683
      minerva_y 609.9277777777784
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca242"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa1790"
      species_meta_id "s_id_sa1790"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 132
    zlevel -1

    cluster [
      cluster "s_id_ca242"
    ]
    graphics [
      x 1200.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "pp1a_space_nsp6_minus_11__endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2224"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2224"
      minerva_name "pp1a Nsp6-11"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.19.12"
      minerva_ref_link2 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.22.69"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P0DTC1"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.22.-"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/43740578"
      minerva_ref_type__resource1 "EC__3.4.19.12"
      minerva_ref_type__resource2 "EC__3.4.22.69"
      minerva_ref_type__resource3 "UNIPROT__P0DTC1"
      minerva_ref_type__resource4 "EC__3.4.22.-"
      minerva_ref_type__resource5 "ENTREZ__43740578"
      minerva_type "Protein"
      minerva_x 3291.5
      minerva_y 1031.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca242"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2224"
      species_meta_id "s_id_sa2224"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 133
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 200.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf7b_space_ds_space_sgmrna__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2352"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2352"
      minerva_name "Orf7b ds sgmRNA"
      minerva_type "RNA"
      minerva_x 2548.0
      minerva_y 2708.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2352"
      species_meta_id "s_id_sa2352"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 134
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 300.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "m_space_(_minus_)ss_space_sgmrna__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2327"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2327"
      minerva_name "M (-)ss sgmRNA"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/43740571"
      minerva_ref_type__resource1 "ENTREZ__43740571"
      minerva_type "RNA"
      minerva_x 1753.0555555555557
      minerva_y 2233.8888888888887
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2327"
      species_meta_id "s_id_sa2327"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 135
    zlevel -1

    cluster [
      cluster "s_id_ca242"
    ]
    graphics [
      x 400.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "pp1a__endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa1789"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa1789"
      minerva_name "pp1a"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.19.12"
      minerva_ref_link2 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.22.69"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P0DTC1"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.22.-"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/43740578"
      minerva_ref_type__resource1 "EC__3.4.19.12"
      minerva_ref_type__resource2 "EC__3.4.22.69"
      minerva_ref_type__resource3 "UNIPROT__P0DTC1"
      minerva_ref_type__resource4 "EC__3.4.22.-"
      minerva_ref_type__resource5 "ENTREZ__43740578"
      minerva_type "Protein"
      minerva_x 3038.7916666666683
      minerva_y 610.9277777777784
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca242"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa1789"
      species_meta_id "s_id_sa1789"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 136
    zlevel -1

    cluster [
      cluster "s_id_ca221"
    ]
    graphics [
      x 500.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf8__endoplasmic_space_reticulum_minus_golgi_space_intermediate_space_compartment__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2248"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2248"
      minerva_name "Orf8"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P0DTC8"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/43740577"
      minerva_ref_type__resource1 "UNIPROT__P0DTC8"
      minerva_ref_type__resource2 "ENTREZ__43740577"
      minerva_type "Protein"
      minerva_x 5092.2715311004795
      minerva_y 2645.3181818181815
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca221"
      compartment_name "endoplasmic_space_reticulum_minus_Golgi_space_intermediate_space_compartment"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2248"
      species_meta_id "s_id_sa2248"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 137
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 600.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf6_space_(_minus_)ss_space_sgmrna__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2323"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2323"
      minerva_name "Orf6 (-)ss sgmRNA"
      minerva_type "RNA"
      minerva_x 1753.0555555555557
      minerva_y 2393.8888888888887
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2323"
      species_meta_id "s_id_sa2323"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 138
    zlevel -1

    cluster [
      cluster "s_id_ca242"
    ]
    graphics [
      x 700.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp4__endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2228"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2228"
      minerva_name "Nsp4"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/YP_009725300"
      minerva_ref_type__resource1 "NCBI_PROTEIN__YP_009725300"
      minerva_type "Protein"
      minerva_x 3011.0
      minerva_y 1613.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca242"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2228"
      species_meta_id "s_id_sa2228"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 139
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 800.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf14_space_ds_space_sgmrna__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2351"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2351"
      minerva_name "Orf14 ds sgmRNA"
      minerva_type "RNA"
      minerva_x 2548.0
      minerva_y 2665.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2351"
      species_meta_id "s_id_sa2351"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 140
    zlevel -1

    cluster [
      cluster "default"
    ]
    graphics [
      x 900.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "camostat_space_mesylate__default__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa1544"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa1544"
      minerva_name "Camostat mesylate"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/2536"
      minerva_ref_type__resource1 "PUBCHEM__2536"
      minerva_type "Drug"
      minerva_x 3312.5
      minerva_y 121.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "default"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa1544"
      species_meta_id "s_id_sa1544"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 141
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 1000.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sa2294_underscore_degraded__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2331"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2331"
      minerva_name "sa2294_degraded"
      minerva_type "Degraded"
      minerva_x 3495.0
      minerva_y 2381.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2331"
      species_meta_id "s_id_sa2331"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 142
    zlevel -1

    cluster [
      cluster "s_id_ca242"
    ]
    graphics [
      x 1100.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp1__endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2356"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2356"
      minerva_name "Nsp1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/YP_009725297"
      minerva_ref_type__resource1 "NCBI_PROTEIN__YP_009725297"
      minerva_type "Protein"
      minerva_x 3166.5
      minerva_y 566.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca242"
      compartment_name "endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2356"
      species_meta_id "s_id_sa2356"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 143
    zlevel -1

    cluster [
      cluster "s_id_ca206"
    ]
    graphics [
      x 1200.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ctsl__endosome__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa1525"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa1525"
      minerva_former_symbols "CTSL1"
      minerva_name "CTSL"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.22.15"
      minerva_ref_link10 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/2537"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CTSL"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/1514"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CTSL"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/1514"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P07711"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P07711"
      minerva_ref_link8 "https://www.ensembl.org/id/ENSG00000135047"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_001912"
      minerva_ref_type__resource1 "EC__3.4.22.15"
      minerva_ref_type__resource10 "HGNC__2537"
      minerva_ref_type__resource2 "HGNC_SYMBOL__CTSL"
      minerva_ref_type__resource3 "ENTREZ__1514"
      minerva_ref_type__resource4 "HGNC_SYMBOL__CTSL"
      minerva_ref_type__resource5 "ENTREZ__1514"
      minerva_ref_type__resource6 "UNIPROT__P07711"
      minerva_ref_type__resource7 "UNIPROT__P07711"
      minerva_ref_type__resource8 "ENSEMBL__ENSG00000135047"
      minerva_ref_type__resource9 "REFSEQ__NM_001912"
      minerva_type "Protein"
      minerva_x 907.75
      minerva_y 1056.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca206"
      compartment_name "endosome"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa1525"
      species_meta_id "s_id_sa1525"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 144
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 200.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sa2301_underscore_degraded__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2339"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2339"
      minerva_name "sa2301_degraded"
      minerva_type "Degraded"
      minerva_x 3495.0
      minerva_y 2668.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2339"
      species_meta_id "s_id_sa2339"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 145
    zlevel -1

    cluster [
      cluster "s_id_ca222"
    ]
    graphics [
      x 300.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "host_space_translation_space_complex__rough_space_endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa428"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa428"
      minerva_fullName "translation initiation complex"
      minerva_name "Host translation complex"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0070992"
      minerva_ref_type__resource1 "GO__GO:0070992"
      minerva_type "Complex"
      minerva_x 4392.111111111111
      minerva_y 2796.7777777777774
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca222"
      compartment_name "rough_space_endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa428"
      species_meta_id "s_id_csa428"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 146
    zlevel -1

    cluster [
      cluster "s_id_ca222"
    ]
    graphics [
      x 400.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s__rough_space_endoplasmic_space_reticulum__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa1688"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa1688"
      minerva_name "S"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P0DTC2"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/43740568"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=S"
      minerva_ref_type__resource1 "UNIPROT__P0DTC2"
      minerva_ref_type__resource2 "ENTREZ__43740568"
      minerva_ref_type__resource3 "HGNC_SYMBOL__S"
      minerva_type "Protein"
      minerva_x 4420.041666666668
      minerva_y 2981.083333333333
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca222"
      compartment_name "rough_space_endoplasmic_space_reticulum"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa1688"
      species_meta_id "s_id_sa1688"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 147
    zlevel -1

    cluster [
      cluster "s_id_ca189"
    ]
    graphics [
      x 500.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf14_space_(_plus_)ss_space_sgmrna__epithelial_space_cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2110"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2110"
      minerva_name "Orf14 (+)ss sgmRNA"
      minerva_type "RNA"
      minerva_x 3229.9493251846206
      minerva_y 3700.3297258297252
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca189"
      compartment_name "epithelial_space_cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_id "s_id_sa2110"
      species_meta_id "s_id_sa2110"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 148
    zlevel -1

    cluster [
      cluster "s_id_ca206"
    ]
    graphics [
      x 600.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s1__endosome__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa1516"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa1516"
      minerva_name "S1"
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "Protein"
      minerva_x 864.0
      minerva_y 958.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca206"
      compartment_name "endosome"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa1516"
      species_meta_id "s_id_sa1516"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 149
    zlevel -1

    cluster [
      cluster "s_id_ca237"
    ]
    graphics [
      x 700.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf3a_space_(_minus_)ss_space_sgmrna__double_space_membrane_space_vesicle_space_viral_space_factory__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2298"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2298"
      minerva_name "Orf3a (-)ss sgmRNA"
      minerva_type "RNA"
      minerva_x 3210.0
      minerva_y 2547.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca237"
      compartment_name "double_space_membrane_space_vesicle_space_viral_space_factory"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa2298"
      species_meta_id "s_id_sa2298"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 150
    zlevel -1

    cluster [
      cluster "s_id_ca206"
    ]
    graphics [
      x 800.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf7a__endosome__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2114"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2114"
      minerva_name "Orf7a"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/43740573"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P0DTC7"
      minerva_ref_type__resource1 "ENTREZ__43740573"
      minerva_ref_type__resource2 "UNIPROT__P0DTC7"
      minerva_type "Protein"
      minerva_x 1783.4372469635628
      minerva_y 710.2744812753035
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca206"
      compartment_name "endosome"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2114"
      species_meta_id "s_id_sa2114"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 151
    zlevel -1

    graphics [
      x 900.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1070"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1070"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 3380.375
      minerva_y 2547.5
    ]
    sbml [
      reaction_id "re1070"
      reaction_meta_id "re1070"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 154
    zlevel -1

    graphics [
      x 1000.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1018"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1018"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 4820.927432216906
      minerva_y 2940.4965909090906
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1018"
      reaction_meta_id "re1018"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 157
    zlevel -1

    graphics [
      x 1100.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1010"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1010"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 4879.82079111549
      minerva_y 2501.917504370629
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1010"
      reaction_meta_id "re1010"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 160
    zlevel -1

    graphics [
      x 1200.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1002"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1002"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 2641.125
      minerva_y 1156.125
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1002"
      reaction_meta_id "re1002"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 163
    zlevel -1

    graphics [
      x 200.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1113"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 3040.5280594312426
      minerva_y 565.14112968718
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1113"
      reaction_meta_id "re1113"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 167
    zlevel -1

    graphics [
      x 300.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1073"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1073"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 3382.875
      minerva_y 2632.0
    ]
    sbml [
      reaction_id "re1073"
      reaction_meta_id "re1073"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 170
    zlevel -1

    graphics [
      x 400.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re855"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re855"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 2377.2244359381075
      minerva_y 424.523450153213
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re855"
      reaction_meta_id "re855"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 182
    zlevel -1

    graphics [
      x 500.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1089"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1089"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 2880.0
      minerva_y 2341.5
    ]
    sbml [
      reaction_id "re1089"
      reaction_meta_id "re1089"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 186
    zlevel -1

    graphics [
      x 600.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1016"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1016"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 4733.594098883574
      minerva_y 2896.9965909090906
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1016"
      reaction_meta_id "re1016"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 189
    zlevel -1

    graphics [
      x 700.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1077"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1077"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 2155.840277777778
      minerva_y 2283.8194444444443
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1077"
      reaction_meta_id "re1077"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 193
    zlevel -1

    graphics [
      x 800.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re989"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re989"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 3269.0
      minerva_y 946.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re989"
      reaction_meta_id "re989"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 199
    zlevel -1

    graphics [
      x 900.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1098"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1098"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 2881.0
      minerva_y 2706.5
    ]
    sbml [
      reaction_id "re1098"
      reaction_meta_id "re1098"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 203
    zlevel -1

    graphics [
      x 1000.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1020"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1020"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 4911.594098883574
      minerva_y 2993.9965909090906
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1020"
      reaction_meta_id "re1020"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 206
    zlevel -1

    graphics [
      x 1100.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1012"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1012"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 4539.170052653947
      minerva_y 2791.700757575749
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1012"
      reaction_meta_id "re1012"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 209
    zlevel -1

    graphics [
      x 1200.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1085"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1085"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 2150.527777777778
      minerva_y 2608.4444444444443
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1085"
      reaction_meta_id "re1085"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 213
    zlevel -1

    graphics [
      x 200.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1093"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1093"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 2879.0
      minerva_y 2506.0
    ]
    sbml [
      reaction_id "re1093"
      reaction_meta_id "re1093"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 217
    zlevel -1

    graphics [
      x 300.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1019"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1019"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 4868.552432216906
      minerva_y 2969.1215909090906
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1019"
      reaction_meta_id "re1019"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 220
    zlevel -1

    graphics [
      x 400.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1004"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1004"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 2716.5
      minerva_y 815.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1004"
      reaction_meta_id "re1004"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 223
    zlevel -1

    graphics [
      x 500.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re979"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re979"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 1662.8333333333333
      minerva_y 905.3541666666669
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re979"
      reaction_meta_id "re979"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 233
    zlevel -1

    graphics [
      x 600.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re894"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re894"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 5139.0625
      minerva_y 614.0600961538461
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re894"
      reaction_meta_id "re894"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 236
    zlevel -1

    graphics [
      x 700.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1097"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1097"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 2879.0
      minerva_y 2667.0
    ]
    sbml [
      reaction_id "re1097"
      reaction_meta_id "re1097"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 240
    zlevel -1

    graphics [
      x 800.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1118"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1118"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 3379.875
      minerva_y 2310.0
    ]
    sbml [
      reaction_id "re1118"
      reaction_meta_id "re1118"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 243
    zlevel -1

    graphics [
      x 900.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1080"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1080"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 2150.527777777778
      minerva_y 2407.9444444444443
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1080"
      reaction_meta_id "re1080"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 247
    zlevel -1

    graphics [
      x 1000.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1105"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "Negative influence"
      minerva_x 3236.0
      minerva_y 566.25
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1105"
      reaction_meta_id "re1105"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 250
    zlevel -1

    graphics [
      x 1100.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1111"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "Negative influence"
      minerva_x 2147.055555555556
      minerva_y 1500.2123799081035
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1111"
      reaction_meta_id "re1111"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 253
    zlevel -1

    graphics [
      x 1200.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1123"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1123"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 1525.991734308289
      minerva_y 2276.9905175655963
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1123"
      reaction_meta_id "re1123"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 267
    zlevel -1

    graphics [
      x 200.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1095"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1095"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 2880.0
      minerva_y 2585.5
    ]
    sbml [
      reaction_id "re1095"
      reaction_meta_id "re1095"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 271
    zlevel -1

    graphics [
      x 300.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1110"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 2435.722222222223
      minerva_y 1303.9623799081035
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1110"
      reaction_meta_id "re1110"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 284
    zlevel -1

    graphics [
      x 400.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1011"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1011"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 4496.796568627453
      minerva_y 2767.200757575757
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1011"
      reaction_meta_id "re1011"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 287
    zlevel -1

    graphics [
      x 500.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1083"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1083"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 2150.527777777778
      minerva_y 2527.4444444444443
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1083"
      reaction_meta_id "re1083"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 291
    zlevel -1

    graphics [
      x 600.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1079"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1079"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 2150.527777777778
      minerva_y 2365.4444444444443
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1079"
      reaction_meta_id "re1079"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 295
    zlevel -1

    graphics [
      x 700.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re838"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re838"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 377.27754620352493
      minerva_y 409.87822935287454
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re838"
      reaction_meta_id "re838"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 301
    zlevel -1

    graphics [
      x 800.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re950"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re950"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 3198.725961538461
      minerva_y 419.6298076923076
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re950"
      reaction_meta_id "re950"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 310
    zlevel -1

    graphics [
      x 900.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1061"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1061"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 3744.3611111111113
      minerva_y 2701.25
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1061"
      reaction_meta_id "re1061"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 313
    zlevel -1

    graphics [
      x 1000.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1064"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1064"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 3381.375
      minerva_y 2341.0
    ]
    sbml [
      reaction_id "re1064"
      reaction_meta_id "re1064"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 316
    zlevel -1

    graphics [
      x 1100.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1031"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1031"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 3825.120495925644
      minerva_y 3526.3912518037514
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1031"
      reaction_meta_id "re1031"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 320
    zlevel -1

    graphics [
      x 1200.0
      y 1700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re981"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re981"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 1415.2980769230778
      minerva_y 1095.3846153846137
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re981"
      reaction_meta_id "re981"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 323
    zlevel -1

    graphics [
      x 200.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1000"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1000"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "Negative influence"
      minerva_x 2665.325
      minerva_y 644.2970588235294
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1000"
      reaction_meta_id "re1000"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 326
    zlevel -1

    graphics [
      x 300.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1109"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 3332.6764705882324
      minerva_y 1361.3659226190425
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1109"
      reaction_meta_id "re1109"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 335
    zlevel -1

    graphics [
      x 400.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re999"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re999"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 3098.970464135021
      minerva_y 1706.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re999"
      reaction_meta_id "re999"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 342
    zlevel -1

    graphics [
      x 500.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1107"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 3011.0
      minerva_y 1435.75
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1107"
      reaction_meta_id "re1107"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 347
    zlevel -1

    graphics [
      x 600.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1067"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1067"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 3383.875
      minerva_y 2422.0
    ]
    sbml [
      reaction_id "re1067"
      reaction_meta_id "re1067"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 350
    zlevel -1

    graphics [
      x 700.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1100"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 2878.5
      minerva_y 2309.0
    ]
    sbml [
      reaction_id "re1100"
      reaction_meta_id "re1100"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 354
    zlevel -1

    graphics [
      x 800.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1068"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1068"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 3381.375
      minerva_y 2469.5
    ]
    sbml [
      reaction_id "re1068"
      reaction_meta_id "re1068"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 357
    zlevel -1

    graphics [
      x 900.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1025"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1025"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 3882.328829258977
      minerva_y 3277.8912518037514
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1025"
      reaction_meta_id "re1025"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 361
    zlevel -1

    graphics [
      x 1000.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re843"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re843"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 863.9058257918554
      minerva_y 1019.3382352941177
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re843"
      reaction_meta_id "re843"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 367
    zlevel -1

    graphics [
      x 1100.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1027"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1027"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 3870.807995925644
      minerva_y 3359.1412518037514
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1027"
      reaction_meta_id "re1027"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 371
    zlevel -1

    graphics [
      x 1200.0
      y 1800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re852"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re852"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 3741.4521071034205
      minerva_y 405.1583059210525
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re852"
      reaction_meta_id "re852"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 377
    zlevel -1

    graphics [
      x 200.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1108"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 2919.5000000000005
      minerva_y 1402.875
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1108"
      reaction_meta_id "re1108"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 382
    zlevel -1

    graphics [
      x 300.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1006"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1006"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 2645.25
      minerva_y 948.25
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1006"
      reaction_meta_id "re1006"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 388
    zlevel -1

    graphics [
      x 400.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re972"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re972"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 4225.75
      minerva_y 2262.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re972"
      reaction_meta_id "re972"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 393
    zlevel -1

    graphics [
      x 500.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1086"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1086"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 2150.527777777778
      minerva_y 2647.9444444444443
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1086"
      reaction_meta_id "re1086"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 397
    zlevel -1

    graphics [
      x 600.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1082"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1082"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 2150.527777777778
      minerva_y 2490.4444444444443
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1082"
      reaction_meta_id "re1082"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 401
    zlevel -1

    graphics [
      x 700.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1009"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1009"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 4819.891119909504
      minerva_y 2466.042504370629
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1009"
      reaction_meta_id "re1009"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 404
    zlevel -1

    graphics [
      x 800.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re841"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re841"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 762.9849398040878
      minerva_y 916.1146938836581
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re841"
      reaction_meta_id "re841"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 408
    zlevel -1

    graphics [
      x 900.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1112"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 2410.281119465331
      minerva_y 1767.8909513366748
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1112"
      reaction_meta_id "re1112"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 429
    zlevel -1

    graphics [
      x 1000.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1015"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1015"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 4686.927432216906
      minerva_y 2872.4965909090906
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1015"
      reaction_meta_id "re1015"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 432
    zlevel -1

    graphics [
      x 1100.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1106"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 3284.0
      minerva_y 1217.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1106"
      reaction_meta_id "re1106"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 435
    zlevel -1

    graphics [
      x 1200.0
      y 1900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re932"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re932"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 3766.2506403561147
      minerva_y 3000.0416666666665
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re932"
      reaction_meta_id "re932"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 438
    zlevel -1

    graphics [
      x 200.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1092"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1092"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 2874.6875
      minerva_y 2469.125
    ]
    sbml [
      reaction_id "re1092"
      reaction_meta_id "re1092"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 442
    zlevel -1

    graphics [
      x 300.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1091"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1091"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 2882.5
      minerva_y 2420.5
    ]
    sbml [
      reaction_id "re1091"
      reaction_meta_id "re1091"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 446
    zlevel -1

    graphics [
      x 400.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1028"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1028"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 3859.807995925644
      minerva_y 3400.5023629148623
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1028"
      reaction_meta_id "re1028"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 450
    zlevel -1

    graphics [
      x 500.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1023"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1023"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 3905.037162592311
      minerva_y 3184.366883116883
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1023"
      reaction_meta_id "re1023"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 454
    zlevel -1

    graphics [
      x 600.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1078"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1078"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 2155.840277777778
      minerva_y 2322.8194444444443
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1078"
      reaction_meta_id "re1078"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 458
    zlevel -1

    graphics [
      x 700.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1081"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1081"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 2155.840277777778
      minerva_y 2446.3194444444443
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1081"
      reaction_meta_id "re1081"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 462
    zlevel -1

    graphics [
      x 800.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1075"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1075"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 3382.375
      minerva_y 2705.5
    ]
    sbml [
      reaction_id "re1075"
      reaction_meta_id "re1075"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 465
    zlevel -1

    graphics [
      x 900.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1066"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1066"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 3379.875
      minerva_y 2381.0
    ]
    sbml [
      reaction_id "re1066"
      reaction_meta_id "re1066"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 468
    zlevel -1

    graphics [
      x 1000.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re859"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re859"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 1897.271523928046
      minerva_y 1330.0833333333335
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re859"
      reaction_meta_id "re859"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 472
    zlevel -1

    graphics [
      x 1100.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1001"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1001"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 3223.166666666667
      minerva_y 806.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1001"
      reaction_meta_id "re1001"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 475
    zlevel -1

    graphics [
      x 1200.0
      y 2000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re940"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re940"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 3577.903203451107
      minerva_y 3212.5710856119836
    ]
    sbml [
      reaction_id "re940"
      reaction_meta_id "re940"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 479
    zlevel -1

    graphics [
      x 200.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1102"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 2857.7500000000036
      minerva_y 755.5000000000002
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1102"
      reaction_meta_id "re1102"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 485
    zlevel -1

    graphics [
      x 300.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1076"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1076"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 2325.2053243803484
      minerva_y 2210.741115222465
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1076"
      reaction_meta_id "re1076"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 489
    zlevel -1

    graphics [
      x 400.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1021"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1021"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 5006.02173046252
      minerva_y 2519.3169856459326
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1021"
      reaction_meta_id "re1021"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 492
    zlevel -1

    graphics [
      x 500.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1013"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1013"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 4591.629901960787
      minerva_y 2824.200757575757
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1013"
      reaction_meta_id "re1013"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 495
    zlevel -1

    graphics [
      x 600.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1104"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 3041.2499999999973
      minerva_y 751.5000000000002
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1104"
      reaction_meta_id "re1104"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 501
    zlevel -1

    graphics [
      x 700.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1069"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1069"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 3380.375
      minerva_y 2508.0
    ]
    sbml [
      reaction_id "re1069"
      reaction_meta_id "re1069"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 504
    zlevel -1

    graphics [
      x 800.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1120"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 3130.107142857143
      minerva_y 2114.9285714285716
    ]
    sbml [
      reaction_id "re1120"
      reaction_meta_id "re1120"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 507
    zlevel -1

    graphics [
      x 900.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1024"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1024"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 3895.3704959256443
      minerva_y 3229.866883116883
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1024"
      reaction_meta_id "re1024"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 511
    zlevel -1

    graphics [
      x 1000.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re895"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re895"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 5170.427884615385
      minerva_y 1648.3617788461538
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re895"
      reaction_meta_id "re895"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 514
    zlevel -1

    graphics [
      x 1100.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1017"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1017"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 4777.094098883574
      minerva_y 2920.9965909090906
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1017"
      reaction_meta_id "re1017"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 517
    zlevel -1

    graphics [
      x 1200.0
      y 2100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1088"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1088"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 2914.25
      minerva_y 2254.0
    ]
    sbml [
      reaction_id "re1088"
      reaction_meta_id "re1088"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 521
    zlevel -1

    graphics [
      x 200.0
      y 2200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1057"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1057"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 3419.375
      minerva_y 2256.5
    ]
    sbml [
      reaction_id "re1057"
      reaction_meta_id "re1057"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 524
    zlevel -1

    graphics [
      x 300.0
      y 2200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1072"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1072"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 3381.375
      minerva_y 2588.0
    ]
    sbml [
      reaction_id "re1072"
      reaction_meta_id "re1072"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 527
    zlevel -1

    graphics [
      x 400.0
      y 2200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1094"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1094"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 2879.0
      minerva_y 2546.0
    ]
    sbml [
      reaction_id "re1094"
      reaction_meta_id "re1094"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 531
    zlevel -1

    graphics [
      x 500.0
      y 2200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1124"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1124"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 2631.3442622950815
      minerva_y 113.21311475409931
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1124"
      reaction_meta_id "re1124"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 535
    zlevel -1

    graphics [
      x 600.0
      y 2200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1029"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1029"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 3853.2871625923103
      minerva_y 3440.2523629148623
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1029"
      reaction_meta_id "re1029"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 539
    zlevel -1

    graphics [
      x 700.0
      y 2200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1014"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1014"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 4643.26076555024
      minerva_y 2847.9965909090906
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1014"
      reaction_meta_id "re1014"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 542
    zlevel -1

    graphics [
      x 800.0
      y 2200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1026"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1026"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 3877.9746625923103
      minerva_y 3317.1412518037514
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1026"
      reaction_meta_id "re1026"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 546
    zlevel -1

    graphics [
      x 900.0
      y 2200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1084"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1084"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 2150.527777777778
      minerva_y 2566.9444444444443
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1084"
      reaction_meta_id "re1084"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 550
    zlevel -1

    graphics [
      x 1000.0
      y 2200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1008"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1008"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 4768.55778657617
      minerva_y 2434.542504370629
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1008"
      reaction_meta_id "re1008"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 553
    zlevel -1

    graphics [
      x 1100.0
      y 2200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1096"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1096"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 2876.1875
      minerva_y 2631.625
    ]
    sbml [
      reaction_id "re1096"
      reaction_meta_id "re1096"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 557
    zlevel -1

    graphics [
      x 1200.0
      y 2200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1115"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 2074.3332529824115
      minerva_y 1152.9499356565402
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1115"
      reaction_meta_id "re1115"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 561
    zlevel -1

    graphics [
      x 200.0
      y 2300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re948"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re948"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 4859.0596469358425
      minerva_y 2160.4408720408974
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re948"
      reaction_meta_id "re948"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 564
    zlevel -1

    graphics [
      x 300.0
      y 2300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1090"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1090"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 2878.5
      minerva_y 2380.0
    ]
    sbml [
      reaction_id "re1090"
      reaction_meta_id "re1090"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 568
    zlevel -1

    graphics [
      x 400.0
      y 2300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1099"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1099"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 2155.840277777778
      minerva_y 2248.375
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1099"
      reaction_meta_id "re1099"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 572
    zlevel -1

    graphics [
      x 500.0
      y 2300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re952"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re952"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 2728.4192649050306
      minerva_y 523.280743659102
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re952"
      reaction_meta_id "re952"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 575
    zlevel -1

    graphics [
      x 600.0
      y 2300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1022"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1022"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 3922.0163292589773
      minerva_y 3137.616883116883
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1022"
      reaction_meta_id "re1022"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 579
    zlevel -1

    graphics [
      x 700.0
      y 2300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1119"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 2912.4120879120883
      minerva_y 1939.5
    ]
    sbml [
      reaction_id "re1119"
      reaction_meta_id "re1119"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 582
    zlevel -1

    graphics [
      x 800.0
      y 2300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re908"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re908"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 1630.9090803494164
      minerva_y 1274.1905079971261
    ]
    sbml [
      reaction_id "re908"
      reaction_meta_id "re908"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 585
    zlevel -1

    graphics [
      x 900.0
      y 2300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1063"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1063"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 1857.9027777777778
      minerva_y 1928.1319444444443
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1063"
      reaction_meta_id "re1063"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 589
    zlevel -1

    graphics [
      x 1000.0
      y 2300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1030"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1030"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 3839.28716259231
      minerva_y 3486.6273629148627
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1030"
      reaction_meta_id "re1030"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 593
    zlevel -1

    graphics [
      x 1100.0
      y 2300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1074"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1074"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002551"
      minerva_ref_type__resource1 "INTERPRO__IPR002551"
      minerva_type "State transition"
      minerva_x 3380.375
      minerva_y 2668.5
    ]
    sbml [
      reaction_id "re1074"
      reaction_meta_id "re1074"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  edge [
    id 596
    source 149
    target 151
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2298"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 597
    source 151
    target 12
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2335"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 598
    source 73
    target 154
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa1878"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 599
    source 154
    target 69
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2249"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 600
    source 40
    target 157
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa1893"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 601
    source 157
    target 29
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2173"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 602
    source 67
    target 160
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2359"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 603
    source 160
    target 62
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2365"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 604
    source 45
    target 163
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa1675"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 605
    source 163
    target 135
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa1789"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 606
    source 56
    target 163
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR0"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa427"
    ]
  ]
  edge [
    id 607
    source 21
    target 167
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2300"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 608
    source 167
    target 52
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2338"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 609
    source 3
    target 170
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa366"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 610
    source 7
    target 170
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2003"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 611
    source 8
    target 170
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa1857"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 612
    source 28
    target 170
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2060"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 613
    source 170
    target 48
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa369"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 615
    source 170
    target 127
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2066"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 616
    source 170
    target 72
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2067"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 617
    source 10
    target 170
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR1"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa1601"
    ]
  ]
  edge [
    id 618
    source 120
    target 170
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR2"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa2372"
    ]
  ]
  edge [
    id 619
    source 68
    target 170
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR3"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa441"
    ]
  ]
  edge [
    id 620
    source 5
    target 182
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2343"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 621
    source 182
    target 88
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2292"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 622
    source 182
    target 80
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2113"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 623
    source 15
    target 186
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa1873"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 624
    source 186
    target 101
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2247"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 625
    source 134
    target 189
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2327"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 626
    source 189
    target 5
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2343"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 627
    source 86
    target 189
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR4"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa440"
    ]
  ]
  edge [
    id 628
    source 97
    target 193
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2240"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 629
    source 193
    target 132
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2224"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 630
    source 193
    target 67
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2359"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 631
    source 193
    target 43
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2241"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 632
    source 133
    target 199
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2352"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 633
    source 199
    target 42
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2302"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 634
    source 199
    target 103
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2106"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 635
    source 74
    target 203
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa1876"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 636
    source 203
    target 81
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2251"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 637
    source 130
    target 206
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa1687"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 638
    source 206
    target 93
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa1892"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 639
    source 116
    target 209
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2319"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 640
    source 209
    target 139
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2351"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 641
    source 86
    target 209
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR6"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa440"
    ]
  ]
  edge [
    id 642
    source 85
    target 213
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2347"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 643
    source 213
    target 64
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2297"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 644
    source 213
    target 91
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2108"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 645
    source 35
    target 217
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa1877"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 646
    source 217
    target 26
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2250"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 647
    source 115
    target 220
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "2.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2221"
      stoichiometry "2"
    ]
  ]
  edge [
    id 648
    source 220
    target 107
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2244"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 649
    source 3
    target 223
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa366"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 650
    source 7
    target 223
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2003"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 651
    source 28
    target 223
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2060"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 652
    source 8
    target 223
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa1857"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 653
    source 223
    target 48
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa369"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 654
    source 223
    target 150
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2114"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 655
    source 223
    target 98
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2115"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 656
    source 223
    target 108
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2116"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 657
    source 10
    target 223
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR7"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa1601"
    ]
  ]
  edge [
    id 658
    source 3
    target 233
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa366"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 660
    source 139
    target 236
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2351"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 661
    source 236
    target 49
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2301"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 662
    source 236
    target 147
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2110"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 663
    source 4
    target 240
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2355"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 664
    source 240
    target 87
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2368"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 665
    source 14
    target 243
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2324"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 666
    source 243
    target 118
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2346"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 667
    source 86
    target 243
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR8"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa440"
    ]
  ]
  edge [
    id 668
    source 142
    target 247
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2356"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 669
    source 247
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2358"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 670
    source 129
    target 250
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2205"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 671
    source 250
    target 60
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2291"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 672
    source 83
    target 253
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2328"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 673
    source 253
    target 27
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2318"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 674
    source 253
    target 65
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2353"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 675
    source 253
    target 134
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2327"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 676
    source 253
    target 47
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2326"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 677
    source 253
    target 128
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2325"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 678
    source 253
    target 14
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2324"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 679
    source 253
    target 137
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2323"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 680
    source 253
    target 102
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2322"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 681
    source 253
    target 25
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2321"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 682
    source 253
    target 11
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2320"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 683
    source 253
    target 116
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2319"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 684
    source 86
    target 253
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR9"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa440"
    ]
  ]
  edge [
    id 685
    source 82
    target 267
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2349"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 686
    source 267
    target 36
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2299"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 687
    source 267
    target 92
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2107"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 688
    source 100
    target 271
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2229"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 689
    source 271
    target 114
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2198"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 690
    source 271
    target 18
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2197"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 691
    source 271
    target 51
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2200"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 692
    source 271
    target 16
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2361"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 693
    source 271
    target 31
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2202"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 694
    source 271
    target 110
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2203"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 695
    source 271
    target 1
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2204"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 696
    source 271
    target 6
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2199"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 697
    source 271
    target 129
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2205"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 698
    source 271
    target 57
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2206"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 699
    source 43
    target 271
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR10"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa2241"
    ]
  ]
  edge [
    id 700
    source 46
    target 284
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa1686"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 701
    source 284
    target 121
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa1891"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 702
    source 25
    target 287
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2321"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 703
    source 287
    target 82
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2349"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 704
    source 86
    target 287
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR11"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa440"
    ]
  ]
  edge [
    id 705
    source 128
    target 291
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2325"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 706
    source 291
    target 96
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2345"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 707
    source 86
    target 291
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR12"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa440"
    ]
  ]
  edge [
    id 708
    source 95
    target 295
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa1462"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 709
    source 29
    target 295
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2173"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 710
    source 295
    target 76
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa430"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 711
    source 24
    target 295
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR13"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa2369"
    ]
  ]
  edge [
    id 712
    source 76
    target 301
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa430"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 713
    source 301
    target 75
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2188"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 714
    source 301
    target 112
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2239"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 715
    source 301
    target 104
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa1539"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 716
    source 301
    target 113
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2238"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 717
    source 50
    target 301
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR14"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa1921"
    ]
  ]
  edge [
    id 718
    source 32
    target 301
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR15"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa1537"
    ]
  ]
  edge [
    id 719
    source 140
    target 301
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR16"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa1544"
    ]
  ]
  edge [
    id 720
    source 90
    target 310
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa1887"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 721
    source 310
    target 44
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa398"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 722
    source 88
    target 313
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2292"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 723
    source 313
    target 54
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2329"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 724
    source 103
    target 316
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2106"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 725
    source 316
    target 74
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa1876"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 726
    source 145
    target 316
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR17"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa428"
    ]
  ]
  edge [
    id 727
    source 75
    target 320
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2188"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 728
    source 320
    target 10
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa1601"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 729
    source 30
    target 323
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2216"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 730
    source 323
    target 77
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2237"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 731
    source 132
    target 326
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2224"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 732
    source 326
    target 1
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2204"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 733
    source 326
    target 110
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2203"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 734
    source 326
    target 117
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2362"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 735
    source 326
    target 16
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2361"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 736
    source 326
    target 51
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2200"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 737
    source 326
    target 39
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2364"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 738
    source 43
    target 326
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR18"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa2241"
    ]
  ]
  edge [
    id 739
    source 66
    target 335
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa439"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 740
    source 335
    target 99
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa438"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 741
    source 138
    target 335
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR19"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa2228"
    ]
  ]
  edge [
    id 742
    source 111
    target 335
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR20"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa2222"
    ]
  ]
  edge [
    id 743
    source 1
    target 335
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR21"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa2204"
    ]
  ]
  edge [
    id 744
    source 62
    target 342
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2365"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 745
    source 342
    target 138
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2228"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 746
    source 342
    target 111
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2222"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 747
    source 84
    target 347
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2295"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 748
    source 347
    target 63
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2332"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 749
    source 124
    target 350
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2354"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 750
    source 350
    target 4
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2355"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 751
    source 350
    target 22
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa1962"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 752
    source 125
    target 354
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2296"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 753
    source 354
    target 94
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2333"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 754
    source 123
    target 357
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2105"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 755
    source 357
    target 9
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa1875"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 756
    source 145
    target 357
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR24"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa428"
    ]
  ]
  edge [
    id 757
    source 76
    target 361
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa430"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 758
    source 361
    target 75
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2188"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 759
    source 361
    target 148
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa1516"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 760
    source 34
    target 361
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR25"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa1524"
    ]
  ]
  edge [
    id 761
    source 143
    target 361
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR26"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa1525"
    ]
  ]
  edge [
    id 762
    source 122
    target 367
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2104"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 763
    source 367
    target 15
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa1873"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 764
    source 145
    target 367
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR27"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa428"
    ]
  ]
  edge [
    id 765
    source 29
    target 371
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2173"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 766
    source 95
    target 371
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa1462"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 767
    source 371
    target 76
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa430"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 768
    source 24
    target 371
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR28"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa2369"
    ]
  ]
  edge [
    id 769
    source 62
    target 377
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2365"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 770
    source 377
    target 111
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2222"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 771
    source 377
    target 138
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2228"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 772
    source 107
    target 382
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2244"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 773
    source 382
    target 100
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2229"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 774
    source 382
    target 43
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2241"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 775
    source 382
    target 67
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2359"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 776
    source 44
    target 388
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa398"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 777
    source 58
    target 388
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2153"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 778
    source 388
    target 78
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa397"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 779
    source 388
    target 86
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa440"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 780
    source 27
    target 393
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2318"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 781
    source 393
    target 133
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2352"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 782
    source 86
    target 393
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR31"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa440"
    ]
  ]
  edge [
    id 783
    source 102
    target 397
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2322"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 784
    source 397
    target 19
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2348"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 785
    source 86
    target 397
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR32"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa440"
    ]
  ]
  edge [
    id 786
    source 93
    target 401
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa1892"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 787
    source 401
    target 7
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2003"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 788
    source 3
    target 404
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa366"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 790
    source 76
    target 404
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR33"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa430"
    ]
  ]
  edge [
    id 791
    source 51
    target 408
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2200"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 792
    source 6
    target 408
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2199"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 793
    source 129
    target 408
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2205"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 794
    source 57
    target 408
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2206"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 795
    source 114
    target 408
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2198"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 796
    source 18
    target 408
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2197"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 797
    source 16
    target 408
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2361"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 798
    source 31
    target 408
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2202"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 799
    source 110
    target 408
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2203"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 800
    source 1
    target 408
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2204"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 801
    source 111
    target 408
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2222"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 802
    source 138
    target 408
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2228"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 803
    source 117
    target 408
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2362"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 804
    source 20
    target 408
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2217"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 805
    source 408
    target 86
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa440"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 806
    source 53
    target 429
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa1874"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 807
    source 429
    target 33
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2246"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 808
    source 67
    target 432
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2359"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 809
    source 432
    target 62
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2365"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 810
    source 79
    target 435
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa1667"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 811
    source 435
    target 90
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa1887"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 812
    source 118
    target 438
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2346"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 813
    source 438
    target 125
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2296"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 814
    source 438
    target 123
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2105"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 815
    source 96
    target 442
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2345"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 816
    source 442
    target 84
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2295"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 817
    source 442
    target 55
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2112"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 818
    source 92
    target 446
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2107"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 819
    source 446
    target 106
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa1879"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 820
    source 145
    target 446
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR34"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa428"
    ]
  ]
  edge [
    id 821
    source 70
    target 450
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2111"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 822
    source 450
    target 130
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa1687"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 823
    source 145
    target 450
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR35"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa428"
    ]
  ]
  edge [
    id 824
    source 47
    target 454
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2326"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 825
    source 454
    target 61
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2344"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 826
    source 86
    target 454
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR36"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa440"
    ]
  ]
  edge [
    id 827
    source 137
    target 458
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2323"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 828
    source 458
    target 85
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2347"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 829
    source 86
    target 458
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR37"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa440"
    ]
  ]
  edge [
    id 830
    source 42
    target 462
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2302"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 831
    source 462
    target 23
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2340"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 832
    source 126
    target 465
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2294"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 833
    source 465
    target 141
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2331"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 834
    source 48
    target 468
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa369"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 835
    source 468
    target 45
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa1675"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 836
    source 468
    target 79
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa1667"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 837
    source 37
    target 472
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "2.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2220"
      stoichiometry "2"
    ]
  ]
  edge [
    id 838
    source 472
    target 97
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2240"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 839
    source 22
    target 475
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa1962"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 840
    source 475
    target 79
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa1667"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 841
    source 109
    target 475
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR38"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa429"
    ]
  ]
  edge [
    id 842
    source 131
    target 479
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa1790"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 843
    source 479
    target 115
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2221"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 844
    source 479
    target 20
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2217"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 845
    source 479
    target 30
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2216"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 846
    source 83
    target 485
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2328"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 847
    source 485
    target 119
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2342"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 848
    source 86
    target 485
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR40"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa440"
    ]
  ]
  edge [
    id 849
    source 59
    target 489
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2245"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 850
    source 489
    target 28
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2060"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 851
    source 146
    target 492
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa1688"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 852
    source 492
    target 40
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa1893"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 853
    source 135
    target 495
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa1789"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 854
    source 495
    target 37
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2220"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 855
    source 495
    target 20
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2217"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 856
    source 495
    target 142
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2356"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 857
    source 64
    target 501
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2297"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 858
    source 501
    target 41
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2334"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 859
    source 86
    target 504
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa440"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 860
    source 504
    target 44
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa398"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 861
    source 55
    target 507
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2112"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 862
    source 507
    target 146
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa1688"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 863
    source 145
    target 507
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR42"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa428"
    ]
  ]
  edge [
    id 864
    source 3
    target 511
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa366"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 866
    source 106
    target 514
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa1879"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 867
    source 514
    target 136
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2248"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 868
    source 119
    target 517
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2342"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 869
    source 517
    target 83
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2328"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 870
    source 517
    target 58
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2153"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 871
    source 83
    target 521
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2328"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 872
    source 521
    target 71
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2315"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 873
    source 36
    target 524
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2299"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 874
    source 524
    target 105
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2337"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 875
    source 19
    target 527
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2348"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 876
    source 527
    target 149
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2298"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 877
    source 527
    target 122
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2104"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 878
    source 104
    target 531
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa1539"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 879
    source 13
    target 531
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2371"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 880
    source 531
    target 68
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa441"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 881
    source 38
    target 535
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2109"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 882
    source 535
    target 73
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa1878"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 883
    source 145
    target 535
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR43"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa428"
    ]
  ]
  edge [
    id 884
    source 9
    target 539
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa1875"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 885
    source 539
    target 59
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2245"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 886
    source 91
    target 542
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2108"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 887
    source 542
    target 53
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa1874"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 888
    source 145
    target 542
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR44"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa428"
    ]
  ]
  edge [
    id 889
    source 11
    target 546
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2320"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 890
    source 546
    target 89
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2350"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 891
    source 86
    target 546
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR45"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa440"
    ]
  ]
  edge [
    id 892
    source 121
    target 550
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa1891"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 893
    source 550
    target 8
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa1857"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 894
    source 89
    target 553
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2350"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 895
    source 553
    target 21
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2300"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 896
    source 553
    target 38
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2109"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 897
    source 45
    target 557
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa1675"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 898
    source 557
    target 131
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa1790"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 899
    source 56
    target 557
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR46"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa427"
    ]
  ]
  edge [
    id 900
    source 78
    target 561
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa397"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 901
    source 561
    target 3
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa366"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 902
    source 61
    target 564
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2344"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 903
    source 564
    target 126
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2294"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 904
    source 564
    target 70
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2111"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 905
    source 65
    target 568
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2353"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 906
    source 568
    target 124
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2354"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 907
    source 86
    target 568
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR47"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa440"
    ]
  ]
  edge [
    id 908
    source 75
    target 572
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2188"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 909
    source 572
    target 10
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa1601"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 910
    source 80
    target 575
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2113"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 911
    source 575
    target 46
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa1686"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 912
    source 145
    target 575
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR48"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa428"
    ]
  ]
  edge [
    id 913
    source 86
    target 579
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa440"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 915
    source 79
    target 582
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa1667"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 916
    source 582
    target 17
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa1920"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 917
    source 45
    target 585
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa1675"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 918
    source 585
    target 83
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2328"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 919
    source 86
    target 585
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR49"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa440"
    ]
  ]
  edge [
    id 920
    source 147
    target 589
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2110"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 921
    source 589
    target 35
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa1877"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 922
    source 145
    target 589
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR50"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa428"
    ]
  ]
  edge [
    id 923
    source 49
    target 593
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2301"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 924
    source 593
    target 144
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa2339"
      stoichiometry "1.0"
    ]
  ]
]
