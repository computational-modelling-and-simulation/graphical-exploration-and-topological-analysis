# generated with VANTED V2.8.0 at Tue Apr 27 20:00:50 CEST 2021
graph [
  background_coloring "true"
  cluster_colors "255,128,128,255:0,0,0,255;191,255,128,255:0,0,0,255;128,255,255,255:0,0,0,255;191,128,255,255:0,0,0,255"
  clusterbackground_fill_outer_region "false"
  clusterbackground_grid 50.0
  clusterbackground_low_alpha 0.2
  clusterbackground_radius 200.0
  clusterbackground_space_fill "true"
  graphbackgroundcolor "#ffffff"
  sbml [
    model_meta_id "PRR"
    model_non_rdf_annotation ""
    model_notes ""
    model_sbml_id "PRR"
    namespace "xmlns:celldesigner=\"http://www.sbml.org/2001/ns/celldesigner\" "
    sbml_level "2"
    sbml_version "4"
  ]
  sbml_compartment_default [
    sbml_compartment_default_id "default"
    sbml_compartment_default_meta_id "default"
    sbml_compartment_default_non_rdf_annotation ""
    sbml_compartment_default_notes ""
    sbml_compartment_default_size "1.0"
    sbml_compartment_default_units "volume"
  ]
  sbml_compartment_s_id_ca1 [
    sbml_compartment_s_id_ca1_annotation ""
    sbml_compartment_s_id_ca1_id "s_id_ca1"
    sbml_compartment_s_id_ca1_meta_id "s_id_ca1"
    sbml_compartment_s_id_ca1_name "cell"
    sbml_compartment_s_id_ca1_non_rdf_annotation ""
    sbml_compartment_s_id_ca1_notes ""
    sbml_compartment_s_id_ca1_outside "default"
    sbml_compartment_s_id_ca1_size "1.0"
    sbml_compartment_s_id_ca1_units "volume"
  ]
  sbml_compartment_s_id_ca2 [
    sbml_compartment_s_id_ca2_annotation ""
    sbml_compartment_s_id_ca2_id "s_id_ca2"
    sbml_compartment_s_id_ca2_meta_id "s_id_ca2"
    sbml_compartment_s_id_ca2_name "endosome"
    sbml_compartment_s_id_ca2_non_rdf_annotation ""
    sbml_compartment_s_id_ca2_notes ""
    sbml_compartment_s_id_ca2_outside "s_id_ca1"
    sbml_compartment_s_id_ca2_size "1.0"
    sbml_compartment_s_id_ca2_units "volume"
  ]
  sbml_compartment_s_id_ca3 [
    sbml_compartment_s_id_ca3_annotation ""
    sbml_compartment_s_id_ca3_id "s_id_ca3"
    sbml_compartment_s_id_ca3_meta_id "s_id_ca3"
    sbml_compartment_s_id_ca3_name "nucleus"
    sbml_compartment_s_id_ca3_non_rdf_annotation ""
    sbml_compartment_s_id_ca3_notes ""
    sbml_compartment_s_id_ca3_outside "s_id_ca1"
    sbml_compartment_s_id_ca3_size "1.0"
    sbml_compartment_s_id_ca3_units "volume"
  ]
  sbml_unit_definition_1 [
    sbml_unit_definition_1_id "area"
    sbml_unit_definition_1_name "area"
    sbml_unit_definition_1_sub_unit_1_ "(1.0 * 10^0 * metre)^2.0"
    sbml_unit_definition_1unit "(1.0 * 10^0 * metre)^2.0"
  ]
  sbml_unit_definition_2 [
    sbml_unit_definition_2_id "volume"
    sbml_unit_definition_2_name "volume"
    sbml_unit_definition_2_sub_unit_1_ "(1.0 * 10^0 * litre)^1.0"
    sbml_unit_definition_2unit "(1.0 * 10^0 * litre)^1.0"
  ]
  sbml_unit_definition_3 [
    sbml_unit_definition_3_id "length"
    sbml_unit_definition_3_name "length"
    sbml_unit_definition_3_sub_unit_1_ "(1.0 * 10^0 * metre)^1.0"
    sbml_unit_definition_3unit "(1.0 * 10^0 * metre)^1.0"
  ]
  sbml_unit_definition_4 [
    sbml_unit_definition_4_id "time"
    sbml_unit_definition_4_name "time"
    sbml_unit_definition_4_sub_unit_1_ "(1.0 * 10^0 * second)^1.0"
    sbml_unit_definition_4unit "(1.0 * 10^0 * second)^1.0"
  ]
  sbml_unit_definition_5 [
    sbml_unit_definition_5_id "substance"
    sbml_unit_definition_5_name "substance"
    sbml_unit_definition_5_sub_unit_1_ "(1.0 * 10^0 * mole)^1.0"
    sbml_unit_definition_5unit "(1.0 * 10^0 * mole)^1.0"
  ]
  directed 0
  node [
    id 1
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tank:traf3:ikbke__cell__activated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa82"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa82"
      minerva_elementId2 "csa83"
      minerva_name "TANK:TRAF3:IKBKE"
      minerva_state1 "activated"
      minerva_type "Complex"
      minerva_x 1590.0
      minerva_x2 380.0
      minerva_y 1365.0
      minerva_y2 820.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa82"
      species_meta_id "s_id_csa82"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 2
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "irf3__cell__phosphorylated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa119"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa156"
      minerva_elementId2 "sa119"
      minerva_name "IRF3"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6118"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14653"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/Q14653"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000126456"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_001571"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IRF3"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IRF3"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/3661"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/3661"
      minerva_ref_type__resource1 "HGNC__6118"
      minerva_ref_type__resource2 "UNIPROT__Q14653"
      minerva_ref_type__resource3 "UNIPROT__Q14653"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000126456"
      minerva_ref_type__resource5 "REFSEQ__NM_001571"
      minerva_ref_type__resource6 "HGNC_SYMBOL__IRF3"
      minerva_ref_type__resource7 "HGNC_SYMBOL__IRF3"
      minerva_ref_type__resource8 "ENTREZ__3661"
      minerva_ref_type__resource9 "ENTREZ__3661"
      minerva_state1 "PHOSPHORYLATED"
      minerva_type "Protein"
      minerva_x 1670.0
      minerva_x2 440.0
      minerva_y 1960.0
      minerva_y2 1445.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa119"
      species_meta_id "s_id_sa119"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 3
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "n__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa381"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa381"
      minerva_elementId2 "sa352"
      minerva_name "N"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/1798174255"
      minerva_ref_type__resource1 "NCBI_PROTEIN__1798174255"
      minerva_type "Protein"
      minerva_x 1220.0
      minerva_x2 1180.0
      minerva_y 2100.0
      minerva_y2 525.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa381"
      species_meta_id "s_id_sa381"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 4
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mapk14__cell__phosphorylated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa368"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa368"
      minerva_former_symbols "CSBP1; CSBP2; CSPB1"
      minerva_name "MAPK14"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.24"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_001315"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000112062"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6876"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/Q16539"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/Q16539"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAPK14"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/1432"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAPK14"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/1432"
      minerva_ref_type__resource1 "EC__2.7.11.24"
      minerva_ref_type__resource10 "REFSEQ__NM_001315"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000112062"
      minerva_ref_type__resource3 "HGNC__6876"
      minerva_ref_type__resource4 "UNIPROT__Q16539"
      minerva_ref_type__resource5 "UNIPROT__Q16539"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MAPK14"
      minerva_ref_type__resource7 "ENTREZ__1432"
      minerva_ref_type__resource8 "HGNC_SYMBOL__MAPK14"
      minerva_ref_type__resource9 "ENTREZ__1432"
      minerva_state1 "PHOSPHORYLATED"
      minerva_type "Protein"
      minerva_x 870.0
      minerva_y 1950.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa368"
      species_meta_id "s_id_sa368"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 6
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "map2k6__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa370"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa370"
      minerva_former_symbols "PRKMK6"
      minerva_name "MAP2K6"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.12.2"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP2K6"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000108984"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/5608"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/5608"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P52564"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P52564"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_002758"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6846"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP2K6"
      minerva_ref_type__resource1 "EC__2.7.12.2"
      minerva_ref_type__resource10 "HGNC_SYMBOL__MAP2K6"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000108984"
      minerva_ref_type__resource3 "ENTREZ__5608"
      minerva_ref_type__resource4 "ENTREZ__5608"
      minerva_ref_type__resource5 "UNIPROT__P52564"
      minerva_ref_type__resource6 "UNIPROT__P52564"
      minerva_ref_type__resource7 "REFSEQ__NM_002758"
      minerva_ref_type__resource8 "HGNC__6846"
      minerva_ref_type__resource9 "HGNC_SYMBOL__MAP2K6"
      minerva_type "Protein"
      minerva_x 805.0
      minerva_y 1480.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa370"
      species_meta_id "s_id_sa370"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 7
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 800.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ikk_space_complex__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa75"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa75"
      minerva_name "IKK Complex"
      minerva_type "Complex"
      minerva_x 1205.0
      minerva_y 1670.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa75"
      species_meta_id "s_id_csa75"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 8
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ripk1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa421"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa421"
      minerva_name "RIPK1"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/10019"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/8737"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=RIPK1"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000137275"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=RIPK1"
      minerva_ref_link5 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.1"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/Q13546"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/Q13546"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_003804"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/8737"
      minerva_ref_type__resource1 "HGNC__10019"
      minerva_ref_type__resource10 "ENTREZ__8737"
      minerva_ref_type__resource2 "HGNC_SYMBOL__RIPK1"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000137275"
      minerva_ref_type__resource4 "HGNC_SYMBOL__RIPK1"
      minerva_ref_type__resource5 "EC__2.7.11.1"
      minerva_ref_type__resource6 "UNIPROT__Q13546"
      minerva_ref_type__resource7 "UNIPROT__Q13546"
      minerva_ref_type__resource8 "REFSEQ__NM_003804"
      minerva_ref_type__resource9 "ENTREZ__8737"
      minerva_type "Protein"
      minerva_x 345.0
      minerva_y 545.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa421"
      species_meta_id "s_id_sa421"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 9
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1000.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp3__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa357"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa361"
      minerva_elementId2 "sa357"
      minerva_elementId3 "sa349"
      minerva_elementId4 "sa354"
      minerva_elementId5 "sa363"
      minerva_name "Nsp3"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/1802476807"
      minerva_ref_type__resource1 "NCBI_PROTEIN__1802476807"
      minerva_ref_type__resource2 "UNIPROT__Nsp3"
      minerva_type "Protein"
      minerva_x 1720.0
      minerva_x2 290.0
      minerva_x3 625.0
      minerva_x4 1405.0
      minerva_x5 1105.0
      minerva_y 1210.0
      minerva_y2 1234.0
      minerva_y3 875.0
      minerva_y4 255.0
      minerva_y5 1035.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa357"
      species_meta_id "s_id_sa357"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 10
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s550__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa404"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa404"
      minerva_name "s550"
      minerva_type "Degraded"
      minerva_x 1180.0
      minerva_y 750.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa404"
      species_meta_id "s_id_sa404"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 11
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1200.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf8__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa355"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa355"
      minerva_name "Orf8"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/1796318604"
      minerva_ref_type__resource1 "NCBI_PROTEIN__1796318604"
      minerva_type "Protein"
      minerva_x 360.0
      minerva_y 1060.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa355"
      species_meta_id "s_id_sa355"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 12
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "map2k3__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa369"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa369"
      minerva_former_symbols "PRKMK3"
      minerva_name "MAP2K3"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/5606"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000034152"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/5606"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.12.2"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P46734"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P46734"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6843"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_145109"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP2K3"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP2K3"
      minerva_ref_type__resource1 "ENTREZ__5606"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000034152"
      minerva_ref_type__resource2 "ENTREZ__5606"
      minerva_ref_type__resource3 "EC__2.7.12.2"
      minerva_ref_type__resource4 "UNIPROT__P46734"
      minerva_ref_type__resource5 "UNIPROT__P46734"
      minerva_ref_type__resource6 "HGNC__6843"
      minerva_ref_type__resource7 "REFSEQ__NM_145109"
      minerva_ref_type__resource8 "HGNC_SYMBOL__MAP2K3"
      minerva_ref_type__resource9 "HGNC_SYMBOL__MAP2K3"
      minerva_type "Protein"
      minerva_x 900.0
      minerva_y 1480.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa369"
      species_meta_id "s_id_sa369"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 13
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ticam1:traf3:traf6__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa9"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa9"
      minerva_name "TICAM1:TRAF3:TRAF6"
      minerva_type "Complex"
      minerva_x 215.0
      minerva_y 695.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa9"
      species_meta_id "s_id_csa9"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 14
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ube2n__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa331"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa331"
      minerva_name "UBE2N"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000177889"
      minerva_ref_link10 "http://purl.uniprot.org/uniprot/P61088"
      minerva_ref_link2 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.23"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=UBE2N"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=UBE2N"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/7334"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/7334"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_003348"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12492"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/P61088"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000177889"
      minerva_ref_type__resource10 "UNIPROT__P61088"
      minerva_ref_type__resource2 "EC__2.3.2.23"
      minerva_ref_type__resource3 "HGNC_SYMBOL__UBE2N"
      minerva_ref_type__resource4 "HGNC_SYMBOL__UBE2N"
      minerva_ref_type__resource5 "ENTREZ__7334"
      minerva_ref_type__resource6 "ENTREZ__7334"
      minerva_ref_type__resource7 "REFSEQ__NM_003348"
      minerva_ref_type__resource8 "HGNC__12492"
      minerva_ref_type__resource9 "UNIPROT__P61088"
      minerva_type "Protein"
      minerva_x 915.0
      minerva_y 870.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa331"
      species_meta_id "s_id_sa331"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 15
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tbk1__cell__activated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa154"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa154"
      minerva_elementId2 "sa117"
      minerva_name "TBK1"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TBK1"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/29110"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000183735"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TBK1"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/Q9UHD2"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/Q9UHD2"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.1"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_013254"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11584"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/29110"
      minerva_ref_type__resource1 "HGNC_SYMBOL__TBK1"
      minerva_ref_type__resource10 "ENTREZ__29110"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000183735"
      minerva_ref_type__resource3 "HGNC_SYMBOL__TBK1"
      minerva_ref_type__resource4 "UNIPROT__Q9UHD2"
      minerva_ref_type__resource5 "UNIPROT__Q9UHD2"
      minerva_ref_type__resource6 "EC__2.7.11.1"
      minerva_ref_type__resource7 "REFSEQ__NM_013254"
      minerva_ref_type__resource8 "HGNC__11584"
      minerva_ref_type__resource9 "ENTREZ__29110"
      minerva_structuralState "activated"
      minerva_type "Protein"
      minerva_x 1730.0
      minerva_x2 425.0
      minerva_y 1600.0
      minerva_y2 995.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa154"
      species_meta_id "s_id_sa154"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 16
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "traf6__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa422"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa265"
      minerva_elementId2 "sa129"
      minerva_elementId3 "sa422"
      minerva_elementId4 "sa428"
      minerva_name "TRAF6"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRAF6"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/7189"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7189"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000175104"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/Q9Y4K3"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/Q9Y4K3"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12036"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_145803"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRAF6"
      minerva_ref_type__resource1 "EC__2.3.2.27"
      minerva_ref_type__resource10 "HGNC_SYMBOL__TRAF6"
      minerva_ref_type__resource2 "ENTREZ__7189"
      minerva_ref_type__resource3 "ENTREZ__7189"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000175104"
      minerva_ref_type__resource5 "UNIPROT__Q9Y4K3"
      minerva_ref_type__resource6 "UNIPROT__Q9Y4K3"
      minerva_ref_type__resource7 "HGNC__12036"
      minerva_ref_type__resource8 "REFSEQ__NM_145803"
      minerva_ref_type__resource9 "HGNC_SYMBOL__TRAF6"
      minerva_type "Protein"
      minerva_x 1010.0
      minerva_x2 1110.0
      minerva_x3 345.0
      minerva_x4 680.0
      minerva_y 915.0
      minerva_y2 890.0
      minerva_y3 545.0
      minerva_y4 740.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa422"
      species_meta_id "s_id_sa422"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 17
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mapk14__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa445"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa445"
      minerva_former_symbols "CSBP1; CSBP2; CSPB1"
      minerva_name "MAPK14"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.24"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_001315"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000112062"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6876"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/Q16539"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/Q16539"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAPK14"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/1432"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAPK14"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/1432"
      minerva_ref_type__resource1 "EC__2.7.11.24"
      minerva_ref_type__resource10 "REFSEQ__NM_001315"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000112062"
      minerva_ref_type__resource3 "HGNC__6876"
      minerva_ref_type__resource4 "UNIPROT__Q16539"
      minerva_ref_type__resource5 "UNIPROT__Q16539"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MAPK14"
      minerva_ref_type__resource7 "ENTREZ__1432"
      minerva_ref_type__resource8 "HGNC_SYMBOL__MAPK14"
      minerva_ref_type__resource9 "ENTREZ__1432"
      minerva_type "Protein"
      minerva_x 870.0
      minerva_y 1780.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa445"
      species_meta_id "s_id_sa445"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 18
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 800.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "traf3__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa493"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa493"
      minerva_name "TRAF3"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRAF3"
      minerva_ref_link10 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12033"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000131323"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRAF3"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/7187"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/7187"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_145725"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q13114"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q13114"
      minerva_ref_type__resource1 "HGNC_SYMBOL__TRAF3"
      minerva_ref_type__resource10 "HGNC__12033"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000131323"
      minerva_ref_type__resource3 "HGNC_SYMBOL__TRAF3"
      minerva_ref_type__resource4 "EC__2.3.2.27"
      minerva_ref_type__resource5 "ENTREZ__7187"
      minerva_ref_type__resource6 "ENTREZ__7187"
      minerva_ref_type__resource7 "REFSEQ__NM_145725"
      minerva_ref_type__resource8 "UNIPROT__Q13114"
      minerva_ref_type__resource9 "UNIPROT__Q13114"
      minerva_type "Protein"
      minerva_x 525.0
      minerva_y 465.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa493"
      species_meta_id "s_id_sa493"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 19
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mavs__cell__none__5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa128"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa128"
      minerva_homodimer "5"
      minerva_name "MAVS"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/Q7Z434"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q7Z434"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAVS"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAVS"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000088888"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/57506"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/57506"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/29233"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_020746"
      minerva_ref_type__resource1 "UNIPROT__Q7Z434"
      minerva_ref_type__resource2 "UNIPROT__Q7Z434"
      minerva_ref_type__resource3 "HGNC_SYMBOL__MAVS"
      minerva_ref_type__resource4 "HGNC_SYMBOL__MAVS"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000088888"
      minerva_ref_type__resource6 "ENTREZ__57506"
      minerva_ref_type__resource7 "ENTREZ__57506"
      minerva_ref_type__resource8 "HGNC__29233"
      minerva_ref_type__resource9 "REFSEQ__NM_020746"
      minerva_type "Protein"
      minerva_x 1450.0
      minerva_y 835.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa128"
      species_meta_id "s_id_sa128"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 20
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1000.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ddx58__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa23"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa23"
      minerva_elementId2 "sa440"
      minerva_name "DDX58"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/23586"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DDX58"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/23586"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_014314"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000107201"
      minerva_ref_link5 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.6.4.13"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/O95786"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/O95786"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/19102"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DDX58"
      minerva_ref_type__resource1 "ENTREZ__23586"
      minerva_ref_type__resource10 "HGNC_SYMBOL__DDX58"
      minerva_ref_type__resource2 "ENTREZ__23586"
      minerva_ref_type__resource3 "REFSEQ__NM_014314"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000107201"
      minerva_ref_type__resource5 "EC__3.6.4.13"
      minerva_ref_type__resource6 "UNIPROT__O95786"
      minerva_ref_type__resource7 "UNIPROT__O95786"
      minerva_ref_type__resource8 "HGNC__19102"
      minerva_ref_type__resource9 "HGNC_SYMBOL__DDX58"
      minerva_type "Protein"
      minerva_x 1155.0
      minerva_x2 1190.0
      minerva_y 465.0
      minerva_y2 160.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa23"
      species_meta_id "s_id_sa23"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 21
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nsp15__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa416"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa415"
      minerva_elementId2 "sa416"
      minerva_name "Nsp15"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/1802476818"
      minerva_ref_type__resource1 "NCBI_PROTEIN__1802476818"
      minerva_type "Protein"
      minerva_x 1620.0
      minerva_x2 740.0
      minerva_y 235.0
      minerva_y2 1675.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa416"
      species_meta_id "s_id_sa416"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 23
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 200.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tlr3__endosome__none__2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa5"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa5"
      minerva_homodimer "2"
      minerva_name "TLR3"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/O15455"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/O15455"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000164342"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_003265"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/7098"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/7098"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TLR3"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TLR3"
      minerva_ref_link9 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11849"
      minerva_ref_type__resource1 "UNIPROT__O15455"
      minerva_ref_type__resource2 "UNIPROT__O15455"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000164342"
      minerva_ref_type__resource4 "REFSEQ__NM_003265"
      minerva_ref_type__resource5 "ENTREZ__7098"
      minerva_ref_type__resource6 "ENTREZ__7098"
      minerva_ref_type__resource7 "HGNC_SYMBOL__TLR3"
      minerva_ref_type__resource8 "HGNC_SYMBOL__TLR3"
      minerva_ref_type__resource9 "HGNC__11849"
      minerva_type "Protein"
      minerva_x 375.0
      minerva_y 185.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "endosome"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa5"
      species_meta_id "s_id_sa5"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 24
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s552__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa407"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa407"
      minerva_name "s552"
      minerva_type "Degraded"
      minerva_x 1220.0
      minerva_y 720.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa407"
      species_meta_id "s_id_sa407"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 25
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "trim25__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa122"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa122"
      minerva_former_symbols "ZNF147"
      minerva_name "TRIM25"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "Protein"
      minerva_x 1435.0
      minerva_y 505.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa122"
      species_meta_id "s_id_sa122"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 26
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mavs:tradd__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa86"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa86"
      minerva_name "MAVS:TRADD"
      minerva_type "Complex"
      minerva_x 1450.0
      minerva_y 1040.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa86"
      species_meta_id "s_id_csa86"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 27
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf9__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa362"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa362"
      minerva_name "Orf9"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/1489679"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P59636"
      minerva_ref_type__resource1 "ENTREZ__1489679"
      minerva_ref_type__resource2 "UNIPROT__P59636"
      minerva_type "Protein"
      minerva_x 925.0
      minerva_y 610.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa362"
      species_meta_id "s_id_sa362"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 28
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tbk1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa426"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa426"
      minerva_elementId2 "sa490"
      minerva_name "TBK1"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TBK1"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/29110"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000183735"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TBK1"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/Q9UHD2"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/Q9UHD2"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.1"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_013254"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11584"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/29110"
      minerva_ref_type__resource1 "HGNC_SYMBOL__TBK1"
      minerva_ref_type__resource10 "ENTREZ__29110"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000183735"
      minerva_ref_type__resource3 "HGNC_SYMBOL__TBK1"
      minerva_ref_type__resource4 "UNIPROT__Q9UHD2"
      minerva_ref_type__resource5 "UNIPROT__Q9UHD2"
      minerva_ref_type__resource6 "EC__2.7.11.1"
      minerva_ref_type__resource7 "REFSEQ__NM_013254"
      minerva_ref_type__resource8 "HGNC__11584"
      minerva_ref_type__resource9 "ENTREZ__29110"
      minerva_type "Protein"
      minerva_x 295.0
      minerva_x2 1730.0
      minerva_y 940.0
      minerva_y2 1455.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa426"
      species_meta_id "s_id_sa426"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 29
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 800.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "myd88:irak:traf__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa56"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa56"
      minerva_name "MYD88:IRAK:TRAF"
      minerva_type "Complex"
      minerva_x 780.5
      minerva_y 951.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa56"
      species_meta_id "s_id_csa56"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 30
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "irf3__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa348"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa348"
      minerva_elementId2 "sa379"
      minerva_name "IRF3"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6118"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14653"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/Q14653"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000126456"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_001571"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IRF3"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IRF3"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/3661"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/3661"
      minerva_ref_type__resource1 "HGNC__6118"
      minerva_ref_type__resource2 "UNIPROT__Q14653"
      minerva_ref_type__resource3 "UNIPROT__Q14653"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000126456"
      minerva_ref_type__resource5 "REFSEQ__NM_001571"
      minerva_ref_type__resource6 "HGNC_SYMBOL__IRF3"
      minerva_ref_type__resource7 "HGNC_SYMBOL__IRF3"
      minerva_ref_type__resource8 "ENTREZ__3661"
      minerva_ref_type__resource9 "ENTREZ__3661"
      minerva_type "Protein"
      minerva_x 275.0
      minerva_x2 1830.0
      minerva_y 1180.0
      minerva_y2 1725.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa348"
      species_meta_id "s_id_sa348"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 31
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1000.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "map3k7__cell__activated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa367"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa367"
      minerva_former_symbols "TAK1"
      minerva_name "MAP3K7"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/O43318"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.25"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/O43318"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000135341"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_145331"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6859"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/6885"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/6885"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP3K7"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP3K7"
      minerva_ref_type__resource1 "UNIPROT__O43318"
      minerva_ref_type__resource10 "EC__2.7.11.25"
      minerva_ref_type__resource2 "UNIPROT__O43318"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000135341"
      minerva_ref_type__resource4 "REFSEQ__NM_145331"
      minerva_ref_type__resource5 "HGNC__6859"
      minerva_ref_type__resource6 "ENTREZ__6885"
      minerva_ref_type__resource7 "ENTREZ__6885"
      minerva_ref_type__resource8 "HGNC_SYMBOL__MAP3K7"
      minerva_ref_type__resource9 "HGNC_SYMBOL__MAP3K7"
      minerva_structuralState "activated"
      minerva_type "Protein"
      minerva_x 955.0
      minerva_y 1365.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa367"
      species_meta_id "s_id_sa367"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 32
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf3b__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa356"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa356"
      minerva_name "Orf3b"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P59633"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/1489670"
      minerva_ref_type__resource1 "UNIPROT__P59633"
      minerva_ref_type__resource2 "ENTREZ__1489670"
      minerva_type "Protein"
      minerva_x 270.0
      minerva_y 1060.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa356"
      species_meta_id "s_id_sa356"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 33
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1200.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "casp8__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa459"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa459"
      minerva_name "CASP8"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1509"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000064012"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CASP8"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/Q14790"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/Q14790"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CASP8"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/841"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/841"
      minerva_ref_link8 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.22.61"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_001228"
      minerva_ref_type__resource1 "HGNC__1509"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000064012"
      minerva_ref_type__resource2 "HGNC_SYMBOL__CASP8"
      minerva_ref_type__resource3 "UNIPROT__Q14790"
      minerva_ref_type__resource4 "UNIPROT__Q14790"
      minerva_ref_type__resource5 "HGNC_SYMBOL__CASP8"
      minerva_ref_type__resource6 "ENTREZ__841"
      minerva_ref_type__resource7 "ENTREZ__841"
      minerva_ref_type__resource8 "EC__3.4.22.61"
      minerva_ref_type__resource9 "REFSEQ__NM_001228"
      minerva_type "Protein"
      minerva_x 1300.0
      minerva_y 1545.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa459"
      species_meta_id "s_id_sa459"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 34
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "m__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa359"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa359"
      minerva_elementId2 "sa353"
      minerva_elementId3 "sa408"
      minerva_elementId4 "sa403"
      minerva_name "M"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/1796318601"
      minerva_ref_type__resource1 "NCBI_PROTEIN__1796318601"
      minerva_ref_type__resource2 "UNIPROT__M"
      minerva_type "Protein"
      minerva_x 1125.0
      minerva_x2 1405.0
      minerva_x3 1720.0
      minerva_x4 520.0
      minerva_y 1995.0
      minerva_y2 205.0
      minerva_y3 1270.0
      minerva_y4 725.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa359"
      species_meta_id "s_id_sa359"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 35
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "traf6__cell__ubiquitinated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa337"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa337"
      minerva_name "TRAF6"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRAF6"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/7189"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7189"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000175104"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/Q9Y4K3"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/Q9Y4K3"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12036"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_145803"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRAF6"
      minerva_ref_type__resource1 "EC__2.3.2.27"
      minerva_ref_type__resource10 "HGNC_SYMBOL__TRAF6"
      minerva_ref_type__resource2 "ENTREZ__7189"
      minerva_ref_type__resource3 "ENTREZ__7189"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000175104"
      minerva_ref_type__resource5 "UNIPROT__Q9Y4K3"
      minerva_ref_type__resource6 "UNIPROT__Q9Y4K3"
      minerva_ref_type__resource7 "HGNC__12036"
      minerva_ref_type__resource8 "REFSEQ__NM_145803"
      minerva_ref_type__resource9 "HGNC_SYMBOL__TRAF6"
      minerva_state1 "UBIQUITINATED"
      minerva_type "Protein"
      minerva_x 1150.0
      minerva_y 1225.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa337"
      species_meta_id "s_id_sa337"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 36
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "map2k6__cell__phosphorylated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa450"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa450"
      minerva_former_symbols "PRKMK6"
      minerva_name "MAP2K6"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.12.2"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP2K6"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000108984"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/5608"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/5608"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P52564"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P52564"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_002758"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6846"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP2K6"
      minerva_ref_type__resource1 "EC__2.7.12.2"
      minerva_ref_type__resource10 "HGNC_SYMBOL__MAP2K6"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000108984"
      minerva_ref_type__resource3 "ENTREZ__5608"
      minerva_ref_type__resource4 "ENTREZ__5608"
      minerva_ref_type__resource5 "UNIPROT__P52564"
      minerva_ref_type__resource6 "UNIPROT__P52564"
      minerva_ref_type__resource7 "REFSEQ__NM_002758"
      minerva_ref_type__resource8 "HGNC__6846"
      minerva_ref_type__resource9 "HGNC_SYMBOL__MAP2K6"
      minerva_state1 "PHOSPHORYLATED"
      minerva_type "Protein"
      minerva_x 805.0
      minerva_y 1615.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa450"
      species_meta_id "s_id_sa450"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 37
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "rela__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa482"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa482"
      minerva_former_symbols "NFKB3"
      minerva_name "RELA"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/9955"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000173039"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/5970"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/5970"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_021975"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=RELA"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=RELA"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q04206"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q04206"
      minerva_ref_type__resource1 "HGNC__9955"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000173039"
      minerva_ref_type__resource3 "ENTREZ__5970"
      minerva_ref_type__resource4 "ENTREZ__5970"
      minerva_ref_type__resource5 "REFSEQ__NM_021975"
      minerva_ref_type__resource6 "HGNC_SYMBOL__RELA"
      minerva_ref_type__resource7 "HGNC_SYMBOL__RELA"
      minerva_ref_type__resource8 "UNIPROT__Q04206"
      minerva_ref_type__resource9 "UNIPROT__Q04206"
      minerva_type "Protein"
      minerva_x 1585.0
      minerva_y 1790.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa482"
      species_meta_id "s_id_sa482"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 38
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 600.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tlr7:ssrna__endosome__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa90"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa90"
      minerva_name "TLR7:ssRNA"
      minerva_type "Complex"
      minerva_x 715.0
      minerva_y 390.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "endosome"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa90"
      species_meta_id "s_id_csa90"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 39
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ikbkg__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa484"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa484"
      minerva_former_symbols "IP1; IP2"
      minerva_name "IKBKG"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/5961"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000269335"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IKBKG"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IKBKG"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003639"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/8517"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/8517"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q9Y6K9"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q9Y6K9"
      minerva_ref_type__resource1 "HGNC__5961"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000269335"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IKBKG"
      minerva_ref_type__resource4 "HGNC_SYMBOL__IKBKG"
      minerva_ref_type__resource5 "REFSEQ__NM_003639"
      minerva_ref_type__resource6 "ENTREZ__8517"
      minerva_ref_type__resource7 "ENTREZ__8517"
      minerva_ref_type__resource8 "UNIPROT__Q9Y6K9"
      minerva_ref_type__resource9 "UNIPROT__Q9Y6K9"
      minerva_type "Protein"
      minerva_x 1245.0
      minerva_y 1375.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa484"
      species_meta_id "s_id_sa484"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 40
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 800.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "dsrna__endosome__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa6"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa6"
      minerva_name "dsRNA"
      minerva_type "RNA"
      minerva_x 505.0
      minerva_y 230.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "endosome"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa6"
      species_meta_id "s_id_sa6"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 41
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "irf7__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa347"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa347"
      minerva_name "IRF7"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/Q92985"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q92985"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000185507"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_001572"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IRF7"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IRF7"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6122"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/3665"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/3665"
      minerva_ref_type__resource1 "UNIPROT__Q92985"
      minerva_ref_type__resource2 "UNIPROT__Q92985"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000185507"
      minerva_ref_type__resource4 "REFSEQ__NM_001572"
      minerva_ref_type__resource5 "HGNC_SYMBOL__IRF7"
      minerva_ref_type__resource6 "HGNC_SYMBOL__IRF7"
      minerva_ref_type__resource7 "HGNC__6122"
      minerva_ref_type__resource8 "ENTREZ__3665"
      minerva_ref_type__resource9 "ENTREZ__3665"
      minerva_type "Protein"
      minerva_x 575.0
      minerva_y 1145.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa347"
      species_meta_id "s_id_sa347"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 42
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1000.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ikbke__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa491"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa491"
      minerva_elementId2 "sa427"
      minerva_name "IKBKE"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/Q14164"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000263528"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14164"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/9641"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/9641"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/14552"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.10"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IKBKE"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IKBKE"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_001193321"
      minerva_ref_type__resource1 "UNIPROT__Q14164"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000263528"
      minerva_ref_type__resource2 "UNIPROT__Q14164"
      minerva_ref_type__resource3 "ENTREZ__9641"
      minerva_ref_type__resource4 "ENTREZ__9641"
      minerva_ref_type__resource5 "HGNC__14552"
      minerva_ref_type__resource6 "EC__2.7.11.10"
      minerva_ref_type__resource7 "HGNC_SYMBOL__IKBKE"
      minerva_ref_type__resource8 "HGNC_SYMBOL__IKBKE"
      minerva_ref_type__resource9 "REFSEQ__NM_001193321"
      minerva_type "Protein"
      minerva_x 645.0
      minerva_x2 515.0
      minerva_y 535.0
      minerva_y2 820.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa491"
      species_meta_id "s_id_sa491"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 43
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1100.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tlr8:ssrna__endosome__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa91"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa91"
      minerva_name "TLR8:ssRNA"
      minerva_type "Complex"
      minerva_x 885.0
      minerva_y 385.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "endosome"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa91"
      species_meta_id "s_id_csa91"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 44
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1200.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "degradation_space_of_space_nfkbia__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa146"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa146"
      minerva_name "Degradation of NFKBIA"
      minerva_type "Degraded"
      minerva_x 1080.0
      minerva_y 2150.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa146"
      species_meta_id "s_id_sa146"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 45
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 200.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "irf7__nucleus__phosphorylated__2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa255"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa255"
      minerva_homodimer "2"
      minerva_name "IRF7"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/Q92985"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q92985"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000185507"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_001572"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IRF7"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IRF7"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6122"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/3665"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/3665"
      minerva_ref_type__resource1 "UNIPROT__Q92985"
      minerva_ref_type__resource2 "UNIPROT__Q92985"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000185507"
      minerva_ref_type__resource4 "REFSEQ__NM_001572"
      minerva_ref_type__resource5 "HGNC_SYMBOL__IRF7"
      minerva_ref_type__resource6 "HGNC_SYMBOL__IRF7"
      minerva_ref_type__resource7 "HGNC__6122"
      minerva_ref_type__resource8 "ENTREZ__3665"
      minerva_ref_type__resource9 "ENTREZ__3665"
      minerva_state1 "PHOSPHORYLATED"
      minerva_type "Protein"
      minerva_x 580.0
      minerva_y 2325.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa255"
      species_meta_id "s_id_sa255"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 46
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 300.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ap1__nucleus__activated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa104"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa104"
      minerva_name "AP1"
      minerva_state1 "activated"
      minerva_type "Complex"
      minerva_x 945.0
      minerva_y 2345.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa104"
      species_meta_id "s_id_csa104"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 47
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "irak4__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa333"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa333"
      minerva_name "IRAK4"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/Q9NWZ3"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q9NWZ3"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_001114182"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.1"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/51135"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/51135"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/17967"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IRAK4"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IRAK4"
      minerva_ref_type__resource1 "UNIPROT__Q9NWZ3"
      minerva_ref_type__resource2 "UNIPROT__Q9NWZ3"
      minerva_ref_type__resource3 "REFSEQ__NM_001114182"
      minerva_ref_type__resource4 "EC__2.7.11.1"
      minerva_ref_type__resource5 "ENTREZ__51135"
      minerva_ref_type__resource6 "ENTREZ__51135"
      minerva_ref_type__resource7 "HGNC__17967"
      minerva_ref_type__resource8 "HGNC_SYMBOL__IRAK4"
      minerva_ref_type__resource9 "HGNC_SYMBOL__IRAK4"
      minerva_type "Protein"
      minerva_x 680.0
      minerva_y 685.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa333"
      species_meta_id "s_id_sa333"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 48
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "rnf135__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa123"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa123"
      minerva_name "RNF135"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000181481"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q8IUD6"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/Q8IUD6"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/21158"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_032322"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=RNF135"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=RNF135"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/84282"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/84282"
      minerva_ref_type__resource1 "EC__2.3.2.27"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000181481"
      minerva_ref_type__resource2 "UNIPROT__Q8IUD6"
      minerva_ref_type__resource3 "UNIPROT__Q8IUD6"
      minerva_ref_type__resource4 "HGNC__21158"
      minerva_ref_type__resource5 "REFSEQ__NM_032322"
      minerva_ref_type__resource6 "HGNC_SYMBOL__RNF135"
      minerva_ref_type__resource7 "HGNC_SYMBOL__RNF135"
      minerva_ref_type__resource8 "ENTREZ__84282"
      minerva_ref_type__resource9 "ENTREZ__84282"
      minerva_type "Protein"
      minerva_x 1435.0
      minerva_y 555.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa123"
      species_meta_id "s_id_sa123"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 49
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "irf7__cell__phosphorylated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa120"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa120"
      minerva_name "IRF7"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/Q92985"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q92985"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000185507"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_001572"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IRF7"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IRF7"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6122"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/3665"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/3665"
      minerva_ref_type__resource1 "UNIPROT__Q92985"
      minerva_ref_type__resource2 "UNIPROT__Q92985"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000185507"
      minerva_ref_type__resource4 "REFSEQ__NM_001572"
      minerva_ref_type__resource5 "HGNC_SYMBOL__IRF7"
      minerva_ref_type__resource6 "HGNC_SYMBOL__IRF7"
      minerva_ref_type__resource7 "HGNC__6122"
      minerva_ref_type__resource8 "ENTREZ__3665"
      minerva_ref_type__resource9 "ENTREZ__3665"
      minerva_state1 "PHOSPHORYLATED"
      minerva_type "Protein"
      minerva_x 575.0
      minerva_y 1445.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa120"
      species_meta_id "s_id_sa120"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 50
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "traf5__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa131"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa131"
      minerva_name "TRAF5"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRAF5"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/O00463"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/O00463"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRAF5"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000082512"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/7188"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/7188"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_004619"
      minerva_ref_link9 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12035"
      minerva_ref_type__resource1 "HGNC_SYMBOL__TRAF5"
      minerva_ref_type__resource2 "UNIPROT__O00463"
      minerva_ref_type__resource3 "UNIPROT__O00463"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRAF5"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000082512"
      minerva_ref_type__resource6 "ENTREZ__7188"
      minerva_ref_type__resource7 "ENTREZ__7188"
      minerva_ref_type__resource8 "REFSEQ__NM_004619"
      minerva_ref_type__resource9 "HGNC__12035"
      minerva_type "Protein"
      minerva_x 1180.0
      minerva_y 840.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa131"
      species_meta_id "s_id_sa131"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 51
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 800.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nfkbia__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa481"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa481"
      minerva_former_symbols "NFKBI"
      minerva_name "NFKBIA"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7797"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_020529"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000100906"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_type__resource1 "HGNC__7797"
      minerva_ref_type__resource2 "REFSEQ__NM_020529"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000100906"
      minerva_ref_type__resource4 "UNIPROT__P25963"
      minerva_ref_type__resource5 "UNIPROT__P25963"
      minerva_ref_type__resource6 "ENTREZ__4792"
      minerva_ref_type__resource7 "ENTREZ__4792"
      minerva_ref_type__resource8 "HGNC_SYMBOL__NFKBIA"
      minerva_ref_type__resource9 "HGNC_SYMBOL__NFKBIA"
      minerva_type "Protein"
      minerva_x 1585.0
      minerva_y 1745.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa481"
      species_meta_id "s_id_sa481"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 52
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tank:traf3:ikbke__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa89"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa89"
      minerva_elementId2 "csa105"
      minerva_name "TANK:TRAF3:IKBKE"
      minerva_type "Complex"
      minerva_x 525.0
      minerva_x2 1590.0
      minerva_y 610.0
      minerva_y2 1115.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa89"
      species_meta_id "s_id_csa89"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 53
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1000.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tank__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa492"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa492"
      minerva_former_symbols "TRAF2"
      minerva_name "TANK"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/10010"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/10010"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000136560"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_133484"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/Q92844"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/Q92844"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11562"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TANK"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TANK"
      minerva_ref_type__resource1 "ENTREZ__10010"
      minerva_ref_type__resource2 "ENTREZ__10010"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000136560"
      minerva_ref_type__resource4 "REFSEQ__NM_133484"
      minerva_ref_type__resource5 "UNIPROT__Q92844"
      minerva_ref_type__resource6 "UNIPROT__Q92844"
      minerva_ref_type__resource7 "HGNC__11562"
      minerva_ref_type__resource8 "HGNC_SYMBOL__TANK"
      minerva_ref_type__resource9 "HGNC_SYMBOL__TANK"
      minerva_type "Protein"
      minerva_x 645.0
      minerva_y 485.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa492"
      species_meta_id "s_id_sa492"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 55
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1200.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "map2k3__cell__phosphorylated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa451"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa451"
      minerva_former_symbols "PRKMK3"
      minerva_name "MAP2K3"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/5606"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000034152"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/5606"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.12.2"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P46734"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P46734"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6843"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_145109"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP2K3"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP2K3"
      minerva_ref_type__resource1 "ENTREZ__5606"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000034152"
      minerva_ref_type__resource2 "ENTREZ__5606"
      minerva_ref_type__resource3 "EC__2.7.12.2"
      minerva_ref_type__resource4 "UNIPROT__P46734"
      minerva_ref_type__resource5 "UNIPROT__P46734"
      minerva_ref_type__resource6 "HGNC__6843"
      minerva_ref_type__resource7 "REFSEQ__NM_145109"
      minerva_ref_type__resource8 "HGNC_SYMBOL__MAP2K3"
      minerva_ref_type__resource9 "HGNC_SYMBOL__MAP2K3"
      minerva_state1 "PHOSPHORYLATED"
      minerva_type "Protein"
      minerva_x 900.0
      minerva_y 1615.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa451"
      species_meta_id "s_id_sa451"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 56
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ripk1:fadd__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa97"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa97"
      minerva_name "RIPK1:FADD"
      minerva_type "Complex"
      minerva_x 1354.125
      minerva_y 1248.25
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa97"
      species_meta_id "s_id_csa97"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 57
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 300.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ssrna__endosome__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa14"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa14"
      minerva_name "ssRNA"
      minerva_type "RNA"
      minerva_x 800.0
      minerva_y 230.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "endosome"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa14"
      species_meta_id "s_id_sa14"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 58
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "map2k4__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa371"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa371"
      minerva_former_symbols "SERK1"
      minerva_name "MAP2K4"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.12.2"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/6416"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6844"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000065559"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_001281435"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P45985"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P45985"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP2K4"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP2K4"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/6416"
      minerva_ref_type__resource1 "EC__2.7.12.2"
      minerva_ref_type__resource10 "ENTREZ__6416"
      minerva_ref_type__resource2 "HGNC__6844"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000065559"
      minerva_ref_type__resource4 "REFSEQ__NM_001281435"
      minerva_ref_type__resource5 "UNIPROT__P45985"
      minerva_ref_type__resource6 "UNIPROT__P45985"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MAP2K4"
      minerva_ref_type__resource8 "HGNC_SYMBOL__MAP2K4"
      minerva_ref_type__resource9 "ENTREZ__6416"
      minerva_type "Protein"
      minerva_x 1000.0
      minerva_y 1480.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa371"
      species_meta_id "s_id_sa371"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 59
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "casp10__cell__activated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa389"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa389"
      minerva_name "CASP10"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CASP10"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000003400"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_032977"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CASP10"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/Q92851"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/Q92851"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/843"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/843"
      minerva_ref_link8 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.22.63"
      minerva_ref_link9 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1500"
      minerva_ref_type__resource1 "HGNC_SYMBOL__CASP10"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000003400"
      minerva_ref_type__resource2 "REFSEQ__NM_032977"
      minerva_ref_type__resource3 "HGNC_SYMBOL__CASP10"
      minerva_ref_type__resource4 "UNIPROT__Q92851"
      minerva_ref_type__resource5 "UNIPROT__Q92851"
      minerva_ref_type__resource6 "ENTREZ__843"
      minerva_ref_type__resource7 "ENTREZ__843"
      minerva_ref_type__resource8 "EC__3.4.22.63"
      minerva_ref_type__resource9 "HGNC__1500"
      minerva_structuralState "activated"
      minerva_type "Protein"
      minerva_x 1435.0
      minerva_y 1655.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa389"
      species_meta_id "s_id_sa389"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 60
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "dsrna__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa27"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa27"
      minerva_elementId2 "sa456"
      minerva_elementId3 "sa26"
      minerva_name "dsRNA"
      minerva_type "RNA"
      minerva_x 1520.0
      minerva_x2 665.0
      minerva_x3 1285.0
      minerva_y 125.0
      minerva_y2 1840.0
      minerva_y3 125.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa27"
      species_meta_id "s_id_sa27"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 61
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "traf2__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa130"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa130"
      minerva_name "TRAF2"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link10 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12032"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRAF2"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRAF2"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/7186"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/7186"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000127191"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_021138"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q12933"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q12933"
      minerva_ref_type__resource1 "EC__2.3.2.27"
      minerva_ref_type__resource10 "HGNC__12032"
      minerva_ref_type__resource2 "HGNC_SYMBOL__TRAF2"
      minerva_ref_type__resource3 "HGNC_SYMBOL__TRAF2"
      minerva_ref_type__resource4 "ENTREZ__7186"
      minerva_ref_type__resource5 "ENTREZ__7186"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000127191"
      minerva_ref_type__resource7 "REFSEQ__NM_021138"
      minerva_ref_type__resource8 "UNIPROT__Q12933"
      minerva_ref_type__resource9 "UNIPROT__Q12933"
      minerva_type "Protein"
      minerva_x 1255.0
      minerva_y 785.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa130"
      species_meta_id "s_id_sa130"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 62
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 800.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ifih1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa439"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa439"
      minerva_name "IFIH1"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/18873"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IFIH1"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/64135"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/64135"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_022168"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000115267"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/Q9BYX4"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/Q9BYX4"
      minerva_ref_link8 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.6.4.13"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IFIH1"
      minerva_ref_type__resource1 "HGNC__18873"
      minerva_ref_type__resource10 "HGNC_SYMBOL__IFIH1"
      minerva_ref_type__resource2 "ENTREZ__64135"
      minerva_ref_type__resource3 "ENTREZ__64135"
      minerva_ref_type__resource4 "REFSEQ__NM_022168"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000115267"
      minerva_ref_type__resource6 "UNIPROT__Q9BYX4"
      minerva_ref_type__resource7 "UNIPROT__Q9BYX4"
      minerva_ref_type__resource8 "EC__3.6.4.13"
      minerva_ref_type__resource9 "HGNC_SYMBOL__IFIH1"
      minerva_type "Protein"
      minerva_x 1620.0
      minerva_y 165.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa439"
      species_meta_id "s_id_sa439"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 63
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "eif2ak2:dsrna__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa96"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa96"
      minerva_name "EIF2AK2:dsRNA"
      minerva_type "Complex"
      minerva_x 665.0
      minerva_y 1557.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa96"
      species_meta_id "s_id_csa96"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 64
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1000.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tlr3:dsrna__endosome__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa88"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa88"
      minerva_name "TLR3:dsRNA"
      minerva_type "Complex"
      minerva_x 410.0
      minerva_y 390.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "endosome"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa88"
      species_meta_id "s_id_csa88"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 65
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "itch__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa436"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa436"
      minerva_name "ITCH"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/Q96J02"
      minerva_ref_link10 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/13890"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q96J02"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.26"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ITCH"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ITCH"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000078747"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_001257137"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/83737"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/83737"
      minerva_ref_type__resource1 "UNIPROT__Q96J02"
      minerva_ref_type__resource10 "HGNC__13890"
      minerva_ref_type__resource2 "UNIPROT__Q96J02"
      minerva_ref_type__resource3 "EC__2.3.2.26"
      minerva_ref_type__resource4 "HGNC_SYMBOL__ITCH"
      minerva_ref_type__resource5 "HGNC_SYMBOL__ITCH"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000078747"
      minerva_ref_type__resource7 "REFSEQ__NM_001257137"
      minerva_ref_type__resource8 "ENTREZ__83737"
      minerva_ref_type__resource9 "ENTREZ__83737"
      minerva_type "Protein"
      minerva_x 1065.0
      minerva_y 575.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa436"
      species_meta_id "s_id_sa436"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 66
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1200.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tradd__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa400"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa400"
      minerva_name "TRADD"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/8717"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000102871"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/8717"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_001323552"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/Q15628"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/Q15628"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12030"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRADD"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRADD"
      minerva_ref_type__resource1 "ENTREZ__8717"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000102871"
      minerva_ref_type__resource3 "ENTREZ__8717"
      minerva_ref_type__resource4 "REFSEQ__NM_001323552"
      minerva_ref_type__resource5 "UNIPROT__Q15628"
      minerva_ref_type__resource6 "UNIPROT__Q15628"
      minerva_ref_type__resource7 "HGNC__12030"
      minerva_ref_type__resource8 "HGNC_SYMBOL__TRADD"
      minerva_ref_type__resource9 "HGNC_SYMBOL__TRADD"
      minerva_type "Protein"
      minerva_x 1545.0
      minerva_y 905.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa400"
      species_meta_id "s_id_sa400"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 67
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 200.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "transcription_space_of_space_proinflammatory_space_proteins__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa480"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa480"
      minerva_name "transcription of proinflammatory proteins"
      minerva_type "Phenotype"
      minerva_x 945.0
      minerva_y 2465.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa480"
      species_meta_id "s_id_sa480"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 68
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 300.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tlr7__endosome__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa430"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa430"
      minerva_name "TLR7"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/Q9NYK1"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q9NYK1"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/15631"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_016562"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TLR7"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TLR7"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000196664"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/51284"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/51284"
      minerva_ref_type__resource1 "UNIPROT__Q9NYK1"
      minerva_ref_type__resource2 "UNIPROT__Q9NYK1"
      minerva_ref_type__resource3 "HGNC__15631"
      minerva_ref_type__resource4 "REFSEQ__NM_016562"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TLR7"
      minerva_ref_type__resource6 "HGNC_SYMBOL__TLR7"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000196664"
      minerva_ref_type__resource8 "ENTREZ__51284"
      minerva_ref_type__resource9 "ENTREZ__51284"
      minerva_type "Protein"
      minerva_x 715.0
      minerva_y 185.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "endosome"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa430"
      species_meta_id "s_id_sa430"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 69
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mavs:traf__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa32"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa32"
      minerva_name "MAVS:TRAF"
      minerva_type "Complex"
      minerva_x 1255.0
      minerva_y 1080.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa32"
      species_meta_id "s_id_csa32"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 70
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mavs__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa127"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa127"
      minerva_name "MAVS"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/Q7Z434"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q7Z434"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAVS"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAVS"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000088888"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/57506"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/57506"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/29233"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_020746"
      minerva_ref_type__resource1 "UNIPROT__Q7Z434"
      minerva_ref_type__resource2 "UNIPROT__Q7Z434"
      minerva_ref_type__resource3 "HGNC_SYMBOL__MAVS"
      minerva_ref_type__resource4 "HGNC_SYMBOL__MAVS"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000088888"
      minerva_ref_type__resource6 "ENTREZ__57506"
      minerva_ref_type__resource7 "ENTREZ__57506"
      minerva_ref_type__resource8 "HGNC__29233"
      minerva_ref_type__resource9 "REFSEQ__NM_020746"
      minerva_type "Protein"
      minerva_x 1340.0
      minerva_y 720.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa127"
      species_meta_id "s_id_sa127"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 71
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nfkb1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa483"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa483"
      minerva_name "NFKB1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4790"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/4790"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000109320"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKB1"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKB1"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7794"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P19838"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P19838"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_003998"
      minerva_ref_type__resource1 "ENTREZ__4790"
      minerva_ref_type__resource2 "ENTREZ__4790"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000109320"
      minerva_ref_type__resource4 "HGNC_SYMBOL__NFKB1"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NFKB1"
      minerva_ref_type__resource6 "HGNC__7794"
      minerva_ref_type__resource7 "UNIPROT__P19838"
      minerva_ref_type__resource8 "UNIPROT__P19838"
      minerva_ref_type__resource9 "REFSEQ__NM_003998"
      minerva_type "Protein"
      minerva_x 1585.0
      minerva_y 1835.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa483"
      species_meta_id "s_id_sa483"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 72
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ikbkb__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa486"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa486"
      minerva_name "IKBKB"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/5960"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/3551"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/protein/NM_001190720"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000104365"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.10"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.1"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IKBKB"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/O14920"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/O14920"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IKBKB"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/3551"
      minerva_ref_type__resource1 "HGNC__5960"
      minerva_ref_type__resource10 "ENTREZ__3551"
      minerva_ref_type__resource11 "REFSEQ__NM_001190720"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000104365"
      minerva_ref_type__resource3 "EC__2.7.11.10"
      minerva_ref_type__resource4 "EC__2.7.11.1"
      minerva_ref_type__resource5 "HGNC_SYMBOL__IKBKB"
      minerva_ref_type__resource6 "UNIPROT__O14920"
      minerva_ref_type__resource7 "UNIPROT__O14920"
      minerva_ref_type__resource8 "HGNC_SYMBOL__IKBKB"
      minerva_ref_type__resource9 "ENTREZ__3551"
      minerva_type "Protein"
      minerva_x 1245.0
      minerva_y 1465.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa486"
      species_meta_id "s_id_sa486"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 73
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 800.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ripk1:fadd__cell__activated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa84"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa84"
      minerva_name "RIPK1:FADD"
      minerva_state1 "activated"
      minerva_type "Complex"
      minerva_x 1354.125
      minerva_y 1428.25
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa84"
      species_meta_id "s_id_csa84"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 74
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ikk_space_complex__cell__activated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa99"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa99"
      minerva_name "IKK Complex"
      minerva_state1 "activated"
      minerva_type "Complex"
      minerva_x 1205.0
      minerva_y 1870.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa99"
      species_meta_id "s_id_csa99"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 75
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1000.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "casp8__cell__activated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa390"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa390"
      minerva_name "CASP8"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1509"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000064012"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CASP8"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/Q14790"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/Q14790"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CASP8"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/841"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/841"
      minerva_ref_link8 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.22.61"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_001228"
      minerva_ref_type__resource1 "HGNC__1509"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000064012"
      minerva_ref_type__resource2 "HGNC_SYMBOL__CASP8"
      minerva_ref_type__resource3 "UNIPROT__Q14790"
      minerva_ref_type__resource4 "UNIPROT__Q14790"
      minerva_ref_type__resource5 "HGNC_SYMBOL__CASP8"
      minerva_ref_type__resource6 "ENTREZ__841"
      minerva_ref_type__resource7 "ENTREZ__841"
      minerva_ref_type__resource8 "EC__3.4.22.61"
      minerva_ref_type__resource9 "REFSEQ__NM_001228"
      minerva_structuralState "activated"
      minerva_type "Protein"
      minerva_x 1300.0
      minerva_y 1655.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa390"
      species_meta_id "s_id_sa390"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 76
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "irak1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa80"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa80"
      minerva_name "IRAK1"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6112"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/3654"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P51617"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P51617"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.1"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000184216"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_001025242"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IRAK1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/3654"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IRAK1"
      minerva_ref_type__resource1 "HGNC__6112"
      minerva_ref_type__resource10 "ENTREZ__3654"
      minerva_ref_type__resource2 "UNIPROT__P51617"
      minerva_ref_type__resource3 "UNIPROT__P51617"
      minerva_ref_type__resource4 "EC__2.7.11.1"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000184216"
      minerva_ref_type__resource6 "REFSEQ__NM_001025242"
      minerva_ref_type__resource7 "HGNC_SYMBOL__IRAK1"
      minerva_ref_type__resource8 "ENTREZ__3654"
      minerva_ref_type__resource9 "HGNC_SYMBOL__IRAK1"
      minerva_type "Protein"
      minerva_x 680.0
      minerva_y 625.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa80"
      species_meta_id "s_id_sa80"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 77
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1200.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ubiquitin_minus_proteasome_space_complex__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa87"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa87"
      minerva_name "ubiquitin-proteasome complex"
      minerva_type "Complex"
      minerva_x 1135.0
      minerva_y 2210.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa87"
      species_meta_id "s_id_csa87"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 78
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ticam1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa418"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa418"
      minerva_elementId2 "sa419"
      minerva_name "TICAM1"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TICAM1"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q8IUC6"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/Q8IUC6"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TICAM1"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000127666"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/18348"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_014261"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/148022"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/148022"
      minerva_ref_type__resource1 "HGNC_SYMBOL__TICAM1"
      minerva_ref_type__resource2 "UNIPROT__Q8IUC6"
      minerva_ref_type__resource3 "UNIPROT__Q8IUC6"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TICAM1"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000127666"
      minerva_ref_type__resource6 "HGNC__18348"
      minerva_ref_type__resource7 "REFSEQ__NM_014261"
      minerva_ref_type__resource8 "ENTREZ__148022"
      minerva_ref_type__resource9 "ENTREZ__148022"
      minerva_type "Protein"
      minerva_x 215.0
      minerva_x2 345.0
      minerva_y 435.0
      minerva_y2 595.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa418"
      species_meta_id "s_id_sa418"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 79
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "map2k4__cell__phosphorylated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa452"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa452"
      minerva_former_symbols "SERK1"
      minerva_name "MAP2K4"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.12.2"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/6416"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6844"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000065559"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_001281435"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P45985"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P45985"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP2K4"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP2K4"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/6416"
      minerva_ref_type__resource1 "EC__2.7.12.2"
      minerva_ref_type__resource10 "ENTREZ__6416"
      minerva_ref_type__resource2 "HGNC__6844"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000065559"
      minerva_ref_type__resource4 "REFSEQ__NM_001281435"
      minerva_ref_type__resource5 "UNIPROT__P45985"
      minerva_ref_type__resource6 "UNIPROT__P45985"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MAP2K4"
      minerva_ref_type__resource8 "HGNC_SYMBOL__MAP2K4"
      minerva_ref_type__resource9 "ENTREZ__6416"
      minerva_state1 "PHOSPHORYLATED"
      minerva_type "Protein"
      minerva_x 1000.0
      minerva_y 1615.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa452"
      species_meta_id "s_id_sa452"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 80
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 400.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nfkb__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa103"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa103"
      minerva_name "NFKB"
      minerva_type "Complex"
      minerva_x 1435.0
      minerva_y 2345.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa103"
      species_meta_id "s_id_csa103"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 81
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 500.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "irf3__nucleus__phosphorylated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa157"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa157"
      minerva_name "IRF3"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6118"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14653"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/Q14653"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000126456"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_001571"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IRF3"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IRF3"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/3661"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/3661"
      minerva_ref_type__resource1 "HGNC__6118"
      minerva_ref_type__resource2 "UNIPROT__Q14653"
      minerva_ref_type__resource3 "UNIPROT__Q14653"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000126456"
      minerva_ref_type__resource5 "REFSEQ__NM_001571"
      minerva_ref_type__resource6 "HGNC_SYMBOL__IRF3"
      minerva_ref_type__resource7 "HGNC_SYMBOL__IRF3"
      minerva_ref_type__resource8 "ENTREZ__3661"
      minerva_ref_type__resource9 "ENTREZ__3661"
      minerva_state1 "PHOSPHORYLATED"
      minerva_type "Protein"
      minerva_x 1670.0
      minerva_y 2335.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa157"
      species_meta_id "s_id_sa157"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 82
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "myd88__cell__activated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa77"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa77"
      minerva_name "MYD88"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4615"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_002468"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000172936"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/4615"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/pubmed/19366914"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/Q99836"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/Q99836"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MYD88"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MYD88"
      minerva_ref_link9 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7562"
      minerva_ref_type__resource1 "ENTREZ__4615"
      minerva_ref_type__resource10 "REFSEQ__NM_002468"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000172936"
      minerva_ref_type__resource3 "ENTREZ__4615"
      minerva_ref_type__resource4 "PUBMED__19366914"
      minerva_ref_type__resource5 "UNIPROT__Q99836"
      minerva_ref_type__resource6 "UNIPROT__Q99836"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MYD88"
      minerva_ref_type__resource8 "HGNC_SYMBOL__MYD88"
      minerva_ref_type__resource9 "HGNC__7562"
      minerva_structuralState "activated"
      minerva_type "Protein"
      minerva_x 780.0
      minerva_y 565.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa77"
      species_meta_id "s_id_sa77"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 83
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "map3k7__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa435"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa435"
      minerva_former_symbols "TAK1"
      minerva_name "MAP3K7"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/O43318"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.25"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/O43318"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000135341"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_145331"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6859"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/6885"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/6885"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP3K7"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP3K7"
      minerva_ref_type__resource1 "UNIPROT__O43318"
      minerva_ref_type__resource10 "EC__2.7.11.25"
      minerva_ref_type__resource2 "UNIPROT__O43318"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000135341"
      minerva_ref_type__resource4 "REFSEQ__NM_145331"
      minerva_ref_type__resource5 "HGNC__6859"
      minerva_ref_type__resource6 "ENTREZ__6885"
      minerva_ref_type__resource7 "ENTREZ__6885"
      minerva_ref_type__resource8 "HGNC_SYMBOL__MAP3K7"
      minerva_ref_type__resource9 "HGNC_SYMBOL__MAP3K7"
      minerva_type "Protein"
      minerva_x 954.0
      minerva_y 1185.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa435"
      species_meta_id "s_id_sa435"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 84
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 800.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "irf3__nucleus__phosphorylated__2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa256"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa256"
      minerva_homodimer "2"
      minerva_name "IRF3"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6118"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14653"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/Q14653"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000126456"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_001571"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IRF3"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IRF3"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/3661"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/3661"
      minerva_ref_type__resource1 "HGNC__6118"
      minerva_ref_type__resource2 "UNIPROT__Q14653"
      minerva_ref_type__resource3 "UNIPROT__Q14653"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000126456"
      minerva_ref_type__resource5 "REFSEQ__NM_001571"
      minerva_ref_type__resource6 "HGNC_SYMBOL__IRF3"
      minerva_ref_type__resource7 "HGNC_SYMBOL__IRF3"
      minerva_ref_type__resource8 "ENTREZ__3661"
      minerva_ref_type__resource9 "ENTREZ__3661"
      minerva_state1 "PHOSPHORYLATED"
      minerva_type "Protein"
      minerva_x 440.0
      minerva_y 2320.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa256"
      species_meta_id "s_id_sa256"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 85
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "map2k7__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa372"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa372"
      minerva_former_symbols "PRKMK7"
      minerva_name "MAP2K7"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.12.2"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP2K7"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/5609"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/5609"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/O14733"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/O14733"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000076984"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6847"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_001297555"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP2K7"
      minerva_ref_type__resource1 "EC__2.7.12.2"
      minerva_ref_type__resource10 "HGNC_SYMBOL__MAP2K7"
      minerva_ref_type__resource2 "ENTREZ__5609"
      minerva_ref_type__resource3 "ENTREZ__5609"
      minerva_ref_type__resource4 "UNIPROT__O14733"
      minerva_ref_type__resource5 "UNIPROT__O14733"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000076984"
      minerva_ref_type__resource7 "HGNC__6847"
      minerva_ref_type__resource8 "REFSEQ__NM_001297555"
      minerva_ref_type__resource9 "HGNC_SYMBOL__MAP2K7"
      minerva_type "Protein"
      minerva_x 1095.0
      minerva_y 1480.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa372"
      species_meta_id "s_id_sa372"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 86
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1000.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ikbke__cell__activated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa118"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa118"
      minerva_name "IKBKE"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/Q14164"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000263528"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q14164"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/9641"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/9641"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/14552"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.10"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IKBKE"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IKBKE"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_001193321"
      minerva_ref_type__resource1 "UNIPROT__Q14164"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000263528"
      minerva_ref_type__resource2 "UNIPROT__Q14164"
      minerva_ref_type__resource3 "ENTREZ__9641"
      minerva_ref_type__resource4 "ENTREZ__9641"
      minerva_ref_type__resource5 "HGNC__14552"
      minerva_ref_type__resource6 "EC__2.7.11.10"
      minerva_ref_type__resource7 "HGNC_SYMBOL__IKBKE"
      minerva_ref_type__resource8 "HGNC_SYMBOL__IKBKE"
      minerva_ref_type__resource9 "REFSEQ__NM_001193321"
      minerva_structuralState "activated"
      minerva_type "Protein"
      minerva_x 515.0
      minerva_y 940.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa118"
      species_meta_id "s_id_sa118"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 87
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "casp10__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa463"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa463"
      minerva_name "CASP10"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CASP10"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000003400"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_032977"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CASP10"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/Q92851"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/Q92851"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/843"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/843"
      minerva_ref_link8 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.22.63"
      minerva_ref_link9 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1500"
      minerva_ref_type__resource1 "HGNC_SYMBOL__CASP10"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000003400"
      minerva_ref_type__resource2 "REFSEQ__NM_032977"
      minerva_ref_type__resource3 "HGNC_SYMBOL__CASP10"
      minerva_ref_type__resource4 "UNIPROT__Q92851"
      minerva_ref_type__resource5 "UNIPROT__Q92851"
      minerva_ref_type__resource6 "ENTREZ__843"
      minerva_ref_type__resource7 "ENTREZ__843"
      minerva_ref_type__resource8 "EC__3.4.22.63"
      minerva_ref_type__resource9 "HGNC__1500"
      minerva_type "Protein"
      minerva_x 1435.0
      minerva_y 1545.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa463"
      species_meta_id "s_id_sa463"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 88
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1200.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s551__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa406"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa406"
      minerva_name "s551"
      minerva_type "Degraded"
      minerva_x 1110.0
      minerva_y 810.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa406"
      species_meta_id "s_id_sa406"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 89
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ddx58:dsrna__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa92"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa92"
      minerva_name "DDX58:dsRNA"
      minerva_type "Complex"
      minerva_x 1285.0
      minerva_y 345.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa92"
      species_meta_id "s_id_csa92"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 90
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ap1__cell__activated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa79"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa79"
      minerva_name "AP1"
      minerva_state1 "activated"
      minerva_type "Complex"
      minerva_x 945.0
      minerva_y 2165.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa79"
      species_meta_id "s_id_csa79"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 91
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ap1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa95"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa95"
      minerva_name "AP1"
      minerva_type "Complex"
      minerva_x 710.0
      minerva_y 2165.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa95"
      species_meta_id "s_id_csa95"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 92
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ticam1__cell__activated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa91"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa91"
      minerva_name "TICAM1"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TICAM1"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q8IUC6"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/Q8IUC6"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TICAM1"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000127666"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/18348"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_014261"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/148022"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/148022"
      minerva_ref_type__resource1 "HGNC_SYMBOL__TICAM1"
      minerva_ref_type__resource2 "UNIPROT__Q8IUC6"
      minerva_ref_type__resource3 "UNIPROT__Q8IUC6"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TICAM1"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000127666"
      minerva_ref_type__resource6 "HGNC__18348"
      minerva_ref_type__resource7 "REFSEQ__NM_014261"
      minerva_ref_type__resource8 "ENTREZ__148022"
      minerva_ref_type__resource9 "ENTREZ__148022"
      minerva_structuralState "activated"
      minerva_type "Protein"
      minerva_x 215.0
      minerva_y 535.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa91"
      species_meta_id "s_id_sa91"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 93
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "eif2ak2__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa454"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa454"
      minerva_former_symbols "PRKR"
      minerva_name "EIF2AK2"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/9437"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000055332"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/gene/5610"
      minerva_ref_link2 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.1"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.10.2"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=EIF2AK2"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=EIF2AK2"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P19525"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P19525"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_002759"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/5610"
      minerva_ref_type__resource1 "HGNC__9437"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000055332"
      minerva_ref_type__resource11 "ENTREZ__5610"
      minerva_ref_type__resource2 "EC__2.7.11.1"
      minerva_ref_type__resource3 "EC__2.7.10.2"
      minerva_ref_type__resource4 "HGNC_SYMBOL__EIF2AK2"
      minerva_ref_type__resource5 "HGNC_SYMBOL__EIF2AK2"
      minerva_ref_type__resource6 "UNIPROT__P19525"
      minerva_ref_type__resource7 "UNIPROT__P19525"
      minerva_ref_type__resource8 "REFSEQ__NM_002759"
      minerva_ref_type__resource9 "ENTREZ__5610"
      minerva_type "Protein"
      minerva_x 740.0
      minerva_y 1750.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa454"
      species_meta_id "s_id_sa454"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 94
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nfkb__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa80"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa80"
      minerva_name "NFKB"
      minerva_type "Complex"
      minerva_x 1435.0
      minerva_y 2150.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa80"
      species_meta_id "s_id_csa80"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 95
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 800.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tlr8__endosome__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa429"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa429"
      minerva_name "TLR8"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_016610"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q9NR97"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/Q9NR97"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/15632"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/51311"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/51311"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TLR8"
      minerva_ref_link8 "https://www.ensembl.org/id/ENSG00000101916"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TLR8"
      minerva_ref_type__resource1 "REFSEQ__NM_016610"
      minerva_ref_type__resource2 "UNIPROT__Q9NR97"
      minerva_ref_type__resource3 "UNIPROT__Q9NR97"
      minerva_ref_type__resource4 "HGNC__15632"
      minerva_ref_type__resource5 "ENTREZ__51311"
      minerva_ref_type__resource6 "ENTREZ__51311"
      minerva_ref_type__resource7 "HGNC_SYMBOL__TLR8"
      minerva_ref_type__resource8 "ENSEMBL__ENSG00000101916"
      minerva_ref_type__resource9 "HGNC_SYMBOL__TLR8"
      minerva_type "Protein"
      minerva_x 885.0
      minerva_y 190.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "endosome"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa429"
      species_meta_id "s_id_sa429"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 96
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "itch__cell__activated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa405"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa405"
      minerva_name "ITCH"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/Q96J02"
      minerva_ref_link10 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/13890"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q96J02"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.26"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ITCH"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ITCH"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000078747"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_001257137"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/83737"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/83737"
      minerva_ref_type__resource1 "UNIPROT__Q96J02"
      minerva_ref_type__resource10 "HGNC__13890"
      minerva_ref_type__resource2 "UNIPROT__Q96J02"
      minerva_ref_type__resource3 "EC__2.3.2.26"
      minerva_ref_type__resource4 "HGNC_SYMBOL__ITCH"
      minerva_ref_type__resource5 "HGNC_SYMBOL__ITCH"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000078747"
      minerva_ref_type__resource7 "REFSEQ__NM_001257137"
      minerva_ref_type__resource8 "ENTREZ__83737"
      minerva_ref_type__resource9 "ENTREZ__83737"
      minerva_structuralState "activated"
      minerva_type "Protein"
      minerva_x 1065.0
      minerva_y 655.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa405"
      species_meta_id "s_id_sa405"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 97
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1000.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "chuk__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa485"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa485"
      minerva_former_symbols "TCF16"
      minerva_name "CHUK"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CHUK"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/1147"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CHUK"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_001278"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.10"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000213341"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1974"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/O15111"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/O15111"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/1147"
      minerva_ref_type__resource1 "HGNC_SYMBOL__CHUK"
      minerva_ref_type__resource10 "ENTREZ__1147"
      minerva_ref_type__resource2 "HGNC_SYMBOL__CHUK"
      minerva_ref_type__resource3 "REFSEQ__NM_001278"
      minerva_ref_type__resource4 "EC__2.7.11.10"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000213341"
      minerva_ref_type__resource6 "HGNC__1974"
      minerva_ref_type__resource7 "UNIPROT__O15111"
      minerva_ref_type__resource8 "UNIPROT__O15111"
      minerva_ref_type__resource9 "ENTREZ__1147"
      minerva_type "Protein"
      minerva_x 1245.0
      minerva_y 1420.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa485"
      species_meta_id "s_id_sa485"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 98
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "map2k7__cell__phosphorylated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa453"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa453"
      minerva_former_symbols "PRKMK7"
      minerva_name "MAP2K7"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.12.2"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP2K7"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/5609"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/5609"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/O14733"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/O14733"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000076984"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6847"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_001297555"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP2K7"
      minerva_ref_type__resource1 "EC__2.7.12.2"
      minerva_ref_type__resource10 "HGNC_SYMBOL__MAP2K7"
      minerva_ref_type__resource2 "ENTREZ__5609"
      minerva_ref_type__resource3 "ENTREZ__5609"
      minerva_ref_type__resource4 "UNIPROT__O14733"
      minerva_ref_type__resource5 "UNIPROT__O14733"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000076984"
      minerva_ref_type__resource7 "HGNC__6847"
      minerva_ref_type__resource8 "REFSEQ__NM_001297555"
      minerva_ref_type__resource9 "HGNC_SYMBOL__MAP2K7"
      minerva_state1 "PHOSPHORYLATED"
      minerva_type "Protein"
      minerva_x 1095.0
      minerva_y 1615.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa453"
      species_meta_id "s_id_sa453"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 99
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1200.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ifih1:dsrna__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa93"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa93"
      minerva_name "IFIH1:dsRNA"
      minerva_type "Complex"
      minerva_x 1520.0
      minerva_y 347.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa93"
      species_meta_id "s_id_csa93"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 100
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nfkbia__cell__phosphorylated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa475"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa475"
      minerva_former_symbols "NFKBI"
      minerva_name "NFKBIA"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7797"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_020529"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000100906"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_type__resource1 "HGNC__7797"
      minerva_ref_type__resource2 "REFSEQ__NM_020529"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000100906"
      minerva_ref_type__resource4 "UNIPROT__P25963"
      minerva_ref_type__resource5 "UNIPROT__P25963"
      minerva_ref_type__resource6 "ENTREZ__4792"
      minerva_ref_type__resource7 "ENTREZ__4792"
      minerva_ref_type__resource8 "HGNC_SYMBOL__NFKBIA"
      minerva_ref_type__resource9 "HGNC_SYMBOL__NFKBIA"
      minerva_state1 "PHOSPHORYLATED"
      minerva_type "Protein"
      minerva_x 1220.0
      minerva_y 2150.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa475"
      species_meta_id "s_id_sa475"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 102
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "myd88__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa433"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa433"
      minerva_name "MYD88"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4615"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_002468"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000172936"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/4615"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/pubmed/19366914"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/Q99836"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/Q99836"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MYD88"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MYD88"
      minerva_ref_link9 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7562"
      minerva_ref_type__resource1 "ENTREZ__4615"
      minerva_ref_type__resource10 "REFSEQ__NM_002468"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000172936"
      minerva_ref_type__resource3 "ENTREZ__4615"
      minerva_ref_type__resource4 "PUBMED__19366914"
      minerva_ref_type__resource5 "UNIPROT__Q99836"
      minerva_ref_type__resource6 "UNIPROT__Q99836"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MYD88"
      minerva_ref_type__resource8 "HGNC_SYMBOL__MYD88"
      minerva_ref_type__resource9 "HGNC__7562"
      minerva_type "Protein"
      minerva_x 960.0
      minerva_y 500.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa433"
      species_meta_id "s_id_sa433"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 103
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ddx58__cell__ubiquitinated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa124"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa124"
      minerva_name "DDX58"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/23586"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DDX58"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/23586"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_014314"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000107201"
      minerva_ref_link5 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.6.4.13"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/O95786"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/O95786"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/19102"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DDX58"
      minerva_ref_type__resource1 "ENTREZ__23586"
      minerva_ref_type__resource10 "HGNC_SYMBOL__DDX58"
      minerva_ref_type__resource2 "ENTREZ__23586"
      minerva_ref_type__resource3 "REFSEQ__NM_014314"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000107201"
      minerva_ref_type__resource5 "EC__3.6.4.13"
      minerva_ref_type__resource6 "UNIPROT__O95786"
      minerva_ref_type__resource7 "UNIPROT__O95786"
      minerva_ref_type__resource8 "HGNC__19102"
      minerva_ref_type__resource9 "HGNC_SYMBOL__DDX58"
      minerva_state1 "UBIQUITINATED"
      minerva_type "Protein"
      minerva_x 1270.0
      minerva_y 605.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa124"
      species_meta_id "s_id_sa124"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 104
    zlevel -1

    graphics [
      x 600.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re242"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re242"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 1179.999999999999
      minerva_y 783.5000000000001
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re242"
      reaction_meta_id "re242"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 108
    zlevel -1

    graphics [
      x 700.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re277"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re277"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 1435.0
      minerva_y 1600.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re277"
      reaction_meta_id "re277"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 112
    zlevel -1

    graphics [
      x 800.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re276"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re276"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 1300.0
      minerva_y 1600.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re276"
      reaction_meta_id "re276"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 116
    zlevel -1

    graphics [
      x 900.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re76"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "Transport"
      minerva_x 1670.0
      minerva_y 2147.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re76"
      reaction_meta_id "re76"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 119
    zlevel -1

    graphics [
      x 1000.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re284"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re284"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "Trigger"
      minerva_x 1359.410447709344
      minerva_y 2474.640013730261
    ]
    sbml [
      reaction_id "re284"
      reaction_meta_id "re284"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 122
    zlevel -1

    graphics [
      x 1100.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re273"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re273"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 1095.0
      minerva_y 1547.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re273"
      reaction_meta_id "re273"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 126
    zlevel -1

    graphics [
      x 1200.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re288"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re288"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 1182.5
      minerva_y 1485.0
    ]
    sbml [
      reaction_id "re288"
      reaction_meta_id "re288"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 131
    zlevel -1

    graphics [
      x 200.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re271"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re271"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 900.0
      minerva_y 1547.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re271"
      reaction_meta_id "re271"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 135
    zlevel -1

    graphics [
      x 300.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re282"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re282"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "Trigger"
      minerva_x 945.0
      minerva_y 2421.875
    ]
    sbml [
      reaction_id "re282"
      reaction_meta_id "re282"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 138
    zlevel -1

    graphics [
      x 400.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re65"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 1415.0
      minerva_y 719.9999999999999
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re65"
      reaction_meta_id "re65"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 143
    zlevel -1

    graphics [
      x 500.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re254"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re254"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 515.0
      minerva_y 880.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re254"
      reaction_meta_id "re254"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 148
    zlevel -1

    graphics [
      x 600.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re183"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re183"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 1390.0
      minerva_y 1975.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re183"
      reaction_meta_id "re183"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 152
    zlevel -1

    graphics [
      x 700.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re285"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re285"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "Trigger"
      minerva_x 709.8471834016611
      minerva_y 2458.309407367339
    ]
    sbml [
      reaction_id "re285"
      reaction_meta_id "re285"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 155
    zlevel -1

    graphics [
      x 800.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re249"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re249"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 215.0
      minerva_y 485.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re249"
      reaction_meta_id "re249"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 159
    zlevel -1

    graphics [
      x 900.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re269"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re269"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 870.0
      minerva_y 1865.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re269"
      reaction_meta_id "re269"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 164
    zlevel -1

    graphics [
      x 1000.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re255"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re255"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 780.25
      minerva_y 714.875
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re255"
      reaction_meta_id "re255"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 170
    zlevel -1

    graphics [
      x 1100.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re267"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re267"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 827.5
      minerva_y 2165.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re267"
      reaction_meta_id "re267"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 175
    zlevel -1

    graphics [
      x 1200.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re135"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re135"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "Transport"
      minerva_x 576.0
      minerva_y 1883.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re135"
      reaction_meta_id "re135"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 178
    zlevel -1

    graphics [
      x 200.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re261"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re261"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 1255.0
      minerva_y 897.25
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re261"
      reaction_meta_id "re261"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 184
    zlevel -1

    graphics [
      x 300.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re134"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re134"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "Transport"
      minerva_x 438.5
      minerva_y 1881.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re134"
      reaction_meta_id "re134"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 187
    zlevel -1

    graphics [
      x 400.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re279"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re279"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 1205.0
      minerva_y 1770.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re279"
      reaction_meta_id "re279"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 194
    zlevel -1

    graphics [
      x 500.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re265"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re265"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 1030.0000000000002
      minerva_y 1035.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re265"
      reaction_meta_id "re265"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 198
    zlevel -1

    graphics [
      x 600.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re260"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re260"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 954.9999999999991
      minerva_y 1308.5000000000018
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re260"
      reaction_meta_id "re260"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 203
    zlevel -1

    graphics [
      x 700.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re290"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re290"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 1730.0
      minerva_y 1527.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re290"
      reaction_meta_id "re290"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 207
    zlevel -1

    graphics [
      x 800.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re275"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re275"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 1354.125
      minerva_y 1338.25
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re275"
      reaction_meta_id "re275"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 211
    zlevel -1

    graphics [
      x 900.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re272"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re272"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 1000.0
      minerva_y 1547.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re272"
      reaction_meta_id "re272"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 215
    zlevel -1

    graphics [
      x 1000.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re73"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 1134.0
      minerva_y 2150.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re73"
      reaction_meta_id "re73"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 219
    zlevel -1

    graphics [
      x 1100.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re286"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re286"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "Trigger"
      minerva_x 639.4910587648237
      minerva_y 2475.4015090235084
    ]
    sbml [
      reaction_id "re286"
      reaction_meta_id "re286"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 222
    zlevel -1

    graphics [
      x 1200.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re256"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re256"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 885.0
      minerva_y 277.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re256"
      reaction_meta_id "re256"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 226
    zlevel -1

    graphics [
      x 200.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re253"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re253"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 379.99999999999994
      minerva_y 940.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re253"
      reaction_meta_id "re253"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 230
    zlevel -1

    graphics [
      x 300.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re289"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re289"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 1590.0
      minerva_y 1240.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re289"
      reaction_meta_id "re289"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 236
    zlevel -1

    graphics [
      x 400.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re176"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re176"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 1010.0
      minerva_y 1080.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re176"
      reaction_meta_id "re176"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 242
    zlevel -1

    graphics [
      x 500.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re243"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re243"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 1110.0
      minerva_y 844.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re243"
      reaction_meta_id "re243"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 246
    zlevel -1

    graphics [
      x 600.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re270"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re270"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 805.0
      minerva_y 1547.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re270"
      reaction_meta_id "re270"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 251
    zlevel -1

    graphics [
      x 700.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re257"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re257"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 715.0
      minerva_y 278.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re257"
      reaction_meta_id "re257"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 255
    zlevel -1

    graphics [
      x 800.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re241"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re241"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 1264.0
      minerva_y 720.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re241"
      reaction_meta_id "re241"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 259
    zlevel -1

    graphics [
      x 900.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re291"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re291"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 525.0
      minerva_y 507.5
    ]
    sbml [
      reaction_id "re291"
      reaction_meta_id "re291"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 264
    zlevel -1

    graphics [
      x 1000.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re259"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re259"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 850.0
      minerva_y 500.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re259"
      reaction_meta_id "re259"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 269
    zlevel -1

    graphics [
      x 1100.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re14"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 1520.0
      minerva_y 211.25
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re14"
      reaction_meta_id "re14"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 276
    zlevel -1

    graphics [
      x 1200.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re274"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re274"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 665.0
      minerva_y 1718.75
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re274"
      reaction_meta_id "re274"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 281
    zlevel -1

    graphics [
      x 200.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re72"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "Transport"
      minerva_x 1435.0
      minerva_y 2247.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re72"
      reaction_meta_id "re72"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 284
    zlevel -1

    graphics [
      x 300.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re287"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re287"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 1469.9007971039719
      minerva_y 1821.2989487973823
    ]
    sbml [
      reaction_id "re287"
      reaction_meta_id "re287"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 289
    zlevel -1

    graphics [
      x 400.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re281"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re281"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "Transport"
      minerva_x 945.0
      minerva_y 2255.0
    ]
    sbml [
      reaction_id "re281"
      reaction_meta_id "re281"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 292
    zlevel -1

    graphics [
      x 500.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re61"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 1270.0
      minerva_y 525.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re61"
      reaction_meta_id "re61"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 299
    zlevel -1

    graphics [
      x 600.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re263"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re263"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 1450.0
      minerva_y 918.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re263"
      reaction_meta_id "re263"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 303
    zlevel -1

    graphics [
      x 700.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re262"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re262"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 1065.0
      minerva_y 615.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re262"
      reaction_meta_id "re262"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 307
    zlevel -1

    graphics [
      x 800.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re280"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re280"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "Dissociation"
      minerva_x 1310.0
      minerva_y 2101.25
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re280"
      reaction_meta_id "re280"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 312
    zlevel -1

    graphics [
      x 900.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re268"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re268"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 995.0
      minerva_y 1865.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re268"
      reaction_meta_id "re268"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 317
    zlevel -1

    graphics [
      x 1000.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re133"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re133"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 575.0
      minerva_y 1295.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re133"
      reaction_meta_id "re133"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 322
    zlevel -1

    graphics [
      x 1100.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re251"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re251"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 215.0
      minerva_y 585.25
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re251"
      reaction_meta_id "re251"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 327
    zlevel -1

    graphics [
      x 1200.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re75"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 1728.250000000002
      minerva_y 1722.7353222607571
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re75"
      reaction_meta_id "re75"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 331
    zlevel -1

    graphics [
      x 200.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re132"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 377.5
      minerva_y 1180.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re132"
      reaction_meta_id "re132"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 338
    zlevel -1

    graphics [
      x 300.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re252"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re252"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 380.00000000000006
      minerva_y 695.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re252"
      reaction_meta_id "re252"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 343
    zlevel -1

    graphics [
      x 400.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re283"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re283"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "Trigger"
      minerva_x 1241.9456432592501
      minerva_y 2457.110130881197
    ]
    sbml [
      reaction_id "re283"
      reaction_meta_id "re283"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 346
    zlevel -1

    graphics [
      x 500.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re13"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "State transition"
      minerva_x 1285.0
      minerva_y 210.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re13"
      reaction_meta_id "re13"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 352
    zlevel -1

    graphics [
      x 600.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re250"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re250"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_005082"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000121060"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7706"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM25"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12932"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q14258"
      minerva_ref_type__resource1 "ENTREZ__7706"
      minerva_ref_type__resource10 "REFSEQ__NM_005082"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000121060"
      minerva_ref_type__resource3 "ENTREZ__7706"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM25"
      minerva_ref_type__resource6 "EC__2.3.2.27"
      minerva_ref_type__resource7 "HGNC__12932"
      minerva_ref_type__resource8 "UNIPROT__Q14258"
      minerva_ref_type__resource9 "UNIPROT__Q14258"
      minerva_type "Heterodimer association"
      minerva_x 410.00000000000006
      minerva_y 275.671875
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re250"
      reaction_meta_id "re250"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 356
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 850.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "jnk__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa94"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa94"
      minerva_name "JNK"
      minerva_type "Complex"
      minerva_x 995.0
      minerva_y 1995.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa94"
      species_meta_id "s_id_csa94"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
    ]
  ]
  node [
    id 362
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 750.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nfkb:nfkbia__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa102"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa102"
      minerva_name "NFKB:NFKBIA"
      minerva_type "Complex"
      minerva_x 1310.0
      minerva_y 1975.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa102"
      species_meta_id "s_id_csa102"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
    ]
  ]
  edge [
    id 367
    source 50
    target 104
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa131"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 368
    source 104
    target 10
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa404"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 369
    source 96
    target 104
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR0"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa405"
    ]
  ]
  edge [
    id 370
    source 87
    target 108
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa463"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 371
    source 108
    target 59
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa389"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 372
    source 73
    target 108
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR1"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa84"
    ]
  ]
  edge [
    id 373
    source 33
    target 112
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa459"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 374
    source 112
    target 75
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa390"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 375
    source 73
    target 112
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR2"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa84"
    ]
  ]
  edge [
    id 376
    source 2
    target 116
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa119"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 377
    source 116
    target 81
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa157"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 378
    source 81
    target 119
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa157"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 379
    source 119
    target 67
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa480"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 380
    source 85
    target 122
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa372"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 381
    source 122
    target 98
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa453"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 382
    source 31
    target 122
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR3"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa367"
    ]
  ]
  edge [
    id 383
    source 39
    target 126
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa484"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 384
    source 97
    target 126
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa485"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 385
    source 72
    target 126
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa486"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 386
    source 126
    target 7
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa75"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 387
    source 12
    target 131
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa369"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 388
    source 131
    target 55
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa451"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 389
    source 31
    target 131
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR4"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa367"
    ]
  ]
  edge [
    id 390
    source 46
    target 135
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa104"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 391
    source 135
    target 67
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa480"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 392
    source 70
    target 138
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa127"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 393
    source 138
    target 19
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa128"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 394
    source 103
    target 138
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR5"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa124"
    ]
  ]
  edge [
    id 395
    source 99
    target 138
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR6"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa93"
    ]
  ]
  edge [
    id 396
    source 42
    target 143
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa491"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 397
    source 143
    target 86
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa118"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 398
    source 1
    target 143
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR7"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa82"
    ]
  ]
  edge [
    id 399
    source 9
    target 143
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR8"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa357"
    ]
  ]
  edge [
    id 400
    source 74
    target 148
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR9"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa99"
    ]
  ]
  edge [
    id 401
    source 45
    target 152
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa255"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 402
    source 152
    target 67
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa480"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 403
    source 78
    target 155
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa418"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 404
    source 155
    target 92
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa91"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 405
    source 64
    target 155
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR10"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa88"
    ]
  ]
  edge [
    id 406
    source 17
    target 159
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa445"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 407
    source 159
    target 4
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa368"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 408
    source 36
    target 159
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR11"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa450"
    ]
  ]
  edge [
    id 409
    source 55
    target 159
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR12"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa451"
    ]
  ]
  edge [
    id 410
    source 82
    target 164
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa77"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 411
    source 76
    target 164
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa80"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 412
    source 47
    target 164
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa333"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 413
    source 16
    target 164
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa422"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 414
    source 164
    target 29
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa56"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 415
    source 91
    target 170
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa95"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 416
    source 170
    target 90
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa79"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 417
    source 4
    target 170
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR13"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa368"
    ]
  ]
  edge [
    id 418
    source 49
    target 175
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa120"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 419
    source 175
    target 45
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa255"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 420
    source 61
    target 178
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa130"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 421
    source 50
    target 178
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa131"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 422
    source 16
    target 178
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa422"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 423
    source 19
    target 178
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa128"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 424
    source 178
    target 69
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa32"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 425
    source 2
    target 184
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa119"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 426
    source 184
    target 84
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa256"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 427
    source 7
    target 187
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa75"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 428
    source 187
    target 74
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa99"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 429
    source 35
    target 187
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR15"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa337"
    ]
  ]
  edge [
    id 430
    source 75
    target 187
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR16"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa390"
    ]
  ]
  edge [
    id 431
    source 34
    target 187
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR17"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa359"
    ]
  ]
  edge [
    id 432
    source 59
    target 187
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR18"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa389"
    ]
  ]
  edge [
    id 433
    source 35
    target 194
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa337"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 434
    source 194
    target 16
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa422"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 435
    source 9
    target 194
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR19"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa357"
    ]
  ]
  edge [
    id 436
    source 83
    target 198
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa435"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 437
    source 198
    target 31
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa367"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 438
    source 35
    target 198
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR20"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa337"
    ]
  ]
  edge [
    id 439
    source 13
    target 198
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR21"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa9"
    ]
  ]
  edge [
    id 440
    source 28
    target 203
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa426"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 441
    source 203
    target 15
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa154"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 442
    source 1
    target 203
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR22"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa82"
    ]
  ]
  edge [
    id 443
    source 56
    target 207
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa97"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 444
    source 207
    target 73
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa84"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 445
    source 26
    target 207
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR23"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa86"
    ]
  ]
  edge [
    id 446
    source 58
    target 211
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa371"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 447
    source 211
    target 79
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa452"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 448
    source 31
    target 211
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR24"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa367"
    ]
  ]
  edge [
    id 449
    source 100
    target 215
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa475"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 450
    source 215
    target 44
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa146"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 451
    source 77
    target 215
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR25"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa87"
    ]
  ]
  edge [
    id 452
    source 84
    target 219
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa256"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 453
    source 219
    target 67
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa480"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 454
    source 95
    target 222
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa429"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 455
    source 57
    target 222
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa14"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 456
    source 222
    target 43
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa91"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 457
    source 28
    target 226
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa426"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 458
    source 226
    target 15
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa154"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 459
    source 1
    target 226
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR26"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa82"
    ]
  ]
  edge [
    id 460
    source 52
    target 230
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa89"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 461
    source 230
    target 1
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa82"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 462
    source 26
    target 230
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR27"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa86"
    ]
  ]
  edge [
    id 463
    source 9
    target 230
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR28"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa357"
    ]
  ]
  edge [
    id 464
    source 34
    target 230
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR29"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa359"
    ]
  ]
  edge [
    id 465
    source 16
    target 236
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa422"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 466
    source 236
    target 35
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa337"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 467
    source 14
    target 236
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR30"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa331"
    ]
  ]
  edge [
    id 468
    source 29
    target 236
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR31"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa56"
    ]
  ]
  edge [
    id 469
    source 69
    target 236
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR32"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa32"
    ]
  ]
  edge [
    id 470
    source 16
    target 242
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa422"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 471
    source 242
    target 88
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa406"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 472
    source 96
    target 242
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR33"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa405"
    ]
  ]
  edge [
    id 473
    source 6
    target 246
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa370"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 474
    source 246
    target 36
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa450"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 475
    source 63
    target 246
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR34"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa96"
    ]
  ]
  edge [
    id 476
    source 31
    target 246
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR35"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa367"
    ]
  ]
  edge [
    id 477
    source 68
    target 251
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa430"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 478
    source 57
    target 251
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa14"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 479
    source 251
    target 38
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa90"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 480
    source 70
    target 255
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa127"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 481
    source 255
    target 24
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa407"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 482
    source 96
    target 255
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR36"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa405"
    ]
  ]
  edge [
    id 483
    source 18
    target 259
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa493"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 484
    source 53
    target 259
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa492"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 485
    source 42
    target 259
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa491"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 486
    source 259
    target 52
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa89"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 487
    source 102
    target 264
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa433"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 488
    source 264
    target 82
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa77"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 489
    source 38
    target 264
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR37"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa90"
    ]
  ]
  edge [
    id 490
    source 43
    target 264
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR38"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa91"
    ]
  ]
  edge [
    id 491
    source 60
    target 269
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa27"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 492
    source 62
    target 269
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa439"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 493
    source 269
    target 99
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa93"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 494
    source 34
    target 269
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR39"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa359"
    ]
  ]
  edge [
    id 495
    source 9
    target 269
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR40"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa357"
    ]
  ]
  edge [
    id 496
    source 21
    target 269
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR41"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa416"
    ]
  ]
  edge [
    id 497
    source 60
    target 276
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa27"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 498
    source 93
    target 276
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa454"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 499
    source 276
    target 63
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa96"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 500
    source 21
    target 276
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR42"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa416"
    ]
  ]
  edge [
    id 501
    source 94
    target 281
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa80"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 502
    source 281
    target 80
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa103"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 503
    source 51
    target 284
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa481"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 504
    source 37
    target 284
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa482"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 505
    source 71
    target 284
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa483"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 506
    source 90
    target 289
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa79"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 507
    source 289
    target 46
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa104"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 508
    source 20
    target 292
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa23"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 509
    source 292
    target 103
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa124"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 510
    source 48
    target 292
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR43"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa123"
    ]
  ]
  edge [
    id 511
    source 25
    target 292
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR44"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa122"
    ]
  ]
  edge [
    id 512
    source 3
    target 292
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR45"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa381"
    ]
  ]
  edge [
    id 513
    source 89
    target 292
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR46"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa92"
    ]
  ]
  edge [
    id 514
    source 19
    target 299
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa128"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 515
    source 66
    target 299
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa400"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 516
    source 299
    target 26
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa86"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 517
    source 65
    target 303
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa436"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 518
    source 303
    target 96
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa405"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 519
    source 27
    target 303
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR47"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa362"
    ]
  ]
  edge [
    id 520
    source 307
    target 94
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa80"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 521
    source 307
    target 100
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa475"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 522
    source 3
    target 307
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR48"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa381"
    ]
  ]
  edge [
    id 523
    source 98
    target 312
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR49"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa453"
    ]
  ]
  edge [
    id 524
    source 79
    target 312
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR50"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa452"
    ]
  ]
  edge [
    id 525
    source 41
    target 317
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa347"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 526
    source 317
    target 49
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa120"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 527
    source 86
    target 317
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR51"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa118"
    ]
  ]
  edge [
    id 528
    source 29
    target 317
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR52"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa56"
    ]
  ]
  edge [
    id 529
    source 92
    target 322
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa91"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 530
    source 16
    target 322
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa422"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 531
    source 78
    target 322
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa418"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 532
    source 322
    target 13
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa9"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 533
    source 30
    target 327
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa348"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 534
    source 327
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa119"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 535
    source 15
    target 327
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR53"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa154"
    ]
  ]
  edge [
    id 536
    source 30
    target 331
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa348"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 537
    source 331
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa119"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 538
    source 15
    target 331
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR54"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa154"
    ]
  ]
  edge [
    id 539
    source 9
    target 331
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR55"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa357"
    ]
  ]
  edge [
    id 540
    source 32
    target 331
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR56"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa356"
    ]
  ]
  edge [
    id 541
    source 11
    target 331
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR57"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa355"
    ]
  ]
  edge [
    id 542
    source 52
    target 338
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa89"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 543
    source 338
    target 1
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa82"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 544
    source 34
    target 338
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR58"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa359"
    ]
  ]
  edge [
    id 545
    source 13
    target 338
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR59"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa9"
    ]
  ]
  edge [
    id 546
    source 80
    target 343
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa103"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 547
    source 343
    target 67
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa480"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 548
    source 60
    target 346
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa27"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 549
    source 20
    target 346
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa23"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 550
    source 346
    target 89
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa92"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 551
    source 34
    target 346
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR60"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa359"
    ]
  ]
  edge [
    id 552
    source 9
    target 346
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR61"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa357"
    ]
  ]
  edge [
    id 553
    source 23
    target 352
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa5"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 554
    source 40
    target 352
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa6"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 555
    source 352
    target 64
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa88"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 556
    source 356
    target 170
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR14"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa94"
    ]
  ]
  edge [
    id 557
    source 312
    target 356
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa94"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 559
    source 148
    target 362
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa102"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 560
    source 362
    target 307
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa102"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 561
    source 284
    target 362
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa100"
      stoichiometry "1.0"
    ]
  ]
]
