# generated with VANTED V2.8.0 at Tue Apr 27 20:00:44 CEST 2021
graph [
  background_coloring "true"
  cluster_colors "255,128,128,255:0,0,0,255;191,255,128,255:0,0,0,255;128,255,255,255:0,0,0,255;191,128,255,255:0,0,0,255"
  clusterbackground_fill_outer_region "false"
  clusterbackground_grid 50.0
  clusterbackground_low_alpha 0.2
  clusterbackground_radius 200.0
  clusterbackground_space_fill "true"
  graphbackgroundcolor "#ffffff"
  sbml [
    model_meta_id "untitled"
    model_non_rdf_annotation ""
    model_notes ""
    model_sbml_id "untitled"
    namespace "xmlns:celldesigner=\"http://www.sbml.org/2001/ns/celldesigner\" "
    sbml_level "2"
    sbml_version "4"
  ]
  sbml_compartment_default [
    sbml_compartment_default_id "default"
    sbml_compartment_default_meta_id "default"
    sbml_compartment_default_non_rdf_annotation ""
    sbml_compartment_default_notes ""
    sbml_compartment_default_size "1.0"
    sbml_compartment_default_units "volume"
  ]
  sbml_compartment_s_id_ca1 [
    sbml_compartment_s_id_ca1_annotation ""
    sbml_compartment_s_id_ca1_id "s_id_ca1"
    sbml_compartment_s_id_ca1_meta_id "s_id_ca1"
    sbml_compartment_s_id_ca1_name "nucleus"
    sbml_compartment_s_id_ca1_non_rdf_annotation ""
    sbml_compartment_s_id_ca1_notes ""
    sbml_compartment_s_id_ca1_outside "s_id_ca2"
    sbml_compartment_s_id_ca1_size "1.0"
    sbml_compartment_s_id_ca1_units "volume"
  ]
  sbml_compartment_s_id_ca2 [
    sbml_compartment_s_id_ca2_annotation ""
    sbml_compartment_s_id_ca2_id "s_id_ca2"
    sbml_compartment_s_id_ca2_meta_id "s_id_ca2"
    sbml_compartment_s_id_ca2_name "cell"
    sbml_compartment_s_id_ca2_non_rdf_annotation ""
    sbml_compartment_s_id_ca2_notes ""
    sbml_compartment_s_id_ca2_outside "s_id_ca3"
    sbml_compartment_s_id_ca2_size "1.0"
    sbml_compartment_s_id_ca2_units "volume"
  ]
  sbml_compartment_s_id_ca3 [
    sbml_compartment_s_id_ca3_id "s_id_ca3"
    sbml_compartment_s_id_ca3_meta_id "s_id_ca3"
    sbml_compartment_s_id_ca3_name "human_space_host"
    sbml_compartment_s_id_ca3_non_rdf_annotation ""
    sbml_compartment_s_id_ca3_notes ""
    sbml_compartment_s_id_ca3_outside "default"
    sbml_compartment_s_id_ca3_size "1.0"
    sbml_compartment_s_id_ca3_units "volume"
  ]
  sbml_unit_definition_1 [
    sbml_unit_definition_1_id "area"
    sbml_unit_definition_1_name "area"
    sbml_unit_definition_1_sub_unit_1_ "(1.0 * 10^0 * metre)^2.0"
    sbml_unit_definition_1unit "(1.0 * 10^0 * metre)^2.0"
  ]
  sbml_unit_definition_2 [
    sbml_unit_definition_2_id "time"
    sbml_unit_definition_2_name "time"
    sbml_unit_definition_2_sub_unit_1_ "(1.0 * 10^0 * second)^1.0"
    sbml_unit_definition_2unit "(1.0 * 10^0 * second)^1.0"
  ]
  sbml_unit_definition_3 [
    sbml_unit_definition_3_id "length"
    sbml_unit_definition_3_name "length"
    sbml_unit_definition_3_sub_unit_1_ "(1.0 * 10^0 * metre)^1.0"
    sbml_unit_definition_3unit "(1.0 * 10^0 * metre)^1.0"
  ]
  sbml_unit_definition_4 [
    sbml_unit_definition_4_id "substance"
    sbml_unit_definition_4_name "substance"
    sbml_unit_definition_4_sub_unit_1_ "(1.0 * 10^0 * mole)^1.0"
    sbml_unit_definition_4unit "(1.0 * 10^0 * mole)^1.0"
  ]
  sbml_unit_definition_5 [
    sbml_unit_definition_5_id "volume"
    sbml_unit_definition_5_name "volume"
    sbml_unit_definition_5_sub_unit_1_ "(1.0 * 10^0 * litre)^1.0"
    sbml_unit_definition_5unit "(1.0 * 10^0 * litre)^1.0"
  ]
  directed 0
  node [
    id 1
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 200.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "crb3__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa34"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa34"
      minerva_elementId2 "sa29"
      minerva_name "CRB3"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/20237"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000130545"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/92359"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/92359"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/Q9BUF7"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/Q9BUF7"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CRB3"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CRB3"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_139161"
      minerva_ref_type__resource1 "HGNC__20237"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000130545"
      minerva_ref_type__resource3 "ENTREZ__92359"
      minerva_ref_type__resource4 "ENTREZ__92359"
      minerva_ref_type__resource5 "UNIPROT__Q9BUF7"
      minerva_ref_type__resource6 "UNIPROT__Q9BUF7"
      minerva_ref_type__resource7 "HGNC_SYMBOL__CRB3"
      minerva_ref_type__resource8 "HGNC_SYMBOL__CRB3"
      minerva_ref_type__resource9 "REFSEQ__NM_139161"
      minerva_type "Protein"
      minerva_x 577.5259740259744
      minerva_x2 223.0714285714289
      minerva_y 1254.0519480519483
      minerva_y2 1252.9610389610389
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa34"
      species_meta_id "s_id_sa34"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 2
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 300.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "csa6_underscore_degraded__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa31"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa31"
      minerva_name "csa6_degraded"
      minerva_type "Degraded"
      minerva_x 737.5
      minerva_y 1092.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa31"
      species_meta_id "s_id_sa31"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 3
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 400.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "na_plus___human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa67"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa67"
      minerva_fullName "sodium(1+)"
      minerva_name "Na+"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:29101"
      minerva_ref_type__resource1 "CHEBI__CHEBI:29101"
      minerva_synonyms "Na(+); Na(+); Na+; SODIUM ION"
      minerva_type "Ion"
      minerva_x 3125.5
      minerva_y 1480.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa67"
      species_meta_id "s_id_sa67"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 4
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cdk9__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa10"
      type "text"
    ]
    minerva [
      minerva_activity "true"
      minerva_elementId "sa10"
      minerva_elementId2 "sa15"
      minerva_former_symbols "CDC2L4"
      minerva_name "CDK9"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.23"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/1025"
      minerva_ref_link11 "https://www.ncbi.nlm.nih.gov/gene/1025"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1780"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000136807"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.22"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_001261"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P50750"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/P50750"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CDK9"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CDK9"
      minerva_ref_type__resource1 "EC__2.7.11.23"
      minerva_ref_type__resource10 "ENTREZ__1025"
      minerva_ref_type__resource11 "ENTREZ__1025"
      minerva_ref_type__resource2 "HGNC__1780"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000136807"
      minerva_ref_type__resource4 "EC__2.7.11.22"
      minerva_ref_type__resource5 "REFSEQ__NM_001261"
      minerva_ref_type__resource6 "UNIPROT__P50750"
      minerva_ref_type__resource7 "UNIPROT__P50750"
      minerva_ref_type__resource8 "HGNC_SYMBOL__CDK9"
      minerva_ref_type__resource9 "HGNC_SYMBOL__CDK9"
      minerva_type "Protein"
      minerva_x 1490.0
      minerva_x2 1665.0
      minerva_y 320.0
      minerva_y2 320.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa10"
      species_meta_id "s_id_sa10"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 5
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "h4c1__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2"
      minerva_former_symbols "H4FA; HIST1H4A"
      minerva_name "H4C1"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000278637"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/4781"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/8359"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P62805"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P62805"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=H4C1"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_003538"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=H4C1"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/121504"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000278637"
      minerva_ref_type__resource2 "HGNC__4781"
      minerva_ref_type__resource3 "ENTREZ__8359"
      minerva_ref_type__resource4 "UNIPROT__P62805"
      minerva_ref_type__resource5 "UNIPROT__P62805"
      minerva_ref_type__resource6 "HGNC_SYMBOL__H4C1"
      minerva_ref_type__resource7 "REFSEQ__NM_003538"
      minerva_ref_type__resource8 "HGNC_SYMBOL__H4C1"
      minerva_ref_type__resource9 "ENTREZ__121504"
      minerva_type "Protein"
      minerva_x 975.0
      minerva_y 447.5510204081611
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2"
      species_meta_id "s_id_sa2"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 6
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "h2a__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa6"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa6"
      minerva_name "H2A"
      minerva_ref_link1 "https://www.ebi.ac.uk/interpro/entry/IPR002119"
      minerva_ref_type__resource1 "INTERPRO__IPR002119"
      minerva_type "Protein"
      minerva_x 975.0
      minerva_y 663.673469387757
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa6"
      species_meta_id "s_id_sa6"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 7
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 800.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "brd2__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa20"
      type "text"
    ]
    minerva [
      minerva_activity "true"
      minerva_elementId "sa20"
      minerva_elementId2 "sa21"
      minerva_former_symbols "BRD2-IT1"
      minerva_name "BRD2"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_001113182"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1103"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P25440"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P25440"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/6046"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/6046"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000204256"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BRD2"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BRD2"
      minerva_ref_type__resource1 "REFSEQ__NM_001113182"
      minerva_ref_type__resource2 "HGNC__1103"
      minerva_ref_type__resource3 "UNIPROT__P25440"
      minerva_ref_type__resource4 "UNIPROT__P25440"
      minerva_ref_type__resource5 "ENTREZ__6046"
      minerva_ref_type__resource6 "ENTREZ__6046"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000204256"
      minerva_ref_type__resource8 "HGNC_SYMBOL__BRD2"
      minerva_ref_type__resource9 "HGNC_SYMBOL__BRD2"
      minerva_type "Protein"
      minerva_x 1243.5259740259744
      minerva_x2 1410.5259740259744
      minerva_y 736.1428571428569
      minerva_y2 737.1428571428569
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa20"
      species_meta_id "s_id_sa20"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 8
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "p_minus_tefb__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa8"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa8"
      minerva_name "P-TEFb"
      minerva_type "Complex"
      minerva_x 1849.4740259740256
      minerva_y 343.3571428571431
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa8"
      species_meta_id "s_id_csa8"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 9
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1000.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mpp5__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa30"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa35"
      minerva_elementId2 "sa30"
      minerva_name "MPP5"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/18669"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_022474"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000072415"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_type__resource1 "HGNC__18669"
      minerva_ref_type__resource2 "ENTREZ__64398"
      minerva_ref_type__resource3 "REFSEQ__NM_022474"
      minerva_ref_type__resource4 "ENTREZ__64398"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000072415"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource8 "UNIPROT__Q8N3R9"
      minerva_ref_type__resource9 "UNIPROT__Q8N3R9"
      minerva_type "Protein"
      minerva_x 483.5259740259744
      minerva_x2 320.0714285714289
      minerva_y 1255.0519480519483
      minerva_y2 1254.9610389610389
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa30"
      species_meta_id "s_id_sa30"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 10
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 1100.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "k_plus___human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa65"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa65"
      minerva_fullName "potassium(1+)"
      minerva_name "K+"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:29103"
      minerva_ref_type__resource1 "CHEBI__CHEBI:29103"
      minerva_synonyms "K(+); K(+); K+; POTASSIUM ION"
      minerva_type "Ion"
      minerva_x 3242.5
      minerva_y 1433.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa65"
      species_meta_id "s_id_sa65"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 11
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1200.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "brd4__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa16"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa16"
      minerva_elementId2 "sa12"
      minerva_name "BRD4"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/23476"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/23476"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/13575"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_058243"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BRD4"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/O60885"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/O60885"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BRD4"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000141867"
      minerva_ref_type__resource1 "ENTREZ__23476"
      minerva_ref_type__resource2 "ENTREZ__23476"
      minerva_ref_type__resource3 "HGNC__13575"
      minerva_ref_type__resource4 "REFSEQ__NM_058243"
      minerva_ref_type__resource5 "HGNC_SYMBOL__BRD4"
      minerva_ref_type__resource6 "UNIPROT__O60885"
      minerva_ref_type__resource7 "UNIPROT__O60885"
      minerva_ref_type__resource8 "HGNC_SYMBOL__BRD4"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000141867"
      minerva_type "Protein"
      minerva_x 1410.0259740259744
      minerva_x2 1271.0259740259744
      minerva_y 254.14285714285688
      minerva_y2 254.14285714285688
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa16"
      species_meta_id "s_id_sa16"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 12
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tbp__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa18"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa17"
      minerva_elementId2 "sa18"
      minerva_former_symbols "GTF2D1; SCA17"
      minerva_name "TBP"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000112592"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/6908"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/6908"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P20226"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P20226"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11588"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TBP"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TBP"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_003194"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000112592"
      minerva_ref_type__resource2 "ENTREZ__6908"
      minerva_ref_type__resource3 "ENTREZ__6908"
      minerva_ref_type__resource4 "UNIPROT__P20226"
      minerva_ref_type__resource5 "UNIPROT__P20226"
      minerva_ref_type__resource6 "HGNC__11588"
      minerva_ref_type__resource7 "HGNC_SYMBOL__TBP"
      minerva_ref_type__resource8 "HGNC_SYMBOL__TBP"
      minerva_ref_type__resource9 "REFSEQ__NM_003194"
      minerva_type "Protein"
      minerva_x 1630.0
      minerva_x2 1466.0
      minerva_y 795.0
      minerva_y2 795.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa18"
      species_meta_id "s_id_sa18"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 13
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "h3c15__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa3"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa3"
      minerva_former_symbols "HIST2H3A"
      minerva_name "H3C15"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=H3C15"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=H3C15"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/333932"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/126961"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/20505"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/Q71DI3"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/Q71DI3"
      minerva_ref_link8 "https://www.ensembl.org/id/ENSG00000203852"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_001005464"
      minerva_ref_type__resource1 "HGNC_SYMBOL__H3C15"
      minerva_ref_type__resource2 "HGNC_SYMBOL__H3C15"
      minerva_ref_type__resource3 "ENTREZ__333932"
      minerva_ref_type__resource4 "ENTREZ__126961"
      minerva_ref_type__resource5 "HGNC__20505"
      minerva_ref_type__resource6 "UNIPROT__Q71DI3"
      minerva_ref_type__resource7 "UNIPROT__Q71DI3"
      minerva_ref_type__resource8 "ENSEMBL__ENSG00000203852"
      minerva_ref_type__resource9 "REFSEQ__NM_001005464"
      minerva_type "Protein"
      minerva_x 975.0
      minerva_y 362.673469387757
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa3"
      species_meta_id "s_id_sa3"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 15
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 500.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "patj__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa28"
      type "text"
    ]
    minerva [
      minerva_activity "true"
      minerva_elementId "sa28"
      minerva_elementId2 "sa33"
      minerva_former_symbols "INADL"
      minerva_name "PATJ"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_170605"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/10207"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000132849"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/Q8NI35"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/Q8NI35"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PATJ"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PATJ"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/28881"
      minerva_ref_type__resource1 "REFSEQ__NM_170605"
      minerva_ref_type__resource2 "ENTREZ__10207"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000132849"
      minerva_ref_type__resource4 "UNIPROT__Q8NI35"
      minerva_ref_type__resource5 "UNIPROT__Q8NI35"
      minerva_ref_type__resource6 "HGNC_SYMBOL__PATJ"
      minerva_ref_type__resource7 "HGNC_SYMBOL__PATJ"
      minerva_ref_type__resource8 "HGNC__28881"
      minerva_type "Protein"
      minerva_x 129.0714285714289
      minerva_x2 669.5259740259744
      minerva_y 1252.9610389610389
      minerva_y2 1256.0519480519483
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa28"
      species_meta_id "s_id_sa28"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 16
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 600.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "k_plus___cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa66"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa66"
      minerva_fullName "potassium(1+)"
      minerva_name "K+"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:29103"
      minerva_ref_type__resource1 "CHEBI__CHEBI:29103"
      minerva_synonyms "K(+); K(+); K+; POTASSIUM ION"
      minerva_type "Ion"
      minerva_x 3242.5
      minerva_y 1122.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa66"
      species_meta_id "s_id_sa66"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 17
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 700.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "maintenance_space_of_space_tight_space_junction__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa74"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa74"
      minerva_name "Maintenance of tight junction"
      minerva_type "Phenotype"
      minerva_x 952.5
      minerva_y 1447.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa74"
      species_meta_id "s_id_sa74"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 18
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 800.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "e__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa32"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa22"
      minerva_elementId2 "sa32"
      minerva_name "E"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/BCD58755"
      minerva_ref_type__resource1 "NCBI_PROTEIN__BCD58755"
      minerva_ref_type__resource2 "UNIPROT__E"
      minerva_type "Protein"
      minerva_x 2437.5
      minerva_x2 217.5259740259744
      minerva_y 1097.5
      minerva_y2 1093.0519480519479
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa32"
      species_meta_id "s_id_sa32"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 19
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "h4c9__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa5"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa5"
      minerva_former_symbols "H4FM; HIST1H4I"
      minerva_name "H4C9"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/4793"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8294"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000276180"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=H4C9"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003495"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P62805"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/P62805"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=H4C1"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/121504"
      minerva_ref_type__resource1 "HGNC__4793"
      minerva_ref_type__resource2 "ENTREZ__8294"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000276180"
      minerva_ref_type__resource4 "HGNC_SYMBOL__H4C9"
      minerva_ref_type__resource5 "REFSEQ__NM_003495"
      minerva_ref_type__resource6 "UNIPROT__P62805"
      minerva_ref_type__resource7 "UNIPROT__P62805"
      minerva_ref_type__resource8 "HGNC_SYMBOL__H4C1"
      minerva_ref_type__resource9 "ENTREZ__121504"
      minerva_type "Protein"
      minerva_x 975.0
      minerva_y 577.2244897959195
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa5"
      species_meta_id "s_id_sa5"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 20
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1000.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "h4_minus_16__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa8"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa8"
      minerva_former_symbols "HIST4H4"
      minerva_name "H4-16"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000197837"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/20510"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_175054"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=H4-16"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P62805"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P62805"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=H4C1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/121504"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/121504"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000197837"
      minerva_ref_type__resource2 "HGNC__20510"
      minerva_ref_type__resource3 "REFSEQ__NM_175054"
      minerva_ref_type__resource4 "HGNC_SYMBOL__H4-16"
      minerva_ref_type__resource5 "UNIPROT__P62805"
      minerva_ref_type__resource6 "UNIPROT__P62805"
      minerva_ref_type__resource7 "HGNC_SYMBOL__H4C1"
      minerva_ref_type__resource8 "ENTREZ__121504"
      minerva_ref_type__resource9 "ENTREZ__121504"
      minerva_type "Protein"
      minerva_x 975.0
      minerva_y 490.77551020408055
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa8"
      species_meta_id "s_id_sa8"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 21
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1100.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "crb3:pals1:patj_space_complex__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa7"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa7"
      minerva_name "CRB3:PALS1:PATJ complex"
      minerva_type "Complex"
      minerva_x 883.5
      minerva_y 1257.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa7"
      species_meta_id "s_id_csa7"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 22
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1200.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "stoml3__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa24"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa24"
      minerva_name "STOML3"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000133115"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/161003"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/161003"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/19420"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=STOML3"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/Q8TAV4"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/Q8TAV4"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=STOML3"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_001144033"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000133115"
      minerva_ref_type__resource2 "ENTREZ__161003"
      minerva_ref_type__resource3 "ENTREZ__161003"
      minerva_ref_type__resource4 "HGNC__19420"
      minerva_ref_type__resource5 "HGNC_SYMBOL__STOML3"
      minerva_ref_type__resource6 "UNIPROT__Q8TAV4"
      minerva_ref_type__resource7 "UNIPROT__Q8TAV4"
      minerva_ref_type__resource8 "HGNC_SYMBOL__STOML3"
      minerva_ref_type__resource9 "REFSEQ__NM_001144033"
      minerva_type "Protein"
      minerva_x 2022.5259740259744
      minerva_y 1254.1428571428573
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa24"
      species_meta_id "s_id_sa24"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 23
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 200.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "na_plus___cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa68"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa68"
      minerva_fullName "sodium(1+)"
      minerva_name "Na+"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:29101"
      minerva_ref_type__resource1 "CHEBI__CHEBI:29101"
      minerva_synonyms "Na(+); Na(+); Na+; SODIUM ION"
      minerva_type "Ion"
      minerva_x 3127.5
      minerva_y 1187.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa68"
      species_meta_id "s_id_sa68"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 24
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "jq_minus_1__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa79"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa79"
      minerva_name "JQ-1"
      minerva_ref_link1 "https://pubchem.ncbi.nlm.nih.gov/compound/46907787"
      minerva_ref_type__resource1 "PUBCHEM__46907787"
      minerva_type "Simple molecule"
      minerva_x 1270.0
      minerva_y 691.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa79"
      species_meta_id "s_id_sa79"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 25
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "e__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa19"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa19"
      minerva_elementId2 "sa13"
      minerva_name "E"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/BCD58755"
      minerva_ref_type__resource1 "NCBI_PROTEIN__BCD58755"
      minerva_ref_type__resource2 "UNIPROT__E"
      minerva_type "Protein"
      minerva_x 1280.0
      minerva_x2 1339.0259740259744
      minerva_y 785.0
      minerva_y2 183.14285714285688
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa19"
      species_meta_id "s_id_sa19"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 26
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "h3c1__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa1"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa1"
      minerva_former_symbols "H3FA; HIST1H3A"
      minerva_name "H3C1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/8350"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P68431"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P68431"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/8350"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/4766"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_003529"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=H3C1"
      minerva_ref_link8 "https://www.ensembl.org/id/ENSG00000275714"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=H3C1"
      minerva_ref_type__resource1 "ENTREZ__8350"
      minerva_ref_type__resource2 "UNIPROT__P68431"
      minerva_ref_type__resource3 "UNIPROT__P68431"
      minerva_ref_type__resource4 "ENTREZ__8350"
      minerva_ref_type__resource5 "HGNC__4766"
      minerva_ref_type__resource6 "REFSEQ__NM_003529"
      minerva_ref_type__resource7 "HGNC_SYMBOL__H3C1"
      minerva_ref_type__resource8 "ENSEMBL__ENSG00000275714"
      minerva_ref_type__resource9 "HGNC_SYMBOL__H3C1"
      minerva_type "Protein"
      minerva_x 975.0
      minerva_y 404.32653061224164
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa1"
      species_meta_id "s_id_sa1"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 27
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ccnt1__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa11"
      type "text"
    ]
    minerva [
      minerva_activity "true"
      minerva_elementId "sa11"
      minerva_elementId2 "sa14"
      minerva_former_symbols "HIVE1"
      minerva_name "CCNT1"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CCNT1"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CCNT1"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_001240"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000129315"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1599"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/904"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/904"
      minerva_ref_link8 "https://purl.uniprot.org/uniprot/O60563"
      minerva_ref_link9 "https://purl.uniprot.org/uniprot/O60563"
      minerva_ref_type__resource1 "HGNC_SYMBOL__CCNT1"
      minerva_ref_type__resource2 "HGNC_SYMBOL__CCNT1"
      minerva_ref_type__resource3 "REFSEQ__NM_001240"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000129315"
      minerva_ref_type__resource5 "HGNC__1599"
      minerva_ref_type__resource6 "ENTREZ__904"
      minerva_ref_type__resource7 "ENTREZ__904"
      minerva_ref_type__resource8 "UNIPROT__O60563"
      minerva_ref_type__resource9 "UNIPROT__O60563"
      minerva_type "Protein"
      minerva_x 1501.0
      minerva_x2 1678.0
      minerva_y 371.0
      minerva_y2 371.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa11"
      species_meta_id "s_id_sa11"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 28
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 700.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "asic1__cell__none__3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa27"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa27"
      minerva_former_symbols "ACCN2"
      minerva_homodimer "3"
      minerva_name "ASIC1"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ASIC1"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ASIC1"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000110881"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P78348"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P78348"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/41"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/41"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_020039"
      minerva_ref_link9 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/100"
      minerva_ref_type__resource1 "HGNC_SYMBOL__ASIC1"
      minerva_ref_type__resource2 "HGNC_SYMBOL__ASIC1"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000110881"
      minerva_ref_type__resource4 "UNIPROT__P78348"
      minerva_ref_type__resource5 "UNIPROT__P78348"
      minerva_ref_type__resource6 "ENTREZ__41"
      minerva_ref_type__resource7 "ENTREZ__41"
      minerva_ref_type__resource8 "REFSEQ__NM_020039"
      minerva_ref_type__resource9 "HGNC__100"
      minerva_type "Protein"
      minerva_x 1709.5259740259744
      minerva_y 1244.1428571428573
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa27"
      species_meta_id "s_id_sa27"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 29
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 800.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "activity_space_of_space_sodium_space_channels__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa73"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa73"
      minerva_name "Activity of sodium channels"
      minerva_type "Phenotype"
      minerva_x 2607.5
      minerva_y 1482.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa73"
      species_meta_id "s_id_sa73"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 30
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "rna_space_polymerase_space_ii_minus_dependent_space_transcription_space___nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa77"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa77"
      minerva_name "RNA Polymerase II-dependent Transcription "
      minerva_type "Phenotype"
      minerva_x 1945.0
      minerva_y 480.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa77"
      species_meta_id "s_id_sa77"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 31
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1000.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "asic1_space_trimer:h_plus_:stoml3__cell__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa4"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa4"
      minerva_name "ASIC1 trimer:H+:STOML3"
      minerva_state1 "active"
      minerva_type "Complex"
      minerva_x 2214.5259740259744
      minerva_y 1260.6428571428573
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa4"
      species_meta_id "s_id_csa4"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 32
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1100.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "e_minus_pals1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa3"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa3"
      minerva_name "E-PALS1"
      minerva_type "Complex"
      minerva_x 617.5
      minerva_y 1092.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa3"
      species_meta_id "s_id_csa3"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 33
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1200.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "atp1a:atp1b:fxyds__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa6"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa6"
      minerva_name "ATP1A:ATP1B:FXYDs"
      minerva_type "Complex"
      minerva_x 2611.5
      minerva_y 1279.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa6"
      species_meta_id "s_id_csa6"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 34
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 200.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "asic1__cell__h+__3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa25"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa25"
      minerva_former_symbols "ACCN2"
      minerva_homodimer "3"
      minerva_name "ASIC1"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ASIC1"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ASIC1"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000110881"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P78348"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P78348"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/41"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/41"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_020039"
      minerva_ref_link9 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/100"
      minerva_ref_type__resource1 "HGNC_SYMBOL__ASIC1"
      minerva_ref_type__resource2 "HGNC_SYMBOL__ASIC1"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000110881"
      minerva_ref_type__resource4 "UNIPROT__P78348"
      minerva_ref_type__resource5 "UNIPROT__P78348"
      minerva_ref_type__resource6 "ENTREZ__41"
      minerva_ref_type__resource7 "ENTREZ__41"
      minerva_ref_type__resource8 "REFSEQ__NM_020039"
      minerva_ref_type__resource9 "HGNC__100"
      minerva_structuralState "H+"
      minerva_type "Protein"
      minerva_x 1904.071428571429
      minerva_y 1247.3246753246754
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa25"
      species_meta_id "s_id_sa25"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 35
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "h2bc21__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa7"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa7"
      minerva_former_symbols "H2B; H2BFQ; HIST2H2BE"
      minerva_name "H2BC21"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/4760"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=H2BC21"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=H2BC21"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000184678"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003528"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/Q16778"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/Q16778"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/8349"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/8349"
      minerva_ref_type__resource1 "HGNC__4760"
      minerva_ref_type__resource2 "HGNC_SYMBOL__H2BC21"
      minerva_ref_type__resource3 "HGNC_SYMBOL__H2BC21"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000184678"
      minerva_ref_type__resource5 "REFSEQ__NM_003528"
      minerva_ref_type__resource6 "UNIPROT__Q16778"
      minerva_ref_type__resource7 "UNIPROT__Q16778"
      minerva_ref_type__resource8 "ENTREZ__8349"
      minerva_ref_type__resource9 "ENTREZ__8349"
      minerva_type "Protein"
      minerva_x 975.0
      minerva_y 620.4489795918389
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa7"
      species_meta_id "s_id_sa7"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 36
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 400.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "h_plus___human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa26"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa26"
      minerva_fullName "hydron"
      minerva_name "H+"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15378"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15378"
      minerva_synonyms "H(+); H+; Hydron"
      minerva_type "Ion"
      minerva_x 1722.5259740259744
      minerva_y 1302.1428571428573
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa26"
      species_meta_id "s_id_sa26"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 37
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "h4c14__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa9"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa9"
      minerva_former_symbols "H4/n; H4F2; H4FN; HIST2H4; HIST2H4A"
      minerva_name "H4C14"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/4794"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/8370"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000270882"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/P62805"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P62805"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_003548"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=H4C1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/121504"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=H4C14"
      minerva_ref_type__resource1 "HGNC__4794"
      minerva_ref_type__resource2 "ENTREZ__8370"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000270882"
      minerva_ref_type__resource4 "UNIPROT__P62805"
      minerva_ref_type__resource5 "UNIPROT__P62805"
      minerva_ref_type__resource6 "REFSEQ__NM_003548"
      minerva_ref_type__resource7 "HGNC_SYMBOL__H4C1"
      minerva_ref_type__resource8 "ENTREZ__121504"
      minerva_ref_type__resource9 "HGNC_SYMBOL__H4C14"
      minerva_type "Protein"
      minerva_x 975.0
      minerva_y 534.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa9"
      species_meta_id "s_id_sa9"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 38
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "chromatin_space_organization__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa78"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa78"
      minerva_name "Chromatin organization"
      minerva_type "Phenotype"
      minerva_x 1685.0
      minerva_y 705.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa78"
      species_meta_id "s_id_sa78"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 40
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 800.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "atp1a:atp1b:fxyds__cell__active__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa10"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa10"
      minerva_name "ATP1A:ATP1B:FXYDs"
      minerva_state1 "active"
      minerva_type "Complex"
      minerva_x 2947.5
      minerva_y 1278.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa10"
      species_meta_id "s_id_csa10"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 41
    zlevel -1

    graphics [
      x 900.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re14"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/18669"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_022474"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000072415"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_type__resource1 "HGNC__18669"
      minerva_ref_type__resource2 "ENTREZ__64398"
      minerva_ref_type__resource3 "REFSEQ__NM_022474"
      minerva_ref_type__resource4 "ENTREZ__64398"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000072415"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource8 "UNIPROT__Q8N3R9"
      minerva_ref_type__resource9 "UNIPROT__Q8N3R9"
      minerva_type "State transition"
      minerva_x 577.5259740259746
      minerva_y 1217.5064935064956
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re14"
      reaction_meta_id "re14"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 44
    zlevel -1

    graphics [
      x 1000.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re17"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/18669"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_022474"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000072415"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_type__resource1 "HGNC__18669"
      minerva_ref_type__resource2 "ENTREZ__64398"
      minerva_ref_type__resource3 "REFSEQ__NM_022474"
      minerva_ref_type__resource4 "ENTREZ__64398"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000072415"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource8 "UNIPROT__Q8N3R9"
      minerva_ref_type__resource9 "UNIPROT__Q8N3R9"
      minerva_type "Heterodimer association"
      minerva_x 1819.8866302613098
      minerva_y 1243.8825590707202
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re17"
      reaction_meta_id "re17"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 48
    zlevel -1

    graphics [
      x 1100.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re34"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/18669"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_022474"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000072415"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_type__resource1 "HGNC__18669"
      minerva_ref_type__resource2 "ENTREZ__64398"
      minerva_ref_type__resource3 "REFSEQ__NM_022474"
      minerva_ref_type__resource4 "ENTREZ__64398"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000072415"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource8 "UNIPROT__Q8N3R9"
      minerva_ref_type__resource9 "UNIPROT__Q8N3R9"
      minerva_type "Trigger"
      minerva_x 2706.8664468260513
      minerva_y 1377.3911788953012
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re34"
      reaction_meta_id "re34"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 51
    zlevel -1

    graphics [
      x 1200.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re32"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/18669"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_022474"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000072415"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_type__resource1 "HGNC__18669"
      minerva_ref_type__resource2 "ENTREZ__64398"
      minerva_ref_type__resource3 "REFSEQ__NM_022474"
      minerva_ref_type__resource4 "ENTREZ__64398"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000072415"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource8 "UNIPROT__Q8N3R9"
      minerva_ref_type__resource9 "UNIPROT__Q8N3R9"
      minerva_type "Physical stimulation"
      minerva_x 952.0
      minerva_y 1350.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re32"
      reaction_meta_id "re32"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 54
    zlevel -1

    graphics [
      x 200.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re2"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/18669"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_022474"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000072415"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_type__resource1 "HGNC__18669"
      minerva_ref_type__resource2 "ENTREZ__64398"
      minerva_ref_type__resource3 "REFSEQ__NM_022474"
      minerva_ref_type__resource4 "ENTREZ__64398"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000072415"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource8 "UNIPROT__Q8N3R9"
      minerva_ref_type__resource9 "UNIPROT__Q8N3R9"
      minerva_type "State transition"
      minerva_x 1589.5
      minerva_y 371.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re2"
      reaction_meta_id "re2"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 58
    zlevel -1

    graphics [
      x 300.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re15"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/18669"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_022474"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000072415"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_type__resource1 "HGNC__18669"
      minerva_ref_type__resource2 "ENTREZ__64398"
      minerva_ref_type__resource3 "REFSEQ__NM_022474"
      minerva_ref_type__resource4 "ENTREZ__64398"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000072415"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource8 "UNIPROT__Q8N3R9"
      minerva_ref_type__resource9 "UNIPROT__Q8N3R9"
      minerva_type "State transition"
      minerva_x 669.5259740259746
      minerva_y 1218.5064935064947
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re15"
      reaction_meta_id "re15"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 61
    zlevel -1

    graphics [
      x 400.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re20"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/18669"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_022474"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000072415"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_type__resource1 "HGNC__18669"
      minerva_ref_type__resource2 "ENTREZ__64398"
      minerva_ref_type__resource3 "REFSEQ__NM_022474"
      minerva_ref_type__resource4 "ENTREZ__64398"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000072415"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource8 "UNIPROT__Q8N3R9"
      minerva_ref_type__resource9 "UNIPROT__Q8N3R9"
      minerva_type "State transition"
      minerva_x 3126.5
      minerva_y 1334.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re20"
      reaction_meta_id "re20"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 65
    zlevel -1

    graphics [
      x 500.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re5"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/18669"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_022474"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000072415"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_type__resource1 "HGNC__18669"
      minerva_ref_type__resource2 "ENTREZ__64398"
      minerva_ref_type__resource3 "REFSEQ__NM_022474"
      minerva_ref_type__resource4 "ENTREZ__64398"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000072415"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource8 "UNIPROT__Q8N3R9"
      minerva_ref_type__resource9 "UNIPROT__Q8N3R9"
      minerva_type "State transition"
      minerva_x 1128.544372127372
      minerva_y 535.1017047145426
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re5"
      reaction_meta_id "re5"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 75
    zlevel -1

    graphics [
      x 600.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re35"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/18669"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_022474"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000072415"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_type__resource1 "HGNC__18669"
      minerva_ref_type__resource2 "ENTREZ__64398"
      minerva_ref_type__resource3 "REFSEQ__NM_022474"
      minerva_ref_type__resource4 "ENTREZ__64398"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000072415"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource8 "UNIPROT__Q8N3R9"
      minerva_ref_type__resource9 "UNIPROT__Q8N3R9"
      minerva_type "Negative influence"
      minerva_x 2172.4771616571365
      minerva_y 1482.4483839554364
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re35"
      reaction_meta_id "re35"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 78
    zlevel -1

    graphics [
      x 700.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re13"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/18669"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_022474"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000072415"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_type__resource1 "HGNC__18669"
      minerva_ref_type__resource2 "ENTREZ__64398"
      minerva_ref_type__resource3 "REFSEQ__NM_022474"
      minerva_ref_type__resource4 "ENTREZ__64398"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000072415"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource8 "UNIPROT__Q8N3R9"
      minerva_ref_type__resource9 "UNIPROT__Q8N3R9"
      minerva_type "State transition"
      minerva_x 483.5259740259743
      minerva_y 1221.275974025974
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re13"
      reaction_meta_id "re13"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 81
    zlevel -1

    graphics [
      x 800.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re3"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/18669"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_022474"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000072415"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_type__resource1 "HGNC__18669"
      minerva_ref_type__resource2 "ENTREZ__64398"
      minerva_ref_type__resource3 "REFSEQ__NM_022474"
      minerva_ref_type__resource4 "ENTREZ__64398"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000072415"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource8 "UNIPROT__Q8N3R9"
      minerva_ref_type__resource9 "UNIPROT__Q8N3R9"
      minerva_type "State transition"
      minerva_x 1410.0259740259744
      minerva_y 584.6428571428569
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re3"
      reaction_meta_id "re3"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 86
    zlevel -1

    graphics [
      x 900.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re30"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/18669"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_022474"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000072415"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_type__resource1 "HGNC__18669"
      minerva_ref_type__resource2 "ENTREZ__64398"
      minerva_ref_type__resource3 "REFSEQ__NM_022474"
      minerva_ref_type__resource4 "ENTREZ__64398"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000072415"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource8 "UNIPROT__Q8N3R9"
      minerva_ref_type__resource9 "UNIPROT__Q8N3R9"
      minerva_type "Trigger"
      minerva_x 1664.1379870129872
      minerva_y 657.0714285714284
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re30"
      reaction_meta_id "re30"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 89
    zlevel -1

    graphics [
      x 1000.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re1"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/18669"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_022474"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000072415"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_type__resource1 "HGNC__18669"
      minerva_ref_type__resource2 "ENTREZ__64398"
      minerva_ref_type__resource3 "REFSEQ__NM_022474"
      minerva_ref_type__resource4 "ENTREZ__64398"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000072415"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource8 "UNIPROT__Q8N3R9"
      minerva_ref_type__resource9 "UNIPROT__Q8N3R9"
      minerva_type "State transition"
      minerva_x 1577.5
      minerva_y 320.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re1"
      reaction_meta_id "re1"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 93
    zlevel -1

    graphics [
      x 1100.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re4"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/18669"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_022474"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000072415"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_type__resource1 "HGNC__18669"
      minerva_ref_type__resource2 "ENTREZ__64398"
      minerva_ref_type__resource3 "REFSEQ__NM_022474"
      minerva_ref_type__resource4 "ENTREZ__64398"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000072415"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource8 "UNIPROT__Q8N3R9"
      minerva_ref_type__resource9 "UNIPROT__Q8N3R9"
      minerva_type "State transition"
      minerva_x 1340.525974025974
      minerva_y 254.14285714285694
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re4"
      reaction_meta_id "re4"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 97
    zlevel -1

    graphics [
      x 1200.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re11"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/18669"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_022474"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000072415"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_type__resource1 "HGNC__18669"
      minerva_ref_type__resource2 "ENTREZ__64398"
      minerva_ref_type__resource3 "REFSEQ__NM_022474"
      minerva_ref_type__resource4 "ENTREZ__64398"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000072415"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource8 "UNIPROT__Q8N3R9"
      minerva_ref_type__resource9 "UNIPROT__Q8N3R9"
      minerva_type "Heterodimer association"
      minerva_x 444.0129870129882
      minerva_y 1092.8214285714291
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re11"
      reaction_meta_id "re11"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 101
    zlevel -1

    graphics [
      x 200.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re6"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/18669"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_022474"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000072415"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_type__resource1 "HGNC__18669"
      minerva_ref_type__resource2 "ENTREZ__64398"
      minerva_ref_type__resource3 "REFSEQ__NM_022474"
      minerva_ref_type__resource4 "ENTREZ__64398"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000072415"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource8 "UNIPROT__Q8N3R9"
      minerva_ref_type__resource9 "UNIPROT__Q8N3R9"
      minerva_type "State transition"
      minerva_x 1327.0259740259744
      minerva_y 736.6428571428569
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re6"
      reaction_meta_id "re6"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 106
    zlevel -1

    graphics [
      x 300.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re16"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/18669"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_022474"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000072415"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_type__resource1 "HGNC__18669"
      minerva_ref_type__resource2 "ENTREZ__64398"
      minerva_ref_type__resource3 "REFSEQ__NM_022474"
      minerva_ref_type__resource4 "ENTREZ__64398"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000072415"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource8 "UNIPROT__Q8N3R9"
      minerva_ref_type__resource9 "UNIPROT__Q8N3R9"
      minerva_type "State transition"
      minerva_x 2779.5
      minerva_y 1223.75
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re16"
      reaction_meta_id "re16"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 110
    zlevel -1

    graphics [
      x 400.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re33"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/18669"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_022474"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000072415"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_type__resource1 "HGNC__18669"
      minerva_ref_type__resource2 "ENTREZ__64398"
      minerva_ref_type__resource3 "REFSEQ__NM_022474"
      minerva_ref_type__resource4 "ENTREZ__64398"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000072415"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource8 "UNIPROT__Q8N3R9"
      minerva_ref_type__resource9 "UNIPROT__Q8N3R9"
      minerva_type "Unknown negative influence"
      minerva_x 1038.4794132624475
      minerva_y 1265.1464397569441
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re33"
      reaction_meta_id "re33"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 113
    zlevel -1

    graphics [
      x 500.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re31"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/18669"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_022474"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000072415"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_type__resource1 "HGNC__18669"
      minerva_ref_type__resource2 "ENTREZ__64398"
      minerva_ref_type__resource3 "REFSEQ__NM_022474"
      minerva_ref_type__resource4 "ENTREZ__64398"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000072415"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource8 "UNIPROT__Q8N3R9"
      minerva_ref_type__resource9 "UNIPROT__Q8N3R9"
      minerva_type "State transition"
      minerva_x 649.4612417704491
      minerva_y 1325.1427138297522
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re31"
      reaction_meta_id "re31"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 118
    zlevel -1

    graphics [
      x 600.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re7"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/18669"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_022474"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000072415"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_type__resource1 "HGNC__18669"
      minerva_ref_type__resource2 "ENTREZ__64398"
      minerva_ref_type__resource3 "REFSEQ__NM_022474"
      minerva_ref_type__resource4 "ENTREZ__64398"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000072415"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource8 "UNIPROT__Q8N3R9"
      minerva_ref_type__resource9 "UNIPROT__Q8N3R9"
      minerva_type "State transition"
      minerva_x 1548.0
      minerva_y 795.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re7"
      reaction_meta_id "re7"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 122
    zlevel -1

    graphics [
      x 700.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re29"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/18669"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_022474"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000072415"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_type__resource1 "HGNC__18669"
      minerva_ref_type__resource2 "ENTREZ__64398"
      minerva_ref_type__resource3 "REFSEQ__NM_022474"
      minerva_ref_type__resource4 "ENTREZ__64398"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000072415"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource8 "UNIPROT__Q8N3R9"
      minerva_ref_type__resource9 "UNIPROT__Q8N3R9"
      minerva_type "Trigger"
      minerva_x 1922.2370129870128
      minerva_y 429.17857142857156
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re29"
      reaction_meta_id "re29"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 125
    zlevel -1

    graphics [
      x 800.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re19"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/18669"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_022474"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000072415"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_type__resource1 "HGNC__18669"
      minerva_ref_type__resource2 "ENTREZ__64398"
      minerva_ref_type__resource3 "REFSEQ__NM_022474"
      minerva_ref_type__resource4 "ENTREZ__64398"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000072415"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource8 "UNIPROT__Q8N3R9"
      minerva_ref_type__resource9 "UNIPROT__Q8N3R9"
      minerva_type "State transition"
      minerva_x 3242.5
      minerva_y 1278.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re19"
      reaction_meta_id "re19"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 129
    zlevel -1

    graphics [
      x 900.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re36"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/18669"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_022474"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000072415"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_type__resource1 "HGNC__18669"
      minerva_ref_type__resource2 "ENTREZ__64398"
      minerva_ref_type__resource3 "REFSEQ__NM_022474"
      minerva_ref_type__resource4 "ENTREZ__64398"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000072415"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource8 "UNIPROT__Q8N3R9"
      minerva_ref_type__resource9 "UNIPROT__Q8N3R9"
      minerva_type "Trigger"
      minerva_x 2508.8368067472866
      minerva_y 1359.7676655685204
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re36"
      reaction_meta_id "re36"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 132
    zlevel -1

    graphics [
      x 1000.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re28"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/18669"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_022474"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000072415"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_type__resource1 "HGNC__18669"
      minerva_ref_type__resource2 "ENTREZ__64398"
      minerva_ref_type__resource3 "REFSEQ__NM_022474"
      minerva_ref_type__resource4 "ENTREZ__64398"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000072415"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource8 "UNIPROT__Q8N3R9"
      minerva_ref_type__resource9 "UNIPROT__Q8N3R9"
      minerva_type "Heterodimer association"
      minerva_x 1773.7240259740256
      minerva_y 343.10714285714334
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re28"
      reaction_meta_id "re28"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 136
    zlevel -1

    graphics [
      x 1100.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re18"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/18669"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_022474"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000072415"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_type__resource1 "HGNC__18669"
      minerva_ref_type__resource2 "ENTREZ__64398"
      minerva_ref_type__resource3 "REFSEQ__NM_022474"
      minerva_ref_type__resource4 "ENTREZ__64398"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000072415"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource8 "UNIPROT__Q8N3R9"
      minerva_ref_type__resource9 "UNIPROT__Q8N3R9"
      minerva_type "Heterodimer association"
      minerva_x 2086.782872221266
      minerva_y 1159.349575919963
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re18"
      reaction_meta_id "re18"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 141
    zlevel -1

    graphics [
      x 1200.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re12"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/18669"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_022474"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/64398"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000072415"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MPP5"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q8N3R9"
      minerva_ref_type__resource1 "HGNC__18669"
      minerva_ref_type__resource2 "ENTREZ__64398"
      minerva_ref_type__resource3 "REFSEQ__NM_022474"
      minerva_ref_type__resource4 "ENTREZ__64398"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000072415"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MPP5"
      minerva_ref_type__resource8 "UNIPROT__Q8N3R9"
      minerva_ref_type__resource9 "UNIPROT__Q8N3R9"
      minerva_type "State transition"
      minerva_x 698.5
      minerva_y 1092.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re12"
      reaction_meta_id "re12"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 144
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 550.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "histone__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa1"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa1"
      minerva_name "histone"
      minerva_type "Complex"
      minerva_x 1551.0259740259744
      minerva_y 534.1428571428569
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa1"
      species_meta_id "s_id_csa1"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
    ]
  ]
  edge [
    id 149
    source 1
    target 41
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa34"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 151
    source 28
    target 44
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa27"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 152
    source 36
    target 44
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa26"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 153
    source 44
    target 34
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa25"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 154
    source 40
    target 48
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa10"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 155
    source 48
    target 29
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa73"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 156
    source 21
    target 51
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa7"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 157
    source 51
    target 17
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa74"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 158
    source 27
    target 54
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa11"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 160
    source 11
    target 54
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR0"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa16"
    ]
  ]
  edge [
    id 161
    source 15
    target 58
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa28"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 163
    source 23
    target 61
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa68"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 164
    source 61
    target 3
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa67"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 165
    source 40
    target 61
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR1"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa10"
    ]
  ]
  edge [
    id 166
    source 37
    target 65
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa9"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 167
    source 20
    target 65
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa8"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 168
    source 5
    target 65
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 169
    source 13
    target 65
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa3"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 170
    source 26
    target 65
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa1"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 171
    source 19
    target 65
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa5"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 172
    source 35
    target 65
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa7"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 173
    source 6
    target 65
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa6"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 174
    source 22
    target 75
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa24"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 175
    source 75
    target 29
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa73"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 176
    source 9
    target 78
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa30"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 178
    source 11
    target 81
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR2"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa16"
    ]
  ]
  edge [
    id 179
    source 7
    target 81
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR3"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa20"
    ]
  ]
  edge [
    id 180
    source 86
    target 38
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa78"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 181
    source 4
    target 89
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa10"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 183
    source 11
    target 89
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR4"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa16"
    ]
  ]
  edge [
    id 184
    source 11
    target 93
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa16"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 186
    source 25
    target 93
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR5"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa19"
    ]
  ]
  edge [
    id 187
    source 18
    target 97
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa32"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 188
    source 9
    target 97
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa30"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 189
    source 97
    target 32
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa3"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 190
    source 7
    target 101
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa20"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 192
    source 25
    target 101
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR6"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa19"
    ]
  ]
  edge [
    id 193
    source 24
    target 101
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR7"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa79"
    ]
  ]
  edge [
    id 194
    source 33
    target 106
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa6"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 195
    source 106
    target 40
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa10"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 196
    source 18
    target 106
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR8"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa32"
    ]
  ]
  edge [
    id 197
    source 32
    target 110
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa3"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 198
    source 110
    target 17
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa74"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 199
    source 9
    target 113
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa30"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 200
    source 1
    target 113
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa34"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 201
    source 15
    target 113
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa28"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 202
    source 113
    target 21
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa7"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 203
    source 12
    target 118
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa18"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 205
    source 7
    target 118
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR9"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa20"
    ]
  ]
  edge [
    id 206
    source 8
    target 122
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa8"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 207
    source 122
    target 30
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa77"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 208
    source 10
    target 125
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa65"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 209
    source 125
    target 16
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa66"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 210
    source 40
    target 125
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR10"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa10"
    ]
  ]
  edge [
    id 211
    source 31
    target 129
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa4"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 212
    source 129
    target 29
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa73"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 213
    source 4
    target 132
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa10"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 214
    source 27
    target 132
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa11"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 215
    source 132
    target 8
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa8"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 216
    source 34
    target 136
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa25"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 217
    source 22
    target 136
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa24"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 218
    source 136
    target 31
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa4"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 219
    source 18
    target 136
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR11"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa32"
    ]
  ]
  edge [
    id 220
    source 32
    target 141
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa3"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 221
    source 141
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa31"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 222
    source 81
    target 144
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa1"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 223
    source 144
    target 86
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa1"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 224
    source 65
    target 144
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa2"
      stoichiometry "1.0"
    ]
  ]
]
