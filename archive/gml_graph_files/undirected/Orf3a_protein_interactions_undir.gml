# generated with VANTED V2.8.0 at Tue Apr 27 20:00:47 CEST 2021
graph [
  background_coloring "true"
  cluster_colors "255,128,128,255:0,0,0,255;191,255,128,255:0,0,0,255;128,255,255,255:0,0,0,255;191,128,255,255:0,0,0,255"
  clusterbackground_fill_outer_region "false"
  clusterbackground_grid 50.0
  clusterbackground_low_alpha 0.2
  clusterbackground_radius 200.0
  clusterbackground_space_fill "true"
  graphbackgroundcolor "#ffffff"
  sbml [
    model_meta_id "Hops_Complex"
    model_non_rdf_annotation ""
    model_notes ""
    model_sbml_id "Hops_Complex"
    namespace "xmlns:celldesigner=\"http://www.sbml.org/2001/ns/celldesigner\" "
    sbml_level "2"
    sbml_version "4"
  ]
  sbml_compartment_default [
    sbml_compartment_default_id "default"
    sbml_compartment_default_meta_id "default"
    sbml_compartment_default_non_rdf_annotation ""
    sbml_compartment_default_notes ""
    sbml_compartment_default_size "1.0"
    sbml_compartment_default_units "volume"
  ]
  sbml_compartment_s_id_ca1 [
    sbml_compartment_s_id_ca1_annotation ""
    sbml_compartment_s_id_ca1_id "s_id_ca1"
    sbml_compartment_s_id_ca1_meta_id "s_id_ca1"
    sbml_compartment_s_id_ca1_name "cell"
    sbml_compartment_s_id_ca1_non_rdf_annotation ""
    sbml_compartment_s_id_ca1_notes ""
    sbml_compartment_s_id_ca1_outside "default"
    sbml_compartment_s_id_ca1_size "1.0"
    sbml_compartment_s_id_ca1_units "volume"
  ]
  sbml_compartment_s_id_ca2 [
    sbml_compartment_s_id_ca2_annotation ""
    sbml_compartment_s_id_ca2_id "s_id_ca2"
    sbml_compartment_s_id_ca2_meta_id "s_id_ca2"
    sbml_compartment_s_id_ca2_name "nucleus"
    sbml_compartment_s_id_ca2_non_rdf_annotation ""
    sbml_compartment_s_id_ca2_notes ""
    sbml_compartment_s_id_ca2_outside "s_id_ca1"
    sbml_compartment_s_id_ca2_size "1.0"
    sbml_compartment_s_id_ca2_units "volume"
  ]
  sbml_compartment_s_id_ca3 [
    sbml_compartment_s_id_ca3_annotation ""
    sbml_compartment_s_id_ca3_id "s_id_ca3"
    sbml_compartment_s_id_ca3_meta_id "s_id_ca3"
    sbml_compartment_s_id_ca3_name "endosome"
    sbml_compartment_s_id_ca3_non_rdf_annotation ""
    sbml_compartment_s_id_ca3_notes ""
    sbml_compartment_s_id_ca3_outside "s_id_ca1"
    sbml_compartment_s_id_ca3_size "1.0"
    sbml_compartment_s_id_ca3_units "volume"
  ]
  sbml_unit_definition_1 [
    sbml_unit_definition_1_id "volume"
    sbml_unit_definition_1_name "volume"
    sbml_unit_definition_1_sub_unit_1_ "(1.0 * 10^0 * litre)^1.0"
    sbml_unit_definition_1unit "(1.0 * 10^0 * litre)^1.0"
  ]
  sbml_unit_definition_2 [
    sbml_unit_definition_2_id "length"
    sbml_unit_definition_2_name "length"
    sbml_unit_definition_2_sub_unit_1_ "(1.0 * 10^0 * metre)^1.0"
    sbml_unit_definition_2unit "(1.0 * 10^0 * metre)^1.0"
  ]
  sbml_unit_definition_3 [
    sbml_unit_definition_3_id "substance"
    sbml_unit_definition_3_name "substance"
    sbml_unit_definition_3_sub_unit_1_ "(1.0 * 10^0 * mole)^1.0"
    sbml_unit_definition_3unit "(1.0 * 10^0 * mole)^1.0"
  ]
  sbml_unit_definition_4 [
    sbml_unit_definition_4_id "time"
    sbml_unit_definition_4_name "time"
    sbml_unit_definition_4_sub_unit_1_ "(1.0 * 10^0 * second)^1.0"
    sbml_unit_definition_4unit "(1.0 * 10^0 * second)^1.0"
  ]
  sbml_unit_definition_5 [
    sbml_unit_definition_5_id "area"
    sbml_unit_definition_5_name "area"
    sbml_unit_definition_5_sub_unit_1_ "(1.0 * 10^0 * metre)^2.0"
    sbml_unit_definition_5unit "(1.0 * 10^0 * metre)^2.0"
  ]
  directed 0
  node [
    id 1
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "traf6__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa101"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa38"
      minerva_elementId2 "sa101"
      minerva_elementId3 "sa135"
      minerva_name "TRAF6"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRAF6"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/7189"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7189"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000175104"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/Q9Y4K3"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/Q9Y4K3"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12036"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_145803"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRAF6"
      minerva_ref_type__resource1 "EC__2.3.2.27"
      minerva_ref_type__resource10 "HGNC_SYMBOL__TRAF6"
      minerva_ref_type__resource2 "ENTREZ__7189"
      minerva_ref_type__resource3 "ENTREZ__7189"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000175104"
      minerva_ref_type__resource5 "UNIPROT__Q9Y4K3"
      minerva_ref_type__resource6 "UNIPROT__Q9Y4K3"
      minerva_ref_type__resource7 "HGNC__12036"
      minerva_ref_type__resource8 "REFSEQ__NM_145803"
      minerva_ref_type__resource9 "HGNC_SYMBOL__TRAF6"
      minerva_type "Protein"
      minerva_x 967.0
      minerva_x2 965.0
      minerva_x3 965.6666666666667
      minerva_y 847.0
      minerva_y2 643.0
      minerva_y3 333.00000000000006
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa101"
      species_meta_id "s_id_sa101"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 2
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tlr4__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa142"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa142"
      minerva_name "TLR4"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_138554"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000136869"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/7099"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7099"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.2.2.6"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11850"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/O00206"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/O00206"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TLR4"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TLR4"
      minerva_ref_type__resource1 "REFSEQ__NM_138554"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000136869"
      minerva_ref_type__resource2 "ENTREZ__7099"
      minerva_ref_type__resource3 "ENTREZ__7099"
      minerva_ref_type__resource4 "EC__3.2.2.6"
      minerva_ref_type__resource5 "HGNC__11850"
      minerva_ref_type__resource6 "UNIPROT__O00206"
      minerva_ref_type__resource7 "UNIPROT__O00206"
      minerva_ref_type__resource8 "HGNC_SYMBOL__TLR4"
      minerva_ref_type__resource9 "HGNC_SYMBOL__TLR4"
      minerva_type "Protein"
      minerva_x 625.0
      minerva_y 184.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa142"
      species_meta_id "s_id_sa142"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 3
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 400.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ifnb1__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa88"
      type "text"
    ]
    minerva [
      minerva_activity "true"
      minerva_elementId "sa88"
      minerva_elementId2 "sa147"
      minerva_former_symbols "IFNB"
      minerva_name "IFNB1"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000171855"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IFNB1"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IFNB1"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/5434"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P01574"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P01574"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_002176"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/3456"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/3456"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000171855"
      minerva_ref_type__resource2 "HGNC_SYMBOL__IFNB1"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IFNB1"
      minerva_ref_type__resource4 "HGNC__5434"
      minerva_ref_type__resource5 "UNIPROT__P01574"
      minerva_ref_type__resource6 "UNIPROT__P01574"
      minerva_ref_type__resource7 "REFSEQ__NM_002176"
      minerva_ref_type__resource8 "ENTREZ__3456"
      minerva_ref_type__resource9 "ENTREZ__3456"
      minerva_type "Protein"
      minerva_x 820.0
      minerva_x2 955.0
      minerva_y 2140.0
      minerva_y2 2140.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa88"
      species_meta_id "s_id_sa88"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 4
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "p65_slash_p015__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa6"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa6"
      minerva_name "P65/P015"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/9955"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7794"
      minerva_ref_type__resource1 "HGNC__9955"
      minerva_ref_type__resource2 "HGNC__7794"
      minerva_type "Complex"
      minerva_x 347.0
      minerva_y 1343.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa6"
      species_meta_id "s_id_csa6"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 5
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nemo_slash_ikka_slash_ikkb__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa12"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa12"
      minerva_elementId2 "csa20"
      minerva_name "NEMO/IKKA/IKKB"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/5960"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/5961"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1974"
      minerva_ref_type__resource1 "HGNC__5960"
      minerva_ref_type__resource2 "HGNC__5961"
      minerva_ref_type__resource3 "HGNC__1974"
      minerva_type "Complex"
      minerva_x 1258.0
      minerva_x2 1259.0
      minerva_y 1526.5
      minerva_y2 1211.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa12"
      species_meta_id "s_id_csa12"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 6
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "btrc__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa137"
      type "text"
    ]
    minerva [
      minerva_activity "true"
      minerva_elementId "sa137"
      minerva_elementId2 "sa75"
      minerva_name "BTRC"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000166167"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/Q9Y297"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/Q9Y297"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1144"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BTRC"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BTRC"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/8945"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/8945"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_033637"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000166167"
      minerva_ref_type__resource2 "UNIPROT__Q9Y297"
      minerva_ref_type__resource3 "UNIPROT__Q9Y297"
      minerva_ref_type__resource4 "HGNC__1144"
      minerva_ref_type__resource5 "HGNC_SYMBOL__BTRC"
      minerva_ref_type__resource6 "HGNC_SYMBOL__BTRC"
      minerva_ref_type__resource7 "ENTREZ__8945"
      minerva_ref_type__resource8 "ENTREZ__8945"
      minerva_ref_type__resource9 "REFSEQ__NM_033637"
      minerva_type "Protein"
      minerva_x 1542.6666666666667
      minerva_x2 1360.0
      minerva_y 1815.0
      minerva_y2 1816.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa137"
      species_meta_id "s_id_sa137"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 7
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 800.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nfkb1:nfknia__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa21"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa21"
      minerva_name "NFKB1:NFKNIA"
      minerva_type "Complex"
      minerva_x 1189.0
      minerva_y 2059.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa21"
      species_meta_id "s_id_csa21"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 8
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "trim38__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa52"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa52"
      minerva_elementId2 "sa50"
      minerva_former_symbols "RNF15"
      minerva_name "TRIM38"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM38"
      minerva_ref_link10 "https://purl.uniprot.org/uniprot/O00635"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM38"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/10475"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/10475"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000112343"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_006355"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/10059"
      minerva_ref_link9 "https://purl.uniprot.org/uniprot/O00635"
      minerva_ref_type__resource1 "HGNC_SYMBOL__TRIM38"
      minerva_ref_type__resource10 "UNIPROT__O00635"
      minerva_ref_type__resource2 "HGNC_SYMBOL__TRIM38"
      minerva_ref_type__resource3 "EC__2.3.2.27"
      minerva_ref_type__resource4 "ENTREZ__10475"
      minerva_ref_type__resource5 "ENTREZ__10475"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000112343"
      minerva_ref_type__resource7 "REFSEQ__NM_006355"
      minerva_ref_type__resource8 "HGNC__10059"
      minerva_ref_type__resource9 "UNIPROT__O00635"
      minerva_type "Protein"
      minerva_x 878.0
      minerva_x2 1017.0
      minerva_y 743.0
      minerva_y2 394.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa52"
      species_meta_id "s_id_sa52"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 9
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1000.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ikbkg_slash_ikbkb_slash_chuk__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa22"
      type "text"
    ]
    minerva [
      minerva_activity "true"
      minerva_elementId "csa22"
      minerva_elementId2 "csa4"
      minerva_name "IKBKG/IKBKB/CHUK"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/5960"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/5961"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1974"
      minerva_ref_type__resource1 "HGNC__5960"
      minerva_ref_type__resource2 "HGNC__5961"
      minerva_ref_type__resource3 "HGNC__1974"
      minerva_type "Complex"
      minerva_x 466.17647058823536
      minerva_x2 467.5
      minerva_y 343.0882352941177
      minerva_y2 706.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa22"
      species_meta_id "s_id_csa22"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 10
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "e__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa83"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa90"
      minerva_elementId2 "sa83"
      minerva_name "E"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/1796318600"
      minerva_ref_type__resource1 "UNIPROT__E"
      minerva_ref_type__resource2 "NCBI_PROTEIN__1796318600"
      minerva_type "Protein"
      minerva_x 766.0
      minerva_x2 527.0
      minerva_y 1805.0
      minerva_y2 1371.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa83"
      species_meta_id "s_id_sa83"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 11
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1200.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "traf3__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa116"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa11"
      minerva_elementId2 "sa116"
      minerva_name "TRAF3"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRAF3"
      minerva_ref_link10 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12033"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000131323"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRAF3"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/7187"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/7187"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_145725"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/Q13114"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q13114"
      minerva_ref_type__resource1 "HGNC_SYMBOL__TRAF3"
      minerva_ref_type__resource10 "HGNC__12033"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000131323"
      minerva_ref_type__resource3 "HGNC_SYMBOL__TRAF3"
      minerva_ref_type__resource4 "EC__2.3.2.27"
      minerva_ref_type__resource5 "ENTREZ__7187"
      minerva_ref_type__resource6 "ENTREZ__7187"
      minerva_ref_type__resource7 "REFSEQ__NM_145725"
      minerva_ref_type__resource8 "UNIPROT__Q13114"
      minerva_ref_type__resource9 "UNIPROT__Q13114"
      minerva_type "Protein"
      minerva_x 666.0
      minerva_x2 665.7058823529412
      minerva_y 924.0
      minerva_y2 705.5882352941176
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa116"
      species_meta_id "s_id_sa116"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 12
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nlrp3__cell__none__3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa10"
      type "text"
    ]
    minerva [
      minerva_activity "true"
      minerva_elementId "sa10"
      minerva_former_symbols "C1orf7; CIAS1; DFNA34"
      minerva_homodimer "3"
      minerva_name "NLRP3"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/16400"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_004895"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/Q96P20"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/Q96P20"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000162711"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NLRP3"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NLRP3"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/114548"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/114548"
      minerva_ref_type__resource1 "HGNC__16400"
      minerva_ref_type__resource2 "REFSEQ__NM_004895"
      minerva_ref_type__resource3 "UNIPROT__Q96P20"
      minerva_ref_type__resource4 "UNIPROT__Q96P20"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000162711"
      minerva_ref_type__resource6 "HGNC_SYMBOL__NLRP3"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NLRP3"
      minerva_ref_type__resource8 "ENTREZ__114548"
      minerva_ref_type__resource9 "ENTREZ__114548"
      minerva_type "Protein"
      minerva_x 791.0
      minerva_y 1227.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa10"
      species_meta_id "s_id_sa10"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 13
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "lps_slash_tnf_space__alpha__slash_il_minus_1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa3"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa3"
      minerva_name "LPS/TNF α/IL-1"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/5991"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6121"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11916"
      minerva_ref_type__resource1 "HGNC__5991"
      minerva_ref_type__resource2 "HGNC__6121"
      minerva_ref_type__resource3 "HGNC__11916"
      minerva_type "Complex"
      minerva_x 469.5
      minerva_y 112.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa3"
      species_meta_id "s_id_csa3"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 14
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s197__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa140"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa140"
      minerva_name "s197"
      minerva_type "Degraded"
      minerva_x 312.0
      minerva_y 895.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa140"
      species_meta_id "s_id_sa140"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 15
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ticam1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa94"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa48"
      minerva_elementId2 "sa94"
      minerva_name "TICAM1"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TICAM1"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q8IUC6"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/Q8IUC6"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TICAM1"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000127666"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/18348"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_014261"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/148022"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/148022"
      minerva_ref_type__resource1 "HGNC_SYMBOL__TICAM1"
      minerva_ref_type__resource2 "UNIPROT__Q8IUC6"
      minerva_ref_type__resource3 "UNIPROT__Q8IUC6"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TICAM1"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000127666"
      minerva_ref_type__resource6 "HGNC__18348"
      minerva_ref_type__resource7 "REFSEQ__NM_014261"
      minerva_ref_type__resource8 "ENTREZ__148022"
      minerva_ref_type__resource9 "ENTREZ__148022"
      minerva_type "Protein"
      minerva_x 1071.0
      minerva_x2 1072.0
      minerva_y 550.0
      minerva_y2 333.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa94"
      species_meta_id "s_id_sa94"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 16
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "casp1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa25"
      type "text"
    ]
    minerva [
      minerva_activity "true"
      minerva_elementId "sa82"
      minerva_elementId2 "sa25"
      minerva_former_symbols "IL1BC"
      minerva_name "CASP1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/834"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/protein/NM_033292"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/834"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=3.4.22.36"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/1499"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000137752"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P29466"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/P29466"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CASP1"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CASP1"
      minerva_ref_type__resource1 "ENTREZ__834"
      minerva_ref_type__resource10 "REFSEQ__NM_033292"
      minerva_ref_type__resource2 "ENTREZ__834"
      minerva_ref_type__resource3 "EC__3.4.22.36"
      minerva_ref_type__resource4 "HGNC__1499"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000137752"
      minerva_ref_type__resource6 "UNIPROT__P29466"
      minerva_ref_type__resource7 "UNIPROT__P29466"
      minerva_ref_type__resource8 "HGNC_SYMBOL__CASP1"
      minerva_ref_type__resource9 "HGNC_SYMBOL__CASP1"
      minerva_type "Protein"
      minerva_x 708.0
      minerva_x2 704.0
      minerva_y 1449.0
      minerva_y2 1737.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa25"
      species_meta_id "s_id_sa25"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 17
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "trim23__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa70"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa70"
      minerva_former_symbols "ARFD1"
      minerva_name "TRIM23"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/373"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P36406"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P36406"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000113595"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM23"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM23"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/660"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/373"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_001656"
      minerva_ref_type__resource1 "EC__2.3.2.27"
      minerva_ref_type__resource10 "ENTREZ__373"
      minerva_ref_type__resource2 "UNIPROT__P36406"
      minerva_ref_type__resource3 "UNIPROT__P36406"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000113595"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TRIM23"
      minerva_ref_type__resource6 "HGNC_SYMBOL__TRIM23"
      minerva_ref_type__resource7 "HGNC__660"
      minerva_ref_type__resource8 "ENTREZ__373"
      minerva_ref_type__resource9 "REFSEQ__NM_001656"
      minerva_type "Protein"
      minerva_x 1039.6666666666667
      minerva_y 696.6666666666667
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa70"
      species_meta_id "s_id_sa70"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 19
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 900.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "serpinf2__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa87"
      type "text"
    ]
    minerva [
      minerva_activity "true"
      minerva_elementId "sa87"
      minerva_former_symbols "PLI"
      minerva_name "SERPINF2"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/P08697"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P08697"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000167711"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/5345"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/5345"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/9075"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_000934"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SERPINF2"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SERPINF2"
      minerva_ref_type__resource1 "UNIPROT__P08697"
      minerva_ref_type__resource2 "UNIPROT__P08697"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000167711"
      minerva_ref_type__resource4 "ENTREZ__5345"
      minerva_ref_type__resource5 "ENTREZ__5345"
      minerva_ref_type__resource6 "HGNC__9075"
      minerva_ref_type__resource7 "REFSEQ__NM_000934"
      minerva_ref_type__resource8 "HGNC_SYMBOL__SERPINF2"
      minerva_ref_type__resource9 "HGNC_SYMBOL__SERPINF2"
      minerva_type "Protein"
      minerva_x 885.0
      minerva_y 2020.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa87"
      species_meta_id "s_id_sa87"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 20
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1000.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "myd88__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa143"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa144"
      minerva_elementId2 "sa49"
      minerva_elementId3 "sa143"
      minerva_name "MYD88"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4615"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000172936"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/4615"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/Q99836"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/Q99836"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MYD88"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MYD88"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7562"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_002468"
      minerva_ref_type__resource1 "ENTREZ__4615"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000172936"
      minerva_ref_type__resource3 "ENTREZ__4615"
      minerva_ref_type__resource4 "UNIPROT__Q99836"
      minerva_ref_type__resource5 "UNIPROT__Q99836"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MYD88"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MYD88"
      minerva_ref_type__resource8 "HGNC__7562"
      minerva_ref_type__resource9 "REFSEQ__NM_002468"
      minerva_type "Protein"
      minerva_x 859.0
      minerva_x2 1130.0
      minerva_x3 623.0
      minerva_y 262.0
      minerva_y2 745.0
      minerva_y3 263.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa143"
      species_meta_id "s_id_sa143"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 21
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "trim27_slash_trim21__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa13"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa13"
      minerva_name "TRIM27/TRIM21"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/9975"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11312"
      minerva_ref_type__resource1 "HGNC__9975"
      minerva_ref_type__resource2 "HGNC__11312"
      minerva_type "Complex"
      minerva_x 1142.0
      minerva_y 1496.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa13"
      species_meta_id "s_id_csa13"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 22
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1200.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf8b__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa132"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa132"
      minerva_name "ORF8b"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/BCD58760"
      minerva_ref_type__resource1 "NCBI_PROTEIN__BCD58760"
      minerva_type "Protein"
      minerva_x 705.0
      minerva_y 1154.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa132"
      species_meta_id "s_id_sa132"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 23
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 200.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nfkb1__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa81"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa81"
      minerva_name "NFKB1"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4790"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/4790"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000109320"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKB1"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKB1"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7794"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/P19838"
      minerva_ref_link8 "http://purl.uniprot.org/uniprot/P19838"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_003998"
      minerva_ref_type__resource1 "ENTREZ__4790"
      minerva_ref_type__resource2 "ENTREZ__4790"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000109320"
      minerva_ref_type__resource4 "HGNC_SYMBOL__NFKB1"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NFKB1"
      minerva_ref_type__resource6 "HGNC__7794"
      minerva_ref_type__resource7 "UNIPROT__P19838"
      minerva_ref_type__resource8 "UNIPROT__P19838"
      minerva_ref_type__resource9 "REFSEQ__NM_003998"
      minerva_type "Protein"
      minerva_x 990.0
      minerva_y 2060.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa81"
      species_meta_id "s_id_sa81"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 24
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "irf6__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa141"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa141"
      minerva_former_symbols "LPS; VWS"
      minerva_name "IRF6"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/O14896"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/O14896"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_006147"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000117595"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6121"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/3664"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/3664"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IRF6"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IRF6"
      minerva_ref_type__resource1 "UNIPROT__O14896"
      minerva_ref_type__resource2 "UNIPROT__O14896"
      minerva_ref_type__resource3 "REFSEQ__NM_006147"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000117595"
      minerva_ref_type__resource5 "HGNC__6121"
      minerva_ref_type__resource6 "ENTREZ__3664"
      minerva_ref_type__resource7 "ENTREZ__3664"
      minerva_ref_type__resource8 "HGNC_SYMBOL__IRF6"
      minerva_ref_type__resource9 "HGNC_SYMBOL__IRF6"
      minerva_type "Protein"
      minerva_x 628.0
      minerva_y 126.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa141"
      species_meta_id "s_id_sa141"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 25
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tab2_slash_tab3_slash_tak1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa19"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa10"
      minerva_elementId2 "csa19"
      minerva_name "TAB2/TAB3/TAK1"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6859"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/30681"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/17075"
      minerva_ref_type__resource1 "HGNC__6859"
      minerva_ref_type__resource2 "HGNC__30681"
      minerva_ref_type__resource3 "HGNC__17075"
      minerva_type "Complex"
      minerva_x 1107.0
      minerva_x2 1106.0
      minerva_y 1179.5
      minerva_y2 887.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa19"
      species_meta_id "s_id_csa19"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 26
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "pycard__cell__ubiquitinated_ubiquitinated_ubiquitinated__3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa14"
      type "text"
    ]
    minerva [
      minerva_activity "true"
      minerva_elementId "sa14"
      minerva_homodimer "3"
      minerva_name "PYCARD"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/29108"
      minerva_ref_link10 "http://purl.uniprot.org/uniprot/Q9ULZ3"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/29108"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_013258"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PYCARD"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PYCARD"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000103490"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/pubmed/32172672"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/16608"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q9ULZ3"
      minerva_ref_type__resource1 "ENTREZ__29108"
      minerva_ref_type__resource10 "UNIPROT__Q9ULZ3"
      minerva_ref_type__resource2 "ENTREZ__29108"
      minerva_ref_type__resource3 "REFSEQ__NM_013258"
      minerva_ref_type__resource4 "HGNC_SYMBOL__PYCARD"
      minerva_ref_type__resource5 "HGNC_SYMBOL__PYCARD"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000103490"
      minerva_ref_type__resource7 "PUBMED__32172672"
      minerva_ref_type__resource8 "HGNC__16608"
      minerva_ref_type__resource9 "UNIPROT__Q9ULZ3"
      minerva_state1 "UBIQUITINATED"
      minerva_state2 "UBIQUITINATED"
      minerva_state3 "UBIQUITINATED"
      minerva_type "Protein"
      minerva_x 915.0
      minerva_y 1189.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa14"
      species_meta_id "s_id_sa14"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 27
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nfkbia__cell__phosphorylated_ubiquitinated_ubiquitinated_ubiquitinated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa65"
      type "text"
    ]
    minerva [
      minerva_activity "true"
      minerva_elementId "sa65"
      minerva_former_symbols "NFKBI"
      minerva_name "NFKBIA"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7797"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_020529"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000100906"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_type__resource1 "HGNC__7797"
      minerva_ref_type__resource2 "REFSEQ__NM_020529"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000100906"
      minerva_ref_type__resource4 "UNIPROT__P25963"
      minerva_ref_type__resource5 "UNIPROT__P25963"
      minerva_ref_type__resource6 "ENTREZ__4792"
      minerva_ref_type__resource7 "ENTREZ__4792"
      minerva_ref_type__resource8 "HGNC_SYMBOL__NFKBIA"
      minerva_ref_type__resource9 "HGNC_SYMBOL__NFKBIA"
      minerva_state1 "PHOSPHORYLATED"
      minerva_state2 "UBIQUITINATED"
      minerva_state3 "UBIQUITINATED"
      minerva_state4 "UBIQUITINATED"
      minerva_type "Protein"
      minerva_x 1069.0
      minerva_y 1911.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa65"
      species_meta_id "s_id_sa65"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 28
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 700.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tlr3__endosome__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa45"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa93"
      minerva_elementId2 "sa45"
      minerva_name "TLR3"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/O15455"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/O15455"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000164342"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_003265"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/7098"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/7098"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TLR3"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TLR3"
      minerva_ref_link9 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11849"
      minerva_ref_type__resource1 "UNIPROT__O15455"
      minerva_ref_type__resource2 "UNIPROT__O15455"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000164342"
      minerva_ref_type__resource4 "REFSEQ__NM_003265"
      minerva_ref_type__resource5 "ENTREZ__7098"
      minerva_ref_type__resource6 "ENTREZ__7098"
      minerva_ref_type__resource7 "HGNC_SYMBOL__TLR3"
      minerva_ref_type__resource8 "HGNC_SYMBOL__TLR3"
      minerva_ref_type__resource9 "HGNC__11849"
      minerva_type "Protein"
      minerva_x 1184.0
      minerva_x2 1406.0
      minerva_y 362.0
      minerva_y2 249.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "endosome"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa45"
      species_meta_id "s_id_sa45"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 29
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 800.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cactin__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa138"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa138"
      minerva_elementId2 "sa77"
      minerva_former_symbols "C19orf29"
      minerva_name "CACTIN"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CACTIN"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CACTIN"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/Q8WUQ7"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/Q8WUQ7"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000105298"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_001080543"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/29938"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/58509"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/58509"
      minerva_ref_type__resource1 "HGNC_SYMBOL__CACTIN"
      minerva_ref_type__resource2 "HGNC_SYMBOL__CACTIN"
      minerva_ref_type__resource3 "UNIPROT__Q8WUQ7"
      minerva_ref_type__resource4 "UNIPROT__Q8WUQ7"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000105298"
      minerva_ref_type__resource6 "REFSEQ__NM_001080543"
      minerva_ref_type__resource7 "HGNC__29938"
      minerva_ref_type__resource8 "ENTREZ__58509"
      minerva_ref_type__resource9 "ENTREZ__58509"
      minerva_type "Protein"
      minerva_x 1084.5
      minerva_x2 1236.0
      minerva_y 2161.0
      minerva_y2 2162.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa138"
      species_meta_id "s_id_sa138"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 30
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 900.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "p65_slash_p015__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa7"
      type "text"
    ]
    minerva [
      minerva_activity "true"
      minerva_elementId "csa7"
      minerva_name "P65/P015"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/9955"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7794"
      minerva_ref_type__resource1 "HGNC__9955"
      minerva_ref_type__resource2 "HGNC__7794"
      minerva_type "Complex"
      minerva_x 710.0
      minerva_y 2110.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa7"
      species_meta_id "s_id_csa7"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 31
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1000.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "trim39__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa76"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa139"
      minerva_elementId2 "sa76"
      minerva_former_symbols "RNF23"
      minerva_name "TRIM39"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_172016"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000204599"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM39"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM39"
      minerva_ref_link4 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/56658"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/56658"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/10065"
      minerva_ref_link8 "https://purl.uniprot.org/uniprot/Q9HCM9"
      minerva_ref_link9 "https://purl.uniprot.org/uniprot/Q9HCM9"
      minerva_ref_type__resource1 "REFSEQ__NM_172016"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000204599"
      minerva_ref_type__resource2 "HGNC_SYMBOL__TRIM39"
      minerva_ref_type__resource3 "HGNC_SYMBOL__TRIM39"
      minerva_ref_type__resource4 "EC__2.3.2.27"
      minerva_ref_type__resource5 "ENTREZ__56658"
      minerva_ref_type__resource6 "ENTREZ__56658"
      minerva_ref_type__resource7 "HGNC__10065"
      minerva_ref_type__resource8 "UNIPROT__Q9HCM9"
      minerva_ref_type__resource9 "UNIPROT__Q9HCM9"
      minerva_type "Protein"
      minerva_x 1160.5
      minerva_x2 1305.0
      minerva_y 2240.0
      minerva_y2 2238.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa76"
      species_meta_id "s_id_sa76"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 32
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "lps_slash_tlr4_slash_myd88__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa8"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa8"
      minerva_name "LPS/TLR4/MYD88"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11850"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6121"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7562"
      minerva_ref_type__resource1 "HGNC__11850"
      minerva_ref_type__resource2 "HGNC__6121"
      minerva_ref_type__resource3 "HGNC__7562"
      minerva_type "Complex"
      minerva_x 910.0
      minerva_y 135.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa8"
      species_meta_id "s_id_csa8"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 33
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1200.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf3a__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa1"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa1"
      minerva_name "Orf3a"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P0DTC3"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/43740569"
      minerva_ref_type__resource1 "UNIPROT__P0DTC3"
      minerva_ref_type__resource2 "ENTREZ__43740569"
      minerva_type "Protein"
      minerva_x 576.0
      minerva_y 1002.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa1"
      species_meta_id "s_id_sa1"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 34
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "pycard__cell__ubiquitinated_ubiquitinated_ubiquitinated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa131"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa131"
      minerva_name "PYCARD"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/29108"
      minerva_ref_link10 "http://purl.uniprot.org/uniprot/Q9ULZ3"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/29108"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_013258"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PYCARD"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=PYCARD"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000103490"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/pubmed/32172672"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/16608"
      minerva_ref_link9 "http://purl.uniprot.org/uniprot/Q9ULZ3"
      minerva_ref_type__resource1 "ENTREZ__29108"
      minerva_ref_type__resource10 "UNIPROT__Q9ULZ3"
      minerva_ref_type__resource2 "ENTREZ__29108"
      minerva_ref_type__resource3 "REFSEQ__NM_013258"
      minerva_ref_type__resource4 "HGNC_SYMBOL__PYCARD"
      minerva_ref_type__resource5 "HGNC_SYMBOL__PYCARD"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000103490"
      minerva_ref_type__resource7 "PUBMED__32172672"
      minerva_ref_type__resource8 "HGNC__16608"
      minerva_ref_type__resource9 "UNIPROT__Q9ULZ3"
      minerva_state1 "UBIQUITINATED"
      minerva_state2 "UBIQUITINATED"
      minerva_state3 "UBIQUITINATED"
      minerva_type "Protein"
      minerva_x 913.0
      minerva_y 1001.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa131"
      species_meta_id "s_id_sa131"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 35
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "trim30a_slash_trim38__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa11"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa11"
      minerva_name "TRIM30a/TRIM38"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P15533"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/10059"
      minerva_ref_type__resource1 "UNIPROT__P15533"
      minerva_ref_type__resource2 "HGNC__10059"
      minerva_type "Complex"
      minerva_x 1350.0
      minerva_y 1032.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa11"
      species_meta_id "s_id_csa11"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 36
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "trim59__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa3"
      type "text"
    ]
    minerva [
      minerva_activity "true"
      minerva_elementId "sa96"
      minerva_elementId2 "sa3"
      minerva_former_symbols "TRIM57"
      minerva_name "TRIM59"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM59"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/286827"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM59"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/Q8IWR1"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/Q8IWR1"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000213186"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/pubmed/22588174"
      minerva_ref_link7 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/30834"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/286827"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_173084"
      minerva_ref_type__resource1 "HGNC_SYMBOL__TRIM59"
      minerva_ref_type__resource10 "ENTREZ__286827"
      minerva_ref_type__resource2 "HGNC_SYMBOL__TRIM59"
      minerva_ref_type__resource3 "UNIPROT__Q8IWR1"
      minerva_ref_type__resource4 "UNIPROT__Q8IWR1"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000213186"
      minerva_ref_type__resource6 "PUBMED__22588174"
      minerva_ref_type__resource7 "HGNC__30834"
      minerva_ref_type__resource8 "ENTREZ__286827"
      minerva_ref_type__resource9 "REFSEQ__NM_173084"
      minerva_type "Protein"
      minerva_x 703.0
      minerva_x2 702.0
      minerva_y 614.0
      minerva_y2 419.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa3"
      species_meta_id "s_id_sa3"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 37
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 500.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "trim9__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa74"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa74"
      minerva_name "TRIM9"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/16288"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM9"
      minerva_ref_link2 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000100505"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/114088"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/114088"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/Q9C026"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/Q9C026"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM9"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_015163"
      minerva_ref_type__resource1 "HGNC__16288"
      minerva_ref_type__resource10 "HGNC_SYMBOL__TRIM9"
      minerva_ref_type__resource2 "EC__2.3.2.27"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000100505"
      minerva_ref_type__resource4 "ENTREZ__114088"
      minerva_ref_type__resource5 "ENTREZ__114088"
      minerva_ref_type__resource6 "UNIPROT__Q9C026"
      minerva_ref_type__resource7 "UNIPROT__Q9C026"
      minerva_ref_type__resource8 "HGNC_SYMBOL__TRIM9"
      minerva_ref_type__resource9 "REFSEQ__NM_015163"
      minerva_type "Protein"
      minerva_x 1452.0
      minerva_y 1751.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa74"
      species_meta_id "s_id_sa74"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 38
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nlrp3__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa134"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa134"
      minerva_former_symbols "C1orf7; CIAS1; DFNA34"
      minerva_name "NLRP3"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/16400"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_004895"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/Q96P20"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/Q96P20"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000162711"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NLRP3"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NLRP3"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/114548"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/114548"
      minerva_ref_type__resource1 "HGNC__16400"
      minerva_ref_type__resource2 "REFSEQ__NM_004895"
      minerva_ref_type__resource3 "UNIPROT__Q96P20"
      minerva_ref_type__resource4 "UNIPROT__Q96P20"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000162711"
      minerva_ref_type__resource6 "HGNC_SYMBOL__NLRP3"
      minerva_ref_type__resource7 "HGNC_SYMBOL__NLRP3"
      minerva_ref_type__resource8 "ENTREZ__114548"
      minerva_ref_type__resource9 "ENTREZ__114548"
      minerva_type "Protein"
      minerva_x 791.0
      minerva_y 1011.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa134"
      species_meta_id "s_id_sa134"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 39
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 700.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa34"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa34"
      minerva_name "S"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P0DTC2"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/43740568"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=S"
      minerva_ref_type__resource1 "UNIPROT__P0DTC2"
      minerva_ref_type__resource2 "ENTREZ__43740568"
      minerva_ref_type__resource3 "HGNC_SYMBOL__S"
      minerva_type "Protein"
      minerva_x 512.0
      minerva_y 1140.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa34"
      species_meta_id "s_id_sa34"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 40
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 800.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "double_minus_stranded_space_rna__endosome__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa41"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa41"
      minerva_name "double-stranded RNA"
      minerva_type "RNA"
      minerva_x 1408.0
      minerva_y 422.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "endosome"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa41"
      species_meta_id "s_id_sa41"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 41
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 900.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "hops_space_complex__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa2"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa2"
      minerva_name "Hops Complex"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/14583"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/J9TC74"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/5013"
      minerva_ref_link4 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/20266"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/24048"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/20593"
      minerva_ref_type__resource1 "HGNC__14583"
      minerva_ref_type__resource2 "UNIPROT__J9TC74"
      minerva_ref_type__resource3 "HGNC__5013"
      minerva_ref_type__resource4 "HGNC__20266"
      minerva_ref_type__resource5 "HGNC__24048"
      minerva_ref_type__resource6 "HGNC__20593"
      minerva_type "Complex"
      minerva_x 312.5
      minerva_y 1078.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa2"
      species_meta_id "s_id_csa2"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 42
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1000.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "trim29__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa67"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa67"
      minerva_name "TRIM29"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/23650"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/23650"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM29"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_012101"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/Q14134"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/Q14134"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM29"
      minerva_ref_link8 "https://www.ensembl.org/id/ENSG00000137699"
      minerva_ref_link9 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/17274"
      minerva_ref_type__resource1 "ENTREZ__23650"
      minerva_ref_type__resource2 "ENTREZ__23650"
      minerva_ref_type__resource3 "HGNC_SYMBOL__TRIM29"
      minerva_ref_type__resource4 "REFSEQ__NM_012101"
      minerva_ref_type__resource5 "UNIPROT__Q14134"
      minerva_ref_type__resource6 "UNIPROT__Q14134"
      minerva_ref_type__resource7 "HGNC_SYMBOL__TRIM29"
      minerva_ref_type__resource8 "ENSEMBL__ENSG00000137699"
      minerva_ref_type__resource9 "HGNC__17274"
      minerva_type "Protein"
      minerva_x 1393.0
      minerva_y 1291.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_concentration 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa67"
      species_meta_id "s_id_sa67"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 43
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1100.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "serpinf2__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa86"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa86"
      minerva_former_symbols "PLI"
      minerva_name "SERPINF2"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/P08697"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P08697"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000167711"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/5345"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/5345"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/9075"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_000934"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SERPINF2"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=SERPINF2"
      minerva_ref_type__resource1 "UNIPROT__P08697"
      minerva_ref_type__resource2 "UNIPROT__P08697"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000167711"
      minerva_ref_type__resource4 "ENTREZ__5345"
      minerva_ref_type__resource5 "ENTREZ__5345"
      minerva_ref_type__resource6 "HGNC__9075"
      minerva_ref_type__resource7 "REFSEQ__NM_000934"
      minerva_ref_type__resource8 "HGNC_SYMBOL__SERPINF2"
      minerva_ref_type__resource9 "HGNC_SYMBOL__SERPINF2"
      minerva_type "Protein"
      minerva_x 884.0
      minerva_y 1719.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa86"
      species_meta_id "s_id_sa86"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 44
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 1200.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nfkbia__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa64"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa64"
      minerva_former_symbols "NFKBI"
      minerva_name "NFKBIA"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7797"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_020529"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000100906"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_type__resource1 "HGNC__7797"
      minerva_ref_type__resource2 "REFSEQ__NM_020529"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000100906"
      minerva_ref_type__resource4 "UNIPROT__P25963"
      minerva_ref_type__resource5 "UNIPROT__P25963"
      minerva_ref_type__resource6 "ENTREZ__4792"
      minerva_ref_type__resource7 "ENTREZ__4792"
      minerva_ref_type__resource8 "HGNC_SYMBOL__NFKBIA"
      minerva_ref_type__resource9 "HGNC_SYMBOL__NFKBIA"
      minerva_type "Protein"
      minerva_x 1066.0
      minerva_y 1728.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa64"
      species_meta_id "s_id_sa64"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 45
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 200.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s187__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa124"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa124"
      minerva_name "s187"
      minerva_type "Degraded"
      minerva_x 512.0
      minerva_y 996.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa124"
      species_meta_id "s_id_sa124"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 46
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 300.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ssrna__endosome__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa42"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa42"
      minerva_name "ssRNA"
      minerva_type "RNA"
      minerva_x 1417.0
      minerva_y 479.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "endosome"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa42"
      species_meta_id "s_id_sa42"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 47
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "il1b__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa92"
      type "text"
    ]
    minerva [
      minerva_activity "true"
      minerva_elementId "sa127"
      minerva_elementId2 "sa92"
      minerva_name "IL1b"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/5992"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL1B"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=IL1B"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P01584"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P01584"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_000576"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/3553"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/3553"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000125538"
      minerva_ref_type__resource1 "HGNC__5992"
      minerva_ref_type__resource2 "HGNC_SYMBOL__IL1B"
      minerva_ref_type__resource3 "HGNC_SYMBOL__IL1B"
      minerva_ref_type__resource4 "UNIPROT__P01584"
      minerva_ref_type__resource5 "UNIPROT__P01584"
      minerva_ref_type__resource6 "REFSEQ__NM_000576"
      minerva_ref_type__resource7 "ENTREZ__3553"
      minerva_ref_type__resource8 "ENTREZ__3553"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000125538"
      minerva_type "Protein"
      minerva_x 633.0
      minerva_x2 794.0
      minerva_y 1889.0
      minerva_y2 1889.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa92"
      species_meta_id "s_id_sa92"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 49
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 600.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "trim14_slash_trim23__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa15"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa15"
      minerva_name "TRIM14/TRIM23"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/16283"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/660"
      minerva_ref_type__resource1 "HGNC__16283"
      minerva_ref_type__resource2 "HGNC__660"
      minerva_type "Complex"
      minerva_x 1373.0
      minerva_y 1496.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa15"
      species_meta_id "s_id_csa15"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 50
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 700.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "myd88__endosome__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa100"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa100"
      minerva_name "MYD88"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/4615"
      minerva_ref_link2 "https://www.ensembl.org/id/ENSG00000172936"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/4615"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/Q99836"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/Q99836"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MYD88"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MYD88"
      minerva_ref_link8 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7562"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/protein/NM_002468"
      minerva_ref_type__resource1 "ENTREZ__4615"
      minerva_ref_type__resource2 "ENSEMBL__ENSG00000172936"
      minerva_ref_type__resource3 "ENTREZ__4615"
      minerva_ref_type__resource4 "UNIPROT__Q99836"
      minerva_ref_type__resource5 "UNIPROT__Q99836"
      minerva_ref_type__resource6 "HGNC_SYMBOL__MYD88"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MYD88"
      minerva_ref_type__resource8 "HGNC__7562"
      minerva_ref_type__resource9 "REFSEQ__NM_002468"
      minerva_type "Protein"
      minerva_x 1128.0
      minerva_y 613.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "endosome"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa100"
      species_meta_id "s_id_sa100"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 51
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 800.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "trim22__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa56"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa56"
      minerva_name "TRIM22"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link10 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM22"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/16379"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/Q8IYM9"
      minerva_ref_link4 "https://purl.uniprot.org/uniprot/Q8IYM9"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000132274"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_006074"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/10346"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/10346"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TRIM22"
      minerva_ref_type__resource1 "EC__2.3.2.27"
      minerva_ref_type__resource10 "HGNC_SYMBOL__TRIM22"
      minerva_ref_type__resource2 "HGNC__16379"
      minerva_ref_type__resource3 "UNIPROT__Q8IYM9"
      minerva_ref_type__resource4 "UNIPROT__Q8IYM9"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000132274"
      minerva_ref_type__resource6 "REFSEQ__NM_006074"
      minerva_ref_type__resource7 "ENTREZ__10346"
      minerva_ref_type__resource8 "ENTREZ__10346"
      minerva_ref_type__resource9 "HGNC_SYMBOL__TRIM22"
      minerva_type "Protein"
      minerva_x 1454.0
      minerva_y 1369.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa56"
      species_meta_id "s_id_sa56"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 52
    zlevel -1

    graphics [
      x 900.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re93"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7797"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_020529"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000100906"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_type__resource1 "HGNC__7797"
      minerva_ref_type__resource2 "REFSEQ__NM_020529"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000100906"
      minerva_ref_type__resource4 "UNIPROT__P25963"
      minerva_ref_type__resource5 "UNIPROT__P25963"
      minerva_ref_type__resource6 "ENTREZ__4792"
      minerva_ref_type__resource7 "ENTREZ__4792"
      minerva_ref_type__resource8 "HGNC_SYMBOL__NFKBIA"
      minerva_ref_type__resource9 "HGNC_SYMBOL__NFKBIA"
      minerva_type "State transition"
      minerva_x 1084.5
      minerva_y 2059.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re93"
      reaction_meta_id "re93"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 57
    zlevel -1

    graphics [
      x 1000.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re32"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7797"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_020529"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000100906"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_type__resource1 "HGNC__7797"
      minerva_ref_type__resource2 "REFSEQ__NM_020529"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000100906"
      minerva_ref_type__resource4 "UNIPROT__P25963"
      minerva_ref_type__resource5 "UNIPROT__P25963"
      minerva_ref_type__resource6 "ENTREZ__4792"
      minerva_ref_type__resource7 "ENTREZ__4792"
      minerva_ref_type__resource8 "HGNC_SYMBOL__NFKBIA"
      minerva_ref_type__resource9 "HGNC_SYMBOL__NFKBIA"
      minerva_type "State transition"
      minerva_x 965.3333333333334
      minerva_y 488.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re32"
      reaction_meta_id "re32"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 61
    zlevel -1

    graphics [
      x 1100.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re106"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7797"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_020529"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000100906"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_type__resource1 "HGNC__7797"
      minerva_ref_type__resource2 "REFSEQ__NM_020529"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000100906"
      minerva_ref_type__resource4 "UNIPROT__P25963"
      minerva_ref_type__resource5 "UNIPROT__P25963"
      minerva_ref_type__resource6 "ENTREZ__4792"
      minerva_ref_type__resource7 "ENTREZ__4792"
      minerva_ref_type__resource8 "HGNC_SYMBOL__NFKBIA"
      minerva_ref_type__resource9 "HGNC_SYMBOL__NFKBIA"
      minerva_type "Positive influence"
      minerva_x 633.0
      minerva_y 1994.5000000000002
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re106"
      reaction_meta_id "re106"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 64
    zlevel -1

    graphics [
      x 1200.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re107"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7797"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_020529"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000100906"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_type__resource1 "HGNC__7797"
      minerva_ref_type__resource2 "REFSEQ__NM_020529"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000100906"
      minerva_ref_type__resource4 "UNIPROT__P25963"
      minerva_ref_type__resource5 "UNIPROT__P25963"
      minerva_ref_type__resource6 "ENTREZ__4792"
      minerva_ref_type__resource7 "ENTREZ__4792"
      minerva_ref_type__resource8 "HGNC_SYMBOL__NFKBIA"
      minerva_ref_type__resource9 "HGNC_SYMBOL__NFKBIA"
      minerva_type "State transition"
      minerva_x 467.50000000000006
      minerva_y 1450.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re107"
      reaction_meta_id "re107"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 68
    zlevel -1

    graphics [
      x 200.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re61"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7797"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_020529"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000100906"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_type__resource1 "HGNC__7797"
      minerva_ref_type__resource2 "REFSEQ__NM_020529"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000100906"
      minerva_ref_type__resource4 "UNIPROT__P25963"
      minerva_ref_type__resource5 "UNIPROT__P25963"
      minerva_ref_type__resource6 "ENTREZ__4792"
      minerva_ref_type__resource7 "ENTREZ__4792"
      minerva_ref_type__resource8 "HGNC_SYMBOL__NFKBIA"
      minerva_ref_type__resource9 "HGNC_SYMBOL__NFKBIA"
      minerva_type "State transition"
      minerva_x 713.5
      minerva_y 1889.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re61"
      reaction_meta_id "re61"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 74
    zlevel -1

    graphics [
      x 300.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re103"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7797"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_020529"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000100906"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_type__resource1 "HGNC__7797"
      minerva_ref_type__resource2 "REFSEQ__NM_020529"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000100906"
      minerva_ref_type__resource4 "UNIPROT__P25963"
      minerva_ref_type__resource5 "UNIPROT__P25963"
      minerva_ref_type__resource6 "ENTREZ__4792"
      minerva_ref_type__resource7 "ENTREZ__4792"
      minerva_ref_type__resource8 "HGNC_SYMBOL__NFKBIA"
      minerva_ref_type__resource9 "HGNC_SYMBOL__NFKBIA"
      minerva_type "State transition"
      minerva_x 913.9679999999994
      minerva_y 1092.25
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re103"
      reaction_meta_id "re103"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 79
    zlevel -1

    graphics [
      x 400.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re73"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7797"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_020529"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000100906"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_type__resource1 "HGNC__7797"
      minerva_ref_type__resource2 "REFSEQ__NM_020529"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000100906"
      minerva_ref_type__resource4 "UNIPROT__P25963"
      minerva_ref_type__resource5 "UNIPROT__P25963"
      minerva_ref_type__resource6 "ENTREZ__4792"
      minerva_ref_type__resource7 "ENTREZ__4792"
      minerva_ref_type__resource8 "HGNC_SYMBOL__NFKBIA"
      minerva_ref_type__resource9 "HGNC_SYMBOL__NFKBIA"
      minerva_type "State transition"
      minerva_x 1128.9999999999995
      minerva_y 679.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re73"
      reaction_meta_id "re73"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 83
    zlevel -1

    graphics [
      x 500.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re109"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7797"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_020529"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000100906"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_type__resource1 "HGNC__7797"
      minerva_ref_type__resource2 "REFSEQ__NM_020529"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000100906"
      minerva_ref_type__resource4 "UNIPROT__P25963"
      minerva_ref_type__resource5 "UNIPROT__P25963"
      minerva_ref_type__resource6 "ENTREZ__4792"
      minerva_ref_type__resource7 "ENTREZ__4792"
      minerva_ref_type__resource8 "HGNC_SYMBOL__NFKBIA"
      minerva_ref_type__resource9 "HGNC_SYMBOL__NFKBIA"
      minerva_type "State transition"
      minerva_x 1160.25
      minerva_y 2161.5000000000005
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re109"
      reaction_meta_id "re109"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 87
    zlevel -1

    graphics [
      x 600.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re24"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7797"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_020529"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000100906"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_type__resource1 "HGNC__7797"
      minerva_ref_type__resource2 "REFSEQ__NM_020529"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000100906"
      minerva_ref_type__resource4 "UNIPROT__P25963"
      minerva_ref_type__resource5 "UNIPROT__P25963"
      minerva_ref_type__resource6 "ENTREZ__4792"
      minerva_ref_type__resource7 "ENTREZ__4792"
      minerva_ref_type__resource8 "HGNC_SYMBOL__NFKBIA"
      minerva_ref_type__resource9 "HGNC_SYMBOL__NFKBIA"
      minerva_type "State transition"
      minerva_x 706.1492537313429
      minerva_y 1593.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re24"
      reaction_meta_id "re24"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 92
    zlevel -1

    graphics [
      x 700.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re113"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7797"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_020529"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000100906"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_type__resource1 "HGNC__7797"
      minerva_ref_type__resource2 "REFSEQ__NM_020529"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000100906"
      minerva_ref_type__resource4 "UNIPROT__P25963"
      minerva_ref_type__resource5 "UNIPROT__P25963"
      minerva_ref_type__resource6 "ENTREZ__4792"
      minerva_ref_type__resource7 "ENTREZ__4792"
      minerva_ref_type__resource8 "HGNC_SYMBOL__NFKBIA"
      minerva_ref_type__resource9 "HGNC_SYMBOL__NFKBIA"
      minerva_type "State transition"
      minerva_x 741.0
      minerva_y 262.5000000000001
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re113"
      reaction_meta_id "re113"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 96
    zlevel -1

    graphics [
      x 800.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re70"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7797"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_020529"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000100906"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_type__resource1 "HGNC__7797"
      minerva_ref_type__resource2 "REFSEQ__NM_020529"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000100906"
      minerva_ref_type__resource4 "UNIPROT__P25963"
      minerva_ref_type__resource5 "UNIPROT__P25963"
      minerva_ref_type__resource6 "ENTREZ__4792"
      minerva_ref_type__resource7 "ENTREZ__4792"
      minerva_ref_type__resource8 "HGNC_SYMBOL__NFKBIA"
      minerva_ref_type__resource9 "HGNC_SYMBOL__NFKBIA"
      minerva_type "State transition"
      minerva_x 702.5
      minerva_y 516.5000000000001
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re70"
      reaction_meta_id "re70"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 100
    zlevel -1

    graphics [
      x 900.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re68"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7797"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_020529"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000100906"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_type__resource1 "HGNC__7797"
      minerva_ref_type__resource2 "REFSEQ__NM_020529"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000100906"
      minerva_ref_type__resource4 "UNIPROT__P25963"
      minerva_ref_type__resource5 "UNIPROT__P25963"
      minerva_ref_type__resource6 "ENTREZ__4792"
      minerva_ref_type__resource7 "ENTREZ__4792"
      minerva_ref_type__resource8 "HGNC_SYMBOL__NFKBIA"
      minerva_ref_type__resource9 "HGNC_SYMBOL__NFKBIA"
      minerva_type "State transition"
      minerva_x 1071.5
      minerva_y 441.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re68"
      reaction_meta_id "re68"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 105
    zlevel -1

    graphics [
      x 1000.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re89"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7797"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_020529"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000100906"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_type__resource1 "HGNC__7797"
      minerva_ref_type__resource2 "REFSEQ__NM_020529"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000100906"
      minerva_ref_type__resource4 "UNIPROT__P25963"
      minerva_ref_type__resource5 "UNIPROT__P25963"
      minerva_ref_type__resource6 "ENTREZ__4792"
      minerva_ref_type__resource7 "ENTREZ__4792"
      minerva_ref_type__resource8 "HGNC_SYMBOL__NFKBIA"
      minerva_ref_type__resource9 "HGNC_SYMBOL__NFKBIA"
      minerva_type "State transition"
      minerva_x 1258.5
      minerva_y 1368.75
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re89"
      reaction_meta_id "re89"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 113
    zlevel -1

    graphics [
      x 1100.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re100"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7797"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_020529"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000100906"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_type__resource1 "HGNC__7797"
      minerva_ref_type__resource2 "REFSEQ__NM_020529"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000100906"
      minerva_ref_type__resource4 "UNIPROT__P25963"
      minerva_ref_type__resource5 "UNIPROT__P25963"
      minerva_ref_type__resource6 "ENTREZ__4792"
      minerva_ref_type__resource7 "ENTREZ__4792"
      minerva_ref_type__resource8 "HGNC_SYMBOL__NFKBIA"
      minerva_ref_type__resource9 "HGNC_SYMBOL__NFKBIA"
      minerva_type "State transition"
      minerva_x 348.2747065789739
      minerva_y 1756.7824094140442
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re100"
      reaction_meta_id "re100"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 119
    zlevel -1

    graphics [
      x 1200.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re111"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7797"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_020529"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000100906"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_type__resource1 "HGNC__7797"
      minerva_ref_type__resource2 "REFSEQ__NM_020529"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000100906"
      minerva_ref_type__resource4 "UNIPROT__P25963"
      minerva_ref_type__resource5 "UNIPROT__P25963"
      minerva_ref_type__resource6 "ENTREZ__4792"
      minerva_ref_type__resource7 "ENTREZ__4792"
      minerva_ref_type__resource8 "HGNC_SYMBOL__NFKBIA"
      minerva_ref_type__resource9 "HGNC_SYMBOL__NFKBIA"
      minerva_type "State transition"
      minerva_x 312.27666607409384
      minerva_y 936.4999111140739
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re111"
      reaction_meta_id "re111"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 123
    zlevel -1

    graphics [
      x 200.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re97"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7797"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_020529"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000100906"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_type__resource1 "HGNC__7797"
      minerva_ref_type__resource2 "REFSEQ__NM_020529"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000100906"
      minerva_ref_type__resource4 "UNIPROT__P25963"
      minerva_ref_type__resource5 "UNIPROT__P25963"
      minerva_ref_type__resource6 "ENTREZ__4792"
      minerva_ref_type__resource7 "ENTREZ__4792"
      minerva_ref_type__resource8 "HGNC_SYMBOL__NFKBIA"
      minerva_ref_type__resource9 "HGNC_SYMBOL__NFKBIA"
      minerva_type "State transition"
      minerva_x 512.0
      minerva_y 1062.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re97"
      reaction_meta_id "re97"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 127
    zlevel -1

    graphics [
      x 300.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re92"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7797"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_020529"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000100906"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_type__resource1 "HGNC__7797"
      minerva_ref_type__resource2 "REFSEQ__NM_020529"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000100906"
      minerva_ref_type__resource4 "UNIPROT__P25963"
      minerva_ref_type__resource5 "UNIPROT__P25963"
      minerva_ref_type__resource6 "ENTREZ__4792"
      minerva_ref_type__resource7 "ENTREZ__4792"
      minerva_ref_type__resource8 "HGNC_SYMBOL__NFKBIA"
      minerva_ref_type__resource9 "HGNC_SYMBOL__NFKBIA"
      minerva_type "State transition"
      minerva_x 665.8529411764707
      minerva_y 814.7941176470586
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re92"
      reaction_meta_id "re92"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 131
    zlevel -1

    graphics [
      x 400.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re75"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7797"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_020529"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000100906"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_type__resource1 "HGNC__7797"
      minerva_ref_type__resource2 "REFSEQ__NM_020529"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000100906"
      minerva_ref_type__resource4 "UNIPROT__P25963"
      minerva_ref_type__resource5 "UNIPROT__P25963"
      minerva_ref_type__resource6 "ENTREZ__4792"
      minerva_ref_type__resource7 "ENTREZ__4792"
      minerva_ref_type__resource8 "HGNC_SYMBOL__NFKBIA"
      minerva_ref_type__resource9 "HGNC_SYMBOL__NFKBIA"
      minerva_type "State transition"
      minerva_x 966.0
      minerva_y 745.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re75"
      reaction_meta_id "re75"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 137
    zlevel -1

    graphics [
      x 500.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re88"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7797"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_020529"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000100906"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_type__resource1 "HGNC__7797"
      minerva_ref_type__resource2 "REFSEQ__NM_020529"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000100906"
      minerva_ref_type__resource4 "UNIPROT__P25963"
      minerva_ref_type__resource5 "UNIPROT__P25963"
      minerva_ref_type__resource6 "ENTREZ__4792"
      minerva_ref_type__resource7 "ENTREZ__4792"
      minerva_ref_type__resource8 "HGNC_SYMBOL__NFKBIA"
      minerva_ref_type__resource9 "HGNC_SYMBOL__NFKBIA"
      minerva_type "State transition"
      minerva_x 1106.5
      minerva_y 1033.25
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re88"
      reaction_meta_id "re88"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 142
    zlevel -1

    graphics [
      x 600.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re110"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7797"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_020529"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000100906"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_type__resource1 "HGNC__7797"
      minerva_ref_type__resource2 "REFSEQ__NM_020529"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000100906"
      minerva_ref_type__resource4 "UNIPROT__P25963"
      minerva_ref_type__resource5 "UNIPROT__P25963"
      minerva_ref_type__resource6 "ENTREZ__4792"
      minerva_ref_type__resource7 "ENTREZ__4792"
      minerva_ref_type__resource8 "HGNC_SYMBOL__NFKBIA"
      minerva_ref_type__resource9 "HGNC_SYMBOL__NFKBIA"
      minerva_type "State transition"
      minerva_x 1232.75
      minerva_y 2239.000000000001
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re110"
      reaction_meta_id "re110"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 145
    zlevel -1

    graphics [
      x 700.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re105"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7797"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_020529"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000100906"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_type__resource1 "HGNC__7797"
      minerva_ref_type__resource2 "REFSEQ__NM_020529"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000100906"
      minerva_ref_type__resource4 "UNIPROT__P25963"
      minerva_ref_type__resource5 "UNIPROT__P25963"
      minerva_ref_type__resource6 "ENTREZ__4792"
      minerva_ref_type__resource7 "ENTREZ__4792"
      minerva_ref_type__resource8 "HGNC_SYMBOL__NFKBIA"
      minerva_ref_type__resource9 "HGNC_SYMBOL__NFKBIA"
      minerva_type "State transition"
      minerva_x 1451.3333333333335
      minerva_y 1815.4999999999998
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re105"
      reaction_meta_id "re105"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 149
    zlevel -1

    graphics [
      x 800.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re50"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7797"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_020529"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000100906"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_type__resource1 "HGNC__7797"
      minerva_ref_type__resource2 "REFSEQ__NM_020529"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000100906"
      minerva_ref_type__resource4 "UNIPROT__P25963"
      minerva_ref_type__resource5 "UNIPROT__P25963"
      minerva_ref_type__resource6 "ENTREZ__4792"
      minerva_ref_type__resource7 "ENTREZ__4792"
      minerva_ref_type__resource8 "HGNC_SYMBOL__NFKBIA"
      minerva_ref_type__resource9 "HGNC_SYMBOL__NFKBIA"
      minerva_type "State transition"
      minerva_x 1067.499999999999
      minerva_y 1819.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re50"
      reaction_meta_id "re50"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 154
    zlevel -1

    graphics [
      x 900.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re104"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7797"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_020529"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000100906"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_type__resource1 "HGNC__7797"
      minerva_ref_type__resource2 "REFSEQ__NM_020529"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000100906"
      minerva_ref_type__resource4 "UNIPROT__P25963"
      minerva_ref_type__resource5 "UNIPROT__P25963"
      minerva_ref_type__resource6 "ENTREZ__4792"
      minerva_ref_type__resource7 "ENTREZ__4792"
      minerva_ref_type__resource8 "HGNC_SYMBOL__NFKBIA"
      minerva_ref_type__resource9 "HGNC_SYMBOL__NFKBIA"
      minerva_type "State transition"
      minerva_x 791.0
      minerva_y 1116.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re104"
      reaction_meta_id "re104"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 158
    zlevel -1

    graphics [
      x 1000.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re64"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7797"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_020529"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000100906"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_type__resource1 "HGNC__7797"
      minerva_ref_type__resource2 "REFSEQ__NM_020529"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000100906"
      minerva_ref_type__resource4 "UNIPROT__P25963"
      minerva_ref_type__resource5 "UNIPROT__P25963"
      minerva_ref_type__resource6 "ENTREZ__4792"
      minerva_ref_type__resource7 "ENTREZ__4792"
      minerva_ref_type__resource8 "HGNC_SYMBOL__NFKBIA"
      minerva_ref_type__resource9 "HGNC_SYMBOL__NFKBIA"
      minerva_type "State transition"
      minerva_x 887.5
      minerva_y 2140.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re64"
      reaction_meta_id "re64"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 162
    zlevel -1

    graphics [
      x 1100.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re112"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7797"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_020529"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000100906"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_type__resource1 "HGNC__7797"
      minerva_ref_type__resource2 "REFSEQ__NM_020529"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000100906"
      minerva_ref_type__resource4 "UNIPROT__P25963"
      minerva_ref_type__resource5 "UNIPROT__P25963"
      minerva_ref_type__resource6 "ENTREZ__4792"
      minerva_ref_type__resource7 "ENTREZ__4792"
      minerva_ref_type__resource8 "HGNC_SYMBOL__NFKBIA"
      minerva_ref_type__resource9 "HGNC_SYMBOL__NFKBIA"
      minerva_type "State transition"
      minerva_x 760.0
      minerva_y 179.625
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re112"
      reaction_meta_id "re112"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 167
    zlevel -1

    graphics [
      x 1200.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re67"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7797"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_020529"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000100906"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_type__resource1 "HGNC__7797"
      minerva_ref_type__resource2 "REFSEQ__NM_020529"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000100906"
      minerva_ref_type__resource4 "UNIPROT__P25963"
      minerva_ref_type__resource5 "UNIPROT__P25963"
      minerva_ref_type__resource6 "ENTREZ__4792"
      minerva_ref_type__resource7 "ENTREZ__4792"
      minerva_ref_type__resource8 "HGNC_SYMBOL__NFKBIA"
      minerva_ref_type__resource9 "HGNC_SYMBOL__NFKBIA"
      minerva_type "State transition"
      minerva_x 1183.8882813565574
      minerva_y 298.7689934160644
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re67"
      reaction_meta_id "re67"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 171
    zlevel -1

    graphics [
      x 200.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re72"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7797"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_020529"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000100906"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_type__resource1 "HGNC__7797"
      minerva_ref_type__resource2 "REFSEQ__NM_020529"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000100906"
      minerva_ref_type__resource4 "UNIPROT__P25963"
      minerva_ref_type__resource5 "UNIPROT__P25963"
      minerva_ref_type__resource6 "ENTREZ__4792"
      minerva_ref_type__resource7 "ENTREZ__4792"
      minerva_ref_type__resource8 "HGNC_SYMBOL__NFKBIA"
      minerva_ref_type__resource9 "HGNC_SYMBOL__NFKBIA"
      minerva_type "State transition"
      minerva_x 1590.4882075471671
      minerva_y 623.0212264150962
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re72"
      reaction_meta_id "re72"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 175
    zlevel -1

    graphics [
      x 300.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re96"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7797"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_020529"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000100906"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_type__resource1 "HGNC__7797"
      minerva_ref_type__resource2 "REFSEQ__NM_020529"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000100906"
      minerva_ref_type__resource4 "UNIPROT__P25963"
      minerva_ref_type__resource5 "UNIPROT__P25963"
      minerva_ref_type__resource6 "ENTREZ__4792"
      minerva_ref_type__resource7 "ENTREZ__4792"
      minerva_ref_type__resource8 "HGNC_SYMBOL__NFKBIA"
      minerva_ref_type__resource9 "HGNC_SYMBOL__NFKBIA"
      minerva_type "State transition"
      minerva_x 466.8382352941177
      minerva_y 525.0441176470588
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re96"
      reaction_meta_id "re96"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 179
    zlevel -1

    graphics [
      x 400.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re90"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7797"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_020529"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000100906"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P25963"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4792"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NFKBIA"
      minerva_ref_type__resource1 "HGNC__7797"
      minerva_ref_type__resource2 "REFSEQ__NM_020529"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000100906"
      minerva_ref_type__resource4 "UNIPROT__P25963"
      minerva_ref_type__resource5 "UNIPROT__P25963"
      minerva_ref_type__resource6 "ENTREZ__4792"
      minerva_ref_type__resource7 "ENTREZ__4792"
      minerva_ref_type__resource8 "HGNC_SYMBOL__NFKBIA"
      minerva_ref_type__resource9 "HGNC_SYMBOL__NFKBIA"
      minerva_type "State transition"
      minerva_x 884.4999999999998
      minerva_y 1869.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re90"
      reaction_meta_id "re90"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 183
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 650.0
      y 350.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tlr7_slash_8_slash_9__endosome__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa9"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa9"
      minerva_name "TLR7/8/9"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/15631"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/15632"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/15633"
      minerva_ref_type__resource1 "HGNC__15631"
      minerva_ref_type__resource2 "HGNC__15632"
      minerva_ref_type__resource3 "HGNC__15633"
      minerva_type "Complex"
      minerva_x 1590.5
      minerva_y 479.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "endosome"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation "Annotation [about=\"#s_id_csa9\", history=]"
      species_id "s_id_csa9"
      species_meta_id "s_id_csa9"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
    ]
  ]
  edge [
    id 189
    source 23
    target 52
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa81"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 190
    source 27
    target 52
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa65"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 191
    source 52
    target 7
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa21"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 192
    source 29
    target 52
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR0"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa138"
    ]
  ]
  edge [
    id 193
    source 1
    target 57
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa101"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 195
    source 32
    target 57
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR1"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa8"
    ]
  ]
  edge [
    id 196
    source 30
    target 61
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa7"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 197
    source 61
    target 47
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa92"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 198
    source 9
    target 64
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa22"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 199
    source 64
    target 30
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa7"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 200
    source 10
    target 64
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR2"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa83"
    ]
  ]
  edge [
    id 201
    source 47
    target 68
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa92"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 203
    source 16
    target 68
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR3"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa25"
    ]
  ]
  edge [
    id 204
    source 10
    target 68
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR4"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa83"
    ]
  ]
  edge [
    id 205
    source 30
    target 68
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR5"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa7"
    ]
  ]
  edge [
    id 206
    source 34
    target 74
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa131"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 207
    source 74
    target 26
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa14"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 208
    source 11
    target 74
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR6"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa116"
    ]
  ]
  edge [
    id 209
    source 33
    target 74
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR7"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa1"
    ]
  ]
  edge [
    id 210
    source 50
    target 79
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa100"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 211
    source 79
    target 20
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa143"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 212
    source 29
    target 83
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa138"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 214
    source 31
    target 83
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR9"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa76"
    ]
  ]
  edge [
    id 215
    source 16
    target 87
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa25"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 216
    source 26
    target 87
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa14"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 217
    source 12
    target 87
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa10"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 219
    source 20
    target 92
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa143"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 221
    source 36
    target 92
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR10"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa3"
    ]
  ]
  edge [
    id 222
    source 36
    target 96
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa3"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 224
    source 33
    target 96
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR11"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa1"
    ]
  ]
  edge [
    id 225
    source 15
    target 100
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa94"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 227
    source 28
    target 100
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR12"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa45"
    ]
  ]
  edge [
    id 228
    source 8
    target 100
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR13"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa52"
    ]
  ]
  edge [
    id 229
    source 5
    target 105
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa12"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 231
    source 25
    target 105
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR14"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa19"
    ]
  ]
  edge [
    id 232
    source 51
    target 105
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR15"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa56"
    ]
  ]
  edge [
    id 233
    source 42
    target 105
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR16"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa67"
    ]
  ]
  edge [
    id 234
    source 49
    target 105
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR17"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa15"
    ]
  ]
  edge [
    id 235
    source 21
    target 105
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR18"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa13"
    ]
  ]
  edge [
    id 236
    source 4
    target 113
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa6"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 237
    source 113
    target 30
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa7"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 238
    source 11
    target 113
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR19"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa116"
    ]
  ]
  edge [
    id 239
    source 9
    target 113
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR20"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa22"
    ]
  ]
  edge [
    id 240
    source 33
    target 113
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR21"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa1"
    ]
  ]
  edge [
    id 241
    source 14
    target 119
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa140"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 242
    source 119
    target 41
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa2"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 243
    source 33
    target 119
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR22"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa1"
    ]
  ]
  edge [
    id 244
    source 45
    target 123
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa124"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 245
    source 123
    target 39
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa34"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 246
    source 33
    target 123
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR23"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa1"
    ]
  ]
  edge [
    id 247
    source 11
    target 127
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa116"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 249
    source 15
    target 127
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR24"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa94"
    ]
  ]
  edge [
    id 250
    source 1
    target 131
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa101"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 252
    source 20
    target 131
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR25"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa143"
    ]
  ]
  edge [
    id 253
    source 8
    target 131
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR26"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa52"
    ]
  ]
  edge [
    id 254
    source 17
    target 131
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR27"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa70"
    ]
  ]
  edge [
    id 255
    source 25
    target 137
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa19"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 257
    source 35
    target 137
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR28"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa11"
    ]
  ]
  edge [
    id 258
    source 1
    target 137
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR29"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa101"
    ]
  ]
  edge [
    id 259
    source 31
    target 142
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa76"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 261
    source 6
    target 145
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa137"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 263
    source 37
    target 145
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR30"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa74"
    ]
  ]
  edge [
    id 264
    source 44
    target 149
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa64"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 265
    source 149
    target 27
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa65"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 266
    source 6
    target 149
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR31"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa137"
    ]
  ]
  edge [
    id 267
    source 5
    target 149
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR32"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa12"
    ]
  ]
  edge [
    id 268
    source 38
    target 154
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa134"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 269
    source 154
    target 12
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa10"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 270
    source 22
    target 154
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR33"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa132"
    ]
  ]
  edge [
    id 271
    source 3
    target 158
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa88"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 273
    source 19
    target 158
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR34"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa87"
    ]
  ]
  edge [
    id 274
    source 2
    target 162
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa142"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 275
    source 24
    target 162
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa141"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 276
    source 20
    target 162
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa143"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 277
    source 162
    target 32
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa8"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 278
    source 28
    target 167
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa45"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 280
    source 40
    target 167
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR35"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa41"
    ]
  ]
  edge [
    id 281
    source 46
    target 171
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR36"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa42"
    ]
  ]
  edge [
    id 282
    source 9
    target 175
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa22"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 284
    source 13
    target 175
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR37"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa3"
    ]
  ]
  edge [
    id 285
    source 43
    target 179
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa86"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 286
    source 179
    target 19
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa87"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 287
    source 25
    target 179
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR38"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa19"
    ]
  ]
  edge [
    id 288
    source 183
    target 171
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa9"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 289
    source 183
    target 79
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR8"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa17"
    ]
  ]
]
