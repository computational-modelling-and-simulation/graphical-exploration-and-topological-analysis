# generated with VANTED V2.8.0 at Tue Apr 27 20:00:52 CEST 2021
graph [
  background_coloring "true"
  cluster_colors "255,128,128,255:0,0,0,255;128,255,128,255:0,0,0,255;128,128,255,255:0,0,0,255"
  clusterbackground_fill_outer_region "false"
  clusterbackground_grid 50.0
  clusterbackground_low_alpha 0.2
  clusterbackground_radius 200.0
  clusterbackground_space_fill "true"
  graphbackgroundcolor "#ffffff"
  sbml [
    model_meta_id "untitled"
    model_non_rdf_annotation ""
    model_notes ""
    model_sbml_id "untitled"
    namespace "xmlns:celldesigner=\"http://www.sbml.org/2001/ns/celldesigner\" "
    sbml_level "2"
    sbml_version "4"
  ]
  sbml_compartment_default [
    sbml_compartment_default_id "default"
    sbml_compartment_default_meta_id "default"
    sbml_compartment_default_non_rdf_annotation ""
    sbml_compartment_default_notes ""
    sbml_compartment_default_size "1.0"
    sbml_compartment_default_units "volume"
  ]
  sbml_compartment_s_id_ca2 [
    sbml_compartment_s_id_ca2_annotation ""
    sbml_compartment_s_id_ca2_id "s_id_ca2"
    sbml_compartment_s_id_ca2_meta_id "s_id_ca2"
    sbml_compartment_s_id_ca2_name "nucleus"
    sbml_compartment_s_id_ca2_non_rdf_annotation ""
    sbml_compartment_s_id_ca2_notes ""
    sbml_compartment_s_id_ca2_outside "s_id_ca3"
    sbml_compartment_s_id_ca2_size "1.0"
    sbml_compartment_s_id_ca2_units "volume"
  ]
  sbml_compartment_s_id_ca3 [
    sbml_compartment_s_id_ca3_annotation ""
    sbml_compartment_s_id_ca3_id "s_id_ca3"
    sbml_compartment_s_id_ca3_meta_id "s_id_ca3"
    sbml_compartment_s_id_ca3_name "cell"
    sbml_compartment_s_id_ca3_non_rdf_annotation ""
    sbml_compartment_s_id_ca3_notes ""
    sbml_compartment_s_id_ca3_outside "default"
    sbml_compartment_s_id_ca3_size "1.0"
    sbml_compartment_s_id_ca3_units "volume"
  ]
  sbml_parameter_1 [
    sbml_parameter_1_id "k107"
    sbml_parameter_1_name "k_diss_E3:Orf10:NEDD8_E3:Orf10:subs"
    sbml_parameter_1_units "substance"
    sbml_parameter_1_value 0.1
  ]
  sbml_parameter_10 [
    sbml_parameter_10_id "k22"
    sbml_parameter_10_name "k_deg_Zyg11B"
    sbml_parameter_10_units "substance"
    sbml_parameter_10_value 0.1
  ]
  sbml_parameter_11 [
    sbml_parameter_11_id "k2"
    sbml_parameter_11_name "k_transl_EloB"
    sbml_parameter_11_units "substance"
    sbml_parameter_11_value 0.1
  ]
  sbml_parameter_12 [
    sbml_parameter_12_id "k39"
    sbml_parameter_12_name "k_bind_Elo:Zyg11B_Cul2:Rbx1"
    sbml_parameter_12_units "substance"
    sbml_parameter_12_value 0.1
  ]
  sbml_parameter_13 [
    sbml_parameter_13_id "k85"
    sbml_parameter_13_name "k_transcr_NEDD8"
    sbml_parameter_13_units "substance"
    sbml_parameter_13_value 0.1
  ]
  sbml_parameter_14 [
    sbml_parameter_14_id "k11"
    sbml_parameter_14_name "k_transl_Rbx1"
    sbml_parameter_14_units "substance"
    sbml_parameter_14_value 0.1
  ]
  sbml_parameter_15 [
    sbml_parameter_15_id "k29"
    sbml_parameter_15_name "k_deg_E2_mRNA"
    sbml_parameter_15_units "substance"
    sbml_parameter_15_value 0.1
  ]
  sbml_parameter_16 [
    sbml_parameter_16_id "k37"
    sbml_parameter_16_name "k_bind_Cul2_Rbx1"
    sbml_parameter_16_units "substance"
    sbml_parameter_16_value 0.1
  ]
  sbml_parameter_17 [
    sbml_parameter_17_id "k131"
    sbml_parameter_17_name "k_syn_CSN5"
    sbml_parameter_17_units "substance"
    sbml_parameter_17_value 0.1
  ]
  sbml_parameter_18 [
    sbml_parameter_18_id "k96"
    sbml_parameter_18_name "k_deg_UBA3"
    sbml_parameter_18_units "substance"
    sbml_parameter_18_value 0.1
  ]
  sbml_parameter_19 [
    sbml_parameter_19_id "k79"
    sbml_parameter_19_name "diss_E2:E3:subs:ub"
    sbml_parameter_19_units "substance"
    sbml_parameter_19_value 0.1
  ]
  sbml_parameter_2 [
    sbml_parameter_2_id "k82"
    sbml_parameter_2_name "k_diss_E3:NEDD8_E3:subs"
    sbml_parameter_2_units "substance"
    sbml_parameter_2_value 0.1
  ]
  sbml_parameter_20 [
    sbml_parameter_20_id "k118"
    sbml_parameter_20_name "k_deg_Ubc12_mRNA"
    sbml_parameter_20_units "substance"
    sbml_parameter_20_value 0.1
  ]
  sbml_parameter_21 [
    sbml_parameter_21_id "k84"
    sbml_parameter_21_name "k_deubiquit_subs:ub"
    sbml_parameter_21_units "substance"
    sbml_parameter_21_value 0.1
  ]
  sbml_parameter_22 [
    sbml_parameter_22_id "k1"
    sbml_parameter_22_name "k_transcr_EloB"
    sbml_parameter_22_units "substance"
    sbml_parameter_22_value 0.1
  ]
  sbml_parameter_23 [
    sbml_parameter_23_id "k134"
    sbml_parameter_23_name "bind_E2_E3:Orf10:subs"
    sbml_parameter_23_units "substance"
    sbml_parameter_23_value 0.1
  ]
  sbml_parameter_24 [
    sbml_parameter_24_id "k120"
    sbml_parameter_24_name "deubiquit_E2:E3:Orf10:subs"
    sbml_parameter_24_units "substance"
    sbml_parameter_24_value 0.1
  ]
  sbml_parameter_25 [
    sbml_parameter_25_id "k15"
    sbml_parameter_25_name "k_transcr_DUB"
    sbml_parameter_25_units "substance"
    sbml_parameter_25_value 0.1
  ]
  sbml_parameter_26 [
    sbml_parameter_26_id "k119"
    sbml_parameter_26_name "k_ubiquit:E2_E3:Orf10:subs"
    sbml_parameter_26_units "substance"
    sbml_parameter_26_value 0.1
  ]
  sbml_parameter_27 [
    sbml_parameter_27_id "k116"
    sbml_parameter_27_name "k_transl_Ubc"
    sbml_parameter_27_units "substance"
    sbml_parameter_27_value 0.1
  ]
  sbml_parameter_28 [
    sbml_parameter_28_id "k36"
    sbml_parameter_28_name "diss_Elo:Zyg11B"
    sbml_parameter_28_units "substance"
    sbml_parameter_28_value 0.1
  ]
  sbml_parameter_29 [
    sbml_parameter_29_id "k123"
    sbml_parameter_29_name "diss_E2:E3:Orf10:subs"
    sbml_parameter_29_units "substance"
    sbml_parameter_29_value 0.1
  ]
  sbml_parameter_3 [
    sbml_parameter_3_id "k26"
    sbml_parameter_3_name "k_deg_Cul2"
    sbml_parameter_3_units "substance"
    sbml_parameter_3_value 0.1
  ]
  sbml_parameter_30 [
    sbml_parameter_30_id "k17"
    sbml_parameter_30_name "k_deg_EloB_mRNA"
    sbml_parameter_30_units "substance"
    sbml_parameter_30_value 0.1
  ]
  sbml_parameter_31 [
    sbml_parameter_31_id "k86"
    sbml_parameter_31_name "k_transcr_NAE1"
    sbml_parameter_31_units "substance"
    sbml_parameter_31_value 0.1
  ]
  sbml_parameter_32 [
    sbml_parameter_32_id "k71"
    sbml_parameter_32_name "k_bind_E3:subs_NEDD8"
    sbml_parameter_32_units "substance"
    sbml_parameter_32_value 0.1
  ]
  sbml_parameter_33 [
    sbml_parameter_33_id "k42"
    sbml_parameter_33_name "k_ubiquit_E2"
    sbml_parameter_33_units "substance"
    sbml_parameter_33_value 0.1
  ]
  sbml_parameter_34 [
    sbml_parameter_34_id "k44"
    sbml_parameter_34_name "diss_E3:subs"
    sbml_parameter_34_units "substance"
    sbml_parameter_34_value 0.1
  ]
  sbml_parameter_35 [
    sbml_parameter_35_id "k97"
    sbml_parameter_35_name "k_bind_NAE1_UBA3"
    sbml_parameter_35_units "substance"
    sbml_parameter_35_value 0.1
  ]
  sbml_parameter_36 [
    sbml_parameter_36_id "k35"
    sbml_parameter_36_name "k_bind_Elo_Zyg11B"
    sbml_parameter_36_units "substance"
    sbml_parameter_36_value 0.1
  ]
  sbml_parameter_37 [
    sbml_parameter_37_id "k130"
    sbml_parameter_37_name "k_deg_26S_proteasom"
    sbml_parameter_37_units "substance"
    sbml_parameter_37_value 0.1
  ]
  sbml_parameter_38 [
    sbml_parameter_38_id "k9"
    sbml_parameter_38_name "k_transl_EloC"
    sbml_parameter_38_units "substance"
    sbml_parameter_38_value 0.1
  ]
  sbml_parameter_39 [
    sbml_parameter_39_id "k63"
    sbml_parameter_39_name "k_bind_NEDD8_NAE"
    sbml_parameter_39_units "substance"
    sbml_parameter_39_value 0.1
  ]
  sbml_parameter_4 [
    sbml_parameter_4_id "k105"
    sbml_parameter_4_name "k_ubiquit_subs:E3:Orf10"
    sbml_parameter_4_units "substance"
    sbml_parameter_4_value 0.1
  ]
  sbml_parameter_40 [
    sbml_parameter_40_id "k33"
    sbml_parameter_40_name "k_bind_EloB_EloC"
    sbml_parameter_40_units "substance"
    sbml_parameter_40_value 0.1
  ]
  sbml_parameter_41 [
    sbml_parameter_41_id "k23"
    sbml_parameter_41_name "k_deg_Rbx1_mRNA"
    sbml_parameter_41_units "substance"
    sbml_parameter_41_value 0.1
  ]
  sbml_parameter_42 [
    sbml_parameter_42_id "k126"
    sbml_parameter_42_name "k_deg_Orf10_mRNA"
    sbml_parameter_42_units "substance"
    sbml_parameter_42_value 0.1
  ]
  sbml_parameter_43 [
    sbml_parameter_43_id "k133"
    sbml_parameter_43_name "k_deg_CSN5"
    sbml_parameter_43_units "substance"
    sbml_parameter_43_value 0.1
  ]
  sbml_parameter_44 [
    sbml_parameter_44_id "k81"
    sbml_parameter_44_name "diss_E3:subs"
    sbml_parameter_44_units "substance"
    sbml_parameter_44_value 0.1
  ]
  sbml_parameter_45 [
    sbml_parameter_45_id "k21"
    sbml_parameter_45_name "k_deg_Zyg11B_mRNA"
    sbml_parameter_45_units "substance"
    sbml_parameter_45_value 0.1
  ]
  sbml_parameter_46 [
    sbml_parameter_46_id "k4"
    sbml_parameter_46_name "k_transcr_Zyg11B"
    sbml_parameter_46_units "substance"
    sbml_parameter_46_value 0.1
  ]
  sbml_parameter_47 [
    sbml_parameter_47_id "k43"
    sbml_parameter_47_name "k_bind_E3_subs"
    sbml_parameter_47_units "substance"
    sbml_parameter_47_value 0.1
  ]
  sbml_parameter_48 [
    sbml_parameter_48_id "k110"
    sbml_parameter_48_name "k_bind_Orf10_E3"
    sbml_parameter_48_units "substance"
    sbml_parameter_48_value 0.1
  ]
  sbml_parameter_49 [
    sbml_parameter_49_id "k5"
    sbml_parameter_49_name "k_transcr_Rbx1"
    sbml_parameter_49_units "substance"
    sbml_parameter_49_value 0.1
  ]
  sbml_parameter_5 [
    sbml_parameter_5_id "k30"
    sbml_parameter_5_name "k_deg_E2"
    sbml_parameter_5_units "substance"
    sbml_parameter_5_value 0.1
  ]
  sbml_parameter_50 [
    sbml_parameter_50_id "k25"
    sbml_parameter_50_name "k_deg_Cul2_mRNA"
    sbml_parameter_50_units "substance"
    sbml_parameter_50_value 0.1
  ]
  sbml_parameter_51 [
    sbml_parameter_51_id "k16"
    sbml_parameter_51_name "k_transl_DUB"
    sbml_parameter_51_units "substance"
    sbml_parameter_51_value 0.1
  ]
  sbml_parameter_52 [
    sbml_parameter_52_id "k90"
    sbml_parameter_52_name "k_transl_NEDD8"
    sbml_parameter_52_units "substance"
    sbml_parameter_52_value 0.1
  ]
  sbml_parameter_53 [
    sbml_parameter_53_id "k129"
    sbml_parameter_53_name "k_syn_26S_proteasom"
    sbml_parameter_53_units "substance"
    sbml_parameter_53_value 0.1
  ]
  sbml_parameter_54 [
    sbml_parameter_54_id "k38"
    sbml_parameter_54_name "diss_Cul2:Rbx1"
    sbml_parameter_54_units "substance"
    sbml_parameter_54_value 0.1
  ]
  sbml_parameter_55 [
    sbml_parameter_55_id "k117"
    sbml_parameter_55_name "k_deg_Ubc12"
    sbml_parameter_55_units "substance"
    sbml_parameter_55_value 0.1
  ]
  sbml_parameter_56 [
    sbml_parameter_56_id "k83"
    sbml_parameter_56_name "k_deg_substrate:ub"
    sbml_parameter_56_units "substance"
    sbml_parameter_56_value 0.1
  ]
  sbml_parameter_57 [
    sbml_parameter_57_id "k112"
    sbml_parameter_57_name "k_bind_E3:Orf10_subs"
    sbml_parameter_57_units "substance"
    sbml_parameter_57_value 0.1
  ]
  sbml_parameter_58 [
    sbml_parameter_58_id "k31"
    sbml_parameter_58_name "k_deg_DUB_mRNA"
    sbml_parameter_58_units "substance"
    sbml_parameter_58_value 0.1
  ]
  sbml_parameter_59 [
    sbml_parameter_59_id "k95"
    sbml_parameter_59_name "k_deg_UBA3_mRNA"
    sbml_parameter_59_units "substance"
    sbml_parameter_59_value 0.1
  ]
  sbml_parameter_6 [
    sbml_parameter_6_id "k89"
    sbml_parameter_6_name "k_transl_NAE1"
    sbml_parameter_6_units "substance"
    sbml_parameter_6_value 0.0
  ]
  sbml_parameter_60 [
    sbml_parameter_60_id "k92"
    sbml_parameter_60_name "k_deg_NEDD8"
    sbml_parameter_60_units "substance"
    sbml_parameter_60_value 0.1
  ]
  sbml_parameter_61 [
    sbml_parameter_61_id "k3"
    sbml_parameter_61_name "k_transcr_EloC"
    sbml_parameter_61_units "substance"
    sbml_parameter_61_value 0.1
  ]
  sbml_parameter_62 [
    sbml_parameter_62_id "k69"
    sbml_parameter_62_name "k_bind_Ubc12_NAE:NEDD8"
    sbml_parameter_62_units "substance"
    sbml_parameter_62_value 0.1
  ]
  sbml_parameter_63 [
    sbml_parameter_63_id "k18"
    sbml_parameter_63_name "k_deg_EloB"
    sbml_parameter_63_units "substance"
    sbml_parameter_63_value 0.1
  ]
  sbml_parameter_64 [
    sbml_parameter_64_id "k72"
    sbml_parameter_64_name "k_ubiquit:E2_E3:subs"
    sbml_parameter_64_units "substance"
    sbml_parameter_64_value 0.1
  ]
  sbml_parameter_65 [
    sbml_parameter_65_id "k7"
    sbml_parameter_65_name "k_transcr_E1"
    sbml_parameter_65_units "substance"
    sbml_parameter_65_value 0.1
  ]
  sbml_parameter_66 [
    sbml_parameter_66_id "k68"
    sbml_parameter_66_name "diss:NAE:Pevonedistat"
    sbml_parameter_66_units "substance"
    sbml_parameter_66_value 0.1
  ]
  sbml_parameter_67 [
    sbml_parameter_67_id "k19"
    sbml_parameter_67_name "k_deg_EloC_mRNA"
    sbml_parameter_67_units "substance"
    sbml_parameter_67_value 0.1
  ]
  sbml_parameter_68 [
    sbml_parameter_68_id "k67"
    sbml_parameter_68_name "k_bind_NAE_Pevonedistat"
    sbml_parameter_68_units "substance"
    sbml_parameter_68_value 0.1
  ]
  sbml_parameter_69 [
    sbml_parameter_69_id "k77"
    sbml_parameter_69_name "k_ubiquit_subs:E3"
    sbml_parameter_69_units "substance"
    sbml_parameter_69_value 0.1
  ]
  sbml_parameter_7 [
    sbml_parameter_7_id "k10"
    sbml_parameter_7_name "k_transl_Zyg11B"
    sbml_parameter_7_units "substance"
    sbml_parameter_7_value 0.1
  ]
  sbml_parameter_70 [
    sbml_parameter_70_id "k12"
    sbml_parameter_70_name "k_transl_Cul2"
    sbml_parameter_70_units "substance"
    sbml_parameter_70_value 0.1
  ]
  sbml_parameter_71 [
    sbml_parameter_71_id "k32"
    sbml_parameter_71_name "k_deg_DUB"
    sbml_parameter_71_units "substance"
    sbml_parameter_71_value 0.1
  ]
  sbml_parameter_72 [
    sbml_parameter_72_id "k88"
    sbml_parameter_72_name "k_transl_UBA3"
    sbml_parameter_72_units "substance"
    sbml_parameter_72_value 0.1
  ]
  sbml_parameter_73 [
    sbml_parameter_73_id "k127"
    sbml_parameter_73_name "k_deg_Orf10"
    sbml_parameter_73_units "substance"
    sbml_parameter_73_value 0.1
  ]
  sbml_parameter_74 [
    sbml_parameter_74_id "k76"
    sbml_parameter_74_name "diss_E2:E3:subs"
    sbml_parameter_74_units "substance"
    sbml_parameter_74_value 0.1
  ]
  sbml_parameter_75 [
    sbml_parameter_75_id "k87"
    sbml_parameter_75_name "k_transcr_UBA3"
    sbml_parameter_75_units "substance"
    sbml_parameter_75_value 0.1
  ]
  sbml_parameter_76 [
    sbml_parameter_76_id "k94"
    sbml_parameter_76_name "k_deg_NAE1"
    sbml_parameter_76_units "substance"
    sbml_parameter_76_value 0.1
  ]
  sbml_parameter_77 [
    sbml_parameter_77_id "k115"
    sbml_parameter_77_name "k_transcr_Ubc12"
    sbml_parameter_77_units "substance"
    sbml_parameter_77_value 0.1
  ]
  sbml_parameter_78 [
    sbml_parameter_78_id "k98"
    sbml_parameter_78_name "diss_NAE"
    sbml_parameter_78_units "substance"
    sbml_parameter_78_value 0.1
  ]
  sbml_parameter_79 [
    sbml_parameter_79_id "k8"
    sbml_parameter_79_name "k_transcr_E2"
    sbml_parameter_79_units "substance"
    sbml_parameter_79_value 0.1
  ]
  sbml_parameter_8 [
    sbml_parameter_8_id "k125"
    sbml_parameter_8_name "k_transl_Orf10"
    sbml_parameter_8_units "substance"
    sbml_parameter_8_value 0.1
  ]
  sbml_parameter_80 [
    sbml_parameter_80_id "k13"
    sbml_parameter_80_name "k_transl_E1"
    sbml_parameter_80_units "substance"
    sbml_parameter_80_value 0.1
  ]
  sbml_parameter_81 [
    sbml_parameter_81_id "k28"
    sbml_parameter_81_name "k_deg_E1"
    sbml_parameter_81_units "substance"
    sbml_parameter_81_value 0.1
  ]
  sbml_parameter_82 [
    sbml_parameter_82_id "k24"
    sbml_parameter_82_name "k_deg_Rbx1"
    sbml_parameter_82_units "substance"
    sbml_parameter_82_value 0.1
  ]
  sbml_parameter_83 [
    sbml_parameter_83_id "k114"
    sbml_parameter_83_name "k_bind_E3:Orf10:subs_NEDD8"
    sbml_parameter_83_units "substance"
    sbml_parameter_83_value 0.1
  ]
  sbml_parameter_84 [
    sbml_parameter_84_id "k93"
    sbml_parameter_84_name "k_deg_NAE1_mRNA"
    sbml_parameter_84_units "substance"
    sbml_parameter_84_value 0.1
  ]
  sbml_parameter_85 [
    sbml_parameter_85_id "k91"
    sbml_parameter_85_name "k_deg_NEDD8_mRNA"
    sbml_parameter_85_units "substance"
    sbml_parameter_85_value 0.1
  ]
  sbml_parameter_86 [
    sbml_parameter_86_id "k34"
    sbml_parameter_86_name "diss_EloB:EloC"
    sbml_parameter_86_units "substance"
    sbml_parameter_86_value 0.1
  ]
  sbml_parameter_87 [
    sbml_parameter_87_id "k113"
    sbml_parameter_87_name "diss_E3:Orf10:subs"
    sbml_parameter_87_units "substance"
    sbml_parameter_87_value 0.1
  ]
  sbml_parameter_88 [
    sbml_parameter_88_id "k106"
    sbml_parameter_88_name "diss_E3:Orf10:subs:ub"
    sbml_parameter_88_units "substance"
    sbml_parameter_88_value 0.1
  ]
  sbml_parameter_89 [
    sbml_parameter_89_id "k6"
    sbml_parameter_89_name "k_transcr_Cul2"
    sbml_parameter_89_units "substance"
    sbml_parameter_89_value 0.1
  ]
  sbml_parameter_9 [
    sbml_parameter_9_id "k111"
    sbml_parameter_9_name "diss_E3:Orf10"
    sbml_parameter_9_units "substance"
    sbml_parameter_9_value 0.1
  ]
  sbml_parameter_90 [
    sbml_parameter_90_id "k128"
    sbml_parameter_90_name "k_transcr_Orf10"
    sbml_parameter_90_units "substance"
    sbml_parameter_90_value 0.1
  ]
  sbml_parameter_91 [
    sbml_parameter_91_id "k75"
    sbml_parameter_91_name "k_bind_E2_E3:subs"
    sbml_parameter_91_units "substance"
    sbml_parameter_91_value 0.1
  ]
  sbml_parameter_92 [
    sbml_parameter_92_id "k41"
    sbml_parameter_92_name "k_ubiquit_E1"
    sbml_parameter_92_units "substance"
    sbml_parameter_92_value 0.1
  ]
  sbml_parameter_93 [
    sbml_parameter_93_id "k40"
    sbml_parameter_93_name "diss_E3"
    sbml_parameter_93_units "substance"
    sbml_parameter_93_value 0.1
  ]
  sbml_parameter_94 [
    sbml_parameter_94_id "k20"
    sbml_parameter_94_name "k_deg_EloC"
    sbml_parameter_94_units "substance"
    sbml_parameter_94_value 0.1
  ]
  sbml_parameter_95 [
    sbml_parameter_95_id "k27"
    sbml_parameter_95_name "k_deg_E1_mRNA"
    sbml_parameter_95_units "substance"
    sbml_parameter_95_value 0.1
  ]
  sbml_parameter_96 [
    sbml_parameter_96_id "k74"
    sbml_parameter_96_name "deubiquit_E2:E3:subs"
    sbml_parameter_96_units "substance"
    sbml_parameter_96_value 0.1
  ]
  sbml_parameter_97 [
    sbml_parameter_97_id "k14"
    sbml_parameter_97_name "k_transl_E2"
    sbml_parameter_97_units "substance"
    sbml_parameter_97_value 0.0
  ]
  sbml_unit_definition_1 [
    sbml_unit_definition_1_id "area"
    sbml_unit_definition_1_name "area"
    sbml_unit_definition_1_sub_unit_1_ "(1.0 * 10^0 * metre)^2.0"
    sbml_unit_definition_1unit "(1.0 * 10^0 * metre)^2.0"
  ]
  sbml_unit_definition_2 [
    sbml_unit_definition_2_id "time"
    sbml_unit_definition_2_name "time"
    sbml_unit_definition_2_sub_unit_1_ "(1.0 * 10^0 * second)^1.0"
    sbml_unit_definition_2unit "(1.0 * 10^0 * second)^1.0"
  ]
  sbml_unit_definition_3 [
    sbml_unit_definition_3_id "length"
    sbml_unit_definition_3_name "length"
    sbml_unit_definition_3_sub_unit_1_ "(1.0 * 10^0 * metre)^1.0"
    sbml_unit_definition_3unit "(1.0 * 10^0 * metre)^1.0"
  ]
  sbml_unit_definition_4 [
    sbml_unit_definition_4_id "substance"
    sbml_unit_definition_4_name "substance"
    sbml_unit_definition_4_sub_unit_1_ "(1.0 * 10^0 * mole)^1.0"
    sbml_unit_definition_4unit "(1.0 * 10^0 * mole)^1.0"
  ]
  sbml_unit_definition_5 [
    sbml_unit_definition_5_id "volume"
    sbml_unit_definition_5_name "volume"
    sbml_unit_definition_5_sub_unit_1_ "(1.0 * 10^0 * litre)^1.0"
    sbml_unit_definition_5unit "(1.0 * 10^0 * litre)^1.0"
  ]
  directed 0
  node [
    id 1
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 200.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nae:pevonedistat__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa12"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa12"
      minerva_name "NAE:Pevonedistat"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NAE1"
      minerva_ref_link2 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:145535"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=UBA3"
      minerva_ref_type__resource1 "HGNC_SYMBOL__NAE1"
      minerva_ref_type__resource2 "CHEBI__CHEBI:145535"
      minerva_ref_type__resource3 "HGNC_SYMBOL__UBA3"
      minerva_type "Complex"
      minerva_x 868.0
      minerva_y 1924.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa12"
      species_meta_id "s_id_csa12"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 2
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 300.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "degradation__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa33"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa27"
      minerva_elementId10 "sa170"
      minerva_elementId11 "sa30"
      minerva_elementId12 "sa185"
      minerva_elementId13 "sa40"
      minerva_elementId14 "sa37"
      minerva_elementId15 "sa182"
      minerva_elementId16 "sa25"
      minerva_elementId17 "sa258"
      minerva_elementId18 "sa268"
      minerva_elementId19 "sa32"
      minerva_elementId2 "sa34"
      minerva_elementId20 "sa269"
      minerva_elementId21 "sa257"
      minerva_elementId22 "sa265"
      minerva_elementId23 "sa279"
      minerva_elementId24 "sa31"
      minerva_elementId25 "sa26"
      minerva_elementId26 "sa39"
      minerva_elementId27 "sa35"
      minerva_elementId28 "sa181"
      minerva_elementId29 "sa183"
      minerva_elementId3 "sa28"
      minerva_elementId4 "sa184"
      minerva_elementId5 "sa266"
      minerva_elementId6 "sa36"
      minerva_elementId7 "sa33"
      minerva_elementId8 "sa187"
      minerva_elementId9 "sa29"
      minerva_name "degradation"
      minerva_type "Degraded"
      minerva_x 384.5
      minerva_x10 3126.0
      minerva_x11 522.5
      minerva_x12 381.0
      minerva_x13 2746.5
      minerva_x14 385.5
      minerva_x15 521.0
      minerva_x16 386.0
      minerva_x17 379.5454545454545
      minerva_x18 3130.0
      minerva_x19 521.5
      minerva_x2 519.5
      minerva_x20 2437.0
      minerva_x21 1616.5454545454545
      minerva_x22 868.0
      minerva_x23 1948.5
      minerva_x24 384.5
      minerva_x25 522.5
      minerva_x26 366.5
      minerva_x27 384.5
      minerva_x28 378.0
      minerva_x29 377.0
      minerva_x3 522.5
      minerva_x4 522.0
      minerva_x5 695.0
      minerva_x6 1713.5
      minerva_x7 384.5
      minerva_x8 522.0
      minerva_x9 383.5
      minerva_y 596.0
      minerva_y10 967.5
      minerva_y11 716.0
      minerva_y12 2018.5
      minerva_y13 1313.0
      minerva_y14 354.0
      minerva_y15 1780.5
      minerva_y16 476.5
      minerva_y17 2226.272727272727
      minerva_y18 1091.5
      minerva_y19 838.0
      minerva_y2 1081.0
      minerva_y20 1060.5
      minerva_y21 2230.272727272727
      minerva_y22 1168.5
      minerva_y23 223.5
      minerva_y24 836.0
      minerva_y25 477.0
      minerva_y26 1325.0
      minerva_y27 230.0
      minerva_y28 1783.5
      minerva_y29 1892.5
      minerva_y3 596.0
      minerva_y4 1892.5
      minerva_y5 1168.5
      minerva_y6 228.0
      minerva_y7 1078.0
      minerva_y8 2012.5
      minerva_y9 716.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa33"
      species_meta_id "s_id_sa33"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 3
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 400.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "e2__cell__ubiquitinated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa42"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa42"
      minerva_name "E2"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=UBE2"
      minerva_ref_type__resource1 "HGNC_SYMBOL__UBE2"
      minerva_state1 "UBIQUITINATED"
      minerva_type "Protein"
      minerva_x 1950.5
      minerva_y 302.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa42"
      species_meta_id "s_id_sa42"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 4
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 500.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "elob__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa1"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa1"
      minerva_former_symbols "TCEB2"
      minerva_name "ELOB"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_007108"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ELOB"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/6923"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000103363"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11619"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/Q15370"
      minerva_ref_type__resource1 "REFSEQ__NM_007108"
      minerva_ref_type__resource2 "HGNC_SYMBOL__ELOB"
      minerva_ref_type__resource3 "ENTREZ__6923"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000103363"
      minerva_ref_type__resource5 "HGNC__11619"
      minerva_ref_type__resource6 "UNIPROT__Q15370"
      minerva_type "Gene"
      minerva_x 243.0
      minerva_y 401.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa1"
      species_meta_id "s_id_sa1"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 5
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 600.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "dub__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa8"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa8"
      minerva_name "DUB"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Gene"
      minerva_x 259.0
      minerva_y 1248.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa8"
      species_meta_id "s_id_sa8"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 6
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 700.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "pevonedistat__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa54"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa54"
      minerva_name "Pevonedistat"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:145535"
      minerva_ref_type__resource1 "CHEBI__CHEBI:145535"
      minerva_type "Drug"
      minerva_x 691.0
      minerva_y 1924.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa54"
      species_meta_id "s_id_sa54"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 7
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 800.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "uba3__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa180"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa180"
      minerva_former_symbols "UBE1C"
      minerva_name "UBA3"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000144744"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=UBA3"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/Q8TBC4"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_198195"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/9039"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12470"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000144744"
      minerva_ref_type__resource2 "HGNC_SYMBOL__UBA3"
      minerva_ref_type__resource3 "UNIPROT__Q8TBC4"
      minerva_ref_type__resource4 "REFSEQ__NM_198195"
      minerva_ref_type__resource5 "ENTREZ__9039"
      minerva_ref_type__resource6 "HGNC__12470"
      minerva_type "Gene"
      minerva_x 252.0
      minerva_y 1930.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa180"
      species_meta_id "s_id_sa180"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 9
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 1000.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "26s_minus_proteasom__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa25"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa25"
      minerva_fullName "proteasome complex"
      minerva_name "26S-proteasom"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0000502"
      minerva_ref_type__resource1 "GO__GO:0000502"
      minerva_type "Complex"
      minerva_x 3006.0
      minerva_y 1092.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa25"
      species_meta_id "s_id_csa25"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 10
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 1100.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cul2_space_ubiquitin_space_ligase:n8:orf10:substrate__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa33"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa33"
      minerva_name "Cul2 ubiquitin ligase:N8:Orf10:substrate"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ELOC"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ELOB"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CUL2"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ZYG11B"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=RBX1"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NEDD8"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/BCD58762"
      minerva_ref_type__resource1 "HGNC_SYMBOL__ELOC"
      minerva_ref_type__resource2 "HGNC_SYMBOL__ELOB"
      minerva_ref_type__resource3 "HGNC_SYMBOL__CUL2"
      minerva_ref_type__resource4 "HGNC_SYMBOL__ZYG11B"
      minerva_ref_type__resource5 "HGNC_SYMBOL__RBX1"
      minerva_ref_type__resource6 "HGNC_SYMBOL__NEDD8"
      minerva_ref_type__resource7 "NCBI_PROTEIN__BCD58762"
      minerva_type "Complex"
      minerva_x 1777.5
      minerva_y 1449.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa33"
      species_meta_id "s_id_csa33"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 12
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 200.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ubit_underscore_traget__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa171"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa105"
      minerva_elementId2 "sa171"
      minerva_name "ubit_traget"
      minerva_type "Protein"
      minerva_x 1239.0
      minerva_x2 2637.0
      minerva_y 1029.5
      minerva_y2 967.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa171"
      species_meta_id "s_id_sa171"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 13
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 300.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cul2_space_ubiquitin_space_ligase:orf10__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa35"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa35"
      minerva_name "Cul2 ubiquitin ligase:Orf10"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ELOC"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ELOB"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CUL2"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ZYG11B"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=RBX1"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/BCD58762"
      minerva_ref_type__resource1 "HGNC_SYMBOL__ELOC"
      minerva_ref_type__resource2 "HGNC_SYMBOL__ELOB"
      minerva_ref_type__resource3 "HGNC_SYMBOL__CUL2"
      minerva_ref_type__resource4 "HGNC_SYMBOL__ZYG11B"
      minerva_ref_type__resource5 "HGNC_SYMBOL__RBX1"
      minerva_ref_type__resource6 "NCBI_PROTEIN__BCD58762"
      minerva_type "Complex"
      minerva_x 1047.5
      minerva_y 1450.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa35"
      species_meta_id "s_id_csa35"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 14
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 400.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf10_space_(_plus_)ss_space_sgmrna__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa278"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa278"
      minerva_name "Orf10 (+)ss sgmRNA"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/BCD58762"
      minerva_ref_type__resource1 "NCBI_PROTEIN__BCD58762"
      minerva_type "RNA"
      minerva_x 709.5
      minerva_y 1084.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa278"
      species_meta_id "s_id_sa278"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 19
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 900.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "csn5__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa23"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa23"
      minerva_name "CSN5"
      minerva_type "Complex"
      minerva_x 2341.0
      minerva_y 1060.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa23"
      species_meta_id "s_id_csa23"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 21
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 1100.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "zyg11b:elobc__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa2"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa2"
      minerva_name "Zyg11B:EloBC"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ELOC"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ELOB"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ZYG11B"
      minerva_ref_type__resource1 "HGNC_SYMBOL__ELOC"
      minerva_ref_type__resource2 "HGNC_SYMBOL__ELOB"
      minerva_ref_type__resource3 "HGNC_SYMBOL__ZYG11B"
      minerva_type "Complex"
      minerva_x 743.0
      minerva_y 662.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa2"
      species_meta_id "s_id_csa2"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 22
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 1200.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cul2_space_ubiquitin_space_ligase:n8:e2:substrate_minus_ub__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa18"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa18"
      minerva_name "Cul2 ubiquitin ligase:N8:E2:substrate-Ub"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ELOC"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ELOB"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CUL2"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ZYG11B"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=RBX1"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NEDD8"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=UBE2"
      minerva_ref_type__resource1 "HGNC_SYMBOL__ELOC"
      minerva_ref_type__resource2 "HGNC_SYMBOL__ELOB"
      minerva_ref_type__resource3 "HGNC_SYMBOL__CUL2"
      minerva_ref_type__resource4 "HGNC_SYMBOL__ZYG11B"
      minerva_ref_type__resource5 "HGNC_SYMBOL__RBX1"
      minerva_ref_type__resource6 "HGNC_SYMBOL__NEDD8"
      minerva_ref_type__resource7 "HGNC_SYMBOL__UBE2"
      minerva_type "Complex"
      minerva_x 2408.5
      minerva_y 782.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa18"
      species_meta_id "s_id_csa18"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 23
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 200.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cul2_space_ubiquitin_space_ligase:n8:substrate_minus_ub__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa20"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa20"
      minerva_name "Cul2 ubiquitin ligase:N8:substrate-Ub"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ELOC"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ELOB"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CUL2"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ZYG11B"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=RBX1"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NEDD8"
      minerva_ref_type__resource1 "HGNC_SYMBOL__ELOC"
      minerva_ref_type__resource2 "HGNC_SYMBOL__ELOB"
      minerva_ref_type__resource3 "HGNC_SYMBOL__CUL2"
      minerva_ref_type__resource4 "HGNC_SYMBOL__ZYG11B"
      minerva_ref_type__resource5 "HGNC_SYMBOL__RBX1"
      minerva_ref_type__resource6 "HGNC_SYMBOL__NEDD8"
      minerva_type "Complex"
      minerva_x 2703.5
      minerva_y 783.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa20"
      species_meta_id "s_id_csa20"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 24
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 300.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cul2_space_ubiquitin_space_ligase:n8:orf10:e2:substrate_minus_ub__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa31"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa31"
      minerva_name "Cul2 ubiquitin ligase:N8:Orf10:E2:substrate-Ub"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ELOC"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ELOB"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CUL2"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ZYG11B"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=RBX1"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NEDD8"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/BCD58762"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=UBE2"
      minerva_ref_type__resource1 "HGNC_SYMBOL__ELOC"
      minerva_ref_type__resource2 "HGNC_SYMBOL__ELOB"
      minerva_ref_type__resource3 "HGNC_SYMBOL__CUL2"
      minerva_ref_type__resource4 "HGNC_SYMBOL__ZYG11B"
      minerva_ref_type__resource5 "HGNC_SYMBOL__RBX1"
      minerva_ref_type__resource6 "HGNC_SYMBOL__NEDD8"
      minerva_ref_type__resource7 "NCBI_PROTEIN__BCD58762"
      minerva_ref_type__resource8 "HGNC_SYMBOL__UBE2"
      minerva_type "Complex"
      minerva_x 2408.5
      minerva_y 1450.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa31"
      species_meta_id "s_id_csa31"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 25
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 400.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "elob:eloc__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa1"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa1"
      minerva_name "ELOB:ELOC"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ELOC"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ELOB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__ELOC"
      minerva_ref_type__resource2 "HGNC_SYMBOL__ELOB"
      minerva_type "Complex"
      minerva_x 702.0
      minerva_y 457.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa1"
      species_meta_id "s_id_csa1"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 26
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 500.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "e2__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa7"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa7"
      minerva_name "E2"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=UBE2"
      minerva_ref_type__resource1 "HGNC_SYMBOL__UBE2"
      minerva_type "Gene"
      minerva_x 242.0
      minerva_y 279.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa7"
      species_meta_id "s_id_sa7"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 27
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 600.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nae__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa9"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa9"
      minerva_name "NAE"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NAE1"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=UBA3"
      minerva_ref_type__resource1 "HGNC_SYMBOL__NAE1"
      minerva_ref_type__resource2 "HGNC_SYMBOL__UBA3"
      minerva_type "Complex"
      minerva_x 690.0
      minerva_y 1820.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa9"
      species_meta_id "s_id_csa9"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 28
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 700.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cul2_space_ubiquitin_space_ligase:orf10:substrate__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa34"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa34"
      minerva_name "Cul2 ubiquitin ligase:Orf10:substrate"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ELOC"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ELOB"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CUL2"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=RBX1"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ZYg11B"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/BCD58762"
      minerva_ref_type__resource1 "HGNC_SYMBOL__ELOC"
      minerva_ref_type__resource2 "HGNC_SYMBOL__ELOB"
      minerva_ref_type__resource3 "HGNC_SYMBOL__CUL2"
      minerva_ref_type__resource4 "HGNC_SYMBOL__RBX1"
      minerva_ref_type__resource5 "HGNC_SYMBOL__ZYg11B"
      minerva_ref_type__resource6 "NCBI_PROTEIN__BCD58762"
      minerva_type "Complex"
      minerva_x 1418.5
      minerva_y 1451.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa34"
      species_meta_id "s_id_csa34"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 30
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 900.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nae1__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa179"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa179"
      minerva_former_symbols "APPBP1"
      minerva_name "NAE1"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NAE1"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/621"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/8883"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/Q13564"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003905"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000159593"
      minerva_ref_type__resource1 "HGNC_SYMBOL__NAE1"
      minerva_ref_type__resource2 "HGNC__621"
      minerva_ref_type__resource3 "ENTREZ__8883"
      minerva_ref_type__resource4 "UNIPROT__Q13564"
      minerva_ref_type__resource5 "REFSEQ__NM_003905"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000159593"
      minerva_type "Gene"
      minerva_x 251.0
      minerva_y 1817.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa179"
      species_meta_id "s_id_sa179"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 31
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 1000.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ube2m__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa256"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa256"
      minerva_name "UBE2M"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12491"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=UBE2M"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P61081"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/9040"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003969"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000130725"
      minerva_ref_type__resource1 "HGNC__12491"
      minerva_ref_type__resource2 "HGNC_SYMBOL__UBE2M"
      minerva_ref_type__resource3 "UNIPROT__P61081"
      minerva_ref_type__resource4 "ENTREZ__9040"
      minerva_ref_type__resource5 "REFSEQ__NM_003969"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000130725"
      minerva_type "Gene"
      minerva_x 253.0
      minerva_y 2141.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa256"
      species_meta_id "s_id_sa256"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 34
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 200.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ppi__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa46"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa46"
      minerva_fullName "diphosphate ion"
      minerva_name "PPi"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:35782"
      minerva_ref_type__resource1 "CHEBI__CHEBI:35782"
      minerva_synonyms "PPi; diphosphate ions"
      minerva_type "Simple molecule"
      minerva_x 1911.5
      minerva_y 103.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa46"
      species_meta_id "s_id_sa46"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 35
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 300.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cul2__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa5"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa5"
      minerva_name "CUL2"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/Q13617"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/2552"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CUL2"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/8453"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003591"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000108094"
      minerva_ref_type__resource1 "UNIPROT__Q13617"
      minerva_ref_type__resource2 "HGNC__2552"
      minerva_ref_type__resource3 "HGNC_SYMBOL__CUL2"
      minerva_ref_type__resource4 "ENTREZ__8453"
      minerva_ref_type__resource5 "REFSEQ__NM_003591"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000108094"
      minerva_type "Gene"
      minerva_x 244.0
      minerva_y 945.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa5"
      species_meta_id "s_id_sa5"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 36
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 400.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "amp__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa45"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa45"
      minerva_fullName "adenosine 5'-monophosphate(2-)"
      minerva_name "AMP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:456215"
      minerva_ref_type__resource1 "CHEBI__CHEBI:456215"
      minerva_synonyms "AMP; AMP dianion; AMP(2-); Adenosine-5-monophosphate dianion; Adenosine-5-monophosphate(2-)"
      minerva_type "Simple molecule"
      minerva_x 1868.0
      minerva_y 91.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa45"
      species_meta_id "s_id_sa45"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 37
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 500.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "orf10__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa55"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa55"
      minerva_name "Orf10"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/BCD58762"
      minerva_ref_type__resource1 "NCBI_PROTEIN__BCD58762"
      minerva_type "Protein"
      minerva_x 868.0
      minerva_y 1084.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa55"
      species_meta_id "s_id_sa55"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 38
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 600.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "zyg11b__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa3"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa3"
      minerva_former_symbols "ZYG11"
      minerva_name "ZYG11B"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/Q9C0D3"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ZYG11B"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/79699"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000162378"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_024646"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/25820"
      minerva_ref_type__resource1 "UNIPROT__Q9C0D3"
      minerva_ref_type__resource2 "HGNC_SYMBOL__ZYG11B"
      minerva_ref_type__resource3 "ENTREZ__79699"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000162378"
      minerva_ref_type__resource5 "REFSEQ__NM_024646"
      minerva_ref_type__resource6 "HGNC__25820"
      minerva_type "Gene"
      minerva_x 243.0
      minerva_y 641.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa3"
      species_meta_id "s_id_sa3"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 40
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 800.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "eloc__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa2"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa2"
      minerva_former_symbols "TCEB1"
      minerva_name "ELOC"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ELOC"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q15369"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/6921"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000154582"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11617"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_005648"
      minerva_ref_type__resource1 "HGNC_SYMBOL__ELOC"
      minerva_ref_type__resource2 "UNIPROT__Q15369"
      minerva_ref_type__resource3 "ENTREZ__6921"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000154582"
      minerva_ref_type__resource5 "HGNC__11617"
      minerva_ref_type__resource6 "REFSEQ__NM_005648"
      minerva_type "Gene"
      minerva_x 244.0
      minerva_y 522.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa2"
      species_meta_id "s_id_sa2"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 41
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 900.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cul2_space_ubiquitin_space_ligase:n8:orf10__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa29"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa29"
      minerva_name "Cul2 ubiquitin ligase:N8:Orf10"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ELOC"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ELOB"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CUL2"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ZYG11B"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=RBX1"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NEDD8"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/BCD58762"
      minerva_ref_type__resource1 "HGNC_SYMBOL__ELOC"
      minerva_ref_type__resource2 "HGNC_SYMBOL__ELOB"
      minerva_ref_type__resource3 "HGNC_SYMBOL__CUL2"
      minerva_ref_type__resource4 "HGNC_SYMBOL__ZYG11B"
      minerva_ref_type__resource5 "HGNC_SYMBOL__RBX1"
      minerva_ref_type__resource6 "HGNC_SYMBOL__NEDD8"
      minerva_ref_type__resource7 "NCBI_PROTEIN__BCD58762"
      minerva_type "Complex"
      minerva_x 2997.5
      minerva_y 1450.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa29"
      species_meta_id "s_id_csa29"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 42
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 1000.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cul2_space_ubiquitin_space_ligase:n8:orf10:e2_minus_ub:substrate__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa32"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa32"
      minerva_name "Cul2 ubiquitin ligase:N8:Orf10:E2-Ub:substrate"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ELOC"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ELOB"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CUL2"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ZYG11B"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=RBX1"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NEDD8"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/BCD58762"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=UBE2"
      minerva_ref_type__resource1 "HGNC_SYMBOL__ELOC"
      minerva_ref_type__resource2 "HGNC_SYMBOL__ELOB"
      minerva_ref_type__resource3 "HGNC_SYMBOL__CUL2"
      minerva_ref_type__resource4 "HGNC_SYMBOL__ZYG11B"
      minerva_ref_type__resource5 "HGNC_SYMBOL__RBX1"
      minerva_ref_type__resource6 "HGNC_SYMBOL__NEDD8"
      minerva_ref_type__resource7 "NCBI_PROTEIN__BCD58762"
      minerva_ref_type__resource8 "HGNC_SYMBOL__UBE2"
      minerva_type "Complex"
      minerva_x 2124.5
      minerva_y 1449.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa32"
      species_meta_id "s_id_csa32"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 44
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 1200.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ube2m:nedd8__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa14"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa14"
      minerva_name "UBE2M:NEDD8"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=UBE2M"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NEDD8"
      minerva_ref_type__resource1 "HGNC_SYMBOL__UBE2M"
      minerva_ref_type__resource2 "HGNC_SYMBOL__NEDD8"
      minerva_type "Complex"
      minerva_x 1551.5
      minerva_y 1649.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa14"
      species_meta_id "s_id_csa14"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 45
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 200.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "e1__cell__ubiquitinated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa41"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa41"
      minerva_name "E1"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=UBA"
      minerva_ref_type__resource1 "HGNC_SYMBOL__UBA"
      minerva_state1 "UBIQUITINATED"
      minerva_type "Protein"
      minerva_x 1949.5
      minerva_y 148.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa41"
      species_meta_id "s_id_sa41"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 46
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 300.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nedd8__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa178"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa178"
      minerva_name "NEDD8"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7732"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/4738"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_006156"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NEDD8"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/Q15843"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000129559"
      minerva_ref_type__resource1 "HGNC__7732"
      minerva_ref_type__resource2 "ENTREZ__4738"
      minerva_ref_type__resource3 "REFSEQ__NM_006156"
      minerva_ref_type__resource4 "HGNC_SYMBOL__NEDD8"
      minerva_ref_type__resource5 "UNIPROT__Q15843"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000129559"
      minerva_type "Gene"
      minerva_x 251.0
      minerva_y 1710.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa178"
      species_meta_id "s_id_sa178"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 48
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 500.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "e1__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa6"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa6"
      minerva_name "E1"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=UBA"
      minerva_ref_type__resource1 "HGNC_SYMBOL__UBA"
      minerva_type "Gene"
      minerva_x 241.0
      minerva_y 151.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa6"
      species_meta_id "s_id_sa6"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 50
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 700.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "rbx1__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa4"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa4"
      minerva_name "RBX1"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P62877"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/protein/NM_014248"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/9928"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=RBX1"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/9978"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000100387"
      minerva_ref_type__resource1 "UNIPROT__P62877"
      minerva_ref_type__resource2 "REFSEQ__NM_014248"
      minerva_ref_type__resource3 "HGNC__9928"
      minerva_ref_type__resource4 "HGNC_SYMBOL__RBX1"
      minerva_ref_type__resource5 "ENTREZ__9978"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000100387"
      minerva_type "Gene"
      minerva_x 243.0
      minerva_y 762.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa4"
      species_meta_id "s_id_sa4"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 52
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 900.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "rbx1:cul2__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa3"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa3"
      minerva_name "Rbx1:Cul2"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CUL2"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=RBX1"
      minerva_ref_type__resource1 "HGNC_SYMBOL__CUL2"
      minerva_ref_type__resource2 "HGNC_SYMBOL__RBX1"
      minerva_type "Complex"
      minerva_x 743.0
      minerva_y 865.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa3"
      species_meta_id "s_id_csa3"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 55
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 1200.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ub__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa43"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa43"
      minerva_name "Ub"
      minerva_type "Simple molecule"
      minerva_x 1752.5
      minerva_y 105.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa43"
      species_meta_id "s_id_sa43"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 56
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 200.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cul2_space_ubiquitin_space_ligase:n8:e2:substrate__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa19"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa19"
      minerva_name "Cul2 ubiquitin ligase:N8:E2:substrate"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ELOC"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ELOB"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CUL2"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ZYG11B"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=RBX1"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NEDD8"
      minerva_ref_type__resource1 "HGNC_SYMBOL__ELOC"
      minerva_ref_type__resource2 "HGNC_SYMBOL__ELOB"
      minerva_ref_type__resource3 "HGNC_SYMBOL__CUL2"
      minerva_ref_type__resource4 "HGNC_SYMBOL__ZYG11B"
      minerva_ref_type__resource5 "HGNC_SYMBOL__RBX1"
      minerva_ref_type__resource6 "HGNC_SYMBOL__NEDD8"
      minerva_type "Complex"
      minerva_x 2125.5
      minerva_y 1023.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa19"
      species_meta_id "s_id_csa19"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 59
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 500.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cul2_space_ubiquitin_space_ligase__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa5"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa5"
      minerva_name "Cul2 ubiquitin ligase"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ELOC"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ELOB"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CUL2"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ZYG11B"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=RBX1"
      minerva_ref_type__resource1 "HGNC_SYMBOL__ELOC"
      minerva_ref_type__resource2 "HGNC_SYMBOL__ELOB"
      minerva_ref_type__resource3 "HGNC_SYMBOL__CUL2"
      minerva_ref_type__resource4 "HGNC_SYMBOL__ZYG11B"
      minerva_ref_type__resource5 "HGNC_SYMBOL__RBX1"
      minerva_type "Complex"
      minerva_x 1047.5
      minerva_y 784.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa5"
      species_meta_id "s_id_csa5"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 61
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 700.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cul2_space_ubiquitin_space_ligase:n8:e2_minus_ub:substrate__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa17"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa17"
      minerva_name "Cul2 ubiquitin ligase:N8:E2-Ub:substrate"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ELOC"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ELOB"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CUL2"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ZYG11B"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=RBX1"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NEDD8"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=UBE2"
      minerva_ref_type__resource1 "HGNC_SYMBOL__ELOC"
      minerva_ref_type__resource2 "HGNC_SYMBOL__ELOB"
      minerva_ref_type__resource3 "HGNC_SYMBOL__CUL2"
      minerva_ref_type__resource4 "HGNC_SYMBOL__ZYG11B"
      minerva_ref_type__resource5 "HGNC_SYMBOL__RBX1"
      minerva_ref_type__resource6 "HGNC_SYMBOL__NEDD8"
      minerva_ref_type__resource7 "HGNC_SYMBOL__UBE2"
      minerva_type "Complex"
      minerva_x 2125.5
      minerva_y 782.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa17"
      species_meta_id "s_id_csa17"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 62
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 800.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "atp__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa44"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa44"
      minerva_fullName "ATP"
      minerva_name "ATP"
      minerva_ref_link1 "https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:15422"
      minerva_ref_type__resource1 "CHEBI__CHEBI:15422"
      minerva_synonyms "ADENOSINE-5'-TRIPHOSPHATE; ATP; Adenosine 5'-triphosphate; Adenosine triphosphate; Adephos; Adetol; Adynol; Atipi; Atriphos; Cardenosine; Fosfobion; Glucobasin; H4atp; Myotriphos; Triadenyl; Triphosphaden"
      minerva_type "Simple molecule"
      minerva_x 1791.5
      minerva_y 91.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa44"
      species_meta_id "s_id_sa44"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 65
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 1100.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cul2_space_ubiquitin_space_ligase:n8:orf10:substrate_minus_ub__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa30"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa30"
      minerva_name "Cul2 ubiquitin ligase:N8:Orf10:substrate-Ub"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ELOC"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ELOB"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CUL2"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ZYG11B"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=RBX1"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NEDD8"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/BCD58762"
      minerva_ref_type__resource1 "HGNC_SYMBOL__ELOC"
      minerva_ref_type__resource2 "HGNC_SYMBOL__ELOB"
      minerva_ref_type__resource3 "HGNC_SYMBOL__CUL2"
      minerva_ref_type__resource4 "HGNC_SYMBOL__ZYG11B"
      minerva_ref_type__resource5 "HGNC_SYMBOL__RBX1"
      minerva_ref_type__resource6 "HGNC_SYMBOL__NEDD8"
      minerva_ref_type__resource7 "NCBI_PROTEIN__BCD58762"
      minerva_type "Complex"
      minerva_x 2703.5
      minerva_y 1451.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa30"
      species_meta_id "s_id_csa30"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 66
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 1200.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nae:nedd8__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa11"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa11"
      minerva_name "NAE:NEDD8"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NAE1"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=UBA3"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NEDD8"
      minerva_ref_type__resource1 "HGNC_SYMBOL__NAE1"
      minerva_ref_type__resource2 "HGNC_SYMBOL__UBA3"
      minerva_ref_type__resource3 "HGNC_SYMBOL__NEDD8"
      minerva_type "Complex"
      minerva_x 868.0
      minerva_y 1747.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa11"
      species_meta_id "s_id_csa11"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 68
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 300.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cul2_space_ubiquitin_space_ligase:n8__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa22"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa22"
      minerva_name "Cul2 ubiquitin ligase:N8"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ELOC"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ELOB"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CUL2"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ZYG11B"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=RBX1"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NEDD8"
      minerva_ref_type__resource1 "HGNC_SYMBOL__ELOC"
      minerva_ref_type__resource2 "HGNC_SYMBOL__ELOB"
      minerva_ref_type__resource3 "HGNC_SYMBOL__CUL2"
      minerva_ref_type__resource4 "HGNC_SYMBOL__ZYG11B"
      minerva_ref_type__resource5 "HGNC_SYMBOL__RBX1"
      minerva_ref_type__resource6 "HGNC_SYMBOL__NEDD8"
      minerva_type "Complex"
      minerva_x 2997.5
      minerva_y 782.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa22"
      species_meta_id "s_id_csa22"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 72
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 700.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ubit_underscore_traget__cell__ubiquitinated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa162"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa162"
      minerva_name "ubit_traget"
      minerva_state1 "UBIQUITINATED"
      minerva_type "Protein"
      minerva_x 2857.0
      minerva_y 967.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_sa162"
      species_meta_id "s_id_sa162"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 73
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 800.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cul2_space_ubiquitin_space_ligase:n8:orf10:0e2:substrate__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa28"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa28"
      minerva_name "Cul2 ubiquitin ligase:N8:Orf10:0E2:substrate"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ELOC"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ELOB"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CUL2"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ZYG11B"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=RBX1"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NEDD8"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/BCD58762"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=UBE2"
      minerva_ref_type__resource1 "HGNC_SYMBOL__ELOC"
      minerva_ref_type__resource2 "HGNC_SYMBOL__ELOB"
      minerva_ref_type__resource3 "HGNC_SYMBOL__CUL2"
      minerva_ref_type__resource4 "HGNC_SYMBOL__ZYG11B"
      minerva_ref_type__resource5 "HGNC_SYMBOL__RBX1"
      minerva_ref_type__resource6 "HGNC_SYMBOL__NEDD8"
      minerva_ref_type__resource7 "NCBI_PROTEIN__BCD58762"
      minerva_ref_type__resource8 "HGNC_SYMBOL__UBE2"
      minerva_type "Complex"
      minerva_x 2125.5
      minerva_y 1692.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_csa28"
      species_meta_id "s_id_csa28"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 74
    zlevel -1

    graphics [
      x 900.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "bind_E3:Orf10_subs"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "bind_E3_Orf10_subs"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Heterodimer association"
      minerva_x 1260.5
      minerva_y 1392.125
    ]
    sbml [
      reaction_id "bind_E3_Orf10_subs"
      reaction_meta_id "bind_E3_Orf10_subs"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 78
    zlevel -1

    graphics [
      x 1000.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "deg_DUB"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "deg_DUB"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "State transition"
      minerva_x 2747.25
      minerva_y 1283.75
    ]
    sbml [
      reaction_id "deg_DUB"
      reaction_meta_id "deg_DUB"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 81
    zlevel -1

    graphics [
      x 1100.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "deg_Ubc12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "deg_Ubc12"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "State transition"
      minerva_x 1616.2727272727273
      minerva_y 2193.1363636363635
    ]
    sbml [
      reaction_id "deg_Ubc12"
      reaction_meta_id "deg_Ubc12"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 84
    zlevel -1

    graphics [
      x 1200.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "deg_Rbx1_mRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "deg_Rbx1_mRNA"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "State transition"
      minerva_x 385.125
      minerva_y 801.5
    ]
    sbml [
      reaction_id "deg_Rbx1_mRNA"
      reaction_meta_id "deg_Rbx1_mRNA"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 87
    zlevel -1

    graphics [
      x 200.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "deg_26S_proteasom"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "deg_26S_proteasom"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "State transition"
      minerva_x 3090.171572875254
      minerva_y 1109.8284271247462
    ]
    sbml [
      reaction_id "deg_26S_proteasom"
      reaction_meta_id "deg_26S_proteasom"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 90
    zlevel -1

    graphics [
      x 300.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "bind_NEDD8_NAE"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "bind_NEDD8_NAE"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Heterodimer association"
      minerva_x 792.5
      minerva_y 1710.0
    ]
    sbml [
      reaction_id "bind_NEDD8_NAE"
      reaction_meta_id "bind_NEDD8_NAE"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 94
    zlevel -1

    graphics [
      x 400.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "diss_E3:Orf10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "diss_E3_Orf10"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Dissociation"
      minerva_x 1105.25
      minerva_y 1213.5
    ]
    sbml [
      reaction_id "diss_E3_Orf10"
      reaction_meta_id "diss_E3_Orf10"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 98
    zlevel -1

    graphics [
      x 500.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ubiquit_subs:E3:Orf10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "ubiquit_subs_E3_Orf10"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "State transition"
      minerva_x 2266.0
      minerva_y 1392.375
    ]
    sbml [
      reaction_id "ubiquit_subs_E3_Orf10"
      reaction_meta_id "ubiquit_subs_E3_Orf10"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 101
    zlevel -1

    graphics [
      x 600.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "transcr_NAE1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "transcr_NAE1"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Transcription"
      minerva_x 320.625
      minerva_y 1817.0
    ]
    sbml [
      reaction_id "transcr_NAE1"
      reaction_meta_id "transcr_NAE1"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 104
    zlevel -1

    graphics [
      x 700.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "bind_E2_E3:subs"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "bind_E2_E3_subs"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Heterodimer association"
      minerva_x 1970.4999999999998
      minerva_y 925.0000000000003
    ]
    sbml [
      reaction_id "bind_E2_E3_subs"
      reaction_meta_id "bind_E2_E3_subs"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 108
    zlevel -1

    graphics [
      x 800.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ubiquit_subs:E3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "ubiquit_subs_E3"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "State transition"
      minerva_x 2267.0
      minerva_y 728.75
    ]
    sbml [
      reaction_id "ubiquit_subs_E3"
      reaction_meta_id "ubiquit_subs_E3"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 111
    zlevel -1

    graphics [
      x 900.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "transl_NAE1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "transl_NAE1"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Translation"
      minerva_x 452.875
      minerva_y 1816.75
    ]
    sbml [
      reaction_id "transl_NAE1"
      reaction_meta_id "transl_NAE1"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 114
    zlevel -1

    graphics [
      x 1000.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "deg_NEDD8_mRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "deg_NEDD8_mRNA"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "State transition"
      minerva_x 378.375
      minerva_y 1749.0
    ]
    sbml [
      reaction_id "deg_NEDD8_mRNA"
      reaction_meta_id "deg_NEDD8_mRNA"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 117
    zlevel -1

    graphics [
      x 1100.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "transl_E2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "transl_E2"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Translation"
      minerva_x 1052.375
      minerva_y 291.5
    ]
    sbml [
      reaction_id "transl_E2"
      reaction_meta_id "transl_E2"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 120
    zlevel -1

    graphics [
      x 1200.0
      y 800.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "diss_E3:subs"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "diss_E3_subs"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Dissociation"
      minerva_x 1280.25
      minerva_y 838.2499999999998
    ]
    sbml [
      reaction_id "diss_E3_subs"
      reaction_meta_id "diss_E3_subs"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 124
    zlevel -1

    graphics [
      x 200.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "bind_NAE_Pevonedistat"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "bind_NAE_Pevonedistat"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Heterodimer association"
      minerva_x 793.0
      minerva_y 1886.25
    ]
    sbml [
      reaction_id "bind_NAE_Pevonedistat"
      reaction_meta_id "bind_NAE_Pevonedistat"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 128
    zlevel -1

    graphics [
      x 300.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "transl_Zyg11B"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "transl_Zyg11B"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Translation"
      minerva_x 455.875
      minerva_y 641.0
    ]
    sbml [
      reaction_id "transl_Zyg11B"
      reaction_meta_id "transl_Zyg11B"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 131
    zlevel -1

    graphics [
      x 400.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "diss_Cul2:Rbx1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "diss_Cul2_Rbx1"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Dissociation"
      minerva_x 622.2500000000001
      minerva_y 912.0
    ]
    sbml [
      reaction_id "diss_Cul2_Rbx1"
      reaction_meta_id "diss_Cul2_Rbx1"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 135
    zlevel -1

    graphics [
      x 500.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "transcr_Cul2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "transcr_Cul2"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Transcription"
      minerva_x 321.125
      minerva_y 945.5
    ]
    sbml [
      reaction_id "transcr_Cul2"
      reaction_meta_id "transcr_Cul2"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 138
    zlevel -1

    graphics [
      x 600.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "deg_UBA3_mRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "deg_UBA3_mRNA"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "State transition"
      minerva_x 380.875
      minerva_y 1977.0
    ]
    sbml [
      reaction_id "deg_UBA3_mRNA"
      reaction_meta_id "deg_UBA3_mRNA"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 141
    zlevel -1

    graphics [
      x 700.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "transcr_DUB"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "transcr_DUB"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Transcription"
      minerva_x 319.625
      minerva_y 1248.0
    ]
    sbml [
      reaction_id "transcr_DUB"
      reaction_meta_id "transcr_DUB"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 144
    zlevel -1

    graphics [
      x 800.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "diss_E3:Orf10:subs:ub"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "diss_E3_Orf10_subs_ub"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Dissociation"
      minerva_x 2838.0
      minerva_y 1312.375
    ]
    sbml [
      reaction_id "diss_E3_Orf10_subs_ub"
      reaction_meta_id "diss_E3_Orf10_subs_ub"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 148
    zlevel -1

    graphics [
      x 900.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "diss_E3:subs:ub"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "diss_E3_sub_ub"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Dissociation"
      minerva_x 2838.000000000001
      minerva_y 729.625
    ]
    sbml [
      reaction_id "diss_E3_sub_ub"
      reaction_meta_id "diss_E3_sub_ub"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 152
    zlevel -1

    graphics [
      x 1000.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ubiquit:E2_E3:subs"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "ubiquit_E2_E3_subs"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Heterodimer association"
      minerva_x 1970.2500000000002
      minerva_y 728.75
    ]
    sbml [
      reaction_id "ubiquit_E2_E3_subs"
      reaction_meta_id "ubiquit_E2_E3_subs"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 156
    zlevel -1

    graphics [
      x 1100.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "diss_NAE:Pevonedistat"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "diss_NAE_Pevonedistat"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Dissociation"
      minerva_x 793.0
      minerva_y 1924.25
    ]
    sbml [
      reaction_id "diss_NAE_Pevonedistat"
      reaction_meta_id "diss_NAE_Pevonedistat"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 160
    zlevel -1

    graphics [
      x 1200.0
      y 900.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "diss_E3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "diss_E3"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Dissociation"
      minerva_x 907.0000000000002
      minerva_y 838.5
    ]
    sbml [
      reaction_id "diss_E3"
      reaction_meta_id "diss_E3"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 164
    zlevel -1

    graphics [
      x 200.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "deg_NAE1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "deg_NAE1"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "State transition"
      minerva_x 522.5
      minerva_y 1860.5
    ]
    sbml [
      reaction_id "deg_NAE1"
      reaction_meta_id "deg_NAE1"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 167
    zlevel -1

    graphics [
      x 300.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "bind_Ubc12_NAE:NEDD8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "bind_Ubc12_NAE_NEDD8"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Heterodimer association"
      minerva_x 1576.375
      minerva_y 1722.3750000000002
    ]
    sbml [
      reaction_id "bind_Ubc12_NAE_NEDD8"
      reaction_meta_id "bind_Ubc12_NAE_NEDD8"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 171
    zlevel -1

    graphics [
      x 400.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "diss_E3:NEDD8_E3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "diss_E3_NEDD8_E3"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "State transition"
      minerva_x 2022.5
      minerva_y 810.375
    ]
    sbml [
      reaction_id "diss_E3_NEDD8_E3"
      reaction_meta_id "diss_E3_NEDD8_E3"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 175
    zlevel -1

    graphics [
      x 500.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "deg_E2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "deg_E2"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "State transition"
      minerva_x 1713.25
      minerva_y 260.25
    ]
    sbml [
      reaction_id "deg_E2"
      reaction_meta_id "deg_E2"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 178
    zlevel -1

    graphics [
      x 600.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "syn_26S_proteasom"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "syn_26S_proteasom"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "State transition"
      minerva_x 3090.171572875254
      minerva_y 1074.1715728752538
    ]
    sbml [
      reaction_id "syn_26S_proteasom"
      reaction_meta_id "syn_26S_proteasom"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 181
    zlevel -1

    graphics [
      x 700.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "transl_EloC"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "transl_EloC"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Translation"
      minerva_x 456.875
      minerva_y 522.0
    ]
    sbml [
      reaction_id "transl_EloC"
      reaction_meta_id "transl_EloC"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 184
    zlevel -1

    graphics [
      x 800.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "bind_E3:subs_NEDD8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "bind_E3_subs_NEDD8"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Heterodimer association"
      minerva_x 1618.5
      minerva_y 728.4999999999997
    ]
    sbml [
      reaction_id "bind_E3_subs_NEDD8"
      reaction_meta_id "bind_E3_subs_NEDD8"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 189
    zlevel -1

    graphics [
      x 900.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "deubiquit_E2:E3:subs"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "deubiquit_E2_E3_subs"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Dissociation"
      minerva_x 1990.25
      minerva_y 782.375
    ]
    sbml [
      reaction_id "deubiquit_E2_E3_subs"
      reaction_meta_id "deubiquit_E2_E3_subs"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 193
    zlevel -1

    graphics [
      x 1000.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "deg_E1_mRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "deg_E1_mRNA"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "State transition"
      minerva_x 384.625
      minerva_y 193.5
    ]
    sbml [
      reaction_id "deg_E1_mRNA"
      reaction_meta_id "deg_E1_mRNA"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 196
    zlevel -1

    graphics [
      x 1100.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "transcr_EloB"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "transcr_EloB"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Transcription"
      minerva_x 321.125
      minerva_y 401.5
    ]
    sbml [
      reaction_id "transcr_EloB"
      reaction_meta_id "transcr_EloB"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 199
    zlevel -1

    graphics [
      x 1200.0
      y 1000.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ubiquit_E2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "ubiquit_E2"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "State transition"
      minerva_x 1831.75
      minerva_y 303.25
    ]
    sbml [
      reaction_id "ubiquit_E2"
      reaction_meta_id "ubiquit_E2"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 204
    zlevel -1

    graphics [
      x 200.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "transl_EloB"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "transl_EloB"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Translation"
      minerva_x 456.875
      minerva_y 401.0
    ]
    sbml [
      reaction_id "transl_EloB"
      reaction_meta_id "transl_EloB"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 207
    zlevel -1

    graphics [
      x 300.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "transl_NEDD8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "transl_NEDD8"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Translation"
      minerva_x 461.375
      minerva_y 1709.75
    ]
    sbml [
      reaction_id "transl_NEDD8"
      reaction_meta_id "transl_NEDD8"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 210
    zlevel -1

    graphics [
      x 400.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "bind_Elo_Zyg11B"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "bind_Elo_Zyg11B"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Heterodimer association"
      minerva_x 670.0
      minerva_y 577.0
    ]
    sbml [
      reaction_id "bind_Elo_Zyg11B"
      reaction_meta_id "bind_Elo_Zyg11B"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 214
    zlevel -1

    graphics [
      x 500.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "bind_E3:Orf10:subs_NEDD8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "bind_E3_Orf10_subs_NEDD8"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Heterodimer association"
      minerva_x 1607.8750000000005
      minerva_y 1392.125
    ]
    sbml [
      reaction_id "bind_E3_Orf10_subs_NEDD8"
      reaction_meta_id "bind_E3_Orf10_subs_NEDD8"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 219
    zlevel -1

    graphics [
      x 600.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "bind_NAE1_UBA3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "bind_NAE1_UBA3"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Heterodimer association"
      minerva_x 615.5
      minerva_y 1820.0
    ]
    sbml [
      reaction_id "bind_NAE1_UBA3"
      reaction_meta_id "bind_NAE1_UBA3"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 223
    zlevel -1

    graphics [
      x 700.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "deg_EloC"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "deg_EloC"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "State transition"
      minerva_x 522.75
      minerva_y 564.75
    ]
    sbml [
      reaction_id "deg_EloC"
      reaction_meta_id "deg_EloC"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 226
    zlevel -1

    graphics [
      x 800.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "deg_DUB_mRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "deg_DUB_mRNA"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "State transition"
      minerva_x 367.125
      minerva_y 1288.5
    ]
    sbml [
      reaction_id "deg_DUB_mRNA"
      reaction_meta_id "deg_DUB_mRNA"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 229
    zlevel -1

    graphics [
      x 900.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "bind_E2_E3:Orf10:subs"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "bind_E2_E3_Orf10_subs"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Heterodimer association"
      minerva_x 1994.9999999999995
      minerva_y 1571.1249999999998
    ]
    sbml [
      reaction_id "bind_E2_E3_Orf10_subs"
      reaction_meta_id "bind_E2_E3_Orf10_subs"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 233
    zlevel -1

    graphics [
      x 1000.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "transl_Rbx1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "transl_Rbx1"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Translation"
      minerva_x 456.375
      minerva_y 762.0
    ]
    sbml [
      reaction_id "transl_Rbx1"
      reaction_meta_id "transl_Rbx1"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 236
    zlevel -1

    graphics [
      x 1100.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "diss_EloB:EloC"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "diss_EloB_EloC"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Dissociation"
      minerva_x 629.0000000000001
      minerva_y 487.0
    ]
    sbml [
      reaction_id "diss_EloB_EloC"
      reaction_meta_id "diss_EloB_EloC"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 240
    zlevel -1

    graphics [
      x 1200.0
      y 1100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "diss_E3:Orf10:NEDD8_E3:Orf10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "diss_E3_Orf10_NEDD8_E3_Orf10"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "State transition"
      minerva_x 2022.5
      minerva_y 1479.0
    ]
    sbml [
      reaction_id "diss_E3_Orf10_NEDD8_E3_Orf10"
      reaction_meta_id "diss_E3_Orf10_NEDD8_E3_Orf10"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 244
    zlevel -1

    graphics [
      x 200.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "bind_EloB_EloC"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "bind_EloB_EloC"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Heterodimer association"
      minerva_x 629.0
      minerva_y 427.5
    ]
    sbml [
      reaction_id "bind_EloB_EloC"
      reaction_meta_id "bind_EloB_EloC"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 248
    zlevel -1

    graphics [
      x 300.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "deg_NAE1_mRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "deg_NAE1_mRNA"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "State transition"
      minerva_x 377.375
      minerva_y 1857.0
    ]
    sbml [
      reaction_id "deg_NAE1_mRNA"
      reaction_meta_id "deg_NAE1_mRNA"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 251
    zlevel -1

    graphics [
      x 400.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "deubiquit_E2:E3:Orf10:subs"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "deubiquit_E2_E3_Orf10_subs"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Dissociation"
      minerva_x 1981.2499999999998
      minerva_y 1448.75
    ]
    sbml [
      reaction_id "deubiquit_E2_E3_Orf10_subs"
      reaction_meta_id "deubiquit_E2_E3_Orf10_subs"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 255
    zlevel -1

    graphics [
      x 500.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "bind_Orf10_E3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "bind_Orf10_E3"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Heterodimer association"
      minerva_x 989.7499999999998
      minerva_y 1205.0000000000005
    ]
    sbml [
      reaction_id "bind_Orf10_E3"
      reaction_meta_id "bind_Orf10_E3"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 259
    zlevel -1

    graphics [
      x 600.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ubiquit:E2_E3:Orf10:subs"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "ubiquit_E2_E3_Orf10_subs"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Heterodimer association"
      minerva_x 1965.75
      minerva_y 1390.5
    ]
    sbml [
      reaction_id "ubiquit_E2_E3_Orf10_subs"
      reaction_meta_id "ubiquit_E2_E3_Orf10_subs"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 263
    zlevel -1

    graphics [
      x 700.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "deg_E2_mRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "deg_E2_mRNA"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "State transition"
      minerva_x 386.125
      minerva_y 318.5
    ]
    sbml [
      reaction_id "deg_E2_mRNA"
      reaction_meta_id "deg_E2_mRNA"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 266
    zlevel -1

    graphics [
      x 800.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "deg_EloB_mRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "deg_EloB_mRNA"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "State transition"
      minerva_x 386.375
      minerva_y 441.25
    ]
    sbml [
      reaction_id "deg_EloB_mRNA"
      reaction_meta_id "deg_EloB_mRNA"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 269
    zlevel -1

    graphics [
      x 900.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "deg_EloC_mRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "deg_EloC_mRNA"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "State transition"
      minerva_x 385.125
      minerva_y 561.5
    ]
    sbml [
      reaction_id "deg_EloC_mRNA"
      reaction_meta_id "deg_EloC_mRNA"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 272
    zlevel -1

    graphics [
      x 1000.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "diss_E2:E3:subs"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "diss_E2_E3_subs"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Dissociation"
      minerva_x 1971.4999999999993
      minerva_y 979.1523437499993
    ]
    sbml [
      reaction_id "diss_E2_E3_subs"
      reaction_meta_id "diss_E2_E3_subs"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 276
    zlevel -1

    graphics [
      x 1100.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "bind_E3_subs"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "bind_E3_subs"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Heterodimer association"
      minerva_x 1260.25
      minerva_y 729.7500000000001
    ]
    sbml [
      reaction_id "bind_E3_subs"
      reaction_meta_id "bind_E3_subs"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 280
    zlevel -1

    graphics [
      x 1200.0
      y 1200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "transl_DUB"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "transl_DUB"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Translation"
      minerva_x 1560.375
      minerva_y 1245.0
    ]
    sbml [
      reaction_id "transl_DUB"
      reaction_meta_id "transl_DUB"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 283
    zlevel -1

    graphics [
      x 200.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "deubiquit_subs:ub"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "deubiquit_subs_ub"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "State transition"
      minerva_x 2747.0
      minerva_y 967.5
    ]
    sbml [
      reaction_id "deubiquit_subs_ub"
      reaction_meta_id "deubiquit_subs_ub"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 287
    zlevel -1

    graphics [
      x 300.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "bind_Elo:Zyg11B_Cul2:Rbx1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "bind_Elo_Zyg11B_Cul2_Rbx1"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Heterodimer association"
      minerva_x 907.0
      minerva_y 730.5
    ]
    sbml [
      reaction_id "bind_Elo_Zyg11B_Cul2_Rbx1"
      reaction_meta_id "bind_Elo_Zyg11B_Cul2_Rbx1"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 291
    zlevel -1

    graphics [
      x 400.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "transcr_UBA3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "transcr_UBA3"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Transcription"
      minerva_x 322.625
      minerva_y 1930.5
    ]
    sbml [
      reaction_id "transcr_UBA3"
      reaction_meta_id "transcr_UBA3"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 294
    zlevel -1

    graphics [
      x 500.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "deg_NEDD8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "deg_NEDD8"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "State transition"
      minerva_x 521.5
      minerva_y 1747.0
    ]
    sbml [
      reaction_id "deg_NEDD8"
      reaction_meta_id "deg_NEDD8"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 297
    zlevel -1

    graphics [
      x 600.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "diss_NAE"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "diss_NAE"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Dissociation"
      minerva_x 615.9999999999998
      minerva_y 1850.0
    ]
    sbml [
      reaction_id "diss_NAE"
      reaction_meta_id "diss_NAE"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 301
    zlevel -1

    graphics [
      x 700.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "deg_CSN5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "deg_CSN5"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "State transition"
      minerva_x 2411.171572875254
      minerva_y 1078.0784271247462
    ]
    sbml [
      reaction_id "deg_CSN5"
      reaction_meta_id "deg_CSN5"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 304
    zlevel -1

    graphics [
      x 800.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "deg_EloB"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "deg_EloB"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "State transition"
      minerva_x 522.25
      minerva_y 444.75
    ]
    sbml [
      reaction_id "deg_EloB"
      reaction_meta_id "deg_EloB"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 307
    zlevel -1

    graphics [
      x 900.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "deg_Rbx1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "deg_Rbx1"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "State transition"
      minerva_x 521.75
      minerva_y 805.75
    ]
    sbml [
      reaction_id "deg_Rbx1"
      reaction_meta_id "deg_Rbx1"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 310
    zlevel -1

    graphics [
      x 1000.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "deg_Zyg11B_mRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "deg_Zyg11B_mRNA"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "State transition"
      minerva_x 384.125
      minerva_y 681.0
    ]
    sbml [
      reaction_id "deg_Zyg11B_mRNA"
      reaction_meta_id "deg_Zyg11B_mRNA"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 313
    zlevel -1

    graphics [
      x 1100.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "deg_substrate:ub"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "deg_substrate_ub"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "State transition"
      minerva_x 3007.5
      minerva_y 967.5
    ]
    sbml [
      reaction_id "deg_substrate_ub"
      reaction_meta_id "deg_substrate_ub"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 317
    zlevel -1

    graphics [
      x 1200.0
      y 1300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re97"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "State transition"
      minerva_x 1949.0
      minerva_y 191.75
    ]
    sbml [
      reaction_id "re97"
      reaction_meta_id "re97"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 320
    zlevel -1

    graphics [
      x 200.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "diss_E3:Orf10:subs"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "diss_E3_Orf10_subs"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Dissociation"
      minerva_x 1280.5
      minerva_y 1508.375
    ]
    sbml [
      reaction_id "diss_E3_Orf10_subs"
      reaction_meta_id "diss_E3_Orf10_subs"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 324
    zlevel -1

    graphics [
      x 300.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "deg_Zyg11B"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "deg_Zyg11B"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "State transition"
      minerva_x 522.25
      minerva_y 684.25
    ]
    sbml [
      reaction_id "deg_Zyg11B"
      reaction_meta_id "deg_Zyg11B"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 327
    zlevel -1

    graphics [
      x 400.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "transl_UBA3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "transl_UBA3"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Translation"
      minerva_x 454.375
      minerva_y 1930.75
    ]
    sbml [
      reaction_id "transl_UBA3"
      reaction_meta_id "transl_UBA3"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 330
    zlevel -1

    graphics [
      x 500.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re99"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "State transition"
      minerva_x 1951.0307337294603
      minerva_y 256.4455181300451
    ]
    sbml [
      reaction_id "re99"
      reaction_meta_id "re99"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 333
    zlevel -1

    graphics [
      x 600.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "transl_E1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "transl_E1"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Translation"
      minerva_x 1050.875
      minerva_y 151.0
    ]
    sbml [
      reaction_id "transl_E1"
      reaction_meta_id "transl_E1"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 336
    zlevel -1

    graphics [
      x 700.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "deg_Cul2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "deg_Cul2"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "State transition"
      minerva_x 519.25
      minerva_y 1044.25
    ]
    sbml [
      reaction_id "deg_Cul2"
      reaction_meta_id "deg_Cul2"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 339
    zlevel -1

    graphics [
      x 800.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "deg_Orf10_mRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "deg_Orf10_mRNA"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "State transition"
      minerva_x 692.0625
      minerva_y 1129.5
    ]
    sbml [
      reaction_id "deg_Orf10_mRNA"
      reaction_meta_id "deg_Orf10_mRNA"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 342
    zlevel -1

    graphics [
      x 900.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "ubiquit_E1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "ubiquit_E1"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "State transition"
      minerva_x 1830.75
      minerva_y 148.75
    ]
    sbml [
      reaction_id "ubiquit_E1"
      reaction_meta_id "ubiquit_E1"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 349
    zlevel -1

    graphics [
      x 1000.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "diss_E2:E3:Orf10:subs"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "diss_E2_E3_Orf10_subs"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Dissociation"
      minerva_x 1989.0
      minerva_y 1626.8554687499993
    ]
    sbml [
      reaction_id "diss_E2_E3_Orf10_subs"
      reaction_meta_id "diss_E2_E3_Orf10_subs"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 353
    zlevel -1

    graphics [
      x 1100.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "deg_Ubc12_mRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "deg_Ubc12_mRNA"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "State transition"
      minerva_x 380.64772727272725
      minerva_y 2187.3863636363635
    ]
    sbml [
      reaction_id "deg_Ubc12_mRNA"
      reaction_meta_id "deg_Ubc12_mRNA"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 356
    zlevel -1

    graphics [
      x 1200.0
      y 1400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "deg_Cul2_mRNA"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "deg_Cul2_mRNA"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "State transition"
      minerva_x 385.125
      minerva_y 1014.0
    ]
    sbml [
      reaction_id "deg_Cul2_mRNA"
      reaction_meta_id "deg_Cul2_mRNA"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 359
    zlevel -1

    graphics [
      x 200.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "transcr_Rbx1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "transcr_Rbx1"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Transcription"
      minerva_x 320.625
      minerva_y 762.5
    ]
    sbml [
      reaction_id "transcr_Rbx1"
      reaction_meta_id "transcr_Rbx1"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 362
    zlevel -1

    graphics [
      x 300.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "transcr_EloC"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "transcr_EloC"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Transcription"
      minerva_x 321.125
      minerva_y 522.5
    ]
    sbml [
      reaction_id "transcr_EloC"
      reaction_meta_id "transcr_EloC"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 365
    zlevel -1

    graphics [
      x 400.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "transl_Orf10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "transl_Orf10"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Translation"
      minerva_x 807.8125
      minerva_y 1084.5
    ]
    sbml [
      reaction_id "transl_Orf10"
      reaction_meta_id "transl_Orf10"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 368
    zlevel -1

    graphics [
      x 500.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "transcr_Ubc12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "transcr_Ubc12"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Transcription"
      minerva_x 323.625
      minerva_y 2142.5
    ]
    sbml [
      reaction_id "transcr_Ubc12"
      reaction_meta_id "transcr_Ubc12"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 371
    zlevel -1

    graphics [
      x 600.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "transcr_E1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "transcr_E1"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Transcription"
      minerva_x 319.125
      minerva_y 152.0
    ]
    sbml [
      reaction_id "transcr_E1"
      reaction_meta_id "transcr_E1"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 374
    zlevel -1

    graphics [
      x 700.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "diss_Elo:Zyg11B"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "diss_Elo_Zyg11B"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Dissociation"
      minerva_x 633.5
      minerva_y 615.5
    ]
    sbml [
      reaction_id "diss_Elo_Zyg11B"
      reaction_meta_id "diss_Elo_Zyg11B"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 378
    zlevel -1

    graphics [
      x 800.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "transcr_E2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "transcr_E2"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Transcription"
      minerva_x 320.625
      minerva_y 279.0
    ]
    sbml [
      reaction_id "transcr_E2"
      reaction_meta_id "transcr_E2"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 381
    zlevel -1

    graphics [
      x 900.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "transl_Ubc12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "transl_Ubc12"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Translation"
      minerva_x 1001.375
      minerva_y 2144.0
    ]
    sbml [
      reaction_id "transl_Ubc12"
      reaction_meta_id "transl_Ubc12"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 384
    zlevel -1

    graphics [
      x 1000.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "deg_UBA3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "deg_UBA3"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "State transition"
      minerva_x 522.5
      minerva_y 1977.5
    ]
    sbml [
      reaction_id "deg_UBA3"
      reaction_meta_id "deg_UBA3"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 387
    zlevel -1

    graphics [
      x 1100.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "transl_Cul2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "transl_Cul2"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Translation"
      minerva_x 463.375
      minerva_y 945.25
    ]
    sbml [
      reaction_id "transl_Cul2"
      reaction_meta_id "transl_Cul2"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 390
    zlevel -1

    graphics [
      x 1200.0
      y 1500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "syn_CSN5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "syn_CSN5"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "State transition"
      minerva_x 2411.171572875254
      minerva_y 1042.4215728752538
    ]
    sbml [
      reaction_id "syn_CSN5"
      reaction_meta_id "syn_CSN5"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 393
    zlevel -1

    graphics [
      x 200.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "diss_E2:E3:subs:ub"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "diss_E2_E3_subs_ub"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Dissociation"
      minerva_x 2541.0
      minerva_y 728.4999999999998
    ]
    sbml [
      reaction_id "diss_E2_E3_subs_ub"
      reaction_meta_id "diss_E2_E3_subs_ub"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 397
    zlevel -1

    graphics [
      x 300.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "transcr_NEDD8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "transcr_NEDD8"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Transcription"
      minerva_x 321.125
      minerva_y 1710.0
    ]
    sbml [
      reaction_id "transcr_NEDD8"
      reaction_meta_id "transcr_NEDD8"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 400
    zlevel -1

    graphics [
      x 400.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "transcr_Zyg11B"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "transcr_Zyg11B"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Transcription"
      minerva_x 320.125
      minerva_y 641.5
    ]
    sbml [
      reaction_id "transcr_Zyg11B"
      reaction_meta_id "transcr_Zyg11B"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 403
    zlevel -1

    graphics [
      x 500.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "deg_Orf10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "deg_Orf10"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "State transition"
      minerva_x 868.0
      minerva_y 1131.0
    ]
    sbml [
      reaction_id "deg_Orf10"
      reaction_meta_id "deg_Orf10"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 406
    zlevel -1

    graphics [
      x 600.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "deg_E1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "deg_E1"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "State transition"
      minerva_x 1712.75
      minerva_y 194.75
    ]
    sbml [
      reaction_id "deg_E1"
      reaction_meta_id "deg_E1"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 409
    zlevel -1

    graphics [
      x 700.0
      y 1600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "bind_Cul2_Rbx1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "bind_Cul2_Rbx1"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "Heterodimer association"
      minerva_x 622.75
      minerva_y 818.875
    ]
    sbml [
      reaction_id "bind_Cul2_Rbx1"
      reaction_meta_id "bind_Cul2_Rbx1"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 413
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 600.0
      y 600.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "eloc__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa10"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa10"
      minerva_former_symbols "TCEB1"
      minerva_name "ELOC"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ELOC"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q15369"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/6921"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000154582"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11617"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/protein/NM_005648"
      minerva_ref_type__resource1 "HGNC_SYMBOL__ELOC"
      minerva_ref_type__resource2 "UNIPROT__Q15369"
      minerva_ref_type__resource3 "ENTREZ__6921"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000154582"
      minerva_ref_type__resource5 "HGNC__11617"
      minerva_ref_type__resource6 "REFSEQ__NM_005648"
      minerva_type "RNA"
      minerva_x 397.0
      minerva_y 522.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation "Annotation [about=\"#s_id_sa10\", history=]"
      species_id "s_id_sa10"
      species_meta_id "s_id_sa10"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
      species_notes "XMLNode 'notes' [childElements size=1]"
    ]
  ]
  node [
    id 421
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 800.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nae1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa174"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa174"
      minerva_former_symbols "APPBP1"
      minerva_name "NAE1"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NAE1"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/621"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/8883"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/Q13564"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_003905"
      minerva_ref_link6 "https://www.ensembl.org/id/ENSG00000159593"
      minerva_ref_type__resource1 "HGNC_SYMBOL__NAE1"
      minerva_ref_type__resource2 "HGNC__621"
      minerva_ref_type__resource3 "ENTREZ__8883"
      minerva_ref_type__resource4 "UNIPROT__Q13564"
      minerva_ref_type__resource5 "REFSEQ__NM_003905"
      minerva_ref_type__resource6 "ENSEMBL__ENSG00000159593"
      minerva_type "RNA"
      minerva_x 389.0
      minerva_y 1817.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation "Annotation [about=\"#s_id_sa174\", history=]"
      species_id "s_id_sa174"
      species_meta_id "s_id_sa174"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
      species_notes "XMLNode 'notes' [childElements size=1]"
    ]
  ]
  node [
    id 429
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 650.0
      y 550.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "rbx1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa20"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa20"
      minerva_name "RBX1"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P62877"
      minerva_ref_link10 "https://www.ncbi.nlm.nih.gov/gene/9978"
      minerva_ref_link11 "https://www.ensembl.org/id/ENSG00000100387"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P62877"
      minerva_ref_link3 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.27"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_014248"
      minerva_ref_link5 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.32"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/9928"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=RBX1"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/9978"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=RBX1"
      minerva_ref_type__resource1 "UNIPROT__P62877"
      minerva_ref_type__resource10 "ENTREZ__9978"
      minerva_ref_type__resource11 "ENSEMBL__ENSG00000100387"
      minerva_ref_type__resource2 "UNIPROT__P62877"
      minerva_ref_type__resource3 "EC__2.3.2.27"
      minerva_ref_type__resource4 "REFSEQ__NM_014248"
      minerva_ref_type__resource5 "EC__2.3.2.32"
      minerva_ref_type__resource6 "HGNC__9928"
      minerva_ref_type__resource7 "HGNC_SYMBOL__RBX1"
      minerva_ref_type__resource8 "ENTREZ__9978"
      minerva_ref_type__resource9 "HGNC_SYMBOL__RBX1"
      minerva_type "Protein"
      minerva_x 522.0
      minerva_y 761.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation "Annotation [about=\"#s_id_sa20\", history=]"
      species_id "s_id_sa20"
      species_meta_id "s_id_sa20"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
      species_notes "XMLNode 'notes' [childElements size=1]"
    ]
  ]
  node [
    id 437
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 1050.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ube2m__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa102"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa102"
      minerva_name "UBE2M"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12491"
      minerva_ref_link10 "https://www.ensembl.org/id/ENSG00000130725"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=UBE2M"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/P61081"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/P61081"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=UBE2M"
      minerva_ref_link6 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.3.2.34"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/9040"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_003969"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/9040"
      minerva_ref_type__resource1 "HGNC__12491"
      minerva_ref_type__resource10 "ENSEMBL__ENSG00000130725"
      minerva_ref_type__resource2 "HGNC_SYMBOL__UBE2M"
      minerva_ref_type__resource3 "UNIPROT__P61081"
      minerva_ref_type__resource4 "UNIPROT__P61081"
      minerva_ref_type__resource5 "HGNC_SYMBOL__UBE2M"
      minerva_ref_type__resource6 "EC__2.3.2.34"
      minerva_ref_type__resource7 "ENTREZ__9040"
      minerva_ref_type__resource8 "REFSEQ__NM_003969"
      minerva_ref_type__resource9 "ENTREZ__9040"
      minerva_type "Protein"
      minerva_x 1616.0
      minerva_y 2144.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation "Annotation [about=\"#s_id_sa102\", history=]"
      species_id "s_id_sa102"
      species_meta_id "s_id_sa102"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
      species_notes "XMLNode 'notes' [childElements size=1]"
    ]
  ]
  node [
    id 446
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 1050.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "nedd8__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa118"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa118"
      minerva_name "NEDD8"
      minerva_ref_link1 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/7732"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/4738"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/4738"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_006156"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NEDD8"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/Q15843"
      minerva_ref_link7 "http://purl.uniprot.org/uniprot/Q15843"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=NEDD8"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000129559"
      minerva_ref_type__resource1 "HGNC__7732"
      minerva_ref_type__resource2 "ENTREZ__4738"
      minerva_ref_type__resource3 "ENTREZ__4738"
      minerva_ref_type__resource4 "REFSEQ__NM_006156"
      minerva_ref_type__resource5 "HGNC_SYMBOL__NEDD8"
      minerva_ref_type__resource6 "UNIPROT__Q15843"
      minerva_ref_type__resource7 "UNIPROT__Q15843"
      minerva_ref_type__resource8 "HGNC_SYMBOL__NEDD8"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000129559"
      minerva_type "Protein"
      minerva_x 522.0
      minerva_y 1709.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation "Annotation [about=\"#s_id_sa118\", history=]"
      species_id "s_id_sa118"
      species_meta_id "s_id_sa118"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
      species_notes "XMLNode 'notes' [childElements size=1]"
    ]
  ]
  node [
    id 453
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 500.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "zyg11b__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa11"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa11"
      minerva_former_symbols "ZYG11"
      minerva_name "ZYG11B"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/Q9C0D3"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ZYG11B"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/79699"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000162378"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/protein/NM_024646"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/25820"
      minerva_ref_type__resource1 "UNIPROT__Q9C0D3"
      minerva_ref_type__resource2 "HGNC_SYMBOL__ZYG11B"
      minerva_ref_type__resource3 "ENTREZ__79699"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000162378"
      minerva_ref_type__resource5 "REFSEQ__NM_024646"
      minerva_ref_type__resource6 "HGNC__25820"
      minerva_type "RNA"
      minerva_x 396.0
      minerva_y 641.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation "Annotation [about=\"#s_id_sa11\", history=]"
      species_id "s_id_sa11"
      species_meta_id "s_id_sa11"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
      species_notes "XMLNode 'notes' [childElements size=1]"
    ]
  ]
  node [
    id 461
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 650.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cul2_space_ubiquitin_space_ligase:substrate__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa4"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa4"
      minerva_name "Cul2 ubiquitin ligase:substrate"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ELOC"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ELOB"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CUL2"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ZYG11B"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=RBX1"
      minerva_ref_type__resource1 "HGNC_SYMBOL__ELOC"
      minerva_ref_type__resource2 "HGNC_SYMBOL__ELOB"
      minerva_ref_type__resource3 "HGNC_SYMBOL__CUL2"
      minerva_ref_type__resource4 "HGNC_SYMBOL__ZYG11B"
      minerva_ref_type__resource5 "HGNC_SYMBOL__RBX1"
      minerva_type "Complex"
      minerva_x 1417.0
      minerva_y 784.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation "Annotation [about=\"#s_id_csa4\", history=]"
      species_id "s_id_csa4"
      species_meta_id "s_id_csa4"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
    ]
  ]
  node [
    id 470
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 650.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "dub__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa16"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa16"
      minerva_name "DUB"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=DUB"
      minerva_ref_type__resource1 "HGNC_SYMBOL__DUB"
      minerva_type "RNA"
      minerva_x 379.0
      minerva_y 1247.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation "Annotation [about=\"#s_id_sa16\", history=]"
      species_id "s_id_sa16"
      species_meta_id "s_id_sa16"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
    ]
  ]
  node [
    id 479
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 1050.0
      y 450.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "e1__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa22"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa22"
      minerva_name "E1"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=UBA"
      minerva_ref_type__resource1 "HGNC_SYMBOL__UBA"
      minerva_type "Protein"
      minerva_x 1712.0
      minerva_y 149.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation "Annotation [about=\"#s_id_sa22\", history=]"
      species_id "s_id_sa22"
      species_meta_id "s_id_sa22"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
      species_notes "XMLNode 'notes' [childElements size=1]"
    ]
  ]
  node [
    id 487
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 900.0
      y 500.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "e2__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa23"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa23"
      minerva_name "E2"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=UBE2"
      minerva_ref_type__resource1 "HGNC_SYMBOL__UBE2"
      minerva_type "Protein"
      minerva_x 1713.0
      minerva_y 304.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation "Annotation [about=\"#s_id_sa23\", history=]"
      species_id "s_id_sa23"
      species_meta_id "s_id_sa23"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
      species_notes "XMLNode 'notes' [childElements size=1]"
    ]
  ]
  node [
    id 499
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 650.0
      y 350.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "uba3__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa175"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa175"
      minerva_former_symbols "UBE1C"
      minerva_name "UBA3"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000144744"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=UBA3"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/Q8TBC4"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_198195"
      minerva_ref_link5 "https://www.ncbi.nlm.nih.gov/gene/9039"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/12470"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000144744"
      minerva_ref_type__resource2 "HGNC_SYMBOL__UBA3"
      minerva_ref_type__resource3 "UNIPROT__Q8TBC4"
      minerva_ref_type__resource4 "REFSEQ__NM_198195"
      minerva_ref_type__resource5 "ENTREZ__9039"
      minerva_ref_type__resource6 "HGNC__12470"
      minerva_type "RNA"
      minerva_x 392.0
      minerva_y 1931.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation "Annotation [about=\"#s_id_sa175\", history=]"
      species_id "s_id_sa175"
      species_meta_id "s_id_sa175"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
      species_notes "XMLNode 'notes' [childElements size=1]"
    ]
  ]
  node [
    id 507
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 800.0
      y 250.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "elob__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa9"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa9"
      minerva_former_symbols "TCEB2"
      minerva_name "ELOB"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_007108"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ELOB"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/6923"
      minerva_ref_link4 "https://www.ensembl.org/id/ENSG00000103363"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11619"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/Q15370"
      minerva_ref_type__resource1 "REFSEQ__NM_007108"
      minerva_ref_type__resource2 "HGNC_SYMBOL__ELOB"
      minerva_ref_type__resource3 "ENTREZ__6923"
      minerva_ref_type__resource4 "ENSEMBL__ENSG00000103363"
      minerva_ref_type__resource5 "HGNC__11619"
      minerva_ref_type__resource6 "UNIPROT__Q15370"
      minerva_type "RNA"
      minerva_x 398.0
      minerva_y 401.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation "Annotation [about=\"#s_id_sa9\", history=]"
      species_id "s_id_sa9"
      species_meta_id "s_id_sa9"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
      species_notes "XMLNode 'notes' [childElements size=1]"
    ]
  ]
  node [
    id 515
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 350.0
      y 700.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "cul2__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa21"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa21"
      minerva_name "CUL2"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/Q13617"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/Q13617"
      minerva_ref_link3 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/2552"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CUL2"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=CUL2"
      minerva_ref_link6 "https://www.ncbi.nlm.nih.gov/gene/8453"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/8453"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_003591"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000108094"
      minerva_ref_type__resource1 "UNIPROT__Q13617"
      minerva_ref_type__resource2 "UNIPROT__Q13617"
      minerva_ref_type__resource3 "HGNC__2552"
      minerva_ref_type__resource4 "HGNC_SYMBOL__CUL2"
      minerva_ref_type__resource5 "HGNC_SYMBOL__CUL2"
      minerva_ref_type__resource6 "ENTREZ__8453"
      minerva_ref_type__resource7 "ENTREZ__8453"
      minerva_ref_type__resource8 "REFSEQ__NM_003591"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000108094"
      minerva_type "Protein"
      minerva_x 519.0
      minerva_y 945.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation "Annotation [about=\"#s_id_sa21\", history=]"
      species_id "s_id_sa21"
      species_meta_id "s_id_sa21"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
      species_notes "XMLNode 'notes' [childElements size=1]"
    ]
  ]
  edge [
    id 523
    source 13
    target 74
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa35"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 524
    source 12
    target 74
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa171"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 525
    source 74
    target 28
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa34"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 526
    source 78
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 527
    source 81
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 528
    source 84
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 529
    source 9
    target 87
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa25"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 530
    source 87
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 531
    source 27
    target 90
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa9"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 532
    source 90
    target 66
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa11"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 533
    source 13
    target 94
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa35"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 534
    source 94
    target 37
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa55"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 535
    source 94
    target 59
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa5"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 536
    source 42
    target 98
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa32"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 537
    source 98
    target 24
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa31"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 538
    source 30
    target 101
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa179"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 539
    source 104
    target 56
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa19"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 540
    source 61
    target 108
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa17"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 541
    source 108
    target 22
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa18"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 542
    source 114
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 543
    source 120
    target 59
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa5"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 544
    source 120
    target 12
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa171"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 545
    source 27
    target 124
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa9"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 546
    source 6
    target 124
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa54"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 547
    source 124
    target 1
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa12"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 548
    source 52
    target 131
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa3"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 549
    source 35
    target 135
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa5"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 550
    source 138
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 551
    source 5
    target 141
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa8"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 552
    source 65
    target 144
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa30"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 553
    source 144
    target 41
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa29"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 554
    source 144
    target 72
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa162"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 555
    source 23
    target 148
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa20"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 556
    source 148
    target 68
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa22"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 557
    source 148
    target 72
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa162"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 558
    source 3
    target 152
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa42"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 559
    source 152
    target 61
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa17"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 560
    source 1
    target 156
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa12"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 561
    source 156
    target 6
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa54"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 562
    source 156
    target 27
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa9"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 563
    source 59
    target 160
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa5"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 564
    source 160
    target 52
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa3"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 565
    source 160
    target 21
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa2"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 566
    source 164
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 567
    source 66
    target 167
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa11"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 568
    source 167
    target 44
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa14"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 569
    source 68
    target 171
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa22"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 570
    source 171
    target 59
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa5"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 571
    source 19
    target 171
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR0"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa23"
    ]
  ]
  edge [
    id 572
    source 175
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 573
    source 2
    target 178
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 574
    source 178
    target 9
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa25"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 575
    source 44
    target 184
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa14"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 576
    source 61
    target 189
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa17"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 577
    source 189
    target 3
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa42"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 578
    source 193
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 579
    source 4
    target 196
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa1"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 580
    source 45
    target 199
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa41"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 581
    source 199
    target 3
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa42"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 582
    source 25
    target 210
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa1"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 583
    source 210
    target 21
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa2"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 584
    source 28
    target 214
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa34"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 585
    source 44
    target 214
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa14"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 586
    source 214
    target 10
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 587
    source 219
    target 27
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa9"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 588
    source 223
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 589
    source 226
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 590
    source 10
    target 229
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 591
    source 229
    target 73
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa28"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 592
    source 25
    target 236
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa1"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 593
    source 41
    target 240
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa29"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 594
    source 240
    target 13
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa35"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 595
    source 19
    target 240
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR1"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa23"
    ]
  ]
  edge [
    id 596
    source 244
    target 25
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa1"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 597
    source 248
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 598
    source 42
    target 251
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa32"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 599
    source 251
    target 10
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 600
    source 251
    target 3
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa42"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 601
    source 59
    target 255
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa5"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 602
    source 37
    target 255
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa55"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 603
    source 255
    target 13
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa35"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 604
    source 10
    target 259
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 605
    source 3
    target 259
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa42"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 606
    source 259
    target 42
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa32"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 607
    source 263
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 608
    source 266
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 609
    source 269
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 610
    source 56
    target 272
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa19"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 611
    source 12
    target 276
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa171"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 612
    source 59
    target 276
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa5"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 613
    source 72
    target 283
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa162"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 614
    source 283
    target 12
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa171"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 615
    source 21
    target 287
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa2"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 616
    source 52
    target 287
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa3"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 617
    source 287
    target 59
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa5"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 618
    source 7
    target 291
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa180"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 619
    source 294
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 620
    source 27
    target 297
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa9"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 621
    source 19
    target 301
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa23"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 622
    source 301
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 623
    source 304
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 624
    source 307
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 625
    source 310
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 626
    source 72
    target 313
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa162"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 627
    source 313
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 628
    source 9
    target 313
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR3"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa25"
    ]
  ]
  edge [
    id 629
    source 45
    target 317
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa41"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 630
    source 317
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 631
    source 28
    target 320
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa34"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 632
    source 320
    target 13
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa35"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 633
    source 320
    target 12
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa171"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 634
    source 324
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 635
    source 3
    target 330
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa42"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 636
    source 330
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 637
    source 336
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 638
    source 14
    target 339
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa278"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 639
    source 339
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 640
    source 55
    target 342
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa43"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 641
    source 62
    target 342
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa44"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 642
    source 342
    target 45
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa41"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 643
    source 342
    target 36
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa45"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 644
    source 342
    target 34
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa46"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 645
    source 73
    target 349
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa28"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 646
    source 349
    target 10
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 647
    source 353
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 648
    source 356
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 649
    source 50
    target 359
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa4"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 650
    source 40
    target 362
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa2"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 651
    source 14
    target 365
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa278"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 652
    source 365
    target 37
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa55"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 653
    source 31
    target 368
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa256"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 654
    source 48
    target 371
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa6"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 655
    source 21
    target 374
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa2"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 656
    source 374
    target 25
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa1"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 657
    source 26
    target 378
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa7"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 658
    source 384
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 659
    source 2
    target 390
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 660
    source 390
    target 19
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa23"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 661
    source 22
    target 393
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa18"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 662
    source 393
    target 23
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa20"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 663
    source 46
    target 397
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa178"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 664
    source 38
    target 400
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa3"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 665
    source 37
    target 403
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa55"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 666
    source 403
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 667
    source 406
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 668
    source 409
    target 52
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa3"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 669
    source 362
    target 413
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa10"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 670
    source 413
    target 181
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa10"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 671
    source 413
    target 269
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa10"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 673
    source 236
    target 413
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa18"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 674
    source 413
    target 223
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa18"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 675
    source 413
    target 244
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa18"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 676
    source 101
    target 421
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa174"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 677
    source 421
    target 111
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa174"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 678
    source 421
    target 248
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa174"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 680
    source 297
    target 421
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa176"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 681
    source 421
    target 164
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa176"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 682
    source 421
    target 219
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa176"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 683
    source 131
    target 429
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa20"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 684
    source 233
    target 429
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa20"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 685
    source 429
    target 307
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa20"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 686
    source 429
    target 409
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa20"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 687
    source 359
    target 429
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa12"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 688
    source 429
    target 84
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa12"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 690
    source 184
    target 437
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa102"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 691
    source 214
    target 437
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa102"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 692
    source 381
    target 437
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa102"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 693
    source 437
    target 81
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa102"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 694
    source 437
    target 167
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa102"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 695
    source 368
    target 437
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa255"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 696
    source 437
    target 353
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa255"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 698
    source 207
    target 446
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa118"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 699
    source 446
    target 90
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa118"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 700
    source 446
    target 294
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa118"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 701
    source 397
    target 446
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa173"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 702
    source 446
    target 114
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa173"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 704
    source 400
    target 453
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa11"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 705
    source 453
    target 128
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa11"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 706
    source 453
    target 310
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa11"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 708
    source 374
    target 453
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa19"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 709
    source 453
    target 210
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa19"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 710
    source 453
    target 324
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa19"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 711
    source 276
    target 461
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa4"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 712
    source 461
    target 120
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa4"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 713
    source 461
    target 184
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa4"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 715
    source 189
    target 461
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa15"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 716
    source 272
    target 461
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa15"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 717
    source 461
    target 104
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa15"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 718
    source 461
    target 152
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa15"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 719
    source 141
    target 470
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa16"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 720
    source 470
    target 226
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa16"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 721
    source 470
    target 280
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa16"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 722
    source 470
    target 283
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR2"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa24"
    ]
  ]
  edge [
    id 724
    source 470
    target 78
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa24"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 725
    source 199
    target 479
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa22"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 726
    source 333
    target 479
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa22"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 727
    source 479
    target 342
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa22"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 728
    source 479
    target 406
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa22"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 729
    source 371
    target 479
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa14"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 730
    source 479
    target 193
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa14"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 732
    source 117
    target 487
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa23"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 733
    source 272
    target 487
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa23"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 734
    source 349
    target 487
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa23"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 735
    source 393
    target 487
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa23"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 736
    source 487
    target 104
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa23"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 737
    source 487
    target 175
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa23"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 738
    source 487
    target 199
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa23"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 739
    source 487
    target 229
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa23"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 740
    source 378
    target 487
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa15"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 742
    source 487
    target 263
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa15"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 743
    source 291
    target 499
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa175"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 744
    source 499
    target 138
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa175"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 745
    source 499
    target 327
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa175"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 746
    source 297
    target 499
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa177"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 748
    source 499
    target 219
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa177"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 749
    source 499
    target 384
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa177"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 750
    source 196
    target 507
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa9"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 751
    source 507
    target 204
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa9"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 752
    source 507
    target 266
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa9"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 754
    source 236
    target 507
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa17"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 755
    source 507
    target 244
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa17"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 756
    source 507
    target 304
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa17"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 757
    source 131
    target 515
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa21"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 758
    source 387
    target 515
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa21"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 759
    source 515
    target 336
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa21"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 760
    source 515
    target 409
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa21"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 761
    source 135
    target 515
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa13"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 762
    source 515
    target 356
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa13"
      stoichiometry "1.0"
    ]
  ]
]
