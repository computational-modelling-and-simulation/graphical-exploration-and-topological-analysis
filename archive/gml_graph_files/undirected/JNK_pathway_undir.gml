# generated with VANTED V2.8.0 at Tue Apr 27 20:00:47 CEST 2021
graph [
  background_coloring "true"
  cluster_colors "255,128,128,255:0,0,0,255;230,255,128,255:0,0,0,255;128,255,179,255:0,0,0,255;128,178,255,255:0,0,0,255;230,128,255,255:0,0,0,255"
  clusterbackground_fill_outer_region "false"
  clusterbackground_grid 50.0
  clusterbackground_low_alpha 0.2
  clusterbackground_radius 200.0
  clusterbackground_space_fill "true"
  graphbackgroundcolor "#ffffff"
  sbml [
    model_meta_id "JNK_signaling_pathway"
    model_non_rdf_annotation ""
    model_notes ""
    model_sbml_id "JNK_signaling_pathway"
    namespace "xmlns:celldesigner=\"http://www.sbml.org/2001/ns/celldesigner\" "
    sbml_level "2"
    sbml_version "4"
  ]
  sbml_compartment_default [
    sbml_compartment_default_id "default"
    sbml_compartment_default_meta_id "default"
    sbml_compartment_default_non_rdf_annotation ""
    sbml_compartment_default_notes ""
    sbml_compartment_default_size "1.0"
    sbml_compartment_default_units "volume"
  ]
  sbml_compartment_s_id_ca1 [
    sbml_compartment_s_id_ca1_annotation ""
    sbml_compartment_s_id_ca1_id "s_id_ca1"
    sbml_compartment_s_id_ca1_meta_id "s_id_ca1"
    sbml_compartment_s_id_ca1_name "mitochondrion"
    sbml_compartment_s_id_ca1_non_rdf_annotation ""
    sbml_compartment_s_id_ca1_notes ""
    sbml_compartment_s_id_ca1_outside "s_id_ca5"
    sbml_compartment_s_id_ca1_size "1.0"
    sbml_compartment_s_id_ca1_units "volume"
  ]
  sbml_compartment_s_id_ca2 [
    sbml_compartment_s_id_ca2_id "s_id_ca2"
    sbml_compartment_s_id_ca2_meta_id "s_id_ca2"
    sbml_compartment_s_id_ca2_name "human_space_host"
    sbml_compartment_s_id_ca2_non_rdf_annotation ""
    sbml_compartment_s_id_ca2_notes ""
    sbml_compartment_s_id_ca2_outside "default"
    sbml_compartment_s_id_ca2_size "1.0"
    sbml_compartment_s_id_ca2_units "volume"
  ]
  sbml_compartment_s_id_ca3 [
    sbml_compartment_s_id_ca3_annotation ""
    sbml_compartment_s_id_ca3_id "s_id_ca3"
    sbml_compartment_s_id_ca3_meta_id "s_id_ca3"
    sbml_compartment_s_id_ca3_name "nucleus"
    sbml_compartment_s_id_ca3_non_rdf_annotation ""
    sbml_compartment_s_id_ca3_notes ""
    sbml_compartment_s_id_ca3_outside "s_id_ca5"
    sbml_compartment_s_id_ca3_size "1.0"
    sbml_compartment_s_id_ca3_units "volume"
  ]
  sbml_compartment_s_id_ca5 [
    sbml_compartment_s_id_ca5_annotation ""
    sbml_compartment_s_id_ca5_id "s_id_ca5"
    sbml_compartment_s_id_ca5_meta_id "s_id_ca5"
    sbml_compartment_s_id_ca5_name "cell"
    sbml_compartment_s_id_ca5_non_rdf_annotation ""
    sbml_compartment_s_id_ca5_notes ""
    sbml_compartment_s_id_ca5_outside "s_id_ca2"
    sbml_compartment_s_id_ca5_size "1.0"
    sbml_compartment_s_id_ca5_units "volume"
  ]
  sbml_unit_definition_1 [
    sbml_unit_definition_1_id "area"
    sbml_unit_definition_1_name "area"
    sbml_unit_definition_1_sub_unit_1_ "(1.0 * 10^0 * metre)^2.0"
    sbml_unit_definition_1unit "(1.0 * 10^0 * metre)^2.0"
  ]
  sbml_unit_definition_2 [
    sbml_unit_definition_2_id "length"
    sbml_unit_definition_2_name "length"
    sbml_unit_definition_2_sub_unit_1_ "(1.0 * 10^0 * metre)^1.0"
    sbml_unit_definition_2unit "(1.0 * 10^0 * metre)^1.0"
  ]
  sbml_unit_definition_3 [
    sbml_unit_definition_3_id "substance"
    sbml_unit_definition_3_name "substance"
    sbml_unit_definition_3_sub_unit_1_ "(1.0 * 10^0 * mole)^1.0"
    sbml_unit_definition_3unit "(1.0 * 10^0 * mole)^1.0"
  ]
  sbml_unit_definition_4 [
    sbml_unit_definition_4_id "time"
    sbml_unit_definition_4_name "time"
    sbml_unit_definition_4_sub_unit_1_ "(1.0 * 10^0 * second)^1.0"
    sbml_unit_definition_4unit "(1.0 * 10^0 * second)^1.0"
  ]
  sbml_unit_definition_5 [
    sbml_unit_definition_5_id "volume"
    sbml_unit_definition_5_name "volume"
    sbml_unit_definition_5_sub_unit_1_ "(1.0 * 10^0 * litre)^1.0"
    sbml_unit_definition_5unit "(1.0 * 10^0 * litre)^1.0"
  ]
  directed 0
  node [
    id 1
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 200.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "3b__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa75"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa75"
      minerva_name "3b"
      minerva_ref_link1 "http://purl.uniprot.org/uniprot/P59633"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/1489670"
      minerva_ref_type__resource1 "UNIPROT__P59633"
      minerva_ref_type__resource2 "ENTREZ__1489670"
      minerva_type "Protein"
      minerva_x 1123.0
      minerva_y 150.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa75"
      species_meta_id "s_id_sa75"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 2
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 300.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "bcl2__mitochondrion__phosphorylated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa12"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa12"
      minerva_name "BCL2"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BCL2"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BCL2"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_000657"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/596"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P10415"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P10415"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000171791"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_000633"
      minerva_ref_link9 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/990"
      minerva_ref_type__resource1 "HGNC_SYMBOL__BCL2"
      minerva_ref_type__resource2 "HGNC_SYMBOL__BCL2"
      minerva_ref_type__resource3 "REFSEQ__NM_000657"
      minerva_ref_type__resource4 "ENTREZ__596"
      minerva_ref_type__resource5 "UNIPROT__P10415"
      minerva_ref_type__resource6 "UNIPROT__P10415"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000171791"
      minerva_ref_type__resource8 "REFSEQ__NM_000633"
      minerva_ref_type__resource9 "HGNC__990"
      minerva_state1 "PHOSPHORYLATED"
      minerva_type "Protein"
      minerva_x 1186.0
      minerva_y 751.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "mitochondrion"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa12"
      species_meta_id "s_id_sa12"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 3
    zlevel -1

    cluster [
      cluster "s_id_ca1"
    ]
    graphics [
      x 400.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "bcl2__mitochondrion__empty__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa53"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa53"
      minerva_name "BCL2"
      minerva_ref_link1 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BCL2"
      minerva_ref_link2 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=BCL2"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/protein/NM_000657"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/596"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/P10415"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P10415"
      minerva_ref_link7 "https://www.ensembl.org/id/ENSG00000171791"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_000633"
      minerva_ref_link9 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/990"
      minerva_ref_type__resource1 "HGNC_SYMBOL__BCL2"
      minerva_ref_type__resource2 "HGNC_SYMBOL__BCL2"
      minerva_ref_type__resource3 "REFSEQ__NM_000657"
      minerva_ref_type__resource4 "ENTREZ__596"
      minerva_ref_type__resource5 "UNIPROT__P10415"
      minerva_ref_type__resource6 "UNIPROT__P10415"
      minerva_ref_type__resource7 "ENSEMBL__ENSG00000171791"
      minerva_ref_type__resource8 "REFSEQ__NM_000633"
      minerva_ref_type__resource9 "HGNC__990"
      minerva_state1 "EMPTY"
      minerva_type "Protein"
      minerva_x 1018.5
      minerva_y 751.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca1"
      compartment_name "mitochondrion"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa53"
      species_meta_id "s_id_sa53"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 4
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 500.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "map2k7__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa44"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa44"
      minerva_former_symbols "PRKMK7"
      minerva_name "MAP2K7"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.12.2"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/5609"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/O14733"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/O14733"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000076984"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6847"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_001297555"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP2K7"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP2K7"
      minerva_ref_type__resource1 "EC__2.7.12.2"
      minerva_ref_type__resource2 "ENTREZ__5609"
      minerva_ref_type__resource3 "UNIPROT__O14733"
      minerva_ref_type__resource4 "UNIPROT__O14733"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000076984"
      minerva_ref_type__resource6 "HGNC__6847"
      minerva_ref_type__resource7 "REFSEQ__NM_001297555"
      minerva_ref_type__resource8 "HGNC_SYMBOL__MAP2K7"
      minerva_ref_type__resource9 "HGNC_SYMBOL__MAP2K7"
      minerva_type "Protein"
      minerva_x 373.6250000000002
      minerva_y 421.33088235294235
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa44"
      species_meta_id "s_id_sa44"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 5
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 600.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "7a__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa76"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa76"
      minerva_name "7a"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/P59635"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/1489674"
      minerva_ref_type__resource1 "UNIPROT__P59635"
      minerva_ref_type__resource2 "ENTREZ__1489674"
      minerva_type "Protein"
      minerva_x 584.0
      minerva_y 150.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa76"
      species_meta_id "s_id_sa76"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 7
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 800.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "s__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa78"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa78"
      minerva_name "S"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/gene/1489668"
      minerva_ref_link2 "http://purl.uniprot.org/uniprot/P59594"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=S"
      minerva_ref_type__resource1 "ENTREZ__1489668"
      minerva_ref_type__resource2 "UNIPROT__P59594"
      minerva_ref_type__resource3 "HGNC_SYMBOL__S"
      minerva_type "Protein"
      minerva_x 690.0
      minerva_y 150.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa78"
      species_meta_id "s_id_sa78"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 9
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 1000.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "map2k7__cell__phosphorylated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa5"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa5"
      minerva_former_symbols "PRKMK7"
      minerva_name "MAP2K7"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.12.2"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/5609"
      minerva_ref_link3 "http://purl.uniprot.org/uniprot/O14733"
      minerva_ref_link4 "http://purl.uniprot.org/uniprot/O14733"
      minerva_ref_link5 "https://www.ensembl.org/id/ENSG00000076984"
      minerva_ref_link6 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6847"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/protein/NM_001297555"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP2K7"
      minerva_ref_link9 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP2K7"
      minerva_ref_type__resource1 "EC__2.7.12.2"
      minerva_ref_type__resource2 "ENTREZ__5609"
      minerva_ref_type__resource3 "UNIPROT__O14733"
      minerva_ref_type__resource4 "UNIPROT__O14733"
      minerva_ref_type__resource5 "ENSEMBL__ENSG00000076984"
      minerva_ref_type__resource6 "HGNC__6847"
      minerva_ref_type__resource7 "REFSEQ__NM_001297555"
      minerva_ref_type__resource8 "HGNC_SYMBOL__MAP2K7"
      minerva_ref_type__resource9 "HGNC_SYMBOL__MAP2K7"
      minerva_state1 "PHOSPHORYLATED"
      minerva_type "Protein"
      minerva_x 562.0
      minerva_y 422.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa5"
      species_meta_id "s_id_sa5"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 10
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 1100.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tp53_space_signalling__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa36"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa36"
      minerva_fullName "signal transduction by p53 class mediator"
      minerva_name "TP53 signalling"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0072331"
      minerva_ref_type__resource1 "GO__GO:0072331"
      minerva_type "Phenotype"
      minerva_x 1452.5
      minerva_y 969.5
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa36"
      species_meta_id "s_id_sa36"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 12
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 200.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "atf2__cell__phosphorylated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa33"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa33"
      minerva_former_symbols "CREB2"
      minerva_name "ATF2"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_001880"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15336"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P15336"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/1386"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/784"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATF2"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATF2"
      minerva_ref_link8 "https://www.ensembl.org/id/ENSG00000115966"
      minerva_ref_type__resource1 "REFSEQ__NM_001880"
      minerva_ref_type__resource2 "UNIPROT__P15336"
      minerva_ref_type__resource3 "UNIPROT__P15336"
      minerva_ref_type__resource4 "ENTREZ__1386"
      minerva_ref_type__resource5 "HGNC__784"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATF2"
      minerva_ref_type__resource7 "HGNC_SYMBOL__ATF2"
      minerva_ref_type__resource8 "ENSEMBL__ENSG00000115966"
      minerva_state1 "PHOSPHORYLATED"
      minerva_type "Protein"
      minerva_x 1090.0
      minerva_y 908.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa33"
      species_meta_id "s_id_sa33"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 16
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 600.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "sars_minus_cov_minus_1_space_proteins__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa5"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa5"
      minerva_name "SARS-CoV-1 proteins"
      minerva_type "Complex"
      minerva_x 278.5
      minerva_y 121.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa5"
      species_meta_id "s_id_csa5"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 17
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 700.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "apoptosis__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa17"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa17"
      minerva_fullName "apoptotic process"
      minerva_name "Apoptosis"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0006915"
      minerva_ref_type__resource1 "GO__GO:0006915"
      minerva_type "Phenotype"
      minerva_x 1452.5
      minerva_y 778.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa17"
      species_meta_id "s_id_sa17"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 18
    zlevel -1

    cluster [
      cluster "s_id_ca2"
    ]
    graphics [
      x 800.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "3a__human_space_host__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa77"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa77"
      minerva_name "3a"
      minerva_ref_link1 "https://purl.uniprot.org/uniprot/P59632"
      minerva_ref_link2 "https://www.ncbi.nlm.nih.gov/gene/1489669"
      minerva_ref_type__resource1 "UNIPROT__P59632"
      minerva_ref_type__resource2 "ENTREZ__1489669"
      minerva_type "Protein"
      minerva_x 484.0
      minerva_y 150.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca2"
      compartment_name "human_space_host"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa77"
      species_meta_id "s_id_sa77"
      species_non_rdf_annotation ""
    ]
  ]
  node [
    id 19
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 900.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "autophagy__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa18"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa18"
      minerva_fullName "autophagy"
      minerva_name "Autophagy"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0006914"
      minerva_ref_type__resource1 "GO__GO:0006914"
      minerva_type "Phenotype"
      minerva_x 1452.5
      minerva_y 732.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa18"
      species_meta_id "s_id_sa18"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 20
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 1000.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tp53__cell__phosphorylated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa34"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa34"
      minerva_name "TP53"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000141510"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11998"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7157"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TP53"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TP53"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P04637"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/P04637"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_000546"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000141510"
      minerva_ref_type__resource2 "HGNC__11998"
      minerva_ref_type__resource3 "ENTREZ__7157"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TP53"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TP53"
      minerva_ref_type__resource6 "UNIPROT__P04637"
      minerva_ref_type__resource7 "UNIPROT__P04637"
      minerva_ref_type__resource8 "REFSEQ__NM_000546"
      minerva_state1 "PHOSPHORYLATED"
      minerva_type "Protein"
      minerva_x 1085.0
      minerva_y 970.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa34"
      species_meta_id "s_id_sa34"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 21
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 1100.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "atf2__cell__empty__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa71"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa71"
      minerva_former_symbols "CREB2"
      minerva_name "ATF2"
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_001880"
      minerva_ref_link2 "https://purl.uniprot.org/uniprot/P15336"
      minerva_ref_link3 "https://purl.uniprot.org/uniprot/P15336"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/gene/1386"
      minerva_ref_link5 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/784"
      minerva_ref_link6 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATF2"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=ATF2"
      minerva_ref_link8 "https://www.ensembl.org/id/ENSG00000115966"
      minerva_ref_type__resource1 "REFSEQ__NM_001880"
      minerva_ref_type__resource2 "UNIPROT__P15336"
      minerva_ref_type__resource3 "UNIPROT__P15336"
      minerva_ref_type__resource4 "ENTREZ__1386"
      minerva_ref_type__resource5 "HGNC__784"
      minerva_ref_type__resource6 "HGNC_SYMBOL__ATF2"
      minerva_ref_type__resource7 "HGNC_SYMBOL__ATF2"
      minerva_ref_type__resource8 "ENSEMBL__ENSG00000115966"
      minerva_state1 "EMPTY"
      minerva_type "Protein"
      minerva_x 813.5555555555557
      minerva_y 907.7777777777778
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa71"
      species_meta_id "s_id_sa71"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 23
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 200.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "map2k4__cell__phosphorylated__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa4"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa4"
      minerva_former_symbols "SERK1"
      minerva_name "MAP2K4"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.12.2"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6844"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000065559"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_001281435"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P45985"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P45985"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP2K4"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP2K4"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/6416"
      minerva_ref_type__resource1 "EC__2.7.12.2"
      minerva_ref_type__resource2 "HGNC__6844"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000065559"
      minerva_ref_type__resource4 "REFSEQ__NM_001281435"
      minerva_ref_type__resource5 "UNIPROT__P45985"
      minerva_ref_type__resource6 "UNIPROT__P45985"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MAP2K4"
      minerva_ref_type__resource8 "HGNC_SYMBOL__MAP2K4"
      minerva_ref_type__resource9 "ENTREZ__6416"
      minerva_state1 "PHOSPHORYLATED"
      minerva_type "Protein"
      minerva_x 577.0
      minerva_y 618.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa4"
      species_meta_id "s_id_sa4"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 25
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 400.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "innate_space_immunity__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa16"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa16"
      minerva_fullName "innate immune response"
      minerva_name "Innate Immunity"
      minerva_ref_link1 "http://amigo.geneontology.org/amigo/term/GO:0045087"
      minerva_ref_type__resource1 "GO__GO:0045087"
      minerva_type "Phenotype"
      minerva_x 1452.5
      minerva_y 474.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa16"
      species_meta_id "s_id_sa16"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 26
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 500.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "tp53__cell__empty__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa70"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa70"
      minerva_name "TP53"
      minerva_ref_link1 "https://www.ensembl.org/id/ENSG00000141510"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/11998"
      minerva_ref_link3 "https://www.ncbi.nlm.nih.gov/gene/7157"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TP53"
      minerva_ref_link5 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=TP53"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/P04637"
      minerva_ref_link7 "https://purl.uniprot.org/uniprot/P04637"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/protein/NM_000546"
      minerva_ref_type__resource1 "ENSEMBL__ENSG00000141510"
      minerva_ref_type__resource2 "HGNC__11998"
      minerva_ref_type__resource3 "ENTREZ__7157"
      minerva_ref_type__resource4 "HGNC_SYMBOL__TP53"
      minerva_ref_type__resource5 "HGNC_SYMBOL__TP53"
      minerva_ref_type__resource6 "UNIPROT__P04637"
      minerva_ref_type__resource7 "UNIPROT__P04637"
      minerva_ref_type__resource8 "REFSEQ__NM_000546"
      minerva_state1 "EMPTY"
      minerva_type "Protein"
      minerva_x 808.5555555555557
      minerva_y 969.7777777777778
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa70"
      species_meta_id "s_id_sa70"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 27
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 600.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "map2k4__cell__empty__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_sa69"
      type "text"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "sa69"
      minerva_former_symbols "SERK1"
      minerva_name "MAP2K4"
      minerva_ref_link1 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.12.2"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6844"
      minerva_ref_link3 "https://www.ensembl.org/id/ENSG00000065559"
      minerva_ref_link4 "https://www.ncbi.nlm.nih.gov/protein/NM_001281435"
      minerva_ref_link5 "http://purl.uniprot.org/uniprot/P45985"
      minerva_ref_link6 "http://purl.uniprot.org/uniprot/P45985"
      minerva_ref_link7 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP2K4"
      minerva_ref_link8 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP2K4"
      minerva_ref_link9 "https://www.ncbi.nlm.nih.gov/gene/6416"
      minerva_ref_type__resource1 "EC__2.7.12.2"
      minerva_ref_type__resource2 "HGNC__6844"
      minerva_ref_type__resource3 "ENSEMBL__ENSG00000065559"
      minerva_ref_type__resource4 "REFSEQ__NM_001281435"
      minerva_ref_type__resource5 "UNIPROT__P45985"
      minerva_ref_type__resource6 "UNIPROT__P45985"
      minerva_ref_type__resource7 "HGNC_SYMBOL__MAP2K4"
      minerva_ref_type__resource8 "HGNC_SYMBOL__MAP2K4"
      minerva_ref_type__resource9 "ENTREZ__6416"
      minerva_state1 "EMPTY"
      minerva_type "Protein"
      minerva_x 377.55555555555566
      minerva_y 618.4444444444443
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_annotation ""
      species_id "s_id_sa69"
      species_meta_id "s_id_sa69"
      species_non_rdf_annotation ""
      species_notes ""
    ]
  ]
  node [
    id 28
    zlevel -1

    graphics [
      x 700.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re43"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_001291958"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.25"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6856"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP3K4"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP3K4"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/Q9Y6R4"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/Q9Y6R4"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4216"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4216"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000085511"
      minerva_ref_type__resource1 "REFSEQ__NM_001291958"
      minerva_ref_type__resource10 "EC__2.7.11.25"
      minerva_ref_type__resource2 "HGNC__6856"
      minerva_ref_type__resource3 "HGNC_SYMBOL__MAP3K4"
      minerva_ref_type__resource4 "HGNC_SYMBOL__MAP3K4"
      minerva_ref_type__resource5 "UNIPROT__Q9Y6R4"
      minerva_ref_type__resource6 "UNIPROT__Q9Y6R4"
      minerva_ref_type__resource7 "ENTREZ__4216"
      minerva_ref_type__resource8 "ENTREZ__4216"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000085511"
      minerva_type "Negative influence"
      minerva_x 1319.25
      minerva_y 769.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re43"
      reaction_meta_id "re43"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 31
    zlevel -1

    graphics [
      x 800.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re22"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_001291958"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.25"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6856"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP3K4"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP3K4"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/Q9Y6R4"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/Q9Y6R4"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4216"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4216"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000085511"
      minerva_ref_type__resource1 "REFSEQ__NM_001291958"
      minerva_ref_type__resource10 "EC__2.7.11.25"
      minerva_ref_type__resource2 "HGNC__6856"
      minerva_ref_type__resource3 "HGNC_SYMBOL__MAP3K4"
      minerva_ref_type__resource4 "HGNC_SYMBOL__MAP3K4"
      minerva_ref_type__resource5 "UNIPROT__Q9Y6R4"
      minerva_ref_type__resource6 "UNIPROT__Q9Y6R4"
      minerva_ref_type__resource7 "ENTREZ__4216"
      minerva_ref_type__resource8 "ENTREZ__4216"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000085511"
      minerva_type "Trigger"
      minerva_x 1334.0
      minerva_y 474.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re22"
      reaction_meta_id "re22"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 34
    zlevel -1

    graphics [
      x 900.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re36"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_001291958"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.25"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6856"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP3K4"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP3K4"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/Q9Y6R4"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/Q9Y6R4"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4216"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4216"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000085511"
      minerva_ref_type__resource1 "REFSEQ__NM_001291958"
      minerva_ref_type__resource10 "EC__2.7.11.25"
      minerva_ref_type__resource2 "HGNC__6856"
      minerva_ref_type__resource3 "HGNC_SYMBOL__MAP3K4"
      minerva_ref_type__resource4 "HGNC_SYMBOL__MAP3K4"
      minerva_ref_type__resource5 "UNIPROT__Q9Y6R4"
      minerva_ref_type__resource6 "UNIPROT__Q9Y6R4"
      minerva_ref_type__resource7 "ENTREZ__4216"
      minerva_ref_type__resource8 "ENTREZ__4216"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000085511"
      minerva_type "State transition"
      minerva_x 1122.7777777777778
      minerva_y 474.0277777777776
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re36"
      reaction_meta_id "re36"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 39
    zlevel -1

    graphics [
      x 1000.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re26"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_001291958"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.25"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6856"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP3K4"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP3K4"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/Q9Y6R4"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/Q9Y6R4"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4216"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4216"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000085511"
      minerva_ref_type__resource1 "REFSEQ__NM_001291958"
      minerva_ref_type__resource10 "EC__2.7.11.25"
      minerva_ref_type__resource2 "HGNC__6856"
      minerva_ref_type__resource3 "HGNC_SYMBOL__MAP3K4"
      minerva_ref_type__resource4 "HGNC_SYMBOL__MAP3K4"
      minerva_ref_type__resource5 "UNIPROT__Q9Y6R4"
      minerva_ref_type__resource6 "UNIPROT__Q9Y6R4"
      minerva_ref_type__resource7 "ENTREZ__4216"
      minerva_ref_type__resource8 "ENTREZ__4216"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000085511"
      minerva_type "State transition"
      minerva_x 946.7777777777778
      minerva_y 969.8888888888889
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re26"
      reaction_meta_id "re26"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 43
    zlevel -1

    graphics [
      x 1100.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re35"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_001291958"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.25"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6856"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP3K4"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP3K4"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/Q9Y6R4"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/Q9Y6R4"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4216"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4216"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000085511"
      minerva_ref_type__resource1 "REFSEQ__NM_001291958"
      minerva_ref_type__resource10 "EC__2.7.11.25"
      minerva_ref_type__resource2 "HGNC__6856"
      minerva_ref_type__resource3 "HGNC_SYMBOL__MAP3K4"
      minerva_ref_type__resource4 "HGNC_SYMBOL__MAP3K4"
      minerva_ref_type__resource5 "UNIPROT__Q9Y6R4"
      minerva_ref_type__resource6 "UNIPROT__Q9Y6R4"
      minerva_ref_type__resource7 "ENTREZ__4216"
      minerva_ref_type__resource8 "ENTREZ__4216"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000085511"
      minerva_type "State transition"
      minerva_x 1102.25
      minerva_y 751.0
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re35"
      reaction_meta_id "re35"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 47
    zlevel -1

    graphics [
      x 1200.0
      y 300.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re2"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_001291958"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.25"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6856"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP3K4"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP3K4"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/Q9Y6R4"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/Q9Y6R4"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4216"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4216"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000085511"
      minerva_ref_type__resource1 "REFSEQ__NM_001291958"
      minerva_ref_type__resource10 "EC__2.7.11.25"
      minerva_ref_type__resource2 "HGNC__6856"
      minerva_ref_type__resource3 "HGNC_SYMBOL__MAP3K4"
      minerva_ref_type__resource4 "HGNC_SYMBOL__MAP3K4"
      minerva_ref_type__resource5 "UNIPROT__Q9Y6R4"
      minerva_ref_type__resource6 "UNIPROT__Q9Y6R4"
      minerva_ref_type__resource7 "ENTREZ__4216"
      minerva_ref_type__resource8 "ENTREZ__4216"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000085511"
      minerva_type "State transition"
      minerva_x 467.8125
      minerva_y 421.7556339511377
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re2"
      reaction_meta_id "re2"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 52
    zlevel -1

    graphics [
      x 200.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re25"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_001291958"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.25"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6856"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP3K4"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP3K4"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/Q9Y6R4"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/Q9Y6R4"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4216"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4216"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000085511"
      minerva_ref_type__resource1 "REFSEQ__NM_001291958"
      minerva_ref_type__resource10 "EC__2.7.11.25"
      minerva_ref_type__resource2 "HGNC__6856"
      minerva_ref_type__resource3 "HGNC_SYMBOL__MAP3K4"
      minerva_ref_type__resource4 "HGNC_SYMBOL__MAP3K4"
      minerva_ref_type__resource5 "UNIPROT__Q9Y6R4"
      minerva_ref_type__resource6 "UNIPROT__Q9Y6R4"
      minerva_ref_type__resource7 "ENTREZ__4216"
      minerva_ref_type__resource8 "ENTREZ__4216"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000085511"
      minerva_type "State transition"
      minerva_x 951.7777777777778
      minerva_y 907.8888888888889
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re25"
      reaction_meta_id "re25"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 56
    zlevel -1

    graphics [
      x 300.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re23"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_001291958"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.25"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6856"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP3K4"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP3K4"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/Q9Y6R4"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/Q9Y6R4"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4216"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4216"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000085511"
      minerva_ref_type__resource1 "REFSEQ__NM_001291958"
      minerva_ref_type__resource10 "EC__2.7.11.25"
      minerva_ref_type__resource2 "HGNC__6856"
      minerva_ref_type__resource3 "HGNC_SYMBOL__MAP3K4"
      minerva_ref_type__resource4 "HGNC_SYMBOL__MAP3K4"
      minerva_ref_type__resource5 "UNIPROT__Q9Y6R4"
      minerva_ref_type__resource6 "UNIPROT__Q9Y6R4"
      minerva_ref_type__resource7 "ENTREZ__4216"
      minerva_ref_type__resource8 "ENTREZ__4216"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000085511"
      minerva_type "State transition"
      minerva_x 236.8125000000001
      minerva_y 316.16544117647106
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re23"
      reaction_meta_id "re23"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 60
    zlevel -1

    graphics [
      x 400.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re4"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_001291958"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.25"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6856"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP3K4"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP3K4"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/Q9Y6R4"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/Q9Y6R4"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4216"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4216"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000085511"
      minerva_ref_type__resource1 "REFSEQ__NM_001291958"
      minerva_ref_type__resource10 "EC__2.7.11.25"
      minerva_ref_type__resource2 "HGNC__6856"
      minerva_ref_type__resource3 "HGNC_SYMBOL__MAP3K4"
      minerva_ref_type__resource4 "HGNC_SYMBOL__MAP3K4"
      minerva_ref_type__resource5 "UNIPROT__Q9Y6R4"
      minerva_ref_type__resource6 "UNIPROT__Q9Y6R4"
      minerva_ref_type__resource7 "ENTREZ__4216"
      minerva_ref_type__resource8 "ENTREZ__4216"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000085511"
      minerva_type "State transition"
      minerva_x 664.0
      minerva_y 693.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re4"
      reaction_meta_id "re4"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 67
    zlevel -1

    graphics [
      x 500.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re42"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_001291958"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.25"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6856"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP3K4"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP3K4"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/Q9Y6R4"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/Q9Y6R4"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4216"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4216"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000085511"
      minerva_ref_type__resource1 "REFSEQ__NM_001291958"
      minerva_ref_type__resource10 "EC__2.7.11.25"
      minerva_ref_type__resource2 "HGNC__6856"
      minerva_ref_type__resource3 "HGNC_SYMBOL__MAP3K4"
      minerva_ref_type__resource4 "HGNC_SYMBOL__MAP3K4"
      minerva_ref_type__resource5 "UNIPROT__Q9Y6R4"
      minerva_ref_type__resource6 "UNIPROT__Q9Y6R4"
      minerva_ref_type__resource7 "ENTREZ__4216"
      minerva_ref_type__resource8 "ENTREZ__4216"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000085511"
      minerva_type "State transition"
      minerva_x 689.0169178145514
      minerva_y 735.7405472866781
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re42"
      reaction_meta_id "re42"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 71
    zlevel -1

    graphics [
      x 600.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re39"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_001291958"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.25"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6856"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP3K4"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP3K4"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/Q9Y6R4"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/Q9Y6R4"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4216"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4216"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000085511"
      minerva_ref_type__resource1 "REFSEQ__NM_001291958"
      minerva_ref_type__resource10 "EC__2.7.11.25"
      minerva_ref_type__resource2 "HGNC__6856"
      minerva_ref_type__resource3 "HGNC_SYMBOL__MAP3K4"
      minerva_ref_type__resource4 "HGNC_SYMBOL__MAP3K4"
      minerva_ref_type__resource5 "UNIPROT__Q9Y6R4"
      minerva_ref_type__resource6 "UNIPROT__Q9Y6R4"
      minerva_ref_type__resource7 "ENTREZ__4216"
      minerva_ref_type__resource8 "ENTREZ__4216"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000085511"
      minerva_type "State transition"
      minerva_x 477.2777777777778
      minerva_y 618.2222222222222
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re39"
      reaction_meta_id "re39"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 76
    zlevel -1

    graphics [
      x 700.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re30"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_001291958"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.25"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6856"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP3K4"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP3K4"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/Q9Y6R4"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/Q9Y6R4"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4216"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4216"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000085511"
      minerva_ref_type__resource1 "REFSEQ__NM_001291958"
      minerva_ref_type__resource10 "EC__2.7.11.25"
      minerva_ref_type__resource2 "HGNC__6856"
      minerva_ref_type__resource3 "HGNC_SYMBOL__MAP3K4"
      minerva_ref_type__resource4 "HGNC_SYMBOL__MAP3K4"
      minerva_ref_type__resource5 "UNIPROT__Q9Y6R4"
      minerva_ref_type__resource6 "UNIPROT__Q9Y6R4"
      minerva_ref_type__resource7 "ENTREZ__4216"
      minerva_ref_type__resource8 "ENTREZ__4216"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000085511"
      minerva_type "Trigger"
      minerva_x 1245.0962383174199
      minerva_y 969.7821819886838
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re30"
      reaction_meta_id "re30"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 79
    zlevel -1

    graphics [
      x 800.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re37"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_001291958"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.25"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6856"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP3K4"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP3K4"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/Q9Y6R4"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/Q9Y6R4"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4216"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4216"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000085511"
      minerva_ref_type__resource1 "REFSEQ__NM_001291958"
      minerva_ref_type__resource10 "EC__2.7.11.25"
      minerva_ref_type__resource2 "HGNC__6856"
      minerva_ref_type__resource3 "HGNC_SYMBOL__MAP3K4"
      minerva_ref_type__resource4 "HGNC_SYMBOL__MAP3K4"
      minerva_ref_type__resource5 "UNIPROT__Q9Y6R4"
      minerva_ref_type__resource6 "UNIPROT__Q9Y6R4"
      minerva_ref_type__resource7 "ENTREZ__4216"
      minerva_ref_type__resource8 "ENTREZ__4216"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000085511"
      minerva_type "State transition"
      minerva_x 229.27777777777783
      minerva_y 527.7222222222222
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re37"
      reaction_meta_id "re37"
      reaction_non_rdf_annotation ""
      reaction_notes ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 83
    zlevel -1

    graphics [
      x 900.0
      y 400.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "re14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "re14"
      type "text"
    ]
    minerva [
      minerva_ref_link1 "https://www.ncbi.nlm.nih.gov/protein/NM_001291958"
      minerva_ref_link10 "https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=2.7.11.25"
      minerva_ref_link2 "https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/6856"
      minerva_ref_link3 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP3K4"
      minerva_ref_link4 "https://www.genenames.org/cgi-bin/gene_symbol_report?match=MAP3K4"
      minerva_ref_link5 "https://purl.uniprot.org/uniprot/Q9Y6R4"
      minerva_ref_link6 "https://purl.uniprot.org/uniprot/Q9Y6R4"
      minerva_ref_link7 "https://www.ncbi.nlm.nih.gov/gene/4216"
      minerva_ref_link8 "https://www.ncbi.nlm.nih.gov/gene/4216"
      minerva_ref_link9 "https://www.ensembl.org/id/ENSG00000085511"
      minerva_ref_type__resource1 "REFSEQ__NM_001291958"
      minerva_ref_type__resource10 "EC__2.7.11.25"
      minerva_ref_type__resource2 "HGNC__6856"
      minerva_ref_type__resource3 "HGNC_SYMBOL__MAP3K4"
      minerva_ref_type__resource4 "HGNC_SYMBOL__MAP3K4"
      minerva_ref_type__resource5 "UNIPROT__Q9Y6R4"
      minerva_ref_type__resource6 "UNIPROT__Q9Y6R4"
      minerva_ref_type__resource7 "ENTREZ__4216"
      minerva_ref_type__resource8 "ENTREZ__4216"
      minerva_ref_type__resource9 "ENSEMBL__ENSG00000085511"
      minerva_type "Trigger"
      minerva_x 1319.25
      minerva_y 741.5
    ]
    sbml [
      reaction_annotation ""
      reaction_id "re14"
      reaction_meta_id "re14"
      reaction_non_rdf_annotation ""
      reversible "false"
      sbmlRole "reaction"
    ]
  ]
  node [
    id 86
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 1050.0
      y 100.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "jnk__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa12"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa12"
      minerva_name "JNK"
      minerva_type "Complex"
      minerva_x 541.0
      minerva_y 735.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa12"
      species_meta_id "s_id_csa12"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
    ]
  ]
  node [
    id 97
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 800.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mlk1_slash_2_slash_3__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa2"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa2"
      minerva_name "MLK1/2/3"
      minerva_type "Complex"
      minerva_x 320.0
      minerva_y 316.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa2"
      species_meta_id "s_id_csa2"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
    ]
  ]
  node [
    id 103
    zlevel -1

    cluster [
      cluster "s_id_ca5"
    ]
    graphics [
      x 400.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "mekk1_slash_4__cell__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa3"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa3"
      minerva_name "MEKK1/4"
      minerva_type "Complex"
      minerva_x 315.0
      minerva_y 528.0
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca5"
      compartment_name "cell"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa3"
      species_meta_id "s_id_csa3"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
    ]
  ]
  node [
    id 109
    zlevel -1

    cluster [
      cluster "s_id_ca3"
    ]
    graphics [
      x 500.0
      y 200.0
      w 20.0
      h 20.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "oval"
    ]
    label "ap_minus_1__nucleus__none__none"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    labelgraphics1 [
      alignment "center"
      anchor "hidden"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      position [
        localAlign 0.0
        relHor 0.0
        relVert 0.0
      ]
      text "s_id_csa9"
      type "text"
    ]
    lmme_dm [
      wasMerged "true"
    ]
    minerva [
      minerva_activity "false"
      minerva_elementId "csa9"
      minerva_name "AP-1"
      minerva_type "Complex"
      minerva_x 1020.5555555555557
      minerva_y 474.0555555555552
    ]
    sbml [
      _species_constant "false"
      boundary_condition "false"
      compartment "s_id_ca3"
      compartment_name "nucleus"
      has_only_substance_units "false"
      initial_amount 0.0
      sbmlRole "species"
      species_id "s_id_csa9"
      species_meta_id "s_id_csa9"
      species_non_rdf_annotation "XMLNode 'annotation' [childElements size=4]"
    ]
  ]
  edge [
    id 113
    source 2
    target 28
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa12"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 114
    source 28
    target 17
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa17"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 115
    source 31
    target 25
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa16"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 116
    source 1
    target 34
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR1"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa75"
    ]
  ]
  edge [
    id 117
    source 26
    target 39
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa70"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 118
    source 39
    target 20
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa34"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 119
    source 3
    target 43
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa53"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 120
    source 43
    target 2
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa12"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 121
    source 4
    target 47
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa44"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 122
    source 47
    target 9
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa5"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 123
    source 16
    target 47
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR5"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa5"
    ]
  ]
  edge [
    id 124
    source 21
    target 52
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa71"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 125
    source 52
    target 12
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa33"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 126
    source 16
    target 56
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR7"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa5"
    ]
  ]
  edge [
    id 127
    source 23
    target 60
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR8"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa4"
    ]
  ]
  edge [
    id 128
    source 9
    target 60
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR9"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa5"
    ]
  ]
  edge [
    id 129
    source 5
    target 60
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR10"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa76"
    ]
  ]
  edge [
    id 130
    source 18
    target 60
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR11"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa77"
    ]
  ]
  edge [
    id 131
    source 7
    target 67
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR12"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_sa78"
    ]
  ]
  edge [
    id 132
    source 27
    target 71
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa69"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 133
    source 71
    target 23
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa4"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 134
    source 16
    target 71
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR14"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa5"
    ]
  ]
  edge [
    id 135
    source 20
    target 76
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa34"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 136
    source 76
    target 10
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa36"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 137
    source 16
    target 79
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR15"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa5"
    ]
  ]
  edge [
    id 138
    source 2
    target 83
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_sa12"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 139
    source 83
    target 19
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_sa18"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 140
    source 86
    target 60
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa12"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 141
    source 86
    target 67
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa12"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 142
    source 86
    target 34
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR0"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa1"
    ]
  ]
  edge [
    id 143
    source 86
    target 39
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR2"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa1"
    ]
  ]
  edge [
    id 144
    source 86
    target 43
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR3"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa1"
    ]
  ]
  edge [
    id 145
    source 86
    target 52
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR6"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa1"
    ]
  ]
  edge [
    id 148
    source 97
    target 47
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR4"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa2"
    ]
  ]
  edge [
    id 149
    source 56
    target 97
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa2"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 151
    source 103
    target 71
    graphics [
      fill "#808080"
      outline "#808080"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    sbml [
      modifier_meta_id "MSR13"
      modifier_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "modifier"
      species "s_id_csa3"
    ]
  ]
  edge [
    id 152
    source 79
    target 103
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      product_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "product"
      species "s_id_csa3"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 154
    source 109
    target 34
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa9"
      stoichiometry "1.0"
    ]
  ]
  edge [
    id 156
    source 109
    target 31
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "none"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
    label "1.0"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
    sbml [
      reactant_non_rdf_annotation "XMLNode 'annotation' [childElements size=3]"
      sbmlRole "reactant"
      species "s_id_csa4"
      stoichiometry "1.0"
    ]
  ]
]
