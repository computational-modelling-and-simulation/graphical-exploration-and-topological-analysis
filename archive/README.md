# Graphical exploration and topological analysis of the COVID-19 disease map

## Contributors:
Falk Schreiber, <falk.schreiber@uni-konstanz.de>\
Tobias Czauderna, <tobias.czauderna@monash.edu>\
Karsten Klein, <karsten.klein@uni-konstanz.de>\
Michael Aichem, <michael.aichem@uni-konstanz.de>\
Felicia Burtscher, <felicia.burtscher@uni.lu>

## Scope:
In this subproject of the [COVID-19 disease map project](https://covid.pages.uni.lu/), we performed topological analysis on the diagrams of the COVID-19 DM repository using the tool [Vanted](https://bmcsystbiol.biomedcentral.com/articles/10.1186/1752-0509-6-139) / [SBGN-ED](https://doi.org/10.1093/bioinformatics/btq407), which supports the import and export of several standard formats (such as SBML, SBGN-ML, and GML).
Our FAIRDOMhub entry can be found [here](https://fairdomhub.org/investigations/477).

## Methodology:
The collection of files within this repository summarize the steps we followed in our pipeline:

![Pipeline](paper-figs/pipeline-overview.png "Conceptual view of the pipeline")

The following pipeline can be run again as soon as some of the available pathway diagrams change or a new pathway diagram is added:
1. The individual pathway networks are merged into an aggregated network. Necessary data is queried using the Minerva API.
2. For all networks (individual pathway networks and aggregated network), loops and parallel edges are removed.
3. The directed and undirected versions of all networks are exported as *.gml files (and also made available).
4. A centrality analysis is performed and the results are stored in data tables (and also made available).
5. The aggregated network and the centrality values are fed back into LMME-DM to explore hierarchical relationships and centralities.

### Scripts
#### scripts/get_model_data.py

`scripts/get_model_data.py` downloads all models and data for the [COVID-19 Disease Map project from MINERVA](https://covid19map.elixir-luxembourg.org/minerva/).

For each model three files are stored: 'model'.xml (CellDesigner xml), 'model'_elements.json (model element data), 'model'_reactions.json (model reaction data).

These files can be used in Vanted, the xml files can be opened as SBML files and the json files can be used with the Vanted scripts.

`scripts/requirements.txt` lists the requirements for `scripts/get_model_data.py`.

#### scripts/C19-DMs_append_rename_merge.bsh

`C19-DMs_append_rename_merge.bsh` is a beanshell script that can be called in Vanted from the menu "Scripts".

It performs three actions on each COVID-19 Disease Maps models which is opened in Vanted.
1. Adding additional attributes from the json files downloaded with get_model_data.py
2. Renaming all nodes following this pattern: name__compartment__state__cardinality
3. Merging nodes within a model which have the same name after renaming

#### scripts/C19-DMs_remove_loops_parallel_edges_save.bsh

`C19-DMs_remove_loops_parallel_edges_save.bsh` is a beanshell script that can be called in Vanted from the menu "Scripts".

It performs three actions on each COVID-19 Disease Maps models which is opened in Vanted.
1. Removes loops and parallel edges
2. Saves both a directed and an undirected version of the resulting graphs in the specified folders

### Centrality Analysis Results

### gml Graph Files

`gml_graph_files` contains the two subdirectories `directed` and `undirected`, which each contain 23 graph `.gml` files (22 pathway networks and 1 aggregated network).

### Graphical Exploration

We also provide a customised version of the Vanted Add-on *LMME* (called *LMME-DM*). It can be downloaded from [GitHub](https://github.com/LSI-UniKonstanz/lmme/tree/disease-map-adjustments). Installation guide as well as user guides can be found in the README on GitHub as well as on the [LMME webpage](https://www.cls.uni-konstanz.de/software/lmme).

#### Getting Started
1. Load the file `directed/aggregated_model_dir.gml` into Vanted
2. Click *Initialise Exploration*
3. Click *Show Overview Graph*
4. Click *Node Highlighting*, paste the content of `centrality_analysis_results/top100_aggrank_agggraph.txt` and click on *Size in Overview*
5. Select nodes of interest and click on *Show Selected Subsystems*

You now see the pathway crosstalks on the left (where the node sizes show the maximum centrality contained in this pathway) and the detailed interactions of the selected pathways/subsystems on the right.

