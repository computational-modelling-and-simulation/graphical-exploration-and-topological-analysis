"""
Copyright 2021 Monash University
Author: Tobias Czauderna
Date: 23/04/2021

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

"""
This script downloads all models for the COVID-19 Disease Map project from MINERVA.
For each model three files are stored: model.xml (CellDesigner xml), model_elements.json (model element data), model_reactions.json (model reaction data).
These files can be used in Vanted, the xml files can be opened as SBML files and the json files can be used with the Vanted scripts.

Usage: python get_model_data.py path

path -- path where files are stored

Requirements:
requests >= 2.25.1  https://pypi.org/project/requests/
"""
# MINERVA Rest API
# documentation https://minerva.pages.uni.lu/doc/api/15.1/index/
# code https://git-r3lab.uni.lu/minerva/core/-/tree/master

import json
import requests
import os
import sys
import xml.etree.ElementTree as ET
from zipfile import ZipFile
from io import BytesIO

URL = "https://covid19map.elixir-luxembourg.org/minerva/api/configuration/"
BASE_URL = "https://covid19map.elixir-luxembourg.org/minerva/api/projects/"

# default: set this to True if xml files should be get from Minerva
# legacy: set this to False if xml files should be read from local folder
GET_MODELS_FROM_MINERVA = True

# namespaces for xml parsing
NAMESPACES = {"default": "http://www.sbml.org/sbml/level2/version4", "celldesigner": "http://www.sbml.org/2001/ns/celldesigner"}


def main():

    # get and check path
    # path where files are stored
    # legacy: path where CellDesigner xml files are read from (see above)
    if len(sys.argv) != 2:
        print("\nUsage: python get_model_data.py path\n\npath -- path where files are stored")
        sys.exit(2)
    path = sys.argv[1]
    if not os.path.isabs(path):
        path = os.path.abspath(path)
    if not os.path.isdir(path):
        print(path, "is not a valid path")
        sys.exit(2)

    # get configuration from Minerva
    response = make_request(URL)
    if response is None or response.status_code != 200:
        sys.exit(1)
    configuration = response.json()
    version = configuration["version"]
    print("=== Minerva", version)
    # workarounds are implemented based on Minerva 15.1.3, a new version contain have some fixes
    if version > "15.1.3":
        print("New Minerva version, check if workarounds can be removed")

    # from configuration get projec ID and base url
    project_id = None
    for option in configuration["options"]:
        if "type" in option and option["type"] == "DEFAULT_MAP" and "value" in option:
            project_id = option["value"]
            break
    if project_id is None:
        sys.exit(1)
    base_url = BASE_URL + project_id + "/"

    # get models (model information) from Minerva
    response = make_request(base_url + "models/")
    if response is None or response.status_code != 200:
        sys.exit(1)
    models = response.json()

    print("=== Writing files to", path)
    print("=== Models")
    # for each model get/read CellDesigner xml, elements, and reactions
    k = 1
    for model in models:
        if "name" in model and "idObject" in model:
            print("[" + str(k) + "/" + str(len(models)) + "]", model["name"])
            filename = model["name"].replace(" ", "_")
            
            # default: get CellDesigner xml from Minerva
            if GET_MODELS_FROM_MINERVA:
                print("\tGetting model from Minerva")
                tree = get_model_xml(base_url, model["idObject"], path, filename)
            # legacy: read (local) CellDesigner xml from repository
            else:
                print("\tReading local model file")
                tree = read_model_xml(model["name"], path)
            if tree is None:
                continue
            
            print("\tParsing CellDesigner xml")
            data = parse_xml(tree, NAMESPACES)

            print("\tGetting element data", end=":")
            get_elements(base_url, model["idObject"], path, filename, data)

            print("\tGetting reaction data", end=":")
            get_reactions(base_url, model["idObject"], path, filename)
        k += 1


def get_model_xml(base_url, model_id, path, filename):
    """
    Get model CellDesigner xml from Minerva
    
    :param base_url: project base url
    :param model_id: model id
    :param path: path where the xml file is saved (and zip file if applicable)
    :param filename: xml filename
    :return: the xml tree
    """

    response = make_request(base_url + "models/" + str(model_id) + ":downloadModel?handlerClass=lcsb.mapviewer.converter.model.celldesigner.CellDesignerXmlParser")
    if response is None or response.status_code != 200:
        print("\tCannot get xml file for " + str(model_id))
        return None

    # Minerva returns a zip file if xml larger than 1MB(?)
    if response.headers["Content-Type"] == "application/octet-stream":
        # save xml file
        with open(os.path.join(path, filename) + ".xml", "w", encoding="utf-8") as xml_file:
            xml_file.write(response.text)
    elif response.headers["Content-Type"] == "application/zip":
        # save zip file
        with open(os.path.join(path, filename) + ".zip", "wb") as zip_file:
            zip_file.write(response.content)
        # extract xml from zip file and save
        zip_file = ZipFile(BytesIO(response.content))
        name_list = zip_file.namelist()
        zip_file.extract(name_list[0], path)
        if os.path.exists(os.path.join(path, filename) + ".xml"):
            os.remove(os.path.join(path, filename) + ".xml")
        os.rename(os.path.join(path, name_list[0]), os.path.join(path, filename + ".xml"))

    # open xml file
    try:
        tree = ET.parse(os.path.join(path, filename + ".xml"))
    except ET.ParseError as parseError:
        print("\t" + repr(parseError))
            
    return tree


def read_model_xml(model_name, path):
    """
    legacy code, by default the CellDesigner xml files should be retrieved from Minerva
    Read a local CellDesigner xml file retrieved from the GitLab repository
    
    :param path: path to the xml
    :param model_name: model name
    :return: the xml tree
    """

    # mapping model name from Minerva to CellDesigner xml from repository
    model_xml = {
        "Apoptosis pathway": "Apoptosis_stable.xml",
        "Coagulation pathway": "Coagulation-pathway_stable.xml",
        "Electron Transport Chain disruption": "ETC_stable.xml",
        "Endoplasmatic Reticulum stress": "ER_Stress_stable.xml",
        "E protein interactions": "E_protein_stable.xml",
        "HMOX1 pathway": "HMOX1_Pathway_stable.xml",
        "Interferon 1 pathway": "Interferon1_stable.xml",
        "Interferon lambda pathway": "IFN-lambda_stable.xml",
        "JNK pathway": "JNK_pathway_stable.xml",
        "Kynurenine synthesis pathway": "Kynurenine_pathway_stable.xml",
        "NLRP3 inflammasome activation": "NLRP3_Activation_stable.xml",
        "Nsp14 and metabolism": "Nsp14_stable.xml",
        "Nsp4 and Nsp6 protein interactions": "Nsp4_Nsp6_stable.xml",
        "Nsp9 protein interactions": "Nsp9_protein_stable.xml",
        "Orf10 Cul2 pathway": "Orf10_Cul2_pathway_stable.xml",
        "Orf3a protein interactions": "Orf3a_stable.xml",
        "PAMP signalling": "PAMP_signaling_stable.xml",
        "Pyrimidine deprivation": "Pyrimidine_deprivation_stable.xml",
        "Renin-angiotensin pathway": "Renin_angiotensin_stable.xml",
        "SARS-CoV-2 RTC and transcription": "RTC-and-transcription_stable.xml",
        "TGFbeta signalling": "TGFB_pathway_stable.xml",
        "Virus replication cycle": "Virus_replication_cycle_stable.xml"
    }

    if not model_name in model_xml:
        print("\tCannot read xml file for " + model_name)
        return None
    # open xml file
    try:
        tree = ET.parse(os.path.join(path, model_xml[model_name]))
    except ET.ParseError as parseError:
        print("\t" + repr(parseError))

    return tree


def parse_xml(tree, namespaces):
    """
    Parse CellDesigner xml to get additional data about model
    
    :param tree: xml tree
    :param namespaces: namespaces used in CellDesigner xml (see at top of script)
    :return: additional data
    """

    root = tree.getroot()
    data = {}
    species_to_alias_mapping = {}

    # get 1:1 mapping compartment alias id -> compartment id from compartment aliases
    for compartment_alias in root.findall("default:model/default:annotation/celldesigner:extension/celldesigner:listOfCompartmentAliases/celldesigner:compartmentAlias", namespaces):
        # print("\t", compartment_alias.get("id"), compartment_alias.get("compartment"))
        data[compartment_alias.get("id")] = { "mapping" : compartment_alias.get("compartment") }

    # get 1:1 mapping complex species alias id -> complex species id from complex species aliases
    # get 1:n mapping complex species id -> complex species alias id
    for compex_species_alias in root.findall("default:model/default:annotation/celldesigner:extension/celldesigner:listOfComplexSpeciesAliases/celldesigner:complexSpeciesAlias", namespaces):
        # print("\t", compex_species_alias.get("id"), compex_species_alias.get("species"))
        data[compex_species_alias.get("id")] = { "mapping" : compex_species_alias.get("species") }
        if compex_species_alias.get("species") in species_to_alias_mapping:
            species_to_alias_mapping[compex_species_alias.get("species")].append(compex_species_alias.get("id"))
        else:
            species_to_alias_mapping[compex_species_alias.get("species")] = [compex_species_alias.get("id")]

    # get 1:1 mapping species aliases id -> species id from species aliases
    # get 1:n mapping species id -> species aliases id
    for species_alias in root.findall("default:model/default:annotation/celldesigner:extension/celldesigner:listOfSpeciesAliases/celldesigner:speciesAlias", namespaces):
        # print("\t", species_alias.get("id"), species_alias.get("species"))
        data[species_alias.get("id")] = { "mapping" : species_alias.get("species") }
        if species_alias.get("species") in species_to_alias_mapping:
            species_to_alias_mapping[species_alias.get("species")].append(species_alias.get("id"))
        else:
            species_to_alias_mapping[species_alias.get("species")] = [species_alias.get("id")]
        
    # get homodimer information from included species, save for each species alias using species_to_alias_mapping
    # getting the homodimer from CellDesigner files is a workaround until it is fixed on Minerva, probably in version 16
    # see https://git-r3lab.uni.lu/minerva/core/-/issues/1468
    # see https://git-r3lab.uni.lu/minerva/core/-/commit/aec1ee5ecac9063f7daa04ee9b8458cc9cad1137
    for species in root.findall("default:model/default:annotation/celldesigner:extension/celldesigner:listOfIncludedSpecies/celldesigner:species", namespaces):
        homodimer = species.find("celldesigner:annotation/celldesigner:speciesIdentity/celldesigner:state/celldesigner:homodimer", namespaces)
        if not homodimer is None:
            # print("\t", species.get("id"), homodimer.text)
            for species_alias_id in species_to_alias_mapping[species.get("id")]:
                data[species_alias_id]["homodimer"] = homodimer.text

    # get structuralStates information from species, save for each species alias using species_to_alias_mapping
    # getting the structuralStates from CellDesigner xml is a workaround just for COMPLEX species until it is provided via Minerva API
    # see https://git-r3lab.uni.lu/minerva/core/-/issues/1476
    # get homodimer information from species, save for each species alias using species_to_alias_mapping
    # getting the homodimer from CellDesigner xml is a workaround until it is provided via Minerva API, probably in version 16
    # see https://git-r3lab.uni.lu/minerva/core/-/issues/1468
    # see https://git-r3lab.uni.lu/minerva/core/-/commit/aec1ee5ecac9063f7daa04ee9b8458cc9cad1137
    for species in root.findall("default:model/default:listOfSpecies/default:species", namespaces):
        # structuralStates are stored as a list in CellDesigner xml but it looks like we usually have only one
        structuralStates = []
        for structuralState in species.findall("default:annotation/celldesigner:extension/celldesigner:speciesIdentity/celldesigner:state/celldesigner:listOfStructuralStates/celldesigner:structuralState", namespaces):
            state = structuralState.get("structuralState")
            if not state is None:
                # print("\t", species.get("id"), state)
                structuralStates.append(state)
        if len(structuralStates) > 0:
            for species_alias_id in species_to_alias_mapping[species.get("id")]:
                data[species_alias_id]["structuralStates"] = structuralStates
        homodimer = species.find("default:annotation/celldesigner:extension/celldesigner:speciesIdentity/celldesigner:state/celldesigner:homodimer", namespaces)
        if not homodimer is None:
            # print("\t", species.get("id"), homodimer.text)
            for species_alias_id in species_to_alias_mapping[species.get("id")]:
                data[species_alias_id]["homodimer"] = homodimer.text

    return data


def get_elements(base_url, model_id, path, filename, data):
    """
    Get elements for model from Minerva and save as json file
    
    :param base_url: project base url
    :param model_id: model id
    :param path: path where the json file is saved
    :param filename: json filename
    :param data: additional data for the model retrieved from the according CellDesigner xml file
    """


    response = make_request(base_url + "models/" + str(model_id) + "/bioEntities/elements/")
    # get particular parameters, replace (*) with comma separated parameters, see also Minerva API documentation
    # response = make_request(base_url + "models/" + str(model_id) + "/bioEntities/elements/?columns=*")
    if response is None or response.status_code != 200:
        print("\tCannot get elements")
        return
    
    elements = response.json()
    print("\t" + str(len(elements)) + " elements")
    
    # add additional data retrieved from the CellDesigner xml
    # sbml compartment ID, sbml species ID, structuralStates, homodimer
    if not data is None:
        for element in elements:
            if "elementId" in element and "type" in element and element["elementId"] in data:
                if element["type"] == "Compartment":
                    element["sbmlCompartmentId"] = data[element["elementId"]]["mapping"]
                else:
                    element["sbmlSpeciesId"] = data[element["elementId"]]["mapping"]
                    # setting the structuralStates from CellDesigner xml is a workaround until it is provided via Minerva API
                    if "structuralStates" in data[element["elementId"]]:
                        element["celldesignerStructuralStates"] = data[element["elementId"]]["structuralStates"]
                    # setting the homodimer from CellDesigner xml is a workaround until it is provided via Minerva API, probably in version 16
                    if "homodimer" in data[element["elementId"]]:
                        element["celldesignerHomodimer"] = data[element["elementId"]]["homodimer"]
    
    with open(os.path.join(path, filename) + "_elements.json", "w") as jsonfile:
        json.dump(elements, jsonfile, indent=4, separators=(",", ": "))


def get_reactions(base_url, model_id, path, filename):
    """
    Get reactions for model from Minerva and save as json file
    
    :param base_url: project base url
    :param model_id: model id
    :param path: path where the json file is saved
    :param filename: json filename
    """

    response = make_request(base_url + "models/" + str(model_id) + "/bioEntities/reactions/")
    # get particular parameters, replace (*) with comma separated parameters, see also Minerva API documentation
    # response = make_request(base_url + "models/" + str(model_id) + "/bioEntities/reactions/?columns=*")
    if response is None or response.status_code != 200:
        print("\tCannot get reactions")
        return
    
    reactions = response.json()
    print("\t" + str(len(reactions)) + " reactions")
    with open(os.path.join(path, filename) + "_reactions.json", "w") as jsonfile:
        json.dump(reactions, jsonfile, indent=4, separators=(",", ": "))


def make_request(url):
    """
    Make a request to Minerva using requests.get and catch all possible errors
    
    :param url: url for request
    :return: response
    """

    try:
        response = requests.get(url)
        response.raise_for_status()
    except requests.exceptions.HTTPError as http_error:
        print ("Http error", repr(http_error))
        return None
    except requests.exceptions.ConnectionError as connection_error:
        print ("Connection error", repr(connection_error))
        return None
    except requests.exceptions.Timeout as timeout_error:
        print ("Timeout error", repr(timeout_error))
        return None
    except requests.exceptions.RequestException as error:
        print ("Unknown error", repr(error))
        return None

    return response


if __name__ == "__main__":
    main()
