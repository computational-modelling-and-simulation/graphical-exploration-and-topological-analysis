"""
Copyright 2022 University of Applied Sciences Mittweida
Author: Tobias Czauderna
Date: 30/06/2022

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

"""
This script creates gml files for Vanted from the bipartite representation of the COVID-19 Disease Maps.
It performs some checks on the data and prints information about these checks.
Two sets of gml files are stored:
- gml files with directed and parallel edges
- gml files with undirected and not parallel edges for the network analysis

Usage: python create_gmls.py path/to/filename.tsv

Requirements:
pandas >= 1.4.2  https://pypi.org/project/pandas/
"""

import os
import pandas as pd
from pathlib import Path
import sys

def add_node(id, label="", diagram="", map_id="", org_id="", name="", uniprot="", hgnc="", node_type="", node_subtype="", full_annotation="", annotation="", count=""):
    """
    Add node
    :param id: Vanted id for the node
    :param label: node label
    :param diagram: diagram name
    :param map_id: map id
    :param org_id: original id
    :param name: name
    :param uniprot: uniprot ids
    :param hgnc: hgnc ids
    :param node_type: node type
    :param node_subtype: node subtype
    :param full_annotation: full annotation
    :param annotation: annotation
    :param count: 
    :return: gml string for the node
    """

    return '  node [\n    id ' + id + ' \n    zlevel -1\n    graphics [\n      x 50.0\n      y 50.0\n      w 25.0\n      h 25.0\n      fill "#FFFFFF"\n      outline "#000000"\n      frameThickness 2.0\n      gradient 0.0\n      opacity 1.0\n      rounding 5.0\n      type "rectangle"\n    ]\n    label "' + label + '"\n    labelgraphics [\n      alignment "center"\n      anchor "c"\n      color "#000000"\n      fontName "Arial"\n      fontSize 12\n      fontStyle "plain"\n      labelOffset [\n        x 0.0\n        y 0.0\n      ]\n      type "text"\n    ]\n    cd19dm [\n      map_id "' + map_id + '"\n      diagram "' + diagram + '"\n      org_id "' + org_id + '"\n      name "' + name + '"\n      uniprot "' + uniprot + '"\n      hgnc "' + hgnc + '"\n      node_type "' + node_type + '"\n      node_subtype "' + node_subtype + '"\n      full_annotation "' + full_annotation + '"\n      annotation "' + annotation + '"\n      count ' + count + '\n    ]\n  ]\n'

def add_edge(id, source_node_id, target_node_id, diagram="", source_id="", target_id="", edge_type=""):
    """
    Add edge
    :param id: Vanted id for the edge
    :param source_node_id: Vanted source node id
    :param target_node_id: Vanted target node id
    :param diagram: diagram name
    :param source_id: source id
    :param target_id: target id
    :param edge_type: edge type
    :return: gml string for the edge
    """

    return '  edge [\n    id ' + id + '\n    source ' + source_node_id + '\n    target ' + target_node_id + '\n    graphics [\n      fill "#000000"\n      outline "#000000"\n      arrow "last"\n      frameThickness 1.0\n      gradient 0.0\n      opacity 1.0\n      rounding 5.0\n      thickness 1.0\n    ]\n    cd19dm [\n      diagram "' + diagram + '"\n      source_id "' + source_id + '"\n      target_id "' + target_id + '"\n      edge_type "' + edge_type + '"\n    ]\n  ]\n'

def main():
    """
    Main function
    """

    if len(sys.argv) != 2:
        print("\nUsage: python create_gmls.py path/to/filename.tsv")
        sys.exit(2)
    FILENAME = sys.argv[1]
    if not os.path.isabs(FILENAME):
        FILENAME = os.path.abspath(FILENAME)
    if not os.path.isfile(FILENAME):
        print(FILENAME, "is not a valid file")
        sys.exit(2)

    path = Path(FILENAME).parent
    folder = Path(FILENAME).stem
    
    # create folders
    parallel_edges = "parallel-edges_edges-directed"
    no_parallel_edges = "no-parallel-edges_edges-undirected_sub-components"
    try:
        Path(path.joinpath(folder)).mkdir()
    except FileExistsError as exception:
        print("Folder", path.joinpath(folder), "exists already")
        # print(exception)
    try:
        Path(path.joinpath(folder, parallel_edges)).mkdir()
    except FileExistsError as exception:
        print("Folder", path.joinpath(folder, parallel_edges), "exists already")
        # print(exception)
    try:
        Path(path.joinpath(folder, no_parallel_edges)).mkdir()
    except FileExistsError as exception:
        print("Folder", path.joinpath(folder, no_parallel_edges), "exists already")
        # print(exception)

    # na_filter=False: do not parse "NA" as float NaN
    df = pd.read_csv(FILENAME, sep="\t", encoding="utf-8", na_filter=False)

    # every row in the tsv describes an edge
    # col 0          col 1     col 2       col 3     col 4       col 5       col 6      col 7            col 8     col 9       col 10    col 11      col 12      col 13     col 14              col 15        col 16
    # source_diagram source_id source_name source_up source_hgnc source_type source_all interaction_type target_id target_name target_up target_hgnc target_type target_all reaction_annotation org_source_id org_target_id

    # column ids
    diagram_col = 0
    src_map_id_col = 1
    src_name_col = 2
    src_uniprot_col = 3
    src_hgnc_col = 4
    src_node_type_col = 5
    src_full_annotation_col = 6
    edge_type_col = 7
    tgt_map_id_col = 8
    tgt_name_col = 9
    tgt_uniprot_col = 10
    tgt_hgnc_col = 11
    tgt_node_type_col = 12
    tgt_full_annotation_col = 13
    annotation_col = 14
    src_org_id_col = 15
    tgt_org_id_col = 16

    # get node types
    print("\nGetting node types ...")
    # assumptions
    # when the interaction type is "PRODUCTION" then the source node is a reaction
    # when the interaction type is not "PRODUCTION" then the target node is a reaction
    # expected result
    # species = ['COMPLEX', 'SIMPLE_MOLECULE', 'PROTEIN', 'RNA', 'DRUG', 'UNKNOWN', 'GENE', 'PHENOTYPE', 'DEGRADED', 'ION']
    # reactions = ['STATE_TRANSITION', 'UNKNOWN_TRANSITION', 'REDUCED_PHYSICAL_STIMULATION', 'NEGATIVE_INFLUENCE', 'UNKNOWN_POSITIVE_INFLUENCE', 'POSITIVE_INFLUENCE', 'UNKNOWN_NEGATIVE_INFLUENCE', 'TRIGGER', 'HETERODIMER_ASSOCIATION', 'KNOWN_TRANSITION_OMITTED', 'TRANSPORT', 'TRANSCRIPTION', 'MODULATION', 'PHYSICAL_STIMULATION', 'TRANSLATION', 'DISSOCIATION', 'TRUNCATION']
    species = []
    reactions = []
    for k in range(df.shape[0]):
        if df.iloc[k, edge_type_col] == "PRODUCTION":
            if not df.iloc[k, src_node_type_col] in reactions:
                reactions.append(df.iloc[k, src_node_type_col])
            if not df.iloc[k, tgt_node_type_col] in species:
                species.append(df.iloc[k, tgt_node_type_col])
        else:
            if not df.iloc[k, src_node_type_col] in species:
                species.append(df.iloc[k, src_node_type_col])
            if not df.iloc[k, tgt_node_type_col] in reactions:
                reactions.append(df.iloc[k, tgt_node_type_col])
    assert set(species) == set(['COMPLEX', 'SIMPLE_MOLECULE', 'PROTEIN', 'RNA', 'DRUG', 'UNKNOWN', 'GENE', 'PHENOTYPE', 'DEGRADED', 'ION'])
    assert set(reactions) == set(['STATE_TRANSITION', 'UNKNOWN_TRANSITION', 'REDUCED_PHYSICAL_STIMULATION', 'NEGATIVE_INFLUENCE', 'UNKNOWN_POSITIVE_INFLUENCE', 'POSITIVE_INFLUENCE', 'UNKNOWN_NEGATIVE_INFLUENCE', 'TRIGGER', 'HETERODIMER_ASSOCIATION', 'KNOWN_TRANSITION_OMITTED', 'TRANSPORT', 'TRANSCRIPTION', 'MODULATION', 'PHYSICAL_STIMULATION', 'TRANSLATION', 'DISSOCIATION', 'TRUNCATION'])

    # check reactions for crosslinks
    print("\nChecking reactions for crosslinks ...")
    # reactions with UniProt id as id
    l = 0
    for k in range(df.shape[0]):
        if df.iloc[k, src_node_type_col] in reactions and df.iloc[k, src_uniprot_col] != "NA" and df.iloc[k, src_map_id_col] == df.iloc[k, src_uniprot_col]:
            l += 1
        if df.iloc[k, tgt_node_type_col] in reactions and df.iloc[k, tgt_uniprot_col] != "NA" and df.iloc[k, tgt_map_id_col] == df.iloc[k, tgt_uniprot_col]:
            l += 1
    if l > 0:
        print(l, "reactions with UniProt id as id")
    # reactions with name as id
    l = 0
    for k in range(df.shape[0]):
        if df.iloc[k, src_node_type_col] in reactions and df.iloc[k, src_name_col] != "NA" and df.iloc[k, src_map_id_col] == df.iloc[k, src_name_col]:
            l += 1
        if df.iloc[k, tgt_node_type_col] in reactions and df.iloc[k, tgt_name_col] != "NA" and df.iloc[k, tgt_map_id_col] == df.iloc[k, tgt_name_col]:
            l += 1
    if l > 0:
        print(l, "reactions with name as id")

    # check whether same reaction appears several times in the same map
    print("\nChecking whether reactions appear several times in the same map ...")
    known_reactions = {}
    l = 0
    for k in range(df.shape[0]):
        if df.iloc[k, src_node_type_col] in reactions:
            if not df.iloc[k, src_map_id_col] in known_reactions:
                known_reactions[df.iloc[k, src_map_id_col]] = df.iloc[k, diagram_col] + "###" + df.iloc[k, src_org_id_col]
            elif known_reactions[df.iloc[k, src_map_id_col]].split("###")[0] == df.iloc[k, diagram_col] and known_reactions[df.iloc[k, src_map_id_col]].split("###")[1] != df.iloc[k, src_org_id_col]:
                l += 1
        if df.iloc[k, tgt_node_type_col] in reactions:
            if not df.iloc[k, tgt_map_id_col] in known_reactions:
                known_reactions[df.iloc[k, tgt_map_id_col]] = df.iloc[k, diagram_col] + "###" + df.iloc[k, tgt_org_id_col]
            elif known_reactions[df.iloc[k, tgt_map_id_col]].split("###")[0] == df.iloc[k, diagram_col] and known_reactions[df.iloc[k, tgt_map_id_col]].split("###")[1] != df.iloc[k, tgt_org_id_col]:
                l += 1
    if l > 0:
        print(l, "reactions appear several times in the same map")

    # check whether same reaction appears in several maps
    print("\nChecking whether reactions appear in several maps ...")
    known_reactions = {}
    l = 0
    for k in range(df.shape[0]):
        if df.iloc[k, src_node_type_col] in reactions:
            if not df.iloc[k, src_map_id_col] in known_reactions:
                known_reactions[df.iloc[k, src_map_id_col]] = df.iloc[k, diagram_col]
            elif known_reactions[df.iloc[k, src_map_id_col]] != df.iloc[k, diagram_col]:
                l += 1
        if df.iloc[k, tgt_node_type_col] in reactions:
            if not df.iloc[k, tgt_map_id_col] in known_reactions:
                known_reactions[df.iloc[k, tgt_map_id_col]] = df.iloc[k, diagram_col]
            elif known_reactions[df.iloc[k, tgt_map_id_col]] != df.iloc[k, diagram_col]:
                l += 1
    if l > 0:
        print(l, "reactions appear in several maps")

    # collecting all node information
    print("\nCollecting all node information ...")
    # source_diagram -> diagram            df.iloc[k, diagram_col]
    # source_id -> map_id                  df.iloc[k, src_map_id_col]
    # source_name -> name                  df.iloc[k, src_name_col]
    # source_up -> uniprot                 df.iloc[k, src_uniprot_col]
    # source_hgnc -> hgnc                  df.iloc[k, src_hgnc_col]
    # source_type -> node_type             df.iloc[k, src_node_type_col]
    # interaction_type -> edge_type        df.iloc[k, edge_type_col]
    # target_id -> map_id                  df.iloc[k, tgt_map_id_col]
    # target_name -> name                  df.iloc[k, tgt_name_col]
    # target_up -> uniprot                 df.iloc[k, tgt_uniprot_col]
    # target_hgnc -> hgnc                  df.iloc[k, tgt_hgnc_col]
    # target_type -> node_type             df.iloc[k, tgt_node_type_col]
    # reaction_annotation -> annotation    df.iloc[k, annotation_col]
    # org_source_id -> org_id              df.iloc[k, src_org_id_col]
    # org_target_id -> org_id              df.iloc[k, tgt_org_id_col]
    nodes = {}
    species = {"id": [], "names": []}  # collect all species names for a species id
    filtered_species = {"id": [], "names": []}  # collect all species names for a species id for filtered species
    for k in range(df.shape[0]):
        # source node
        # reaction
        if df.iloc[k, src_node_type_col] in reactions:
            node_type = "reaction"
            annotation = df.iloc[k, annotation_col]
        # species
        else:
            node_type = "species"
            annotation = ""  # only reactions are annotated
            # collect species names
            if not df.iloc[k, src_map_id_col] in species["id"]:
                species["id"].append(df.iloc[k, src_map_id_col])
                species["names"].append([df.iloc[k, src_name_col]])
            else:
                idx = species["id"].index(df.iloc[k, src_map_id_col])
                if not df.iloc[k, src_name_col] in species["names"][idx]:
                    species["names"][idx].append(df.iloc[k, src_name_col])
            # collect species names for filtered species
            if "chebi" in df.iloc[k, src_full_annotation_col]:
                if not df.iloc[k, src_map_id_col] in filtered_species["id"]:
                    filtered_species["id"].append(df.iloc[k, src_map_id_col])
                    filtered_species["names"].append([df.iloc[k, src_name_col]])
                else:
                    idx = filtered_species["id"].index(df.iloc[k, src_map_id_col])
                    if not df.iloc[k, src_name_col] in filtered_species["names"][idx]:
                        filtered_species["names"][idx].append(df.iloc[k, src_name_col])
        # create new entry in nodes when src_map_id_col is unknown
        if not df.iloc[k, src_map_id_col] in nodes:
            nodes[df.iloc[k, src_map_id_col]] = {
                "count": 1,
                "diagram": [df.iloc[k, diagram_col]],
                "map_id": [df.iloc[k, src_map_id_col]],
                "name": [df.iloc[k, src_name_col]],
                "uniprot": [df.iloc[k, src_uniprot_col]],
                "hgnc": [df.iloc[k, src_hgnc_col]],
                "node_type": [node_type],
                "node_subtype": [df.iloc[k, src_node_type_col]],
                "full_annotation": [df.iloc[k, src_full_annotation_col]],
                "annotation": [annotation],
                "org_id": [df.iloc[k, src_org_id_col]]
            }
        # if the original id is already in org_id then ignore (do not add information for the same node twice)
        # else append to the entry in nodes
        elif not df.iloc[k, src_org_id_col] in nodes[df.iloc[k, src_map_id_col]]["org_id"]:
            nodes[df.iloc[k, src_map_id_col]]["count"] += 1
            if not df.iloc[k, diagram_col] in nodes[df.iloc[k, src_map_id_col]]["diagram"]:
                nodes[df.iloc[k, src_map_id_col]]["diagram"].append(df.iloc[k, diagram_col])
            if not df.iloc[k, src_map_id_col] in nodes[df.iloc[k, src_map_id_col]]["map_id"]:
                nodes[df.iloc[k, src_map_id_col]]["map_id"].append(df.iloc[k, src_map_id_col])
            if not df.iloc[k, src_name_col] in nodes[df.iloc[k, src_map_id_col]]["name"]:
                nodes[df.iloc[k, src_map_id_col]]["name"].append(df.iloc[k, src_name_col])
            if not df.iloc[k, src_uniprot_col] in nodes[df.iloc[k, src_map_id_col]]["uniprot"]:
                nodes[df.iloc[k, src_map_id_col]]["uniprot"].append(df.iloc[k, src_uniprot_col])
            if not df.iloc[k, src_hgnc_col] in nodes[df.iloc[k, src_map_id_col]]["hgnc"]:
                nodes[df.iloc[k, src_map_id_col]]["hgnc"].append(df.iloc[k, src_hgnc_col])
            if not node_type in nodes[df.iloc[k, src_map_id_col]]["node_type"]:
                nodes[df.iloc[k, src_map_id_col]]["node_type"].append(node_type)
            if not df.iloc[k, src_node_type_col] in nodes[df.iloc[k, src_map_id_col]]["node_subtype"]:
                nodes[df.iloc[k, src_map_id_col]]["node_subtype"].append(df.iloc[k, src_node_type_col])
            if not df.iloc[k, src_full_annotation_col] in nodes[df.iloc[k, src_map_id_col]]["full_annotation"]:
                nodes[df.iloc[k, src_map_id_col]]["full_annotation"].append(df.iloc[k, src_full_annotation_col])
            if not annotation in nodes[df.iloc[k, src_map_id_col]]["annotation"]:
                nodes[df.iloc[k, src_map_id_col]]["annotation"].append(annotation)
            if not df.iloc[k, src_org_id_col] in nodes[df.iloc[k, src_map_id_col]]["org_id"]:
                nodes[df.iloc[k, src_map_id_col]]["org_id"].append(df.iloc[k, src_org_id_col])
        # target node
        # reaction
        if df.iloc[k, tgt_node_type_col] in reactions:
            node_type = "reaction"
            annotation = df.iloc[k, annotation_col]
        # species
        else:
            node_type = "species"
            annotation = ""  # only reactions are annotated
            # collect species names
            if not df.iloc[k, tgt_map_id_col] in species["id"]:
                species["id"].append(df.iloc[k, tgt_map_id_col])
                species["names"].append([df.iloc[k, tgt_name_col]])
            else:
                idx = species["id"].index(df.iloc[k, tgt_map_id_col])
                if not df.iloc[k, tgt_name_col] in species["names"][idx]:
                    species["names"][idx].append(df.iloc[k, tgt_name_col])
            # collect species names for filtered species
            if "chebi" in df.iloc[k, tgt_full_annotation_col]:
                if not df.iloc[k, tgt_map_id_col] in filtered_species["id"]:
                    filtered_species["id"].append(df.iloc[k, tgt_map_id_col])
                    filtered_species["names"].append([df.iloc[k, tgt_name_col]])
                else:
                    idx = filtered_species["id"].index(df.iloc[k, tgt_map_id_col])
                    if not df.iloc[k, tgt_name_col] in filtered_species["names"][idx]:
                        filtered_species["names"][idx].append(df.iloc[k, tgt_name_col])
        # create new entry in nodes when tgt_map_id_col is unknown
        if not df.iloc[k, tgt_map_id_col] in nodes:
            nodes[df.iloc[k, tgt_map_id_col]] = {
                "count": 1,
                "diagram": [df.iloc[k, diagram_col]],
                "map_id": [df.iloc[k, tgt_map_id_col]],
                "name": [df.iloc[k, tgt_name_col]],
                "uniprot": [df.iloc[k, tgt_uniprot_col]],
                "hgnc": [df.iloc[k, tgt_hgnc_col]],
                "node_type": [node_type],
                "node_subtype": [df.iloc[k, tgt_node_type_col]],
                "full_annotation": [df.iloc[k, tgt_full_annotation_col]],
                "annotation": [annotation],
                "org_id": [df.iloc[k, tgt_org_id_col]]
            }
        # if the original id is already in org_id then ignore (do not add information for the same node twice)
        # else append to the entry in nodes
        elif not df.iloc[k, tgt_org_id_col] in nodes[df.iloc[k, tgt_map_id_col]]["org_id"]:
            nodes[df.iloc[k, tgt_map_id_col]]["count"] += 1
            if not df.iloc[k, diagram_col] in nodes[df.iloc[k, tgt_map_id_col]]["diagram"]:
                nodes[df.iloc[k, tgt_map_id_col]]["diagram"].append(df.iloc[k, diagram_col])
            if not df.iloc[k, tgt_map_id_col] in nodes[df.iloc[k, tgt_map_id_col]]["map_id"]:
                nodes[df.iloc[k, tgt_map_id_col]]["map_id"].append(df.iloc[k, tgt_map_id_col])
            if not df.iloc[k, tgt_name_col] in nodes[df.iloc[k, tgt_map_id_col]]["name"]:
                nodes[df.iloc[k, tgt_map_id_col]]["name"].append(df.iloc[k, tgt_name_col])
            if not df.iloc[k, tgt_uniprot_col] in nodes[df.iloc[k, tgt_map_id_col]]["uniprot"]:
                nodes[df.iloc[k, tgt_map_id_col]]["uniprot"].append(df.iloc[k, tgt_uniprot_col])
            if not df.iloc[k, tgt_hgnc_col] in nodes[df.iloc[k, tgt_map_id_col]]["hgnc"]:
                nodes[df.iloc[k, tgt_map_id_col]]["hgnc"].append(df.iloc[k, tgt_hgnc_col])
            if not node_type in nodes[df.iloc[k, tgt_map_id_col]]["node_type"]:
                nodes[df.iloc[k, tgt_map_id_col]]["node_type"].append(node_type)
            if not df.iloc[k, tgt_node_type_col] in nodes[df.iloc[k, tgt_map_id_col]]["node_subtype"]:
                nodes[df.iloc[k, tgt_map_id_col]]["node_subtype"].append(df.iloc[k, tgt_node_type_col])
            if not df.iloc[k, tgt_full_annotation_col] in nodes[df.iloc[k, tgt_map_id_col]]["full_annotation"]:
                nodes[df.iloc[k, tgt_map_id_col]]["full_annotation"].append(df.iloc[k, tgt_full_annotation_col])
            if not annotation in nodes[df.iloc[k, tgt_map_id_col]]["annotation"]:
                nodes[df.iloc[k, tgt_map_id_col]]["annotation"].append(annotation)
            if not df.iloc[k, tgt_org_id_col] in nodes[df.iloc[k, tgt_map_id_col]]["org_id"]:
                nodes[df.iloc[k, tgt_map_id_col]]["org_id"].append(df.iloc[k, tgt_org_id_col])

    # save species as csv
    # find maximum number of names
    max_num_names = 0    
    for k in range(len(species["id"])):
        if len(species["names"][k]) > max_num_names:
            max_num_names = len(species["names"][k])
    # create entries in dict according to maximum number of names and set to ""
    for k in range(max_num_names):
        species["name" + str(k + 1)] = [""] * len(species["id"])
    # set names for a species according to names available
    for k in range(len(species["id"])):
        for l in range(len(species["names"][k])):
            species["name" + str(l + 1)][k] = species["names"][k][l]
    del species["names"]
    # create data frame from dict and save as csv
    df_species = pd.DataFrame.from_dict(species)
    df_species.to_csv(path.joinpath(folder, "species.csv"))
    # save filtered species as csv
    # find maximum number of names
    max_num_names = 0    
    for k in range(len(filtered_species["id"])):
        if len(filtered_species["names"][k]) > max_num_names:
            max_num_names = len(filtered_species["names"][k])
    # create entries in dict according to maximum number of names and set to ""
    for k in range(max_num_names):
        filtered_species["name" + str(k + 1)] = [""] * len(filtered_species["id"])
    # set names for a species according to names available
    for k in range(len(filtered_species["id"])):
        for l in range(len(filtered_species["names"][k])):
            filtered_species["name" + str(l + 1)][k] = filtered_species["names"][k][l]
    del filtered_species["names"]
    # create data frame from dict and save as csv
    df_filtered = pd.DataFrame.from_dict(filtered_species)
    df_filtered.to_csv(path.joinpath(folder, "filtered.csv"))

    # print information about nodes
    print("\n\tNodes crosslinked by UniProt id with more than one node type and more than one name")
    for node_id, attributes in nodes.items():
        if node_id.startswith("UNIPROT") and len(attributes["node_subtype"]) > 1 and len(attributes["name"]) > 1:
            print("\n\t\t" + node_id + "\n\t\t\tcount " + str(attributes["count"]) + "\n\t\t\tdiagrams " + str(attributes["diagram"]) + "\n\t\t\tnode types " + str(attributes["node_subtype"]) + "\n\t\t\tnames " + str(attributes["name"]))
    print("\n\tNodes crosslinked by UniProt id with more than one node type")
    for node_id, attributes in nodes.items():
        if node_id.startswith("UNIPROT") and len(attributes["node_subtype"]) > 1 and len(attributes["name"]) == 1:
            print("\n\t\t" + node_id + "\n\t\t\tcount " + str(attributes["count"]) + "\n\t\t\tdiagrams " + str(attributes["diagram"]) + "\n\t\t\tnode types " + str(attributes["node_subtype"]))
    print("\n\tNodes crosslinked by UniProt id with more than one name")
    for node_id, attributes in nodes.items():
        if node_id.startswith("UNIPROT") and len(attributes["node_subtype"]) == 1 and len(attributes["name"]) > 1:
            print("\n\t\t" + node_id + "\n\t\t\tcount " + str(attributes["count"]) + "\n\t\t\tdiagrams " + str(attributes["diagram"]) + "\n\t\t\tnames " + str(attributes["name"]))
    print("\n\tNodes crosslinked by name with more than one node type")
    for node_id, attributes in nodes.items():
        if not node_id.startswith("UNIPROT") and len(attributes["node_subtype"]) > 1:
            print("\n\t\t" + node_id + "\n\t\t\tcount " + str(attributes["count"]) + "\n\t\t\tdiagrams " + str(attributes["diagram"]) + "\n\t\t\tnode types " + str(attributes["node_subtype"]))

    # save gmls
    # mapping between columns names in the tsv and node/edge attributes in Vanted
    # source_diagram -> diagram
    # source_id -> map_id
    # source_name -> name
    # source_up -> uniprot
    # source_hgnc -> hgnc
    # source_type -> node_type
    # interaction_type -> edge_type
    # target_id -> map_id
    # target_name -> name
    # target_up -> uniprot
    # target_hgnc -> hgnc
    # target_type -> node_type
    # reaction_annotation -> annotation
    # org_source_id -> org_id
    # org_target_id -> org_id
    
    # a gml for each diagram from the tsv
    diagrams = df["source_diagram"].unique().tolist()
    # a gml for all WikiPathways diagrams
    diagrams.insert(0, "WP")
    # a gml for all C19 Disease Maps diagrams
    diagrams.insert(0, "C19DMap")
    # a gml for all diagrams
    diagrams.insert(0, "all")
    # we have only two diagrams from Reactome, thus no map with all Reactome diagrams is created

    for diagram in diagrams:
        # filename or prefix for the filename
        if diagram == "all":
            filename = Path(FILENAME).stem
        elif diagram == "WP":
            filename = "WikiPathways"
        elif diagram == "C19DMap":
            filename = "C19_Disease_Map"
        else:
            filename = diagram.replace(":", "_").replace(" ", "_")
        gml_nodes = ''
        gml_edges = ''
        # we cannot have parallel edges between nodes for the network analysis
        gml_edges_not_parallel = ''
        processed_nodes = {}
        processed_edges = []
        # id for nodes and edges in the gml
        id = 1
        for k in range(df.shape[0]):
            # source node
            startswith_diagram = False
            for val in nodes[df.iloc[k, src_map_id_col]]["diagram"]:
                if val.startswith(diagram):
                    startswith_diagram = True
                    break
            # add source node if it is part of the diagram and has not been added yet
            if (diagram == "all" or startswith_diagram or diagram in nodes[df.iloc[k, src_map_id_col]]["diagram"]) and not df.iloc[k, src_map_id_col] in processed_nodes:
                processed_nodes[df.iloc[k, src_map_id_col]] = str(id)
                gml_nodes = gml_nodes + add_node(str(id), df.iloc[k, src_map_id_col], "; ".join(nodes[df.iloc[k, src_map_id_col]]["diagram"]), "; ".join(nodes[df.iloc[k, src_map_id_col]]["map_id"]), "; ".join(nodes[df.iloc[k, src_map_id_col]]["org_id"]), "; ".join(nodes[df.iloc[k, src_map_id_col]]["name"]), "; ".join(nodes[df.iloc[k, src_map_id_col]]["uniprot"]), "; ".join(nodes[df.iloc[k, src_map_id_col]]["hgnc"]), "; ".join(nodes[df.iloc[k, src_map_id_col]]["node_type"]), "; ".join(nodes[df.iloc[k, src_map_id_col]]["node_subtype"]), "; ".join(nodes[df.iloc[k, src_map_id_col]]["full_annotation"]), "; ".join(nodes[df.iloc[k, src_map_id_col]]["annotation"]), str(nodes[df.iloc[k, src_map_id_col]]["count"]))
                id += 1
            # target node
            startswith_diagram = False
            for val in nodes[df.iloc[k, tgt_map_id_col]]["diagram"]:
                if val.startswith(diagram):
                    startswith_diagram = True
                    break
            # add target node if it is part of the diagram and has not been added yet
            if (diagram == "all" or startswith_diagram or diagram in nodes[df.iloc[k, tgt_map_id_col]]["diagram"]) and not df.iloc[k, tgt_map_id_col] in processed_nodes:
                processed_nodes[df.iloc[k, tgt_map_id_col]] = str(id)
                gml_nodes = gml_nodes + add_node(str(id), df.iloc[k, tgt_map_id_col], "; ".join(nodes[df.iloc[k, tgt_map_id_col]]["diagram"]), "; ".join(nodes[df.iloc[k, tgt_map_id_col]]["map_id"]), "; ".join(nodes[df.iloc[k, tgt_map_id_col]]["org_id"]), "; ".join(nodes[df.iloc[k, tgt_map_id_col]]["name"]), "; ".join(nodes[df.iloc[k, tgt_map_id_col]]["uniprot"]), "; ".join(nodes[df.iloc[k, tgt_map_id_col]]["hgnc"]), "; ".join(nodes[df.iloc[k, tgt_map_id_col]]["node_type"]), "; ".join(nodes[df.iloc[k, tgt_map_id_col]]["node_subtype"]), "; ".join(nodes[df.iloc[k, tgt_map_id_col]]["full_annotation"]), "; ".join(nodes[df.iloc[k, tgt_map_id_col]]["annotation"]), str(nodes[df.iloc[k, tgt_map_id_col]]["count"]))
                id += 1
            # the edge between source node and target node
            # add edge only if the source node and target node have been added already
            if df.iloc[k, src_map_id_col] in processed_nodes and df.iloc[k, tgt_map_id_col] in processed_nodes:
                edge = add_edge(str(id), processed_nodes[df.iloc[k, src_map_id_col]], processed_nodes[df.iloc[k, tgt_map_id_col]], df.iloc[k, diagram_col], df.iloc[k, src_map_id_col], df.iloc[k, tgt_map_id_col], df.iloc[k, edge_type_col])
                gml_edges = gml_edges + edge
                # we cannot have parallel edges between nodes for the network analysis
                # add edge only if no edge between source node and target node has been added yet
                if not set([processed_nodes[df.iloc[k, src_map_id_col]], processed_nodes[df.iloc[k, tgt_map_id_col]]]) in processed_edges:
                    processed_edges.append(set([processed_nodes[df.iloc[k, src_map_id_col]], processed_nodes[df.iloc[k, tgt_map_id_col]]]))
                    gml_edges_not_parallel = gml_edges_not_parallel + edge
                id += 1
        # write gml files
        # save gml with nodes and parallel edges
        with open(path.joinpath(folder, parallel_edges, filename + ".gml"), "w", encoding="utf-8") as f:
            f.write('graph [\n  graphbackgroundcolor "#ffffff"\n  directed 1\n' + gml_nodes + gml_edges + ']\n')
        # save gml with nodes and undirected and not parallel edges for the network analysis 
        with open(path.joinpath(folder, no_parallel_edges, filename + ".gml"), "w", encoding="utf-8") as f:
            f.write('graph [\n  graphbackgroundcolor "#ffffff"\n  directed 0\n' + gml_nodes + gml_edges_not_parallel + ']\n')

if __name__ == "__main__":
    main()
