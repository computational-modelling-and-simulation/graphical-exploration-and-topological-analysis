# Graphical exploration and topological analysis of the COVID-19 Disease Map

## Contributors
Falk Schreiber, <falk.schreiber@uni-konstanz.de>\
Tobias Czauderna, <tobias.czauderna@hs-mittweida.de>\
Karsten Klein, <karsten.klein@uni-konstanz.de>\
Michael Aichem, <michael.aichem@uni-konstanz.de>\
Felicia Burtscher, <felicia.burtscher@uni.lu>

## Scope
In this subproject of the [COVID-19 Disease Map project](https://fairdomhub.org/projects/190), we performed topological analysis on pathways from a [harmonised dataset](https://git-r3lab.uni.lu/covid/models/-/tree/master/Resources/Expand%20the%20diagrams) containing pathways from the COVID-19 Disease Map, WikiPathways, and Reactome. The analysis was done using [Vanted](https://doi.org/10.1186/1752-0509-6-139), [SBGN-ED](https://doi.org/10.1093/bioinformatics/btq407), and [LMME](https://doi.org/10.1093/bioinformatics/btab335) which support the import and export of several standard formats (such as SBML, and SBGN-ML).

Our FAIRDOMHub entry can be found [here](https://fairdomhub.org/investigations/477).

## Methodology

<!--
The collection of files within this repository summarize the steps we followed in our pipeline:

![Pipeline](paper-figs/pipeline-overview.png "Conceptual view of the pipeline")

The following pipeline can be run again as soon as some of the available pathway diagrams change or a new pathway diagram is added:
1. The individual pathway networks are merged into an aggregated network. Necessary data is queried using the Minerva API.
2. For all networks (individual pathway networks and aggregated network), loops and parallel edges are removed.
3. The directed and undirected versions of all networks are exported as *.gml files (and also made available).
4. A centrality analysis is performed and the results are stored in data tables (and also made available).
5. The aggregated network and the centrality values are fed back into LMME-DM to explore hierarchical relationships and centralities.
-->

The following pipeline can be run again as soon as some of the available pathways change or a new pathway is added:
1. Run create_gmls.py to create the gml files.
2. Open gml files in Vanted.
3. Run apply_force_directed_layout.bsh to apply a layout.
4. Run save_all_networks.bsh to save the gml files (maybe not necessary when the next step is performed).
5. Run save_connected_components_gml.bsh to save all connected components for the topological analysis.
6. A centrality analysis is performed and the results are stored in data tables (and also made available).
7. The aggregated network and the centrality values are fed back into LMME-DM to explore hierarchical relationships and centralities.

### Scripts

#### scripts/create_gmls.py

- `scripts/create_gmls.py` creates gml files from the [harmonised dataset](https://git-r3lab.uni.lu/covid/models/-/tree/master/Resources/Expand%20the%20diagrams) which is a tab-separated (tsv) representation of the COVID-19 maps (CD19 Disease Maps, Reactome, WikiPathways).
- It creates two sets of gml files:
  - gml files with directed and parallel edges (the original representation in the tsv file), and
  - gml files with undirected and not parallel edges (for the topological analysis).
- It works on [COVID19_Disease_Map_bipartite.tsv](https://git-r3lab.uni.lu/covid/models/-/blob/master/Resources/Expand%20the%20diagrams/COVID19_Disease_Map_bipartite.tsv) and [COVID19_Disease_Map_bipartite_crosslinked.tsv](https://git-r3lab.uni.lu/covid/models/-/blob/master/Resources/Expand%20the%20diagrams/COVID19_Disease_Map_bipartite_crosslinked.tsv).
- It has not been tested on [COVID19_Disease_Map_bipartite_crosslinked_additional_HGNCs.tsv](https://git-r3lab.uni.lu/covid/models/-/blob/master/Resources/Expand%20the%20diagrams/COVID19_Disease_Map_bipartite_crosslinked_additional_HGNCs.tsv).

#### scripts/apply_force_directed_layout.bsh

`apply_force_directed_layout.bsh` is a beanshell script that can be called in Vanted from the menu "Scripts".

- `apply_force_directed_layout.bsh` applies a force-directed layout to all open networks (sessions) in Vanted.
- It should be run on the gml files created with the script above because this script places all nodes of a gml file at the same position (x=50, y=50).

#### scripts/save_all_networks.bsh

`save_all_networks.bsh` is a beanshell script that can be called in Vanted from the menu "Scripts".

- `save_all_networks.bsh` should be run on all networks (sessions) from the layout step above because the layout runs in threads and the networks cannot be saved automatically in the `apply_force_directed_layout` script itself.
- It is a convenience script, otherwise all networks (sessions) have to be saved manually.
- It is probably not necessary to run this script when the `save_connected_components_gml` script is run afterwards.

#### scripts/save_connected_components_gml.bsh

`save_connected_components_gml.bsh` is a beanshell script that can be called in Vanted from the menu "Scripts".

- `save_connected_components_gml.bsh` saves all connected components from all open networks (sessions) in Vanted.
- It creates a folder for each network containing the connected components as gml files.
- It creates also a folder „largest-components“ which contains the largest connected component for each network.

### Centrality Analysis Results

### gml Graph Files

<!-- `gml_graph_files` contains the two subdirectories `directed` and `undirected`, which each contain 23 graph `.gml` files (22 pathway networks and 1 aggregated network).) -->

## Graphical Exploration

We also provide *LMME-DM*, a customised version of the Vanted Add-on *LMME*. It can be downloaded from [GitHub](https://github.com/LSI-UniKonstanz/lmme-dm). An installation guide as well as user guides can be found in the README on GitHub as well as on the [webpage](https://www.cls.uni-konstanz.de/software/lmme/lmme-dm).

#### Getting Started
1. Install LMME-DM (instructions can be found [here](https://www.cls.uni-konstanz.de/software/lmme/lmme-dm))
2. Load the file `gml_graph_files/gmls_crosslinked/parallel_edges/COVID19_Disease_Map_bipartite_crosslinked.gml` into Vanted
3. Click *Initialise Exploration*
4. Click *Show Overview Graph*
5. Click *Node Highlighting*, paste the content of `exploration/top100SpeciesCovid19DiseaseMap.txt` and click on *Size in Overview*
6. Select pathway nodes of interest and click on *Show Selected Subsystems*

You can now see the pathway crosstalks on the left (where the node sizes show the maximum centrality contained in this pathway) and the detailed interactions of the selected pathways/subsystems on the right.
