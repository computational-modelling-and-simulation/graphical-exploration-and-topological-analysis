# generated with VANTED V2.8.2 at Fri Mar 04 09:53:02 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc.symbol:C3;urn:miriam:mesh:D003179;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000125730;urn:miriam:refseq:NM_000064;urn:miriam:uniprot:P01024;urn:miriam:uniprot:P01024;urn:miriam:hgnc:1318;urn:miriam:ncbigene:718;urn:miriam:ncbigene:718"
      hgnc "HGNC_SYMBOL:C3"
      map_id "M121_180"
      name "C3b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa310"
      uniprot "UNIPROT:P01024"
    ]
    graphics [
      x 1052.2337090602362
      y 259.63904426031627
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_180"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:26521297"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_61"
      name "PMID:26521297"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re285"
      uniprot "NA"
    ]
    graphics [
      x 946.4379606543516
      y 316.50128722473505
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D003179;urn:miriam:brenda:3.4.21.47;urn:miriam:taxonomy:9606;urn:miriam:pubmed:12440962;urn:miriam:hgnc:1037;urn:miriam:mesh:D051561;urn:miriam:hgnc.symbol:C3;urn:miriam:mesh:D003179;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000125730;urn:miriam:refseq:NM_000064;urn:miriam:uniprot:P01024;urn:miriam:uniprot:P01024;urn:miriam:hgnc:1318;urn:miriam:ncbigene:718;urn:miriam:ncbigene:718;urn:miriam:ec-code:3.4.21.47;urn:miriam:ensembl:ENSG00000243649;urn:miriam:hgnc.symbol:CFB;urn:miriam:uniprot:P00751;urn:miriam:uniprot:P00751;urn:miriam:hgnc.symbol:CFB;urn:miriam:hgnc:1037;urn:miriam:refseq:NM_001710;urn:miriam:ncbigene:629;urn:miriam:ncbigene:629"
      hgnc "HGNC_SYMBOL:C3;HGNC_SYMBOL:CFB"
      map_id "M121_8"
      name "C3b:Bb"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa26"
      uniprot "UNIPROT:P01024;UNIPROT:P00751"
    ]
    graphics [
      x 1092.0828651946026
      y 208.01583592019642
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:brenda:34.4.21.47;urn:miriam:mesh:D003179;urn:miriam:taxonomy:9606;urn:miriam:mesh:D051566;urn:miriam:pubmed:12440962;urn:miriam:hgnc:1037;urn:miriam:ec-code:3.4.21.47;urn:miriam:ensembl:ENSG00000243649;urn:miriam:hgnc.symbol:CFB;urn:miriam:uniprot:P00751;urn:miriam:uniprot:P00751;urn:miriam:hgnc.symbol:CFB;urn:miriam:hgnc:1037;urn:miriam:refseq:NM_001710;urn:miriam:ncbigene:629;urn:miriam:ncbigene:629;urn:miriam:hgnc.symbol:C3;urn:miriam:mesh:D003179;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000125730;urn:miriam:refseq:NM_000064;urn:miriam:uniprot:P01024;urn:miriam:uniprot:P01024;urn:miriam:hgnc:1318;urn:miriam:ncbigene:718;urn:miriam:ncbigene:718"
      hgnc "HGNC_SYMBOL:CFB;HGNC_SYMBOL:C3"
      map_id "M121_9"
      name "C3b:Bb:C3b"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa27"
      uniprot "UNIPROT:P00751;UNIPROT:P01024"
    ]
    graphics [
      x 937.6914932716288
      y 521.0820715519912
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:20689271;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_271"
      name "s86"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa532"
      uniprot "NA"
    ]
    graphics [
      x 1981.124797381525
      y 596.9789756656701
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_271"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "PUBMED:32525548"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_129"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re398"
      uniprot "NA"
    ]
    graphics [
      x 1999.1560767471926
      y 475.717459239632
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_129"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D007008;urn:miriam:taxonomy:9606"
      hgnc "NA"
      map_id "M121_270"
      name "Hypokalemia"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa531"
      uniprot "NA"
    ]
    graphics [
      x 1920.639286468765
      y 497.88934017570296
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_270"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29103"
      hgnc "NA"
      map_id "M121_265"
      name "K_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "sa521"
      uniprot "NA"
    ]
    graphics [
      x 2035.034926813978
      y 579.3598258043876
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_265"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:mesh:D050776;urn:miriam:mesh:C050974;urn:miriam:hgnc:1339;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:C6;urn:miriam:hgnc.symbol:C6;urn:miriam:refseq:NM_000065;urn:miriam:ensembl:ENSG00000039537;urn:miriam:uniprot:P13671;urn:miriam:uniprot:P13671;urn:miriam:hgnc:1339;urn:miriam:ncbigene:729;urn:miriam:ncbigene:729;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1331;urn:miriam:hgnc.symbol:C5;urn:miriam:mesh:D050776;urn:miriam:refseq:NM_001735;urn:miriam:ensembl:ENSG00000106804;urn:miriam:uniprot:P01031;urn:miriam:uniprot:P01031;urn:miriam:ncbigene:727;urn:miriam:ncbigene:727"
      hgnc "HGNC_SYMBOL:C6;HGNC_SYMBOL:C5"
      map_id "M121_10"
      name "C5b:C6"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa28"
      uniprot "UNIPROT:P13671;UNIPROT:P01031"
    ]
    graphics [
      x 1407.2987999899046
      y 1702.8888441276708
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "PUBMED:5058233"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_53"
      name "PMID:5058233"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re264"
      uniprot "NA"
    ]
    graphics [
      x 1301.0292138528412
      y 1771.7176126432628
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P10643;urn:miriam:uniprot:P10643;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000112936;urn:miriam:hgnc.symbol:C7;urn:miriam:ncbigene:730;urn:miriam:hgnc.symbol:C7;urn:miriam:ncbigene:730;urn:miriam:hgnc:1346;urn:miriam:refseq:NM_000587"
      hgnc "HGNC_SYMBOL:C7"
      map_id "M121_184"
      name "C7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa321"
      uniprot "UNIPROT:P10643"
    ]
    graphics [
      x 1368.5123560430384
      y 1889.6555271570155
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_184"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:28630159;urn:miriam:mesh:C037453;urn:miriam:hgnc:1346;urn:miriam:mesh:D050776;urn:miriam:hgnc:1339;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1331;urn:miriam:hgnc.symbol:C5;urn:miriam:mesh:D050776;urn:miriam:refseq:NM_001735;urn:miriam:ensembl:ENSG00000106804;urn:miriam:uniprot:P01031;urn:miriam:uniprot:P01031;urn:miriam:ncbigene:727;urn:miriam:ncbigene:727;urn:miriam:uniprot:P10643;urn:miriam:uniprot:P10643;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000112936;urn:miriam:hgnc.symbol:C7;urn:miriam:ncbigene:730;urn:miriam:hgnc.symbol:C7;urn:miriam:ncbigene:730;urn:miriam:hgnc:1346;urn:miriam:refseq:NM_000587;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:C6;urn:miriam:hgnc.symbol:C6;urn:miriam:refseq:NM_000065;urn:miriam:ensembl:ENSG00000039537;urn:miriam:uniprot:P13671;urn:miriam:uniprot:P13671;urn:miriam:hgnc:1339;urn:miriam:ncbigene:729;urn:miriam:ncbigene:729"
      hgnc "HGNC_SYMBOL:C5;HGNC_SYMBOL:C7;HGNC_SYMBOL:C6"
      map_id "M121_11"
      name "C5b:C6:C7"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa29"
      uniprot "UNIPROT:P01031;UNIPROT:P10643;UNIPROT:P13671"
    ]
    graphics [
      x 1219.2941017280261
      y 1532.004859043946
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_236"
      name "s539"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa462"
      uniprot "NA"
    ]
    graphics [
      x 1084.1948734515192
      y 405.1790843181028
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_236"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_78"
      name "PMICID:PMC7260598"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re324"
      uniprot "NA"
    ]
    graphics [
      x 1086.622256514026
      y 529.5676692953364
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:2697049;urn:miriam:mesh:D012327"
      hgnc "NA"
      map_id "M121_158"
      name "SARS_minus_CoV_minus_2_space_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa207"
      uniprot "NA"
    ]
    graphics [
      x 855.1726844534998
      y 732.1067806720897
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_158"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0005579;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1352;urn:miriam:hgnc:1353;urn:miriam:hgnc:1354;urn:miriam:hgnc:1346;urn:miriam:mesh:D050776;urn:miriam:hgnc:1358;urn:miriam:mesh:D015938;urn:miriam:hgnc:1339;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:C6;urn:miriam:hgnc.symbol:C6;urn:miriam:refseq:NM_000065;urn:miriam:ensembl:ENSG00000039537;urn:miriam:uniprot:P13671;urn:miriam:uniprot:P13671;urn:miriam:hgnc:1339;urn:miriam:ncbigene:729;urn:miriam:ncbigene:729;urn:miriam:hgnc.symbol:C8A;urn:miriam:refseq:NM_000562;urn:miriam:hgnc.symbol:C8A;urn:miriam:hgnc:1352;urn:miriam:uniprot:P07357;urn:miriam:uniprot:P07357;urn:miriam:ncbigene:731;urn:miriam:ncbigene:731;urn:miriam:ensembl:ENSG00000157131;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1331;urn:miriam:hgnc.symbol:C5;urn:miriam:mesh:D050776;urn:miriam:refseq:NM_001735;urn:miriam:ensembl:ENSG00000106804;urn:miriam:uniprot:P01031;urn:miriam:uniprot:P01031;urn:miriam:ncbigene:727;urn:miriam:ncbigene:727;urn:miriam:uniprot:P10643;urn:miriam:uniprot:P10643;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000112936;urn:miriam:hgnc.symbol:C7;urn:miriam:ncbigene:730;urn:miriam:hgnc.symbol:C7;urn:miriam:ncbigene:730;urn:miriam:hgnc:1346;urn:miriam:refseq:NM_000587;urn:miriam:ensembl:ENSG00000176919;urn:miriam:refseq:NM_000606;urn:miriam:hgnc:1354;urn:miriam:hgnc.symbol:C8G;urn:miriam:ncbigene:733;urn:miriam:hgnc.symbol:C8G;urn:miriam:ncbigene:733;urn:miriam:uniprot:P07360;urn:miriam:uniprot:P07360;urn:miriam:ncbigene:735;urn:miriam:ncbigene:735;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1358;urn:miriam:ensembl:ENSG00000113600;urn:miriam:hgnc.symbol:C9;urn:miriam:hgnc.symbol:C9;urn:miriam:refseq:NM_001737;urn:miriam:uniprot:P02748;urn:miriam:uniprot:P02748;urn:miriam:refseq:NM_000066;urn:miriam:hgnc.symbol:C8B;urn:miriam:hgnc.symbol:C8B;urn:miriam:hgnc:1353;urn:miriam:ncbigene:732;urn:miriam:ensembl:ENSG00000021852;urn:miriam:ncbigene:732;urn:miriam:uniprot:P07358;urn:miriam:uniprot:P07358"
      hgnc "HGNC_SYMBOL:C6;HGNC_SYMBOL:C8A;HGNC_SYMBOL:C5;HGNC_SYMBOL:C7;HGNC_SYMBOL:C8G;HGNC_SYMBOL:C9;HGNC_SYMBOL:C8B"
      map_id "M121_14"
      name "C5b_minus_9"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa32"
      uniprot "UNIPROT:P13671;UNIPROT:P07357;UNIPROT:P01031;UNIPROT:P10643;UNIPROT:P07360;UNIPROT:P02748;UNIPROT:P07358"
    ]
    graphics [
      x 1389.7918695412582
      y 622.6844424513722
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc.symbol:PLG;urn:miriam:hgnc.symbol:PLG;urn:miriam:ec-code:3.4.21.7;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000122194;urn:miriam:ncbigene:5340;urn:miriam:ncbigene:5340;urn:miriam:hgnc:9071;urn:miriam:refseq:NM_000301;urn:miriam:uniprot:P00747;urn:miriam:uniprot:P00747"
      hgnc "HGNC_SYMBOL:PLG"
      map_id "M121_160"
      name "Plasminogen"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa211"
      uniprot "UNIPROT:P00747"
    ]
    graphics [
      x 947.7776071027876
      y 894.6283330235951
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_160"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "PUBMED:3850647;PUBMED:89876;PUBMED:6539333;PUBMED:2966802"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_35"
      name "PMID: 89876, PMID:385647"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re178"
      uniprot "NA"
    ]
    graphics [
      x 820.2273583252697
      y 1065.073068854926
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:ec-code:3.4.21.68;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000104368;urn:miriam:hgnc:9051;urn:miriam:hgnc.symbol:PLAT;urn:miriam:hgnc.symbol:PLAT;urn:miriam:uniprot:P00750;urn:miriam:uniprot:P00750;urn:miriam:ncbigene:5327;urn:miriam:ncbigene:5327;urn:miriam:refseq:NM_000930"
      hgnc "HGNC_SYMBOL:PLAT"
      map_id "M121_162"
      name "PLAT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa213"
      uniprot "UNIPROT:P00750"
    ]
    graphics [
      x 1125.225957851355
      y 1009.2489688786352
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_162"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:ncbigene:5328;urn:miriam:ncbigene:5328;urn:miriam:hgnc.symbol:PLAU;urn:miriam:hgnc.symbol:PLAU;urn:miriam:ec-code:3.4.21.73;urn:miriam:ensembl:ENSG00000122861;urn:miriam:hgnc:9052;urn:miriam:uniprot:P00749;urn:miriam:uniprot:P00749;urn:miriam:refseq:NM_002658"
      hgnc "HGNC_SYMBOL:PLAU"
      map_id "M121_167"
      name "PLAU"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa236"
      uniprot "UNIPROT:P00749"
    ]
    graphics [
      x 792.6676445929211
      y 807.3103081973885
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_167"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:ncbigene:2160;urn:miriam:ncbigene:2160;urn:miriam:uniprot:P03951;urn:miriam:uniprot:P03951;urn:miriam:ensembl:ENSG00000088926;urn:miriam:mesh:D015945;urn:miriam:hgnc.symbol:F11;urn:miriam:brenda:3.4.21.27;urn:miriam:hgnc:3529;urn:miriam:ec-code:3.4.21.27;urn:miriam:refseq:NM_000128"
      hgnc "HGNC_SYMBOL:F11"
      map_id "M121_137"
      name "F11a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa173"
      uniprot "UNIPROT:P03951"
    ]
    graphics [
      x 530.8500618271219
      y 1036.1818447408948
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_137"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:refseq:NM_000892;urn:miriam:ensembl:ENSG00000164344;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.21.34;urn:miriam:uniprot:P03952;urn:miriam:uniprot:P03952;urn:miriam:hgnc:6371;urn:miriam:ncbigene:3818;urn:miriam:ncbigene:3818;urn:miriam:hgnc.symbol:KLKB1;urn:miriam:hgnc.symbol:KLKB1"
      hgnc "HGNC_SYMBOL:KLKB1"
      map_id "M121_132"
      name "KLKB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa167"
      uniprot "UNIPROT:P03952"
    ]
    graphics [
      x 961.4818925132521
      y 1227.3805513311904
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc.symbol:PLG;urn:miriam:ec-code:3.4.21.7;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000122194;urn:miriam:ncbigene:5340;urn:miriam:ncbigene:5340;urn:miriam:hgnc:9071;urn:miriam:refseq:NM_000301;urn:miriam:mesh:D005341;urn:miriam:brenda:3.4.21.7;urn:miriam:uniprot:P00747;urn:miriam:uniprot:P00747"
      hgnc "HGNC_SYMBOL:PLG"
      map_id "M121_161"
      name "Plasmin"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa212"
      uniprot "UNIPROT:P00747"
    ]
    graphics [
      x 700.4736035545086
      y 1310.6099135824913
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_161"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc:5992;urn:miriam:hgnc.symbol:IL1B;urn:miriam:hgnc.symbol:IL1B;urn:miriam:uniprot:P01584;urn:miriam:uniprot:P01584;urn:miriam:refseq:NM_000576;urn:miriam:ncbigene:3553;urn:miriam:ncbigene:3553;urn:miriam:ensembl:ENSG00000125538"
      hgnc "HGNC_SYMBOL:IL1B"
      map_id "M121_170"
      name "IL1B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa244"
      uniprot "UNIPROT:P01584"
    ]
    graphics [
      x 868.9031110028501
      y 955.3837288013633
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_170"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "PUBMED:27561337"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_43"
      name "PMID:27561337"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re227"
      uniprot "NA"
    ]
    graphics [
      x 1012.7758701924907
      y 1171.134402645666
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:mesh:D013923;urn:miriam:mesh:D055806"
      hgnc "NA"
      map_id "M121_175"
      name "Thrombosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa271"
      uniprot "NA"
    ]
    graphics [
      x 1199.0795779635364
      y 1207.9592065773538
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_175"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc.symbol:REN;urn:miriam:hgnc.symbol:REN;urn:miriam:taxonomy:9606;urn:miriam:uniprot:P00797;urn:miriam:uniprot:P00797;urn:miriam:hgnc:9958;urn:miriam:ensembl:ENSG00000143839;urn:miriam:ncbigene:5972;urn:miriam:refseq:NM_000537;urn:miriam:ncbigene:5972;urn:miriam:ec-code:3.4.23.15"
      hgnc "HGNC_SYMBOL:REN"
      map_id "M121_216"
      name "Prorenin"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa415"
      uniprot "UNIPROT:P00797"
    ]
    graphics [
      x 513.2292128877466
      y 1343.6907888284386
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_216"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      annotation "PUBMED:692685"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_83"
      name "PMID:692685"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re330"
      uniprot "NA"
    ]
    graphics [
      x 646.8610710239965
      y 1276.9327218481403
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:brenda:3.4.21.34;urn:miriam:refseq:NM_000892;urn:miriam:ensembl:ENSG00000164344;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.21.34;urn:miriam:uniprot:P03952;urn:miriam:uniprot:P03952;urn:miriam:mesh:D020842;urn:miriam:hgnc:6371;urn:miriam:ncbigene:3818;urn:miriam:ncbigene:3818;urn:miriam:hgnc.symbol:KLKB1"
      hgnc "HGNC_SYMBOL:KLKB1"
      map_id "M121_172"
      name "Kallikrein"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa251"
      uniprot "UNIPROT:P03952"
    ]
    graphics [
      x 897.4175082277405
      y 1206.7019228812158
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_172"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc.symbol:REN;urn:miriam:taxonomy:9606;urn:miriam:uniprot:P00797;urn:miriam:uniprot:P00797;urn:miriam:hgnc:9958;urn:miriam:ensembl:ENSG00000143839;urn:miriam:ncbigene:5972;urn:miriam:refseq:NM_000537;urn:miriam:ncbigene:5972;urn:miriam:ec-code:3.4.23.15"
      hgnc "HGNC_SYMBOL:REN"
      map_id "M121_151"
      name "REN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa197"
      uniprot "UNIPROT:P00797"
    ]
    graphics [
      x 648.7348582899609
      y 1372.919125295947
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_151"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc:6137;urn:miriam:hgnc:12726;urn:miriam:pubmed:25051961;urn:miriam:taxonomy:10090;urn:miriam:hgnc:14338;urn:miriam:hgnc:6153;urn:miriam:hgnc:6137;urn:miriam:refseq:NM_002203;urn:miriam:ncbigene:3673;urn:miriam:ensembl:ENSG00000164171;urn:miriam:uniprot:P17301;urn:miriam:uniprot:P17301;urn:miriam:ncbigene:3673;urn:miriam:hgnc.symbol:ITGA2;urn:miriam:hgnc.symbol:ITGA2;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:GP6;urn:miriam:hgnc.symbol:GP6;urn:miriam:hgnc:14388;urn:miriam:ensembl:ENSG00000088053;urn:miriam:ncbigene:51206;urn:miriam:ncbigene:51206;urn:miriam:refseq:NM_001083899;urn:miriam:uniprot:Q9HCN6;urn:miriam:uniprot:Q9HCN6;urn:miriam:hgnc:12726;urn:miriam:taxonomy:9606;urn:miriam:refseq:NM_000552;urn:miriam:uniprot:P04275;urn:miriam:uniprot:P04275;urn:miriam:ncbigene:7450;urn:miriam:ncbigene:7450;urn:miriam:hgnc.symbol:VWF;urn:miriam:hgnc.symbol:VWF;urn:miriam:ensembl:ENSG00000110799;urn:miriam:refseq:NM_002211;urn:miriam:uniprot:P05556;urn:miriam:uniprot:P05556;urn:miriam:hgnc.symbol:ITGB1;urn:miriam:hgnc.symbol:ITGB1;urn:miriam:ncbigene:3688;urn:miriam:ncbigene:3688;urn:miriam:hgnc:6153;urn:miriam:ensembl:ENSG00000150093"
      hgnc "HGNC_SYMBOL:ITGA2;HGNC_SYMBOL:GP6;HGNC_SYMBOL:VWF;HGNC_SYMBOL:ITGB1"
      map_id "M121_19"
      name "GP6:alpha2beta1:VWF"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa40"
      uniprot "UNIPROT:P17301;UNIPROT:Q9HCN6;UNIPROT:P04275;UNIPROT:P05556"
    ]
    graphics [
      x 880.7869225812357
      y 803.1033138357571
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "PUBMED:19286885"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_98"
      name "PMID:19286885"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "re352"
      uniprot "NA"
    ]
    graphics [
      x 730.7312782145971
      y 625.2029303080124
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:19286885;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_223"
      name "s521"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa440"
      uniprot "NA"
    ]
    graphics [
      x 610.4315118770292
      y 608.5446211947951
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_223"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:obo.chebi:CHEBI%3A2718"
      hgnc "NA"
      map_id "M121_149"
      name "angiotensin_space_I"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa195"
      uniprot "NA"
    ]
    graphics [
      x 1305.8336213422556
      y 1163.0786951700554
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_149"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      annotation "PUBMED:10969042;PUBMED:190881"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_25"
      name "PMID:19065996"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re157"
      uniprot "NA"
    ]
    graphics [
      x 1625.0077819737717
      y 1048.9263744436553
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc:2707;urn:miriam:uniprot:P12821;urn:miriam:uniprot:P12821;urn:miriam:ec-code:3.4.15.1;urn:miriam:taxonomy:9606;urn:miriam:ncbigene:1636;urn:miriam:ncbigene:1636;urn:miriam:hgnc.symbol:ACE;urn:miriam:refseq:NM_000789;urn:miriam:hgnc.symbol:ACE;urn:miriam:ec-code:3.2.1.-;urn:miriam:ensembl:ENSG00000159640"
      hgnc "HGNC_SYMBOL:ACE"
      map_id "M121_152"
      name "ACE"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa198"
      uniprot "UNIPROT:P12821"
    ]
    graphics [
      x 1739.8226179203657
      y 904.1800069812305
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_152"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:obo.chebi:CHEBI%3A2718"
      hgnc "NA"
      map_id "M121_148"
      name "angiotensin_space_II"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa194"
      uniprot "NA"
    ]
    graphics [
      x 1617.7085122847757
      y 895.2092761035042
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_148"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:refseq:NM_000594;urn:miriam:hgnc.symbol:TNF;urn:miriam:hgnc.symbol:TNF;urn:miriam:taxonomy:9606;urn:miriam:uniprot:P01375;urn:miriam:uniprot:P01375;urn:miriam:hgnc:11892;urn:miriam:ncbigene:7124;urn:miriam:ncbigene:7124;urn:miriam:ensembl:ENSG00000232810"
      hgnc "HGNC_SYMBOL:TNF"
      map_id "M121_168"
      name "TNF"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa238"
      uniprot "UNIPROT:P01375"
    ]
    graphics [
      x 1259.850972881663
      y 974.0562863013296
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_168"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "PUBMED:27561337"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_45"
      name "PMID:27561337"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re231"
      uniprot "NA"
    ]
    graphics [
      x 1339.4415103106612
      y 1183.5446343651702
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P00734;urn:miriam:uniprot:P00734;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.21.5;urn:miriam:hgnc:3535;urn:miriam:refseq:NM_000506;urn:miriam:ensembl:ENSG00000180210;urn:miriam:hgnc.symbol:F2;urn:miriam:hgnc.symbol:F2;urn:miriam:ncbigene:2147;urn:miriam:ncbigene:2147"
      hgnc "HGNC_SYMBOL:F2"
      map_id "M121_145"
      name "Thrombin"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa181"
      uniprot "UNIPROT:P00734"
    ]
    graphics [
      x 375.1628776480658
      y 1202.7462373875737
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_145"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      annotation "PUBMED:8388351;PUBMED:6282863"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_47"
      name "PMID:8388351"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re256"
      uniprot "NA"
    ]
    graphics [
      x 395.28443426806405
      y 1397.0560661961922
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:ncbigene:7056;urn:miriam:ncbigene:7056;urn:miriam:refseq:NM_000361;urn:miriam:uniprot:P07204;urn:miriam:uniprot:P07204;urn:miriam:hgnc:11784;urn:miriam:ensembl:ENSG00000178726;urn:miriam:hgnc.symbol:THBD;urn:miriam:hgnc.symbol:THBD"
      hgnc "HGNC_SYMBOL:THBD"
      map_id "M121_176"
      name "Thrombomodulin"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa301"
      uniprot "UNIPROT:P07204"
    ]
    graphics [
      x 267.3491440184059
      y 1300.1484525143126
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_176"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D013917;urn:miriam:hgnc:11784;urn:miriam:taxonomy:9986;urn:miriam:pubmed:6282863;urn:miriam:taxonomy:9606;urn:miriam:ncbigene:7056;urn:miriam:ncbigene:7056;urn:miriam:refseq:NM_000361;urn:miriam:uniprot:P07204;urn:miriam:uniprot:P07204;urn:miriam:hgnc:11784;urn:miriam:ensembl:ENSG00000178726;urn:miriam:hgnc.symbol:THBD;urn:miriam:hgnc.symbol:THBD;urn:miriam:mesh:D013917;urn:miriam:uniprot:P00734;urn:miriam:uniprot:P00734;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.21.5;urn:miriam:hgnc:3535;urn:miriam:refseq:NM_000506;urn:miriam:ensembl:ENSG00000180210;urn:miriam:hgnc.symbol:F2;urn:miriam:ncbigene:2147;urn:miriam:ncbigene:2147"
      hgnc "HGNC_SYMBOL:THBD;HGNC_SYMBOL:F2"
      map_id "M121_7"
      name "Thrombin:Thrombomodulin"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa25"
      uniprot "UNIPROT:P07204;UNIPROT:P00734"
    ]
    graphics [
      x 601.9990006389894
      y 1477.9516356364095
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:refseq:NM_002377;urn:miriam:ensembl:ENSG00000130368;urn:miriam:hgnc:6899;urn:miriam:ncbigene:4142;urn:miriam:ncbigene:4142;urn:miriam:hgnc.symbol:MAS1;urn:miriam:uniprot:P04201;urn:miriam:uniprot:P04201;urn:miriam:hgnc.symbol:MAS1"
      hgnc "HGNC_SYMBOL:MAS1"
      map_id "M121_250"
      name "MAS1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa496"
      uniprot "UNIPROT:P04201"
    ]
    graphics [
      x 1599.164339649857
      y 1268.7003833344315
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_250"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "PUBMED:18026570"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_108"
      name "PMID:18026570"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re364"
      uniprot "NA"
    ]
    graphics [
      x 1605.3580001019711
      y 1132.4266436405273
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A55438;urn:miriam:taxonomy:9606"
      hgnc "NA"
      map_id "M121_208"
      name "angiotensin_space_I_minus_7"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa400"
      uniprot "NA"
    ]
    graphics [
      x 1560.1716371189323
      y 851.1304923685082
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_208"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:refseq:NM_002377;urn:miriam:ensembl:ENSG00000130368;urn:miriam:hgnc:6899;urn:miriam:ncbigene:4142;urn:miriam:ncbigene:4142;urn:miriam:hgnc.symbol:MAS1;urn:miriam:uniprot:P04201;urn:miriam:uniprot:P04201;urn:miriam:hgnc.symbol:MAS1"
      hgnc "HGNC_SYMBOL:MAS1"
      map_id "M121_245"
      name "MAS1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa483"
      uniprot "UNIPROT:P04201"
    ]
    graphics [
      x 1434.2573033650829
      y 1284.4264467994626
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_245"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:uniprot:P05121;urn:miriam:uniprot:P05121;urn:miriam:ncbigene:5054;urn:miriam:ncbigene:5054;urn:miriam:ensembl:ENSG00000106366;urn:miriam:hgnc:8593;urn:miriam:refseq:NM_000602;urn:miriam:hgnc.symbol:SERPINE1;urn:miriam:hgnc.symbol:SERPINE1;urn:miriam:hgnc:8583"
      hgnc "HGNC_SYMBOL:SERPINE1"
      map_id "M121_163"
      name "SERPINE1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa224"
      uniprot "UNIPROT:P05121"
    ]
    graphics [
      x 1119.301522067155
      y 617.9324769556854
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_163"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "PUBMED:22449964"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_39"
      name "PMID:22449964"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re182"
      uniprot "NA"
    ]
    graphics [
      x 861.3642244010805
      y 674.7722239547
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:ec-code:3.4.21.68;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000104368;urn:miriam:hgnc:9051;urn:miriam:hgnc.symbol:PLAT;urn:miriam:hgnc.symbol:PLAT;urn:miriam:uniprot:P00750;urn:miriam:uniprot:P00750;urn:miriam:ncbigene:5327;urn:miriam:ncbigene:5327;urn:miriam:refseq:NM_000930"
      hgnc "HGNC_SYMBOL:PLAT"
      map_id "M121_164"
      name "PLAT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa225"
      uniprot "UNIPROT:P00750"
    ]
    graphics [
      x 1050.630373497087
      y 709.2921783821432
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_164"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:2697049;urn:miriam:mesh:D012327"
      hgnc "NA"
      map_id "M121_253"
      name "SARS_minus_CoV_minus_2_space_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa499"
      uniprot "NA"
    ]
    graphics [
      x 607.4753337975824
      y 1023.4112444408557
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_253"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc:9051;urn:miriam:pubmed:22449964;urn:miriam:hgnc:8593;urn:miriam:intact:EBI-7800882;urn:miriam:taxonomy:9606;urn:miriam:uniprot:P05121;urn:miriam:uniprot:P05121;urn:miriam:ncbigene:5054;urn:miriam:ncbigene:5054;urn:miriam:ensembl:ENSG00000106366;urn:miriam:hgnc:8593;urn:miriam:refseq:NM_000602;urn:miriam:hgnc.symbol:SERPINE1;urn:miriam:hgnc.symbol:SERPINE1;urn:miriam:hgnc:8583;urn:miriam:ec-code:3.4.21.68;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000104368;urn:miriam:hgnc:9051;urn:miriam:hgnc.symbol:PLAT;urn:miriam:hgnc.symbol:PLAT;urn:miriam:uniprot:P00750;urn:miriam:uniprot:P00750;urn:miriam:ncbigene:5327;urn:miriam:ncbigene:5327;urn:miriam:refseq:NM_000930"
      hgnc "HGNC_SYMBOL:SERPINE1;HGNC_SYMBOL:PLAT"
      map_id "M121_5"
      name "PLAT:SERPINE1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa16"
      uniprot "UNIPROT:P05121;UNIPROT:P00750"
    ]
    graphics [
      x 827.0894916415466
      y 512.7301946870285
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_248"
      name "s585"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa494"
      uniprot "NA"
    ]
    graphics [
      x 1499.1709133890665
      y 985.100299180319
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_248"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      annotation "PUBMED:9066005"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_113"
      name "PMID:9066005"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re372"
      uniprot "NA"
    ]
    graphics [
      x 1359.7622367195077
      y 994.9606744059705
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:obo.chebi:CHEBI%3A3165"
      hgnc "NA"
      map_id "M121_209"
      name "Bradykinin"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa402"
      uniprot "NA"
    ]
    graphics [
      x 1490.423878099913
      y 924.9587804663622
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_209"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      annotation "PUBMED:21304106"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_82"
      name "PMID:21304106"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re329"
      uniprot "NA"
    ]
    graphics [
      x 857.4186899566513
      y 1343.4816780889023
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc:3530;urn:miriam:ensembl:ENSG00000131187;urn:miriam:ncbigene:2161;urn:miriam:ncbigene:2161;urn:miriam:mesh:D015956;urn:miriam:refseq:NM_000505;urn:miriam:hgnc.symbol:F12;urn:miriam:brenda:3.4.21.38;urn:miriam:uniprot:P00748;urn:miriam:uniprot:P00748;urn:miriam:ec-code:3.4.21.38"
      hgnc "HGNC_SYMBOL:F12"
      map_id "M121_131"
      name "F12a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa165"
      uniprot "UNIPROT:P00748"
    ]
    graphics [
      x 820.3740978005779
      y 1484.1081645838567
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_131"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:ncbigene:3569;urn:miriam:ncbigene:3569;urn:miriam:taxonomy:9606;urn:miriam:uniprot:P05231;urn:miriam:uniprot:P05231;urn:miriam:ensembl:ENSG00000136244;urn:miriam:hgnc:6018;urn:miriam:hgnc.symbol:IL6;urn:miriam:hgnc.symbol:IL6;urn:miriam:refseq:NM_000600"
      hgnc "HGNC_SYMBOL:IL6"
      map_id "M121_169"
      name "IL6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa242"
      uniprot "UNIPROT:P05231"
    ]
    graphics [
      x 1315.9798622733576
      y 708.7731284777676
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_169"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      annotation "PUBMED:27561337"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_44"
      name "PMID:27561337"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re229"
      uniprot "NA"
    ]
    graphics [
      x 1287.9151974611987
      y 1018.8091857801076
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:refseq:NM_000132;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:F8;urn:miriam:hgnc.symbol:F8;urn:miriam:hgnc:3546;urn:miriam:uniprot:P00451;urn:miriam:uniprot:P00451;urn:miriam:ncbigene:2157;urn:miriam:ncbigene:2157;urn:miriam:ensembl:ENSG00000185010"
      hgnc "HGNC_SYMBOL:F8"
      map_id "M121_143"
      name "F8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa179"
      uniprot "UNIPROT:P00451"
    ]
    graphics [
      x 433.86107340629394
      y 869.8173680967143
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_143"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      annotation "PUBMED:15746105"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_122"
      name "NA"
      node_subtype "TRUNCATION"
      node_type "reaction"
      org_id "re390"
      uniprot "NA"
    ]
    graphics [
      x 420.9593913465958
      y 1069.0527500955093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_122"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:ec-code:3.4.21.69;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000115718;urn:miriam:hgnc:9451;urn:miriam:refseq:NM_000312;urn:miriam:uniprot:P04070;urn:miriam:uniprot:P04070;urn:miriam:hgnc.symbol:PROC;urn:miriam:ncbigene:5624;urn:miriam:hgnc.symbol:PROC;urn:miriam:ncbigene:5624"
      hgnc "HGNC_SYMBOL:PROC"
      map_id "M121_157"
      name "PROC"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa205"
      uniprot "UNIPROT:P04070"
    ]
    graphics [
      x 761.0881504218812
      y 1189.5299382769415
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_157"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D010446"
      hgnc "NA"
      map_id "M121_147"
      name "Small_space_peptide"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa183"
      uniprot "NA"
    ]
    graphics [
      x 391.9245969168187
      y 983.4600495817147
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_147"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:refseq:NM_000132;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:F8;urn:miriam:mesh:D015944;urn:miriam:hgnc:3546;urn:miriam:uniprot:P00451;urn:miriam:uniprot:P00451;urn:miriam:ncbigene:2157;urn:miriam:ncbigene:2157;urn:miriam:ensembl:ENSG00000185010"
      hgnc "HGNC_SYMBOL:F8"
      map_id "M121_144"
      name "F8a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa180"
      uniprot "UNIPROT:P00451"
    ]
    graphics [
      x 224.04870893584257
      y 942.505945842204
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_144"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:20689271;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_207"
      name "s86"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa399"
      uniprot "NA"
    ]
    graphics [
      x 1639.4029218203796
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_207"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      annotation "PUBMED:28116710"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_69"
      name "PMID:20689271"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re306"
      uniprot "NA"
    ]
    graphics [
      x 1636.833317928823
      y 192.44941879580188
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000130234;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:ACE2"
      map_id "M121_206"
      name "ACE2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa398"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1570.5839300187831
      y 383.2788911324337
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_206"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc:3530;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000131187;urn:miriam:ncbigene:2161;urn:miriam:ncbigene:2161;urn:miriam:refseq:NM_000505;urn:miriam:hgnc.symbol:F12;urn:miriam:uniprot:P00748;urn:miriam:uniprot:P00748;urn:miriam:hgnc.symbol:F12;urn:miriam:ec-code:3.4.21.38"
      hgnc "HGNC_SYMBOL:F12"
      map_id "M121_134"
      name "F12"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa170"
      uniprot "UNIPROT:P00748"
    ]
    graphics [
      x 1261.6175091381122
      y 1555.4721815721732
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_134"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      annotation "PUBMED:7391081;PUBMED:864009"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_26"
      name "PMID:7391081"
      node_subtype "TRUNCATION"
      node_type "reaction"
      org_id "re159"
      uniprot "NA"
    ]
    graphics [
      x 1105.8925493557606
      y 1464.9377122766482
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D010446"
      hgnc "NA"
      map_id "M121_135"
      name "Small_space_peptide"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa171"
      uniprot "NA"
    ]
    graphics [
      x 1338.654286068951
      y 1447.6767607586035
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_135"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc:14388;urn:miriam:pubmed:19296670;urn:miriam:obo.go:GO%3A0005577;urn:miriam:hgnc:3661;urn:miriam:hgnc.symbol:FGA;urn:miriam:refseq:NM_000508;urn:miriam:hgnc.symbol:FGA;urn:miriam:ncbigene:2243;urn:miriam:uniprot:P02671;urn:miriam:uniprot:P02671;urn:miriam:ncbigene:2243;urn:miriam:ensembl:ENSG00000171560;urn:miriam:hgnc:3662;urn:miriam:hgnc.symbol:FGB;urn:miriam:uniprot:P02675;urn:miriam:uniprot:P02675;urn:miriam:hgnc.symbol:FGB;urn:miriam:ensembl:ENSG00000171564;urn:miriam:refseq:NM_005141;urn:miriam:ncbigene:2244;urn:miriam:ncbigene:2244;urn:miriam:uniprot:P02679;urn:miriam:uniprot:P02679;urn:miriam:hgnc:3694;urn:miriam:ensembl:ENSG00000171557;urn:miriam:refseq:NM_021870;urn:miriam:hgnc.symbol:FGG;urn:miriam:hgnc.symbol:FGG;urn:miriam:ncbigene:2266;urn:miriam:ncbigene:2266;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:GP6;urn:miriam:hgnc.symbol:GP6;urn:miriam:hgnc:14388;urn:miriam:ensembl:ENSG00000088053;urn:miriam:ncbigene:51206;urn:miriam:ncbigene:51206;urn:miriam:refseq:NM_001083899;urn:miriam:uniprot:Q9HCN6;urn:miriam:uniprot:Q9HCN6"
      hgnc "HGNC_SYMBOL:FGA;HGNC_SYMBOL:FGB;HGNC_SYMBOL:FGG;HGNC_SYMBOL:GP6"
      map_id "M121_21"
      name "Fibrinogen:GP6"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa42"
      uniprot "UNIPROT:P02671;UNIPROT:P02675;UNIPROT:P02679;UNIPROT:Q9HCN6"
    ]
    graphics [
      x 1467.1628492937618
      y 1528.2646164041444
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      annotation "PUBMED:29472360"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_101"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re356"
      uniprot "NA"
    ]
    graphics [
      x 1592.1319989812437
      y 1346.6944155958658
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:obo.go:GO%3A0030168"
      hgnc "NA"
      map_id "M121_221"
      name "platelet_space_activation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa430"
      uniprot "NA"
    ]
    graphics [
      x 1398.039242593168
      y 1152.7417219333258
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_221"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc:1331;urn:miriam:hgnc.symbol:C5;urn:miriam:hgnc.symbol:C5;urn:miriam:refseq:NM_001735;urn:miriam:ensembl:ENSG00000106804;urn:miriam:uniprot:P01031;urn:miriam:uniprot:P01031;urn:miriam:ncbigene:727;urn:miriam:ncbigene:727"
      hgnc "HGNC_SYMBOL:C5"
      map_id "M121_181"
      name "C5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa314"
      uniprot "UNIPROT:P01031"
    ]
    graphics [
      x 943.0965228537077
      y 687.3354475543855
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_181"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      annotation "PUBMED:30083158;PUBMED:12878586"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_51"
      name "PMID:30083158, PMID: 12878586"
      node_subtype "TRUNCATION"
      node_type "reaction"
      org_id "re262"
      uniprot "NA"
    ]
    graphics [
      x 945.7059848426211
      y 810.9582463902667
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D051574;urn:miriam:taxonomy:9606;urn:miriam:brenda:3.4.21.43;urn:miriam:mesh:D050678;urn:miriam:hgnc:1324;urn:miriam:refseq:NM_001002029;urn:miriam:ensembl:ENSG00000224389;urn:miriam:taxonomy:9606;urn:miriam:ncbigene:100293534;urn:miriam:hgnc.symbol:C4B;urn:miriam:ncbigene:721;urn:miriam:hgnc.symbol:C4B;urn:miriam:hgnc:1324;urn:miriam:uniprot:P0C0L5;urn:miriam:uniprot:P0C0L5;urn:miriam:hgnc.symbol:C2;urn:miriam:uniprot:P06681;urn:miriam:uniprot:P06681;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000166278;urn:miriam:ec-code:3.4.21.43;urn:miriam:refseq:NM_000063;urn:miriam:mesh:D050678;urn:miriam:hgnc:1248;urn:miriam:ncbigene:717;urn:miriam:ncbigene:717"
      hgnc "HGNC_SYMBOL:C4B;HGNC_SYMBOL:C2"
      map_id "M121_15"
      name "C2a:C4b"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa35"
      uniprot "UNIPROT:P0C0L5;UNIPROT:P06681"
    ]
    graphics [
      x 1210.3632481299453
      y 695.6072037263596
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc:1331;urn:miriam:hgnc.symbol:C5;urn:miriam:mesh:D050776;urn:miriam:refseq:NM_001735;urn:miriam:ensembl:ENSG00000106804;urn:miriam:uniprot:P01031;urn:miriam:uniprot:P01031;urn:miriam:ncbigene:727;urn:miriam:ncbigene:727"
      hgnc "HGNC_SYMBOL:C5"
      map_id "M121_182"
      name "C5b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa315"
      uniprot "UNIPROT:P01031"
    ]
    graphics [
      x 1160.2634253832866
      y 1154.8324167950075
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_182"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc:1331;urn:miriam:hgnc.symbol:C5;urn:miriam:refseq:NM_001735;urn:miriam:mesh:D015936;urn:miriam:ensembl:ENSG00000106804;urn:miriam:uniprot:P01031;urn:miriam:uniprot:P01031;urn:miriam:ncbigene:727;urn:miriam:ncbigene:727"
      hgnc "HGNC_SYMBOL:C5"
      map_id "M121_174"
      name "C5a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa253"
      uniprot "UNIPROT:P01031"
    ]
    graphics [
      x 632.9550212647637
      y 875.4962145177299
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_174"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000149257;urn:miriam:refseq:NM_004353;urn:miriam:hgnc.symbol:SERPINH1;urn:miriam:hgnc.symbol:SERPINH1;urn:miriam:hgnc:1546;urn:miriam:ncbigene:871;urn:miriam:ncbigene:871;urn:miriam:uniprot:P50454;urn:miriam:uniprot:P50454"
      hgnc "HGNC_SYMBOL:SERPINH1"
      map_id "M121_165"
      name "TAFI"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa226"
      uniprot "UNIPROT:P50454"
    ]
    graphics [
      x 481.6452780407121
      y 811.6094258438634
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_165"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      annotation "PUBMED:23809134"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_38"
      name "PMID:23809134"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re181"
      uniprot "NA"
    ]
    graphics [
      x 464.34353846556905
      y 983.352807761565
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000149257;urn:miriam:refseq:NM_004353;urn:miriam:hgnc.symbol:SERPINH1;urn:miriam:hgnc.symbol:SERPINH1;urn:miriam:hgnc:1546;urn:miriam:ncbigene:871;urn:miriam:ncbigene:871;urn:miriam:uniprot:P50454;urn:miriam:uniprot:P50454"
      hgnc "HGNC_SYMBOL:SERPINH1"
      map_id "M121_166"
      name "TAFI"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa227"
      uniprot "UNIPROT:P50454"
    ]
    graphics [
      x 642.693369298845
      y 969.2724886132685
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_166"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A27584"
      hgnc "NA"
      map_id "M121_260"
      name "aldosterone"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa509"
      uniprot "NA"
    ]
    graphics [
      x 1229.4179981597372
      y 181.86553234771168
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_260"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      annotation "PUBMED:27045029"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_126"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re394"
      uniprot "NA"
    ]
    graphics [
      x 987.7676253351904
      y 145.55667295054138
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_126"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0001974"
      hgnc "NA"
      map_id "M121_262"
      name "vascular_space_remodeling"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa518"
      uniprot "NA"
    ]
    graphics [
      x 812.5518387252157
      y 212.51039668053647
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_262"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc:3662;urn:miriam:taxonomy:9606;urn:miriam:hgnc:3661;urn:miriam:hgnc:3694;urn:miriam:pubmed:19296670;urn:miriam:obo.go:GO%3A0005577;urn:miriam:hgnc:3661;urn:miriam:hgnc.symbol:FGA;urn:miriam:refseq:NM_000508;urn:miriam:hgnc.symbol:FGA;urn:miriam:ncbigene:2243;urn:miriam:uniprot:P02671;urn:miriam:uniprot:P02671;urn:miriam:ncbigene:2243;urn:miriam:ensembl:ENSG00000171560;urn:miriam:uniprot:P02679;urn:miriam:uniprot:P02679;urn:miriam:hgnc:3694;urn:miriam:ensembl:ENSG00000171557;urn:miriam:refseq:NM_021870;urn:miriam:hgnc.symbol:FGG;urn:miriam:hgnc.symbol:FGG;urn:miriam:ncbigene:2266;urn:miriam:ncbigene:2266;urn:miriam:hgnc:3662;urn:miriam:hgnc.symbol:FGB;urn:miriam:uniprot:P02675;urn:miriam:uniprot:P02675;urn:miriam:hgnc.symbol:FGB;urn:miriam:ensembl:ENSG00000171564;urn:miriam:refseq:NM_005141;urn:miriam:ncbigene:2244;urn:miriam:ncbigene:2244"
      hgnc "HGNC_SYMBOL:FGA;HGNC_SYMBOL:FGG;HGNC_SYMBOL:FGB"
      map_id "M121_6"
      name "Fibrinogen"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa17"
      uniprot "UNIPROT:P02671;UNIPROT:P02679;UNIPROT:P02675"
    ]
    graphics [
      x 1023.5522764187673
      y 1375.384937295389
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      annotation "PUBMED:28228446;PUBMED:6282863;PUBMED:2117226"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_46"
      name "PMID:28228446, PMID: 6282863, PMID:2117226 "
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re234"
      uniprot "NA"
    ]
    graphics [
      x 849.789391369304
      y 1426.2383954222087
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D013917;urn:miriam:uniprot:P00734;urn:miriam:uniprot:P00734;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.21.5;urn:miriam:hgnc:3535;urn:miriam:refseq:NM_000506;urn:miriam:ensembl:ENSG00000180210;urn:miriam:hgnc.symbol:F2;urn:miriam:ncbigene:2147;urn:miriam:ncbigene:2147"
      hgnc "HGNC_SYMBOL:F2"
      map_id "M121_252"
      name "Thrombin"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa498"
      uniprot "UNIPROT:P00734"
    ]
    graphics [
      x 888.6000092429832
      y 1557.424790849408
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_252"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:C011468;urn:miriam:taxonomy:9606"
      hgnc "NA"
      map_id "M121_204"
      name "Fibrin_space_monomer"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa395"
      uniprot "NA"
    ]
    graphics [
      x 1092.8873115052106
      y 1549.8482420183877
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_204"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc:1352;urn:miriam:hgnc:1353;urn:miriam:pubmed:28630159;urn:miriam:hgnc:1354;urn:miriam:mesh:C042295;urn:miriam:mesh:D050776;urn:miriam:hgnc:1339;urn:miriam:refseq:NM_000066;urn:miriam:hgnc.symbol:C8B;urn:miriam:hgnc.symbol:C8B;urn:miriam:hgnc:1353;urn:miriam:ncbigene:732;urn:miriam:ensembl:ENSG00000021852;urn:miriam:ncbigene:732;urn:miriam:uniprot:P07358;urn:miriam:uniprot:P07358;urn:miriam:ensembl:ENSG00000176919;urn:miriam:refseq:NM_000606;urn:miriam:hgnc:1354;urn:miriam:hgnc.symbol:C8G;urn:miriam:ncbigene:733;urn:miriam:hgnc.symbol:C8G;urn:miriam:ncbigene:733;urn:miriam:uniprot:P07360;urn:miriam:uniprot:P07360;urn:miriam:hgnc.symbol:C8A;urn:miriam:refseq:NM_000562;urn:miriam:hgnc.symbol:C8A;urn:miriam:hgnc:1352;urn:miriam:uniprot:P07357;urn:miriam:uniprot:P07357;urn:miriam:ncbigene:731;urn:miriam:ncbigene:731;urn:miriam:ensembl:ENSG00000157131;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:C6;urn:miriam:hgnc.symbol:C6;urn:miriam:refseq:NM_000065;urn:miriam:ensembl:ENSG00000039537;urn:miriam:uniprot:P13671;urn:miriam:uniprot:P13671;urn:miriam:hgnc:1339;urn:miriam:ncbigene:729;urn:miriam:ncbigene:729;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1331;urn:miriam:hgnc.symbol:C5;urn:miriam:mesh:D050776;urn:miriam:refseq:NM_001735;urn:miriam:ensembl:ENSG00000106804;urn:miriam:uniprot:P01031;urn:miriam:uniprot:P01031;urn:miriam:ncbigene:727;urn:miriam:ncbigene:727;urn:miriam:uniprot:P10643;urn:miriam:uniprot:P10643;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000112936;urn:miriam:hgnc.symbol:C7;urn:miriam:ncbigene:730;urn:miriam:hgnc.symbol:C7;urn:miriam:ncbigene:730;urn:miriam:hgnc:1346;urn:miriam:refseq:NM_000587"
      hgnc "HGNC_SYMBOL:C8B;HGNC_SYMBOL:C8G;HGNC_SYMBOL:C8A;HGNC_SYMBOL:C6;HGNC_SYMBOL:C5;HGNC_SYMBOL:C7"
      map_id "M121_13"
      name "C5b:C6:C7:C8A:C8B:C8G"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa31"
      uniprot "UNIPROT:P07358;UNIPROT:P07360;UNIPROT:P07357;UNIPROT:P13671;UNIPROT:P01031;UNIPROT:P10643"
    ]
    graphics [
      x 1150.3566507376145
      y 946.9343544022579
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      annotation "PUBMED:6796960"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_55"
      name "PMID:6796960"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re267"
      uniprot "NA"
    ]
    graphics [
      x 1259.0220528321047
      y 662.3535806046746
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:ncbigene:735;urn:miriam:ncbigene:735;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1358;urn:miriam:ensembl:ENSG00000113600;urn:miriam:hgnc.symbol:C9;urn:miriam:hgnc.symbol:C9;urn:miriam:refseq:NM_001737;urn:miriam:uniprot:P02748;urn:miriam:uniprot:P02748"
      hgnc "HGNC_SYMBOL:C9"
      map_id "M121_185"
      name "C9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa337"
      uniprot "UNIPROT:P02748"
    ]
    graphics [
      x 1175.7898313730855
      y 538.567508405958
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_185"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_235"
      name "s538"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa461"
      uniprot "NA"
    ]
    graphics [
      x 552.9687111348085
      y 549.9221282901107
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_235"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_86"
      name "PMCID:PMC7260598"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re334"
      uniprot "NA"
    ]
    graphics [
      x 636.6066081751936
      y 679.9725437329878
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32299776;urn:miriam:taxonomy:9606;urn:miriam:mesh:D018366"
      hgnc "NA"
      map_id "M121_201"
      name "C5b_minus_9_space_deposition"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa390"
      uniprot "NA"
    ]
    graphics [
      x 2012.6346857677217
      y 870.728713981093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_201"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      annotation "PUBMED:19362461"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_104"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re359"
      uniprot "NA"
    ]
    graphics [
      x 2132.283555645624
      y 1016.3938829052636
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:obo.go:GO%3A0030168"
      hgnc "NA"
      map_id "M121_212"
      name "platelet_space_aggregation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa409"
      uniprot "NA"
    ]
    graphics [
      x 2117.3973035152317
      y 1167.308293039963
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_212"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:refseq:NM_001002029;urn:miriam:ensembl:ENSG00000224389;urn:miriam:taxonomy:9606;urn:miriam:ncbigene:100293534;urn:miriam:hgnc.symbol:C4b;urn:miriam:mesh:C032261;urn:miriam:ncbigene:721;urn:miriam:hgnc.symbol:C4B;urn:miriam:hgnc:1324;urn:miriam:uniprot:P0C0L5;urn:miriam:uniprot:P0C0L5"
      hgnc "HGNC_SYMBOL:C4b;HGNC_SYMBOL:C4B"
      map_id "M121_198"
      name "C4d"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa387"
      uniprot "UNIPROT:P0C0L5"
    ]
    graphics [
      x 1374.3973885841713
      y 1346.951335767683
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_198"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      annotation "PUBMED:25573909"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_91"
      name "PMID:25573909"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re340"
      uniprot "NA"
    ]
    graphics [
      x 1222.8177714287312
      y 1437.326489561167
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A27584"
      hgnc "NA"
      map_id "M121_256"
      name "aldosterone"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa503"
      uniprot "NA"
    ]
    graphics [
      x 1696.2537293338667
      y 452.40940679664527
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_256"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      annotation "PUBMED:8202152"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_119"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re379"
      uniprot "NA"
    ]
    graphics [
      x 1474.9920105691845
      y 280.2139095058943
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_249"
      name "s586"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa495"
      uniprot "NA"
    ]
    graphics [
      x 1199.493399728152
      y 910.2089659920551
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_249"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      annotation "PUBMED:9012652"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_92"
      name "PMID:9012652"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re341"
      uniprot "NA"
    ]
    graphics [
      x 1172.5816918736843
      y 767.2642099509915
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32278764;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_228"
      name "s534"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa454"
      uniprot "NA"
    ]
    graphics [
      x 1187.2553452374846
      y 487.806405409091
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_228"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      annotation "PUBMED:32286245"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_85"
      name "PMID:32286245"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re333"
      uniprot "NA"
    ]
    graphics [
      x 1051.4185189949321
      y 591.4839817972191
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:2504360"
      hgnc "NA"
      map_id "M121_217"
      name "cytokine_space_storm"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa416"
      uniprot "NA"
    ]
    graphics [
      x 944.9598216498757
      y 613.7560198840974
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_217"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:ncbigene:1401;urn:miriam:ncbigene:1401;urn:miriam:taxonomy:9606;urn:miriam:hgnc:2367;urn:miriam:hgnc.symbol:CRP;urn:miriam:uniprot:P02741;urn:miriam:uniprot:P02741;urn:miriam:hgnc.symbol:CRP;urn:miriam:ensembl:ENSG00000132693;urn:miriam:refseq:NM_000567"
      hgnc "HGNC_SYMBOL:CRP"
      map_id "M121_213"
      name "CRP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa410"
      uniprot "UNIPROT:P02741"
    ]
    graphics [
      x 837.4024653973936
      y 1214.8745320002365
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_213"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      annotation "PUBMED:9490235"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_75"
      name "PMID:9490235"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re320"
      uniprot "NA"
    ]
    graphics [
      x 987.9879437103374
      y 1295.1368648205516
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_266"
      name "s618"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa526"
      uniprot "NA"
    ]
    graphics [
      x 1496.8015505704727
      y 700.6942476483357
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_266"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      annotation "PUBMED:5932931"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_123"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re391"
      uniprot "NA"
    ]
    graphics [
      x 1673.361693370478
      y 677.6523896706739
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_123"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      annotation "PUBMED:7944388"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_28"
      name "PMID:7944388"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re163"
      uniprot "NA"
    ]
    graphics [
      x 1032.7335680852384
      y 916.3318178702674
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:KNG1;urn:miriam:hgnc.symbol:KNG1;urn:miriam:hgnc.symbol:6383;urn:miriam:ncbigene:3827;urn:miriam:ncbigene:3827;urn:miriam:uniprot:P01042;urn:miriam:uniprot:P01042;urn:miriam:refseq:NM_001102416;urn:miriam:hgnc:6383;urn:miriam:ensembl:ENSG00000113889"
      hgnc "HGNC_SYMBOL:KNG1;HGNC_SYMBOL:6383"
      map_id "M121_133"
      name "KNG1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa169"
      uniprot "UNIPROT:P01042"
    ]
    graphics [
      x 1007.5195004390051
      y 764.0741006801022
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_133"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:17598838;urn:miriam:taxonomy:9606;urn:miriam:intact:EBI-10087151;urn:miriam:hgnc:6371;urn:miriam:hgnc:6383;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:KNG1;urn:miriam:hgnc.symbol:KNG1;urn:miriam:hgnc.symbol:6383;urn:miriam:ncbigene:3827;urn:miriam:ncbigene:3827;urn:miriam:uniprot:P01042;urn:miriam:uniprot:P01042;urn:miriam:refseq:NM_001102416;urn:miriam:hgnc:6383;urn:miriam:ensembl:ENSG00000113889;urn:miriam:refseq:NM_000892;urn:miriam:ensembl:ENSG00000164344;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.21.34;urn:miriam:uniprot:P03952;urn:miriam:uniprot:P03952;urn:miriam:hgnc:6371;urn:miriam:ncbigene:3818;urn:miriam:ncbigene:3818;urn:miriam:hgnc.symbol:KLKB1;urn:miriam:hgnc.symbol:KLKB1"
      hgnc "HGNC_SYMBOL:KNG1;HGNC_SYMBOL:6383;HGNC_SYMBOL:KLKB1"
      map_id "M121_1"
      name "KNG1:KLKB1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa11"
      uniprot "UNIPROT:P01042;UNIPROT:P03952"
    ]
    graphics [
      x 1112.0845743915052
      y 846.6756457503586
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      annotation "PUBMED:27045029"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_127"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re395"
      uniprot "NA"
    ]
    graphics [
      x 1199.9397357993214
      y 302.0715058276817
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_127"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_269"
      name "vascular_space_inflammation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa530"
      uniprot "NA"
    ]
    graphics [
      x 1258.4115443488101
      y 486.0361811779981
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_269"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32299776;urn:miriam:taxonomy:9606;urn:miriam:mesh:D018366"
      hgnc "NA"
      map_id "M121_199"
      name "C4d_space_deposition"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa388"
      uniprot "NA"
    ]
    graphics [
      x 968.3439981628491
      y 1789.8686882136233
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_199"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      annotation "PUBMED:19362461"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_106"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re361"
      uniprot "NA"
    ]
    graphics [
      x 830.5060695295158
      y 1896.9197769042303
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:mesh:D007681"
      hgnc "NA"
      map_id "M121_200"
      name "septal_space_capillary_space_necrosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa389"
      uniprot "NA"
    ]
    graphics [
      x 726.1246614481759
      y 1972.0162798539554
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_200"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      annotation "PUBMED:19362461"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_64"
      name "PMID:32299776"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re292"
      uniprot "NA"
    ]
    graphics [
      x 1744.808842223394
      y 720.1756048506588
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:27077125;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_237"
      name "s546"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa467"
      uniprot "NA"
    ]
    graphics [
      x 363.9666031932006
      y 1276.210439643506
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_237"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      annotation "PUBMED:27077125"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_94"
      name "PMID:27077125"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re343"
      uniprot "NA"
    ]
    graphics [
      x 469.9501700152798
      y 1141.253628910375
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc.symbol:PLG;urn:miriam:ec-code:3.4.21.7;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000122194;urn:miriam:ncbigene:5340;urn:miriam:ncbigene:5340;urn:miriam:hgnc:9071;urn:miriam:refseq:NM_000301;urn:miriam:mesh:D005341;urn:miriam:brenda:3.4.21.7;urn:miriam:uniprot:P00747;urn:miriam:uniprot:P00747"
      hgnc "HGNC_SYMBOL:PLG"
      map_id "M121_238"
      name "Plasmin"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa468"
      uniprot "UNIPROT:P00747"
    ]
    graphics [
      x 423.3599400736018
      y 1300.6951919336962
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_238"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:ec-code:3.4.21.69;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000115718;urn:miriam:hgnc:9451;urn:miriam:refseq:NM_000312;urn:miriam:uniprot:P04070;urn:miriam:uniprot:P04070;urn:miriam:hgnc.symbol:PROC;urn:miriam:ncbigene:5624;urn:miriam:hgnc.symbol:PROC;urn:miriam:ncbigene:5624"
      hgnc "HGNC_SYMBOL:PROC"
      map_id "M121_244"
      name "PROC"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa482"
      uniprot "UNIPROT:P04070"
    ]
    graphics [
      x 1092.6013923286657
      y 1155.5528558158214
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_244"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      annotation "PUBMED:32302438"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_77"
      name "PMID:32302438"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re323"
      uniprot "NA"
    ]
    graphics [
      x 938.5741094603527
      y 1027.5565866590716
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      annotation "PUBMED:284414"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_54"
      name "PMID:284414"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re266"
      uniprot "NA"
    ]
    graphics [
      x 1157.3025789445019
      y 1262.6966027388203
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc:1352;urn:miriam:hgnc:1353;urn:miriam:hgnc:1354;urn:miriam:mesh:D003185;urn:miriam:ensembl:ENSG00000176919;urn:miriam:refseq:NM_000606;urn:miriam:hgnc:1354;urn:miriam:hgnc.symbol:C8G;urn:miriam:ncbigene:733;urn:miriam:hgnc.symbol:C8G;urn:miriam:ncbigene:733;urn:miriam:uniprot:P07360;urn:miriam:uniprot:P07360;urn:miriam:hgnc.symbol:C8A;urn:miriam:refseq:NM_000562;urn:miriam:hgnc.symbol:C8A;urn:miriam:hgnc:1352;urn:miriam:uniprot:P07357;urn:miriam:uniprot:P07357;urn:miriam:ncbigene:731;urn:miriam:ncbigene:731;urn:miriam:ensembl:ENSG00000157131;urn:miriam:refseq:NM_000066;urn:miriam:hgnc.symbol:C8B;urn:miriam:hgnc.symbol:C8B;urn:miriam:hgnc:1353;urn:miriam:ncbigene:732;urn:miriam:ensembl:ENSG00000021852;urn:miriam:ncbigene:732;urn:miriam:uniprot:P07358;urn:miriam:uniprot:P07358"
      hgnc "HGNC_SYMBOL:C8G;HGNC_SYMBOL:C8A;HGNC_SYMBOL:C8B"
      map_id "M121_12"
      name "C8A:C8B:C8G"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa30"
      uniprot "UNIPROT:P07360;UNIPROT:P07357;UNIPROT:P07358"
    ]
    graphics [
      x 1056.8879008428758
      y 1292.35710261937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc:6137;urn:miriam:pubmed:25051961;urn:miriam:taxonomy:10090;urn:miriam:hgnc:14338;urn:miriam:hgnc:6153;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:GP6;urn:miriam:hgnc.symbol:GP6;urn:miriam:hgnc:14388;urn:miriam:ensembl:ENSG00000088053;urn:miriam:ncbigene:51206;urn:miriam:ncbigene:51206;urn:miriam:refseq:NM_001083899;urn:miriam:uniprot:Q9HCN6;urn:miriam:uniprot:Q9HCN6;urn:miriam:hgnc:6137;urn:miriam:refseq:NM_002203;urn:miriam:ncbigene:3673;urn:miriam:ensembl:ENSG00000164171;urn:miriam:uniprot:P17301;urn:miriam:uniprot:P17301;urn:miriam:ncbigene:3673;urn:miriam:hgnc.symbol:ITGA2;urn:miriam:hgnc.symbol:ITGA2;urn:miriam:refseq:NM_002211;urn:miriam:uniprot:P05556;urn:miriam:uniprot:P05556;urn:miriam:hgnc.symbol:ITGB1;urn:miriam:hgnc.symbol:ITGB1;urn:miriam:ncbigene:3688;urn:miriam:ncbigene:3688;urn:miriam:hgnc:6153;urn:miriam:ensembl:ENSG00000150093"
      hgnc "HGNC_SYMBOL:GP6;HGNC_SYMBOL:ITGA2;HGNC_SYMBOL:ITGB1"
      map_id "M121_18"
      name "GP6:alpha2:beta1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa39"
      uniprot "UNIPROT:Q9HCN6;UNIPROT:P17301;UNIPROT:P05556"
    ]
    graphics [
      x 783.098889876043
      y 1371.0635883226619
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      annotation "PUBMED:25051961"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_103"
      name "PMID:25051961"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re358"
      uniprot "NA"
    ]
    graphics [
      x 749.2300796963573
      y 1059.4457557865326
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc:12726;urn:miriam:taxonomy:9606;urn:miriam:refseq:NM_000552;urn:miriam:uniprot:P04275;urn:miriam:uniprot:P04275;urn:miriam:ncbigene:7450;urn:miriam:ncbigene:7450;urn:miriam:hgnc.symbol:VWF;urn:miriam:hgnc.symbol:VWF;urn:miriam:ensembl:ENSG00000110799"
      hgnc "HGNC_SYMBOL:VWF"
      map_id "M121_214"
      name "VWF"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa411"
      uniprot "UNIPROT:P04275"
    ]
    graphics [
      x 747.3193024439248
      y 921.221878152793
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_214"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:1489668;urn:miriam:taxonomy:2697049;urn:miriam:uniprot:P59594;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:S"
      map_id "M121_202"
      name "S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa391"
      uniprot "UNIPROT:P0DTC2;UNIPROT:P59594"
    ]
    graphics [
      x 1646.8621205967697
      y 781.1157752857863
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_202"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      annotation "PUBMED:32299776"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_71"
      name "PMID:32299776"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re308"
      uniprot "NA"
    ]
    graphics [
      x 1549.2703932652566
      y 1092.4322627966521
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32278764;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_229"
      name "s534"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa455"
      uniprot "NA"
    ]
    graphics [
      x 1238.7813348006882
      y 598.0144141286951
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_229"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      annotation "PUBMED:32504360"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_95"
      name "PMID:32504360"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re344"
      uniprot "NA"
    ]
    graphics [
      x 1104.5121027565629
      y 717.1937921049721
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      annotation "PUBMED:4627469;PUBMED:6768384"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_42"
      name "PMID:6768384, PMID:4627469"
      node_subtype "TRUNCATION"
      node_type "reaction"
      org_id "re202"
      uniprot "NA"
    ]
    graphics [
      x 1191.89774392151
      y 1033.417661194638
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D019679;urn:miriam:hgnc.symbol:KNG1;urn:miriam:taxonomy:9606;urn:miriam:ncbigene:3827;urn:miriam:ncbigene:3827;urn:miriam:uniprot:P01042;urn:miriam:uniprot:P01042;urn:miriam:refseq:NM_001102416;urn:miriam:hgnc:6383;urn:miriam:ensembl:ENSG00000113889"
      hgnc "HGNC_SYMBOL:KNG1"
      map_id "M121_171"
      name "Kininogen"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa250"
      uniprot "UNIPROT:P01042"
    ]
    graphics [
      x 1265.9597928172425
      y 925.1875616502585
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_171"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:ec-code:3.4.21.6;urn:miriam:taxonomy:9606;urn:miriam:uniprot:P00742;urn:miriam:uniprot:P00742;urn:miriam:mesh:D015951;urn:miriam:refseq:NM_000504;urn:miriam:ensembl:ENSG00000126218;urn:miriam:hgnc.symbol:F10;urn:miriam:brenda:3.4.21.6;urn:miriam:hgnc:3528;urn:miriam:ncbigene:2159;urn:miriam:ncbigene:2159"
      hgnc "HGNC_SYMBOL:F10"
      map_id "M121_140"
      name "F10a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa176"
      uniprot "UNIPROT:P00742"
    ]
    graphics [
      x 242.71697341336358
      y 1142.5554999678661
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_140"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      annotation "PUBMED:2303476"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_32"
      name "PMID:2303476"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re172"
      uniprot "NA"
    ]
    graphics [
      x 236.37107130979382
      y 1371.992007614705
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc.symbol:F5;urn:miriam:uniprot:P12259;urn:miriam:uniprot:P12259;urn:miriam:taxonomy:9606;urn:miriam:hgnc:3542;urn:miriam:mesh:D015943;urn:miriam:refseq:NM_000130;urn:miriam:ncbigene:2153;urn:miriam:ncbigene:2153;urn:miriam:ensembl:ENSG00000198734"
      hgnc "HGNC_SYMBOL:F5"
      map_id "M121_154"
      name "F5a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa201"
      uniprot "UNIPROT:P12259"
    ]
    graphics [
      x 368.3716178489134
      y 1541.0806708982034
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_154"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:mesh:D015951;urn:miriam:pubmed:2303476;urn:miriam:mesh:D15943;urn:miriam:mesh:C022475;urn:miriam:ec-code:3.4.21.6;urn:miriam:taxonomy:9606;urn:miriam:uniprot:P00742;urn:miriam:uniprot:P00742;urn:miriam:mesh:D015951;urn:miriam:refseq:NM_000504;urn:miriam:ensembl:ENSG00000126218;urn:miriam:hgnc.symbol:F10;urn:miriam:brenda:3.4.21.6;urn:miriam:hgnc:3528;urn:miriam:ncbigene:2159;urn:miriam:ncbigene:2159;urn:miriam:hgnc.symbol:F5;urn:miriam:uniprot:P12259;urn:miriam:uniprot:P12259;urn:miriam:taxonomy:9606;urn:miriam:hgnc:3542;urn:miriam:mesh:D015943;urn:miriam:refseq:NM_000130;urn:miriam:ncbigene:2153;urn:miriam:ncbigene:2153;urn:miriam:ensembl:ENSG00000198734"
      hgnc "HGNC_SYMBOL:F10;HGNC_SYMBOL:F5"
      map_id "M121_3"
      name "F5a:F10a"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa13"
      uniprot "UNIPROT:P00742;UNIPROT:P12259"
    ]
    graphics [
      x 129.60001486306305
      y 1200.6096927523704
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      annotation "PUBMED:21304106;PUBMED:8631976"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_27"
      name "PMID:21304106, PMID:8631976"
      node_subtype "TRUNCATION"
      node_type "reaction"
      org_id "re160"
      uniprot "NA"
    ]
    graphics [
      x 558.6530866811847
      y 1292.1434085789983
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:ncbigene:2160;urn:miriam:ncbigene:2160;urn:miriam:uniprot:P03951;urn:miriam:uniprot:P03951;urn:miriam:ensembl:ENSG00000088926;urn:miriam:hgnc.symbol:F11;urn:miriam:hgnc.symbol:F11;urn:miriam:hgnc:3529;urn:miriam:ec-code:3.4.21.27;urn:miriam:refseq:NM_000128"
      hgnc "HGNC_SYMBOL:F11"
      map_id "M121_136"
      name "F11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa172"
      uniprot "UNIPROT:P03951"
    ]
    graphics [
      x 646.6420181607962
      y 1418.235713246676
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_136"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc:336;urn:miriam:refseq:NM_000685;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:uniprot:P30556;urn:miriam:uniprot:P30556;urn:miriam:ensembl:ENSG00000144891;urn:miriam:ncbigene:185;urn:miriam:ncbigene:185"
      hgnc "HGNC_SYMBOL:AGTR1"
      map_id "M121_254"
      name "AGTR1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa500"
      uniprot "UNIPROT:P30556"
    ]
    graphics [
      x 1858.026930079898
      y 1191.827511274278
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_254"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      annotation "PUBMED:8158359"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_110"
      name "PMID:8158359"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re366"
      uniprot "NA"
    ]
    graphics [
      x 1723.2089926487567
      y 1168.4745861609713
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc:336;urn:miriam:refseq:NM_000685;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:uniprot:P30556;urn:miriam:uniprot:P30556;urn:miriam:ensembl:ENSG00000144891;urn:miriam:ncbigene:185;urn:miriam:ncbigene:185"
      hgnc "HGNC_SYMBOL:AGTR1"
      map_id "M121_246"
      name "AGTR1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa484"
      uniprot "UNIPROT:P30556"
    ]
    graphics [
      x 1561.3195378872485
      y 1376.8179481543568
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_246"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32278764;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_230"
      name "s534"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa456"
      uniprot "NA"
    ]
    graphics [
      x 679.4764118510345
      y 1039.5084085642784
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_230"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      annotation "PUBMED:32171076"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_73"
      name "PMID:32171076"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re315"
      uniprot "NA"
    ]
    graphics [
      x 812.1745949291409
      y 991.7532269194857
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:GP6;urn:miriam:hgnc.symbol:GP6;urn:miriam:hgnc:14388;urn:miriam:ensembl:ENSG00000088053;urn:miriam:ncbigene:51206;urn:miriam:ncbigene:51206;urn:miriam:refseq:NM_001083899;urn:miriam:uniprot:Q9HCN6;urn:miriam:uniprot:Q9HCN6"
      hgnc "HGNC_SYMBOL:GP6"
      map_id "M121_222"
      name "GP6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa431"
      uniprot "UNIPROT:Q9HCN6"
    ]
    graphics [
      x 1050.7352220428481
      y 1721.572208803912
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_222"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      annotation "PUBMED:25051961"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_102"
      name "PMID:25051961"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re357"
      uniprot "NA"
    ]
    graphics [
      x 889.1200962141613
      y 1640.276763100113
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc:6137;urn:miriam:taxonomy:9606;urn:miriam:intact:EBI-16428357;urn:miriam:hgnc:6153;urn:miriam:hgnc:6137;urn:miriam:refseq:NM_002203;urn:miriam:ncbigene:3673;urn:miriam:ensembl:ENSG00000164171;urn:miriam:uniprot:P17301;urn:miriam:uniprot:P17301;urn:miriam:ncbigene:3673;urn:miriam:hgnc.symbol:ITGA2;urn:miriam:hgnc.symbol:ITGA2;urn:miriam:refseq:NM_002211;urn:miriam:uniprot:P05556;urn:miriam:uniprot:P05556;urn:miriam:hgnc.symbol:ITGB1;urn:miriam:hgnc.symbol:ITGB1;urn:miriam:ncbigene:3688;urn:miriam:ncbigene:3688;urn:miriam:hgnc:6153;urn:miriam:ensembl:ENSG00000150093"
      hgnc "HGNC_SYMBOL:ITGA2;HGNC_SYMBOL:ITGB1"
      map_id "M121_20"
      name "ITGA2:ITGAB1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa41"
      uniprot "UNIPROT:P17301;UNIPROT:P05556"
    ]
    graphics [
      x 788.8950074943652
      y 1671.772528641397
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D062106;urn:miriam:doi:10.1101/2020.04.25.200"
      hgnc "NA"
      map_id "M121_240"
      name "s552"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa478"
      uniprot "NA"
    ]
    graphics [
      x 123.98806617252853
      y 1102.1103799995842
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_240"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_81"
      name "DOI:10.1101/2020.04.25.20077842"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re328"
      uniprot "NA"
    ]
    graphics [
      x 289.466056947118
      y 1100.697950883301
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:uniprot:P05121;urn:miriam:uniprot:P05121;urn:miriam:ncbigene:5054;urn:miriam:ncbigene:5054;urn:miriam:ensembl:ENSG00000106366;urn:miriam:hgnc:8593;urn:miriam:refseq:NM_000602;urn:miriam:hgnc.symbol:SERPINE1;urn:miriam:hgnc.symbol:SERPINE1;urn:miriam:hgnc:8583"
      hgnc "HGNC_SYMBOL:SERPINE1"
      map_id "M121_258"
      name "SERPINE1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa505"
      uniprot "UNIPROT:P05121"
    ]
    graphics [
      x 1594.8863091689266
      y 516.0986303896377
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_258"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      annotation "PUBMED:20591974;PUBMED:8034668;PUBMED:11983698;PUBMED:2091055"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_118"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re377"
      uniprot "NA"
    ]
    graphics [
      x 1496.123418597869
      y 602.3486240405696
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_118"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc:336;urn:miriam:refseq:NM_000685;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:uniprot:P30556;urn:miriam:uniprot:P30556;urn:miriam:ensembl:ENSG00000144891;urn:miriam:ncbigene:185;urn:miriam:ncbigene:185"
      hgnc "HGNC_SYMBOL:AGTR1"
      map_id "M121_263"
      name "AGTR1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa519"
      uniprot "UNIPROT:P30556"
    ]
    graphics [
      x 1531.7516984333224
      y 481.7719813816226
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_263"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    cd19dm [
      annotation "PUBMED:19362461"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_63"
      name "PMID: 32299776"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re289"
      uniprot "NA"
    ]
    graphics [
      x 1142.050826813297
      y 1621.8730129929827
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:2697049;urn:miriam:mesh:D012327"
      hgnc "NA"
      map_id "M121_242"
      name "SARS_minus_CoV_minus_2_space_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa480"
      uniprot "NA"
    ]
    graphics [
      x 1744.6128517082252
      y 986.7333413360225
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_242"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    cd19dm [
      annotation "PUBMED:32525548"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_128"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re396"
      uniprot "NA"
    ]
    graphics [
      x 1878.020053592184
      y 740.9782854861297
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_128"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:20689271;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_255"
      name "s86"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa501"
      uniprot "NA"
    ]
    graphics [
      x 1408.676034547405
      y 1050.214810718484
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_255"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 158
    zlevel -1

    cd19dm [
      annotation "PUBMED:32048163"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_116"
      name "PMID:32048163"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re375"
      uniprot "NA"
    ]
    graphics [
      x 1585.1984466223444
      y 1005.4517747720728
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 159
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D013917;urn:miriam:uniprot:P00734;urn:miriam:uniprot:P00734;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.21.5;urn:miriam:hgnc:3535;urn:miriam:refseq:NM_000506;urn:miriam:ensembl:ENSG00000180210;urn:miriam:hgnc.symbol:F2;urn:miriam:ncbigene:2147;urn:miriam:ncbigene:2147"
      hgnc "HGNC_SYMBOL:F2"
      map_id "M121_156"
      name "Thrombin"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa203"
      uniprot "UNIPROT:P00734"
    ]
    graphics [
      x 243.88779300765873
      y 1048.099042098836
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_156"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 160
    zlevel -1

    cd19dm [
      annotation "PUBMED:579490"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_34"
      name "PMID:579490"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re174"
      uniprot "NA"
    ]
    graphics [
      x 176.65517335330856
      y 1161.6220792523752
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 161
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P01008;urn:miriam:uniprot:P01008;urn:miriam:hgnc:775;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000117601;urn:miriam:ncbigene:462;urn:miriam:ncbigene:462;urn:miriam:refseq:NM_000488;urn:miriam:hgnc.symbol:SERPINC1;urn:miriam:hgnc.symbol:SERPINC1"
      hgnc "HGNC_SYMBOL:SERPINC1"
      map_id "M121_155"
      name "Antithrombin"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa202"
      uniprot "UNIPROT:P01008"
    ]
    graphics [
      x 101.06112655291463
      y 944.7217068275538
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_155"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 162
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A28304;urn:miriam:taxonomy:9606;urn:miriam:pubmed:708377"
      hgnc "NA"
      map_id "M121_186"
      name "Heparin"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa355"
      uniprot "NA"
    ]
    graphics [
      x 90.8077444178374
      y 1286.1646296764698
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_186"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 163
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:RPS3AP29;urn:miriam:refseq:NG_011230;urn:miriam:ensembl:ENSG00000237818;urn:miriam:hgnc:35531;urn:miriam:ncbigene:730861"
      hgnc "HGNC_SYMBOL:RPS3AP29"
      map_id "M121_138"
      name "F9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa174"
      uniprot "NA"
    ]
    graphics [
      x 391.0090762381526
      y 606.6077524567186
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_138"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 164
    zlevel -1

    cd19dm [
      annotation "PUBMED:9100000"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_29"
      name "PMID:9100000"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re169"
      uniprot "NA"
    ]
    graphics [
      x 354.13488921246847
      y 766.4526878254716
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 165
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:refseq:NM_000133;urn:miriam:taxonomy:9606;urn:miriam:hgnc:3551;urn:miriam:ensembl:ENSG00000101981;urn:miriam:ec-code:3.4.21.22;urn:miriam:uniprot:P00740;urn:miriam:uniprot:P00740;urn:miriam:hgnc.symbol:F9;urn:miriam:mesh:D015949;urn:miriam:ncbigene:2158;urn:miriam:ncbigene:2158"
      hgnc "HGNC_SYMBOL:F9"
      map_id "M121_139"
      name "F9a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa175"
      uniprot "UNIPROT:P00740"
    ]
    graphics [
      x 187.61979470652238
      y 686.9125901162972
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_139"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 166
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:ec-code:3.4.21.6;urn:miriam:taxonomy:9606;urn:miriam:uniprot:P00742;urn:miriam:uniprot:P00742;urn:miriam:refseq:NM_000504;urn:miriam:ensembl:ENSG00000126218;urn:miriam:hgnc.symbol:F10;urn:miriam:hgnc.symbol:F10;urn:miriam:hgnc:3528;urn:miriam:ncbigene:2159;urn:miriam:ncbigene:2159"
      hgnc "HGNC_SYMBOL:F10"
      map_id "M121_141"
      name "F10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa177"
      uniprot "UNIPROT:P00742"
    ]
    graphics [
      x 129.87090893855225
      y 759.5686649161041
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_141"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 167
    zlevel -1

    cd19dm [
      annotation "PUBMED:15853774;PUBMED:11551226"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_24"
      name "PMID:11551226"
      node_subtype "TRUNCATION"
      node_type "reaction"
      org_id "re155"
      uniprot "NA"
    ]
    graphics [
      x 178.05181344734012
      y 863.4903183251474
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 168
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D010446"
      hgnc "NA"
      map_id "M121_142"
      name "Small_space_peptide"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa178"
      uniprot "NA"
    ]
    graphics [
      x 238.12074350835303
      y 733.1493021671646
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_142"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 169
    zlevel -1

    cd19dm [
      annotation "PUBMED:32048163"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_40"
      name "PMID:32048163"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re183"
      uniprot "NA"
    ]
    graphics [
      x 1488.5985913469399
      y 1050.8154391604558
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 170
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc:336;urn:miriam:refseq:NM_000685;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:hgnc.symbol:AGTR1;urn:miriam:uniprot:P30556;urn:miriam:uniprot:P30556;urn:miriam:ensembl:ENSG00000144891;urn:miriam:ncbigene:185;urn:miriam:ncbigene:185"
      hgnc "HGNC_SYMBOL:AGTR1"
      map_id "M121_264"
      name "AGTR1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa520"
      uniprot "UNIPROT:P30556"
    ]
    graphics [
      x 1260.7228793314027
      y 753.1239874520627
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_264"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 171
    zlevel -1

    cd19dm [
      annotation "PUBMED:11983698"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_121"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re386"
      uniprot "NA"
    ]
    graphics [
      x 1441.4425200591488
      y 682.1136658009927
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_121"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 172
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:refseq:NM_000584;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000169429;urn:miriam:hgnc:6025;urn:miriam:hgnc.symbol:CXCL8;urn:miriam:hgnc.symbol:CXCL8;urn:miriam:uniprot:P10145;urn:miriam:uniprot:P10145;urn:miriam:ncbigene:3576;urn:miriam:ncbigene:3576"
      hgnc "HGNC_SYMBOL:CXCL8"
      map_id "M121_177"
      name "IL8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa304"
      uniprot "UNIPROT:P10145"
    ]
    graphics [
      x 1072.5546789085947
      y 1091.5133076008292
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_177"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 173
    zlevel -1

    cd19dm [
      annotation "PUBMED:27561337"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_48"
      name "PMID:27561337"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re259"
      uniprot "NA"
    ]
    graphics [
      x 1085.1766201728205
      y 1352.3063418926004
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 174
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P00734;urn:miriam:uniprot:P00734;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.21.5;urn:miriam:hgnc:3535;urn:miriam:refseq:NM_000506;urn:miriam:ensembl:ENSG00000180210;urn:miriam:hgnc.symbol:F2;urn:miriam:hgnc.symbol:F2;urn:miriam:ncbigene:2147;urn:miriam:ncbigene:2147"
      hgnc "HGNC_SYMBOL:F2"
      map_id "M121_146"
      name "Prothrombin"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa182"
      uniprot "UNIPROT:P00734"
    ]
    graphics [
      x 309.5218587047332
      y 943.0306001696212
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_146"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 175
    zlevel -1

    cd19dm [
      annotation "PUBMED:4430674;PUBMED:3818642"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_33"
      name "PMID:4430674,PMID:3818642"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re173"
      uniprot "NA"
    ]
    graphics [
      x 174.05453205605988
      y 1027.2506497338704
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 176
    zlevel -1

    cd19dm [
      annotation "PUBMED:29096812;PUBMED:7577232"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_96"
      name "PMID:29096812, PMID:7577232"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re345"
      uniprot "NA"
    ]
    graphics [
      x 1319.4142748811596
      y 1554.881890541932
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 177
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D026122;urn:miriam:taxonomy:9606;urn:miriam:brenda:2.3.2.13;urn:miriam:hgnc.symbol:F13"
      hgnc "HGNC_SYMBOL:F13"
      map_id "M121_220"
      name "F13a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa425"
      uniprot "NA"
    ]
    graphics [
      x 1467.0314632297518
      y 1618.980643216477
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_220"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 178
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:mesh:C465961"
      hgnc "NA"
      map_id "M121_219"
      name "Fibrin_space_polymer"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa424"
      uniprot "NA"
    ]
    graphics [
      x 1119.4671359303898
      y 1335.158000125984
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_219"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 179
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:20689271;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_257"
      name "s86"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa504"
      uniprot "NA"
    ]
    graphics [
      x 1913.5789829626551
      y 605.6319655209455
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_257"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 180
    zlevel -1

    cd19dm [
      annotation "PUBMED:173529;PUBMED:8404594;PUBMED:32565254"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_117"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re376"
      uniprot "NA"
    ]
    graphics [
      x 1803.6810597917497
      y 647.214193402497
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 181
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A3892;urn:miriam:hgnc:9201"
      hgnc "NA"
      map_id "M121_267"
      name "ACTH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa527"
      uniprot "NA"
    ]
    graphics [
      x 1846.0569993531844
      y 526.3946459557213
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_267"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 182
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:taxonomy:9606;urn:miriam:hgnc:13557;urn:miriam:taxonomy:2697049;urn:miriam:pdb:6CS2;urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:1489668;urn:miriam:taxonomy:2697049;urn:miriam:uniprot:P59594;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S;urn:miriam:ensembl:ENSG00000130234;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:S;HGNC_SYMBOL:ACE2"
      map_id "M121_16"
      name "ACE2:Spike"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa37"
      uniprot "UNIPROT:P0DTC2;UNIPROT:P59594;UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1753.712327999626
      y 808.1576838341521
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 183
    zlevel -1

    cd19dm [
      annotation "PUBMED:32142651"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_114"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re373"
      uniprot "NA"
    ]
    graphics [
      x 1743.2107625415924
      y 1063.2078302807022
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 184
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:2697049;urn:miriam:mesh:D012327"
      hgnc "NA"
      map_id "M121_247"
      name "SARS_minus_CoV_minus_2_space_viral_space_entry"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa485"
      uniprot "NA"
    ]
    graphics [
      x 1666.5723034028583
      y 1246.967293663328
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_247"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 185
    zlevel -1

    cd19dm [
      annotation "PUBMED:10807586"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_105"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re360"
      uniprot "NA"
    ]
    graphics [
      x 2113.218549747161
      y 818.9346644520425
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 186
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:obo.go:GO%3A0006915"
      hgnc "NA"
      map_id "M121_211"
      name "apoptosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa408"
      uniprot "NA"
    ]
    graphics [
      x 1981.4882836714155
      y 808.3696979047813
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_211"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 187
    zlevel -1

    cd19dm [
      annotation "PUBMED:25573909"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_93"
      name "PMID:25573909"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re342"
      uniprot "NA"
    ]
    graphics [
      x 1336.3194365795348
      y 912.3430553423057
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 188
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32278764;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_226"
      name "s534"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa452"
      uniprot "NA"
    ]
    graphics [
      x 992.0424938648673
      y 983.1883532634259
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_226"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 189
    zlevel -1

    cd19dm [
      annotation "PUBMED:32286245"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_87"
      name "PMID:32286245"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re336"
      uniprot "NA"
    ]
    graphics [
      x 1004.6801340804831
      y 833.0160211862842
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 190
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc:3541;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:F3;urn:miriam:refseq:NM_001993;urn:miriam:hgnc.symbol:F3;urn:miriam:ncbigene:2152;urn:miriam:ncbigene:2152;urn:miriam:ensembl:ENSG00000117525;urn:miriam:uniprot:P13726;urn:miriam:uniprot:P13726"
      hgnc "HGNC_SYMBOL:F3"
      map_id "M121_153"
      name "F5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa200"
      uniprot "UNIPROT:P13726"
    ]
    graphics [
      x 695.9760887515632
      y 1701.8519287637432
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_153"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 191
    zlevel -1

    cd19dm [
      annotation "PUBMED:2322551;PUBMED:6572921;PUBMED:6282863"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_31"
      name "PMID:2322551, PMID:6282863, PMID:6572921"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re171"
      uniprot "NA"
    ]
    graphics [
      x 590.2849154751982
      y 1549.422421633517
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 192
    zlevel -1

    cd19dm [
      annotation "PUBMED:32252108"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_130"
      name "NA"
      node_subtype "MODULATION"
      node_type "reaction"
      org_id "re399"
      uniprot "NA"
    ]
    graphics [
      x 1852.550068618847
      y 373.47799066076607
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_130"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 193
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:uniprot:P01589;urn:miriam:uniprot:P01589;urn:miriam:ncbigene:3559;urn:miriam:ncbigene:3559;urn:miriam:hgnc:6008;urn:miriam:hgnc.symbol:IL2RA;urn:miriam:refseq:NM_000417;urn:miriam:hgnc.symbol:IL2RA;urn:miriam:ensembl:ENSG00000134460"
      hgnc "HGNC_SYMBOL:IL2RA"
      map_id "M121_178"
      name "IL2RA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa306"
      uniprot "UNIPROT:P01589"
    ]
    graphics [
      x 860.0473699150662
      y 864.9033642395806
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_178"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 194
    zlevel -1

    cd19dm [
      annotation "PUBMED:20483636"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_49"
      name "PMID:20483636"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re260"
      uniprot "NA"
    ]
    graphics [
      x 1008.8653311657416
      y 1100.7740811044132
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 195
    zlevel -1

    cd19dm [
      annotation "PUBMED:22471307"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_30"
      name "PMID:22471307"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re170"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 829.7334841017062
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 196
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:22471307;urn:miriam:intact:EBI-11621595;urn:miriam:taxonomy:9606;urn:miriam:hgnc:3546;urn:miriam:hgnc:35531;urn:miriam:refseq:NM_000132;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:F8;urn:miriam:mesh:D015944;urn:miriam:hgnc:3546;urn:miriam:uniprot:P00451;urn:miriam:uniprot:P00451;urn:miriam:ncbigene:2157;urn:miriam:ncbigene:2157;urn:miriam:ensembl:ENSG00000185010;urn:miriam:refseq:NM_000133;urn:miriam:taxonomy:9606;urn:miriam:hgnc:3551;urn:miriam:ensembl:ENSG00000101981;urn:miriam:ec-code:3.4.21.22;urn:miriam:uniprot:P00740;urn:miriam:uniprot:P00740;urn:miriam:hgnc.symbol:F9;urn:miriam:mesh:D015949;urn:miriam:ncbigene:2158;urn:miriam:ncbigene:2158"
      hgnc "HGNC_SYMBOL:F8;HGNC_SYMBOL:F9"
      map_id "M121_2"
      name "F8:F9"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa12"
      uniprot "UNIPROT:P00451;UNIPROT:P00740"
    ]
    graphics [
      x 107.14462007391603
      y 980.4357321098335
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 197
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:refseq:NM_001002029;urn:miriam:ensembl:ENSG00000224389;urn:miriam:taxonomy:9606;urn:miriam:ncbigene:100293534;urn:miriam:hgnc.symbol:C4B;urn:miriam:ncbigene:721;urn:miriam:hgnc.symbol:C4B;urn:miriam:hgnc:1324;urn:miriam:uniprot:P0C0L5;urn:miriam:uniprot:P0C0L5"
      hgnc "HGNC_SYMBOL:C4B"
      map_id "M121_193"
      name "C4b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa365"
      uniprot "UNIPROT:P0C0L5"
    ]
    graphics [
      x 1598.5222418844235
      y 1208.0562693641252
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_193"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 198
    zlevel -1

    cd19dm [
      annotation "PUBMED:26521297"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_60"
      name "PMID:26521297"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re278"
      uniprot "NA"
    ]
    graphics [
      x 1427.0759234473076
      y 944.5426914049281
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 199
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc.symbol:C2;urn:miriam:uniprot:P06681;urn:miriam:uniprot:P06681;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000166278;urn:miriam:ec-code:3.4.21.43;urn:miriam:refseq:NM_000063;urn:miriam:mesh:D050678;urn:miriam:hgnc:1248;urn:miriam:ncbigene:717;urn:miriam:ncbigene:717"
      hgnc "HGNC_SYMBOL:C2"
      map_id "M121_195"
      name "C2a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa367"
      uniprot "UNIPROT:P06681"
    ]
    graphics [
      x 1465.616905109181
      y 843.8459263131674
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_195"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 200
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000127241;urn:miriam:ncbigene:5648;urn:miriam:ncbigene:5648;urn:miriam:hgnc.symbol:MASP1;urn:miriam:refseq:NM_001879;urn:miriam:hgnc.symbol:MASP1;urn:miriam:hgnc:6901;urn:miriam:ec-code:3.4.21.-;urn:miriam:uniprot:P48740;urn:miriam:uniprot:P48740"
      hgnc "HGNC_SYMBOL:MASP1"
      map_id "M121_232"
      name "MASP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa458"
      uniprot "UNIPROT:P48740"
    ]
    graphics [
      x 612.2079390595104
      y 1134.4776497499026
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_232"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 201
    zlevel -1

    cd19dm [
      annotation "PUBMED:11290788"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_58"
      name "PMID:11290788"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re275"
      uniprot "NA"
    ]
    graphics [
      x 750.7798324018421
      y 1118.732601992024
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 202
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:O00187;urn:miriam:uniprot:O00187;urn:miriam:taxonomy:9606;urn:miriam:refseq:NM_006610;urn:miriam:ensembl:ENSG00000009724;urn:miriam:ec-code:3.4.21.104;urn:miriam:hgnc:6902;urn:miriam:hgnc.symbol:MASP2;urn:miriam:ncbigene:10747;urn:miriam:hgnc.symbol:MASP2;urn:miriam:ncbigene:10747"
      hgnc "HGNC_SYMBOL:MASP2"
      map_id "M121_190"
      name "MBL2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa362"
      uniprot "UNIPROT:O00187"
    ]
    graphics [
      x 645.2655535371134
      y 1194.9549707522795
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_190"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 203
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000127241;urn:miriam:ncbigene:5648;urn:miriam:ncbigene:5648;urn:miriam:hgnc.symbol:MASP1;urn:miriam:refseq:NM_001879;urn:miriam:hgnc.symbol:MASP1;urn:miriam:hgnc:6901;urn:miriam:ec-code:3.4.21.-;urn:miriam:uniprot:P48740;urn:miriam:uniprot:P48740"
      hgnc "HGNC_SYMBOL:MASP1"
      map_id "M121_189"
      name "MASP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa358"
      uniprot "UNIPROT:P48740"
    ]
    graphics [
      x 1054.2988368310835
      y 998.4515481172511
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_189"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 204
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc.symbol:C3;urn:miriam:hgnc.symbol:C3;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000125730;urn:miriam:refseq:NM_000064;urn:miriam:uniprot:P01024;urn:miriam:uniprot:P01024;urn:miriam:hgnc:1318;urn:miriam:ncbigene:718;urn:miriam:ncbigene:718"
      hgnc "HGNC_SYMBOL:C3"
      map_id "M121_173"
      name "C3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa252"
      uniprot "UNIPROT:P01024"
    ]
    graphics [
      x 1280.7063308555985
      y 234.39444901130366
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_173"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 205
    zlevel -1

    cd19dm [
      annotation "PUBMED:17395591;PUBMED:427127"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_50"
      name "PMID:427127, PMID:17395591"
      node_subtype "TRUNCATION"
      node_type "reaction"
      org_id "re261"
      uniprot "NA"
    ]
    graphics [
      x 1211.3427881228768
      y 359.3760865072684
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 206
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc.symbol:C3;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000125730;urn:miriam:refseq:NM_000064;urn:miriam:uniprot:P01024;urn:miriam:uniprot:P01024;urn:miriam:mesh:D015926;urn:miriam:hgnc:1318;urn:miriam:ncbigene:718;urn:miriam:ncbigene:718"
      hgnc "HGNC_SYMBOL:C3"
      map_id "M121_179"
      name "C3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa308"
      uniprot "UNIPROT:P01024"
    ]
    graphics [
      x 1108.7575924716036
      y 315.87339100570887
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_179"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 207
    zlevel -1

    cd19dm [
      annotation "PUBMED:3124286;PUBMED:3096399;PUBMED:12091055;PUBMED:10373228"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_37"
      name "PMID:10373228, PMID:3124286"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re180"
      uniprot "NA"
    ]
    graphics [
      x 1254.5786047789347
      y 848.7933010532187
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 208
    zlevel -1

    cd19dm [
      annotation "PUBMED:18026570;PUBMED:21789389"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_109"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re365"
      uniprot "NA"
    ]
    graphics [
      x 1253.5067811484314
      y 1361.551443459427
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 209
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D013917;urn:miriam:taxonomy:9606"
      hgnc "NA"
      map_id "M121_224"
      name "thrombus_space_formation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa441"
      uniprot "NA"
    ]
    graphics [
      x 1182.5337942402105
      y 1403.31741879469
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_224"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 210
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32278764;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_231"
      name "s534"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa457"
      uniprot "NA"
    ]
    graphics [
      x 462.2302464136086
      y 613.3603164113196
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_231"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 211
    zlevel -1

    cd19dm [
      annotation "PUBMED:32367170"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_65"
      name "PMID:32367170"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re295"
      uniprot "NA"
    ]
    graphics [
      x 547.3439108383234
      y 713.099710864899
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 212
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_251"
      name "s589"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa497"
      uniprot "NA"
    ]
    graphics [
      x 982.4396278303709
      y 945.6026781437054
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_251"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 213
    zlevel -1

    cd19dm [
      annotation "PUBMED:32172226"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_41"
      name "PMID:32172226"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re192"
      uniprot "NA"
    ]
    graphics [
      x 890.0595909136778
      y 1083.514096876655
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 214
    zlevel -1

    cd19dm [
      annotation "PUBMED:32275855"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_68"
      name "PMID:32275855"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re305"
      uniprot "NA"
    ]
    graphics [
      x 1683.224677213038
      y 591.8984972408739
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 215
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32299776;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_234"
      name "s537"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa460"
      uniprot "NA"
    ]
    graphics [
      x 1298.048756869825
      y 1421.843718347494
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_234"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 216
    zlevel -1

    cd19dm [
      annotation "PUBMED:32299776"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_79"
      name "PMID:32299776"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re325"
      uniprot "NA"
    ]
    graphics [
      x 1487.783991595389
      y 1303.9556714540738
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 217
    zlevel -1

    cd19dm [
      annotation "PUBMED:29096812;PUBMED:10574983;PUBMED:32172226"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_97"
      name "PMID:29096812, PMID:10574983, PMID:32172226"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re346"
      uniprot "NA"
    ]
    graphics [
      x 803.9586575308472
      y 1147.8021639852586
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 218
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:mesh:C036309;urn:miriam:pubmed:19008457"
      hgnc "NA"
      map_id "M121_159"
      name "D_minus_dimer"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa210"
      uniprot "NA"
    ]
    graphics [
      x 761.9821324778677
      y 985.7664778595191
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_159"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 219
    zlevel -1

    cd19dm [
      annotation "PUBMED:10969042"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_115"
      name "PMID:10749699"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re374"
      uniprot "NA"
    ]
    graphics [
      x 1836.2499911484638
      y 981.8535067114237
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 220
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:C079000;urn:miriam:taxonomy:9606"
      hgnc "NA"
      map_id "M121_210"
      name "Bradykinin(1_minus_5)"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa407"
      uniprot "NA"
    ]
    graphics [
      x 1953.6398503836347
      y 1057.7893812825127
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_210"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 221
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D010446"
      hgnc "NA"
      map_id "M121_205"
      name "Small_space_peptide"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa397"
      uniprot "NA"
    ]
    graphics [
      x 1974.3025210466126
      y 979.4610095229613
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_205"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 222
    zlevel -1

    cd19dm [
      annotation "PUBMED:25051961"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_99"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re353"
      uniprot "NA"
    ]
    graphics [
      x 1171.5084048039776
      y 880.2089659920551
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 223
    zlevel -1

    cd19dm [
      annotation "PUBMED:26709040"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_125"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re393"
      uniprot "NA"
    ]
    graphics [
      x 1649.1063510511126
      y 331.5705205720707
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_125"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 224
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:mesh:D013923;urn:miriam:mesh:D055806"
      hgnc "NA"
      map_id "M121_268"
      name "Thrombosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa529"
      uniprot "NA"
    ]
    graphics [
      x 1575.9673336818691
      y 415.9750878207716
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_268"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 225
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P01019;urn:miriam:uniprot:P01019;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:AGT;urn:miriam:hgnc.symbol:AGT;urn:miriam:ensembl:ENSG00000135744;urn:miriam:hgnc:333;urn:miriam:ncbigene:183;urn:miriam:ncbigene:183;urn:miriam:refseq:NM_000029"
      hgnc "HGNC_SYMBOL:AGT"
      map_id "M121_150"
      name "AGT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa196"
      uniprot "UNIPROT:P01019"
    ]
    graphics [
      x 761.8897691590464
      y 1452.4592690274662
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_150"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 226
    zlevel -1

    cd19dm [
      annotation "PUBMED:10585461;PUBMED:6172448;PUBMED:30934934"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_23"
      name "PMID:10585461"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re154"
      uniprot "NA"
    ]
    graphics [
      x 927.4727623674305
      y 1370.6262540250623
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 227
    zlevel -1

    cd19dm [
      annotation "PUBMED:23392115"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_70"
      name "PMID:23392115, PMID:32336612"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re307"
      uniprot "NA"
    ]
    graphics [
      x 1790.9375619181833
      y 867.6430670451612
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 228
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:ensembl:ENSG00000130234;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:ACE2"
      map_id "M121_203"
      name "ACE2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa394"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1695.8462640782332
      y 828.8653114780548
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_203"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 229
    zlevel -1

    cd19dm [
      annotation "PUBMED:19362461"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_62"
      name "PMID:19362461"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re288"
      uniprot "NA"
    ]
    graphics [
      x 1415.5221887139073
      y 1216.9016565014417
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 230
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:ec-code:3.4.21.46;urn:miriam:taxonomy:9606;urn:miriam:hgnc:2771;urn:miriam:hgnc.symbol:CFD;urn:miriam:hgnc.symbol:CFD;urn:miriam:refseq:NM_001928;urn:miriam:ensembl:ENSG00000197766;urn:miriam:uniprot:P00746;urn:miriam:uniprot:P00746;urn:miriam:ncbigene:1675;urn:miriam:ncbigene:1675"
      hgnc "HGNC_SYMBOL:CFD"
      map_id "M121_197"
      name "CFI"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa385"
      uniprot "UNIPROT:P00746"
    ]
    graphics [
      x 1258.790599536705
      y 1160.3959743876026
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_197"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 231
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:ncbigene:5328;urn:miriam:ncbigene:5328;urn:miriam:hgnc.symbol:PLAU;urn:miriam:hgnc.symbol:PLAU;urn:miriam:ec-code:3.4.21.73;urn:miriam:ensembl:ENSG00000122861;urn:miriam:hgnc:9052;urn:miriam:uniprot:P00749;urn:miriam:uniprot:P00749;urn:miriam:refseq:NM_002658"
      hgnc "HGNC_SYMBOL:PLAU"
      map_id "M121_187"
      name "PLAU"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa356"
      uniprot "UNIPROT:P00749"
    ]
    graphics [
      x 806.0764097024418
      y 426.5103536096632
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_187"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 232
    zlevel -1

    cd19dm [
      annotation "PUBMED:21199867"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_56"
      name "PMID:21199867"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re268"
      uniprot "NA"
    ]
    graphics [
      x 883.2901953788185
      y 565.5239832933489
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 233
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32278764;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_227"
      name "s534"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa453"
      uniprot "NA"
    ]
    graphics [
      x 628.3489536433999
      y 759.0258164321083
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_227"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 234
    zlevel -1

    cd19dm [
      annotation "PUBMED:32359396"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_84"
      name "PMID:32359396"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re332"
      uniprot "NA"
    ]
    graphics [
      x 763.0936973559755
      y 681.7352409661271
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 235
    zlevel -1

    cd19dm [
      annotation "PUBMED:16391415"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_111"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re369"
      uniprot "NA"
    ]
    graphics [
      x 1372.4777028074361
      y 1398.9844314542954
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 236
    zlevel -1

    cd19dm [
      annotation "PUBMED:32299776"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_72"
      name "PMID:32299776"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re309"
      uniprot "NA"
    ]
    graphics [
      x 1607.0969882130846
      y 621.5260767263436
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 237
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:obo.go:GO%3A0005579;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1352;urn:miriam:hgnc:1353;urn:miriam:hgnc:1354;urn:miriam:hgnc:1346;urn:miriam:mesh:D050776;urn:miriam:hgnc:1358;urn:miriam:mesh:D015938;urn:miriam:hgnc:1339;urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:1489668;urn:miriam:taxonomy:2697049;urn:miriam:uniprot:P59594;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S;urn:miriam:refseq:NM_000066;urn:miriam:hgnc.symbol:C8B;urn:miriam:hgnc.symbol:C8B;urn:miriam:hgnc:1353;urn:miriam:ncbigene:732;urn:miriam:ensembl:ENSG00000021852;urn:miriam:ncbigene:732;urn:miriam:uniprot:P07358;urn:miriam:uniprot:P07358;urn:miriam:ncbigene:735;urn:miriam:ncbigene:735;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1358;urn:miriam:ensembl:ENSG00000113600;urn:miriam:hgnc.symbol:C9;urn:miriam:hgnc.symbol:C9;urn:miriam:refseq:NM_001737;urn:miriam:uniprot:P02748;urn:miriam:uniprot:P02748;urn:miriam:ensembl:ENSG00000176919;urn:miriam:refseq:NM_000606;urn:miriam:hgnc:1354;urn:miriam:hgnc.symbol:C8G;urn:miriam:ncbigene:733;urn:miriam:hgnc.symbol:C8G;urn:miriam:ncbigene:733;urn:miriam:uniprot:P07360;urn:miriam:uniprot:P07360;urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:C6;urn:miriam:hgnc.symbol:C6;urn:miriam:refseq:NM_000065;urn:miriam:ensembl:ENSG00000039537;urn:miriam:uniprot:P13671;urn:miriam:uniprot:P13671;urn:miriam:hgnc:1339;urn:miriam:ncbigene:729;urn:miriam:ncbigene:729;urn:miriam:uniprot:P10643;urn:miriam:uniprot:P10643;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000112936;urn:miriam:hgnc.symbol:C7;urn:miriam:ncbigene:730;urn:miriam:hgnc.symbol:C7;urn:miriam:ncbigene:730;urn:miriam:hgnc:1346;urn:miriam:refseq:NM_000587;urn:miriam:hgnc.symbol:C8A;urn:miriam:refseq:NM_000562;urn:miriam:hgnc.symbol:C8A;urn:miriam:hgnc:1352;urn:miriam:uniprot:P07357;urn:miriam:uniprot:P07357;urn:miriam:ncbigene:731;urn:miriam:ncbigene:731;urn:miriam:ensembl:ENSG00000157131;urn:miriam:taxonomy:9606;urn:miriam:hgnc:1331;urn:miriam:hgnc.symbol:C5;urn:miriam:mesh:D050776;urn:miriam:refseq:NM_001735;urn:miriam:ensembl:ENSG00000106804;urn:miriam:uniprot:P01031;urn:miriam:uniprot:P01031;urn:miriam:ncbigene:727;urn:miriam:ncbigene:727"
      hgnc "HGNC_SYMBOL:S;HGNC_SYMBOL:C8B;HGNC_SYMBOL:C9;HGNC_SYMBOL:C8G;HGNC_SYMBOL:C6;HGNC_SYMBOL:C7;HGNC_SYMBOL:C8A;HGNC_SYMBOL:C5"
      map_id "M121_22"
      name "C5b_minus_9"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa43"
      uniprot "UNIPROT:P0DTC2;UNIPROT:P59594;UNIPROT:P07358;UNIPROT:P02748;UNIPROT:P07360;UNIPROT:P13671;UNIPROT:P10643;UNIPROT:P07357;UNIPROT:P01031"
    ]
    graphics [
      x 1739.1872193317795
      y 550.6759170271912
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 238
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc.symbol:NR3C2;urn:miriam:uniprot:P08235;urn:miriam:uniprot:P08235;urn:miriam:hgnc.symbol:NR3C2;urn:miriam:ncbigene:4306;urn:miriam:ncbigene:4306;urn:miriam:refseq:NM_000901;urn:miriam:hgnc:7979;urn:miriam:ensembl:ENSG00000151623"
      hgnc "HGNC_SYMBOL:NR3C2"
      map_id "M121_259"
      name "NR3C2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa506"
      uniprot "UNIPROT:P08235"
    ]
    graphics [
      x 1178.9745354291247
      y 617.5142692267458
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_259"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 239
    zlevel -1

    cd19dm [
      annotation "PUBMED:21349712;PUBMED:7045029"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_120"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re382"
      uniprot "NA"
    ]
    graphics [
      x 1323.6663496979263
      y 395.2133584588098
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 240
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc.symbol:NR3C2;urn:miriam:uniprot:P08235;urn:miriam:uniprot:P08235;urn:miriam:hgnc.symbol:NR3C2;urn:miriam:ncbigene:4306;urn:miriam:ncbigene:4306;urn:miriam:refseq:NM_000901;urn:miriam:hgnc:7979;urn:miriam:ensembl:ENSG00000151623"
      hgnc "HGNC_SYMBOL:NR3C2"
      map_id "M121_261"
      name "NR3C2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa516"
      uniprot "UNIPROT:P08235"
    ]
    graphics [
      x 1400.9356537048384
      y 240.43121300967164
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_261"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 241
    zlevel -1

    cd19dm [
      annotation "PUBMED:20975035"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_112"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re371"
      uniprot "NA"
    ]
    graphics [
      x 1451.303657281685
      y 1104.143608510837
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 242
    zlevel -1

    cd19dm [
      annotation "PUBMED:16008552"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_67"
      name "PMID:32336612, PMID:16008552"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re304"
      uniprot "NA"
    ]
    graphics [
      x 1573.3047715527932
      y 763.1095468567543
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 243
    zlevel -1

    cd19dm [
      annotation "PUBMED:5058233"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_52"
      name "PMID:5058233"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re263"
      uniprot "NA"
    ]
    graphics [
      x 1347.2720573495687
      y 1501.776019431562
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 244
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:C6;urn:miriam:hgnc.symbol:C6;urn:miriam:refseq:NM_000065;urn:miriam:ensembl:ENSG00000039537;urn:miriam:uniprot:P13671;urn:miriam:uniprot:P13671;urn:miriam:hgnc:1339;urn:miriam:ncbigene:729;urn:miriam:ncbigene:729"
      hgnc "HGNC_SYMBOL:C6"
      map_id "M121_183"
      name "C6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa318"
      uniprot "UNIPROT:P13671"
    ]
    graphics [
      x 1363.7744417928134
      y 1635.8740681854724
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_183"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 245
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc:12726;urn:miriam:taxonomy:9606;urn:miriam:refseq:NM_000552;urn:miriam:uniprot:P04275;urn:miriam:uniprot:P04275;urn:miriam:ncbigene:7450;urn:miriam:ncbigene:7450;urn:miriam:hgnc.symbol:VWF;urn:miriam:hgnc.symbol:VWF;urn:miriam:ensembl:ENSG00000110799"
      hgnc "HGNC_SYMBOL:VWF"
      map_id "M121_239"
      name "VWF"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa472"
      uniprot "UNIPROT:P04275"
    ]
    graphics [
      x 677.8654896366104
      y 577.3560563578487
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_239"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 246
    zlevel -1

    cd19dm [
      annotation "PUBMED:32367170"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_74"
      name "PMID:32367170"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re319"
      uniprot "NA"
    ]
    graphics [
      x 677.8070401484273
      y 781.2302742434899
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 247
    zlevel -1

    cd19dm [
      annotation "PUBMED:8136018"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_36"
      name "PMID: 8136018"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re179"
      uniprot "NA"
    ]
    graphics [
      x 217.49146096353263
      y 1241.8227663762673
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 248
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:2697049;urn:miriam:mesh:D012327"
      hgnc "NA"
      map_id "M121_243"
      name "SARS_minus_CoV_minus_2_space_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa481"
      uniprot "NA"
    ]
    graphics [
      x 344.1690270518685
      y 1457.5396933864927
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_243"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 249
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:mesh:D013917;urn:miriam:hgnc:775;urn:miriam:taxonomy:9606;urn:miriam:mesh:C046193;urn:miriam:pubmed:22930518;urn:miriam:mesh:D013917;urn:miriam:uniprot:P00734;urn:miriam:uniprot:P00734;urn:miriam:taxonomy:9606;urn:miriam:ec-code:3.4.21.5;urn:miriam:hgnc:3535;urn:miriam:refseq:NM_000506;urn:miriam:ensembl:ENSG00000180210;urn:miriam:hgnc.symbol:F2;urn:miriam:ncbigene:2147;urn:miriam:ncbigene:2147;urn:miriam:uniprot:P01008;urn:miriam:uniprot:P01008;urn:miriam:hgnc:775;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000117601;urn:miriam:ncbigene:462;urn:miriam:ncbigene:462;urn:miriam:refseq:NM_000488;urn:miriam:hgnc.symbol:SERPINC1;urn:miriam:hgnc.symbol:SERPINC1"
      hgnc "HGNC_SYMBOL:F2;HGNC_SYMBOL:SERPINC1"
      map_id "M121_4"
      name "TAT_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa14"
      uniprot "UNIPROT:P00734;UNIPROT:P01008"
    ]
    graphics [
      x 312.3734924093076
      y 1184.0070858933561
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 250
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:O00187;urn:miriam:uniprot:O00187;urn:miriam:taxonomy:9606;urn:miriam:refseq:NM_006610;urn:miriam:ensembl:ENSG00000009724;urn:miriam:ec-code:3.4.21.104;urn:miriam:hgnc:6902;urn:miriam:hgnc.symbol:MASP2;urn:miriam:ncbigene:10747;urn:miriam:hgnc.symbol:MASP2;urn:miriam:ncbigene:10747"
      hgnc "HGNC_SYMBOL:MASP2"
      map_id "M121_233"
      name "MASP2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa459"
      uniprot "UNIPROT:O00187"
    ]
    graphics [
      x 1955.9740188574394
      y 1450.454373516101
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_233"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 251
    zlevel -1

    cd19dm [
      annotation "PUBMED:32299776"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_90"
      name "PMID:32299776"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re339"
      uniprot "NA"
    ]
    graphics [
      x 2016.3700991382893
      y 1363.6960287388556
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 252
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32299776;urn:miriam:taxonomy:9606"
      hgnc "NA"
      map_id "M121_218"
      name "MASP2_space_deposition"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa417"
      uniprot "NA"
    ]
    graphics [
      x 1882.4076195260054
      y 1290.2982994578092
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_218"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 253
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc.symbol:C2;urn:miriam:uniprot:P06681;urn:miriam:uniprot:P06681;urn:miriam:hgnc.symbol:C2;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000166278;urn:miriam:ec-code:3.4.21.43;urn:miriam:refseq:NM_000063;urn:miriam:hgnc:1248;urn:miriam:ncbigene:717;urn:miriam:ncbigene:717"
      hgnc "HGNC_SYMBOL:C2"
      map_id "M121_194"
      name "C2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa366"
      uniprot "UNIPROT:P06681"
    ]
    graphics [
      x 1452.8331218500362
      y 779.8102593871461
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_194"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 254
    zlevel -1

    cd19dm [
      annotation "PUBMED:10946292"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_89"
      name "PMID:10946292"
      node_subtype "TRUNCATION"
      node_type "reaction"
      org_id "re338"
      uniprot "NA"
    ]
    graphics [
      x 1330.2905326821267
      y 837.6603343123566
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 255
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:hgnc.symbol:C2;urn:miriam:uniprot:P06681;urn:miriam:uniprot:P06681;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000166278;urn:miriam:ec-code:3.4.21.43;urn:miriam:refseq:NM_000063;urn:miriam:mesh:D050679;urn:miriam:hgnc:1248;urn:miriam:ncbigene:717;urn:miriam:ncbigene:717"
      hgnc "HGNC_SYMBOL:C2"
      map_id "M121_196"
      name "C2b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa368"
      uniprot "UNIPROT:P06681"
    ]
    graphics [
      x 1398.6316352338324
      y 757.4391339557912
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_196"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 256
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32278764;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_225"
      name "s534"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa451"
      uniprot "NA"
    ]
    graphics [
      x 758.2693771894633
      y 728.3454491162504
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_225"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 257
    zlevel -1

    cd19dm [
      annotation "PUBMED:32286245"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_88"
      name "PMID:32286245"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re337"
      uniprot "NA"
    ]
    graphics [
      x 807.0256579701346
      y 612.1168176791831
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 258
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:O00187;urn:miriam:uniprot:O00187;urn:miriam:taxonomy:9606;urn:miriam:refseq:NM_006610;urn:miriam:ensembl:ENSG00000009724;urn:miriam:ec-code:3.4.21.104;urn:miriam:hgnc:6902;urn:miriam:hgnc.symbol:MASP2;urn:miriam:ncbigene:10747;urn:miriam:hgnc.symbol:MASP2;urn:miriam:ncbigene:10747"
      hgnc "HGNC_SYMBOL:MASP2"
      map_id "M121_188"
      name "MASP2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa357"
      uniprot "UNIPROT:O00187"
    ]
    graphics [
      x 1733.616711356452
      y 1494.9114593611407
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_188"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 259
    zlevel -1

    cd19dm [
      annotation "PUBMED:32299776;PUBMED:11290788"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_57"
      name "PMID:11290788, PMID:32299776"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re274"
      uniprot "NA"
    ]
    graphics [
      x 1803.8404043644623
      y 1332.9629726119128
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 260
    zlevel -1

    cd19dm [
      annotation "PUBMED:25051961;PUBMED:19465929"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_100"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re355"
      uniprot "NA"
    ]
    graphics [
      x 1308.0321464240203
      y 1296.8036085904498
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 261
    zlevel -1

    cd19dm [
      annotation "PUBMED:29472360"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_107"
      name "PMID:29472360"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re363"
      uniprot "NA"
    ]
    graphics [
      x 1208.2110156038918
      y 1627.9591494990584
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 262
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:pubmed:32302438;urn:miriam:mesh:D062106"
      hgnc "NA"
      map_id "M121_241"
      name "s553"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa479"
      uniprot "NA"
    ]
    graphics [
      x 214.90726196117157
      y 791.8841934864704
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_241"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 263
    zlevel -1

    cd19dm [
      annotation "PUBMED:32302438"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_66"
      name "PMID:32302438"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re296"
      uniprot "NA"
    ]
    graphics [
      x 315.13297412629254
      y 881.2493718227506
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 264
    zlevel -1

    cd19dm [
      annotation "PUBMED:2437112"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_80"
      name "PMID:2437112, DOI:10.1101/2020.04.25.20077842"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re326"
      uniprot "NA"
    ]
    graphics [
      x 506.7810439369274
      y 1558.3326733025642
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 265
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:uniprot:P08697;urn:miriam:uniprot:P08697;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000167711;urn:miriam:ncbigene:5345;urn:miriam:ncbigene:5345;urn:miriam:hgnc:9075;urn:miriam:refseq:NM_000934;urn:miriam:hgnc.symbol:SERPINF2;urn:miriam:hgnc.symbol:SERPINF2"
      hgnc "HGNC_SYMBOL:SERPINF2"
      map_id "M121_215"
      name "SERPINF2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa412"
      uniprot "UNIPROT:P08697"
    ]
    graphics [
      x 573.6577156521209
      y 1688.6277522799892
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_215"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 266
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:2437112;urn:miriam:hgnc:9075;urn:miriam:mesh:D005341;urn:miriam:uniprot:P08697;urn:miriam:uniprot:P08697;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000167711;urn:miriam:ncbigene:5345;urn:miriam:ncbigene:5345;urn:miriam:hgnc:9075;urn:miriam:refseq:NM_000934;urn:miriam:hgnc.symbol:SERPINF2;urn:miriam:hgnc.symbol:SERPINF2;urn:miriam:hgnc.symbol:PLG;urn:miriam:ec-code:3.4.21.7;urn:miriam:taxonomy:9606;urn:miriam:ensembl:ENSG00000122194;urn:miriam:ncbigene:5340;urn:miriam:ncbigene:5340;urn:miriam:hgnc:9071;urn:miriam:refseq:NM_000301;urn:miriam:mesh:D005341;urn:miriam:brenda:3.4.21.7;urn:miriam:uniprot:P00747;urn:miriam:uniprot:P00747"
      hgnc "HGNC_SYMBOL:SERPINF2;HGNC_SYMBOL:PLG"
      map_id "M121_17"
      name "SERPINF2:Plasmin"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa38"
      uniprot "UNIPROT:P08697;UNIPROT:P00747"
    ]
    graphics [
      x 663.5326130527505
      y 1640.3863486710545
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 267
    zlevel -1

    cd19dm [
      annotation "PUBMED:3165516"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_124"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re392"
      uniprot "NA"
    ]
    graphics [
      x 1544.3197295744235
      y 236.0894090905946
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_124"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 268
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:mesh:D00318"
      hgnc "NA"
      map_id "M121_191"
      name "C4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa363"
      uniprot "NA"
    ]
    graphics [
      x 1896.7536338146251
      y 1495.303545828701
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_191"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 269
    zlevel -1

    cd19dm [
      annotation "PUBMED:21664989"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_59"
      name "PMID:22949645"
      node_subtype "TRUNCATION"
      node_type "reaction"
      org_id "re276"
      uniprot "NA"
    ]
    graphics [
      x 1804.3621087946947
      y 1440.6845667700607
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 270
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:hgnc.symbol:C4A;urn:miriam:ncbigene:720;urn:miriam:hgnc.symbol:C4A;urn:miriam:ncbigene:720;urn:miriam:hgnc:1323;urn:miriam:ensembl:ENSG00000244731;urn:miriam:uniprot:P0C0L4;urn:miriam:uniprot:P0C0L4;urn:miriam:refseq:NM_007293"
      hgnc "HGNC_SYMBOL:C4A"
      map_id "M121_192"
      name "C4a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa364"
      uniprot "UNIPROT:P0C0L4"
    ]
    graphics [
      x 1850.2151474445423
      y 1579.0803355060414
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_192"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 271
    zlevel -1

    cd19dm [
      annotation "PUBMED:3500650"
      count 1
      diagram "C19DMap:Coagulation pathway"
      full_annotation "NA"
      hgnc "NA"
      map_id "M121_76"
      name "PMID:3500650"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re321"
      uniprot "NA"
    ]
    graphics [
      x 948.8223374698249
      y 1105.4349301650404
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M121_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 272
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_180"
      target_id "M121_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 273
    source 3
    target 2
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_8"
      target_id "M121_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 274
    source 2
    target 4
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_61"
      target_id "M121_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 275
    source 5
    target 6
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_271"
      target_id "M121_129"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 276
    source 7
    target 6
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "M121_270"
      target_id "M121_129"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 277
    source 6
    target 8
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_129"
      target_id "M121_265"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 278
    source 9
    target 10
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_10"
      target_id "M121_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 279
    source 11
    target 10
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_184"
      target_id "M121_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 280
    source 10
    target 12
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_53"
      target_id "M121_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 281
    source 13
    target 14
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_236"
      target_id "M121_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 282
    source 15
    target 14
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "M121_158"
      target_id "M121_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 283
    source 14
    target 16
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_78"
      target_id "M121_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 284
    source 17
    target 18
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_160"
      target_id "M121_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 285
    source 19
    target 18
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_162"
      target_id "M121_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 286
    source 20
    target 18
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_167"
      target_id "M121_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 287
    source 21
    target 18
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_137"
      target_id "M121_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 288
    source 22
    target 18
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_132"
      target_id "M121_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 289
    source 18
    target 23
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_35"
      target_id "M121_161"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 290
    source 24
    target 25
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_170"
      target_id "M121_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 291
    source 25
    target 26
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_43"
      target_id "M121_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 292
    source 27
    target 28
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_216"
      target_id "M121_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 293
    source 29
    target 28
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_172"
      target_id "M121_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 294
    source 28
    target 30
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_83"
      target_id "M121_151"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 295
    source 31
    target 32
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_19"
      target_id "M121_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 296
    source 32
    target 33
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_98"
      target_id "M121_223"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 297
    source 34
    target 35
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_149"
      target_id "M121_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 298
    source 36
    target 35
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_152"
      target_id "M121_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 299
    source 35
    target 37
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_25"
      target_id "M121_148"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 300
    source 38
    target 39
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_168"
      target_id "M121_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 301
    source 39
    target 26
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_45"
      target_id "M121_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 302
    source 40
    target 41
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_145"
      target_id "M121_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 303
    source 42
    target 41
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_176"
      target_id "M121_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 304
    source 41
    target 43
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_47"
      target_id "M121_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 305
    source 44
    target 45
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_250"
      target_id "M121_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 306
    source 46
    target 45
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M121_208"
      target_id "M121_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 307
    source 45
    target 47
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_108"
      target_id "M121_245"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 308
    source 48
    target 49
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_163"
      target_id "M121_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 309
    source 50
    target 49
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_164"
      target_id "M121_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 310
    source 51
    target 49
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "M121_253"
      target_id "M121_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 311
    source 49
    target 52
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_39"
      target_id "M121_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 312
    source 53
    target 54
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_248"
      target_id "M121_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 313
    source 55
    target 54
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M121_209"
      target_id "M121_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 314
    source 54
    target 19
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_113"
      target_id "M121_162"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 315
    source 22
    target 56
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_132"
      target_id "M121_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 316
    source 57
    target 56
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_131"
      target_id "M121_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 317
    source 56
    target 29
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_82"
      target_id "M121_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 318
    source 58
    target 59
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_169"
      target_id "M121_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 319
    source 59
    target 26
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_44"
      target_id "M121_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 320
    source 60
    target 61
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_143"
      target_id "M121_122"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 321
    source 40
    target 61
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_145"
      target_id "M121_122"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 322
    source 62
    target 61
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "M121_157"
      target_id "M121_122"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 323
    source 61
    target 63
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_122"
      target_id "M121_147"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 324
    source 61
    target 64
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_122"
      target_id "M121_144"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 325
    source 65
    target 66
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_207"
      target_id "M121_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 326
    source 66
    target 67
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_69"
      target_id "M121_206"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 327
    source 68
    target 69
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_134"
      target_id "M121_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 328
    source 22
    target 69
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_132"
      target_id "M121_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 329
    source 69
    target 57
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_26"
      target_id "M121_131"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 330
    source 69
    target 70
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_26"
      target_id "M121_135"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 331
    source 71
    target 72
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_21"
      target_id "M121_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 332
    source 72
    target 73
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_101"
      target_id "M121_221"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 333
    source 74
    target 75
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_181"
      target_id "M121_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 334
    source 76
    target 75
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_15"
      target_id "M121_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 335
    source 4
    target 75
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_9"
      target_id "M121_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 336
    source 75
    target 77
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_51"
      target_id "M121_182"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 337
    source 75
    target 78
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_51"
      target_id "M121_174"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 338
    source 79
    target 80
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_165"
      target_id "M121_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 339
    source 40
    target 80
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_145"
      target_id "M121_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 340
    source 80
    target 81
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_38"
      target_id "M121_166"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 341
    source 82
    target 83
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_260"
      target_id "M121_126"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 342
    source 83
    target 84
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_126"
      target_id "M121_262"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 343
    source 85
    target 86
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_6"
      target_id "M121_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 344
    source 87
    target 86
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_252"
      target_id "M121_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 345
    source 43
    target 86
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "M121_7"
      target_id "M121_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 346
    source 51
    target 86
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "M121_253"
      target_id "M121_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 347
    source 86
    target 88
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_46"
      target_id "M121_204"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 348
    source 89
    target 90
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_13"
      target_id "M121_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 349
    source 91
    target 90
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_185"
      target_id "M121_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 350
    source 90
    target 16
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_55"
      target_id "M121_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 351
    source 92
    target 93
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_235"
      target_id "M121_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 352
    source 15
    target 93
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "M121_158"
      target_id "M121_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 353
    source 93
    target 78
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_86"
      target_id "M121_174"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 354
    source 94
    target 95
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_201"
      target_id "M121_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 355
    source 95
    target 96
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_104"
      target_id "M121_212"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 356
    source 97
    target 98
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_198"
      target_id "M121_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 357
    source 98
    target 26
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_91"
      target_id "M121_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 358
    source 99
    target 100
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_256"
      target_id "M121_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 359
    source 100
    target 82
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_119"
      target_id "M121_260"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 360
    source 101
    target 102
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_249"
      target_id "M121_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 361
    source 16
    target 102
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M121_14"
      target_id "M121_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 362
    source 102
    target 17
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_92"
      target_id "M121_160"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 363
    source 103
    target 104
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_228"
      target_id "M121_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 364
    source 15
    target 104
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M121_158"
      target_id "M121_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 365
    source 105
    target 104
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M121_217"
      target_id "M121_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 366
    source 104
    target 58
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_85"
      target_id "M121_169"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 367
    source 106
    target 107
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_213"
      target_id "M121_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 368
    source 107
    target 26
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_75"
      target_id "M121_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 369
    source 108
    target 109
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_266"
      target_id "M121_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 370
    source 99
    target 109
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_256"
      target_id "M121_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 371
    source 109
    target 36
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_123"
      target_id "M121_152"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 372
    source 22
    target 110
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_132"
      target_id "M121_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 373
    source 111
    target 110
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_133"
      target_id "M121_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 374
    source 110
    target 112
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_28"
      target_id "M121_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 375
    source 82
    target 113
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_260"
      target_id "M121_127"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 376
    source 113
    target 114
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_127"
      target_id "M121_269"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 377
    source 115
    target 116
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_199"
      target_id "M121_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 378
    source 116
    target 117
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_106"
      target_id "M121_200"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 379
    source 16
    target 118
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_14"
      target_id "M121_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 380
    source 118
    target 94
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_64"
      target_id "M121_201"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 381
    source 119
    target 120
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_237"
      target_id "M121_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 382
    source 121
    target 120
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "M121_238"
      target_id "M121_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 383
    source 120
    target 78
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_94"
      target_id "M121_174"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 384
    source 122
    target 123
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_244"
      target_id "M121_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 385
    source 15
    target 123
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "M121_158"
      target_id "M121_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 386
    source 123
    target 62
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_77"
      target_id "M121_157"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 387
    source 12
    target 124
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_11"
      target_id "M121_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 388
    source 125
    target 124
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_12"
      target_id "M121_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 389
    source 124
    target 89
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_54"
      target_id "M121_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 390
    source 126
    target 127
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_18"
      target_id "M121_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 391
    source 128
    target 127
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_214"
      target_id "M121_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 392
    source 127
    target 31
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_103"
      target_id "M121_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 393
    source 129
    target 130
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_202"
      target_id "M121_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 394
    source 130
    target 97
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_71"
      target_id "M121_198"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 395
    source 131
    target 132
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_229"
      target_id "M121_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 396
    source 15
    target 132
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M121_158"
      target_id "M121_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 397
    source 105
    target 132
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M121_217"
      target_id "M121_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 398
    source 132
    target 38
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_95"
      target_id "M121_168"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 399
    source 112
    target 133
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_1"
      target_id "M121_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 400
    source 133
    target 134
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_42"
      target_id "M121_171"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 401
    source 133
    target 55
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_42"
      target_id "M121_209"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 402
    source 133
    target 29
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_42"
      target_id "M121_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 403
    source 135
    target 136
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_140"
      target_id "M121_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 404
    source 137
    target 136
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_154"
      target_id "M121_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 405
    source 136
    target 138
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_32"
      target_id "M121_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 406
    source 57
    target 139
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_131"
      target_id "M121_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 407
    source 40
    target 139
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_145"
      target_id "M121_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 408
    source 139
    target 140
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_27"
      target_id "M121_136"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 409
    source 139
    target 21
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_27"
      target_id "M121_137"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 410
    source 141
    target 142
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_254"
      target_id "M121_110"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 411
    source 37
    target 142
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M121_148"
      target_id "M121_110"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 412
    source 142
    target 143
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_110"
      target_id "M121_246"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 413
    source 144
    target 145
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_230"
      target_id "M121_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 414
    source 15
    target 145
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "M121_158"
      target_id "M121_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 415
    source 145
    target 106
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_73"
      target_id "M121_213"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 416
    source 146
    target 147
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_222"
      target_id "M121_102"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 417
    source 148
    target 147
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_20"
      target_id "M121_102"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 418
    source 147
    target 126
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_102"
      target_id "M121_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 419
    source 149
    target 150
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_240"
      target_id "M121_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 420
    source 51
    target 150
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "M121_253"
      target_id "M121_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 421
    source 150
    target 42
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_81"
      target_id "M121_176"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 422
    source 151
    target 152
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_258"
      target_id "M121_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 423
    source 99
    target 152
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_256"
      target_id "M121_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 424
    source 37
    target 152
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_148"
      target_id "M121_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 425
    source 46
    target 152
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "M121_208"
      target_id "M121_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 426
    source 58
    target 152
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_169"
      target_id "M121_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 427
    source 153
    target 152
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_263"
      target_id "M121_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 428
    source 152
    target 48
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_118"
      target_id "M121_163"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 429
    source 97
    target 154
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_198"
      target_id "M121_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 430
    source 154
    target 115
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_63"
      target_id "M121_199"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 431
    source 155
    target 156
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_242"
      target_id "M121_128"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 432
    source 156
    target 7
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_128"
      target_id "M121_270"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 433
    source 157
    target 158
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_255"
      target_id "M121_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 434
    source 155
    target 158
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_242"
      target_id "M121_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 435
    source 158
    target 37
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_116"
      target_id "M121_148"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 436
    source 159
    target 160
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_156"
      target_id "M121_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 437
    source 161
    target 160
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "M121_155"
      target_id "M121_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 438
    source 162
    target 160
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_186"
      target_id "M121_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 439
    source 160
    target 40
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_34"
      target_id "M121_145"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 440
    source 163
    target 164
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_138"
      target_id "M121_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 441
    source 21
    target 164
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_137"
      target_id "M121_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 442
    source 164
    target 165
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_29"
      target_id "M121_139"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 443
    source 166
    target 167
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_141"
      target_id "M121_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 444
    source 165
    target 167
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_139"
      target_id "M121_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 445
    source 161
    target 167
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "M121_155"
      target_id "M121_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 446
    source 167
    target 135
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_24"
      target_id "M121_140"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 447
    source 167
    target 168
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_24"
      target_id "M121_142"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 448
    source 34
    target 169
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_149"
      target_id "M121_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 449
    source 169
    target 37
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_40"
      target_id "M121_148"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 450
    source 170
    target 171
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_264"
      target_id "M121_121"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 451
    source 37
    target 171
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M121_148"
      target_id "M121_121"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 452
    source 171
    target 153
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_121"
      target_id "M121_263"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 453
    source 172
    target 173
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_177"
      target_id "M121_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 454
    source 173
    target 26
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_48"
      target_id "M121_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 455
    source 174
    target 175
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_146"
      target_id "M121_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 456
    source 138
    target 175
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_3"
      target_id "M121_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 457
    source 175
    target 159
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_33"
      target_id "M121_156"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 458
    source 88
    target 176
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_204"
      target_id "M121_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 459
    source 177
    target 176
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_220"
      target_id "M121_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 460
    source 176
    target 178
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_96"
      target_id "M121_219"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 461
    source 179
    target 180
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_257"
      target_id "M121_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 462
    source 155
    target 180
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "M121_242"
      target_id "M121_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 463
    source 37
    target 180
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_148"
      target_id "M121_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 464
    source 8
    target 180
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_265"
      target_id "M121_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 465
    source 153
    target 180
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_263"
      target_id "M121_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 466
    source 181
    target 180
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_267"
      target_id "M121_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 467
    source 180
    target 99
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_117"
      target_id "M121_256"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 468
    source 182
    target 183
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_16"
      target_id "M121_114"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 469
    source 183
    target 184
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_114"
      target_id "M121_247"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 470
    source 94
    target 185
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_201"
      target_id "M121_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 471
    source 185
    target 186
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_105"
      target_id "M121_211"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 472
    source 16
    target 187
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_14"
      target_id "M121_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 473
    source 187
    target 26
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_93"
      target_id "M121_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 474
    source 188
    target 189
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_226"
      target_id "M121_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 475
    source 15
    target 189
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M121_158"
      target_id "M121_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 476
    source 105
    target 189
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M121_217"
      target_id "M121_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 477
    source 189
    target 172
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_87"
      target_id "M121_177"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 478
    source 190
    target 191
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_153"
      target_id "M121_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 479
    source 43
    target 191
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "M121_7"
      target_id "M121_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 480
    source 62
    target 191
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "M121_157"
      target_id "M121_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 481
    source 191
    target 137
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_31"
      target_id "M121_154"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 482
    source 99
    target 192
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_256"
      target_id "M121_130"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 483
    source 192
    target 7
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_130"
      target_id "M121_270"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 484
    source 193
    target 194
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_178"
      target_id "M121_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 485
    source 194
    target 26
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_49"
      target_id "M121_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 486
    source 165
    target 195
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_139"
      target_id "M121_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 487
    source 64
    target 195
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_144"
      target_id "M121_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 488
    source 195
    target 196
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_30"
      target_id "M121_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 489
    source 197
    target 198
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_193"
      target_id "M121_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 490
    source 199
    target 198
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_195"
      target_id "M121_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 491
    source 198
    target 76
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_60"
      target_id "M121_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 492
    source 200
    target 201
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_232"
      target_id "M121_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 493
    source 202
    target 201
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M121_190"
      target_id "M121_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 494
    source 201
    target 203
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_58"
      target_id "M121_189"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 495
    source 204
    target 205
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_173"
      target_id "M121_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 496
    source 3
    target 205
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_8"
      target_id "M121_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 497
    source 76
    target 205
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_15"
      target_id "M121_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 498
    source 205
    target 206
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_50"
      target_id "M121_179"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 499
    source 205
    target 1
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_50"
      target_id "M121_180"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 500
    source 50
    target 207
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_164"
      target_id "M121_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 501
    source 48
    target 207
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "M121_163"
      target_id "M121_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 502
    source 55
    target 207
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_209"
      target_id "M121_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 503
    source 37
    target 207
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_148"
      target_id "M121_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 504
    source 46
    target 207
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "M121_208"
      target_id "M121_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 505
    source 62
    target 207
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "M121_157"
      target_id "M121_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 506
    source 207
    target 19
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_37"
      target_id "M121_162"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 507
    source 47
    target 208
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_245"
      target_id "M121_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 508
    source 208
    target 209
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_109"
      target_id "M121_224"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 509
    source 210
    target 211
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_231"
      target_id "M121_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 510
    source 15
    target 211
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "M121_158"
      target_id "M121_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 511
    source 211
    target 60
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_65"
      target_id "M121_143"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 512
    source 212
    target 213
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_251"
      target_id "M121_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 513
    source 51
    target 213
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "M121_253"
      target_id "M121_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 514
    source 213
    target 85
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_41"
      target_id "M121_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 515
    source 67
    target 214
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_206"
      target_id "M121_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 516
    source 129
    target 214
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_202"
      target_id "M121_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 517
    source 214
    target 182
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_68"
      target_id "M121_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 518
    source 215
    target 216
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_234"
      target_id "M121_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 519
    source 155
    target 216
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M121_242"
      target_id "M121_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 520
    source 216
    target 97
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_79"
      target_id "M121_198"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 521
    source 178
    target 217
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_219"
      target_id "M121_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 522
    source 23
    target 217
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_161"
      target_id "M121_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 523
    source 81
    target 217
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_166"
      target_id "M121_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 524
    source 51
    target 217
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "M121_253"
      target_id "M121_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 525
    source 217
    target 218
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_97"
      target_id "M121_159"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 526
    source 55
    target 219
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_209"
      target_id "M121_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 527
    source 36
    target 219
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_152"
      target_id "M121_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 528
    source 219
    target 220
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_115"
      target_id "M121_210"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 529
    source 219
    target 221
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_115"
      target_id "M121_205"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 530
    source 31
    target 222
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_19"
      target_id "M121_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 531
    source 222
    target 73
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_99"
      target_id "M121_221"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 532
    source 153
    target 223
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_263"
      target_id "M121_125"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 533
    source 223
    target 224
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_125"
      target_id "M121_268"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 534
    source 225
    target 226
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_150"
      target_id "M121_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 535
    source 30
    target 226
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_151"
      target_id "M121_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 536
    source 226
    target 34
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_23"
      target_id "M121_149"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 537
    source 37
    target 227
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_148"
      target_id "M121_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 538
    source 228
    target 227
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_203"
      target_id "M121_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 539
    source 227
    target 46
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_70"
      target_id "M121_208"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 540
    source 197
    target 229
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_193"
      target_id "M121_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 541
    source 230
    target 229
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_197"
      target_id "M121_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 542
    source 229
    target 97
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_62"
      target_id "M121_198"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 543
    source 231
    target 232
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_187"
      target_id "M121_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 544
    source 48
    target 232
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "M121_163"
      target_id "M121_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 545
    source 232
    target 20
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_56"
      target_id "M121_167"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 546
    source 233
    target 234
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_227"
      target_id "M121_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 547
    source 15
    target 234
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M121_158"
      target_id "M121_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 548
    source 105
    target 234
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M121_217"
      target_id "M121_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 549
    source 234
    target 24
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_84"
      target_id "M121_170"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 550
    source 143
    target 235
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_246"
      target_id "M121_111"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 551
    source 235
    target 209
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_111"
      target_id "M121_224"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 552
    source 16
    target 236
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_14"
      target_id "M121_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 553
    source 129
    target 236
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_202"
      target_id "M121_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 554
    source 236
    target 237
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_72"
      target_id "M121_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 555
    source 238
    target 239
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_259"
      target_id "M121_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 556
    source 82
    target 239
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M121_260"
      target_id "M121_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 557
    source 153
    target 239
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_263"
      target_id "M121_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 558
    source 239
    target 240
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_120"
      target_id "M121_261"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 559
    source 37
    target 241
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_148"
      target_id "M121_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 560
    source 241
    target 26
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_112"
      target_id "M121_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 561
    source 67
    target 242
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_206"
      target_id "M121_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 562
    source 155
    target 242
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "M121_242"
      target_id "M121_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 563
    source 242
    target 228
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_67"
      target_id "M121_203"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 564
    source 77
    target 243
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_182"
      target_id "M121_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 565
    source 244
    target 243
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_183"
      target_id "M121_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 566
    source 243
    target 9
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_52"
      target_id "M121_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 567
    source 245
    target 246
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_239"
      target_id "M121_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 568
    source 51
    target 246
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "M121_253"
      target_id "M121_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 569
    source 246
    target 128
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_74"
      target_id "M121_214"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 570
    source 161
    target 247
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_155"
      target_id "M121_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 571
    source 40
    target 247
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_145"
      target_id "M121_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 572
    source 248
    target 247
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "TRIGGER"
      source_id "M121_243"
      target_id "M121_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 573
    source 247
    target 249
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_36"
      target_id "M121_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 574
    source 250
    target 251
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_233"
      target_id "M121_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 575
    source 251
    target 252
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_90"
      target_id "M121_218"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 576
    source 253
    target 254
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_194"
      target_id "M121_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 577
    source 203
    target 254
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_189"
      target_id "M121_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 578
    source 254
    target 199
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_89"
      target_id "M121_195"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 579
    source 254
    target 255
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_89"
      target_id "M121_196"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 580
    source 256
    target 257
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_225"
      target_id "M121_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 581
    source 15
    target 257
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M121_158"
      target_id "M121_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 582
    source 105
    target 257
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M121_217"
      target_id "M121_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 583
    source 257
    target 193
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_88"
      target_id "M121_178"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 584
    source 258
    target 259
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_188"
      target_id "M121_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 585
    source 155
    target 259
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M121_242"
      target_id "M121_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 586
    source 259
    target 250
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_57"
      target_id "M121_233"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 587
    source 73
    target 260
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_221"
      target_id "M121_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 588
    source 260
    target 209
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_100"
      target_id "M121_224"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 589
    source 85
    target 261
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_6"
      target_id "M121_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 590
    source 146
    target 261
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_222"
      target_id "M121_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 591
    source 261
    target 71
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_107"
      target_id "M121_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 592
    source 262
    target 263
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_241"
      target_id "M121_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 593
    source 51
    target 263
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "INHIBITION"
      source_id "M121_253"
      target_id "M121_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 594
    source 263
    target 161
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_66"
      target_id "M121_155"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 595
    source 23
    target 264
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_161"
      target_id "M121_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 596
    source 265
    target 264
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_215"
      target_id "M121_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 597
    source 248
    target 264
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M121_243"
      target_id "M121_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 598
    source 264
    target 266
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_80"
      target_id "M121_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 599
    source 240
    target 267
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_261"
      target_id "M121_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 600
    source 267
    target 224
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_124"
      target_id "M121_268"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 601
    source 268
    target 269
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_191"
      target_id "M121_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 602
    source 250
    target 269
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CATALYSIS"
      source_id "M121_233"
      target_id "M121_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 603
    source 269
    target 197
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_59"
      target_id "M121_193"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 604
    source 269
    target 270
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_59"
      target_id "M121_192"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 605
    source 128
    target 271
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "CONSPUMPTION"
      source_id "M121_214"
      target_id "M121_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 606
    source 271
    target 26
    cd19dm [
      diagram "C19DMap:Coagulation pathway"
      edge_type "PRODUCTION"
      source_id "M121_76"
      target_id "M121_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
