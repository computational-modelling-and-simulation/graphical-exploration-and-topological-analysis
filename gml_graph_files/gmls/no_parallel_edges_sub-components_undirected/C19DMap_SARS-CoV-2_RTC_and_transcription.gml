# generated with VANTED V2.8.2 at Fri Mar 04 09:53:02 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_20"
      name "nsp8_space_(I)"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph49"
      uniprot "NA"
    ]
    graphics [
      x 529.7216405204267
      y 73.53111835907202
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_41"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph76"
      uniprot "NA"
    ]
    graphics [
      x 649.3446713402152
      y 151.58763444076072
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_22"
      name "nsp8_space_(II)"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph50"
      uniprot "NA"
    ]
    graphics [
      x 518.3145020067734
      y 140.26599899972302
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_9"
      name "nsp7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph33"
      uniprot "NA"
    ]
    graphics [
      x 541.4110592229031
      y 207.3175440009975
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_45"
      name "complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "glyph8"
      uniprot "NA"
    ]
    graphics [
      x 631.1280805619899
      y 321.94525870525024
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_38"
      name "deg"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "glyph73"
      uniprot "NA"
    ]
    graphics [
      x 77.33677191952984
      y 448.47274873098564
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_51"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph85"
      uniprot "NA"
    ]
    graphics [
      x 197.15074049099528
      y 452.9638099605658
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_1"
      name "(_plus_)gRNA_space_(including_space_ORF1a_space_ORF1b)"
      node_subtype "GENE"
      node_type "species"
      org_id "glyph13"
      uniprot "NA"
    ]
    graphics [
      x 323.29826112646145
      y 551.7137464546815
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_31"
      name "pp1a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph63"
      uniprot "NA"
    ]
    graphics [
      x 241.47044885777146
      y 315.64804091075956
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_21"
      name "CoV_space_poly_minus__br_merase_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "glyph5"
      uniprot "NA"
    ]
    graphics [
      x 460.3165978814568
      y 661.1515647020395
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_42"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph77"
      uniprot "NA"
    ]
    graphics [
      x 557.7142901875892
      y 491.25096141829687
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_23"
      name "nsp9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph51"
      uniprot "NA"
    ]
    graphics [
      x 470.87873467682164
      y 357.28839665531257
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_8"
      name "Replication_space_transcription_space_complex_space_"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "glyph3"
      uniprot "NA"
    ]
    graphics [
      x 798.688366074304
      y 595.760626589819
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_39"
      name "deg"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "glyph74"
      uniprot "NA"
    ]
    graphics [
      x 989.9907866128103
      y 527.5537012130494
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_47"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph81"
      uniprot "NA"
    ]
    graphics [
      x 1022.4897925970954
      y 617.0462019887577
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_7"
      name "(_plus_)sgRNA_space_(2_minus_9)"
      node_subtype "GENE"
      node_type "species"
      org_id "glyph20"
      uniprot "NA"
    ]
    graphics [
      x 1192.6509086315973
      y 591.1487580103911
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_52"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph86"
      uniprot "NA"
    ]
    graphics [
      x 807.1483206892032
      y 754.0974085532935
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_36"
      name "deg"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "glyph71"
      uniprot "NA"
    ]
    graphics [
      x 727.8677125732268
      y 847.9728431526715
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_6"
      name "Replication_space_transcription_space_complex_space_"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "glyph2"
      uniprot "NA"
    ]
    graphics [
      x 906.5343526743063
      y 703.9811397450752
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_35"
      name "deg"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "glyph70"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 697.6647412423491
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_53"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph87"
      uniprot "NA"
    ]
    graphics [
      x 178.66147897120453
      y 673.7282389532218
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_15"
      name "pp1ab"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph40"
      uniprot "NA"
    ]
    graphics [
      x 125.76435592321639
      y 827.8774848055311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_44"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "glyph79"
      uniprot "NA"
    ]
    graphics [
      x 182.89296512576527
      y 960.3683967806377
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_30"
      name "nsp12"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph62"
      uniprot "NA"
    ]
    graphics [
      x 229.71197438370243
      y 818.1393082527266
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_26"
      name "nsp14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph54"
      uniprot "NA"
    ]
    graphics [
      x 297.8801061601665
      y 853.78837393102
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_10"
      name "nsp13"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph34"
      uniprot "NA"
    ]
    graphics [
      x 287.2546342058231
      y 1024.844637981219
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_11"
      name "nsp16"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph36"
      uniprot "NA"
    ]
    graphics [
      x 147.23503088563598
      y 1084.6997347262545
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_24"
      name "nsp15"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph52"
      uniprot "NA"
    ]
    graphics [
      x 239.45573973499629
      y 1094.8765119998768
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_37"
      name "deg"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "glyph72"
      uniprot "NA"
    ]
    graphics [
      x 529.6676911252437
      y 642.4150795814352
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_43"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph78"
      uniprot "NA"
    ]
    graphics [
      x 648.5542368494597
      y 665.77412381057
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_3"
      name "(_plus_)gRNA_space_(including_space_ORF1a_space_ORF1b)"
      node_subtype "GENE"
      node_type "species"
      org_id "glyph17"
      uniprot "NA"
    ]
    graphics [
      x 801.9246427986424
      y 679.7722665667914
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_25"
      name "pp1ab"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph53"
      uniprot "NA"
    ]
    graphics [
      x 559.6486958225116
      y 577.3686370064006
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_34"
      name "deg"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "glyph69"
      uniprot "NA"
    ]
    graphics [
      x 893.0911787436248
      y 416.04826277088506
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_49"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph83"
      uniprot "NA"
    ]
    graphics [
      x 832.9716430825629
      y 519.861605561221
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_19"
      name "pp1a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph46"
      uniprot "NA"
    ]
    graphics [
      x 762.8330485690408
      y 430.7821187443851
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_48"
      name "NA"
      node_subtype "KNOWN_TRANSITION_OMITTED"
      node_type "reaction"
      org_id "glyph82"
      uniprot "NA"
    ]
    graphics [
      x 382.0500587993893
      y 200.85244406334402
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_13"
      name "nsp1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph39"
      uniprot "NA"
    ]
    graphics [
      x 336.08468305387186
      y 297.86869304735126
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_12"
      name "nsp2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph37"
      uniprot "NA"
    ]
    graphics [
      x 431.47506781547787
      y 90.37794807292096
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_18"
      name "nsp3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph45"
      uniprot "NA"
    ]
    graphics [
      x 366.86922186489977
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_32"
      name "nsp11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph66"
      uniprot "NA"
    ]
    graphics [
      x 258.8301042426683
      y 143.50853683603327
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_27"
      name "nsp6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph56"
      uniprot "NA"
    ]
    graphics [
      x 465.37653391731124
      y 262.0642312390311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_33"
      name "nsp5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph67"
      uniprot "NA"
    ]
    graphics [
      x 304.47922844294374
      y 91.70880933455095
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_16"
      name "nsp4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph41"
      uniprot "NA"
    ]
    graphics [
      x 259.82000224604167
      y 215.9559386216202
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_29"
      name "nsp10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph61"
      uniprot "NA"
    ]
    graphics [
      x 344.1246018617228
      y 443.42707900894356
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_46"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph80"
      uniprot "NA"
    ]
    graphics [
      x 956.2406240420339
      y 815.8690877104649
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_14"
      name "CoV_space_poly_minus__br_merase_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "glyph4"
      uniprot "NA"
    ]
    graphics [
      x 841.8460066359688
      y 846.4655723554683
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_28"
      name "complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "glyph6"
      uniprot "NA"
    ]
    graphics [
      x 1026.1017807468415
      y 914.4244701947371
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_17"
      name "nsp9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph42"
      uniprot "NA"
    ]
    graphics [
      x 943.9118690965922
      y 946.6223133753605
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_2"
      name "(_minus_)gRNA_space_(including_space_ORF1a_space_ORF1b)"
      node_subtype "GENE"
      node_type "species"
      org_id "glyph16"
      uniprot "NA"
    ]
    graphics [
      x 875.1223146344626
      y 919.0677943490563
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_40"
      name "deg"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "glyph75"
      uniprot "NA"
    ]
    graphics [
      x 1288.566184913806
      y 411.2415845339598
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_54"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph88"
      uniprot "NA"
    ]
    graphics [
      x 1343.5460051011278
      y 526.5306365656196
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_5"
      name "viral_space_accessory"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph19"
      uniprot "NA"
    ]
    graphics [
      x 1395.7384809403811
      y 620.7895976513064
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_4"
      name "structural_space_proteins"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "glyph18"
      uniprot "NA"
    ]
    graphics [
      x 1247.9642402380975
      y 487.3363811727173
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      full_annotation "NA"
      hgnc "NA"
      map_id "M15_50"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "glyph84"
      uniprot "NA"
    ]
    graphics [
      x 319.4701734987969
      y 684.3670066779862
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M15_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 55
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_20"
      target_id "M15_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 56
    source 3
    target 2
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_22"
      target_id "M15_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 57
    source 4
    target 2
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_9"
      target_id "M15_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 58
    source 2
    target 5
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_41"
      target_id "M15_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 59
    source 6
    target 7
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_38"
      target_id "M15_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 60
    source 8
    target 7
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "TRIGGER"
      source_id "M15_1"
      target_id "M15_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 61
    source 7
    target 9
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_51"
      target_id "M15_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 62
    source 10
    target 11
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_21"
      target_id "M15_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 63
    source 12
    target 11
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_23"
      target_id "M15_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 64
    source 5
    target 11
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_45"
      target_id "M15_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 65
    source 8
    target 11
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_1"
      target_id "M15_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 66
    source 11
    target 13
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_42"
      target_id "M15_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 67
    source 14
    target 15
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_39"
      target_id "M15_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 68
    source 13
    target 15
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "TRIGGER"
      source_id "M15_8"
      target_id "M15_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 69
    source 15
    target 16
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_47"
      target_id "M15_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 70
    source 13
    target 17
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_8"
      target_id "M15_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 71
    source 18
    target 17
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_36"
      target_id "M15_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 72
    source 17
    target 19
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_52"
      target_id "M15_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 73
    source 20
    target 21
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_35"
      target_id "M15_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 74
    source 8
    target 21
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "TRIGGER"
      source_id "M15_1"
      target_id "M15_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 75
    source 21
    target 22
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_53"
      target_id "M15_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 76
    source 22
    target 23
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_15"
      target_id "M15_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 77
    source 23
    target 24
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_44"
      target_id "M15_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 78
    source 23
    target 25
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_44"
      target_id "M15_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 79
    source 23
    target 26
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_44"
      target_id "M15_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 80
    source 23
    target 27
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_44"
      target_id "M15_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 81
    source 23
    target 28
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_44"
      target_id "M15_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 82
    source 29
    target 30
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_37"
      target_id "M15_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 83
    source 31
    target 30
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "TRIGGER"
      source_id "M15_3"
      target_id "M15_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 84
    source 30
    target 32
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_43"
      target_id "M15_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 85
    source 33
    target 34
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_34"
      target_id "M15_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 86
    source 31
    target 34
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "TRIGGER"
      source_id "M15_3"
      target_id "M15_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 87
    source 34
    target 35
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_49"
      target_id "M15_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 88
    source 9
    target 36
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_31"
      target_id "M15_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 89
    source 36
    target 1
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "M15_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 90
    source 36
    target 3
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "M15_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 91
    source 36
    target 4
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "M15_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 92
    source 36
    target 12
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "M15_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 93
    source 36
    target 37
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "M15_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 94
    source 36
    target 38
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "M15_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 95
    source 36
    target 39
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "M15_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 96
    source 36
    target 40
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "M15_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 97
    source 36
    target 41
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "M15_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 98
    source 36
    target 42
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "M15_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 99
    source 36
    target 43
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "M15_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 100
    source 36
    target 44
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_48"
      target_id "M15_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 101
    source 19
    target 45
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_6"
      target_id "M15_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 102
    source 45
    target 46
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_46"
      target_id "M15_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 103
    source 45
    target 47
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_46"
      target_id "M15_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 104
    source 45
    target 48
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_46"
      target_id "M15_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 105
    source 45
    target 31
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_46"
      target_id "M15_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 106
    source 45
    target 49
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_46"
      target_id "M15_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 107
    source 50
    target 51
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_40"
      target_id "M15_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 108
    source 16
    target 51
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "TRIGGER"
      source_id "M15_7"
      target_id "M15_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 109
    source 51
    target 52
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_54"
      target_id "M15_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 110
    source 51
    target 53
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_54"
      target_id "M15_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 111
    source 25
    target 54
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_26"
      target_id "M15_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 112
    source 44
    target 54
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_29"
      target_id "M15_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 113
    source 24
    target 54
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "CONSPUMPTION"
      source_id "M15_30"
      target_id "M15_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 114
    source 54
    target 10
    cd19dm [
      diagram "C19DMap:SARS-CoV-2 RTC and transcription"
      edge_type "PRODUCTION"
      source_id "M15_50"
      target_id "M15_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
