# generated with VANTED V2.8.2 at Fri Mar 04 09:52:59 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "urn:miriam:ncbigene:3455;urn:miriam:ensembl:ENSG00000162434;urn:miriam:ensembl:ENSG00000105397;urn:miriam:ncbigene:3454"
      hgnc "NA"
      map_id "W15_9"
      name "cd056"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "cd056"
      uniprot "NA"
    ]
    graphics [
      x 371.9829101974498
      y 819.4546994378634
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_29"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id683cbfa3"
      uniprot "NA"
    ]
    graphics [
      x 386.2991672071758
      y 664.8193200607416
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "urn:miriam:ensembl:ENSG00000115415;urn:miriam:ensembl:ENSG00000170581"
      hgnc "NA"
      map_id "W15_21"
      name "f278d"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "f278d"
      uniprot "NA"
    ]
    graphics [
      x 403.28501163914405
      y 494.86949860548293
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "urn:miriam:uniprot:694009"
      hgnc "NA"
      map_id "W15_8"
      name "S"
      node_subtype "GENE"
      node_type "species"
      org_id "c8d7f"
      uniprot "UNIPROT:694009"
    ]
    graphics [
      x 481.7695794297315
      y 605.2916662566511
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_32"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ida37bfa65"
      uniprot "NA"
    ]
    graphics [
      x 628.7313674412134
      y 640.6877610957094
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_10"
      name "Endocytosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "cde94"
      uniprot "NA"
    ]
    graphics [
      x 772.8979622753732
      y 692.0929117433332
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_25"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id2489aa99"
      uniprot "NA"
    ]
    graphics [
      x 432.6066491140725
      y 345.3512010901054
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "urn:miriam:ensembl:ENSG00000213928"
      hgnc "NA"
      map_id "W15_11"
      name "IRF9"
      node_subtype "GENE"
      node_type "species"
      org_id "cffa4"
      uniprot "NA"
    ]
    graphics [
      x 345.1048130350656
      y 273.1355431220989
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "urn:miriam:ensembl:ENSG00000213928;urn:miriam:ensembl:ENSG00000115415;urn:miriam:ensembl:ENSG00000170581"
      hgnc "NA"
      map_id "W15_17"
      name "ed2d7"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ed2d7"
      uniprot "NA"
    ]
    graphics [
      x 484.3657769640341
      y 212.7578632186288
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "urn:miriam:ensembl:ENSG00000130234"
      hgnc "NA"
      map_id "W15_5"
      name "ACE2"
      node_subtype "GENE"
      node_type "species"
      org_id "c2d8e"
      uniprot "NA"
    ]
    graphics [
      x 182.3917709246973
      y 619.5490892012854
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_33"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idb755ca84"
      uniprot "NA"
    ]
    graphics [
      x 94.62833842629823
      y 713.6440998606543
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "urn:miriam:uniprot:694009;urn:miriam:ensembl:ENSG00000130234"
      hgnc "NA"
      map_id "W15_1"
      name "a5c10"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "a5c10"
      uniprot "UNIPROT:694009"
    ]
    graphics [
      x 62.5
      y 827.2863253016011
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "urn:miriam:ensembl:ENSG00000126456"
      hgnc "NA"
      map_id "W15_15"
      name "ea539"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ea539"
      uniprot "NA"
    ]
    graphics [
      x 693.4059348878269
      y 967.2946822584183
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "PUBMED:15681410"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_36"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "ided8e62a3"
      uniprot "NA"
    ]
    graphics [
      x 644.5893812277969
      y 1085.468385749202
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_7"
      name "IFN_minus_beta_br_Response_space_element"
      node_subtype "GENE"
      node_type "species"
      org_id "c7d27"
      uniprot "NA"
    ]
    graphics [
      x 615.2409224773172
      y 1196.5219175086477
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_26"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id27cf2cc3"
      uniprot "NA"
    ]
    graphics [
      x 855.46222415862
      y 801.2569078157886
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "urn:miriam:ensembl:ENSG00000115267"
      hgnc "NA"
      map_id "W15_14"
      name "IFIH1"
      node_subtype "GENE"
      node_type "species"
      org_id "e1fcd"
      uniprot "NA"
    ]
    graphics [
      x 928.1386847394118
      y 901.40303822897
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "urn:miriam:ensembl:ENSG00000120242;urn:miriam:ensembl:ENSG00000228083;urn:miriam:ensembl:ENSG00000186803;urn:miriam:ensembl:ENSG00000234829;urn:miriam:ensembl:ENSG00000233816;urn:miriam:ensembl:ENSG00000147885;urn:miriam:ensembl:ENSG00000197919;urn:miriam:ensembl:ENSG00000120235;urn:miriam:ensembl:ENSG00000236637;urn:miriam:ensembl:ENSG00000188379;urn:miriam:ensembl:ENSG00000147873;urn:miriam:ensembl:ENSG00000137080;urn:miriam:ensembl:ENSG00000214042"
      hgnc "NA"
      map_id "W15_3"
      name "IFN_minus_alpha_space_group"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b12b5"
      uniprot "NA"
    ]
    graphics [
      x 376.01147470766966
      y 1064.7637761007536
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_35"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "ide4fe418d"
      uniprot "NA"
    ]
    graphics [
      x 402.3643332943186
      y 949.027566919061
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_37"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idf68781f"
      uniprot "NA"
    ]
    graphics [
      x 935.8245030443961
      y 711.0141855121885
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "urn:miriam:ensembl:ENSG00000107201"
      hgnc "NA"
      map_id "W15_20"
      name "DDX58"
      node_subtype "GENE"
      node_type "species"
      org_id "f1eea"
      uniprot "NA"
    ]
    graphics [
      x 1050.4420785822283
      y 741.5753330010319
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "urn:miriam:uniprot:694009"
      hgnc "NA"
      map_id "W15_13"
      name "S"
      node_subtype "GENE"
      node_type "species"
      org_id "db168"
      uniprot "UNIPROT:694009"
    ]
    graphics [
      x 794.4257261468879
      y 224.8725042707012
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_30"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id8d8be581"
      uniprot "NA"
    ]
    graphics [
      x 905.2843980942732
      y 199.28161617936388
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_6"
      name "NA"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "c5c37"
      uniprot "NA"
    ]
    graphics [
      x 878.6311502552932
      y 311.5293376139906
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "urn:miriam:ensembl:ENSG00000115267;urn:miriam:ensembl:ENSG00000107201"
      hgnc "NA"
      map_id "W15_4"
      name "b9f09"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b9f09"
      uniprot "NA"
    ]
    graphics [
      x 1251.686902363409
      y 548.8248036235674
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_27"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id3573c07"
      uniprot "NA"
    ]
    graphics [
      x 1249.9440671427594
      y 439.35092741028905
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_16"
      name "NA"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "ecbcf"
      uniprot "NA"
    ]
    graphics [
      x 1338.3105449888349
      y 532.498182469854
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "urn:miriam:ensembl:ENSG00000088888"
      hgnc "NA"
      map_id "W15_19"
      name "MAVS"
      node_subtype "GENE"
      node_type "species"
      org_id "f0e60"
      uniprot "NA"
    ]
    graphics [
      x 1117.7490689921888
      y 495.48905734933254
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_39"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idf9e7dc0d"
      uniprot "NA"
    ]
    graphics [
      x 550.4881581949703
      y 99.51265787746104
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_2"
      name "ISRE_space__br_Response_space_element"
      node_subtype "GENE"
      node_type "species"
      org_id "a74fc"
      uniprot "NA"
    ]
    graphics [
      x 667.882196945113
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "urn:miriam:ensembl:ENSG00000171855"
      hgnc "NA"
      map_id "W15_22"
      name "IFN_minus_beta"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "f5fa0"
      uniprot "NA"
    ]
    graphics [
      x 222.65047819842044
      y 851.278355205196
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_28"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id3e37c065"
      uniprot "NA"
    ]
    graphics [
      x 292.5927375711928
      y 923.7125245662942
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_24"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id134a11f0"
      uniprot "NA"
    ]
    graphics [
      x 318.92048784596267
      y 589.631497397099
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_38"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idf94c3c70"
      uniprot "NA"
    ]
    graphics [
      x 993.2500923643503
      y 582.5323504265615
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "urn:miriam:ensembl:ENSG00000131323;urn:miriam:ensembl:ENSG00000183735;urn:miriam:ensembl:ENSG00000263528"
      hgnc "NA"
      map_id "W15_12"
      name "d1b60"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d1b60"
      uniprot "NA"
    ]
    graphics [
      x 868.298939771416
      y 698.7470476020951
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "urn:miriam:ensembl:ENSG00000126456"
      hgnc "NA"
      map_id "W15_18"
      name "IRF3"
      node_subtype "GENE"
      node_type "species"
      org_id "f0053"
      uniprot "NA"
    ]
    graphics [
      x 839.0987192522459
      y 1010.8124143495554
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idbcd773be"
      uniprot "NA"
    ]
    graphics [
      x 790.5521221002916
      y 872.5870579579349
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4912"
      full_annotation "urn:miriam:ncbigene:148022"
      hgnc "NA"
      map_id "W15_23"
      name "TICAM1"
      node_subtype "GENE"
      node_type "species"
      org_id "f9104"
      uniprot "NA"
    ]
    graphics [
      x 1010.1938723555161
      y 1112.8110973418068
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4912"
      full_annotation "NA"
      hgnc "NA"
      map_id "W15_31"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id95166c5e"
      uniprot "NA"
    ]
    graphics [
      x 898.6475278261132
      y 1127.1075893289135
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W15_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 40
    source 1
    target 2
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "W15_9"
      target_id "W15_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 41
    source 2
    target 3
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_29"
      target_id "W15_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 42
    source 4
    target 5
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "W15_8"
      target_id "W15_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 43
    source 5
    target 6
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_32"
      target_id "W15_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 44
    source 3
    target 7
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "W15_21"
      target_id "W15_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 45
    source 8
    target 7
    cd19dm [
      diagram "WP4912"
      edge_type "MODULATION"
      source_id "W15_11"
      target_id "W15_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 46
    source 7
    target 9
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_25"
      target_id "W15_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 47
    source 10
    target 11
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "W15_5"
      target_id "W15_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 48
    source 11
    target 12
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_33"
      target_id "W15_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 49
    source 13
    target 14
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "W15_15"
      target_id "W15_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 50
    source 14
    target 15
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_36"
      target_id "W15_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 51
    source 6
    target 16
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "W15_10"
      target_id "W15_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 52
    source 16
    target 17
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_26"
      target_id "W15_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 53
    source 18
    target 19
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "W15_3"
      target_id "W15_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 54
    source 19
    target 1
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_35"
      target_id "W15_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 55
    source 6
    target 20
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "W15_10"
      target_id "W15_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 56
    source 20
    target 21
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_37"
      target_id "W15_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 57
    source 22
    target 23
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "W15_13"
      target_id "W15_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 58
    source 23
    target 24
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_30"
      target_id "W15_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 59
    source 25
    target 26
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "W15_4"
      target_id "W15_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 60
    source 27
    target 26
    cd19dm [
      diagram "WP4912"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W15_16"
      target_id "W15_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 61
    source 26
    target 28
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_27"
      target_id "W15_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 62
    source 9
    target 29
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "W15_17"
      target_id "W15_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 63
    source 29
    target 30
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_39"
      target_id "W15_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 64
    source 31
    target 32
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "W15_22"
      target_id "W15_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 65
    source 32
    target 1
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_28"
      target_id "W15_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 66
    source 4
    target 33
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "W15_8"
      target_id "W15_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 67
    source 33
    target 10
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_24"
      target_id "W15_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 68
    source 28
    target 34
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "W15_19"
      target_id "W15_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 69
    source 34
    target 35
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_38"
      target_id "W15_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 70
    source 36
    target 37
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "W15_18"
      target_id "W15_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 71
    source 35
    target 37
    cd19dm [
      diagram "WP4912"
      edge_type "CATALYSIS"
      source_id "W15_12"
      target_id "W15_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 72
    source 37
    target 13
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_34"
      target_id "W15_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 73
    source 38
    target 39
    cd19dm [
      diagram "WP4912"
      edge_type "CONSPUMPTION"
      source_id "W15_23"
      target_id "W15_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 74
    source 39
    target 36
    cd19dm [
      diagram "WP4912"
      edge_type "PRODUCTION"
      source_id "W15_31"
      target_id "W15_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
