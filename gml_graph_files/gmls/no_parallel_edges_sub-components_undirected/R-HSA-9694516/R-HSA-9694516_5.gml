# generated with VANTED V2.8.2 at Fri Mar 04 10:04:36 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9694383;urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1;urn:miriam:reactome:R-COV-9682229"
      hgnc "NA"
      map_id "R2_37"
      name "nsp9"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2229"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 186.74478963011165
      y 551.3881441572044
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9694261;PUBMED:19153232;PUBMED:32592996"
      count 1
      diagram "R-HSA-9694516"
      full_annotation "NA"
      hgnc "NA"
      map_id "R2_304"
      name "nsp9 forms a homotetramer"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9694261__layout_2228"
      uniprot "NA"
    ]
    graphics [
      x 204.94620962245676
      y 645.6431071437162
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_304"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9694516"
      full_annotation "urn:miriam:reactome:R-COV-9694286;urn:miriam:uniprot:P0DTC1;urn:miriam:uniprot:P0DTD1"
      hgnc "NA"
      map_id "R2_38"
      name "nsp9_space_tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2230"
      uniprot "UNIPROT:P0DTC1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 305.73713736608977
      y 753.2486322353767
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R2_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 4
    source 1
    target 2
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "CONSPUMPTION"
      source_id "R2_37"
      target_id "R2_304"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 5
    source 2
    target 3
    cd19dm [
      diagram "R-HSA-9694516"
      edge_type "PRODUCTION"
      source_id "R2_304"
      target_id "R2_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
