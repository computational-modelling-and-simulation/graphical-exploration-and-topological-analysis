# generated with VANTED V2.8.2 at Fri Mar 04 09:53:04 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9682469;urn:miriam:uniprot:P0C6U8;urn:miriam:refseq:NC_004718.3;urn:miriam:reactome:R-COV-9694618;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_32"
      name "SARS_minus_CoV_minus_1_space_gRNA:RTC:nascent_space_RNA_space_minus_space_strand"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_141"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 527.0632383405497
      y 1365.0642037449759
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9687388;PUBMED:31645453;PUBMED:32258351;PUBMED:32284326;PUBMED:29511076;PUBMED:32253226;PUBMED:31233808;PUBMED:32094225;PUBMED:28124907"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_368"
      name "SARS-CoV-1 gRNA:RTC:nascent RNA minus strand binds RTC inhibitors"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9687388__layout_1410"
      uniprot "NA"
    ]
    graphics [
      x 574.6617211225507
      y 1514.804133944848
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_368"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-ALL-9687408"
      hgnc "NA"
      map_id "R1_38"
      name "RTC_space_inhibitors"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_1483"
      uniprot "NA"
    ]
    graphics [
      x 748.3644359299876
      y 1604.3990465752906
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9694327;urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9687385;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_33"
      name "SARS_minus_CoV_minus_1_space_gRNA:RTC:nascent_space_RNA_space_minus_space_strand:RTC_space_inhibitors"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_1411"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 425.65689834313366
      y 1379.7339742812746
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9694642;urn:miriam:uniprot:P0C6X7;urn:miriam:reactome:R-COV-9678281"
      hgnc "NA"
      map_id "R1_158"
      name "pp1ab_minus_nsp13"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_32"
      uniprot "UNIPROT:P0C6X7"
    ]
    graphics [
      x 1415.7212439833957
      y 1747.3284615535695
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_158"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9682626;PUBMED:31131400;PUBMED:22615777;PUBMED:17520018"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_276"
      name "nsp13 binds nsp12"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9682626__layout_114"
      uniprot "NA"
    ]
    graphics [
      x 1087.6692433369692
      y 1880.423136581473
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_276"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9682451;urn:miriam:reactome:R-COV-9694597;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_13"
      name "nsp7:nsp8:nsp12:nsp14:nsp10"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_113"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 875.9178570340421
      y 1878.428881846477
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9682616;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_14"
      name "nsp7:nsp8:nsp12:nsp14:nsp10:nsp13"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_115"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 1068.226023055925
      y 2066.6419070495695
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-SPO-2997551-3;urn:miriam:reactome:R-XTR-2997551;urn:miriam:reactome:R-SPO-2997551-2;urn:miriam:reactome:R-CEL-2997551;urn:miriam:reactome:R-SSC-2997551;urn:miriam:uniprot:P49841;urn:miriam:reactome:R-BTA-2997551;urn:miriam:reactome:R-SCE-2997551-2;urn:miriam:reactome:R-SCE-2997551-3;urn:miriam:reactome:R-SCE-2997551-4;urn:miriam:reactome:R-DRE-2997551;urn:miriam:reactome:R-SPO-2997551;urn:miriam:reactome:R-HSA-2997551;urn:miriam:reactome:R-DME-2997551;urn:miriam:reactome:R-RNO-198619;urn:miriam:reactome:R-SCE-2997551;urn:miriam:reactome:R-DME-2997551-2;urn:miriam:reactome:R-CEL-2997551-3;urn:miriam:reactome:R-DME-2997551-3;urn:miriam:reactome:R-CEL-2997551-4;urn:miriam:reactome:R-GGA-2997551;urn:miriam:reactome:R-CEL-2997551-2;urn:miriam:reactome:R-CFA-2997551;urn:miriam:reactome:R-MMU-2997551;urn:miriam:reactome:R-DDI-2997551"
      hgnc "NA"
      map_id "R1_59"
      name "GSK3B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_1717"
      uniprot "UNIPROT:P49841"
    ]
    graphics [
      x 1642.8246859737546
      y 1170.9772908769892
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9687724;PUBMED:34593624;PUBMED:19666099;PUBMED:19389332;PUBMED:11162580;PUBMED:24931005;PUBMED:18977324;PUBMED:18938143;PUBMED:19106108;PUBMED:18806775"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_370"
      name "GSK3B binds GSKi"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9687724__layout_1893"
      uniprot "NA"
    ]
    graphics [
      x 1811.8135298644543
      y 1093.5019949168507
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_370"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-ALL-9687688;urn:miriam:pubmed:19106108"
      hgnc "NA"
      map_id "R1_66"
      name "GSKi"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_1894"
      uniprot "NA"
    ]
    graphics [
      x 1833.921766123212
      y 1208.395664042428
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-HSA-9687663;urn:miriam:uniprot:P49841"
      hgnc "NA"
      map_id "R1_67"
      name "GSK3B:GSKi"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_1895"
      uniprot "UNIPROT:P49841"
    ]
    graphics [
      x 1977.6210569453547
      y 933.7284685178975
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:pubmed:14754895;urn:miriam:uniprot:Q9BYF1;urn:miriam:reactome:R-HSA-9683480"
      hgnc "NA"
      map_id "R1_240"
      name "glycosylated_minus_ACE2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_713"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 208.1010614691553
      y 1090.037057986664
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_240"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9695415;PUBMED:32619700;PUBMED:34052578;PUBMED:32345140;PUBMED:33497607;PUBMED:32149769;PUBMED:33609497;PUBMED:33303459;PUBMED:33309272"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_371"
      name "glycosylated-ACE2 binds ACE2 inhibitors"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9695415__layout_2063"
      uniprot "NA"
    ]
    graphics [
      x 221.95993870568782
      y 869.6443466355884
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_371"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-ALL-9695417;urn:miriam:pubchem.compound:10206;urn:miriam:pubchem.compound:441397;urn:miriam:pubchem.compound:272833;urn:miriam:pubchem.compound:656511;urn:miriam:pubchem.compound:47499"
      hgnc "NA"
      map_id "R1_96"
      name "ACE2_space_inhibitors"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_2064"
      uniprot "NA"
    ]
    graphics [
      x 325.4998803546906
      y 697.6226296479782
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:pubchem.compound:10206;urn:miriam:pubchem.compound:441397;urn:miriam:pubchem.compound:272833;urn:miriam:uniprot:Q9BYF1;urn:miriam:reactome:R-HSA-9695376;urn:miriam:pubchem.compound:656511;urn:miriam:pubchem.compound:47499"
      hgnc "NA"
      map_id "R1_97"
      name "glycosylated_minus_ACE2:ACE2_space_inhibitors"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2065"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 391.37167628697125
      y 851.4146481867976
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:pubmed:15094372;urn:miriam:reactome:R-COV-9683649;urn:miriam:uniprot:P59595"
      hgnc "NA"
      map_id "R1_245"
      name "SUMO1_minus_K62_minus_p_minus_S177_minus_N_space_dimer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_739"
      uniprot "UNIPROT:P59595"
    ]
    graphics [
      x 473.85117303259403
      y 292.72474140416296
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_245"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683606;PUBMED:15848177"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_286"
      name "Nucleoprotein translocates to the nucleolus"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683606__layout_262"
      uniprot "NA"
    ]
    graphics [
      x 355.97797093296094
      y 257.1323430074042
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_286"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9683761;urn:miriam:pubmed:15094372;urn:miriam:uniprot:P59595;urn:miriam:reactome:R-COV-9694659"
      hgnc "NA"
      map_id "R1_132"
      name "SUMO_minus_p_minus_N_space_dimer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_263"
      uniprot "UNIPROT:P59595"
    ]
    graphics [
      x 407.4838298344665
      y 381.2175493597015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9683597;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694305"
      hgnc "NA"
      map_id "R1_170"
      name "3xPalmC_minus_E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_362"
      uniprot "UNIPROT:P59637"
    ]
    graphics [
      x 269.66409387476324
      y 1345.6472473467795
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_170"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683679;PUBMED:20409569"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_300"
      name "Ubiquination of protein E"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683679__layout_363"
      uniprot "NA"
    ]
    graphics [
      x 236.194125060362
      y 1490.5705287864994
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_300"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:P62987;urn:miriam:uniprot:P62979;urn:miriam:uniprot:P0CG48;urn:miriam:uniprot:P0CG47;urn:miriam:reactome:R-HSA-8943136"
      hgnc "NA"
      map_id "R1_171"
      name "Ub"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_364"
      uniprot "UNIPROT:P62987;UNIPROT:P62979;UNIPROT:P0CG48;UNIPROT:P0CG47"
    ]
    graphics [
      x 250.21297491572375
      y 1283.3299601311776
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_171"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694423;urn:miriam:reactome:R-COV-9683626"
      hgnc "NA"
      map_id "R1_172"
      name "Ub_minus_3xPalmC_minus_E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_365"
      uniprot "UNIPROT:P59637"
    ]
    graphics [
      x 413.18606535580625
      y 1715.506145661467
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_172"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9713302;urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9713651;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_112"
      name "m7GpppA_minus_SARS_minus_CoV_minus_1_space_plus_space_strand_space_subgenomic_space_mRNAs:RTC"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2103"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 1290.1974855322119
      y 1584.2502684161564
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684033;PUBMED:18417574;PUBMED:21637813;PUBMED:24478444;PUBMED:20421945;PUBMED:12456663;PUBMED:6165837"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_322"
      name "nsp16 acts as a cap 2'-O-methyltransferase to modify SARS-CoV-1 mRNAs"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684033__layout_427"
      uniprot "NA"
    ]
    graphics [
      x 1102.6505500318756
      y 1359.3985963660903
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_322"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-ALL-9639461;urn:miriam:obo.chebi:CHEBI%3A15414"
      hgnc "NA"
      map_id "R1_191"
      name "S_minus_adenosyl_minus_L_minus_methionine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_422"
      uniprot "NA"
    ]
    graphics [
      x 1231.7870594778212
      y 1563.3626084485281
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_191"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16680;urn:miriam:reactome:R-ALL-9639443"
      hgnc "NA"
      map_id "R1_190"
      name "S_minus_adenosyl_minus_L_minus_homocysteine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_420"
      uniprot "NA"
    ]
    graphics [
      x 1224.3523963306843
      y 1620.882296380414
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_190"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9686016;urn:miriam:reactome:R-COV-9694302;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_204"
      name "RTC"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_458"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 867.6248313475603
      y 1221.5077887205591
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_204"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9685891;urn:miriam:reactome:R-COV-9694259;urn:miriam:refseq:NC_004718.3"
      hgnc "NA"
      map_id "R1_194"
      name "m7G(5')pppAm_minus_SARS_minus_CoV_minus_1_space_plus_space_strand_space_subgenomic_space_mRNAs"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_429"
      uniprot "NA"
    ]
    graphics [
      x 1086.2715987465674
      y 902.0095703212588
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_194"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9681663;urn:miriam:uniprot:P0C6U8;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P0C6X7;urn:miriam:reactome:R-COV-9694725"
      hgnc "NA"
      map_id "R1_28"
      name "SARS_space_coronavirus_space_gRNA:RTC:RNA_space_primer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_138"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 1047.087527790444
      y 1599.192033801679
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9681674;PUBMED:25197083;PUBMED:22791111"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_268"
      name "nsp12 synthesizes minus strand SARS-CoV-1 genomic RNA complement"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9681674__layout_140"
      uniprot "NA"
    ]
    graphics [
      x 694.290566838573
      y 1558.9507356353977
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_268"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-ALL-6806881;urn:miriam:obo.chebi:CHEBI%3A61557"
      hgnc "NA"
      map_id "R1_34"
      name "NTP(4_minus_)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_142"
      uniprot "NA"
    ]
    graphics [
      x 713.1095400453828
      y 1704.2326569552054
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9694255;urn:miriam:reactome:R-COV-9680476;urn:miriam:uniprot:P0C6U8;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_20"
      name "SARS_space_coronavirus_space_gRNA:RTC:RNA_space_primer:RTC_space_inhibitors"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_1266"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 797.1578565203544
      y 1697.0777857731487
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A33019;urn:miriam:reactome:R-ALL-111294"
      hgnc "NA"
      map_id "R1_30"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_139"
      uniprot "NA"
    ]
    graphics [
      x 461.5107923421856
      y 1530.969591349861
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-ALL-1131311;urn:miriam:obo.chebi:CHEBI%3A25609"
      hgnc "NA"
      map_id "R1_206"
      name "a_space_nucleotide_space_sugar"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_48"
      uniprot "NA"
    ]
    graphics [
      x 1027.601083618351
      y 434.19833658695256
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_206"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684290;PUBMED:15564471"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_333"
      name "nsp3 is glycosylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684290__layout_56"
      uniprot "NA"
    ]
    graphics [
      x 840.6708245478519
      y 402.12903652528894
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_333"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9684877;urn:miriam:uniprot:P0C6X7;urn:miriam:reactome:R-COV-9694428"
      hgnc "NA"
      map_id "R1_212"
      name "nsp3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_53"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 612.7473808993109
      y 480.28438369657215
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_212"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-70106"
      hgnc "NA"
      map_id "R1_207"
      name "H_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_49"
      uniprot "NA"
    ]
    graphics [
      x 1026.257307242021
      y 582.9146522009788
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_207"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9682205;urn:miriam:reactome:R-COV-9694735;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_214"
      name "N_minus_glycan_space_nsp3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_57"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 728.0403525205866
      y 480.69940248194996
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_214"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A57930;urn:miriam:reactome:R-ALL-9684302"
      hgnc "NA"
      map_id "R1_211"
      name "nucleoside_space_5'_minus_diphosphate(3_minus_)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_51"
      uniprot "NA"
    ]
    graphics [
      x 956.222134440537
      y 576.1994715777003
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_211"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9713306;urn:miriam:reactome:R-COV-9713634;urn:miriam:uniprot:P0C6U8;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_105"
      name "m7GpppA_minus_capped_space_SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_complement_space_(minus_space_strand):RTC"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2081"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 195.65696582972384
      y 1297.1446783291083
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684030;PUBMED:18417574;PUBMED:21637813;PUBMED:24478444;PUBMED:20421945;PUBMED:12456663;PUBMED:6165837"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_320"
      name "nsp16 acts as a cap 2'-O-methyltransferase to modify SARS-CoV-1 gRNA complement (minus strand)"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684030__layout_167"
      uniprot "NA"
    ]
    graphics [
      x 467.22580789208143
      y 1216.9403871457007
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_320"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-ALL-9639461;urn:miriam:obo.chebi:CHEBI%3A15414"
      hgnc "NA"
      map_id "R1_49"
      name "S_minus_adenosyl_minus_L_minus_methionine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_155"
      uniprot "NA"
    ]
    graphics [
      x 210.10972644396463
      y 1231.6356032648168
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16680;urn:miriam:reactome:R-ALL-9639443"
      hgnc "NA"
      map_id "R1_51"
      name "S_minus_adenosyl_minus_L_minus_homocysteine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_158"
      uniprot "NA"
    ]
    graphics [
      x 379.4073121782502
      y 1246.5010748506752
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9685513;urn:miriam:reactome:R-COV-9694603;urn:miriam:refseq:NC_004718.3"
      hgnc "NA"
      map_id "R1_78"
      name "m7G(5')pppAm_minus_capped_space_SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_complement_space_(minus_space_strand)"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_1941"
      uniprot "NA"
    ]
    graphics [
      x 730.8356391325085
      y 1148.9226454298932
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:P59594;urn:miriam:reactome:R-COV-9682868"
      hgnc "NA"
      map_id "R1_119"
      name "nascent_space_Spike"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_227"
      uniprot "UNIPROT:P59594"
    ]
    graphics [
      x 1037.5870593890686
      y 949.5354480231913
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683755;PUBMED:20129637;PUBMED:15831954;PUBMED:12775768"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_311"
      name "Spike protein gets N-glycosylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683755__layout_271"
      uniprot "NA"
    ]
    graphics [
      x 1225.5381887013605
      y 815.152416663871
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_311"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-ALL-9683033"
      hgnc "NA"
      map_id "R1_134"
      name "nucleotide_minus_sugar"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "layout_272"
      uniprot "NA"
    ]
    graphics [
      x 1274.4398635776
      y 744.3014807723733
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_134"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-156540"
      hgnc "NA"
      map_id "R1_137"
      name "H_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_275"
      uniprot "NA"
    ]
    graphics [
      x 1126.0141982390073
      y 765.6494710990537
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_137"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-ALL-9683046"
      hgnc "NA"
      map_id "R1_136"
      name "nucleoside_space_5'_minus_diphosphate(3âˆ’)"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "layout_274"
      uniprot "NA"
    ]
    graphics [
      x 1211.7754732718236
      y 714.4015690261265
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_136"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:pubmed:20129637;urn:miriam:pubmed:15253436;urn:miriam:pubmed:17715238;urn:miriam:reactome:R-COV-9683594;urn:miriam:pubmed:16122388;urn:miriam:uniprot:P59594;urn:miriam:pubmed:12775768;urn:miriam:pubmed:14760722"
      hgnc "NA"
      map_id "R1_135"
      name "N_minus_glycan_space_Spike"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_273"
      uniprot "UNIPROT:P59594"
    ]
    graphics [
      x 1494.1404862393506
      y 900.0909262539722
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_135"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9713313;urn:miriam:reactome:R-COV-9713643;urn:miriam:uniprot:P0C6U8;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_108"
      name "m7GpppA_minus_capped_space_SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand):RTC"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2093"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 1007.0662696548009
      y 1197.8233743423339
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684032;PUBMED:18417574;PUBMED:21637813;PUBMED:24478444;PUBMED:20421945;PUBMED:12456663;PUBMED:6165837"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_321"
      name "nsp16 acts as a cap 2'-O-methyltransferase to modify SARS-CoV-1 gRNA (plus strand)"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684032__layout_185"
      uniprot "NA"
    ]
    graphics [
      x 775.6154363946382
      y 1099.481160751179
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_321"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-ALL-9639461;urn:miriam:obo.chebi:CHEBI%3A15414"
      hgnc "NA"
      map_id "R1_63"
      name "S_minus_adenosyl_minus_L_minus_methionine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_182"
      uniprot "NA"
    ]
    graphics [
      x 975.961421394078
      y 1249.8176345766728
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9694619;urn:miriam:refseq:NC_004718.3;urn:miriam:reactome:R-COV-9684636"
      hgnc "NA"
      map_id "R1_74"
      name "m7G(5')pppAm_minus_capped_space_SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand)"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_1914"
      uniprot "NA"
    ]
    graphics [
      x 854.9146836021694
      y 885.5079598040927
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-ALL-9685614"
      hgnc "NA"
      map_id "R1_92"
      name "CQ,_space_HCQ"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_2035"
      uniprot "NA"
    ]
    graphics [
      x 982.7664548613939
      y 913.893721914123
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683478;PUBMED:21124966"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_285"
      name "CQ, HCQ diffuses from cytosol to endocytic vesicle lumen"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683478__layout_2034"
      uniprot "NA"
    ]
    graphics [
      x 1147.219072331645
      y 838.2058178859679
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_285"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-ALL-9685610"
      hgnc "NA"
      map_id "R1_90"
      name "CQ,_space_HCQ"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_2029"
      uniprot "NA"
    ]
    graphics [
      x 1416.0176492697553
      y 764.5530048645535
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:pubmed:15848177;urn:miriam:reactome:R-COV-9682916;urn:miriam:reactome:R-COV-9729340;urn:miriam:uniprot:P59595"
      hgnc "NA"
      map_id "R1_184"
      name "SUMO1_minus_K62_minus_ADPr_minus_p_minus_S177_minus_N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_395"
      uniprot "UNIPROT:P59595"
    ]
    graphics [
      x 642.6033551166811
      y 1854.509207726791
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_184"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683634;PUBMED:15094372;PUBMED:15848177;PUBMED:23717688"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_291"
      name "Dimerisation of nucleoprotein"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683634__layout_260"
      uniprot "NA"
    ]
    graphics [
      x 420.437686996357
      y 2034.167072764561
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_291"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:pubmed:15094372;urn:miriam:reactome:R-COV-9683649;urn:miriam:uniprot:P59595"
      hgnc "NA"
      map_id "R1_131"
      name "SUMO1_minus_K62_minus_p_minus_S177_minus_N_space_dimer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_261"
      uniprot "UNIPROT:P59595"
    ]
    graphics [
      x 371.6310873701891
      y 2196.3219695395856
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_131"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9684875;urn:miriam:reactome:R-COV-9694569;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_233"
      name "N_minus_glycan_space_nsp4"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_66"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 858.9457338270629
      y 515.552267976161
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_233"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9686015;PUBMED:20542253;PUBMED:24991833;PUBMED:28738245;PUBMED:23943763"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_356"
      name "Nsp3, nsp4, and nsp6 produce replicative organelles"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9686015__layout_1487"
      uniprot "NA"
    ]
    graphics [
      x 666.2242265037173
      y 546.9004711964817
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_356"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9694361;urn:miriam:reactome:R-COV-9682227;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_40"
      name "nsp6"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_1488"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 539.6316836951847
      y 553.336405630075
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9686003;urn:miriam:reactome:R-COV-9694366;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_200"
      name "nsp3:nsp4"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_449"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 764.6879252179458
      y 581.3098276898083
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_200"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9694770;urn:miriam:reactome:R-COV-9686011;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_202"
      name "nsp6"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_450"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 775.7328601796867
      y 656.3699904942932
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_202"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-SSC-9660621;urn:miriam:reactome:R-DME-9660621-2;urn:miriam:reactome:R-CFA-9660621;urn:miriam:reactome:R-DRE-9660621;urn:miriam:reactome:R-MMU-9660621;urn:miriam:reactome:R-DDI-9660621;urn:miriam:reactome:R-HSA-9660621;urn:miriam:reactome:R-XTR-9660621;urn:miriam:reactome:R-CEL-9660621;urn:miriam:uniprot:P06400;urn:miriam:reactome:R-RNO-9660621;urn:miriam:reactome:R-DME-9660621;urn:miriam:reactome:R-DME-9660621-3"
      hgnc "NA"
      map_id "R1_102"
      name "RB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_207"
      uniprot "UNIPROT:P06400"
    ]
    graphics [
      x 645.5846157964612
      y 1631.719737822146
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9682712;PUBMED:22301153"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_279"
      name "nsp15 binds RB1"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9682712__layout_206"
      uniprot "NA"
    ]
    graphics [
      x 715.4707717254511
      y 1838.1077088904494
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_279"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9694570;urn:miriam:pubmed:18045871;urn:miriam:pubmed:16882730;urn:miriam:pubmed:16828802;urn:miriam:pubmed:16216269;urn:miriam:reactome:R-COV-9682715;urn:miriam:pubmed:22301153;urn:miriam:pubmed:17409150;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_15"
      name "nsp15_space_hexamer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_117"
      uniprot "UNIPROT:P0C6X7"
    ]
    graphics [
      x 944.6366285560059
      y 2019.1665156051895
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-HSA-9694258;urn:miriam:uniprot:P06400;urn:miriam:reactome:R-HSA-9682726;urn:miriam:pubmed:22301153;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_104"
      name "nsp15:RB1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_208"
      uniprot "UNIPROT:P06400;UNIPROT:P0C6X7"
    ]
    graphics [
      x 539.6041051086015
      y 1810.0235273748367
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9682465;PUBMED:25197083"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_272"
      name "RTC completes synthesis of the minus strand genomic RNA complement"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9682465__layout_150"
      uniprot "NA"
    ]
    graphics [
      x 299.96275601679747
      y 1406.743906560855
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_272"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-ALL-6806881;urn:miriam:obo.chebi:CHEBI%3A61557"
      hgnc "NA"
      map_id "R1_41"
      name "NTP(4_minus_)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_149"
      uniprot "NA"
    ]
    graphics [
      x 144.99468566568305
      y 1371.5738540259908
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9697431;urn:miriam:reactome:R-COV-9685518;urn:miriam:refseq:NC_004718.3;urn:miriam:reactome:R-COV-9694393"
      hgnc "NA"
      map_id "R1_73"
      name "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand)"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_1905"
      uniprot "NA"
    ]
    graphics [
      x 691.0561704427682
      y 1276.7864537131202
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9713316;urn:miriam:reactome:R-COV-9713636;urn:miriam:uniprot:P0C6U8;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_103"
      name "SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_complement_space_(minus_space_strand):RTC"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2074"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 62.5
      y 1402.6438054479804
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:O15393;urn:miriam:reactome:R-HSA-9686707"
      hgnc "NA"
      map_id "R1_256"
      name "TMPRSS2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_893"
      uniprot "UNIPROT:O15393"
    ]
    graphics [
      x 881.7061771624354
      y 2189.7827103411355
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_256"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9681514;PUBMED:14647384;PUBMED:32142651;PUBMED:21068237;PUBMED:27550352;PUBMED:27277342;PUBMED:24227843;PUBMED:28414992;PUBMED:22496216"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_265"
      name "TMPRSS2 binds TMPRSS2 inhibitors"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9681514__layout_1043"
      uniprot "NA"
    ]
    graphics [
      x 1005.4921800888341
      y 2276.4616376600625
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_265"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-ALL-9682035"
      hgnc "NA"
      map_id "R1_5"
      name "TMPRSS2_space_inhibitors"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_1044"
      uniprot "NA"
    ]
    graphics [
      x 1111.645473467434
      y 2193.55674473157
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-HSA-9681532;urn:miriam:uniprot:O15393"
      hgnc "NA"
      map_id "R1_6"
      name "TMPRSS2:TMPRSS2_space_inhibitors"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_1045"
      uniprot "UNIPROT:O15393"
    ]
    graphics [
      x 855.4689516513614
      y 2251.308317327189
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9694339;urn:miriam:refseq:NC_004718.3;urn:miriam:reactome:R-COV-9681658"
      hgnc "NA"
      map_id "R1_77"
      name "SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand)"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_1932"
      uniprot "NA"
    ]
    graphics [
      x 620.4271966075642
      y 1483.6926949606964
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9682009;PUBMED:22362731"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_270"
      name "ZCRB1 binds 5'UTR of SARS-CoV-1 genomic RNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9682009__layout_200"
      uniprot "NA"
    ]
    graphics [
      x 504.432093319924
      y 1614.2668043836857
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_270"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-HSA-9682010;urn:miriam:uniprot:Q8TBF4"
      hgnc "NA"
      map_id "R1_87"
      name "ZCRB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_201"
      uniprot "UNIPROT:Q8TBF4"
    ]
    graphics [
      x 555.5276855144889
      y 1735.8291356110117
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-HSA-9682008;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:Q8TBF4"
      hgnc "NA"
      map_id "R1_88"
      name "ZCRB1:SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand)"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_202"
      uniprot "UNIPROT:Q8TBF4"
    ]
    graphics [
      x 548.7619120332942
      y 1469.3500989669742
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694787;urn:miriam:reactome:R-COV-9683623"
      hgnc "NA"
      map_id "R1_174"
      name "Ub_minus_3xPalmC_minus_E_space_pentamer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_371"
      uniprot "UNIPROT:P59637"
    ]
    graphics [
      x 557.0911437005548
      y 2143.183903580439
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_174"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683635;PUBMED:21524776;PUBMED:21450821"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_292"
      name "E pentamer is transported to the Golgi"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683635__layout_372"
      uniprot "NA"
    ]
    graphics [
      x 597.1612744310232
      y 2070.020040108835
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_292"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9683621;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694408"
      hgnc "NA"
      map_id "R1_175"
      name "Ub_minus_3xPalmC_minus_E_space_pentamer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_373"
      uniprot "UNIPROT:P59637"
    ]
    graphics [
      x 694.8567530248085
      y 1968.5613945147297
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_175"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-GGA-5205651;urn:miriam:uniprot:Q9GZQ8;urn:miriam:reactome:R-DDI-5205651-2;urn:miriam:reactome:R-DDI-5205651-3;urn:miriam:reactome:R-RNO-5205651;urn:miriam:reactome:R-CEL-5205651;urn:miriam:reactome:R-PFA-5205651;urn:miriam:reactome:R-BTA-5205651;urn:miriam:reactome:R-HSA-5205651;urn:miriam:reactome:R-DDI-5205651;urn:miriam:reactome:R-SCE-5205651;urn:miriam:reactome:R-SSC-5205651;urn:miriam:reactome:R-MMU-5205651;urn:miriam:reactome:R-DRE-5205651;urn:miriam:reactome:R-CFA-5205651;urn:miriam:reactome:R-SPO-5205651"
      hgnc "NA"
      map_id "R1_42"
      name "MAP1LC3B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_1492"
      uniprot "UNIPROT:Q9GZQ8"
    ]
    graphics [
      x 619.1253821856701
      y 1379.0069734291283
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9687435;PUBMED:31231549;PUBMED:15331731;PUBMED:21799305;PUBMED:25135833"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_369"
      name "Enhanced autophagosome formation"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9687435__layout_1506"
      uniprot "NA"
    ]
    graphics [
      x 648.2930892058383
      y 1096.4922923789609
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_369"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:P59636;urn:miriam:reactome:R-COV-9689378;urn:miriam:reactome:R-COV-9694649"
      hgnc "NA"
      map_id "R1_71"
      name "9b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_1900"
      uniprot "UNIPROT:P59636"
    ]
    graphics [
      x 621.0785684787686
      y 975.5124012495194
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:Q80H93;urn:miriam:reactome:R-COV-9678168"
      hgnc "NA"
      map_id "R1_44"
      name "8b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_1496"
      uniprot "UNIPROT:Q80H93"
    ]
    graphics [
      x 584.9053342210796
      y 1212.9405543217786
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:Q99570;urn:miriam:reactome:R-CEL-5683632;urn:miriam:reactome:R-HSA-5683632;urn:miriam:reactome:R-XTR-5683632;urn:miriam:reactome:R-DME-5683632;urn:miriam:reactome:R-RNO-5683632;urn:miriam:reactome:R-SPO-5683632;urn:miriam:reactome:R-MMU-5683632;urn:miriam:reactome:R-DDI-5683632;urn:miriam:reactome:R-CFA-5683632;urn:miriam:reactome:R-GGA-5683632;urn:miriam:reactome:R-DRE-5683632;urn:miriam:reactome:R-SCE-5683632;urn:miriam:uniprot:Q14457;urn:miriam:reactome:R-SSC-5683632;urn:miriam:uniprot:Q9P2Y5;urn:miriam:uniprot:Q8NEB9"
      hgnc "NA"
      map_id "R1_57"
      name "UVRAG_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_1644"
      uniprot "UNIPROT:Q99570;UNIPROT:Q14457;UNIPROT:Q9P2Y5;UNIPROT:Q8NEB9"
    ]
    graphics [
      x 481.5926986432195
      y 1097.2233452675039
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-XTR-917723;urn:miriam:uniprot:Q9UQN3;urn:miriam:reactome:R-DDI-917723;urn:miriam:reactome:R-DME-917723;urn:miriam:uniprot:Q8WUX9;urn:miriam:reactome:R-SSC-917723;urn:miriam:reactome:R-GGA-917723;urn:miriam:uniprot:Q96CF2;urn:miriam:reactome:R-RNO-917723;urn:miriam:uniprot:Q9Y3E7;urn:miriam:uniprot:Q9BY43;urn:miriam:reactome:R-DRE-917723;urn:miriam:reactome:R-HSA-917723;urn:miriam:reactome:R-CFA-917723;urn:miriam:reactome:R-BTA-917723;urn:miriam:uniprot:Q9H444;urn:miriam:uniprot:Q96FZ7;urn:miriam:reactome:R-SPO-917723;urn:miriam:reactome:R-MMU-917723;urn:miriam:uniprot:O43633;urn:miriam:reactome:R-SCE-917723;urn:miriam:reactome:R-CEL-917723"
      hgnc "NA"
      map_id "R1_56"
      name "ESCRT_minus_III"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_1643"
      uniprot "UNIPROT:Q9UQN3;UNIPROT:Q8WUX9;UNIPROT:Q96CF2;UNIPROT:Q9Y3E7;UNIPROT:Q9BY43;UNIPROT:Q9H444;UNIPROT:Q96FZ7;UNIPROT:O43633"
    ]
    graphics [
      x 478.9471459812406
      y 1011.3435392520896
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9684354;urn:miriam:reactome:R-COV-9694668;urn:miriam:uniprot:P0C6U8"
      hgnc "NA"
      map_id "R1_11"
      name "pp1a_minus_nsp6_minus_11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_11"
      uniprot "UNIPROT:P0C6U8"
    ]
    graphics [
      x 1392.2300189921853
      y 1027.4070216483256
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-CEL-5683641;urn:miriam:reactome:R-DDI-5683641-3;urn:miriam:reactome:R-DDI-5683641-2;urn:miriam:reactome:R-RNO-5683641;urn:miriam:uniprot:Q9GZQ8;urn:miriam:reactome:R-HSA-5683641;urn:miriam:reactome:R-SPO-5683641;urn:miriam:reactome:R-CFA-5683641;urn:miriam:reactome:R-GGA-5683641;urn:miriam:reactome:R-MMU-5683641;urn:miriam:reactome:R-DRE-5683641;urn:miriam:reactome:R-DDI-5683641;urn:miriam:reactome:R-PFA-5683641;urn:miriam:reactome:R-SCE-5683641;urn:miriam:reactome:R-SSC-5683641;urn:miriam:reactome:R-BTA-5683641"
      hgnc "NA"
      map_id "R1_46"
      name "MAP1LC3B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_1507"
      uniprot "UNIPROT:Q9GZQ8"
    ]
    graphics [
      x 500.5682243532965
      y 1052.659077096343
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9684199;urn:miriam:reactome:R-COV-9697428;urn:miriam:reactome:R-COV-9694612;urn:miriam:uniprot:P59595;urn:miriam:refseq:NC_004718.3"
      hgnc "NA"
      map_id "R1_222"
      name "encapsidated_space_SARS_space_coronavirus_space_genomic_space_RNA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_598"
      uniprot "UNIPROT:P59595"
    ]
    graphics [
      x 592.4029694647941
      y 1896.9314379430593
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_222"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684238;PUBMED:16684538;PUBMED:18703211;PUBMED:19322648;PUBMED:16254320;PUBMED:15474033;PUBMED:15147946;PUBMED:18792806;PUBMED:31226023;PUBMED:16877062;PUBMED:16507314;PUBMED:17530462;PUBMED:15713601;PUBMED:15351485;PUBMED:31133031;PUBMED:25855243;PUBMED:18753196;PUBMED:16343974;PUBMED:15507643"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_326"
      name "E and N are recruited to the M lattice"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684238__layout_603"
      uniprot "NA"
    ]
    graphics [
      x 821.6900033835767
      y 1755.0213931795915
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_326"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9684216;urn:miriam:uniprot:P59596;urn:miriam:reactome:R-COV-9694371"
      hgnc "NA"
      map_id "R1_225"
      name "M_space_lattice"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_602"
      uniprot "UNIPROT:P59596"
    ]
    graphics [
      x 827.1127951309373
      y 1275.632545597267
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_225"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9684226;urn:miriam:uniprot:P59637;urn:miriam:uniprot:P59595;urn:miriam:refseq:NC_004718.3;urn:miriam:reactome:R-COV-9694491;urn:miriam:uniprot:P59596"
      hgnc "NA"
      map_id "R1_226"
      name "M_space_lattice:E_space_protein:encapsidated_space_SARS_space_coronavirus_space_genomic_space_RNA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_605"
      uniprot "UNIPROT:P59637;UNIPROT:P59595;UNIPROT:P59596"
    ]
    graphics [
      x 1136.896175028915
      y 1640.5933691383839
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_226"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9713317;urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9713652;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P0C6X7;urn:miriam:obo.chebi:CHEBI%3A33019;urn:miriam:reactome:R-ALL-111294"
      hgnc "NA"
      map_id "R1_109"
      name "SARS_minus_CoV_minus_1_space_minus_space_strand_space_subgenomic_space_mRNAs:RTC"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2097"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 1714.2030197452705
      y 1803.9638116691908
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9685681;PUBMED:12917450;PUBMED:12927536;PUBMED:14569023;PUBMED:26919232"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_349"
      name "Synthesis of SARS-CoV-1 plus strand subgenomic mRNAs"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9685681__layout_415"
      uniprot "NA"
    ]
    graphics [
      x 1719.0118023166742
      y 1981.9639575008462
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_349"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-ALL-6806881;urn:miriam:obo.chebi:CHEBI%3A61557"
      hgnc "NA"
      map_id "R1_188"
      name "NTP(4_minus_)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_414"
      uniprot "NA"
    ]
    graphics [
      x 1850.510838777784
      y 1995.501764077478
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_188"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9713314;urn:miriam:reactome:R-COV-9713653;urn:miriam:uniprot:P0C6U8;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_110"
      name "SARS_minus_CoV_minus_1_space_plus_space_strand_space_subgenomic_space_mRNAs:RTC"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2099"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 1614.9880704889115
      y 1892.281731388911
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A33019;urn:miriam:reactome:R-ALL-111294"
      hgnc "NA"
      map_id "R1_186"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_407"
      uniprot "NA"
    ]
    graphics [
      x 1497.5754232916313
      y 1921.6172361903666
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_186"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9694383;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7;urn:miriam:reactome:R-COV-9682229"
      hgnc "NA"
      map_id "R1_239"
      name "nsp9"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_71"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 835.7783875461482
      y 2564.2686236216127
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_239"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684282;PUBMED:19153232"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_332"
      name "nsp9 forms a homodimer"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684282__layout_70"
      uniprot "NA"
    ]
    graphics [
      x 786.0147756197985
      y 2462.7895815706943
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_332"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9684862;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_246"
      name "nsp9_space_dimer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_74"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 785.4846416526136
      y 2294.775802701204
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_246"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9694446;urn:miriam:uniprot:P59596;urn:miriam:reactome:R-COV-9684206"
      hgnc "NA"
      map_id "R1_224"
      name "N_minus_glycan_space_M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_601"
      uniprot "UNIPROT:P59596"
    ]
    graphics [
      x 996.2573072420209
      y 557.7001733864633
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_224"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684218;PUBMED:16442106;PUBMED:18703211;PUBMED:9658133;PUBMED:16254320;PUBMED:20154085;PUBMED:7721788;PUBMED:15474033;PUBMED:19534833;PUBMED:15147946;PUBMED:16877062;PUBMED:10799570;PUBMED:18753196;PUBMED:23700447;PUBMED:15507643"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_323"
      name "M protein oligomerization"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684218__layout_599"
      uniprot "NA"
    ]
    graphics [
      x 885.9593322692167
      y 792.4027987667154
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_323"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9684213;urn:miriam:uniprot:P59596;urn:miriam:reactome:R-COV-9694439"
      hgnc "NA"
      map_id "R1_223"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_600"
      uniprot "UNIPROT:P59596"
    ]
    graphics [
      x 888.2424948337984
      y 482.14749162109945
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_223"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:P59632;urn:miriam:uniprot:P59635;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694585;urn:miriam:uniprot:P59594;urn:miriam:reactome:R-COV-9686703;urn:miriam:uniprot:P59595;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P59596"
      hgnc "NA"
      map_id "R1_253"
      name "S1:S2:M:E:encapsidated_space_SARS_space_coronavirus_space_genomic_space_RNA:_space_7a:O_minus_glycosyl_space_3a_space_tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_842"
      uniprot "UNIPROT:P59632;UNIPROT:P59635;UNIPROT:P59637;UNIPROT:P59594;UNIPROT:P59595;UNIPROT:P59596"
    ]
    graphics [
      x 707.7090966161977
      y 1909.5212395818298
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_253"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9686699;PUBMED:26311884"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_359"
      name "Fusion and Release of SARS-CoV-1 Nucleocapsid"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9686699__layout_861"
      uniprot "NA"
    ]
    graphics [
      x 630.2075479805169
      y 2231.4832333980703
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_359"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-CEL-1181251;urn:miriam:reactome:R-HSA-1181251;urn:miriam:reactome:R-XTR-1181251;urn:miriam:reactome:R-CFA-1181251;urn:miriam:reactome:R-GGA-1181251;urn:miriam:reactome:R-MMU-1181251;urn:miriam:reactome:R-RNO-1181251;urn:miriam:reactome:R-DRE-1181251;urn:miriam:reactome:R-DDI-1181251;urn:miriam:reactome:R-PFA-1181251;urn:miriam:uniprot:P55072;urn:miriam:reactome:R-SPO-1181251;urn:miriam:reactome:R-SSC-1181251;urn:miriam:reactome:R-SCE-1181251;urn:miriam:reactome:R-DME-1181251"
      hgnc "NA"
      map_id "R1_55"
      name "VCP"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_1642"
      uniprot "UNIPROT:P55072"
    ]
    graphics [
      x 636.1112816768837
      y 2377.31042317391
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:P59632;urn:miriam:uniprot:P59635;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694534;urn:miriam:uniprot:P59594;urn:miriam:uniprot:P59596;urn:miriam:reactome:R-COV-9686705"
      hgnc "NA"
      map_id "R1_254"
      name "S1:S2:M:E:_space_7a:O_minus_glycosyl_space_3a_space_tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_862"
      uniprot "UNIPROT:P59632;UNIPROT:P59635;UNIPROT:P59637;UNIPROT:P59594;UNIPROT:P59596"
    ]
    graphics [
      x 557.2314208458351
      y 2362.0275134323597
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_254"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9694461;urn:miriam:reactome:R-COV-9686697;urn:miriam:uniprot:P59595;urn:miriam:refseq:NC_004718.3"
      hgnc "NA"
      map_id "R1_255"
      name "encapsidated_space_SARS_space_coronavirus_space_genomic_space_RNA_space_(plus_space_strand)"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_863"
      uniprot "UNIPROT:P59595"
    ]
    graphics [
      x 481.08731936699144
      y 2280.817064566544
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_255"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:P59632;urn:miriam:uniprot:P59635;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-HSA-9694758;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:P59594;urn:miriam:uniprot:P59595;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P59596;urn:miriam:reactome:R-HSA-9686692;urn:miriam:uniprot:P59632;urn:miriam:uniprot:P59635;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694585;urn:miriam:uniprot:P59594;urn:miriam:reactome:R-COV-9686703;urn:miriam:uniprot:P59595;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P59596"
      hgnc "NA"
      map_id "R1_249"
      name "S3:M:E:encapsidated_space_SARS_space_coronavirus_space_genomic_space_RNA:_space_7a:O_minus_glycosyl_space_3a_space_tetramer:glycosylated_minus_ACE2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_795"
      uniprot "UNIPROT:P59632;UNIPROT:P59635;UNIPROT:P59637;UNIPROT:Q9BYF1;UNIPROT:P59594;UNIPROT:P59595;UNIPROT:P59596"
    ]
    graphics [
      x 816.0243687406411
      y 1486.9215308824582
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_249"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9686710;PUBMED:22816037;PUBMED:19321428"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_361"
      name "Cleavage of S protein into S1:S2"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9686710__layout_835"
      uniprot "NA"
    ]
    graphics [
      x 721.6709111289329
      y 1431.0790346945103
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_361"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:P07711;urn:miriam:reactome:R-HSA-9686717"
      hgnc "NA"
      map_id "R1_252"
      name "Cathepsin_space_L1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_837"
      uniprot "UNIPROT:P07711"
    ]
    graphics [
      x 678.1173428833483
      y 1171.530585035039
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_252"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-HSA-9683316;urn:miriam:uniprot:P07711;urn:miriam:pubchem.compound:3767"
      hgnc "NA"
      map_id "R1_3"
      name "CTSL:CTSL_space_inhibitors"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_1038"
      uniprot "UNIPROT:P07711"
    ]
    graphics [
      x 775.4681718937331
      y 1181.7386817584588
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:pubmed:14754895;urn:miriam:uniprot:Q9BYF1;urn:miriam:reactome:R-HSA-9683480"
      hgnc "NA"
      map_id "R1_251"
      name "glycosylated_minus_ACE2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_836"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 861.9097125762097
      y 1475.4810607750376
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_251"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9687121;PUBMED:15331731"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_366"
      name "nsp8 binds MAP1LC3B"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9687121__layout_1491"
      uniprot "NA"
    ]
    graphics [
      x 783.4939394342251
      y 1647.9916983685005
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_366"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9682241;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_259"
      name "nsp8"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_98"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 1076.012618811042
      y 1762.5743838420804
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_259"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-HSA-9694566;urn:miriam:reactome:R-HSA-9687117;urn:miriam:uniprot:Q9GZQ8;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_43"
      name "nsp8:MAP1LC3B"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_1493"
      uniprot "UNIPROT:Q9GZQ8;UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 637.6022317019781
      y 1694.5359280968205
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9682253;urn:miriam:reactome:R-COV-9694404;urn:miriam:uniprot:P0C6U8;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_58"
      name "SARS_minus_CoV_minus_1_space_gRNA_space_complement_space_(minus_space_strand):RTC"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_170"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 1074.7340036424785
      y 990.5517886415257
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9687384;PUBMED:31645453;PUBMED:32258351;PUBMED:32284326;PUBMED:29511076;PUBMED:32253226;PUBMED:31233808;PUBMED:32094225;PUBMED:28124907"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_367"
      name "SARS-CoV-1 gRNA complement (minus strand):RTC binds RTC inhibitors"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9687384__layout_1484"
      uniprot "NA"
    ]
    graphics [
      x 1246.2758418075073
      y 900.8308468986429
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_367"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-ALL-9687408"
      hgnc "NA"
      map_id "R1_39"
      name "RTC_space_inhibitors"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_1486"
      uniprot "NA"
    ]
    graphics [
      x 1344.4333280045005
      y 779.5096181246727
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:P0C6U8;urn:miriam:refseq:NC_004718.3;urn:miriam:reactome:R-COV-9694690;urn:miriam:reactome:R-COV-9687382;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_29"
      name "SARS_minus_CoV_minus_1_space_gRNA_space_complement_space_(minus_space_strand):RTC:RTC_space_inhibitors"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_1383"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 1176.3728320679738
      y 1045.970959059526
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-5228597"
      hgnc "NA"
      map_id "R1_89"
      name "H_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2028"
      uniprot "NA"
    ]
    graphics [
      x 1688.468279257187
      y 664.763988010457
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683467;PUBMED:9719345;PUBMED:32145363;PUBMED:16115318;PUBMED:28596841;PUBMED:25693996;PUBMED:32226290"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_284"
      name "CQ, HCQ are protonated to CQ2+, HCQ2+"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683467__layout_2027"
      uniprot "NA"
    ]
    graphics [
      x 1579.7614684222417
      y 595.758367340005
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_284"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:kegg.compound:24848403;urn:miriam:reactome:R-ALL-9685618;urn:miriam:kegg.compound:78435478"
      hgnc "NA"
      map_id "R1_91"
      name "CQ2_plus_,_space_HCQ2_plus_"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_2030"
      uniprot "NA"
    ]
    graphics [
      x 1501.5388315262853
      y 547.2614041665431
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:pubmed:31138817;urn:miriam:reactome:R-COV-9680807;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_7"
      name "nsp7:nsp8:nsp12"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_106"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 1152.5524297699862
      y 1984.2653383333436
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9682258;PUBMED:25197083;PUBMED:16549795"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_271"
      name "nsp14 binds nsp12"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9682258__layout_112"
      uniprot "NA"
    ]
    graphics [
      x 1027.054064340496
      y 1883.030473483469
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_271"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9682545;urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9694688;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_12"
      name "nsp10:nsp14"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_111"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 1219.9669035196641
      y 1745.7369260286978
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9694503;urn:miriam:reactome:R-COV-9685916;urn:miriam:refseq:NC_004718.3"
      hgnc "NA"
      map_id "R1_79"
      name "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA2"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_1969"
      uniprot "NA"
    ]
    graphics [
      x 869.0563868635761
      y 1332.4145633233659
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683624;PUBMED:12917450"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_289"
      name "mRNA2 is translated to Spike"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683624__layout_225"
      uniprot "NA"
    ]
    graphics [
      x 934.5367802237907
      y 1180.4302489080521
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_289"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9678128;PUBMED:14647384;PUBMED:16166518;PUBMED:32125455;PUBMED:22816037"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_260"
      name "Spike glycoprotein of SARS coronavirus binds ACE2 on host cell"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9678128__layout_712"
      uniprot "NA"
    ]
    graphics [
      x 333.1677295384385
      y 1338.2145938592469
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_260"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:P59632;urn:miriam:uniprot:P59635;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694500;urn:miriam:uniprot:P59594;urn:miriam:uniprot:P59595;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P59596;urn:miriam:reactome:R-COV-9685506"
      hgnc "NA"
      map_id "R1_244"
      name "S3:M:E:encapsidated_space_SARS_space_coronavirus_space_genomic_space_RNA:_space_7a:O_minus_glycosyl_space_3a_space_tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_729"
      uniprot "UNIPROT:P59632;UNIPROT:P59635;UNIPROT:P59637;UNIPROT:P59594;UNIPROT:P59595;UNIPROT:P59596"
    ]
    graphics [
      x 385.80056548343987
      y 1181.1470782565234
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_244"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:P59632;urn:miriam:uniprot:P59635;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-HSA-9686311;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:P59594;urn:miriam:uniprot:P59595;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P59596"
      hgnc "NA"
      map_id "R1_241"
      name "S3:M:E:encapsidated_space_SARS_space_coronavirus_space_genomic_space_RNA:_space_7a:O_minus_glycosyl_space_3a_space_tetramer:glycosylated_minus_ACE2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_714"
      uniprot "UNIPROT:P59632;UNIPROT:P59635;UNIPROT:P59637;UNIPROT:Q9BYF1;UNIPROT:P59594;UNIPROT:P59595;UNIPROT:P59596"
    ]
    graphics [
      x 495.23346278794213
      y 1722.2094241588297
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_241"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9683684;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694312"
      hgnc "NA"
      map_id "R1_122"
      name "nascent_space_E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_233"
      uniprot "UNIPROT:P59637"
    ]
    graphics [
      x 862.5209511779747
      y 1112.5172209122509
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_122"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683720;PUBMED:22548323;PUBMED:16507314"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_306"
      name "E protein gets palmitoylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683720__layout_361"
      uniprot "NA"
    ]
    graphics [
      x 470.7990396757996
      y 1306.8658594836631
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_306"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-ALL-9683736;urn:miriam:obo.chebi:CHEBI%3A15525"
      hgnc "NA"
      map_id "R1_146"
      name "palmitoyl_minus_CoA"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_291"
      uniprot "NA"
    ]
    graphics [
      x 472.77055133924046
      y 1482.7200243100779
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_146"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-ALL-162743;urn:miriam:obo.chebi:CHEBI%3A57287"
      hgnc "NA"
      map_id "R1_147"
      name "CoA_minus_SH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_292"
      uniprot "NA"
    ]
    graphics [
      x 343.7667426051099
      y 1535.5594544242283
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_147"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9682215;urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9694358;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_9"
      name "nsp10"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_108"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 1525.9529129818234
      y 1483.8924862963909
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9682544;PUBMED:25197083;PUBMED:22635272;PUBMED:25074927"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_273"
      name "nsp14 binds nsp10"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9682544__layout_107"
      uniprot "NA"
    ]
    graphics [
      x 1446.0865106207002
      y 1652.03346647142
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_273"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9678285;urn:miriam:reactome:R-COV-9694537;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_145"
      name "pp1ab_minus_nsp14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_29"
      uniprot "UNIPROT:P0C6X7"
    ]
    graphics [
      x 1661.8936545904235
      y 1620.8616358094444
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_145"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9681314;PUBMED:22791111"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_264"
      name "Replication transcription complex binds SARS-CoV-1 genomic RNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9681314__layout_122"
      uniprot "NA"
    ]
    graphics [
      x 983.4832775013475
      y 1415.3743386634244
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_264"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9694731;urn:miriam:reactome:R-COV-9682668;urn:miriam:uniprot:P0C6U8;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_19"
      name "SARS_space_coronavirus_space_gRNA_space_with_space_secondary_space_structure:RTC"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_124"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 1297.8753024793443
      y 1688.9649568926566
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683670;PUBMED:15522242;PUBMED:22819936"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_299"
      name "Protein E forms a homopentamer"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683670__layout_370"
      uniprot "NA"
    ]
    graphics [
      x 538.5337172546908
      y 1961.2785230277777
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_299"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9697431;urn:miriam:reactome:R-COV-9685518;urn:miriam:refseq:NC_004718.3;urn:miriam:reactome:R-COV-9694393"
      hgnc "NA"
      map_id "R1_72"
      name "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand)"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_1903"
      uniprot "NA"
    ]
    graphics [
      x 1557.3921709989856
      y 1578.6452406095814
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684277;PUBMED:15680415"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_331"
      name "mRNA1 is translated to pp1ab"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684277__layout_4"
      uniprot "NA"
    ]
    graphics [
      x 1602.049259965725
      y 1779.9796322214104
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_331"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9684355;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_208"
      name "pp1ab"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_5"
      uniprot "UNIPROT:P0C6X7"
    ]
    graphics [
      x 1745.2060401748724
      y 1694.7566637037705
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_208"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9681840;PUBMED:25197083;PUBMED:22791111"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_269"
      name "RTC synthesizes SARS-CoV-1 plus strand genomic RNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9681840__layout_171"
      uniprot "NA"
    ]
    graphics [
      x 960.5305909332453
      y 1059.3957562983483
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_269"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-ALL-6806881;urn:miriam:obo.chebi:CHEBI%3A61557"
      hgnc "NA"
      map_id "R1_60"
      name "NTP(4_minus_)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_174"
      uniprot "NA"
    ]
    graphics [
      x 924.6600844581602
      y 1126.5729421242672
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9713644;urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9713311;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P0C6X7;urn:miriam:reactome:R-COV-9694339;urn:miriam:refseq:NC_004718.3;urn:miriam:reactome:R-COV-9681658"
      hgnc "NA"
      map_id "R1_106"
      name "SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand):RTC"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2086"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 1130.1813561672413
      y 1152.0622655754503
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A33019;urn:miriam:reactome:R-ALL-111294"
      hgnc "NA"
      map_id "R1_247"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_745"
      uniprot "NA"
    ]
    graphics [
      x 1086.9053680164727
      y 1076.300585237571
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_247"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9685597;PUBMED:22791111"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_346"
      name "RTC binds SARS-CoV-1 genomic RNA complement (minus strand)"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9685597__layout_169"
      uniprot "NA"
    ]
    graphics [
      x 869.810628867724
      y 1036.459937350018
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_346"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9694300;urn:miriam:pubmed:15496142;urn:miriam:uniprot:P59595;urn:miriam:pubmed:12775768;urn:miriam:reactome:R-COV-9683625"
      hgnc "NA"
      map_id "R1_124"
      name "N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_239"
      uniprot "UNIPROT:P59595"
    ]
    graphics [
      x 1719.4680239300287
      y 591.0269774991781
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_124"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683714;PUBMED:16103198"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_303"
      name "Unphosphorylated nucleoprotein translocates to the plasma membrane"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683714__layout_2264"
      uniprot "NA"
    ]
    graphics [
      x 1629.5211882756591
      y 678.0880698473306
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_303"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9694356;urn:miriam:uniprot:P59595;urn:miriam:reactome:R-COV-9683611;urn:miriam:pubmed:12775768"
      hgnc "NA"
      map_id "R1_118"
      name "N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2265"
      uniprot "UNIPROT:P59595"
    ]
    graphics [
      x 1578.8365221880665
      y 831.8668413267542
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_118"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 158
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9686711;PUBMED:15163706;PUBMED:15010527;PUBMED:22816037;PUBMED:15140961"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_362"
      name "Endocytois of SARS-CoV-1 Virion"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9686711__layout_794"
      uniprot "NA"
    ]
    graphics [
      x 687.1193416723395
      y 1649.209016559942
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_362"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 159
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:reactome:R-ALL-29356"
      hgnc "NA"
      map_id "R1_107"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2088"
      uniprot "NA"
    ]
    graphics [
      x 1273.5176389344047
      y 1404.0049967796888
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 160
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684017;PUBMED:15220459;PUBMED:19208801;PUBMED:12456663;PUBMED:6165837"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_318"
      name "nsp14 acts as a cap N7 methyltransferase to modify SARS-CoV-1 gRNA (plus strand)"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684017__layout_177"
      uniprot "NA"
    ]
    graphics [
      x 1194.1059215680832
      y 1313.3169822313614
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_318"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 161
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A37565;urn:miriam:reactome:R-ALL-29438"
      hgnc "NA"
      map_id "R1_64"
      name "GTP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_183"
      uniprot "NA"
    ]
    graphics [
      x 1368.8862809330424
      y 1334.2841382362644
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 162
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A43474;urn:miriam:reactome:R-ALL-29372"
      hgnc "NA"
      map_id "R1_62"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_181"
      uniprot "NA"
    ]
    graphics [
      x 1340.6093834247656
      y 1462.0196188433115
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 163
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16680;urn:miriam:reactome:R-ALL-9639443"
      hgnc "NA"
      map_id "R1_65"
      name "S_minus_adenosyl_minus_L_minus_homocysteine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_184"
      uniprot "NA"
    ]
    graphics [
      x 1343.1879924382788
      y 1413.4320109067512
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 164
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A33019;urn:miriam:reactome:R-ALL-111294"
      hgnc "NA"
      map_id "R1_232"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_649"
      uniprot "NA"
    ]
    graphics [
      x 1278.1024624024278
      y 1463.7447731204509
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_232"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 165
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9683676;urn:miriam:pubmed:15367599;urn:miriam:uniprot:P59594"
      hgnc "NA"
      map_id "R1_151"
      name "trimmed_space_N_minus_glycan_minus_PALM_minus_Spike_space_trimer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_300"
      uniprot "UNIPROT:P59594"
    ]
    graphics [
      x 1573.7300747918216
      y 1991.5081821169392
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_151"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 166
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683764;PUBMED:31226023"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_313"
      name "Spike trimer translocates to ERGIC"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683764__layout_301"
      uniprot "NA"
    ]
    graphics [
      x 1800.9869686482616
      y 1963.8811997513878
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_313"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 167
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9683676;urn:miriam:pubmed:15367599;urn:miriam:uniprot:P59594"
      hgnc "NA"
      map_id "R1_152"
      name "trimmed_space_N_minus_glycan_minus_PALM_minus_Spike_space_trimer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_302"
      uniprot "UNIPROT:P59594"
    ]
    graphics [
      x 1986.0121121955654
      y 1925.7042338359383
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_152"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 168
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:reactome:R-ALL-29356"
      hgnc "NA"
      map_id "R1_37"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_147"
      uniprot "NA"
    ]
    graphics [
      x 379.43361175034465
      y 1015.037379394636
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 169
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9682603;PUBMED:25197083;PUBMED:17927896;PUBMED:20463816;PUBMED:22635272;PUBMED:16549795;PUBMED:25074927"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_275"
      name "nsp14 acts as a 3'-to-5' exonuclease to remove misincorporated nucleotides from nascent RNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9682603__layout_146"
      uniprot "NA"
    ]
    graphics [
      x 490.9612739164651
      y 1152.1900040002665
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_275"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 170
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9694269;urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9682566;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_35"
      name "SARS_space_coronavirus_space_gRNA:RTC:nascent_space_RNA_space_minus_space_strand_space_with_space_mismatched_space_nucleotide"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_144"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 602.1420502748244
      y 1299.3071640901003
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 171
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-ALL-9689912;urn:miriam:obo.chebi:CHEBI%3A26558"
      hgnc "NA"
      map_id "R1_85"
      name "NMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_1998"
      uniprot "NA"
    ]
    graphics [
      x 503.3427395164688
      y 957.449095093793
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 172
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:pubmed:20129637;urn:miriam:pubmed:15253436;urn:miriam:pubmed:17715238;urn:miriam:reactome:R-COV-9683608;urn:miriam:pubmed:16122388;urn:miriam:uniprot:P59594;urn:miriam:pubmed:12775768;urn:miriam:pubmed:14760722"
      hgnc "NA"
      map_id "R1_143"
      name "trimmed_space_N_minus_glycan_space_Spike"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_287"
      uniprot "UNIPROT:P59594"
    ]
    graphics [
      x 882.6908255651915
      y 1555.4677135047555
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_143"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 173
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683653;PUBMED:17134730;PUBMED:20580052"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_294"
      name "Spike protein gets palmitoylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683653__layout_290"
      uniprot "NA"
    ]
    graphics [
      x 587.2886070027769
      y 1669.7839633203862
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_294"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 174
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:pubmed:20129637;urn:miriam:pubmed:15253436;urn:miriam:pubmed:17715238;urn:miriam:pubmed:16122388;urn:miriam:reactome:R-COV-9683638;urn:miriam:uniprot:P59594;urn:miriam:pubmed:12775768;urn:miriam:pubmed:14760722"
      hgnc "NA"
      map_id "R1_148"
      name "trimmed_space_N_minus_glycan_minus_PALM_minus_Spike"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_293"
      uniprot "UNIPROT:P59594"
    ]
    graphics [
      x 927.2474252391053
      y 1882.1059138425808
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_148"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 175
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-HSA-4656922;urn:miriam:reactome:R-XTR-4656922;urn:miriam:reactome:R-RNO-4656922;urn:miriam:reactome:R-MMU-4656922;urn:miriam:uniprot:P63279;urn:miriam:reactome:R-DRE-4656922;urn:miriam:uniprot:P63165"
      hgnc "NA"
      map_id "R1_68"
      name "SUMO1:C93_minus_UBE2I"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_1896"
      uniprot "UNIPROT:P63279;UNIPROT:P63165"
    ]
    graphics [
      x 932.2013306904379
      y 1498.8431200338496
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 176
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683687;PUBMED:15848177"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_301"
      name "Nucleoprotein is SUMOylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683687__layout_257"
      uniprot "NA"
    ]
    graphics [
      x 909.8395268256207
      y 1679.0547466210003
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_301"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 177
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9729275;urn:miriam:reactome:R-COV-9686058;urn:miriam:uniprot:P59595"
      hgnc "NA"
      map_id "R1_183"
      name "ADPr_minus_p_minus_S177_minus_N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_394"
      uniprot "UNIPROT:P59595"
    ]
    graphics [
      x 1186.0641664097648
      y 1792.7071875495908
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_183"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 178
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-DRE-4655342;urn:miriam:reactome:R-MMU-4655403;urn:miriam:uniprot:P63279;urn:miriam:reactome:R-DME-4655342;urn:miriam:reactome:R-RNO-4655342;urn:miriam:reactome:R-HSA-4655342;urn:miriam:reactome:R-XTR-4655342"
      hgnc "NA"
      map_id "R1_130"
      name "UBE2I"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_259"
      uniprot "UNIPROT:P63279"
    ]
    graphics [
      x 941.4696080473436
      y 1583.1039330599153
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_130"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 179
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9694573;urn:miriam:reactome:R-COV-9684203;urn:miriam:reactome:R-COV-9697429;urn:miriam:uniprot:P59595"
      hgnc "NA"
      map_id "R1_220"
      name "SUMO1_minus_K62_minus_p_minus_S177_minus_N_space_dimer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_590"
      uniprot "UNIPROT:P59595"
    ]
    graphics [
      x 496.05040779338356
      y 1888.5815162474362
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_220"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 180
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684234;PUBMED:24418573;PUBMED:23717688;PUBMED:17229691;PUBMED:17379242;PUBMED:16873249"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_325"
      name "Encapsidation of SARS coronavirus genomic RNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684234__layout_597"
      uniprot "NA"
    ]
    graphics [
      x 391.4393606038277
      y 1928.9049085254537
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_325"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 181
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9684230;urn:miriam:reactome:R-COV-9694402;urn:miriam:uniprot:P59595;urn:miriam:refseq:NC_004718.3"
      hgnc "NA"
      map_id "R1_221"
      name "SUMO_minus_p_minus_N_space_dimer:SARS_space_coronavirus_space_genomic_space_RNA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_591"
      uniprot "UNIPROT:P59595"
    ]
    graphics [
      x 446.0906494218301
      y 1811.6900440941465
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_221"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 182
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:P17844;urn:miriam:reactome:R-HSA-9682643"
      hgnc "NA"
      map_id "R1_93"
      name "DDX5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_204"
      uniprot "UNIPROT:P17844"
    ]
    graphics [
      x 1253.6971103883886
      y 1821.1619356782412
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 183
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9682631;PUBMED:19224332"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_277"
      name "nsp13 binds DDX5"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9682631__layout_203"
      uniprot "NA"
    ]
    graphics [
      x 1280.7627950148012
      y 1926.469191487347
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_277"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 184
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:pubmed:19224332;urn:miriam:reactome:R-HSA-9682634;urn:miriam:uniprot:P17844;urn:miriam:uniprot:P0C6X7;urn:miriam:reactome:R-HSA-9694692"
      hgnc "NA"
      map_id "R1_94"
      name "nsp13:DDX5"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_205"
      uniprot "UNIPROT:P17844;UNIPROT:P0C6X7"
    ]
    graphics [
      x 1369.7808499028456
      y 1979.9689170669026
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 185
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9685920;urn:miriam:reactome:R-COV-9694456;urn:miriam:refseq:NC_004718.3"
      hgnc "NA"
      map_id "R1_82"
      name "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA5"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_1972"
      uniprot "NA"
    ]
    graphics [
      x 1102.327497629328
      y 212.00315272868647
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 186
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683622;PUBMED:12917450"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_288"
      name "mRNA5 is translated to protein M"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683622__layout_234"
      uniprot "NA"
    ]
    graphics [
      x 961.2450843325644
      y 248.943008570164
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_288"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 187
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9683586;urn:miriam:reactome:R-COV-9694279;urn:miriam:uniprot:P59596"
      hgnc "NA"
      map_id "R1_123"
      name "nascent_space_M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_236"
      uniprot "UNIPROT:P59596"
    ]
    graphics [
      x 827.1606804972884
      y 303.86723366038086
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_123"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 188
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684016;PUBMED:15220459;PUBMED:19208801;PUBMED:12456663;PUBMED:6165837"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_317"
      name "nsp14 acts as a cap N7 methyltransferase to modify SARS-CoV-1 mRNAs"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684016__layout_417"
      uniprot "NA"
    ]
    graphics [
      x 1383.2723582581907
      y 1802.153782565756
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_317"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 189
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:reactome:R-ALL-29356"
      hgnc "NA"
      map_id "R1_189"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_418"
      uniprot "NA"
    ]
    graphics [
      x 1460.9371102752111
      y 1872.928631862218
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_189"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 190
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A37565;urn:miriam:reactome:R-ALL-29438"
      hgnc "NA"
      map_id "R1_192"
      name "GTP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_424"
      uniprot "NA"
    ]
    graphics [
      x 1338.7461514782742
      y 1914.9333047151895
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_192"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 191
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A43474;urn:miriam:reactome:R-ALL-29372"
      hgnc "NA"
      map_id "R1_193"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_426"
      uniprot "NA"
    ]
    graphics [
      x 1322.1680546453786
      y 1984.5935870335584
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_193"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 192
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9694458;urn:miriam:refseq:NC_004718.3;urn:miriam:reactome:R-COV-9681659;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_24"
      name "SARS_minus_CoV_minus_1_space_gRNA:RTC"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_132"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 1764.456330481695
      y 1816.8525602974719
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 193
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9681651;PUBMED:17024178;PUBMED:22039154"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_267"
      name "nsp8 generates RNA primers"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9681651__layout_136"
      uniprot "NA"
    ]
    graphics [
      x 1616.8252322280327
      y 1575.2444959865825
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_267"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 194
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-ALL-6806881;urn:miriam:obo.chebi:CHEBI%3A61557"
      hgnc "NA"
      map_id "R1_27"
      name "NTP(4_minus_)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_137"
      uniprot "NA"
    ]
    graphics [
      x 1727.7452042678115
      y 1312.2918915691027
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 195
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A33019;urn:miriam:reactome:R-ALL-111294"
      hgnc "NA"
      map_id "R1_21"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_1268"
      uniprot "NA"
    ]
    graphics [
      x 1883.2525202561012
      y 1621.4144620844306
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 196
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:reactome:R-ALL-29356"
      hgnc "NA"
      map_id "R1_185"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_40"
      uniprot "NA"
    ]
    graphics [
      x 550.3600154947032
      y 995.1604157363895
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_185"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 197
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684321;PUBMED:12917450;PUBMED:15564471"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_336"
      name "nsp3 cleaves nsp1-4"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684321__layout_58"
      uniprot "NA"
    ]
    graphics [
      x 510.87043864848715
      y 687.1529157349036
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_336"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 198
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9694784;urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9684866;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_187"
      name "nsp1_minus_4"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_41"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 493.92402757281866
      y 850.8994268588606
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_187"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 199
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9682205;urn:miriam:reactome:R-COV-9694735;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_250"
      name "N_minus_glycan_space_nsp3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_81"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 389.2950941093719
      y 708.5197716580087
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_250"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 200
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9694760;urn:miriam:reactome:R-COV-9682222;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_205"
      name "nsp2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_46"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 519.1101377450508
      y 785.4014457615144
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_205"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 201
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9694741;urn:miriam:reactome:R-COV-9682210;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_198"
      name "nsp1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_44"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 562.5666705598908
      y 921.8597340531165
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_198"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 202
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9694577;urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9684876;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_213"
      name "nsp4"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_54"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 825.4535414519439
      y 595.1513429893399
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_213"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 203
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9682245;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_2"
      name "nsp7"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_101"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 1297.449259382043
      y 1744.396044845936
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 204
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9680812;PUBMED:31138817;PUBMED:16228002"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_263"
      name "nsp7 binds nsp8"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9680812__layout_97"
      uniprot "NA"
    ]
    graphics [
      x 1201.911625742988
      y 1873.0197897369644
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_263"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 205
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9680806;urn:miriam:pubmed:31138817;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_4"
      name "nsp7:nsp8"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_104"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 1386.600475127967
      y 1918.0167397346213
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 206
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9686709;PUBMED:31226023"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_360"
      name "Uncoating of SARS-CoV-1 Genome"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9686709__layout_913"
      uniprot "NA"
    ]
    graphics [
      x 557.3170676400059
      y 2252.3182696539184
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_360"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 207
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9684443;urn:miriam:reactome:R-COV-9694712;urn:miriam:refseq:NC_004718.3"
      hgnc "NA"
      map_id "R1_75"
      name "m7GpppA_minus_capped_space_SARS_minus_CoV_minus_1_space_genomic_space_RNA_space_(plus_space_strand)"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_1923"
      uniprot "NA"
    ]
    graphics [
      x 766.6277220094206
      y 2178.0145807119707
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 208
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9678289;urn:miriam:reactome:R-COV-9694647;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_167"
      name "pp1ab_minus_nsp16"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_35"
      uniprot "UNIPROT:P0C6X7"
    ]
    graphics [
      x 1689.0873155528056
      y 1163.5958841003712
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_167"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 209
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683455;PUBMED:25732088"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_283"
      name "nsp16 binds VHL"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683455__layout_214"
      uniprot "NA"
    ]
    graphics [
      x 1513.0341392746373
      y 825.6369905505778
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_283"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 210
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-CEL-391423;urn:miriam:reactome:R-DDI-391423;urn:miriam:reactome:R-MMU-391423;urn:miriam:reactome:R-DME-391423;urn:miriam:reactome:R-XTR-391423;urn:miriam:reactome:R-SSC-391423;urn:miriam:reactome:R-RNO-391423;urn:miriam:reactome:R-GGA-391423;urn:miriam:reactome:R-DRE-391423;urn:miriam:uniprot:P40337;urn:miriam:reactome:R-BTA-391423;urn:miriam:reactome:R-HSA-391423;urn:miriam:reactome:R-CFA-391423"
      hgnc "NA"
      map_id "R1_113"
      name "VHL"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_215"
      uniprot "UNIPROT:P40337"
    ]
    graphics [
      x 1391.8361278175294
      y 720.5799457247533
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 211
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-HSA-9694398;urn:miriam:reactome:R-HSA-9683453;urn:miriam:uniprot:P40337;urn:miriam:pubmed:25732088;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_114"
      name "nsp16:VHL"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_216"
      uniprot "UNIPROT:P40337;UNIPROT:P0C6X7"
    ]
    graphics [
      x 1361.3726921109703
      y 676.5973071828012
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 212
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9694598;urn:miriam:reactome:R-COV-9685967"
      hgnc "NA"
      map_id "R1_181"
      name "O_minus_glycosyl_space_3a_space_tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_392"
      uniprot "UNIPROT:P59632"
    ]
    graphics [
      x 945.648065224842
      y 1345.730584655699
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_181"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 213
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9686174;PUBMED:16840309;PUBMED:15807784;PUBMED:16894145;PUBMED:15781262;PUBMED:23202509;PUBMED:15194747"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_358"
      name "Accessory proteins are recruited to the maturing virion"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9686174__layout_613"
      uniprot "NA"
    ]
    graphics [
      x 1039.0064343791623
      y 1129.732877191545
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_358"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 214
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:P59635;urn:miriam:reactome:R-COV-9686193"
      hgnc "NA"
      map_id "R1_228"
      name "7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_614"
      uniprot "UNIPROT:P59635"
    ]
    graphics [
      x 1160.8173698462856
      y 1002.9049005771478
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_228"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 215
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9686310;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694321;urn:miriam:uniprot:P59594;urn:miriam:uniprot:P59595;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P59596"
      hgnc "NA"
      map_id "R1_235"
      name "S3:M:E:encapsidated_space_SARS_space_coronavirus_space_genomic_space_RNA:O_minus_glycosyl_space_3a_space_tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_670"
      uniprot "UNIPROT:P59637;UNIPROT:P59594;UNIPROT:P59595;UNIPROT:P59596"
    ]
    graphics [
      x 1287.14516070909
      y 1240.0203081242084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_235"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 216
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9684225;urn:miriam:uniprot:P59635;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694324;urn:miriam:uniprot:P59594;urn:miriam:uniprot:P59595;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P59596"
      hgnc "NA"
      map_id "R1_227"
      name "S3:M:E:encapsidated_space_SARS_space_coronavirus_space_genomic_space_RNA:7a:O_minus_glycosyl_space_3a_space_tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_612"
      uniprot "UNIPROT:P59632;UNIPROT:P59635;UNIPROT:P59637;UNIPROT:P59594;UNIPROT:P59595;UNIPROT:P59596"
    ]
    graphics [
      x 796.9074254235301
      y 951.939466653126
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_227"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 217
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-ALL-9686811"
      hgnc "NA"
      map_id "R1_8"
      name "ER_minus_alpha_minus_glucosidase_space_inhibitors"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_1079"
      uniprot "NA"
    ]
    graphics [
      x 2065.116841651116
      y 1208.7571126946784
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 218
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9686790;PUBMED:16188993;PUBMED:24716661;PUBMED:23816430;PUBMED:19223639;PUBMED:23503623;PUBMED:7986008"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_364"
      name "ER-alpha glucosidases bind ER-alpha glucosidase inhibitors"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9686790__layout_1078"
      uniprot "NA"
    ]
    graphics [
      x 2125.6473117587793
      y 1088.668547109399
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_364"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 219
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-HSA-9682983;urn:miriam:uniprot:Q14697;urn:miriam:uniprot:P14314;urn:miriam:pubmed:25348530;urn:miriam:uniprot:Q13724"
      hgnc "NA"
      map_id "R1_142"
      name "ER_space_alpha_minus_glucosidases"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_280"
      uniprot "UNIPROT:Q14697;UNIPROT:P14314;UNIPROT:Q13724"
    ]
    graphics [
      x 1984.1932858769828
      y 1074.8587141999426
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_142"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 220
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-HSA-9686842;urn:miriam:uniprot:Q14697;urn:miriam:uniprot:P14314;urn:miriam:uniprot:Q13724"
      hgnc "NA"
      map_id "R1_10"
      name "ER_minus_alpha_space_glucosidases:ER_minus_alpha_space_glucosidase_space_inhibitors"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_1080"
      uniprot "UNIPROT:Q14697;UNIPROT:P14314;UNIPROT:Q13724"
    ]
    graphics [
      x 2026.0697421104983
      y 1009.3369510563675
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 221
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9685639;PUBMED:12917450;PUBMED:16928755;PUBMED:12927536;PUBMED:14569023;PUBMED:25736566;PUBMED:26919232"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_347"
      name "Synthesis of SARS-CoV-1 minus strand subgenomic mRNAs by template switching"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9685639__layout_402"
      uniprot "NA"
    ]
    graphics [
      x 1910.946036469762
      y 1854.5164966274542
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_347"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 222
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9694300;urn:miriam:pubmed:15496142;urn:miriam:uniprot:P59595;urn:miriam:pubmed:12775768;urn:miriam:reactome:R-COV-9683625"
      hgnc "NA"
      map_id "R1_116"
      name "N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2261"
      uniprot "UNIPROT:P59595"
    ]
    graphics [
      x 2072.4523274391436
      y 1849.8503233266563
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 223
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683765;PUBMED:17210170"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_314"
      name "Nucleoprotein translocates to the ERGIC outer membrane"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683765__layout_269"
      uniprot "NA"
    ]
    graphics [
      x 467.2314971635641
      y 2049.1968207748155
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_314"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 224
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9684357;urn:miriam:uniprot:P0C6U8"
      hgnc "NA"
      map_id "R1_149"
      name "pp1a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_3"
      uniprot "UNIPROT:P0C6U8"
    ]
    graphics [
      x 1460.244249588445
      y 1129.9341680308232
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_149"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 225
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684351;PUBMED:15788388;PUBMED:21203998;PUBMED:27799534"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_341"
      name "pp1a cleaves itself"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684351__layout_8"
      uniprot "NA"
    ]
    graphics [
      x 1328.5504128946657
      y 1007.0517114425666
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_341"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 226
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:reactome:R-ALL-29356"
      hgnc "NA"
      map_id "R1_257"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_9"
      uniprot "NA"
    ]
    graphics [
      x 1162.1661948953679
      y 950.8870790792428
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_257"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 227
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9694743;urn:miriam:reactome:R-COV-9684326"
      hgnc "NA"
      map_id "R1_238"
      name "pp1a_space_dimer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_7"
      uniprot "UNIPROT:P0C6U8"
    ]
    graphics [
      x 1277.588401554719
      y 1090.343544452107
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_238"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 228
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9678259"
      hgnc "NA"
      map_id "R1_17"
      name "pp1a_minus_nsp5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_12"
      uniprot "UNIPROT:P0C6U8"
    ]
    graphics [
      x 1501.0704489746145
      y 1040.589777421374
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 229
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9684311;urn:miriam:reactome:R-COV-9694778;urn:miriam:uniprot:P0C6U8"
      hgnc "NA"
      map_id "R1_1"
      name "pp1a_minus_nsp1_minus_4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_10"
      uniprot "UNIPROT:P0C6U8"
    ]
    graphics [
      x 1520.2038743840749
      y 980.4158768989322
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 230
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:P59632;urn:miriam:uniprot:P59635;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9752958;urn:miriam:uniprot:P59594;urn:miriam:uniprot:P59595;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P59596;urn:miriam:reactome:R-COV-9685539"
      hgnc "NA"
      map_id "R1_229"
      name "S3:M:E:encapsidated_space_SARS_space_coronavirus_space_genomic_space_RNA:7a:O_minus_glycosyl_space_3a_space_tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_616"
      uniprot "UNIPROT:P59632;UNIPROT:P59635;UNIPROT:P59637;UNIPROT:P59594;UNIPROT:P59595;UNIPROT:P59596"
    ]
    graphics [
      x 317.63406768660525
      y 784.0329632687815
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_229"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 231
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9685542;PUBMED:31226023;PUBMED:16877062;PUBMED:25855243"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_345"
      name "Viral release"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9685542__layout_617"
      uniprot "NA"
    ]
    graphics [
      x 138.3176766677184
      y 751.3292323082915
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_345"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 232
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:P59632;urn:miriam:uniprot:P59635;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694500;urn:miriam:uniprot:P59594;urn:miriam:uniprot:P59595;urn:miriam:refseq:NC_004718.3;urn:miriam:uniprot:P59596;urn:miriam:reactome:R-COV-9685506"
      hgnc "NA"
      map_id "R1_230"
      name "S3:M:E:encapsidated_space_SARS_space_coronavirus_space_genomic_space_RNA:_space_7a:O_minus_glycosyl_space_3a_space_tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_618"
      uniprot "UNIPROT:P59632;UNIPROT:P59635;UNIPROT:P59637;UNIPROT:P59594;UNIPROT:P59595;UNIPROT:P59596"
    ]
    graphics [
      x 67.20040832356312
      y 861.1632517508506
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_230"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 233
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9694781;urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9686674"
      hgnc "NA"
      map_id "R1_218"
      name "O_minus_glycosyl_space_3a_space_tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_584"
      uniprot "UNIPROT:P59632"
    ]
    graphics [
      x 2178.3679308375667
      y 1419.420673085601
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_218"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 234
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9685950;PUBMED:19398035;PUBMED:15194747;PUBMED:16212942"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_352"
      name "Endocytosis of protein 3a"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9685950__layout_586"
      uniprot "NA"
    ]
    graphics [
      x 2041.290048484682
      y 1602.6923062505807
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_352"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 235
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9694781;urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9686674"
      hgnc "NA"
      map_id "R1_48"
      name "O_minus_glycosyl_space_3a_space_tetramer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_1543"
      uniprot "UNIPROT:P59632"
    ]
    graphics [
      x 1865.3749726431388
      y 1735.6106813725924
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 236
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684241;PUBMED:24418573;PUBMED:22238235;PUBMED:17166901;PUBMED:27145752;PUBMED:25855243;PUBMED:20580052;PUBMED:20007283;PUBMED:18792806;PUBMED:16873249"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_327"
      name "Recruitment of Spike trimer to assembling virion"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684241__layout_611"
      uniprot "NA"
    ]
    graphics [
      x 1426.4679865316934
      y 1491.857592781642
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_327"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 237
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:P59594;urn:miriam:reactome:R-COV-9683768"
      hgnc "NA"
      map_id "R1_155"
      name "complex_space_N_minus_glycan_minus_PALM_minus_Spike_space_trimer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_317"
      uniprot "UNIPROT:P59594"
    ]
    graphics [
      x 1747.2074996492584
      y 1611.4213307566426
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_155"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 238
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:pubmed:20129637;urn:miriam:pubmed:15253436;urn:miriam:pubmed:17715238;urn:miriam:pubmed:16122388;urn:miriam:reactome:R-COV-9683716;urn:miriam:uniprot:P59594;urn:miriam:pubmed:12775768;urn:miriam:pubmed:14760722"
      hgnc "NA"
      map_id "R1_140"
      name "trimmed_space_unfolded_space_N_minus_glycan_space_Spike"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_279"
      uniprot "UNIPROT:P59594"
    ]
    graphics [
      x 1490.5749161438657
      y 1211.5152693185375
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_140"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 239
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683772;PUBMED:22915798"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_316"
      name "Trimmed spike protein binds to calnexin"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683772__layout_286"
      uniprot "NA"
    ]
    graphics [
      x 1182.2964573763434
      y 1399.8863692941404
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_316"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 240
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-HSA-195906;urn:miriam:uniprot:P27824;urn:miriam:reactome:R-BTA-195906;urn:miriam:reactome:R-CFA-195906;urn:miriam:reactome:R-SPO-195906;urn:miriam:reactome:R-SSC-195906;urn:miriam:reactome:R-RNO-195906;urn:miriam:reactome:R-GGA-195906;urn:miriam:reactome:R-DRE-195906;urn:miriam:reactome:R-CEL-195906;urn:miriam:reactome:R-DDI-195906;urn:miriam:reactome:R-MMU-195906;urn:miriam:reactome:R-DME-195906;urn:miriam:reactome:R-XTR-195906;urn:miriam:reactome:R-DME-195906-2;urn:miriam:reactome:R-SCE-195906;urn:miriam:reactome:R-SPO-195906-2;urn:miriam:reactome:R-DME-195906-3;urn:miriam:reactome:R-SPO-195906-3;urn:miriam:reactome:R-DME-195906-4"
      hgnc "NA"
      map_id "R1_144"
      name "CANX"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_288"
      uniprot "UNIPROT:P27824"
    ]
    graphics [
      x 1146.7535068061156
      y 1495.4580790452646
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_144"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 241
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9680811;PUBMED:22318142;PUBMED:31138817"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_262"
      name "nsp12 binds nsp7 and nsp8"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9680811__layout_105"
      uniprot "NA"
    ]
    graphics [
      x 1311.7307002927475
      y 1844.0937200134458
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_262"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 242
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:P0C6X7;urn:miriam:reactome:R-COV-9678280"
      hgnc "NA"
      map_id "R1_161"
      name "pp1ab_minus_nsp12"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_34"
      uniprot "UNIPROT:P0C6X7"
    ]
    graphics [
      x 1558.3384086204242
      y 1695.589514659694
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_161"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 243
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9687109;PUBMED:31231549"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_365"
      name "8b binds MAP1LC3B"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9687109__layout_1495"
      uniprot "NA"
    ]
    graphics [
      x 675.9133178014403
      y 1354.5110957815102
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_365"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 244
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:Q80H93;urn:miriam:uniprot:Q9GZQ8;urn:miriam:reactome:R-HSA-9687111"
      hgnc "NA"
      map_id "R1_45"
      name "8b:MAP1LC3B"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_1497"
      uniprot "UNIPROT:Q80H93;UNIPROT:Q9GZQ8"
    ]
    graphics [
      x 777.0220808067899
      y 1456.291235096875
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 245
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9683691;urn:miriam:reactome:R-COV-9694716;urn:miriam:pubmed:16474139"
      hgnc "NA"
      map_id "R1_121"
      name "3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_230"
      uniprot "UNIPROT:P59632"
    ]
    graphics [
      x 1415.7881954840113
      y 1550.458455943301
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_121"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 246
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683712;PUBMED:16474139"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_302"
      name "3a translocates to the ERGIC"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683712__layout_341"
      uniprot "NA"
    ]
    graphics [
      x 1375.6420606622994
      y 1384.9147920434982
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_302"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 247
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9683691;urn:miriam:reactome:R-COV-9694716;urn:miriam:pubmed:16474139"
      hgnc "NA"
      map_id "R1_162"
      name "3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_342"
      uniprot "UNIPROT:P59632"
    ]
    graphics [
      x 1522.9899642802734
      y 1404.753700320223
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_162"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 248
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684350;PUBMED:27799534;PUBMED:21203998"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_340"
      name "pp1a forms a dimer"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684350__layout_6"
      uniprot "NA"
    ]
    graphics [
      x 1361.9256101320648
      y 1132.12094237554
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_340"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 249
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9683433;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_18"
      name "nsp16:nsp10"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_121"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 1356.173692372033
      y 1257.7373654324413
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 250
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9686005;PUBMED:18255185;PUBMED:18827877"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_354"
      name "nsp3 binds to nsp7-8 and nsp12-16"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9686005__layout_457"
      uniprot "NA"
    ]
    graphics [
      x 1091.3077362872425
      y 1291.264635152226
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_354"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 251
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9694740;urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9683403;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_16"
      name "nsp7:nsp8:nsp12:nsp14:nsp10:nsp13:nsp15"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_119"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 1064.4644033262025
      y 1658.9223197020374
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 252
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9686008;urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9694728;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_203"
      name "nsp3:nsp4:nsp6"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_451"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 1010.574454741143
      y 1013.7543238020112
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_203"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 253
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683719;PUBMED:15367599"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_305"
      name "Spike protein forms a homotrimer"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683719__layout_299"
      uniprot "NA"
    ]
    graphics [
      x 1243.186925660949
      y 2041.582604009266
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_305"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 254
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-ALL-9693170;urn:miriam:pubchem.compound:3767"
      hgnc "NA"
      map_id "R1_95"
      name "CTSL_space_inhibitors"
      node_subtype "DRUG"
      node_type "species"
      org_id "layout_2051"
      uniprot "NA"
    ]
    graphics [
      x 737.4524854798524
      y 760.660676570127
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 255
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9685655;PUBMED:32179150;PUBMED:23105391;PUBMED:16962401;PUBMED:26335104;PUBMED:26953343"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_348"
      name "CTSL bind CTSL inhibitors"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9685655__layout_1036"
      uniprot "NA"
    ]
    graphics [
      x 718.8100899559938
      y 977.7334579447181
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_348"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 256
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:reactome:R-ALL-29356"
      hgnc "NA"
      map_id "R1_61"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_18"
      uniprot "NA"
    ]
    graphics [
      x 1727.3524704020465
      y 964.33844522918
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 257
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684309;PUBMED:14561748"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_335"
      name "3CLp cleaves nsp6-11"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684309__layout_17"
      uniprot "NA"
    ]
    graphics [
      x 1826.9882138455537
      y 977.0146307848759
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_335"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 258
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9684343;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7;urn:miriam:reactome:R-COV-9694407"
      hgnc "NA"
      map_id "R1_31"
      name "3CLp_space_dimer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_14"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 1911.723271715331
      y 1244.2512382544767
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 259
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9681588;urn:miriam:uniprot:P0C6U8;urn:miriam:obo.chebi:CHEBI%3A2981;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_210"
      name "3CLp_space_dimer:Î±_minus_Ketoamides"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_504"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 1873.1375844344993
      y 1170.6937830198813
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_210"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 260
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9678266;urn:miriam:uniprot:P0C6U8"
      hgnc "NA"
      map_id "R1_120"
      name "pp1a_minus_nsp7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_23"
      uniprot "UNIPROT:P0C6U8"
    ]
    graphics [
      x 1848.5703422576241
      y 813.3413605540417
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 261
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9678271;urn:miriam:uniprot:P0C6U8"
      hgnc "NA"
      map_id "R1_70"
      name "pp1a_minus_nsp11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_19"
      uniprot "UNIPROT:P0C6U8"
    ]
    graphics [
      x 1927.4828252064094
      y 858.4739766174775
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 262
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9678265;urn:miriam:reactome:R-COV-9694256;urn:miriam:uniprot:P0C6U8"
      hgnc "NA"
      map_id "R1_111"
      name "pp1a_minus_nsp6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_21"
      uniprot "UNIPROT:P0C6U8"
    ]
    graphics [
      x 1909.7057840104505
      y 929.7613085500736
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 263
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9678273;urn:miriam:uniprot:P0C6U8"
      hgnc "NA"
      map_id "R1_86"
      name "pp1a_minus_nsp9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_20"
      uniprot "UNIPROT:P0C6U8"
    ]
    graphics [
      x 1955.591817855118
      y 991.5425136956246
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 264
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9678275;urn:miriam:uniprot:P0C6U8"
      hgnc "NA"
      map_id "R1_125"
      name "pp1a_minus_nsp8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_24"
      uniprot "UNIPROT:P0C6U8"
    ]
    graphics [
      x 1784.0317661714391
      y 857.8053092534392
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_125"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 265
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9678272;urn:miriam:uniprot:P0C6U8"
      hgnc "NA"
      map_id "R1_115"
      name "pp1a_minus_nsp10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_22"
      uniprot "UNIPROT:P0C6U8"
    ]
    graphics [
      x 1737.0556519569802
      y 828.773616788795
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 266
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683630;PUBMED:16103198"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_290"
      name "Nucleoprotein translocates to the plasma membrane"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683630__layout_255"
      uniprot "NA"
    ]
    graphics [
      x 245.85911021173024
      y 2137.430838018156
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_290"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 267
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9686056;urn:miriam:reactome:R-COV-9694464;urn:miriam:uniprot:P59595"
      hgnc "NA"
      map_id "R1_182"
      name "SUMO_minus_p_minus_N_space_dimer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_393"
      uniprot "UNIPROT:P59595"
    ]
    graphics [
      x 324.163109420869
      y 1988.9173628675262
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_182"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 268
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684229;PUBMED:24418573;PUBMED:17002283;PUBMED:15147189;PUBMED:17379242;PUBMED:15849181;PUBMED:18456656;PUBMED:15094372;PUBMED:19052082;PUBMED:16214138;PUBMED:16103198;PUBMED:15020242;PUBMED:18561946;PUBMED:23717688;PUBMED:17881296;PUBMED:18631359;PUBMED:16228284"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_324"
      name "SUMO-p-N protein dimer binds genomic RNA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684229__layout_589"
      uniprot "NA"
    ]
    graphics [
      x 559.8498170724774
      y 1616.8570239338032
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_324"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 269
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-ALL-9685933"
      hgnc "NA"
      map_id "R1_217"
      name "Host_space_Derived_space_Lipid_space_Bilayer_space_Membrane"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "layout_583"
      uniprot "NA"
    ]
    graphics [
      x 2246.866097867528
      y 1135.77797689947
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_217"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 270
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9685956;PUBMED:16352545"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_353"
      name "3a is externalized together with membrane structures"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9685956__layout_582"
      uniprot "NA"
    ]
    graphics [
      x 2144.2754519106857
      y 1208.7922182218354
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_353"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 271
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9694475;urn:miriam:reactome:R-COV-9685958"
      hgnc "NA"
      map_id "R1_219"
      name "3a:membranous_space_structure"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_585"
      uniprot "UNIPROT:P59632"
    ]
    graphics [
      x 2177.330340232244
      y 1059.4736345298625
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_219"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 272
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30616;urn:miriam:reactome:R-ALL-113592"
      hgnc "NA"
      map_id "R1_195"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_431"
      uniprot "NA"
    ]
    graphics [
      x 1005.5203269599147
      y 373.8902341261811
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_195"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 273
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9685906;PUBMED:27760233"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_350"
      name "Polyadenylation of SARS-CoV-1 subgenomic mRNAs (plus strand)"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9685906__layout_430"
      uniprot "NA"
    ]
    graphics [
      x 1099.5328481291033
      y 527.6526980223451
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_350"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 274
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9694561;urn:miriam:refseq:NC_004718.3;urn:miriam:reactome:R-COV-9685910"
      hgnc "NA"
      map_id "R1_196"
      name "m7G(5')pppAm_minus_capped,_space_polyadenylated_space_SARS_minus_CoV_minus_1_space_subgenomic_space_mRNAs_space_(plus_space_strand)"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_432"
      uniprot "NA"
    ]
    graphics [
      x 1168.407723840369
      y 378.71007805722013
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_196"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 275
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A33019;urn:miriam:reactome:R-ALL-111294"
      hgnc "NA"
      map_id "R1_22"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_1274"
      uniprot "NA"
    ]
    graphics [
      x 1110.3899107454927
      y 390.95629097533276
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 276
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683429;PUBMED:20699222;PUBMED:22022266;PUBMED:26041293;PUBMED:21637813;PUBMED:21393853;PUBMED:22635272;PUBMED:16873246;PUBMED:20421945;PUBMED:16873247;PUBMED:25074927"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_282"
      name "nsp16 binds nsp10"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683429__layout_120"
      uniprot "NA"
    ]
    graphics [
      x 1560.3766045769319
      y 1299.1373003426884
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_282"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 277
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9686013;PUBMED:18367524"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_355"
      name "Nsp3:nsp4 binds to nsp6"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9686013__layout_448"
      uniprot "NA"
    ]
    graphics [
      x 914.0637231075372
      y 725.4045377308008
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_355"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 278
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:reactome:R-ALL-29356"
      hgnc "NA"
      map_id "R1_84"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_1997"
      uniprot "NA"
    ]
    graphics [
      x 791.4879956052015
      y 824.3336948869423
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 279
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684352;PUBMED:15564471"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_342"
      name "nsp3-4 cleaves itself"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684352__layout_52"
      uniprot "NA"
    ]
    graphics [
      x 683.4327731866091
      y 643.3208317644153
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_342"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 280
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9694554;urn:miriam:reactome:R-COV-9684869;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_201"
      name "nsp3_minus_4"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_45"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 852.1128479812173
      y 714.7294965663109
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_201"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 281
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9684874;urn:miriam:reactome:R-COV-9694600;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_243"
      name "N_minus_glycan_space_pp1ab_minus_nsp3_minus_4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_727"
      uniprot "UNIPROT:P0C6X7"
    ]
    graphics [
      x 579.1586021084307
      y 670.2046918628184
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_243"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 282
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684301;PUBMED:15680415"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_334"
      name "mRNA1 is translated to pp1a"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684301__layout_1"
      uniprot "NA"
    ]
    graphics [
      x 1495.3585804832524
      y 1340.9689473991245
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_334"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 283
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683751;PUBMED:16442106"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_310"
      name "M protein gets N-glycosylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683751__layout_386"
      uniprot "NA"
    ]
    graphics [
      x 863.4197885194988
      y 194.53478623982892
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_310"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 284
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-ALL-9683033"
      hgnc "NA"
      map_id "R1_179"
      name "nucleotide_minus_sugar"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "layout_389"
      uniprot "NA"
    ]
    graphics [
      x 1020.1649501466335
      y 241.0392520318028
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_179"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 285
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-156540"
      hgnc "NA"
      map_id "R1_180"
      name "H_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_391"
      uniprot "NA"
    ]
    graphics [
      x 901.0874700882464
      y 393.54043360928733
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_180"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 286
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-ALL-9683046"
      hgnc "NA"
      map_id "R1_178"
      name "nucleoside_space_5'_minus_diphosphate(3âˆ’)"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "layout_388"
      uniprot "NA"
    ]
    graphics [
      x 861.6009597867811
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_178"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 287
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:pubmed:16442106;urn:miriam:reactome:R-COV-9683588;urn:miriam:reactome:R-COV-9694684;urn:miriam:uniprot:P59596"
      hgnc "NA"
      map_id "R1_177"
      name "N_minus_glycan_space_M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_387"
      uniprot "UNIPROT:P59596"
    ]
    graphics [
      x 816.726859487945
      y 439.0937793652371
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_177"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 288
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684275;PUBMED:15564471"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_330"
      name "nsp3-4 is glycosylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684275__layout_47"
      uniprot "NA"
    ]
    graphics [
      x 1147.3558450550245
      y 586.450287634155
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_330"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 289
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9684863;urn:miriam:uniprot:P0C6U8;urn:miriam:reactome:R-COV-9694479;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_209"
      name "N_minus_glycan_space_nsp3_minus_4"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_50"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 1391.3726921109703
      y 661.0035355321753
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_209"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 290
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9682695;PUBMED:19224332;PUBMED:16579970;PUBMED:15140959;PUBMED:12917423;PUBMED:20671029;PUBMED:22615777"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_278"
      name "nsp13 helicase melts secondary structures in SARS-CoV-1 genomic RNA template"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9682695__layout_130"
      uniprot "NA"
    ]
    graphics [
      x 1639.2124275909905
      y 2020.8903041961862
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_278"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 291
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30616;urn:miriam:reactome:R-ALL-113592"
      hgnc "NA"
      map_id "R1_23"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_131"
      uniprot "NA"
    ]
    graphics [
      x 1732.0541908351113
      y 2142.015904101373
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 292
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A43474;urn:miriam:reactome:R-ALL-29372"
      hgnc "NA"
      map_id "R1_26"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_134"
      uniprot "NA"
    ]
    graphics [
      x 1422.7212760581353
      y 2039.4951385486797
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 293
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A456216;urn:miriam:reactome:R-ALL-29370"
      hgnc "NA"
      map_id "R1_25"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_133"
      uniprot "NA"
    ]
    graphics [
      x 1649.5474622360844
      y 2172.7562311021497
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 294
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683393;PUBMED:28143984;PUBMED:16828802;PUBMED:16882730;PUBMED:18045871;PUBMED:16216269;PUBMED:18255185;PUBMED:17409150;PUBMED:22301153"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_281"
      name "nsp15 binds nsp8"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683393__layout_118"
      uniprot "NA"
    ]
    graphics [
      x 1036.0438851693773
      y 1953.7325651883648
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_281"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 295
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684273;PUBMED:14561748"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_329"
      name "3CLp cleaves pp1a"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684273__layout_15"
      uniprot "NA"
    ]
    graphics [
      x 1666.0421904206323
      y 1073.4666730857716
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_329"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 296
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:reactome:R-ALL-29356"
      hgnc "NA"
      map_id "R1_52"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_16"
      uniprot "NA"
    ]
    graphics [
      x 1578.2362708223618
      y 1059.7789209756627
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 297
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683718;PUBMED:19534833"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_304"
      name "Protein M localizes to the Golgi membrane"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683718__layout_374"
      uniprot "NA"
    ]
    graphics [
      x 775.8092467499695
      y 260.8840006080012
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_304"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 298
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9694335;urn:miriam:reactome:R-COV-9678288;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_150"
      name "pp1ab_minus_nsp15"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_30"
      uniprot "UNIPROT:P0C6X7"
    ]
    graphics [
      x 1504.0174188740582
      y 1748.362915840426
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_150"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 299
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9682718;PUBMED:18045871;PUBMED:16882730;PUBMED:16828802;PUBMED:16216269;PUBMED:22301153;PUBMED:17409150"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_280"
      name "nsp15 forms a hexamer"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9682718__layout_116"
      uniprot "NA"
    ]
    graphics [
      x 1216.1101814435638
      y 1951.2663092494126
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_280"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 300
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9694513;urn:miriam:refseq:NC_004718.3;urn:miriam:reactome:R-COV-9685921"
      hgnc "NA"
      map_id "R1_83"
      name "m7G(5')pppAm_minus_capped,polyadenylated_minus_mRNA9"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_1990"
      uniprot "NA"
    ]
    graphics [
      x 1211.374792685373
      y 529.8259995494693
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 301
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683735;PUBMED:31226023;PUBMED:16877062;PUBMED:12917450;PUBMED:15848177;PUBMED:17210170;PUBMED:15496142;PUBMED:12775768"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_308"
      name "mRNA9 is translated to Nucleoprotein"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683735__layout_237"
      uniprot "NA"
    ]
    graphics [
      x 1458.3619561740834
      y 488.8407334214098
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_308"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 302
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9686731;PUBMED:20926566;PUBMED:21325420;PUBMED:21068237"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_363"
      name "TMPRSS2 Mediated SARS-CoV-1 Spike Protein Cleavage and Endocytosis"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9686731__layout_892"
      uniprot "NA"
    ]
    graphics [
      x 742.2286069296196
      y 2058.9514284029397
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_363"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 303
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-HSA-9698958;urn:miriam:uniprot:Q9BYF1"
      hgnc "NA"
      map_id "R1_99"
      name "ACE2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2067"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 907.8174684120831
      y 1960.0724557435074
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 304
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9681596;PUBMED:32045235;PUBMED:15507456"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_266"
      name "3CLp dimer binds Î±-Ketoamides"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9681596__layout_577"
      uniprot "NA"
    ]
    graphics [
      x 2008.1453935872155
      y 1176.196952842532
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_266"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 305
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683734;PUBMED:19534833"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_307"
      name "Glycosylated M localizes to the Golgi membrane"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683734__layout_587"
      uniprot "NA"
    ]
    graphics [
      x 961.3137088240875
      y 452.2937339880973
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_307"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 306
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30616;urn:miriam:reactome:R-ALL-113592"
      hgnc "NA"
      map_id "R1_126"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_248"
      uniprot "NA"
    ]
    graphics [
      x 1890.7510876376382
      y 727.5554965659716
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_126"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 307
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683664;PUBMED:16103198;PUBMED:19106108"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_297"
      name "GSK3 phosphorylates Nucleoprotein"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683664__layout_247"
      uniprot "NA"
    ]
    graphics [
      x 1847.2185181275204
      y 881.7998006054249
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_297"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 308
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:P49840;urn:miriam:reactome:R-HSA-198358;urn:miriam:uniprot:P49841"
      hgnc "NA"
      map_id "R1_117"
      name "GSK3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_2262"
      uniprot "UNIPROT:P49840;UNIPROT:P49841"
    ]
    graphics [
      x 1589.3662423818387
      y 911.3292155575255
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 309
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A456216;urn:miriam:reactome:R-ALL-29370"
      hgnc "NA"
      map_id "R1_127"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_249"
      uniprot "NA"
    ]
    graphics [
      x 1957.9137839493942
      y 755.8888061212757
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_127"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 310
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:P59595;urn:miriam:reactome:R-COV-9683754;urn:miriam:pubmed:19106108"
      hgnc "NA"
      map_id "R1_128"
      name "p_minus_S177_minus_N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_250"
      uniprot "UNIPROT:P59595"
    ]
    graphics [
      x 1721.6427912814897
      y 1394.151891985181
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_128"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 311
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-70106"
      hgnc "NA"
      map_id "R1_129"
      name "H_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_254"
      uniprot "NA"
    ]
    graphics [
      x 2002.9487159559374
      y 813.7473417741386
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_129"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 312
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30616;urn:miriam:reactome:R-ALL-113592"
      hgnc "NA"
      map_id "R1_76"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_193"
      uniprot "NA"
    ]
    graphics [
      x 1092.769297678358
      y 715.991000584683
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 313
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9685519;PUBMED:30918070;PUBMED:10799579;PUBMED:27760233"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_343"
      name "Polyadenylation of SARS-CoV-1 genomic RNA (plus strand)"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9685519__layout_192"
      uniprot "NA"
    ]
    graphics [
      x 968.354704682882
      y 833.6685030438201
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_343"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 314
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A33019;urn:miriam:reactome:R-ALL-111294"
      hgnc "NA"
      map_id "R1_231"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_637"
      uniprot "NA"
    ]
    graphics [
      x 1107.6926878879688
      y 650.0264337982162
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_231"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 315
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9685531;PUBMED:16877062;PUBMED:9658133;PUBMED:10799570;PUBMED:31133031;PUBMED:16254320;PUBMED:25855243;PUBMED:18792806"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_344"
      name "SARS virus buds into ERGIC lumen"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9685531__layout_615"
      uniprot "NA"
    ]
    graphics [
      x 565.3067082737117
      y 825.1636608836634
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_344"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 316
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A57540;urn:miriam:reactome:R-ALL-29360"
      hgnc "NA"
      map_id "R1_197"
      name "NAD_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_439"
      uniprot "NA"
    ]
    graphics [
      x 1561.8163384860748
      y 1928.8981764216458
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_197"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 317
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9686061;PUBMED:29199039;PUBMED:32029454"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_357"
      name "Nucleoprotein is ADP-ribosylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9686061__layout_438"
      uniprot "NA"
    ]
    graphics [
      x 1537.8513722433527
      y 1827.2208789034898
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_357"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 318
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-CFA-8938273;urn:miriam:uniprot:Q53GL7;urn:miriam:reactome:R-HSA-8938273;urn:miriam:reactome:R-XTR-8938273;urn:miriam:uniprot:Q2NL67;urn:miriam:reactome:R-DDI-8938273;urn:miriam:uniprot:Q8N3A8;urn:miriam:reactome:R-GGA-8938273;urn:miriam:reactome:R-RNO-8938273;urn:miriam:reactome:R-MMU-8938273;urn:miriam:reactome:R-BTA-8938273;urn:miriam:reactome:R-DRE-8938273;urn:miriam:reactome:R-DME-8938259;urn:miriam:uniprot:Q8N5Y8;urn:miriam:uniprot:Q460N5;urn:miriam:uniprot:Q8IXQ6;urn:miriam:uniprot:Q9UKK3;urn:miriam:reactome:R-SSC-8938273"
      hgnc "NA"
      map_id "R1_258"
      name "PARPs"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_914"
      uniprot "UNIPROT:Q53GL7;UNIPROT:Q2NL67;UNIPROT:Q8N3A8;UNIPROT:Q8N5Y8;UNIPROT:Q460N5;UNIPROT:Q8IXQ6;UNIPROT:Q9UKK3"
    ]
    graphics [
      x 1496.7869204713722
      y 1973.8040710338105
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_258"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 319
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17154;urn:miriam:reactome:R-ALL-197277"
      hgnc "NA"
      map_id "R1_242"
      name "NAM"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_716"
      uniprot "NA"
    ]
    graphics [
      x 1513.6058098658848
      y 2024.4098786988775
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_242"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 320
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-70106"
      hgnc "NA"
      map_id "R1_199"
      name "H_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_442"
      uniprot "NA"
    ]
    graphics [
      x 1570.2239898698938
      y 2029.7422389701583
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_199"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 321
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9683640;urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9694386;urn:miriam:pubmed:16474139"
      hgnc "NA"
      map_id "R1_166"
      name "O_minus_glycosyl_space_3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_349"
      uniprot "UNIPROT:P59632"
    ]
    graphics [
      x 517.7626256604622
      y 1419.7458039583344
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_166"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 322
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683746;PUBMED:30761102"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_309"
      name "Protein 3a forms a homotetramer"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683746__layout_352"
      uniprot "NA"
    ]
    graphics [
      x 742.2959893891468
      y 1354.9142379539253
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_309"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 323
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A57705;urn:miriam:reactome:R-ALL-9683631"
      hgnc "NA"
      map_id "R1_154"
      name "UDP_minus_N_minus_acetyl_minus_alpha_minus_D_minus_glucosamine(2âˆ’)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_316"
      uniprot "NA"
    ]
    graphics [
      x 1952.6872665032022
      y 1718.7121225626834
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_154"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 324
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683648;PUBMED:20129637;PUBMED:15367599;PUBMED:15831954"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_293"
      name "Spike trimer glycoside chains are extended"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683648__layout_315"
      uniprot "NA"
    ]
    graphics [
      x 2021.8868142018534
      y 1726.1575912314495
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_293"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 325
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-MMU-964750;urn:miriam:reactome:R-XTR-964750;urn:miriam:reactome:R-GGA-964750;urn:miriam:reactome:R-DME-964750;urn:miriam:reactome:R-CEL-964750;urn:miriam:reactome:R-DDI-964750;urn:miriam:uniprot:P26572;urn:miriam:reactome:R-BTA-964750;urn:miriam:reactome:R-CFA-964750;urn:miriam:reactome:R-DRE-964750;urn:miriam:reactome:R-RNO-964750;urn:miriam:reactome:R-SSC-964750;urn:miriam:reactome:R-HSA-964750"
      hgnc "NA"
      map_id "R1_159"
      name "MGAT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_320"
      uniprot "UNIPROT:P26572"
    ]
    graphics [
      x 2030.0949663289816
      y 1827.5408108925437
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_159"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 326
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-156540"
      hgnc "NA"
      map_id "R1_156"
      name "H_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_318"
      uniprot "NA"
    ]
    graphics [
      x 1831.9235964349682
      y 1598.1248586396648
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_156"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 327
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17659;urn:miriam:reactome:R-ALL-9683078"
      hgnc "NA"
      map_id "R1_157"
      name "UDP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_319"
      uniprot "NA"
    ]
    graphics [
      x 1834.8360143552263
      y 1665.8028674297395
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_157"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 328
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16846;urn:miriam:reactome:R-ALL-9683025"
      hgnc "NA"
      map_id "R1_163"
      name "UDP_minus_GalNAc"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_344"
      uniprot "NA"
    ]
    graphics [
      x 1703.6318532884984
      y 1349.2301414419765
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_163"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 329
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683760;PUBMED:16474139"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_312"
      name "GalNAc is transferred onto 3a"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683760__layout_343"
      uniprot "NA"
    ]
    graphics [
      x 1615.9925362918239
      y 1498.4079769660755
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_312"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 330
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:Q10472;urn:miriam:reactome:R-HSA-9682906"
      hgnc "NA"
      map_id "R1_165"
      name "GALNT1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_346"
      uniprot "UNIPROT:Q10472"
    ]
    graphics [
      x 1760.693497363478
      y 1407.2733844645827
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_165"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 331
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9683709;urn:miriam:reactome:R-COV-9694658;urn:miriam:pubmed:16474139"
      hgnc "NA"
      map_id "R1_164"
      name "GalNAc_minus_O_minus_3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_345"
      uniprot "UNIPROT:P59632"
    ]
    graphics [
      x 1055.40259229926
      y 1522.7904232599356
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_164"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 332
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-ALL-1131311;urn:miriam:obo.chebi:CHEBI%3A25609"
      hgnc "NA"
      map_id "R1_234"
      name "a_space_nucleotide_space_sugar"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_67"
      uniprot "NA"
    ]
    graphics [
      x 1295.146466523961
      y 454.6204009815185
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_234"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 333
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684261;PUBMED:17855519"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_328"
      name "nsp4 is glycosylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684261__layout_65"
      uniprot "NA"
    ]
    graphics [
      x 1116.3890251547293
      y 456.7242196870923
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_328"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 334
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378;urn:miriam:reactome:R-ALL-70106"
      hgnc "NA"
      map_id "R1_236"
      name "H_plus_"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_68"
      uniprot "NA"
    ]
    graphics [
      x 1266.3260313512963
      y 508.9275751096068
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_236"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 335
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A57930;urn:miriam:reactome:R-ALL-9684302"
      hgnc "NA"
      map_id "R1_237"
      name "nucleoside_space_5'_minus_diphosphate(3_minus_)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_69"
      uniprot "NA"
    ]
    graphics [
      x 1216.2993381679312
      y 322.9113683515907
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_237"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 336
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9682563;PUBMED:20463816"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_274"
      name "nsp12 misincorporates a nucleotide in nascent RNA minus strand"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9682563__layout_143"
      uniprot "NA"
    ]
    graphics [
      x 731.3360922535551
      y 1515.3405380627596
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_274"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 337
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A33019;urn:miriam:reactome:R-ALL-111294"
      hgnc "NA"
      map_id "R1_36"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_145"
      uniprot "NA"
    ]
    graphics [
      x 819.7419367854467
      y 1593.4127282817615
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 338
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9685939;PUBMED:19398035;PUBMED:15194747"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_351"
      name "3a localizes to the cell membrane"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9685939__layout_580"
      uniprot "NA"
    ]
    graphics [
      x 995.3203562739657
      y 1561.6143039502292
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_351"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 339
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9685962;urn:miriam:uniprot:P59632;urn:miriam:reactome:R-COV-9694433"
      hgnc "NA"
      map_id "R1_216"
      name "O_minus_glycosyl_space_3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_581"
      uniprot "UNIPROT:P59632"
    ]
    graphics [
      x 1012.3592709691222
      y 1747.6322785623038
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_216"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 340
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9685917;urn:miriam:reactome:R-COV-9694622;urn:miriam:refseq:NC_004718.3"
      hgnc "NA"
      map_id "R1_81"
      name "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA4"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_1971"
      uniprot "NA"
    ]
    graphics [
      x 1255.9709378567957
      y 1275.0927636610043
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 341
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683656;PUBMED:12917450"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_295"
      name "mRNA4 is translated to protein E"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683656__layout_231"
      uniprot "NA"
    ]
    graphics [
      x 1113.7208567054072
      y 1211.2869888710593
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_295"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 342
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:reactome:R-ALL-113519"
      hgnc "NA"
      map_id "R1_138"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_277"
      uniprot "NA"
    ]
    graphics [
      x 1731.1921428547967
      y 906.9869231089588
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_138"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 343
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683663;PUBMED:25348530"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_296"
      name "N-glycan trimming of Spike"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683663__layout_276"
      uniprot "NA"
    ]
    graphics [
      x 1764.0794796992266
      y 1033.5624717160135
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_296"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 344
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15903;urn:miriam:reactome:R-ALL-9683087"
      hgnc "NA"
      map_id "R1_139"
      name "beta_minus_D_minus_glucose"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_278"
      uniprot "NA"
    ]
    graphics [
      x 1889.9053588263432
      y 1047.5605190913573
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_139"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 345
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684336;PUBMED:15564471"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_338"
      name "nsp1-4 cleaves itself"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684336__layout_39"
      uniprot "NA"
    ]
    graphics [
      x 657.2770239529646
      y 874.7734175690414
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_338"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 346
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9684311;urn:miriam:reactome:R-COV-9694778;urn:miriam:uniprot:P0C6U8"
      hgnc "NA"
      map_id "R1_248"
      name "pp1a_minus_nsp1_minus_4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_75"
      uniprot "UNIPROT:P0C6U8"
    ]
    graphics [
      x 671.7354725989694
      y 1036.4590567008972
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_248"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 347
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:reactome:R-ALL-29356"
      hgnc "NA"
      map_id "R1_50"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_156"
      uniprot "NA"
    ]
    graphics [
      x 186.69711848992517
      y 1584.655094619289
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 348
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684018;PUBMED:15220459;PUBMED:19208801;PUBMED:12456663;PUBMED:6165837"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_319"
      name "nsp14 acts as a cap N7 methyltransferase to modify SARS-CoV-1 gRNA complement (minus strand)"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684018__layout_153"
      uniprot "NA"
    ]
    graphics [
      x 150.89027767991593
      y 1442.1738069186033
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_319"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 349
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A37565;urn:miriam:reactome:R-ALL-29438"
      hgnc "NA"
      map_id "R1_47"
      name "GTP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_154"
      uniprot "NA"
    ]
    graphics [
      x 362.8992632867107
      y 1505.5594544242283
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 350
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A43474;urn:miriam:reactome:R-ALL-29372"
      hgnc "NA"
      map_id "R1_54"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_161"
      uniprot "NA"
    ]
    graphics [
      x 363.56760336692093
      y 1458.1087696030888
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 351
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A33019;urn:miriam:reactome:R-ALL-111294"
      hgnc "NA"
      map_id "R1_53"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_160"
      uniprot "NA"
    ]
    graphics [
      x 81.67346079048684
      y 1561.3440604822704
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 352
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9682197;urn:miriam:uniprot:P0C6U8;urn:miriam:uniprot:P0C6X7;urn:miriam:reactome:R-COV-9694748"
      hgnc "NA"
      map_id "R1_69"
      name "nsp5"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_1897"
      uniprot "UNIPROT:P0C6U8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 2085.573674516151
      y 1488.07746542257
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 353
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684323;PUBMED:14965777;PUBMED:14561748"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_337"
      name "3CLp forms a homodimer"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684323__layout_13"
      uniprot "NA"
    ]
    graphics [
      x 2070.370534311456
      y 1347.7237318663633
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_337"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 354
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9685914;urn:miriam:reactome:R-COV-9694325;urn:miriam:refseq:NC_004718.3"
      hgnc "NA"
      map_id "R1_80"
      name "m7G(5')pppAm_minus_capped,polyadenylated_space_mRNA3"
      node_subtype "RNA"
      node_type "species"
      org_id "layout_1970"
      uniprot "NA"
    ]
    graphics [
      x 1625.3769066118155
      y 1684.2517748057276
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 355
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683618;PUBMED:12917450;PUBMED:15963240;PUBMED:16500894"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_287"
      name "mRNA3 is translated to protein 3a"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683618__layout_228"
      uniprot "NA"
    ]
    graphics [
      x 1509.0252597712429
      y 1620.6197903182783
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_287"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 356
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683669;PUBMED:16684538;PUBMED:20129637"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_298"
      name "E protein gets N-glycosylated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683669__layout_354"
      uniprot "NA"
    ]
    graphics [
      x 1163.3426407557422
      y 883.2854167747823
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_298"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 357
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:pubmed:16684538;urn:miriam:reactome:R-COV-9683652;urn:miriam:uniprot:P59637;urn:miriam:reactome:R-COV-9694754"
      hgnc "NA"
      map_id "R1_169"
      name "N_minus_glycan_space_E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_355"
      uniprot "UNIPROT:P59637"
    ]
    graphics [
      x 1310.4531570551576
      y 896.7864095278687
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_169"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 358
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9684340;PUBMED:14561748"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_339"
      name "3CLp cleaves pp1ab"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9684340__layout_25"
      uniprot "NA"
    ]
    graphics [
      x 1765.0960104621838
      y 1498.2583293734056
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_339"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 359
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377;urn:miriam:reactome:R-ALL-29356"
      hgnc "NA"
      map_id "R1_215"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_579"
      uniprot "NA"
    ]
    graphics [
      x 1928.8179197930435
      y 1441.0048248968121
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_215"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 360
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9694697;urn:miriam:reactome:R-COV-9682232;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_160"
      name "pp1ab_minus_nsp9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_33"
      uniprot "UNIPROT:P0C6X7"
    ]
    graphics [
      x 1649.1634629027267
      y 1415.8781364057222
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_160"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 361
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9682244;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_173"
      name "pp1ab_minus_nsp7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_37"
      uniprot "UNIPROT:P0C6X7"
    ]
    graphics [
      x 1943.9275138138319
      y 1514.6377413102769
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_173"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 362
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9682196;urn:miriam:reactome:R-COV-9694504;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_141"
      name "pp1ab_minus_nsp5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_28"
      uniprot "UNIPROT:P0C6X7"
    ]
    graphics [
      x 1669.2911876378935
      y 1289.2656501378747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_141"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 363
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9684861;urn:miriam:reactome:R-COV-9694695;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_133"
      name "pp1ab_minus_nsp1_minus_4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_27"
      uniprot "UNIPROT:P0C6X7"
    ]
    graphics [
      x 1877.7602104107027
      y 1478.0423794999967
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_133"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 364
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9682242;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_153"
      name "pp1ab_minus_nsp8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_31"
      uniprot "UNIPROT:P0C6X7"
    ]
    graphics [
      x 1867.9556213385818
      y 1399.973952698888
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_153"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 365
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9682233;urn:miriam:reactome:R-COV-9694425;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_98"
      name "pp1ab_minus_nsp6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_2066"
      uniprot "UNIPROT:P0C6X7"
    ]
    graphics [
      x 1893.3301803631616
      y 1547.4235748744243
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 366
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-COV-9694372;urn:miriam:reactome:R-COV-9682216;urn:miriam:uniprot:P0C6X7"
      hgnc "NA"
      map_id "R1_176"
      name "pp1ab_minus_nsp10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "layout_38"
      uniprot "UNIPROT:P0C6X7"
    ]
    graphics [
      x 1672.479609344812
      y 1376.524851316928
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_176"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 367
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9680262;PUBMED:31645453;PUBMED:32258351;PUBMED:32284326;PUBMED:29511076;PUBMED:32253226;PUBMED:31233808;PUBMED:32094225;PUBMED:28124907"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_261"
      name "SARS coronavirus gRNA:RTC:RNA primer binds RTC inhibitors"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9680262__layout_1260"
      uniprot "NA"
    ]
    graphics [
      x 908.0952331576252
      y 1736.717873608369
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_261"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 368
    zlevel -1

    cd19dm [
      annotation "REACTOME:R-HSA-9683769;PUBMED:16474139"
      count 1
      diagram "R-HSA-9678108"
      full_annotation "NA"
      hgnc "NA"
      map_id "R1_315"
      name "O-glycosylation of 3a is terminated"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "reaction_9683769__layout_347"
      uniprot "NA"
    ]
    graphics [
      x 465.5951064093932
      y 1588.0319560282496
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_315"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 369
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:reactome:R-ALL-9699093;urn:miriam:obo.chebi:CHEBI%3A16556"
      hgnc "NA"
      map_id "R1_100"
      name "CMP_minus_Neu5Ac"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2068"
      uniprot "NA"
    ]
    graphics [
      x 301.45338612382363
      y 1602.7495246381845
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 370
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:uniprot:Q16842;urn:miriam:reactome:R-HSA-9683042;urn:miriam:uniprot:Q11201;urn:miriam:uniprot:Q11203;urn:miriam:uniprot:Q9UJ37;urn:miriam:uniprot:Q9H4F1;urn:miriam:uniprot:P15907;urn:miriam:uniprot:Q8NDV1;urn:miriam:uniprot:Q11206"
      hgnc "NA"
      map_id "R1_168"
      name "sialyltransferases"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "layout_351"
      uniprot "UNIPROT:Q16842;UNIPROT:Q11201;UNIPROT:Q11203;UNIPROT:Q9UJ37;UNIPROT:Q9H4F1;UNIPROT:P15907;UNIPROT:Q8NDV1;UNIPROT:Q11206"
    ]
    graphics [
      x 401.59219707107434
      y 1437.3476526564166
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_168"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 371
    zlevel -1

    cd19dm [
      count 1
      diagram "R-HSA-9678108"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17361;urn:miriam:reactome:R-ALL-9699094"
      hgnc "NA"
      map_id "R1_101"
      name "CMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "layout_2069"
      uniprot "NA"
    ]
    graphics [
      x 324.1141487824649
      y 1667.5336487408201
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "R1_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 372
    source 1
    target 2
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_32"
      target_id "R1_368"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 373
    source 3
    target 2
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_38"
      target_id "R1_368"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 374
    source 2
    target 4
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_368"
      target_id "R1_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 375
    source 5
    target 6
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_158"
      target_id "R1_276"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 376
    source 7
    target 6
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_13"
      target_id "R1_276"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 377
    source 6
    target 8
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_276"
      target_id "R1_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 378
    source 9
    target 10
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_59"
      target_id "R1_370"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 379
    source 11
    target 10
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_66"
      target_id "R1_370"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 380
    source 10
    target 12
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_370"
      target_id "R1_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 381
    source 13
    target 14
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_240"
      target_id "R1_371"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 382
    source 15
    target 14
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_96"
      target_id "R1_371"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 383
    source 14
    target 16
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_371"
      target_id "R1_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 384
    source 17
    target 18
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_245"
      target_id "R1_286"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 385
    source 18
    target 19
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_286"
      target_id "R1_132"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 386
    source 20
    target 21
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_170"
      target_id "R1_300"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 387
    source 22
    target 21
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_171"
      target_id "R1_300"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 388
    source 21
    target 23
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_300"
      target_id "R1_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 389
    source 24
    target 25
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_112"
      target_id "R1_322"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 390
    source 26
    target 25
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_191"
      target_id "R1_322"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 391
    source 25
    target 27
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_322"
      target_id "R1_190"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 392
    source 25
    target 28
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_322"
      target_id "R1_204"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 393
    source 25
    target 29
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_322"
      target_id "R1_194"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 394
    source 30
    target 31
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_28"
      target_id "R1_268"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 395
    source 32
    target 31
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_34"
      target_id "R1_268"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 396
    source 33
    target 31
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "MODULATION"
      source_id "R1_20"
      target_id "R1_268"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 397
    source 31
    target 1
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_268"
      target_id "R1_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 398
    source 31
    target 34
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_268"
      target_id "R1_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 399
    source 35
    target 36
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_206"
      target_id "R1_333"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 400
    source 37
    target 36
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_212"
      target_id "R1_333"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 401
    source 36
    target 38
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_333"
      target_id "R1_207"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 402
    source 36
    target 39
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_333"
      target_id "R1_214"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 403
    source 36
    target 40
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_333"
      target_id "R1_211"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 404
    source 41
    target 42
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_105"
      target_id "R1_320"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 405
    source 43
    target 42
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_49"
      target_id "R1_320"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 406
    source 42
    target 44
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_320"
      target_id "R1_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 407
    source 42
    target 28
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_320"
      target_id "R1_204"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 408
    source 42
    target 45
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_320"
      target_id "R1_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 409
    source 46
    target 47
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_119"
      target_id "R1_311"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 410
    source 48
    target 47
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_134"
      target_id "R1_311"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 411
    source 47
    target 49
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_311"
      target_id "R1_137"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 412
    source 47
    target 50
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_311"
      target_id "R1_136"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 413
    source 47
    target 51
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_311"
      target_id "R1_135"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 414
    source 52
    target 53
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_108"
      target_id "R1_321"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 415
    source 54
    target 53
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_63"
      target_id "R1_321"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 416
    source 53
    target 44
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_321"
      target_id "R1_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 417
    source 53
    target 28
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_321"
      target_id "R1_204"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 418
    source 53
    target 55
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_321"
      target_id "R1_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 419
    source 56
    target 57
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_92"
      target_id "R1_285"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 420
    source 57
    target 58
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_285"
      target_id "R1_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 421
    source 59
    target 60
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_184"
      target_id "R1_291"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 422
    source 60
    target 61
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_291"
      target_id "R1_131"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 423
    source 62
    target 63
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_233"
      target_id "R1_356"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 424
    source 39
    target 63
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_214"
      target_id "R1_356"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 425
    source 64
    target 63
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_40"
      target_id "R1_356"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 426
    source 63
    target 65
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_356"
      target_id "R1_200"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 427
    source 63
    target 66
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_356"
      target_id "R1_202"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 428
    source 67
    target 68
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_102"
      target_id "R1_279"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 429
    source 69
    target 68
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_15"
      target_id "R1_279"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 430
    source 68
    target 70
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_279"
      target_id "R1_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 431
    source 1
    target 71
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_32"
      target_id "R1_272"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 432
    source 72
    target 71
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_41"
      target_id "R1_272"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 433
    source 4
    target 71
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "MODULATION"
      source_id "R1_33"
      target_id "R1_272"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 434
    source 71
    target 73
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_272"
      target_id "R1_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 435
    source 71
    target 74
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_272"
      target_id "R1_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 436
    source 71
    target 34
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_272"
      target_id "R1_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 437
    source 75
    target 76
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_256"
      target_id "R1_265"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 438
    source 77
    target 76
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_5"
      target_id "R1_265"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 439
    source 76
    target 78
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_265"
      target_id "R1_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 440
    source 79
    target 80
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_77"
      target_id "R1_270"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 441
    source 81
    target 80
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_87"
      target_id "R1_270"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 442
    source 80
    target 82
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_270"
      target_id "R1_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 443
    source 83
    target 84
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_174"
      target_id "R1_292"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 444
    source 84
    target 85
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_292"
      target_id "R1_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 445
    source 86
    target 87
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_42"
      target_id "R1_369"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 446
    source 88
    target 87
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PHYSICAL_STIMULATION"
      source_id "R1_71"
      target_id "R1_369"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 447
    source 89
    target 87
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PHYSICAL_STIMULATION"
      source_id "R1_44"
      target_id "R1_369"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 448
    source 90
    target 87
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PHYSICAL_STIMULATION"
      source_id "R1_57"
      target_id "R1_369"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 449
    source 91
    target 87
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PHYSICAL_STIMULATION"
      source_id "R1_56"
      target_id "R1_369"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 450
    source 92
    target 87
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PHYSICAL_STIMULATION"
      source_id "R1_11"
      target_id "R1_369"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 451
    source 87
    target 93
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_369"
      target_id "R1_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 452
    source 94
    target 95
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_222"
      target_id "R1_326"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 453
    source 96
    target 95
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_225"
      target_id "R1_326"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 454
    source 85
    target 95
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_175"
      target_id "R1_326"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 455
    source 95
    target 97
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_326"
      target_id "R1_226"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 456
    source 98
    target 99
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_109"
      target_id "R1_349"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 457
    source 100
    target 99
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_188"
      target_id "R1_349"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 458
    source 99
    target 101
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_349"
      target_id "R1_110"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 459
    source 99
    target 102
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_349"
      target_id "R1_186"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 460
    source 103
    target 104
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_239"
      target_id "R1_332"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 461
    source 104
    target 105
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_332"
      target_id "R1_246"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 462
    source 106
    target 107
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_224"
      target_id "R1_323"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 463
    source 108
    target 107
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_223"
      target_id "R1_323"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 464
    source 107
    target 96
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_323"
      target_id "R1_225"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 465
    source 109
    target 110
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_253"
      target_id "R1_359"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 466
    source 111
    target 110
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PHYSICAL_STIMULATION"
      source_id "R1_55"
      target_id "R1_359"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 467
    source 110
    target 112
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_359"
      target_id "R1_254"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 468
    source 110
    target 113
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_359"
      target_id "R1_255"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 469
    source 114
    target 115
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_249"
      target_id "R1_361"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 470
    source 116
    target 115
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CATALYSIS"
      source_id "R1_252"
      target_id "R1_361"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 471
    source 117
    target 115
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "MODULATION"
      source_id "R1_3"
      target_id "R1_361"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 472
    source 115
    target 109
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_361"
      target_id "R1_253"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 473
    source 115
    target 118
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_361"
      target_id "R1_251"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 474
    source 86
    target 119
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_42"
      target_id "R1_366"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 475
    source 120
    target 119
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_259"
      target_id "R1_366"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 476
    source 119
    target 121
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_366"
      target_id "R1_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 477
    source 122
    target 123
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_58"
      target_id "R1_367"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 478
    source 124
    target 123
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_39"
      target_id "R1_367"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 479
    source 123
    target 125
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_367"
      target_id "R1_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 480
    source 126
    target 127
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_89"
      target_id "R1_284"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 481
    source 58
    target 127
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_90"
      target_id "R1_284"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 482
    source 127
    target 128
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_284"
      target_id "R1_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 483
    source 129
    target 130
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_7"
      target_id "R1_271"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 484
    source 131
    target 130
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_12"
      target_id "R1_271"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 485
    source 130
    target 7
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_271"
      target_id "R1_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 486
    source 132
    target 133
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_79"
      target_id "R1_289"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 487
    source 133
    target 46
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_289"
      target_id "R1_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 488
    source 13
    target 134
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_240"
      target_id "R1_260"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 489
    source 135
    target 134
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_244"
      target_id "R1_260"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 490
    source 134
    target 136
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_260"
      target_id "R1_241"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 491
    source 137
    target 138
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_122"
      target_id "R1_306"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 492
    source 139
    target 138
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_146"
      target_id "R1_306"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 493
    source 138
    target 140
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_306"
      target_id "R1_147"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 494
    source 138
    target 20
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_306"
      target_id "R1_170"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 495
    source 141
    target 142
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_9"
      target_id "R1_273"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 496
    source 143
    target 142
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_145"
      target_id "R1_273"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 497
    source 142
    target 131
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_273"
      target_id "R1_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 498
    source 28
    target 144
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_204"
      target_id "R1_264"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 499
    source 73
    target 144
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_73"
      target_id "R1_264"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 500
    source 144
    target 145
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_264"
      target_id "R1_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 501
    source 23
    target 146
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_172"
      target_id "R1_299"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 502
    source 146
    target 83
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_299"
      target_id "R1_174"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 503
    source 147
    target 148
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_72"
      target_id "R1_331"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 504
    source 148
    target 149
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_331"
      target_id "R1_208"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 505
    source 122
    target 150
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_58"
      target_id "R1_269"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 506
    source 151
    target 150
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_60"
      target_id "R1_269"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 507
    source 125
    target 150
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "MODULATION"
      source_id "R1_29"
      target_id "R1_269"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 508
    source 150
    target 28
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_269"
      target_id "R1_204"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 509
    source 150
    target 152
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_269"
      target_id "R1_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 510
    source 150
    target 45
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_269"
      target_id "R1_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 511
    source 150
    target 153
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_269"
      target_id "R1_247"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 512
    source 28
    target 154
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_204"
      target_id "R1_346"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 513
    source 45
    target 154
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_78"
      target_id "R1_346"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 514
    source 154
    target 122
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_346"
      target_id "R1_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 515
    source 155
    target 156
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_124"
      target_id "R1_303"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 516
    source 156
    target 157
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_303"
      target_id "R1_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 517
    source 136
    target 158
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_241"
      target_id "R1_362"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 518
    source 158
    target 114
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_362"
      target_id "R1_249"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 519
    source 159
    target 160
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_107"
      target_id "R1_318"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 520
    source 161
    target 160
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_64"
      target_id "R1_318"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 521
    source 152
    target 160
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_106"
      target_id "R1_318"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 522
    source 54
    target 160
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_63"
      target_id "R1_318"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 523
    source 160
    target 162
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_318"
      target_id "R1_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 524
    source 160
    target 52
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_318"
      target_id "R1_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 525
    source 160
    target 163
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_318"
      target_id "R1_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 526
    source 160
    target 164
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_318"
      target_id "R1_232"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 527
    source 165
    target 166
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_151"
      target_id "R1_313"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 528
    source 166
    target 167
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_313"
      target_id "R1_152"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 529
    source 168
    target 169
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_37"
      target_id "R1_275"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 530
    source 170
    target 169
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_35"
      target_id "R1_275"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 531
    source 169
    target 1
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_275"
      target_id "R1_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 532
    source 169
    target 171
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_275"
      target_id "R1_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 533
    source 172
    target 173
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_143"
      target_id "R1_294"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 534
    source 139
    target 173
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_146"
      target_id "R1_294"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 535
    source 173
    target 140
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_294"
      target_id "R1_147"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 536
    source 173
    target 174
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_294"
      target_id "R1_148"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 537
    source 175
    target 176
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_68"
      target_id "R1_301"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 538
    source 177
    target 176
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_183"
      target_id "R1_301"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 539
    source 176
    target 59
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_301"
      target_id "R1_184"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 540
    source 176
    target 178
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_301"
      target_id "R1_130"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 541
    source 179
    target 180
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_220"
      target_id "R1_325"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 542
    source 181
    target 180
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_221"
      target_id "R1_325"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 543
    source 180
    target 94
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_325"
      target_id "R1_222"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 544
    source 182
    target 183
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_93"
      target_id "R1_277"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 545
    source 5
    target 183
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_158"
      target_id "R1_277"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 546
    source 183
    target 184
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_277"
      target_id "R1_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 547
    source 185
    target 186
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_82"
      target_id "R1_288"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 548
    source 186
    target 187
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_288"
      target_id "R1_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 549
    source 101
    target 188
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_110"
      target_id "R1_317"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 550
    source 189
    target 188
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_189"
      target_id "R1_317"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 551
    source 190
    target 188
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_192"
      target_id "R1_317"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 552
    source 26
    target 188
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_191"
      target_id "R1_317"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 553
    source 188
    target 191
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_317"
      target_id "R1_193"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 554
    source 188
    target 27
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_317"
      target_id "R1_190"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 555
    source 188
    target 24
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_317"
      target_id "R1_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 556
    source 188
    target 102
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_317"
      target_id "R1_186"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 557
    source 192
    target 193
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_24"
      target_id "R1_267"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 558
    source 194
    target 193
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_27"
      target_id "R1_267"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 559
    source 193
    target 30
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_267"
      target_id "R1_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 560
    source 193
    target 195
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_267"
      target_id "R1_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 561
    source 196
    target 197
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_185"
      target_id "R1_336"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 562
    source 198
    target 197
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_187"
      target_id "R1_336"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 563
    source 199
    target 197
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CATALYSIS"
      source_id "R1_250"
      target_id "R1_336"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 564
    source 197
    target 200
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_336"
      target_id "R1_205"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 565
    source 197
    target 201
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_336"
      target_id "R1_198"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 566
    source 197
    target 202
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_336"
      target_id "R1_213"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 567
    source 197
    target 37
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_336"
      target_id "R1_212"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 568
    source 203
    target 204
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_2"
      target_id "R1_263"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 569
    source 120
    target 204
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_259"
      target_id "R1_263"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 570
    source 204
    target 205
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_263"
      target_id "R1_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 571
    source 113
    target 206
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_255"
      target_id "R1_360"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 572
    source 206
    target 207
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_360"
      target_id "R1_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 573
    source 206
    target 61
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_360"
      target_id "R1_131"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 574
    source 208
    target 209
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_167"
      target_id "R1_283"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 575
    source 210
    target 209
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_113"
      target_id "R1_283"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 576
    source 209
    target 211
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_283"
      target_id "R1_114"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 577
    source 212
    target 213
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_181"
      target_id "R1_358"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 578
    source 214
    target 213
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_228"
      target_id "R1_358"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 579
    source 215
    target 213
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_235"
      target_id "R1_358"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 580
    source 213
    target 216
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_358"
      target_id "R1_227"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 581
    source 217
    target 218
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_8"
      target_id "R1_364"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 582
    source 219
    target 218
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_142"
      target_id "R1_364"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 583
    source 218
    target 220
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_364"
      target_id "R1_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 584
    source 192
    target 221
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_24"
      target_id "R1_347"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 585
    source 100
    target 221
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_188"
      target_id "R1_347"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 586
    source 222
    target 221
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PHYSICAL_STIMULATION"
      source_id "R1_116"
      target_id "R1_347"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 587
    source 221
    target 98
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_347"
      target_id "R1_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 588
    source 221
    target 195
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_347"
      target_id "R1_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 589
    source 61
    target 223
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_131"
      target_id "R1_314"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 590
    source 223
    target 179
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_314"
      target_id "R1_220"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 591
    source 224
    target 225
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_149"
      target_id "R1_341"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 592
    source 226
    target 225
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_257"
      target_id "R1_341"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 593
    source 227
    target 225
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CATALYSIS"
      source_id "R1_238"
      target_id "R1_341"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 594
    source 225
    target 228
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_341"
      target_id "R1_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 595
    source 225
    target 92
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_341"
      target_id "R1_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 596
    source 225
    target 229
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_341"
      target_id "R1_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 597
    source 230
    target 231
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_229"
      target_id "R1_345"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 598
    source 231
    target 232
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_345"
      target_id "R1_230"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 599
    source 233
    target 234
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_218"
      target_id "R1_352"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 600
    source 234
    target 235
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_352"
      target_id "R1_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 601
    source 97
    target 236
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_226"
      target_id "R1_327"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 602
    source 237
    target 236
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_155"
      target_id "R1_327"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 603
    source 236
    target 215
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_327"
      target_id "R1_235"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 604
    source 238
    target 239
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_140"
      target_id "R1_316"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 605
    source 240
    target 239
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CATALYSIS"
      source_id "R1_144"
      target_id "R1_316"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 606
    source 239
    target 172
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_316"
      target_id "R1_143"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 607
    source 205
    target 241
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_4"
      target_id "R1_262"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 608
    source 242
    target 241
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_161"
      target_id "R1_262"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 609
    source 120
    target 241
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_259"
      target_id "R1_262"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 610
    source 241
    target 129
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_262"
      target_id "R1_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 611
    source 86
    target 243
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_42"
      target_id "R1_365"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 612
    source 89
    target 243
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_44"
      target_id "R1_365"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 613
    source 243
    target 244
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_365"
      target_id "R1_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 614
    source 245
    target 246
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_121"
      target_id "R1_302"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 615
    source 246
    target 247
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_302"
      target_id "R1_162"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 616
    source 224
    target 248
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_149"
      target_id "R1_340"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 617
    source 248
    target 227
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_340"
      target_id "R1_238"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 618
    source 249
    target 250
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_18"
      target_id "R1_354"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 619
    source 251
    target 250
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_16"
      target_id "R1_354"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 620
    source 252
    target 250
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_203"
      target_id "R1_354"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 621
    source 250
    target 28
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_354"
      target_id "R1_204"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 622
    source 174
    target 253
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_148"
      target_id "R1_305"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 623
    source 253
    target 165
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_305"
      target_id "R1_151"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 624
    source 254
    target 255
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_95"
      target_id "R1_348"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 625
    source 116
    target 255
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_252"
      target_id "R1_348"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 626
    source 255
    target 117
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_348"
      target_id "R1_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 627
    source 256
    target 257
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_61"
      target_id "R1_335"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 628
    source 92
    target 257
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_11"
      target_id "R1_335"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 629
    source 258
    target 257
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CATALYSIS"
      source_id "R1_31"
      target_id "R1_335"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 630
    source 259
    target 257
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "MODULATION"
      source_id "R1_210"
      target_id "R1_335"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 631
    source 257
    target 260
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_335"
      target_id "R1_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 632
    source 257
    target 261
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_335"
      target_id "R1_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 633
    source 257
    target 262
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_335"
      target_id "R1_111"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 634
    source 257
    target 263
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_335"
      target_id "R1_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 635
    source 257
    target 264
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_335"
      target_id "R1_125"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 636
    source 257
    target 265
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_335"
      target_id "R1_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 637
    source 61
    target 266
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_131"
      target_id "R1_290"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 638
    source 266
    target 267
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_290"
      target_id "R1_182"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 639
    source 73
    target 268
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_73"
      target_id "R1_324"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 640
    source 179
    target 268
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_220"
      target_id "R1_324"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 641
    source 268
    target 181
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_324"
      target_id "R1_221"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 642
    source 269
    target 270
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_217"
      target_id "R1_353"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 643
    source 233
    target 270
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_218"
      target_id "R1_353"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 644
    source 270
    target 271
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_353"
      target_id "R1_219"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 645
    source 272
    target 273
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_195"
      target_id "R1_350"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 646
    source 29
    target 273
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_194"
      target_id "R1_350"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 647
    source 273
    target 274
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_350"
      target_id "R1_196"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 648
    source 273
    target 275
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_350"
      target_id "R1_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 649
    source 141
    target 276
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_9"
      target_id "R1_282"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 650
    source 208
    target 276
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_167"
      target_id "R1_282"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 651
    source 276
    target 249
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_282"
      target_id "R1_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 652
    source 65
    target 277
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_200"
      target_id "R1_355"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 653
    source 66
    target 277
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_202"
      target_id "R1_355"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 654
    source 277
    target 252
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_355"
      target_id "R1_203"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 655
    source 278
    target 279
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_84"
      target_id "R1_342"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 656
    source 280
    target 279
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_201"
      target_id "R1_342"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 657
    source 281
    target 279
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CATALYSIS"
      source_id "R1_243"
      target_id "R1_342"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 658
    source 279
    target 202
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_342"
      target_id "R1_213"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 659
    source 279
    target 37
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_342"
      target_id "R1_212"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 660
    source 147
    target 282
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_72"
      target_id "R1_334"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 661
    source 282
    target 224
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_334"
      target_id "R1_149"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 662
    source 187
    target 283
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_123"
      target_id "R1_310"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 663
    source 284
    target 283
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_179"
      target_id "R1_310"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 664
    source 283
    target 285
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_310"
      target_id "R1_180"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 665
    source 283
    target 286
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_310"
      target_id "R1_178"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 666
    source 283
    target 287
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_310"
      target_id "R1_177"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 667
    source 35
    target 288
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_206"
      target_id "R1_330"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 668
    source 280
    target 288
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_201"
      target_id "R1_330"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 669
    source 288
    target 38
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_330"
      target_id "R1_207"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 670
    source 288
    target 40
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_330"
      target_id "R1_211"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 671
    source 288
    target 289
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_330"
      target_id "R1_209"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 672
    source 145
    target 290
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_19"
      target_id "R1_278"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 673
    source 291
    target 290
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_23"
      target_id "R1_278"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 674
    source 290
    target 292
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_278"
      target_id "R1_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 675
    source 290
    target 293
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_278"
      target_id "R1_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 676
    source 290
    target 192
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_278"
      target_id "R1_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 677
    source 69
    target 294
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_15"
      target_id "R1_281"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 678
    source 8
    target 294
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_14"
      target_id "R1_281"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 679
    source 294
    target 251
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_281"
      target_id "R1_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 680
    source 224
    target 295
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_149"
      target_id "R1_329"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 681
    source 296
    target 295
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_52"
      target_id "R1_329"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 682
    source 258
    target 295
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CATALYSIS"
      source_id "R1_31"
      target_id "R1_329"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 683
    source 259
    target 295
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "MODULATION"
      source_id "R1_210"
      target_id "R1_329"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 684
    source 295
    target 228
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_329"
      target_id "R1_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 685
    source 295
    target 92
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_329"
      target_id "R1_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 686
    source 295
    target 229
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_329"
      target_id "R1_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 687
    source 187
    target 297
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_123"
      target_id "R1_304"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 688
    source 297
    target 108
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_304"
      target_id "R1_223"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 689
    source 298
    target 299
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_150"
      target_id "R1_280"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 690
    source 299
    target 69
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_280"
      target_id "R1_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 691
    source 300
    target 301
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_83"
      target_id "R1_308"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 692
    source 301
    target 155
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_308"
      target_id "R1_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 693
    source 136
    target 302
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_241"
      target_id "R1_363"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 694
    source 75
    target 302
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CATALYSIS"
      source_id "R1_256"
      target_id "R1_363"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 695
    source 78
    target 302
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "MODULATION"
      source_id "R1_6"
      target_id "R1_363"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 696
    source 302
    target 303
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_363"
      target_id "R1_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 697
    source 302
    target 109
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_363"
      target_id "R1_253"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 698
    source 258
    target 304
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_31"
      target_id "R1_266"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 699
    source 304
    target 259
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_266"
      target_id "R1_210"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 700
    source 287
    target 305
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_177"
      target_id "R1_307"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 701
    source 305
    target 106
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_307"
      target_id "R1_224"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 702
    source 306
    target 307
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_126"
      target_id "R1_297"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 703
    source 155
    target 307
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_124"
      target_id "R1_297"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 704
    source 308
    target 307
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CATALYSIS"
      source_id "R1_117"
      target_id "R1_297"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 705
    source 12
    target 307
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "MODULATION"
      source_id "R1_67"
      target_id "R1_297"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 706
    source 307
    target 309
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_297"
      target_id "R1_127"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 707
    source 307
    target 310
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_297"
      target_id "R1_128"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 708
    source 307
    target 311
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_297"
      target_id "R1_129"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 709
    source 312
    target 313
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_76"
      target_id "R1_343"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 710
    source 55
    target 313
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_74"
      target_id "R1_343"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 711
    source 313
    target 73
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_343"
      target_id "R1_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 712
    source 313
    target 314
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_343"
      target_id "R1_231"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 713
    source 216
    target 315
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_227"
      target_id "R1_344"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 714
    source 315
    target 230
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_344"
      target_id "R1_229"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 715
    source 316
    target 317
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_197"
      target_id "R1_357"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 716
    source 310
    target 317
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_128"
      target_id "R1_357"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 717
    source 318
    target 317
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CATALYSIS"
      source_id "R1_258"
      target_id "R1_357"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 718
    source 317
    target 319
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_357"
      target_id "R1_242"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 719
    source 317
    target 320
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_357"
      target_id "R1_199"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 720
    source 317
    target 177
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_357"
      target_id "R1_183"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 721
    source 321
    target 322
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_166"
      target_id "R1_309"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 722
    source 322
    target 212
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_309"
      target_id "R1_181"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 723
    source 323
    target 324
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_154"
      target_id "R1_293"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 724
    source 167
    target 324
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_152"
      target_id "R1_293"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 725
    source 325
    target 324
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CATALYSIS"
      source_id "R1_159"
      target_id "R1_293"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 726
    source 324
    target 326
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_293"
      target_id "R1_156"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 727
    source 324
    target 237
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_293"
      target_id "R1_155"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 728
    source 324
    target 327
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_293"
      target_id "R1_157"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 729
    source 328
    target 329
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_163"
      target_id "R1_312"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 730
    source 247
    target 329
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_162"
      target_id "R1_312"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 731
    source 330
    target 329
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CATALYSIS"
      source_id "R1_165"
      target_id "R1_312"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 732
    source 329
    target 326
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_312"
      target_id "R1_156"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 733
    source 329
    target 331
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_312"
      target_id "R1_164"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 734
    source 329
    target 327
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_312"
      target_id "R1_157"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 735
    source 332
    target 333
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_234"
      target_id "R1_328"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 736
    source 202
    target 333
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_213"
      target_id "R1_328"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 737
    source 333
    target 334
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_328"
      target_id "R1_236"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 738
    source 333
    target 62
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_328"
      target_id "R1_233"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 739
    source 333
    target 335
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_328"
      target_id "R1_237"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 740
    source 1
    target 336
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_32"
      target_id "R1_274"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 741
    source 32
    target 336
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_34"
      target_id "R1_274"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 742
    source 30
    target 336
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CATALYSIS"
      source_id "R1_28"
      target_id "R1_274"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 743
    source 33
    target 336
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "MODULATION"
      source_id "R1_20"
      target_id "R1_274"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 744
    source 336
    target 170
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_274"
      target_id "R1_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 745
    source 336
    target 337
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_274"
      target_id "R1_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 746
    source 212
    target 338
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_181"
      target_id "R1_351"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 747
    source 338
    target 339
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_351"
      target_id "R1_216"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 748
    source 340
    target 341
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_81"
      target_id "R1_295"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 749
    source 341
    target 137
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_295"
      target_id "R1_122"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 750
    source 342
    target 343
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_138"
      target_id "R1_296"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 751
    source 51
    target 343
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_135"
      target_id "R1_296"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 752
    source 219
    target 343
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CATALYSIS"
      source_id "R1_142"
      target_id "R1_296"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 753
    source 220
    target 343
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "MODULATION"
      source_id "R1_10"
      target_id "R1_296"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 754
    source 343
    target 238
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_296"
      target_id "R1_140"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 755
    source 343
    target 344
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_296"
      target_id "R1_139"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 756
    source 196
    target 345
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_185"
      target_id "R1_338"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 757
    source 198
    target 345
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_187"
      target_id "R1_338"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 758
    source 346
    target 345
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CATALYSIS"
      source_id "R1_248"
      target_id "R1_338"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 759
    source 345
    target 200
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_338"
      target_id "R1_205"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 760
    source 345
    target 201
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_338"
      target_id "R1_198"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 761
    source 345
    target 280
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_338"
      target_id "R1_201"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 762
    source 347
    target 348
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_50"
      target_id "R1_319"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 763
    source 349
    target 348
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_47"
      target_id "R1_319"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 764
    source 43
    target 348
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_49"
      target_id "R1_319"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 765
    source 74
    target 348
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_103"
      target_id "R1_319"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 766
    source 348
    target 350
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_319"
      target_id "R1_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 767
    source 348
    target 41
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_319"
      target_id "R1_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 768
    source 348
    target 44
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_319"
      target_id "R1_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 769
    source 348
    target 351
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_319"
      target_id "R1_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 770
    source 352
    target 353
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_69"
      target_id "R1_337"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 771
    source 353
    target 258
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_337"
      target_id "R1_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 772
    source 354
    target 355
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_80"
      target_id "R1_287"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 773
    source 355
    target 245
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_287"
      target_id "R1_121"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 774
    source 137
    target 356
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_122"
      target_id "R1_298"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 775
    source 48
    target 356
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_134"
      target_id "R1_298"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 776
    source 356
    target 49
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_298"
      target_id "R1_137"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 777
    source 356
    target 357
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_298"
      target_id "R1_169"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 778
    source 356
    target 50
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_298"
      target_id "R1_136"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 779
    source 149
    target 358
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_208"
      target_id "R1_339"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 780
    source 359
    target 358
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_215"
      target_id "R1_339"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 781
    source 258
    target 358
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CATALYSIS"
      source_id "R1_31"
      target_id "R1_339"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 782
    source 259
    target 358
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "MODULATION"
      source_id "R1_210"
      target_id "R1_339"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 783
    source 358
    target 360
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_339"
      target_id "R1_160"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 784
    source 358
    target 208
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_339"
      target_id "R1_167"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 785
    source 358
    target 361
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_339"
      target_id "R1_173"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 786
    source 358
    target 143
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_339"
      target_id "R1_145"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 787
    source 358
    target 362
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_339"
      target_id "R1_141"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 788
    source 358
    target 363
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_339"
      target_id "R1_133"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 789
    source 358
    target 364
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_339"
      target_id "R1_153"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 790
    source 358
    target 5
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_339"
      target_id "R1_158"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 791
    source 358
    target 365
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_339"
      target_id "R1_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 792
    source 358
    target 366
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_339"
      target_id "R1_176"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 793
    source 358
    target 298
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_339"
      target_id "R1_150"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 794
    source 358
    target 242
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_339"
      target_id "R1_161"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 795
    source 30
    target 367
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_28"
      target_id "R1_261"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 796
    source 3
    target 367
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_38"
      target_id "R1_261"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 797
    source 367
    target 33
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_261"
      target_id "R1_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 798
    source 331
    target 368
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_164"
      target_id "R1_315"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 799
    source 369
    target 368
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CONSPUMPTION"
      source_id "R1_100"
      target_id "R1_315"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 800
    source 370
    target 368
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "CATALYSIS"
      source_id "R1_168"
      target_id "R1_315"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 801
    source 368
    target 371
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_315"
      target_id "R1_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 802
    source 368
    target 321
    cd19dm [
      diagram "R-HSA-9678108"
      edge_type "PRODUCTION"
      source_id "R1_315"
      target_id "R1_166"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
