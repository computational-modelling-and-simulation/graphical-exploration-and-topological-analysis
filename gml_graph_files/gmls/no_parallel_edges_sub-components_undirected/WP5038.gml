# generated with VANTED V2.8.2 at Fri Mar 04 09:53:04 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_30"
      name "NA"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "b4d82"
      uniprot "NA"
    ]
    graphics [
      x 778.8239036640557
      y 536.3777914833768
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_57"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "cb6e2"
      uniprot "NA"
    ]
    graphics [
      x 899.1546982626504
      y 498.53972898762584
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:uniprot:Q9BYF1"
      hgnc "NA"
      map_id "W10_2"
      name "ACE2"
      node_subtype "GENE"
      node_type "species"
      org_id "a23f4"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1065.553968969586
      y 480.03195958325523
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_55"
      name "NA"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "ca5d0"
      uniprot "NA"
    ]
    graphics [
      x 1278.2066352990812
      y 329.10476848160033
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "PUBMED:33015593"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_8"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "a40d3"
      uniprot "NA"
    ]
    graphics [
      x 1079.580500246887
      y 254.69811906442067
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:uniprot:Q8N884"
      hgnc "NA"
      map_id "W10_43"
      name "cGAS"
      node_subtype "GENE"
      node_type "species"
      org_id "c16eb"
      uniprot "UNIPROT:Q8N884"
    ]
    graphics [
      x 872.7362708001673
      y 279.1204391033157
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:uniprot:Q9Y5S8"
      hgnc "NA"
      map_id "W10_37"
      name "NOX1"
      node_subtype "GENE"
      node_type "species"
      org_id "b9d18"
      uniprot "UNIPROT:Q9Y5S8"
    ]
    graphics [
      x 1257.5122381978633
      y 880.1276790830811
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "PUBMED:33106987"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_108"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id1baa9d1"
      uniprot "NA"
    ]
    graphics [
      x 1380.7289497461463
      y 898.9224379366999
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A26523"
      hgnc "NA"
      map_id "W10_92"
      name "ROS"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "ed2b8"
      uniprot "NA"
    ]
    graphics [
      x 1491.9522978171567
      y 862.5681524730586
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:wikidata:Q106020256"
      hgnc "NA"
      map_id "W10_1"
      name "S1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a19f1"
      uniprot "NA"
    ]
    graphics [
      x 1369.514532116322
      y 691.1594325993069
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "PUBMED:32818486;PUBMED:33116300"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_86"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "e5c68"
      uniprot "NA"
    ]
    graphics [
      x 1525.1521099258039
      y 772.7788637443654
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ensembl:ENSG00000135047"
      hgnc "NA"
      map_id "W10_69"
      name "CTSL"
      node_subtype "GENE"
      node_type "species"
      org_id "d39ba"
      uniprot "NA"
    ]
    graphics [
      x 1596.9390801278842
      y 893.8743362046447
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_52"
      name "Endocytosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "c78a2"
      uniprot "NA"
    ]
    graphics [
      x 1462.0923983194696
      y 664.1455695213543
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:uniprot:P0DTC6"
      hgnc "NA"
      map_id "W10_89"
      name "orf6"
      node_subtype "GENE"
      node_type "species"
      org_id "e9623"
      uniprot "UNIPROT:P0DTC6"
    ]
    graphics [
      x 614.7208725482942
      y 1319.4005776084368
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      annotation "PUBMED:32979938"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_109"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id4345b2ab"
      uniprot "NA"
    ]
    graphics [
      x 670.3855512438454
      y 1421.5278154790599
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ensembl:ENSG00000126456;urn:miriam:ensembl:ENSG00000185507"
      hgnc "NA"
      map_id "W10_21"
      name "ab03e"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ab03e"
      uniprot "NA"
    ]
    graphics [
      x 721.3770245674348
      y 1314.0919102865219
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:wikidata:Q94648377"
      hgnc "NA"
      map_id "W10_16"
      name "nsp13"
      node_subtype "GENE"
      node_type "species"
      org_id "a8ee9"
      uniprot "NA"
    ]
    graphics [
      x 624.5740424031562
      y 1008.5817338906797
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_104"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "fd3bd"
      uniprot "NA"
    ]
    graphics [
      x 530.434049805926
      y 1084.4439145375386
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ensembl:ENSG00000183735"
      hgnc "NA"
      map_id "W10_26"
      name "TBK1"
      node_subtype "GENE"
      node_type "species"
      org_id "ad4b3"
      uniprot "NA"
    ]
    graphics [
      x 636.7135302522503
      y 1140.3209517673683
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_93"
      name "Fusion"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "ee6e6"
      uniprot "NA"
    ]
    graphics [
      x 986.6905931545983
      y 502.9756150653599
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_18"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "aa497"
      uniprot "NA"
    ]
    graphics [
      x 1146.9985123278686
      y 427.3134932437915
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A2719"
      hgnc "NA"
      map_id "W10_7"
      name "angiotensin_space_II"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "a3fcb"
      uniprot "NA"
    ]
    graphics [
      x 1006.4270204409784
      y 786.1910424702966
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "PUBMED:32464637"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_75"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "da98c"
      uniprot "NA"
    ]
    graphics [
      x 951.0942941127817
      y 606.5235173080212
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A55438"
      hgnc "NA"
      map_id "W10_11"
      name "angiotensin_space_(1_minus_7)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "a6d7b"
      uniprot "NA"
    ]
    graphics [
      x 810.7921805215333
      y 455.6537005215298
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ensembl:ENSG00000184012"
      hgnc "NA"
      map_id "W10_72"
      name "TMPRSS2"
      node_subtype "GENE"
      node_type "species"
      org_id "d8284"
      uniprot "NA"
    ]
    graphics [
      x 1986.0822503922172
      y 401.0986169542133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "PUBMED:33116300"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_100"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f843b"
      uniprot "NA"
    ]
    graphics [
      x 2088.6453460986686
      y 446.7823133745537
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:wikidata:Q106020384"
      hgnc "NA"
      map_id "W10_29"
      name "S2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b27b6"
      uniprot "NA"
    ]
    graphics [
      x 2054.935893204
      y 553.5590037482049
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ensembl:ENSG00000080824.19;urn:miriam:ensembl:ENSG00000183735;urn:miriam:ensembl:ENSG00000126456"
      hgnc "NA"
      map_id "W10_73"
      name "d836e"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d836e"
      uniprot "NA"
    ]
    graphics [
      x 486.6624820524131
      y 1541.1208926773675
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "PUBMED:21597473;PUBMED:32979938;PUBMED:33019591"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_40"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "bacb0"
      uniprot "NA"
    ]
    graphics [
      x 542.5604560502253
      y 1416.20380804665
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ensembl:ENSG00000263528"
      hgnc "NA"
      map_id "W10_19"
      name "IKBKE"
      node_subtype "GENE"
      node_type "species"
      org_id "aa6d4"
      uniprot "NA"
    ]
    graphics [
      x 427.1307666128539
      y 1366.2839974648664
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ncbiprotein:YP_009725302"
      hgnc "NA"
      map_id "W10_99"
      name "nsp6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f6f18"
      uniprot "NA"
    ]
    graphics [
      x 483.9066797714437
      y 1309.934651553229
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "PUBMED:33015593"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_64"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "d15f2"
      uniprot "NA"
    ]
    graphics [
      x 729.2293000967013
      y 409.37783917223544
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A71580"
      hgnc "NA"
      map_id "W10_39"
      name "cGAMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "ba77f"
      uniprot "NA"
    ]
    graphics [
      x 665.4789831993896
      y 612.1840980370764
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ensembl:ENSG00000154174"
      hgnc "NA"
      map_id "W10_41"
      name "TOMM70"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "bc950"
      uniprot "NA"
    ]
    graphics [
      x 670.9816345227426
      y 1681.8599303654141
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      annotation "PUBMED:33019591"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_32"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "b6c5f"
      uniprot "NA"
    ]
    graphics [
      x 555.2710330446624
      y 1643.5857029787617
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ensembl:ENSG00000160703"
      hgnc "NA"
      map_id "W10_46"
      name "NLRX1"
      node_subtype "GENE"
      node_type "species"
      org_id "c2498"
      uniprot "NA"
    ]
    graphics [
      x 1017.4136177094742
      y 1612.0892293975826
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      annotation "PUBMED:23321557"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_62"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "cf0de"
      uniprot "NA"
    ]
    graphics [
      x 1100.1097732475264
      y 1672.6252272265501
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:wikipathways:WP4936"
      hgnc "NA"
      map_id "W10_63"
      name "Autophagy"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "d0d85"
      uniprot "NA"
    ]
    graphics [
      x 1236.9167683448077
      y 1671.0834482352561
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:wikidata:Q104520877"
      hgnc "NA"
      map_id "W10_101"
      name "orf9c"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f975c"
      uniprot "NA"
    ]
    graphics [
      x 1358.19369647308
      y 1751.0222922593284
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_96"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f4935"
      uniprot "NA"
    ]
    graphics [
      x 1171.2328597680787
      y 1737.1946864921547
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      annotation "PUBMED:19692591"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_58"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "cbf4e"
      uniprot "NA"
    ]
    graphics [
      x 1087.6868688237882
      y 1454.7992397472142
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:wikipathways:WP111"
      hgnc "NA"
      map_id "W10_3"
      name "Electron_space_Transport_space_Chain_space_(OXPHOS)_space_"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "a2943"
      uniprot "NA"
    ]
    graphics [
      x 1220.8146775069167
      y 1324.2734426308555
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "PUBMED:21187859"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_97"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "f5f6e"
      uniprot "NA"
    ]
    graphics [
      x 1647.209776989248
      y 767.5010418792643
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ensembl:ENSG00000077150;urn:miriam:ensembl:ENSG00000109320"
      hgnc "NA"
      map_id "W10_31"
      name "b6b94"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b6b94"
      uniprot "NA"
    ]
    graphics [
      x 1781.0083142745723
      y 705.0697392209795
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "PUBMED:33106987"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_110"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "id57f20ac8"
      uniprot "NA"
    ]
    graphics [
      x 1133.7801703511468
      y 840.7036477846794
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      annotation "PUBMED:21597473;PUBMED:33937724"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_82"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "e1040"
      uniprot "NA"
    ]
    graphics [
      x 889.7051611335835
      y 1173.2348470278876
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ncbigene:3438"
      hgnc "NA"
      map_id "W10_95"
      name "IFN_minus_I"
      node_subtype "GENE"
      node_type "species"
      org_id "f26de"
      uniprot "NA"
    ]
    graphics [
      x 1033.5229812635812
      y 1137.8773351685209
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_33"
      name "NA"
      node_subtype "POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "b6e4c"
      uniprot "NA"
    ]
    graphics [
      x 941.7674750745181
      y 380.03621886272185
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A2720"
      hgnc "NA"
      map_id "W10_38"
      name "AGT"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "b9dff"
      uniprot "NA"
    ]
    graphics [
      x 1530.3078473993392
      y 345.50973466919254
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      annotation "PUBMED:32464637;PUBMED:32818486"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_61"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "cecdf"
      uniprot "NA"
    ]
    graphics [
      x 1394.264183328623
      y 426.2924213677119
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A2718"
      hgnc "NA"
      map_id "W10_84"
      name "angiotensin_space_I"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "e2a9a"
      uniprot "NA"
    ]
    graphics [
      x 1238.2770118488418
      y 485.5063892331921
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_48"
      name "SARS_minus_CoV_minus_2"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "c4774"
      uniprot "NA"
    ]
    graphics [
      x 1403.7656659986235
      y 1425.8836871698124
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_36"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "b98e3"
      uniprot "NA"
    ]
    graphics [
      x 1482.6755318206567
      y 1506.3583340984815
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:uniprot:P0DTC4"
      hgnc "NA"
      map_id "W10_53"
      name "Envelope_space_Protein_space_E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c7c21"
      uniprot "UNIPROT:P0DTC4"
    ]
    graphics [
      x 1591.5679368303386
      y 1434.4066606756437
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ncbigene:148022"
      hgnc "NA"
      map_id "W10_60"
      name "TICAM1"
      node_subtype "GENE"
      node_type "species"
      org_id "cea88"
      uniprot "NA"
    ]
    graphics [
      x 1535.1881254574712
      y 1157.3542780487635
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_105"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "fe2c7"
      uniprot "NA"
    ]
    graphics [
      x 1422.1802203291
      y 1158.3853858419343
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ensembl:ENSG00000126456"
      hgnc "NA"
      map_id "W10_98"
      name "IRF3"
      node_subtype "GENE"
      node_type "species"
      org_id "f6899"
      uniprot "NA"
    ]
    graphics [
      x 1298.475226562469
      y 1116.0928409784453
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ensembl:ENSG00000074582"
      hgnc "NA"
      map_id "W10_12"
      name "BCS1L"
      node_subtype "GENE"
      node_type "species"
      org_id "a71fb"
      uniprot "NA"
    ]
    graphics [
      x 1536.6759608491466
      y 1491.4396767602702
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_15"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "a8e16"
      uniprot "NA"
    ]
    graphics [
      x 1443.0659534156566
      y 1346.0985791405542
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:wikipathways:WP4921"
      hgnc "NA"
      map_id "W10_5"
      name "Mitochondrial_space_CIII_space_assembly"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "a360f"
      uniprot "NA"
    ]
    graphics [
      x 1300.162432749341
      y 1279.984241635165
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      annotation "PUBMED:32464637"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_111"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id68e36368"
      uniprot "NA"
    ]
    graphics [
      x 1004.0518944036638
      y 948.9511411742634
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ensembl:ENSG00000180772"
      hgnc "NA"
      map_id "W10_9"
      name "AGTR2"
      node_subtype "GENE"
      node_type "species"
      org_id "a4260"
      uniprot "NA"
    ]
    graphics [
      x 979.4369431861561
      y 1102.618830138527
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      annotation "PUBMED:32818486"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_66"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "d1f58"
      uniprot "NA"
    ]
    graphics [
      x 1129.6837790075647
      y 650.3522428173154
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:uniprot:P12821"
      hgnc "NA"
      map_id "W10_81"
      name "ACE"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e0c12"
      uniprot "UNIPROT:P12821"
    ]
    graphics [
      x 1049.0993035528986
      y 563.7282485167834
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:uniprot:P0DTD1"
      hgnc "NA"
      map_id "W10_20"
      name "nsp2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "aaa58"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 856.8152982610072
      y 85.44593402224791
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      annotation "PUBMED:32699849"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_78"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "dd4b4"
      uniprot "NA"
    ]
    graphics [
      x 975.1045756053825
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:uniprot:Q99623;urn:miriam:uniprot:P35232"
      hgnc "NA"
      map_id "W10_56"
      name "ca82b"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ca82b"
      uniprot "UNIPROT:Q99623;UNIPROT:P35232"
    ]
    graphics [
      x 1078.0881550364168
      y 152.46837035081558
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      annotation "PUBMED:32574107"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_51"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "c5b62"
      uniprot "NA"
    ]
    graphics [
      x 884.8045902207455
      y 1329.1085579381827
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:uniprot:Q86WV6"
      hgnc "NA"
      map_id "W10_49"
      name "TMEM173"
      node_subtype "GENE"
      node_type "species"
      org_id "c488d"
      uniprot "UNIPROT:Q86WV6"
    ]
    graphics [
      x 759.7901717704982
      y 1027.1387268202127
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      annotation "PUBMED:32574107"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "b73fa"
      uniprot "NA"
    ]
    graphics [
      x 875.2111289080818
      y 914.466185145632
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ensembl:ENSG00000175104"
      hgnc "NA"
      map_id "W10_90"
      name "TRAF6"
      node_subtype "GENE"
      node_type "species"
      org_id "ea384"
      uniprot "NA"
    ]
    graphics [
      x 1908.6215333371206
      y 782.8915656164688
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      annotation "PUBMED:33015593"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_10"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "a5994"
      uniprot "NA"
    ]
    graphics [
      x 1907.0434651704925
      y 674.3608105929155
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_91"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "eca63"
      uniprot "NA"
    ]
    graphics [
      x 1143.045904751483
      y 1258.0269663486743
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      annotation "PUBMED:33116300"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_88"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "e7424"
      uniprot "NA"
    ]
    graphics [
      x 1228.266585606834
      y 591.9353992611709
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_106"
      name "dsRNA"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "ff0ff"
      uniprot "NA"
    ]
    graphics [
      x 1810.0500744715196
      y 1516.192899195692
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_17"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "aa323"
      uniprot "NA"
    ]
    graphics [
      x 1776.8618777198365
      y 1642.7164232044547
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ensembl:ENSG00000107201;urn:miriam:hgnc.symbol:IFIH1"
      hgnc "HGNC_SYMBOL:IFIH1"
      map_id "W10_35"
      name "b9671"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b9671"
      uniprot "NA"
    ]
    graphics [
      x 1675.8262400632927
      y 1753.3270191740678
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      annotation "PUBMED:32464637"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_116"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "ide445d5"
      uniprot "NA"
    ]
    graphics [
      x 639.8886417656643
      y 421.77572489898444
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ensembl:ENSG00000206470"
      hgnc "NA"
      map_id "W10_13"
      name "MAS1"
      node_subtype "GENE"
      node_type "species"
      org_id "a876a"
      uniprot "NA"
    ]
    graphics [
      x 524.570259447725
      y 465.89645825280303
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:uniprot:P0DTD2"
      hgnc "NA"
      map_id "W10_87"
      name "ORF9b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e62df"
      uniprot "UNIPROT:P0DTD2"
    ]
    graphics [
      x 901.9928739455981
      y 1593.6178476368605
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859;PUBMED:32728199"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_23"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "ac21e"
      uniprot "NA"
    ]
    graphics [
      x 789.1545954326359
      y 1649.2551329557748
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_27"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ae213"
      uniprot "NA"
    ]
    graphics [
      x 1256.945847634455
      y 1801.8521500891372
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ensembl:ENSG00000137806;urn:miriam:ensembl:ENSG00000147684;urn:miriam:ensembl:ENSG00000130159;urn:miriam:ensembl:ENSG00000177646"
      hgnc "NA"
      map_id "W10_54"
      name "c9f6c"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "c9f6c"
      uniprot "NA"
    ]
    graphics [
      x 1180.2120333400346
      y 1664.081396581901
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A80128"
      hgnc "NA"
      map_id "W10_102"
      name "angiotensin_minus_(1_minus_9)"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "f991b"
      uniprot "NA"
    ]
    graphics [
      x 1000.198697874765
      y 301.9323706860307
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      annotation "PUBMED:32464637"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_112"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id925594bb"
      uniprot "NA"
    ]
    graphics [
      x 958.0899324519821
      y 433.8474675631236
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      annotation "PUBMED:33015593"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_114"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idaa39462d"
      uniprot "NA"
    ]
    graphics [
      x 686.3209401203432
      y 818.2209700830089
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:uniprot:P0DTD2"
      hgnc "NA"
      map_id "W10_42"
      name "PLpro"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "bca7b"
      uniprot "UNIPROT:P0DTD2"
    ]
    graphics [
      x 1229.9917186346063
      y 1457.9658511988923
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      annotation "PUBMED:33015593;PUBMED:32726803"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_74"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "d8629"
      uniprot "NA"
    ]
    graphics [
      x 1285.7292043586585
      y 1564.5421325613402
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ensembl:ENSG00000126456"
      hgnc "NA"
      map_id "W10_47"
      name "IRF3"
      node_subtype "GENE"
      node_type "species"
      org_id "c429c"
      uniprot "NA"
    ]
    graphics [
      x 1376.2793355356202
      y 1528.7649481016797
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:uniprot:A0A1B0GUZ2"
      hgnc "NA"
      map_id "W10_68"
      name "Renin"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d29ef"
      uniprot "UNIPROT:A0A1B0GUZ2"
    ]
    graphics [
      x 1622.2874990868486
      y 191.09898683109293
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_22"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "abdce"
      uniprot "NA"
    ]
    graphics [
      x 1653.0056175028749
      y 291.05091292773784
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_4"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "a32af"
      uniprot "NA"
    ]
    graphics [
      x 954.5422132240643
      y 1756.6660314847559
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:wikipathways:WP3656"
      hgnc "NA"
      map_id "W10_107"
      name "Interleukin_minus_1_space_Induced_space_Activation_space_of_space_NF_minus_kappa_minus_B"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "ffab8"
      uniprot "NA"
    ]
    graphics [
      x 863.7865929903405
      y 1834.1290864602083
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:uniprot:P0DTD1"
      hgnc "NA"
      map_id "W10_67"
      name "nsp7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d2766"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 373.2622358465535
      y 1707.55622977596
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_77"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "dd0be"
      uniprot "NA"
    ]
    graphics [
      x 264.8289540921676
      y 1760.7411431714695
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ensembl:ENSG00000137806"
      hgnc "NA"
      map_id "W10_94"
      name "NDUFAF1"
      node_subtype "GENE"
      node_type "species"
      org_id "ee9df"
      uniprot "NA"
    ]
    graphics [
      x 255.67941333554575
      y 1646.1326146069255
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      annotation "PUBMED:32464637"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_115"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "iddf33d8ac"
      uniprot "NA"
    ]
    graphics [
      x 889.5071376250171
      y 816.834829352453
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ensembl:ENSG00000144891"
      hgnc "NA"
      map_id "W10_79"
      name "AGTR1"
      node_subtype "GENE"
      node_type "species"
      org_id "e034b"
      uniprot "NA"
    ]
    graphics [
      x 804.9027661964269
      y 730.4306696405981
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_44"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "c1a5d"
      uniprot "NA"
    ]
    graphics [
      x 1190.296961779956
      y 244.65008487244984
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ensembl:ENSG00000175104;urn:miriam:ensembl:ENSG00000088888;urn:miriam:ensembl:ENSG00000131323;urn:miriam:ensembl:ENSG00000154174"
      hgnc "NA"
      map_id "W10_25"
      name "ad3f8"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ad3f8"
      uniprot "NA"
    ]
    graphics [
      x 1225.740939748154
      y 364.50518735132925
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_113"
      name "NA"
      node_subtype "UNKNOWN_POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "ida0dbfe4d"
      uniprot "NA"
    ]
    graphics [
      x 1527.6442306252852
      y 649.8850364944637
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:wikipathways:WP3404"
      hgnc "NA"
      map_id "W10_28"
      name "Oxidative_space_Stress_space_Induced_space_Senescence_br_"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "b0078"
      uniprot "NA"
    ]
    graphics [
      x 1509.0084574053292
      y 514.044378209531
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      annotation "PUBMED:28956771;PUBMED:21703540"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_50"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "c58d8"
      uniprot "NA"
    ]
    graphics [
      x 1149.6783653465811
      y 1811.9562476095532
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ensembl:ENSG00000088888"
      hgnc "NA"
      map_id "W10_83"
      name "MAVS"
      node_subtype "GENE"
      node_type "species"
      org_id "e240d"
      uniprot "NA"
    ]
    graphics [
      x 1348.315343070787
      y 1878.8859295799514
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_71"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "d7e7c"
      uniprot "NA"
    ]
    graphics [
      x 1382.3278707902732
      y 505.1371963300137
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_85"
      name "NA"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "e45d6"
      uniprot "NA"
    ]
    graphics [
      x 138.7427986255459
      y 948.8355248260405
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_59"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "cc8d7"
      uniprot "NA"
    ]
    graphics [
      x 62.500000000000114
      y 864.1830075671364
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "W10_65"
      name "Membrane_space_Glycoprotein_space_M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d1b58"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 157.2075141072694
      y 801.4866701157055
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      annotation "PUBMED:32464637;PUBMED:32818486"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_70"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "d7130"
      uniprot "NA"
    ]
    graphics [
      x 1119.8730336641872
      y 357.5900129296034
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_103"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "fb344"
      uniprot "NA"
    ]
    graphics [
      x 1173.843398229316
      y 1504.0996322960814
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      annotation "PUBMED:31115493"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_6"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "a3897"
      uniprot "NA"
    ]
    graphics [
      x 1370.9700202721951
      y 1081.7278133264947
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859;PUBMED:32839770"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_24"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ad224"
      uniprot "NA"
    ]
    graphics [
      x 1487.2497166879361
      y 1653.2098797449448
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:hgnc.symbol:IFIH1"
      hgnc "HGNC_SYMBOL:IFIH1"
      map_id "W10_45"
      name "IFIH1"
      node_subtype "GENE"
      node_type "species"
      org_id "c1e37"
      uniprot "NA"
    ]
    graphics [
      x 475.39943522870067
      y 912.23169693636
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_14"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "a88cd"
      uniprot "NA"
    ]
    graphics [
      x 595.2428872616242
      y 944.3357814963458
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      annotation "PUBMED:33015593"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_80"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "e0662"
      uniprot "NA"
    ]
    graphics [
      x 759.8709133773766
      y 1137.1431577038093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      annotation "PUBMED:32464637"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_117"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "idf9d6210a"
      uniprot "NA"
    ]
    graphics [
      x 863.8189234345748
      y 615.4212353014486
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      annotation "PUBMED:32995797;PUBMED:17451827"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_76"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "db52e"
      uniprot "NA"
    ]
    graphics [
      x 1530.3378698223987
      y 1835.8118673206018
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 118
    source 1
    target 2
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_30"
      target_id "W10_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 119
    source 2
    target 3
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_57"
      target_id "W10_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 120
    source 4
    target 5
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_55"
      target_id "W10_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 121
    source 5
    target 6
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_8"
      target_id "W10_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 122
    source 7
    target 8
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_37"
      target_id "W10_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 123
    source 8
    target 9
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_108"
      target_id "W10_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 124
    source 10
    target 11
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_1"
      target_id "W10_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 125
    source 12
    target 11
    cd19dm [
      diagram "WP5038"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W10_69"
      target_id "W10_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 126
    source 11
    target 13
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_86"
      target_id "W10_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 127
    source 14
    target 15
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_89"
      target_id "W10_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 128
    source 15
    target 16
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_109"
      target_id "W10_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 129
    source 17
    target 18
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_16"
      target_id "W10_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 130
    source 18
    target 19
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_104"
      target_id "W10_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 131
    source 20
    target 21
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_93"
      target_id "W10_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 132
    source 21
    target 4
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_18"
      target_id "W10_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 133
    source 22
    target 23
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_7"
      target_id "W10_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 134
    source 3
    target 23
    cd19dm [
      diagram "WP5038"
      edge_type "CATALYSIS"
      source_id "W10_2"
      target_id "W10_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 135
    source 23
    target 24
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_75"
      target_id "W10_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 136
    source 25
    target 26
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_72"
      target_id "W10_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 137
    source 26
    target 27
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_100"
      target_id "W10_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 138
    source 28
    target 29
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_73"
      target_id "W10_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 139
    source 30
    target 29
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_19"
      target_id "W10_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 140
    source 31
    target 29
    cd19dm [
      diagram "WP5038"
      edge_type "INHIBITION"
      source_id "W10_99"
      target_id "W10_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 141
    source 29
    target 16
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_40"
      target_id "W10_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 142
    source 6
    target 32
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_43"
      target_id "W10_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 143
    source 32
    target 33
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_64"
      target_id "W10_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 144
    source 34
    target 35
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_41"
      target_id "W10_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 145
    source 35
    target 28
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_32"
      target_id "W10_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 146
    source 36
    target 37
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_46"
      target_id "W10_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 147
    source 37
    target 38
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_62"
      target_id "W10_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 148
    source 39
    target 40
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_101"
      target_id "W10_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 149
    source 40
    target 36
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_96"
      target_id "W10_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 150
    source 36
    target 41
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_46"
      target_id "W10_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 151
    source 41
    target 42
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_58"
      target_id "W10_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 152
    source 9
    target 43
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_92"
      target_id "W10_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 153
    source 43
    target 44
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_97"
      target_id "W10_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 154
    source 22
    target 45
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_7"
      target_id "W10_110"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 155
    source 45
    target 7
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_110"
      target_id "W10_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 156
    source 16
    target 46
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_21"
      target_id "W10_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 157
    source 46
    target 47
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_82"
      target_id "W10_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 158
    source 3
    target 48
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_2"
      target_id "W10_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 159
    source 48
    target 20
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_33"
      target_id "W10_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 160
    source 49
    target 50
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_38"
      target_id "W10_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 161
    source 50
    target 51
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_61"
      target_id "W10_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 162
    source 52
    target 53
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_48"
      target_id "W10_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 163
    source 53
    target 54
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_36"
      target_id "W10_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 164
    source 55
    target 56
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_60"
      target_id "W10_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 165
    source 56
    target 57
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_105"
      target_id "W10_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 166
    source 58
    target 59
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_12"
      target_id "W10_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 167
    source 59
    target 60
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_15"
      target_id "W10_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 168
    source 22
    target 61
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_7"
      target_id "W10_111"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 169
    source 61
    target 62
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_111"
      target_id "W10_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 170
    source 51
    target 63
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_84"
      target_id "W10_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 171
    source 64
    target 63
    cd19dm [
      diagram "WP5038"
      edge_type "MODULATION"
      source_id "W10_81"
      target_id "W10_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 172
    source 63
    target 22
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_66"
      target_id "W10_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 173
    source 65
    target 66
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_20"
      target_id "W10_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 174
    source 66
    target 67
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_78"
      target_id "W10_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 175
    source 36
    target 68
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_46"
      target_id "W10_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 176
    source 68
    target 69
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_51"
      target_id "W10_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 177
    source 22
    target 70
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_7"
      target_id "W10_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 178
    source 70
    target 69
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_34"
      target_id "W10_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 179
    source 71
    target 72
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_90"
      target_id "W10_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 180
    source 72
    target 44
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_10"
      target_id "W10_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 181
    source 60
    target 73
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_5"
      target_id "W10_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 182
    source 73
    target 42
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_91"
      target_id "W10_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 183
    source 3
    target 74
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_2"
      target_id "W10_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 184
    source 74
    target 10
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_88"
      target_id "W10_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 185
    source 75
    target 76
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_106"
      target_id "W10_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 186
    source 76
    target 77
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_17"
      target_id "W10_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 187
    source 24
    target 78
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_11"
      target_id "W10_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 188
    source 78
    target 79
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_116"
      target_id "W10_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 189
    source 80
    target 81
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_87"
      target_id "W10_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 190
    source 81
    target 34
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_23"
      target_id "W10_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 191
    source 39
    target 82
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_101"
      target_id "W10_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 192
    source 82
    target 83
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_27"
      target_id "W10_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 193
    source 84
    target 85
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_102"
      target_id "W10_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 194
    source 64
    target 85
    cd19dm [
      diagram "WP5038"
      edge_type "CATALYSIS"
      source_id "W10_81"
      target_id "W10_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 195
    source 85
    target 24
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_112"
      target_id "W10_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 196
    source 33
    target 86
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_39"
      target_id "W10_114"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 197
    source 86
    target 69
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_114"
      target_id "W10_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 198
    source 87
    target 88
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_42"
      target_id "W10_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 199
    source 88
    target 89
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_74"
      target_id "W10_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 200
    source 90
    target 91
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_68"
      target_id "W10_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 201
    source 91
    target 49
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_22"
      target_id "W10_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 202
    source 36
    target 92
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_46"
      target_id "W10_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 203
    source 92
    target 93
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_4"
      target_id "W10_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 204
    source 94
    target 95
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_67"
      target_id "W10_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 205
    source 95
    target 96
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_77"
      target_id "W10_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 206
    source 22
    target 97
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_7"
      target_id "W10_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 207
    source 97
    target 98
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_115"
      target_id "W10_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 208
    source 67
    target 99
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_56"
      target_id "W10_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 209
    source 99
    target 100
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_44"
      target_id "W10_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 210
    source 9
    target 101
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_92"
      target_id "W10_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 211
    source 101
    target 102
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_113"
      target_id "W10_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 212
    source 36
    target 103
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_46"
      target_id "W10_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 213
    source 103
    target 104
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_50"
      target_id "W10_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 214
    source 13
    target 105
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_52"
      target_id "W10_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 215
    source 105
    target 4
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_71"
      target_id "W10_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 216
    source 106
    target 107
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_85"
      target_id "W10_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 217
    source 107
    target 108
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_59"
      target_id "W10_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 218
    source 51
    target 109
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_84"
      target_id "W10_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 219
    source 3
    target 109
    cd19dm [
      diagram "WP5038"
      edge_type "CATALYSIS"
      source_id "W10_2"
      target_id "W10_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 220
    source 109
    target 84
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_70"
      target_id "W10_102"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 221
    source 83
    target 110
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_54"
      target_id "W10_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 222
    source 110
    target 42
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_103"
      target_id "W10_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 223
    source 42
    target 111
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_3"
      target_id "W10_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 224
    source 111
    target 9
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_6"
      target_id "W10_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 225
    source 39
    target 112
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_101"
      target_id "W10_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 226
    source 112
    target 58
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_24"
      target_id "W10_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 227
    source 113
    target 114
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_45"
      target_id "W10_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 228
    source 114
    target 69
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_14"
      target_id "W10_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 229
    source 69
    target 115
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_49"
      target_id "W10_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 230
    source 115
    target 19
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_80"
      target_id "W10_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 98
    target 116
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_79"
      target_id "W10_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 232
    source 116
    target 20
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_117"
      target_id "W10_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 233
    source 77
    target 117
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_35"
      target_id "W10_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 234
    source 117
    target 104
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_76"
      target_id "W10_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
