# generated with VANTED V2.8.2 at Fri Mar 04 09:53:02 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A35222"
      hgnc "NA"
      map_id "W11_3"
      name "TMPRSS2_space_inhibitor"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "a45e2"
      uniprot "NA"
    ]
    graphics [
      x 209.2446040080024
      y 471.60048458475023
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_26"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id27ade87d"
      uniprot "NA"
    ]
    graphics [
      x 113.68347005856151
      y 546.9559542475963
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_5"
      name "SARS_minus_CoV_minus_2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "aae40"
      uniprot "NA"
    ]
    graphics [
      x 97.97947789488387
      y 677.939074471304
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "urn:miriam:ncbigene:4142"
      hgnc "NA"
      map_id "W11_10"
      name "MAS1"
      node_subtype "GENE"
      node_type "species"
      org_id "c93ee"
      uniprot "NA"
    ]
    graphics [
      x 1043.2923946054298
      y 782.2301175393329
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "PUBMED:32125455"
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_27"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id2c3a414b"
      uniprot "NA"
    ]
    graphics [
      x 1021.2186257663109
      y 910.0501125811275
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_4"
      name "Anti_minus_Inflammation_br_Anti_minus_oxidant_br_Anti_minus_fibrosis_br_Vasodilation_br_Anti_minus_atrophy"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "a8dcd"
      uniprot "NA"
    ]
    graphics [
      x 1039.40170304855
      y 1033.631472311854
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "urn:miriam:ncbigene:59272"
      hgnc "NA"
      map_id "W11_8"
      name "ACE2"
      node_subtype "GENE"
      node_type "species"
      org_id "bf1a9"
      uniprot "NA"
    ]
    graphics [
      x 1731.316399480353
      y 758.4435583831245
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_28"
      name "NA"
      node_subtype "UNKNOWN_POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "id2fc925af"
      uniprot "NA"
    ]
    graphics [
      x 1689.1038259450045
      y 883.9528789446356
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_19"
      name "Accumulation_space_of_space__br_angiotensin"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "db780"
      uniprot "NA"
    ]
    graphics [
      x 1580.9010534045763
      y 943.7100991218998
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_6"
      name "Pro_minus_atrophy_br_Pro_minus_Inflammation_br_Vasoconstriction_br_Pro_minus_oxidant_br_Pro_minus_fibrosis"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b0920"
      uniprot "NA"
    ]
    graphics [
      x 562.3872598627831
      y 304.6453405130137
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "PUBMED:32125455"
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_37"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idb0d71735"
      uniprot "NA"
    ]
    graphics [
      x 652.4132441239377
      y 400.4101452713585
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_12"
      name "Tissue_space_injury"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "cfb7f"
      uniprot "NA"
    ]
    graphics [
      x 765.4587160877484
      y 472.2780098295663
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "urn:miriam:ncbigene:185"
      hgnc "NA"
      map_id "W11_24"
      name "AT1R"
      node_subtype "GENE"
      node_type "species"
      org_id "f6bb2"
      uniprot "NA"
    ]
    graphics [
      x 565.9678324468829
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "PUBMED:32125455"
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_35"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id93eee6dc"
      uniprot "NA"
    ]
    graphics [
      x 523.0608929884374
      y 181.1521013963578
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A80128"
      hgnc "NA"
      map_id "W11_1"
      name "Angiotensin_space_1_minus_9"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "a0555"
      uniprot "NA"
    ]
    graphics [
      x 1084.6623742378217
      y 540.9857565267995
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "PUBMED:32125455"
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_30"
      name "NA"
      node_subtype "UNKNOWN_POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "id501a4bdc"
      uniprot "NA"
    ]
    graphics [
      x 1050.4773713798502
      y 654.4094831324157
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "urn:miriam:obo.chebi:2718"
      hgnc "NA"
      map_id "W11_16"
      name "Angiotensin_space_1"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "d391a"
      uniprot "NA"
    ]
    graphics [
      x 686.8702449257316
      y 480.5418354992815
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      annotation "PUBMED:32125455"
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_29"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id45cf6f5e"
      uniprot "NA"
    ]
    graphics [
      x 748.3731120589741
      y 323.8584878984914
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "urn:miriam:ncbigene:1636"
      hgnc "NA"
      map_id "W11_22"
      name "ACE"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e5d6d"
      uniprot "NA"
    ]
    graphics [
      x 860.6696098913504
      y 294.1726764286133
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "urn:miriam:obo.chebi:2719"
      hgnc "NA"
      map_id "W11_14"
      name "Angiotensin_space_2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "d23b7"
      uniprot "NA"
    ]
    graphics [
      x 744.3634261283241
      y 181.84071550871835
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "PUBMED:32125455"
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_25"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id2338925"
      uniprot "NA"
    ]
    graphics [
      x 1104.74015141144
      y 1133.2198030586178
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_17"
      name "Tissue_space_production"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "d3c68"
      uniprot "NA"
    ]
    graphics [
      x 1054.5200796610086
      y 1229.9887917344274
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "PUBMED:32125455"
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_32"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "id7114ee4c"
      uniprot "NA"
    ]
    graphics [
      x 688.8327713442632
      y 65.50511780447721
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A90710"
      hgnc "NA"
      map_id "W11_15"
      name "ACE2_space_surface_space__br_receptor_space_blocker"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "d2e3a"
      uniprot "NA"
    ]
    graphics [
      x 236.41927820623562
      y 907.8133424234077
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_33"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id770baa8e"
      uniprot "NA"
    ]
    graphics [
      x 139.9003670703944
      y 831.3434932982626
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "urn:miriam:ncbigene:183"
      hgnc "NA"
      map_id "W11_23"
      name "AGT"
      node_subtype "GENE"
      node_type "species"
      org_id "f53bd"
      uniprot "NA"
    ]
    graphics [
      x 719.5578973881209
      y 726.3038205594208
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      annotation "PUBMED:32125455"
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_31"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id695320d0"
      uniprot "NA"
    ]
    graphics [
      x 663.7734938080769
      y 618.2880470623884
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "urn:miriam:ncbigene:5972"
      hgnc "NA"
      map_id "W11_9"
      name "REN"
      node_subtype "GENE"
      node_type "species"
      org_id "c91b1"
      uniprot "NA"
    ]
    graphics [
      x 609.1876997098668
      y 724.6554421909007
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "PUBMED:32142651;PUBMED:32125455"
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_38"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idc649fb6a"
      uniprot "NA"
    ]
    graphics [
      x 1610.5162428649655
      y 752.8937213373973
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "urn:miriam:ncbigene:59272"
      hgnc "NA"
      map_id "W11_18"
      name "Soluble_space_ACE2"
      node_subtype "GENE"
      node_type "species"
      org_id "dac5e"
      uniprot "NA"
    ]
    graphics [
      x 1495.6520616726957
      y 752.73647186363
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A90710"
      hgnc "NA"
      map_id "W11_11"
      name "ARBs_space_surface_space__br_receptor_space_blocker"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "cf757"
      uniprot "NA"
    ]
    graphics [
      x 343.3034637972954
      y 625.2284090690422
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_2"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "a0ca7"
      uniprot "NA"
    ]
    graphics [
      x 224.49994593888187
      y 640.8443401906425
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_21"
      name "SARS_minus_CoV_minus_2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e2279"
      uniprot "NA"
    ]
    graphics [
      x 1663.0414565952824
      y 505.72169055816005
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "PUBMED:32142651;PUBMED:32125455"
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_39"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "iddb6a1659"
      uniprot "NA"
    ]
    graphics [
      x 1719.2727414916758
      y 623.9861710298425
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_7"
      name "Spike_space_vaccine"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "bc845"
      uniprot "NA"
    ]
    graphics [
      x 193.31277824985057
      y 771.6676061571306
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_34"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id7806bdcd"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 780.7801697781973
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A55438"
      hgnc "NA"
      map_id "W11_13"
      name "Angiotensin_space_1_minus_7"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "cfdf4"
      uniprot "NA"
    ]
    graphics [
      x 1229.0197536581313
      y 834.5676267755872
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "PUBMED:32125455"
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_36"
      name "NA"
      node_subtype "UNKNOWN_POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "id965b8019"
      uniprot "NA"
    ]
    graphics [
      x 1159.961994177987
      y 750.4125601447454
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_20"
      name "SARS_minus_CoV_minus_2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "de4ce"
      uniprot "NA"
    ]
    graphics [
      x 1456.3765236619383
      y 402.37151869533596
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4883"
      full_annotation "NA"
      hgnc "NA"
      map_id "W11_40"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idf45aaf4a"
      uniprot "NA"
    ]
    graphics [
      x 1578.0100489416077
      y 413.27275608572586
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W11_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 41
    source 1
    target 2
    cd19dm [
      diagram "WP4883"
      edge_type "CONSPUMPTION"
      source_id "W11_3"
      target_id "W11_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 42
    source 2
    target 3
    cd19dm [
      diagram "WP4883"
      edge_type "PRODUCTION"
      source_id "W11_26"
      target_id "W11_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 43
    source 4
    target 5
    cd19dm [
      diagram "WP4883"
      edge_type "CONSPUMPTION"
      source_id "W11_10"
      target_id "W11_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 44
    source 5
    target 6
    cd19dm [
      diagram "WP4883"
      edge_type "PRODUCTION"
      source_id "W11_27"
      target_id "W11_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 45
    source 7
    target 8
    cd19dm [
      diagram "WP4883"
      edge_type "CONSPUMPTION"
      source_id "W11_8"
      target_id "W11_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 46
    source 8
    target 9
    cd19dm [
      diagram "WP4883"
      edge_type "PRODUCTION"
      source_id "W11_28"
      target_id "W11_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 47
    source 10
    target 11
    cd19dm [
      diagram "WP4883"
      edge_type "CONSPUMPTION"
      source_id "W11_6"
      target_id "W11_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 48
    source 11
    target 12
    cd19dm [
      diagram "WP4883"
      edge_type "PRODUCTION"
      source_id "W11_37"
      target_id "W11_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 49
    source 13
    target 14
    cd19dm [
      diagram "WP4883"
      edge_type "CONSPUMPTION"
      source_id "W11_24"
      target_id "W11_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 50
    source 14
    target 10
    cd19dm [
      diagram "WP4883"
      edge_type "PRODUCTION"
      source_id "W11_35"
      target_id "W11_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 51
    source 15
    target 16
    cd19dm [
      diagram "WP4883"
      edge_type "CONSPUMPTION"
      source_id "W11_1"
      target_id "W11_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 52
    source 16
    target 4
    cd19dm [
      diagram "WP4883"
      edge_type "PRODUCTION"
      source_id "W11_30"
      target_id "W11_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 53
    source 17
    target 18
    cd19dm [
      diagram "WP4883"
      edge_type "CONSPUMPTION"
      source_id "W11_16"
      target_id "W11_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 54
    source 19
    target 18
    cd19dm [
      diagram "WP4883"
      edge_type "UNKNOWN_CATALYSIS"
      source_id "W11_22"
      target_id "W11_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 55
    source 18
    target 20
    cd19dm [
      diagram "WP4883"
      edge_type "PRODUCTION"
      source_id "W11_29"
      target_id "W11_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 56
    source 6
    target 21
    cd19dm [
      diagram "WP4883"
      edge_type "CONSPUMPTION"
      source_id "W11_4"
      target_id "W11_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 57
    source 21
    target 22
    cd19dm [
      diagram "WP4883"
      edge_type "PRODUCTION"
      source_id "W11_25"
      target_id "W11_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 58
    source 20
    target 23
    cd19dm [
      diagram "WP4883"
      edge_type "CONSPUMPTION"
      source_id "W11_14"
      target_id "W11_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 59
    source 23
    target 13
    cd19dm [
      diagram "WP4883"
      edge_type "PRODUCTION"
      source_id "W11_32"
      target_id "W11_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 60
    source 24
    target 25
    cd19dm [
      diagram "WP4883"
      edge_type "CONSPUMPTION"
      source_id "W11_15"
      target_id "W11_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 61
    source 25
    target 3
    cd19dm [
      diagram "WP4883"
      edge_type "PRODUCTION"
      source_id "W11_33"
      target_id "W11_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 62
    source 26
    target 27
    cd19dm [
      diagram "WP4883"
      edge_type "CONSPUMPTION"
      source_id "W11_23"
      target_id "W11_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 63
    source 28
    target 27
    cd19dm [
      diagram "WP4883"
      edge_type "UNKNOWN_CATALYSIS"
      source_id "W11_9"
      target_id "W11_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 64
    source 27
    target 17
    cd19dm [
      diagram "WP4883"
      edge_type "PRODUCTION"
      source_id "W11_31"
      target_id "W11_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 65
    source 7
    target 29
    cd19dm [
      diagram "WP4883"
      edge_type "CONSPUMPTION"
      source_id "W11_8"
      target_id "W11_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 66
    source 29
    target 30
    cd19dm [
      diagram "WP4883"
      edge_type "PRODUCTION"
      source_id "W11_38"
      target_id "W11_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 67
    source 31
    target 32
    cd19dm [
      diagram "WP4883"
      edge_type "CONSPUMPTION"
      source_id "W11_11"
      target_id "W11_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 68
    source 32
    target 3
    cd19dm [
      diagram "WP4883"
      edge_type "PRODUCTION"
      source_id "W11_2"
      target_id "W11_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 69
    source 33
    target 34
    cd19dm [
      diagram "WP4883"
      edge_type "CONSPUMPTION"
      source_id "W11_21"
      target_id "W11_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 70
    source 34
    target 7
    cd19dm [
      diagram "WP4883"
      edge_type "PRODUCTION"
      source_id "W11_39"
      target_id "W11_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 71
    source 35
    target 36
    cd19dm [
      diagram "WP4883"
      edge_type "CONSPUMPTION"
      source_id "W11_7"
      target_id "W11_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 72
    source 36
    target 3
    cd19dm [
      diagram "WP4883"
      edge_type "PRODUCTION"
      source_id "W11_34"
      target_id "W11_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 73
    source 37
    target 38
    cd19dm [
      diagram "WP4883"
      edge_type "CONSPUMPTION"
      source_id "W11_13"
      target_id "W11_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 74
    source 38
    target 4
    cd19dm [
      diagram "WP4883"
      edge_type "PRODUCTION"
      source_id "W11_36"
      target_id "W11_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 75
    source 39
    target 40
    cd19dm [
      diagram "WP4883"
      edge_type "CONSPUMPTION"
      source_id "W11_20"
      target_id "W11_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 76
    source 40
    target 33
    cd19dm [
      diagram "WP4883"
      edge_type "PRODUCTION"
      source_id "W11_40"
      target_id "W11_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
