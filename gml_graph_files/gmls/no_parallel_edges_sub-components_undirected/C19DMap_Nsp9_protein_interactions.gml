# generated with VANTED V2.8.2 at Fri Mar 04 09:53:02 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:doi:10.1101/2020.03.31.019216;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-;urn:miriam:uniprot:P0C6X7;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-;urn:miriam:uniprot:P0C6X7;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:hgnc.symbol:NEMF;urn:miriam:hgnc.symbol:NEMF;urn:miriam:ncbigene:9147;urn:miriam:ncbigene:9147;urn:miriam:ensembl:ENSG00000165525;urn:miriam:hgnc:10663;urn:miriam:uniprot:O60524;urn:miriam:refseq:NM_004713"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:NEMF"
      map_id "M115_101"
      name "pathogen"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa85"
      uniprot "UNIPROT:P0C6X7;UNIPROT:P0DTD1;UNIPROT:O60524"
    ]
    graphics [
      x 1259.793797618529
      y 1609.4806802087148
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_193"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10172"
      uniprot "NA"
    ]
    graphics [
      x 1103.2267583302146
      y 1664.7632638685823
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_193"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_314"
      name "Neutrophil_underscore_activation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa1278"
      uniprot "NA"
    ]
    graphics [
      x 998.131306568885
      y 1595.8654174533285
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_314"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:10428810;urn:miriam:ncbigene:2963;urn:miriam:refseq:NM_004128;urn:miriam:ncbigene:2963;urn:miriam:hgnc:4653;urn:miriam:uniprot:P13984;urn:miriam:ec-code:3.6.4.12;urn:miriam:hgnc.symbol:GTF2F2;urn:miriam:hgnc.symbol:GTF2F2;urn:miriam:ensembl:ENSG00000188342"
      hgnc "HGNC_SYMBOL:GTF2F2"
      map_id "M115_382"
      name "GTF2F2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa981"
      uniprot "UNIPROT:P13984"
    ]
    graphics [
      x 789.5057852720568
      y 1077.3203046142055
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_382"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "PUBMED:3860504;PUBMED:16878124;PUBMED:26496610;PUBMED:8662660;PUBMED:26344197;PUBMED:17643375;PUBMED:11278533"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_119"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10082"
      uniprot "NA"
    ]
    graphics [
      x 639.8923752999049
      y 822.5488550564102
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:2.3.1.48;urn:miriam:ncbigene:2959;urn:miriam:ncbigene:2959;urn:miriam:hgnc:4648;urn:miriam:uniprot:Q00403;urn:miriam:hgnc.symbol:GTF2B;urn:miriam:refseq:NM_001514;urn:miriam:hgnc.symbol:GTF2B;urn:miriam:ensembl:ENSG00000137947"
      hgnc "HGNC_SYMBOL:GTF2B"
      map_id "M115_249"
      name "GTF2B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1058"
      uniprot "UNIPROT:Q00403"
    ]
    graphics [
      x 755.0358184376043
      y 800.9964103070906
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_249"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:refseq:NM_000937;urn:miriam:ec-code:2.7.7.48;urn:miriam:uniprot:P30876;urn:miriam:ncbigene:5430;urn:miriam:ncbigene:5430;urn:miriam:ncbigene:5431;urn:miriam:hgnc.symbol:POLR2B;urn:miriam:hgnc:9187;urn:miriam:uniprot:P24928;urn:miriam:uniprot:P24928;urn:miriam:hgnc.symbol:POLR2A;urn:miriam:hgnc.symbol:POLR2A;urn:miriam:ensembl:ENSG00000181222;urn:miriam:ec-code:2.7.7.6"
      hgnc "HGNC_SYMBOL:POLR2B;HGNC_SYMBOL:POLR2A"
      map_id "M115_237"
      name "POLR2A"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1000"
      uniprot "UNIPROT:P30876;UNIPROT:P24928"
    ]
    graphics [
      x 529.2512155283272
      y 795.431201463926
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_237"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000047315;urn:miriam:hgnc.symbol:POLR2B;urn:miriam:refseq:NM_000938;urn:miriam:hgnc.symbol:POLR2B;urn:miriam:uniprot:P30876;urn:miriam:ncbigene:5431;urn:miriam:ncbigene:5431;urn:miriam:ec-code:2.7.7.6;urn:miriam:hgnc:9188"
      hgnc "HGNC_SYMBOL:POLR2B"
      map_id "M115_246"
      name "POLR2B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1055"
      uniprot "UNIPROT:P30876"
    ]
    graphics [
      x 602.1246130645889
      y 923.1999984725798
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_246"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:refseq:NM_002695;urn:miriam:uniprot:P19388;urn:miriam:ensembl:ENSG00000099817;urn:miriam:hgnc.symbol:POLR2E;urn:miriam:hgnc.symbol:POLR2E;urn:miriam:hgnc:9192;urn:miriam:ncbigene:5434;urn:miriam:ncbigene:5434"
      hgnc "HGNC_SYMBOL:POLR2E"
      map_id "M115_247"
      name "POLR2E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1056"
      uniprot "UNIPROT:P19388"
    ]
    graphics [
      x 694.655036122167
      y 653.8875799784629
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_247"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:refseq:NM_002696;urn:miriam:hgnc.symbol:POLR2G;urn:miriam:hgnc.symbol:POLR2G;urn:miriam:hgnc:9194;urn:miriam:ncbigene:5436;urn:miriam:ncbigene:5436;urn:miriam:uniprot:P62487;urn:miriam:ensembl:ENSG00000168002"
      hgnc "HGNC_SYMBOL:POLR2G"
      map_id "M115_248"
      name "POLR2G"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1057"
      uniprot "UNIPROT:P62487"
    ]
    graphics [
      x 751.6509434618965
      y 731.9100225714067
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_248"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:26344197;urn:miriam:refseq:NM_002695;urn:miriam:uniprot:P19388;urn:miriam:ensembl:ENSG00000099817;urn:miriam:hgnc.symbol:POLR2E;urn:miriam:hgnc.symbol:POLR2E;urn:miriam:hgnc:9192;urn:miriam:ncbigene:5434;urn:miriam:ncbigene:5434;urn:miriam:refseq:NM_002696;urn:miriam:hgnc.symbol:POLR2G;urn:miriam:hgnc.symbol:POLR2G;urn:miriam:hgnc:9194;urn:miriam:ncbigene:5436;urn:miriam:ncbigene:5436;urn:miriam:uniprot:P62487;urn:miriam:ensembl:ENSG00000168002;urn:miriam:ec-code:2.3.1.48;urn:miriam:ncbigene:2959;urn:miriam:ncbigene:2959;urn:miriam:hgnc:4648;urn:miriam:uniprot:Q00403;urn:miriam:hgnc.symbol:GTF2B;urn:miriam:refseq:NM_001514;urn:miriam:hgnc.symbol:GTF2B;urn:miriam:ensembl:ENSG00000137947;urn:miriam:pubmed:10428810;urn:miriam:ncbigene:2963;urn:miriam:refseq:NM_004128;urn:miriam:ncbigene:2963;urn:miriam:hgnc:4653;urn:miriam:uniprot:P13984;urn:miriam:ec-code:3.6.4.12;urn:miriam:hgnc.symbol:GTF2F2;urn:miriam:hgnc.symbol:GTF2F2;urn:miriam:ensembl:ENSG00000188342;urn:miriam:ensembl:ENSG00000047315;urn:miriam:hgnc.symbol:POLR2B;urn:miriam:refseq:NM_000938;urn:miriam:hgnc.symbol:POLR2B;urn:miriam:uniprot:P30876;urn:miriam:ncbigene:5431;urn:miriam:ncbigene:5431;urn:miriam:ec-code:2.7.7.6;urn:miriam:hgnc:9188;urn:miriam:refseq:NM_000937;urn:miriam:ec-code:2.7.7.48;urn:miriam:uniprot:P30876;urn:miriam:ncbigene:5430;urn:miriam:ncbigene:5430;urn:miriam:ncbigene:5431;urn:miriam:hgnc.symbol:POLR2B;urn:miriam:hgnc:9187;urn:miriam:uniprot:P24928;urn:miriam:uniprot:P24928;urn:miriam:hgnc.symbol:POLR2A;urn:miriam:hgnc.symbol:POLR2A;urn:miriam:ensembl:ENSG00000181222;urn:miriam:ec-code:2.7.7.6"
      hgnc "HGNC_SYMBOL:POLR2E;HGNC_SYMBOL:POLR2G;HGNC_SYMBOL:GTF2B;HGNC_SYMBOL:GTF2F2;HGNC_SYMBOL:POLR2B;HGNC_SYMBOL:POLR2A"
      map_id "M115_34"
      name "gtfrnapoly"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa13"
      uniprot "UNIPROT:P19388;UNIPROT:P62487;UNIPROT:Q00403;UNIPROT:P13984;UNIPROT:P30876;UNIPROT:P24928"
    ]
    graphics [
      x 623.1659351638534
      y 658.9600571457336
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M115_364"
      name "Nsp9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1423"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1586.2385552263024
      y 1103.2227751275489
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_364"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_141"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10117"
      uniprot "NA"
    ]
    graphics [
      x 1538.9742582851645
      y 1192.6454378909407
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_141"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32296183;urn:miriam:pubmed:9049309"
      hgnc "NA"
      map_id "M115_2"
      name "Nuclear_space_Pore"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa10"
      uniprot "NA"
    ]
    graphics [
      x 1588.0051163660337
      y 1430.6505101241044
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:pubmed:9049309;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M115_40"
      name "Nuclear_space_Pore_space_comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa21"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1540.2570232043695
      y 1052.31699280058
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:FBLN5;urn:miriam:refseq:NM_001384158;urn:miriam:hgnc.symbol:FBLN5;urn:miriam:hgnc:3602;urn:miriam:ensembl:ENSG00000140092;urn:miriam:uniprot:Q9UBX5;urn:miriam:uniprot:Q9UBX5;urn:miriam:ncbigene:10516;urn:miriam:ncbigene:10516"
      hgnc "HGNC_SYMBOL:FBLN5"
      map_id "M115_270"
      name "FBLN5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1138"
      uniprot "UNIPROT:Q9UBX5"
    ]
    graphics [
      x 1308.940654283569
      y 384.3976726992664
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_270"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      annotation "PUBMED:21001709;PUBMED:14745449"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_126"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10102"
      uniprot "NA"
    ]
    graphics [
      x 1191.2126937258497
      y 340.3356933133274
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_126"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:6665;urn:miriam:refseq:NM_005576;urn:miriam:ensembl:ENSG00000129038;urn:miriam:uniprot:Q08397;urn:miriam:hgnc.symbol:LOXL1;urn:miriam:hgnc.symbol:LOXL1;urn:miriam:ec-code:1.4.3.-;urn:miriam:ncbigene:4016;urn:miriam:ncbigene:4016"
      hgnc "HGNC_SYMBOL:LOXL1"
      map_id "M115_267"
      name "LOXL1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1127"
      uniprot "UNIPROT:Q08397"
    ]
    graphics [
      x 1327.738378770222
      y 243.38590186030171
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_267"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:PLG;urn:miriam:hgnc.symbol:PLG;urn:miriam:ec-code:3.4.21.7;urn:miriam:ensembl:ENSG00000122194;urn:miriam:ncbigene:5340;urn:miriam:ncbigene:5340;urn:miriam:hgnc:9071;urn:miriam:refseq:NM_000301;urn:miriam:uniprot:P00747;urn:miriam:uniprot:P00747"
      hgnc "HGNC_SYMBOL:PLG"
      map_id "M115_373"
      name "PLG"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1433"
      uniprot "UNIPROT:P00747"
    ]
    graphics [
      x 1193.5023202278314
      y 735.5868358143023
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_373"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:14745449;urn:miriam:hgnc.symbol:FBLN5;urn:miriam:refseq:NM_001384158;urn:miriam:hgnc.symbol:FBLN5;urn:miriam:hgnc:3602;urn:miriam:ensembl:ENSG00000140092;urn:miriam:uniprot:Q9UBX5;urn:miriam:uniprot:Q9UBX5;urn:miriam:ncbigene:10516;urn:miriam:ncbigene:10516;urn:miriam:hgnc:6665;urn:miriam:refseq:NM_005576;urn:miriam:ensembl:ENSG00000129038;urn:miriam:uniprot:Q08397;urn:miriam:hgnc.symbol:LOXL1;urn:miriam:hgnc.symbol:LOXL1;urn:miriam:ec-code:1.4.3.-;urn:miriam:ncbigene:4016;urn:miriam:ncbigene:4016"
      hgnc "HGNC_SYMBOL:FBLN5;HGNC_SYMBOL:LOXL1"
      map_id "M115_63"
      name "LOXcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa48"
      uniprot "UNIPROT:Q9UBX5;UNIPROT:Q08397"
    ]
    graphics [
      x 1250.4227457063948
      y 187.78373579704328
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000073756;urn:miriam:hgnc:9605;urn:miriam:hgnc.symbol:PTGS2;urn:miriam:hgnc.symbol:PTGS2;urn:miriam:uniprot:P35354;urn:miriam:uniprot:P35354;urn:miriam:refseq:NM_000963;urn:miriam:ncbigene:5743;urn:miriam:ncbigene:5743;urn:miriam:ec-code:1.14.99.1"
      hgnc "HGNC_SYMBOL:PTGS2"
      map_id "M115_274"
      name "PTGS2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1153"
      uniprot "UNIPROT:P35354"
    ]
    graphics [
      x 401.5479375958796
      y 159.14736515853247
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_274"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      annotation "PUBMED:20724158;PUBMED:10555907;PUBMED:10693877;PUBMED:10643177;PUBMED:11398914;PUBMED:20166930;PUBMED:10580458;PUBMED:11752352;PUBMED:10566562"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_154"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10131"
      uniprot "NA"
    ]
    graphics [
      x 346.80352930878166
      y 260.58009885789545
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_154"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:5090"
      hgnc "NA"
      map_id "M115_275"
      name "Rofecoxib"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1154"
      uniprot "NA"
    ]
    graphics [
      x 233.7797053001218
      y 226.49274409833026
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_275"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:20724158;urn:miriam:ensembl:ENSG00000073756;urn:miriam:hgnc:9605;urn:miriam:hgnc.symbol:PTGS2;urn:miriam:hgnc.symbol:PTGS2;urn:miriam:uniprot:P35354;urn:miriam:uniprot:P35354;urn:miriam:refseq:NM_000963;urn:miriam:ncbigene:5743;urn:miriam:ncbigene:5743;urn:miriam:ec-code:1.14.99.1;urn:miriam:pubchem.compound:5090"
      hgnc "HGNC_SYMBOL:PTGS2"
      map_id "M115_69"
      name "PTGScomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa54"
      uniprot "UNIPROT:P35354"
    ]
    graphics [
      x 567.7176885891931
      y 419.71366792348556
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:23589;urn:miriam:hgnc.symbol:ZNF503;urn:miriam:hgnc.symbol:ZNF503;urn:miriam:uniprot:Q96F45;urn:miriam:ensembl:ENSG00000165655;urn:miriam:refseq:NM_032772;urn:miriam:ncbigene:84858;urn:miriam:ncbigene:84858"
      hgnc "HGNC_SYMBOL:ZNF503"
      map_id "M115_377"
      name "ZNF503"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa964"
      uniprot "UNIPROT:Q96F45"
    ]
    graphics [
      x 1524.2211277737547
      y 400.19534203539
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_377"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      annotation "PUBMED:27705803"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_142"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10118"
      uniprot "NA"
    ]
    graphics [
      x 1827.4628472008142
      y 351.69301962360953
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_142"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:P61962;urn:miriam:pubmed:16887337;urn:miriam:ensembl:ENSG00000136485;urn:miriam:hgnc.symbol:DCAF7;urn:miriam:hgnc.symbol:DCAF7;urn:miriam:ncbigene:10238;urn:miriam:ncbigene:10238;urn:miriam:refseq:NM_005828;urn:miriam:hgnc:30915;urn:miriam:pubmed:16949367"
      hgnc "HGNC_SYMBOL:DCAF7"
      map_id "M115_383"
      name "DCAF7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa983"
      uniprot "UNIPROT:P61962"
    ]
    graphics [
      x 1879.6647339993947
      y 539.0686111371755
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_383"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:27705803;urn:miriam:hgnc:23589;urn:miriam:hgnc.symbol:ZNF503;urn:miriam:hgnc.symbol:ZNF503;urn:miriam:uniprot:Q96F45;urn:miriam:ensembl:ENSG00000165655;urn:miriam:refseq:NM_032772;urn:miriam:ncbigene:84858;urn:miriam:ncbigene:84858;urn:miriam:uniprot:P61962;urn:miriam:pubmed:16887337;urn:miriam:ensembl:ENSG00000136485;urn:miriam:hgnc.symbol:DCAF7;urn:miriam:hgnc.symbol:DCAF7;urn:miriam:ncbigene:10238;urn:miriam:ncbigene:10238;urn:miriam:refseq:NM_005828;urn:miriam:hgnc:30915;urn:miriam:pubmed:16949367"
      hgnc "HGNC_SYMBOL:ZNF503;HGNC_SYMBOL:DCAF7"
      map_id "M115_46"
      name "dcafznf"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa29"
      uniprot "UNIPROT:Q96F45;UNIPROT:P61962"
    ]
    graphics [
      x 1969.010569504811
      y 310.24599893884454
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:17678888;urn:miriam:hgnc:3176;urn:miriam:refseq:NM_001955;urn:miriam:uniprot:P05305;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:ensembl:ENSG00000078401"
      hgnc "HGNC_SYMBOL:EDN1"
      map_id "M115_53"
      name "EDN1_minus_homo"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa38"
      uniprot "UNIPROT:P05305"
    ]
    graphics [
      x 861.5218118235456
      y 769.4070907433404
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      annotation "PUBMED:7509919"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_132"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10108"
      uniprot "NA"
    ]
    graphics [
      x 939.1163616241562
      y 499.300346648094
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:refseq:NM_001166055;urn:miriam:hgnc:3179;urn:miriam:uniprot:P25101;urn:miriam:ensembl:ENSG00000151617;urn:miriam:ncbigene:1909;urn:miriam:ncbigene:1909;urn:miriam:hgnc.symbol:EDNRA;urn:miriam:hgnc.symbol:EDNRA"
      hgnc "HGNC_SYMBOL:EDNRA"
      map_id "M115_264"
      name "EDNRA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1116"
      uniprot "UNIPROT:P25101"
    ]
    graphics [
      x 953.8207020039924
      y 572.1969817516976
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_264"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:7509919;urn:miriam:refseq:NM_001166055;urn:miriam:hgnc:3179;urn:miriam:uniprot:P25101;urn:miriam:ensembl:ENSG00000151617;urn:miriam:ncbigene:1909;urn:miriam:ncbigene:1909;urn:miriam:hgnc.symbol:EDNRA;urn:miriam:hgnc.symbol:EDNRA;urn:miriam:hgnc:3176;urn:miriam:refseq:NM_001955;urn:miriam:uniprot:P05305;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:ensembl:ENSG00000078401"
      hgnc "HGNC_SYMBOL:EDNRA;HGNC_SYMBOL:EDN1"
      map_id "M115_60"
      name "EDN1_minus_homo"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa45"
      uniprot "UNIPROT:P25101;UNIPROT:P05305"
    ]
    graphics [
      x 982.1537029289073
      y 310.2506540236552
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M115_385"
      name "Nsp8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa997"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 828.7654149054727
      y 692.6518066046772
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_385"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_211"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10190"
      uniprot "NA"
    ]
    graphics [
      x 659.9494007941223
      y 504.06832788839966
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_211"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:2.9.1.2;urn:miriam:hgnc:30605;urn:miriam:uniprot:Q9HD40;urn:miriam:hgnc.symbol:SEPSECS;urn:miriam:ncbigene:51091;urn:miriam:refseq:NM_016955;urn:miriam:ensembl:ENSG00000109618;urn:miriam:hgnc.symbol:SEPSECS;urn:miriam:ncbigene:51091"
      hgnc "HGNC_SYMBOL:SEPSECS"
      map_id "M115_332"
      name "SEPSECS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1337"
      uniprot "UNIPROT:Q9HD40"
    ]
    graphics [
      x 407.6600095431155
      y 536.1697006102744
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_332"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:2.9.1.2;urn:miriam:hgnc:30605;urn:miriam:uniprot:Q9HD40;urn:miriam:hgnc.symbol:SEPSECS;urn:miriam:ncbigene:51091;urn:miriam:refseq:NM_016955;urn:miriam:ensembl:ENSG00000109618;urn:miriam:hgnc.symbol:SEPSECS;urn:miriam:ncbigene:51091;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:SEPSECS;HGNC_SYMBOL:rep"
      map_id "M115_11"
      name "SEPSECScomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa108"
      uniprot "UNIPROT:Q9HD40;UNIPROT:P0DTD1"
    ]
    graphics [
      x 767.2436342204708
      y 335.82791959477527
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:26274;urn:miriam:uniprot:Q96I59;urn:miriam:hgnc.symbol:NARS2;urn:miriam:ensembl:ENSG00000137513;urn:miriam:hgnc.symbol:NARS2;urn:miriam:ncbigene:79731;urn:miriam:ncbigene:79731;urn:miriam:refseq:NM_024678;urn:miriam:ec-code:6.1.1.22"
      hgnc "HGNC_SYMBOL:NARS2"
      map_id "M115_336"
      name "NARS2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1349"
      uniprot "UNIPROT:Q96I59"
    ]
    graphics [
      x 523.5292875427316
      y 1240.7235815343586
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_336"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "PUBMED:12874385;PUBMED:16753178"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_216"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10195"
      uniprot "NA"
    ]
    graphics [
      x 631.4012072312446
      y 1429.0828354042296
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_216"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:6267;urn:miriam:obo.chebi:CHEBI%3A17196"
      hgnc "NA"
      map_id "M115_337"
      name "L_minus_Asparagine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa1353"
      uniprot "NA"
    ]
    graphics [
      x 769.2891245784999
      y 1420.497006430539
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_337"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:16753178;urn:miriam:hgnc:26274;urn:miriam:uniprot:Q96I59;urn:miriam:hgnc.symbol:NARS2;urn:miriam:ensembl:ENSG00000137513;urn:miriam:hgnc.symbol:NARS2;urn:miriam:ncbigene:79731;urn:miriam:ncbigene:79731;urn:miriam:refseq:NM_024678;urn:miriam:ec-code:6.1.1.22;urn:miriam:pubchem.compound:6267;urn:miriam:obo.chebi:CHEBI%3A17196"
      hgnc "HGNC_SYMBOL:NARS2"
      map_id "M115_16"
      name "NLcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa113"
      uniprot "UNIPROT:Q96I59"
    ]
    graphics [
      x 686.4043500584386
      y 1557.2386675436783
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M115_1"
      name "Nsp9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a7c94"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1329.4606733983103
      y 1106.9118727767504
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_149"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10125"
      uniprot "NA"
    ]
    graphics [
      x 1337.164050813882
      y 671.7066252479198
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_149"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000166147;urn:miriam:uniprot:P35555;urn:miriam:uniprot:P35555;urn:miriam:hgnc.symbol:FBN1;urn:miriam:hgnc.symbol:FBN1;urn:miriam:hgnc:3603;urn:miriam:ncbigene:2200;urn:miriam:ncbigene:2200;urn:miriam:refseq:NM_000138"
      hgnc "HGNC_SYMBOL:FBN1"
      map_id "M115_378"
      name "FBN1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa965"
      uniprot "UNIPROT:P35555"
    ]
    graphics [
      x 1267.8437797932547
      y 532.9448682094071
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_378"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:hgnc.symbol:FBLN5;urn:miriam:refseq:NM_001384158;urn:miriam:hgnc.symbol:FBLN5;urn:miriam:hgnc:3602;urn:miriam:ensembl:ENSG00000140092;urn:miriam:uniprot:Q9UBX5;urn:miriam:uniprot:Q9UBX5;urn:miriam:ncbigene:10516;urn:miriam:ncbigene:10516;urn:miriam:ensembl:ENSG00000166147;urn:miriam:uniprot:P35555;urn:miriam:uniprot:P35555;urn:miriam:hgnc.symbol:FBN1;urn:miriam:hgnc.symbol:FBN1;urn:miriam:hgnc:3603;urn:miriam:ncbigene:2200;urn:miriam:ncbigene:2200;urn:miriam:refseq:NM_000138;urn:miriam:hgnc.symbol:FBLN2;urn:miriam:hgnc.symbol:FBLN2;urn:miriam:uniprot:P98095;urn:miriam:uniprot:P98095;urn:miriam:refseq:NM_001004019;urn:miriam:ensembl:ENSG00000163520;urn:miriam:hgnc:3601;urn:miriam:ncbigene:2199;urn:miriam:ncbigene:2199;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:FBLN5;HGNC_SYMBOL:FBN1;HGNC_SYMBOL:FBLN2;HGNC_SYMBOL:rep"
      map_id "M115_38"
      name "Fibrillincomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa17"
      uniprot "UNIPROT:Q9UBX5;UNIPROT:P35555;UNIPROT:P98095;UNIPROT:P0DTD1"
    ]
    graphics [
      x 1340.523826969935
      y 513.9684110187402
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_217"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10196"
      uniprot "NA"
    ]
    graphics [
      x 1276.0604808316255
      y 627.9723234745352
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_217"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbigene:25983;urn:miriam:ncbigene:25983;urn:miriam:uniprot:Q8NEJ9;urn:miriam:hgnc.symbol:NGDN;urn:miriam:hgnc.symbol:NGDN;urn:miriam:refseq:NM_001042635;urn:miriam:hgnc:20271;urn:miriam:ensembl:ENSG00000129460"
      hgnc "HGNC_SYMBOL:NGDN"
      map_id "M115_339"
      name "NGDN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1358"
      uniprot "UNIPROT:Q8NEJ9"
    ]
    graphics [
      x 1464.2724381234475
      y 579.3568100182065
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_339"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ncbigene:25983;urn:miriam:ncbigene:25983;urn:miriam:uniprot:Q8NEJ9;urn:miriam:hgnc.symbol:NGDN;urn:miriam:hgnc.symbol:NGDN;urn:miriam:refseq:NM_001042635;urn:miriam:hgnc:20271;urn:miriam:ensembl:ENSG00000129460"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:NGDN"
      map_id "M115_17"
      name "NGDNcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa114"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q8NEJ9"
    ]
    graphics [
      x 1420.0160939013742
      y 513.2837147418202
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:2.3.2.27;urn:miriam:uniprot:Q86YT6;urn:miriam:pubmed:24185901;urn:miriam:ncbigene:57534;urn:miriam:ncbigene:57534;urn:miriam:refseq:NM_020774;urn:miriam:hgnc.symbol:MIB1;urn:miriam:ensembl:ENSG00000101752;urn:miriam:hgnc.symbol:MIB1;urn:miriam:hgnc:21086"
      hgnc "HGNC_SYMBOL:MIB1"
      map_id "M115_380"
      name "MIB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa977"
      uniprot "UNIPROT:Q86YT6"
    ]
    graphics [
      x 2081.2937406391693
      y 486.4037153743461
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_380"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "PUBMED:21985982"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_128"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10104"
      uniprot "NA"
    ]
    graphics [
      x 1921.2251163674619
      y 278.6767798110583
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_128"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbigene:28514;urn:miriam:ensembl:ENSG00000198719;urn:miriam:ncbigene:28514;urn:miriam:hgnc:2908;urn:miriam:uniprot:O00548;urn:miriam:refseq:NM_005618;urn:miriam:hgnc.symbol:DLL1;urn:miriam:hgnc.symbol:DLL1"
      hgnc "HGNC_SYMBOL:DLL1"
      map_id "M115_269"
      name "DLL1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1135"
      uniprot "UNIPROT:O00548"
    ]
    graphics [
      x 1698.8931977309035
      y 365.18262105244537
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_269"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:21985982;urn:miriam:ncbigene:28514;urn:miriam:ensembl:ENSG00000198719;urn:miriam:ncbigene:28514;urn:miriam:hgnc:2908;urn:miriam:uniprot:O00548;urn:miriam:refseq:NM_005618;urn:miriam:hgnc.symbol:DLL1;urn:miriam:hgnc.symbol:DLL1;urn:miriam:ec-code:2.3.2.27;urn:miriam:uniprot:Q86YT6;urn:miriam:pubmed:24185901;urn:miriam:ncbigene:57534;urn:miriam:ncbigene:57534;urn:miriam:refseq:NM_020774;urn:miriam:hgnc.symbol:MIB1;urn:miriam:ensembl:ENSG00000101752;urn:miriam:hgnc.symbol:MIB1;urn:miriam:hgnc:21086"
      hgnc "HGNC_SYMBOL:DLL1;HGNC_SYMBOL:MIB1"
      map_id "M115_65"
      name "MIBcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa50"
      uniprot "UNIPROT:O00548;UNIPROT:Q86YT6"
    ]
    graphics [
      x 1928.5160123123353
      y 145.7237634096516
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:3176;urn:miriam:refseq:NM_001955;urn:miriam:ncbigene:1906;urn:miriam:hgnc.symbol:EDN1;urn:miriam:uniprot:P05305;urn:miriam:ensembl:ENSG00000078401"
      hgnc "HGNC_SYMBOL:EDN1"
      map_id "M115_240"
      name "EDN1"
      node_subtype "GENE"
      node_type "species"
      org_id "sa1007"
      uniprot "UNIPROT:P05305"
    ]
    graphics [
      x 2075.765198506215
      y 300.05271203953464
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_240"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      annotation "PUBMED:27880803"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_116"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10061"
      uniprot "NA"
    ]
    graphics [
      x 2097.302734543966
      y 411.6461159143523
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:3176;urn:miriam:refseq:NM_001955;urn:miriam:ncbigene:1906;urn:miriam:hgnc.symbol:EDN1;urn:miriam:uniprot:P05305;urn:miriam:ensembl:ENSG00000078401"
      hgnc "HGNC_SYMBOL:EDN1"
      map_id "M115_239"
      name "EDN1"
      node_subtype "RNA"
      node_type "species"
      org_id "sa1006"
      uniprot "UNIPROT:P05305"
    ]
    graphics [
      x 2141.9091189534415
      y 550.4459850501826
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_239"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbigene:23111;urn:miriam:ncbigene:23111;urn:miriam:hgnc.symbol:SPART;urn:miriam:hgnc.symbol:SPART;urn:miriam:ensembl:ENSG00000133104;urn:miriam:hgnc:18514;urn:miriam:refseq:NM_001142294;urn:miriam:uniprot:Q8N0X7"
      hgnc "HGNC_SYMBOL:SPART"
      map_id "M115_379"
      name "SPART"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa970"
      uniprot "UNIPROT:Q8N0X7"
    ]
    graphics [
      x 1801.20057465098
      y 1699.711627502646
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_379"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      annotation "PUBMED:19765186"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_130"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10106"
      uniprot "NA"
    ]
    graphics [
      x 1587.21951814385
      y 1766.263924248253
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_130"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:AIFM1;urn:miriam:hgnc.symbol:AIFM1;urn:miriam:hgnc:8768;urn:miriam:ncbigene:9131;urn:miriam:ncbigene:9131;urn:miriam:ec-code:1.6.99.-;urn:miriam:uniprot:O95831;urn:miriam:ensembl:ENSG00000156709;urn:miriam:refseq:NM_001130846"
      hgnc "HGNC_SYMBOL:AIFM1"
      map_id "M115_253"
      name "AIFM1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1071"
      uniprot "UNIPROT:O95831"
    ]
    graphics [
      x 1416.3568383747202
      y 1551.9842971656476
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_253"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:19765186;urn:miriam:ncbigene:23111;urn:miriam:ncbigene:23111;urn:miriam:hgnc.symbol:SPART;urn:miriam:hgnc.symbol:SPART;urn:miriam:ensembl:ENSG00000133104;urn:miriam:hgnc:18514;urn:miriam:refseq:NM_001142294;urn:miriam:uniprot:Q8N0X7;urn:miriam:hgnc.symbol:AIFM1;urn:miriam:hgnc.symbol:AIFM1;urn:miriam:hgnc:8768;urn:miriam:ncbigene:9131;urn:miriam:ncbigene:9131;urn:miriam:ec-code:1.6.99.-;urn:miriam:uniprot:O95831;urn:miriam:ensembl:ENSG00000156709;urn:miriam:refseq:NM_001130846"
      hgnc "HGNC_SYMBOL:SPART;HGNC_SYMBOL:AIFM1"
      map_id "M115_61"
      name "SPARTcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa46"
      uniprot "UNIPROT:Q8N0X7;UNIPROT:O95831"
    ]
    graphics [
      x 1601.4231584937675
      y 1912.0735778064786
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M115_362"
      name "Nsp7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1421"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 2051.6512363389984
      y 1275.1493372712162
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_362"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_214"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10193"
      uniprot "NA"
    ]
    graphics [
      x 2076.838973614324
      y 1495.6143259934142
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_214"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000174780;urn:miriam:hgnc:11303;urn:miriam:refseq:NM_001267722;urn:miriam:hgnc.symbol:SRP72;urn:miriam:ncbigene:6731;urn:miriam:hgnc.symbol:SRP72;urn:miriam:ncbigene:6731;urn:miriam:uniprot:O76094"
      hgnc "HGNC_SYMBOL:SRP72"
      map_id "M115_335"
      name "SRP72"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1346"
      uniprot "UNIPROT:O76094"
    ]
    graphics [
      x 2060.9598478174667
      y 1414.2864833055094
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_335"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ensembl:ENSG00000174780;urn:miriam:hgnc:11303;urn:miriam:refseq:NM_001267722;urn:miriam:hgnc.symbol:SRP72;urn:miriam:ncbigene:6731;urn:miriam:hgnc.symbol:SRP72;urn:miriam:ncbigene:6731;urn:miriam:uniprot:O76094;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:SRP72;HGNC_SYMBOL:rep"
      map_id "M115_14"
      name "SRP72comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa111"
      uniprot "UNIPROT:O76094;UNIPROT:P0DTD1"
    ]
    graphics [
      x 1895.7765317560961
      y 1519.4725684760256
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000166200;urn:miriam:refseq:NM_004236;urn:miriam:ncbigene:9318;urn:miriam:ncbigene:9318;urn:miriam:hgnc:30747;urn:miriam:uniprot:P61201;urn:miriam:hgnc.symbol:COPS2;urn:miriam:hgnc.symbol:COPS2"
      hgnc "HGNC_SYMBOL:COPS2"
      map_id "M115_261"
      name "COPS2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1090"
      uniprot "UNIPROT:P61201"
    ]
    graphics [
      x 556.5187425360571
      y 135.62594797976726
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_261"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      annotation "PUBMED:10903862;PUBMED:26186194;PUBMED:23408908;PUBMED:11967155;PUBMED:26344197;PUBMED:27173435;PUBMED:16045761;PUBMED:22939629;PUBMED:26496610;PUBMED:21145461;PUBMED:27833851;PUBMED:18850735;PUBMED:26976604;PUBMED:9707402;PUBMED:28514442;PUBMED:16624822;PUBMED:19295130;PUBMED:19615732;PUBMED:22863883;PUBMED:12628923"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_124"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10091"
      uniprot "NA"
    ]
    graphics [
      x 632.7827604781596
      y 250.9484403273451
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_124"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbigene:51138;urn:miriam:ncbigene:51138;urn:miriam:ensembl:ENSG00000138663;urn:miriam:refseq:NM_001258006;urn:miriam:uniprot:Q9BT78;urn:miriam:uniprot:Q9BT78;urn:miriam:hgnc:16702;urn:miriam:hgnc.symbol:COPS4;urn:miriam:hgnc.symbol:COPS4;urn:miriam:hgnc.symbol:COPS7A;urn:miriam:uniprot:Q9UBW8;urn:miriam:ncbigene:50813"
      hgnc "HGNC_SYMBOL:COPS4;HGNC_SYMBOL:COPS7A"
      map_id "M115_260"
      name "COPS4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1089"
      uniprot "UNIPROT:Q9BT78;UNIPROT:Q9UBW8"
    ]
    graphics [
      x 868.3201255948311
      y 288.97804362519
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_260"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:2240;urn:miriam:ec-code:3.4.-.-;urn:miriam:ensembl:ENSG00000121022;urn:miriam:uniprot:Q92905;urn:miriam:ncbigene:10987;urn:miriam:ncbigene:10987;urn:miriam:hgnc.symbol:COPS5;urn:miriam:refseq:NM_006837;urn:miriam:hgnc.symbol:COPS5"
      hgnc "HGNC_SYMBOL:COPS5"
      map_id "M115_258"
      name "COPS5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1087"
      uniprot "UNIPROT:Q92905"
    ]
    graphics [
      x 788.1644211212281
      y 402.85316821572655
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_258"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000168090;urn:miriam:refseq:NM_006833;urn:miriam:ncbigene:10980;urn:miriam:ncbigene:10980;urn:miriam:hgnc.symbol:COPS6;urn:miriam:hgnc.symbol:COPS6;urn:miriam:uniprot:Q7L5N1;urn:miriam:hgnc:21749"
      hgnc "HGNC_SYMBOL:COPS6"
      map_id "M115_257"
      name "COPS6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1086"
      uniprot "UNIPROT:Q7L5N1"
    ]
    graphics [
      x 653.0841427531295
      y 696.104234475261
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_257"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:refseq:NM_001164093;urn:miriam:hgnc:16758;urn:miriam:ensembl:ENSG00000111652;urn:miriam:hgnc.symbol:COPS7A;urn:miriam:hgnc.symbol:COPS7A;urn:miriam:uniprot:Q9UBW8;urn:miriam:ncbigene:50813;urn:miriam:ncbigene:50813"
      hgnc "HGNC_SYMBOL:COPS7A"
      map_id "M115_259"
      name "COPS7A"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1088"
      uniprot "UNIPROT:Q9UBW8"
    ]
    graphics [
      x 769.0288827920326
      y 201.54494606646995
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_259"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:refseq:NM_006710;urn:miriam:uniprot:Q99627;urn:miriam:hgnc.symbol:COPS8;urn:miriam:ncbigene:10920;urn:miriam:ensembl:ENSG00000198612;urn:miriam:hgnc.symbol:COPS8;urn:miriam:ncbigene:10920;urn:miriam:hgnc:24335"
      hgnc "HGNC_SYMBOL:COPS8"
      map_id "M115_262"
      name "COPS8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1091"
      uniprot "UNIPROT:Q99627"
    ]
    graphics [
      x 629.2163485248626
      y 102.64469311020946
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_262"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:16045761;urn:miriam:ncbigene:51138;urn:miriam:ncbigene:51138;urn:miriam:ensembl:ENSG00000138663;urn:miriam:refseq:NM_001258006;urn:miriam:uniprot:Q9BT78;urn:miriam:uniprot:Q9BT78;urn:miriam:hgnc:16702;urn:miriam:hgnc.symbol:COPS4;urn:miriam:hgnc.symbol:COPS4;urn:miriam:hgnc.symbol:COPS7A;urn:miriam:uniprot:Q9UBW8;urn:miriam:ncbigene:50813;urn:miriam:ensembl:ENSG00000168090;urn:miriam:refseq:NM_006833;urn:miriam:ncbigene:10980;urn:miriam:ncbigene:10980;urn:miriam:hgnc.symbol:COPS6;urn:miriam:hgnc.symbol:COPS6;urn:miriam:uniprot:Q7L5N1;urn:miriam:hgnc:21749;urn:miriam:hgnc:2240;urn:miriam:ec-code:3.4.-.-;urn:miriam:ensembl:ENSG00000121022;urn:miriam:uniprot:Q92905;urn:miriam:ncbigene:10987;urn:miriam:ncbigene:10987;urn:miriam:hgnc.symbol:COPS5;urn:miriam:refseq:NM_006837;urn:miriam:hgnc.symbol:COPS5;urn:miriam:refseq:NM_001164093;urn:miriam:hgnc:16758;urn:miriam:ensembl:ENSG00000111652;urn:miriam:hgnc.symbol:COPS7A;urn:miriam:hgnc.symbol:COPS7A;urn:miriam:uniprot:Q9UBW8;urn:miriam:ncbigene:50813;urn:miriam:ncbigene:50813;urn:miriam:ensembl:ENSG00000166200;urn:miriam:refseq:NM_004236;urn:miriam:ncbigene:9318;urn:miriam:ncbigene:9318;urn:miriam:hgnc:30747;urn:miriam:uniprot:P61201;urn:miriam:hgnc.symbol:COPS2;urn:miriam:hgnc.symbol:COPS2;urn:miriam:refseq:NM_006710;urn:miriam:uniprot:Q99627;urn:miriam:hgnc.symbol:COPS8;urn:miriam:ncbigene:10920;urn:miriam:ensembl:ENSG00000198612;urn:miriam:hgnc.symbol:COPS8;urn:miriam:ncbigene:10920;urn:miriam:hgnc:24335"
      hgnc "HGNC_SYMBOL:COPS4;HGNC_SYMBOL:COPS7A;HGNC_SYMBOL:COPS6;HGNC_SYMBOL:COPS5;HGNC_SYMBOL:COPS2;HGNC_SYMBOL:COPS8"
      map_id "M115_55"
      name "COPS"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa40"
      uniprot "UNIPROT:Q9BT78;UNIPROT:Q9UBW8;UNIPROT:Q7L5N1;UNIPROT:Q92905;UNIPROT:P61201;UNIPROT:Q99627"
    ]
    graphics [
      x 681.0714201367325
      y 312.8481947947439
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M115_284"
      name "Nsp12"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1184"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1492.3178078592282
      y 843.1745121836244
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_284"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_168"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10145"
      uniprot "NA"
    ]
    graphics [
      x 1436.6564573165317
      y 1158.2301693861857
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_168"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:2.7.11.4;urn:miriam:uniprot:O14874;urn:miriam:hgnc.symbol:BCKDK;urn:miriam:hgnc.symbol:BCKDK;urn:miriam:ncbigene:10295;urn:miriam:refseq:NM_005881;urn:miriam:ncbigene:10295;urn:miriam:ensembl:ENSG00000103507;urn:miriam:hgnc:16902"
      hgnc "HGNC_SYMBOL:BCKDK"
      map_id "M115_289"
      name "BCKDK"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1198"
      uniprot "UNIPROT:O14874"
    ]
    graphics [
      x 1312.8094866750878
      y 1518.0391548137457
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_289"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ec-code:2.7.11.4;urn:miriam:uniprot:O14874;urn:miriam:hgnc.symbol:BCKDK;urn:miriam:hgnc.symbol:BCKDK;urn:miriam:ncbigene:10295;urn:miriam:refseq:NM_005881;urn:miriam:ncbigene:10295;urn:miriam:ensembl:ENSG00000103507;urn:miriam:hgnc:16902"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:BCKDK"
      map_id "M115_81"
      name "s389"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa66"
      uniprot "UNIPROT:P0DTD1;UNIPROT:O14874"
    ]
    graphics [
      x 1452.078418070566
      y 1049.1922635836813
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:I2A5W5;urn:miriam:taxonomy:11676;urn:miriam:hgnc.symbol:vpr"
      hgnc "HGNC_SYMBOL:vpr"
      map_id "M115_256"
      name "Vpr"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1085"
      uniprot "UNIPROT:I2A5W5"
    ]
    graphics [
      x 799.1150790168715
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_256"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      annotation "PUBMED:22190034;PUBMED:12237292"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_135"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10111"
      uniprot "NA"
    ]
    graphics [
      x 730.2380820822663
      y 172.88981303025162
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_135"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:22190034;urn:miriam:ensembl:ENSG00000168090;urn:miriam:refseq:NM_006833;urn:miriam:ncbigene:10980;urn:miriam:ncbigene:10980;urn:miriam:hgnc.symbol:COPS6;urn:miriam:hgnc.symbol:COPS6;urn:miriam:uniprot:Q7L5N1;urn:miriam:hgnc:21749;urn:miriam:refseq:NM_001164093;urn:miriam:hgnc:16758;urn:miriam:ensembl:ENSG00000111652;urn:miriam:hgnc.symbol:COPS7A;urn:miriam:hgnc.symbol:COPS7A;urn:miriam:uniprot:Q9UBW8;urn:miriam:ncbigene:50813;urn:miriam:ncbigene:50813;urn:miriam:refseq:NM_006710;urn:miriam:uniprot:Q99627;urn:miriam:hgnc.symbol:COPS8;urn:miriam:ncbigene:10920;urn:miriam:ensembl:ENSG00000198612;urn:miriam:hgnc.symbol:COPS8;urn:miriam:ncbigene:10920;urn:miriam:hgnc:24335;urn:miriam:ensembl:ENSG00000166200;urn:miriam:refseq:NM_004236;urn:miriam:ncbigene:9318;urn:miriam:ncbigene:9318;urn:miriam:hgnc:30747;urn:miriam:uniprot:P61201;urn:miriam:hgnc.symbol:COPS2;urn:miriam:hgnc.symbol:COPS2;urn:miriam:ncbigene:51138;urn:miriam:ncbigene:51138;urn:miriam:ensembl:ENSG00000138663;urn:miriam:refseq:NM_001258006;urn:miriam:uniprot:Q9BT78;urn:miriam:uniprot:Q9BT78;urn:miriam:hgnc:16702;urn:miriam:hgnc.symbol:COPS4;urn:miriam:hgnc.symbol:COPS4;urn:miriam:hgnc.symbol:COPS7A;urn:miriam:uniprot:Q9UBW8;urn:miriam:ncbigene:50813;urn:miriam:uniprot:I2A5W5;urn:miriam:taxonomy:11676;urn:miriam:hgnc.symbol:vpr;urn:miriam:hgnc:2240;urn:miriam:ec-code:3.4.-.-;urn:miriam:ensembl:ENSG00000121022;urn:miriam:uniprot:Q92905;urn:miriam:ncbigene:10987;urn:miriam:ncbigene:10987;urn:miriam:hgnc.symbol:COPS5;urn:miriam:refseq:NM_006837;urn:miriam:hgnc.symbol:COPS5"
      hgnc "HGNC_SYMBOL:COPS6;HGNC_SYMBOL:COPS7A;HGNC_SYMBOL:COPS8;HGNC_SYMBOL:COPS2;HGNC_SYMBOL:COPS4;HGNC_SYMBOL:vpr;HGNC_SYMBOL:COPS5"
      map_id "M115_56"
      name "COPS"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa41"
      uniprot "UNIPROT:Q7L5N1;UNIPROT:Q9UBW8;UNIPROT:Q99627;UNIPROT:P61201;UNIPROT:Q9BT78;UNIPROT:I2A5W5;UNIPROT:Q92905"
    ]
    graphics [
      x 833.5667316840008
      y 308.3888649733176
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbigene:3615;urn:miriam:ncbigene:3615;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:ec-code:1.1.1.205;urn:miriam:uniprot:P12268;urn:miriam:refseq:NM_000884;urn:miriam:hgnc:6053;urn:miriam:ensembl:ENSG00000178035"
      hgnc "HGNC_SYMBOL:IMPDH2"
      map_id "M115_353"
      name "IMPDH2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1395"
      uniprot "UNIPROT:P12268"
    ]
    graphics [
      x 1590.6354810640396
      y 568.2171480599052
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_353"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      annotation "PUBMED:18506437;PUBMED:11752352"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_229"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10208"
      uniprot "NA"
    ]
    graphics [
      x 1753.0732034177472
      y 281.2628775294704
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_229"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:667490"
      hgnc "NA"
      map_id "M115_356"
      name "Mercaptopurine"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1402"
      uniprot "NA"
    ]
    graphics [
      x 1798.5459494961417
      y 146.47715920639394
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_356"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:18506437;urn:miriam:pubchem.compound:667490;urn:miriam:ncbigene:3615;urn:miriam:ncbigene:3615;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:ec-code:1.1.1.205;urn:miriam:uniprot:P12268;urn:miriam:refseq:NM_000884;urn:miriam:hgnc:6053;urn:miriam:ensembl:ENSG00000178035"
      hgnc "HGNC_SYMBOL:IMPDH2"
      map_id "M115_29"
      name "IMercomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa125"
      uniprot "UNIPROT:P12268"
    ]
    graphics [
      x 1760.8640127022704
      y 405.66321836658847
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M115_368"
      name "Nsp9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1428"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1926.714862781398
      y 1092.3193712968389
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_368"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_143"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10119"
      uniprot "NA"
    ]
    graphics [
      x 2140.740626719449
      y 757.5032276229805
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_143"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:2.3.2.27;urn:miriam:uniprot:Q86YT6;urn:miriam:pubmed:24185901;urn:miriam:ncbigene:57534;urn:miriam:ncbigene:57534;urn:miriam:refseq:NM_020774;urn:miriam:hgnc.symbol:MIB1;urn:miriam:ensembl:ENSG00000101752;urn:miriam:hgnc.symbol:MIB1;urn:miriam:hgnc:21086;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:MIB1;HGNC_SYMBOL:rep"
      map_id "M115_45"
      name "mibcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa28"
      uniprot "UNIPROT:Q86YT6;UNIPROT:P0DTD1"
    ]
    graphics [
      x 2168.476374534347
      y 654.6297078009127
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000093010;urn:miriam:uniprot:P21964;urn:miriam:hgnc.symbol:COMT;urn:miriam:hgnc.symbol:COMT;urn:miriam:hgnc:2228;urn:miriam:ec-code:2.1.1.6;urn:miriam:refseq:NM_000754;urn:miriam:ncbigene:1312;urn:miriam:ncbigene:1312"
      hgnc "HGNC_SYMBOL:COMT"
      map_id "M115_313"
      name "COMT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1277"
      uniprot "UNIPROT:P21964"
    ]
    graphics [
      x 1284.8862151187577
      y 2131.941260570109
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_313"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      annotation "PUBMED:17016423;PUBMED:17139284;PUBMED:10592235"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_197"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10176"
      uniprot "NA"
    ]
    graphics [
      x 1578.2972744223555
      y 2042.2699755148278
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_197"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:3870203"
      hgnc "NA"
      map_id "M115_317"
      name "3,5_minus_Dinitrocatechol"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1287"
      uniprot "NA"
    ]
    graphics [
      x 1791.5294757341446
      y 1934.6207976804278
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_317"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:17016423;urn:miriam:ensembl:ENSG00000093010;urn:miriam:uniprot:P21964;urn:miriam:hgnc.symbol:COMT;urn:miriam:hgnc.symbol:COMT;urn:miriam:hgnc:2228;urn:miriam:ec-code:2.1.1.6;urn:miriam:refseq:NM_000754;urn:miriam:ncbigene:1312;urn:miriam:ncbigene:1312;urn:miriam:pubchem.compound:3870203"
      hgnc "HGNC_SYMBOL:COMT"
      map_id "M115_110"
      name "DCcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa94"
      uniprot "UNIPROT:P21964"
    ]
    graphics [
      x 1662.8887888791726
      y 1837.9644090091235
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:1371;urn:miriam:uniprot:O43570;urn:miriam:ncbigene:771;urn:miriam:refseq:NM_001218;urn:miriam:ncbigene:771;urn:miriam:ec-code:4.2.1.1;urn:miriam:hgnc.symbol:CA12;urn:miriam:hgnc.symbol:CA12;urn:miriam:ensembl:ENSG00000074410"
      hgnc "HGNC_SYMBOL:CA12"
      map_id "M115_299"
      name "CA12"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1228"
      uniprot "UNIPROT:O43570"
    ]
    graphics [
      x 1254.0535753755066
      y 984.4996142218105
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_299"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      annotation "PUBMED:17582922;PUBMED:18537527;PUBMED:17762320;PUBMED:19703035;PUBMED:18782051"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_179"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10157"
      uniprot "NA"
    ]
    graphics [
      x 1132.4595462536395
      y 1273.1006599214722
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_179"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.substance:5734"
      hgnc "NA"
      map_id "M115_300"
      name "Zonisamide"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1231"
      uniprot "NA"
    ]
    graphics [
      x 1245.8396141760265
      y 1377.1973004407464
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_300"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:19703035;urn:miriam:hgnc:1371;urn:miriam:uniprot:O43570;urn:miriam:ncbigene:771;urn:miriam:refseq:NM_001218;urn:miriam:ncbigene:771;urn:miriam:ec-code:4.2.1.1;urn:miriam:hgnc.symbol:CA12;urn:miriam:hgnc.symbol:CA12;urn:miriam:ensembl:ENSG00000074410;urn:miriam:pubchem.substance:5734"
      hgnc "HGNC_SYMBOL:CA12"
      map_id "M115_92"
      name "ZonisamideComp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa77"
      uniprot "UNIPROT:O43570"
    ]
    graphics [
      x 1104.5112521159153
      y 1477.6562148819366
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_127"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10103"
      uniprot "NA"
    ]
    graphics [
      x 1532.355097624702
      y 1331.1388210230366
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_127"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:10644686;urn:miriam:pubmed:23425511;urn:miriam:pubmed:25075345;urn:miriam:ncbigene:27430;urn:miriam:ncbigene:27430;urn:miriam:uniprot:Q9NZL9;urn:miriam:hgnc.symbol:MAT2B;urn:miriam:hgnc.symbol:MAT2B;urn:miriam:pubmed:23189196;urn:miriam:refseq:NM_013283;urn:miriam:ensembl:ENSG00000038274;urn:miriam:hgnc:6905"
      hgnc "HGNC_SYMBOL:MAT2B"
      map_id "M115_376"
      name "MAT2B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa962"
      uniprot "UNIPROT:Q9NZL9"
    ]
    graphics [
      x 1427.9008651155716
      y 1499.2568678843445
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_376"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:pubmed:10644686;urn:miriam:pubmed:23425511;urn:miriam:pubmed:25075345;urn:miriam:ncbigene:27430;urn:miriam:ncbigene:27430;urn:miriam:uniprot:Q9NZL9;urn:miriam:hgnc.symbol:MAT2B;urn:miriam:hgnc.symbol:MAT2B;urn:miriam:pubmed:23189196;urn:miriam:refseq:NM_013283;urn:miriam:ensembl:ENSG00000038274;urn:miriam:hgnc:6905"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:MAT2B"
      map_id "M115_36"
      name "mat2bcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa15"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q9NZL9"
    ]
    graphics [
      x 1676.155782792946
      y 1399.569476763605
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-;urn:miriam:uniprot:P0C6X7"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M115_347"
      name "Nsp14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1378"
      uniprot "UNIPROT:P0C6X7"
    ]
    graphics [
      x 1735.4414401993706
      y 1237.4089536190913
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_347"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_231"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10210"
      uniprot "NA"
    ]
    graphics [
      x 1621.5576042682985
      y 997.7627093157273
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_231"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:Q9NXA8;urn:miriam:ec-code:2.3.1.-;urn:miriam:hgnc.symbol:SIRT5;urn:miriam:hgnc.symbol:SIRT5;urn:miriam:hgnc:14933;urn:miriam:ncbigene:23408;urn:miriam:ensembl:ENSG00000124523;urn:miriam:ncbigene:23408;urn:miriam:refseq:NM_001193267"
      hgnc "HGNC_SYMBOL:SIRT5"
      map_id "M115_358"
      name "SIRT5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1408"
      uniprot "UNIPROT:Q9NXA8"
    ]
    graphics [
      x 1397.1871425780698
      y 716.9485675957865
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_358"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:uniprot:Q9NXA8;urn:miriam:ec-code:2.3.1.-;urn:miriam:hgnc.symbol:SIRT5;urn:miriam:hgnc.symbol:SIRT5;urn:miriam:hgnc:14933;urn:miriam:ncbigene:23408;urn:miriam:ensembl:ENSG00000124523;urn:miriam:ncbigene:23408;urn:miriam:refseq:NM_001193267;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-;urn:miriam:uniprot:P0C6X7"
      hgnc "HGNC_SYMBOL:SIRT5;HGNC_SYMBOL:rep"
      map_id "M115_31"
      name "SIRT5comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa127"
      uniprot "UNIPROT:Q9NXA8;UNIPROT:P0C6X7"
    ]
    graphics [
      x 1740.579285566439
      y 1122.5845710262074
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M115_307"
      name "Nsp10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1257"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 820.0800411036944
      y 1521.4193406306879
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_307"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_225"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10204"
      uniprot "NA"
    ]
    graphics [
      x 609.8418323078847
      y 1486.776534216798
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_225"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:AP2A2;urn:miriam:hgnc.symbol:AP2A2;urn:miriam:uniprot:O94973;urn:miriam:refseq:NM_012305;urn:miriam:ncbigene:161;urn:miriam:ensembl:ENSG00000183020;urn:miriam:ncbigene:161;urn:miriam:hgnc:562"
      hgnc "HGNC_SYMBOL:AP2A2"
      map_id "M115_352"
      name "AP2A2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1392"
      uniprot "UNIPROT:O94973"
    ]
    graphics [
      x 670.7479333153851
      y 1339.716627671548
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_352"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:hgnc.symbol:AP2A2;urn:miriam:hgnc.symbol:AP2A2;urn:miriam:uniprot:O94973;urn:miriam:refseq:NM_012305;urn:miriam:ncbigene:161;urn:miriam:ensembl:ENSG00000183020;urn:miriam:ncbigene:161;urn:miriam:hgnc:562"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:AP2A2"
      map_id "M115_26"
      name "AP2A2comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa122"
      uniprot "UNIPROT:P0DTD1;UNIPROT:O94973"
    ]
    graphics [
      x 548.8466760162185
      y 1579.8446828515343
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M115_365"
      name "Nsp9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1424"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1200.4369224286513
      y 847.382282645684
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_365"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_146"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10122"
      uniprot "NA"
    ]
    graphics [
      x 1456.7207958573167
      y 913.4153473751212
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_146"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:Q15056;urn:miriam:pubmed:10585411;urn:miriam:hgnc.symbol:EIF4H;urn:miriam:hgnc.symbol:EIF4H;urn:miriam:ensembl:ENSG00000106682;urn:miriam:hgnc:12741;urn:miriam:pubmed:11418588;urn:miriam:ncbigene:7458;urn:miriam:refseq:NM_022170;urn:miriam:ncbigene:7458"
      hgnc "HGNC_SYMBOL:EIF4H"
      map_id "M115_384"
      name "EIF4H"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa985"
      uniprot "UNIPROT:Q15056"
    ]
    graphics [
      x 1572.2094589191477
      y 920.1936281468126
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_384"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:uniprot:Q15056;urn:miriam:pubmed:10585411;urn:miriam:hgnc.symbol:EIF4H;urn:miriam:hgnc.symbol:EIF4H;urn:miriam:ensembl:ENSG00000106682;urn:miriam:hgnc:12741;urn:miriam:pubmed:11418588;urn:miriam:ncbigene:7458;urn:miriam:refseq:NM_022170;urn:miriam:ncbigene:7458"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:EIF4H"
      map_id "M115_42"
      name "eifcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa25"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q15056"
    ]
    graphics [
      x 1582.8086276394458
      y 847.7495885590776
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M115_370"
      name "Nsp7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1430"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1793.425490563222
      y 1382.083705380624
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_370"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_188"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10167"
      uniprot "NA"
    ]
    graphics [
      x 1674.4671449341251
      y 1258.8042037492346
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_188"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:refseq:NM_000398;urn:miriam:ensembl:ENSG00000100243;urn:miriam:hgnc:2873;urn:miriam:uniprot:P00387;urn:miriam:ncbigene:1727;urn:miriam:ncbigene:1727;urn:miriam:hgnc.symbol:CYB5R3;urn:miriam:hgnc.symbol:CYB5R3;urn:miriam:ec-code:1.6.2.2"
      hgnc "HGNC_SYMBOL:CYB5R3"
      map_id "M115_308"
      name "CYB5R3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1260"
      uniprot "UNIPROT:P00387"
    ]
    graphics [
      x 1619.6158735874965
      y 1403.0260300106984
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_308"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:refseq:NM_000398;urn:miriam:ensembl:ENSG00000100243;urn:miriam:hgnc:2873;urn:miriam:uniprot:P00387;urn:miriam:ncbigene:1727;urn:miriam:ncbigene:1727;urn:miriam:hgnc.symbol:CYB5R3;urn:miriam:hgnc.symbol:CYB5R3;urn:miriam:ec-code:1.6.2.2"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:CYB5R3"
      map_id "M115_102"
      name "CYB5R3comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa86"
      uniprot "UNIPROT:P0DTD1;UNIPROT:P00387"
    ]
    graphics [
      x 1678.4352761268933
      y 1119.0665450988056
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      annotation "PUBMED:17016423;PUBMED:17139284;PUBMED:10592235"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_190"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10169"
      uniprot "NA"
    ]
    graphics [
      x 1920.2579811101614
      y 1355.810265958076
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_190"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:643975;urn:miriam:obo.chebi:CHEBI%3A16238"
      hgnc "NA"
      map_id "M115_310"
      name "FAD"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa1269"
      uniprot "NA"
    ]
    graphics [
      x 2015.824792061172
      y 1246.425517298627
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_310"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:17016423;urn:miriam:refseq:NM_000398;urn:miriam:ensembl:ENSG00000100243;urn:miriam:hgnc:2873;urn:miriam:uniprot:P00387;urn:miriam:ncbigene:1727;urn:miriam:ncbigene:1727;urn:miriam:hgnc.symbol:CYB5R3;urn:miriam:hgnc.symbol:CYB5R3;urn:miriam:ec-code:1.6.2.2;urn:miriam:pubchem.compound:643975;urn:miriam:obo.chebi:CHEBI%3A16238"
      hgnc "HGNC_SYMBOL:CYB5R3"
      map_id "M115_104"
      name "FADcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa88"
      uniprot "UNIPROT:P00387"
    ]
    graphics [
      x 2082.878616358988
      y 1362.3365189051108
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      annotation "PUBMED:24778252;PUBMED:32353859;PUBMED:29845934;PUBMED:18483487;PUBMED:26725010;PUBMED:17643375"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_218"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10197"
      uniprot "NA"
    ]
    graphics [
      x 880.7950166684539
      y 458.91811874703046
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_218"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:MEPCE;urn:miriam:hgnc.symbol:MEPCE;urn:miriam:ensembl:ENSG00000146834;urn:miriam:ec-code:2.1.1.-;urn:miriam:hgnc:20247;urn:miriam:ncbigene:56257;urn:miriam:ncbigene:56257;urn:miriam:refseq:NM_001194990;urn:miriam:uniprot:Q7L2J0"
      hgnc "HGNC_SYMBOL:MEPCE"
      map_id "M115_340"
      name "MEPCE"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1361"
      uniprot "UNIPROT:Q7L2J0"
    ]
    graphics [
      x 1007.8940883955415
      y 393.3411500658036
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_340"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:24912;urn:miriam:ensembl:ENSG00000174720;urn:miriam:hgnc.symbol:LARP7;urn:miriam:ncbigene:51574;urn:miriam:hgnc.symbol:LARP7;urn:miriam:ncbigene:51574;urn:miriam:uniprot:Q4G0J3;urn:miriam:refseq:NM_016648"
      hgnc "HGNC_SYMBOL:LARP7"
      map_id "M115_341"
      name "LARP7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1364"
      uniprot "UNIPROT:Q4G0J3"
    ]
    graphics [
      x 930.9561393704985
      y 330.6723903987844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_341"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:29845934;urn:miriam:hgnc.symbol:MEPCE;urn:miriam:hgnc.symbol:MEPCE;urn:miriam:ensembl:ENSG00000146834;urn:miriam:ec-code:2.1.1.-;urn:miriam:hgnc:20247;urn:miriam:ncbigene:56257;urn:miriam:ncbigene:56257;urn:miriam:refseq:NM_001194990;urn:miriam:uniprot:Q7L2J0;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:hgnc:24912;urn:miriam:ensembl:ENSG00000174720;urn:miriam:hgnc.symbol:LARP7;urn:miriam:ncbigene:51574;urn:miriam:hgnc.symbol:LARP7;urn:miriam:ncbigene:51574;urn:miriam:uniprot:Q4G0J3;urn:miriam:refseq:NM_016648"
      hgnc "HGNC_SYMBOL:MEPCE;HGNC_SYMBOL:rep;HGNC_SYMBOL:LARP7"
      map_id "M115_18"
      name "MEPCEcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa115"
      uniprot "UNIPROT:Q7L2J0;UNIPROT:P0DTD1;UNIPROT:Q4G0J3"
    ]
    graphics [
      x 757.7696114239382
      y 382.7965058598693
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      annotation "PUBMED:19153232"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_185"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10164"
      uniprot "NA"
    ]
    graphics [
      x 1484.7326602453863
      y 1248.4708154592374
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_185"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:19153232;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M115_99"
      name "homodimer"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa83"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1524.0585324454705
      y 1426.188632237307
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:446541;urn:miriam:pubmed:17496727"
      hgnc "NA"
      map_id "M115_367"
      name "Mycophenolic_space_acid"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1427"
      uniprot "NA"
    ]
    graphics [
      x 1203.043967006859
      y 1054.119697630918
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_367"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_235"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "re10214"
      uniprot "NA"
    ]
    graphics [
      x 1388.147638442678
      y 881.2645338075683
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_235"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:446541;urn:miriam:pubmed:17496727"
      hgnc "NA"
      map_id "M115_354"
      name "Mycophenolic_space_acid"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1398"
      uniprot "NA"
    ]
    graphics [
      x 1516.8523428688254
      y 647.250792345174
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_354"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      annotation "PUBMED:17472992;PUBMED:10961375;PUBMED:11849873;PUBMED:11704565;PUBMED:11752352;PUBMED:11728166;PUBMED:11447307"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_159"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10136"
      uniprot "NA"
    ]
    graphics [
      x 1093.140972754218
      y 325.0878843081613
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_159"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:643975"
      hgnc "NA"
      map_id "M115_279"
      name "Sitaxentan"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1169"
      uniprot "NA"
    ]
    graphics [
      x 1088.5046150290434
      y 162.77342726248037
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_279"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:17472992;urn:miriam:pubchem.compound:643975;urn:miriam:refseq:NM_001166055;urn:miriam:hgnc:3179;urn:miriam:uniprot:P25101;urn:miriam:ensembl:ENSG00000151617;urn:miriam:ncbigene:1909;urn:miriam:ncbigene:1909;urn:miriam:hgnc.symbol:EDNRA;urn:miriam:hgnc.symbol:EDNRA"
      hgnc "HGNC_SYMBOL:EDNRA"
      map_id "M115_74"
      name "EDNRASitaComp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa59"
      uniprot "UNIPROT:P25101"
    ]
    graphics [
      x 1241.175433741662
      y 259.7236011126462
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:8702639;urn:miriam:hgnc.symbol:FBLN2;urn:miriam:hgnc.symbol:FBLN2;urn:miriam:uniprot:P98095;urn:miriam:uniprot:P98095;urn:miriam:refseq:NM_001004019;urn:miriam:ensembl:ENSG00000163520;urn:miriam:hgnc:3601;urn:miriam:ncbigene:2199;urn:miriam:ncbigene:2199;urn:miriam:ensembl:ENSG00000166147;urn:miriam:uniprot:P35555;urn:miriam:uniprot:P35555;urn:miriam:hgnc.symbol:FBN1;urn:miriam:hgnc.symbol:FBN1;urn:miriam:hgnc:3603;urn:miriam:ncbigene:2200;urn:miriam:ncbigene:2200;urn:miriam:refseq:NM_000138"
      hgnc "HGNC_SYMBOL:FBLN2;HGNC_SYMBOL:FBN1"
      map_id "M115_95"
      name "Fibrillin"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa8"
      uniprot "UNIPROT:P98095;UNIPROT:P35555"
    ]
    graphics [
      x 1158.8986718205235
      y 546.0977876757037
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      annotation "PUBMED:21001709;PUBMED:10544250;PUBMED:10825173"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_152"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10128"
      uniprot "NA"
    ]
    graphics [
      x 1317.9879674832082
      y 571.3224944216528
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_152"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbigene:2006;urn:miriam:ncbigene:2006;urn:miriam:uniprot:P15502;urn:miriam:hgnc:3327;urn:miriam:ensembl:ENSG00000049540;urn:miriam:hgnc.symbol:ELN;urn:miriam:hgnc.symbol:ELN;urn:miriam:refseq:NM_000501"
      hgnc "HGNC_SYMBOL:ELN"
      map_id "M115_271"
      name "ELN"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1139"
      uniprot "UNIPROT:P15502"
    ]
    graphics [
      x 1686.3889667445114
      y 662.6484976700051
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_271"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:8702639;urn:miriam:pubmed:10544250;urn:miriam:pubmed:10825173;urn:miriam:ensembl:ENSG00000166147;urn:miriam:uniprot:P35555;urn:miriam:uniprot:P35555;urn:miriam:hgnc.symbol:FBN1;urn:miriam:hgnc.symbol:FBN1;urn:miriam:hgnc:3603;urn:miriam:ncbigene:2200;urn:miriam:ncbigene:2200;urn:miriam:refseq:NM_000138;urn:miriam:ncbigene:2006;urn:miriam:ncbigene:2006;urn:miriam:uniprot:P15502;urn:miriam:hgnc:3327;urn:miriam:ensembl:ENSG00000049540;urn:miriam:hgnc.symbol:ELN;urn:miriam:hgnc.symbol:ELN;urn:miriam:refseq:NM_000501;urn:miriam:hgnc.symbol:FBLN2;urn:miriam:hgnc.symbol:FBLN2;urn:miriam:uniprot:P98095;urn:miriam:uniprot:P98095;urn:miriam:refseq:NM_001004019;urn:miriam:ensembl:ENSG00000163520;urn:miriam:hgnc:3601;urn:miriam:ncbigene:2199;urn:miriam:ncbigene:2199"
      hgnc "HGNC_SYMBOL:FBN1;HGNC_SYMBOL:ELN;HGNC_SYMBOL:FBLN2"
      map_id "M115_66"
      name "Fibrillin"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa51"
      uniprot "UNIPROT:P35555;UNIPROT:P15502;UNIPROT:P98095"
    ]
    graphics [
      x 1198.7862687560614
      y 458.7181058496965
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:EXOSC2;urn:miriam:hgnc.symbol:EXOSC2;urn:miriam:refseq:NM_014285;urn:miriam:uniprot:Q13868;urn:miriam:hgnc:17097;urn:miriam:ncbigene:23404;urn:miriam:ensembl:ENSG00000130713;urn:miriam:ncbigene:23404"
      hgnc "HGNC_SYMBOL:EXOSC2"
      map_id "M115_326"
      name "EXOSC2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1322"
      uniprot "UNIPROT:Q13868"
    ]
    graphics [
      x 1130.2242479276238
      y 672.5687994739411
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_326"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_208"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10187"
      uniprot "NA"
    ]
    graphics [
      x 1002.8896148115551
      y 604.0483351776768
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_208"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:EXOSC3;urn:miriam:hgnc.symbol:EXOSC3;urn:miriam:uniprot:Q9NQT5;urn:miriam:hgnc:17944;urn:miriam:refseq:NM_016042;urn:miriam:ncbigene:51010;urn:miriam:ensembl:ENSG00000107371;urn:miriam:ncbigene:51010"
      hgnc "HGNC_SYMBOL:EXOSC3"
      map_id "M115_330"
      name "EXOSC3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1333"
      uniprot "UNIPROT:Q9NQT5"
    ]
    graphics [
      x 1113.1774085493307
      y 499.81046715595704
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_330"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:Q9NQT4;urn:miriam:hgnc.symbol:EXOSC5;urn:miriam:hgnc.symbol:EXOSC5;urn:miriam:hgnc:24662;urn:miriam:refseq:NM_020158;urn:miriam:ensembl:ENSG00000077348;urn:miriam:ncbigene:56915;urn:miriam:ncbigene:56915"
      hgnc "HGNC_SYMBOL:EXOSC5"
      map_id "M115_331"
      name "EXOSC5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1335"
      uniprot "UNIPROT:Q9NQT4"
    ]
    graphics [
      x 1066.0820436976555
      y 700.5735954636368
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_331"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:17035;urn:miriam:ensembl:ENSG00000120699;urn:miriam:ncbigene:11340;urn:miriam:ncbigene:11340;urn:miriam:hgnc.symbol:EXOSC8;urn:miriam:hgnc.symbol:EXOSC8;urn:miriam:refseq:NM_181503;urn:miriam:uniprot:Q96B26"
      hgnc "HGNC_SYMBOL:EXOSC8"
      map_id "M115_338"
      name "EXOSC8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1356"
      uniprot "UNIPROT:Q96B26"
    ]
    graphics [
      x 971.7100787194651
      y 691.1522918929605
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_338"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:pubmed:28514442;urn:miriam:hgnc.symbol:EXOSC3;urn:miriam:hgnc.symbol:EXOSC3;urn:miriam:uniprot:Q9NQT5;urn:miriam:hgnc:17944;urn:miriam:refseq:NM_016042;urn:miriam:ncbigene:51010;urn:miriam:ensembl:ENSG00000107371;urn:miriam:ncbigene:51010;urn:miriam:hgnc.symbol:EXOSC2;urn:miriam:hgnc.symbol:EXOSC2;urn:miriam:refseq:NM_014285;urn:miriam:uniprot:Q13868;urn:miriam:hgnc:17097;urn:miriam:ncbigene:23404;urn:miriam:ensembl:ENSG00000130713;urn:miriam:ncbigene:23404;urn:miriam:hgnc:17035;urn:miriam:ensembl:ENSG00000120699;urn:miriam:ncbigene:11340;urn:miriam:ncbigene:11340;urn:miriam:hgnc.symbol:EXOSC8;urn:miriam:hgnc.symbol:EXOSC8;urn:miriam:refseq:NM_181503;urn:miriam:uniprot:Q96B26;urn:miriam:uniprot:Q9NQT4;urn:miriam:hgnc.symbol:EXOSC5;urn:miriam:hgnc.symbol:EXOSC5;urn:miriam:hgnc:24662;urn:miriam:refseq:NM_020158;urn:miriam:ensembl:ENSG00000077348;urn:miriam:ncbigene:56915;urn:miriam:ncbigene:56915;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:EXOSC3;HGNC_SYMBOL:EXOSC2;HGNC_SYMBOL:EXOSC8;HGNC_SYMBOL:EXOSC5;HGNC_SYMBOL:rep"
      map_id "M115_8"
      name "EXOCcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa105"
      uniprot "UNIPROT:Q9NQT5;UNIPROT:Q13868;UNIPROT:Q96B26;UNIPROT:Q9NQT4;UNIPROT:P0DTD1"
    ]
    graphics [
      x 1100.8169524776592
      y 626.370630433433
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000144029;urn:miriam:uniprot:P82675;urn:miriam:uniprot:P82675;urn:miriam:hgnc:14498;urn:miriam:hgnc.symbol:MRPS5;urn:miriam:hgnc.symbol:MRPS5;urn:miriam:refseq:NM_031902;urn:miriam:ncbigene:64969;urn:miriam:ncbigene:64969"
      hgnc "HGNC_SYMBOL:MRPS5"
      map_id "M115_327"
      name "MRPS5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1325"
      uniprot "UNIPROT:P82675"
    ]
    graphics [
      x 1392.9067700346304
      y 546.1758081101585
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_327"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_209"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10188"
      uniprot "NA"
    ]
    graphics [
      x 1240.8603352279326
      y 446.47380734311764
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_209"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:14495;urn:miriam:refseq:NM_001371401;urn:miriam:ncbigene:51116;urn:miriam:ensembl:ENSG00000122140;urn:miriam:ncbigene:51116;urn:miriam:hgnc.symbol:MRPS2;urn:miriam:hgnc.symbol:MRPS2;urn:miriam:uniprot:Q9Y399"
      hgnc "HGNC_SYMBOL:MRPS2"
      map_id "M115_328"
      name "MRPS2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1328"
      uniprot "UNIPROT:Q9Y399"
    ]
    graphics [
      x 1353.2801885453443
      y 316.4225011942276
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_328"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:hgnc:14495;urn:miriam:refseq:NM_001371401;urn:miriam:ncbigene:51116;urn:miriam:ensembl:ENSG00000122140;urn:miriam:ncbigene:51116;urn:miriam:hgnc.symbol:MRPS2;urn:miriam:hgnc.symbol:MRPS2;urn:miriam:uniprot:Q9Y399;urn:miriam:ensembl:ENSG00000144029;urn:miriam:uniprot:P82675;urn:miriam:uniprot:P82675;urn:miriam:hgnc:14498;urn:miriam:hgnc.symbol:MRPS5;urn:miriam:hgnc.symbol:MRPS5;urn:miriam:refseq:NM_031902;urn:miriam:ncbigene:64969;urn:miriam:ncbigene:64969"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:MRPS2;HGNC_SYMBOL:MRPS5"
      map_id "M115_9"
      name "MRPScomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa106"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q9Y399;UNIPROT:P82675"
    ]
    graphics [
      x 1397.7301632092729
      y 375.71756718967356
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:667;urn:miriam:refseq:NM_001664;urn:miriam:ensembl:ENSG00000067560;urn:miriam:ec-code:3.6.5.2;urn:miriam:ncbigene:387;urn:miriam:ncbigene:387;urn:miriam:uniprot:P61586;urn:miriam:hgnc.symbol:RHOA;urn:miriam:hgnc.symbol:RHOA"
      hgnc "HGNC_SYMBOL:RHOA"
      map_id "M115_322"
      name "RHOA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1305"
      uniprot "UNIPROT:P61586"
    ]
    graphics [
      x 1889.657007676891
      y 858.0560054522659
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_322"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      annotation "PUBMED:17016423;PUBMED:17139284;PUBMED:10592235"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_204"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10183"
      uniprot "NA"
    ]
    graphics [
      x 1849.366661795531
      y 512.4955108060828
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_204"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17552;urn:miriam:pubchem.compound:135398619"
      hgnc "NA"
      map_id "M115_312"
      name "GDP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa1274"
      uniprot "NA"
    ]
    graphics [
      x 2016.3251304841658
      y 605.1467361000948
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_312"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:17016423;urn:miriam:hgnc:667;urn:miriam:refseq:NM_001664;urn:miriam:ensembl:ENSG00000067560;urn:miriam:ec-code:3.6.5.2;urn:miriam:ncbigene:387;urn:miriam:ncbigene:387;urn:miriam:uniprot:P61586;urn:miriam:hgnc.symbol:RHOA;urn:miriam:hgnc.symbol:RHOA;urn:miriam:obo.chebi:CHEBI%3A17552;urn:miriam:pubchem.compound:135398619"
      hgnc "HGNC_SYMBOL:RHOA"
      map_id "M115_4"
      name "RGcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa101"
      uniprot "UNIPROT:P61586"
    ]
    graphics [
      x 1689.1945725043502
      y 335.030821345557
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      annotation "PUBMED:25544563"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_129"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10105"
      uniprot "NA"
    ]
    graphics [
      x 1560.4966792952046
      y 1482.363080997008
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_129"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:8743;urn:miriam:uniprot:P29120;urn:miriam:uniprot:P29120;urn:miriam:hgnc.symbol:PCSK1;urn:miriam:hgnc.symbol:PCSK1;urn:miriam:ensembl:ENSG00000175426;urn:miriam:ec-code:3.4.21.93;urn:miriam:ncbigene:5122;urn:miriam:refseq:NM_000439;urn:miriam:ncbigene:5122"
      hgnc "HGNC_SYMBOL:PCSK1"
      map_id "M115_268"
      name "PCSK1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1132"
      uniprot "UNIPROT:P29120"
    ]
    graphics [
      x 1366.4635847676766
      y 1192.490745398556
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_268"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:25544563;urn:miriam:pubmed:10644686;urn:miriam:pubmed:23425511;urn:miriam:pubmed:25075345;urn:miriam:ncbigene:27430;urn:miriam:ncbigene:27430;urn:miriam:uniprot:Q9NZL9;urn:miriam:hgnc.symbol:MAT2B;urn:miriam:hgnc.symbol:MAT2B;urn:miriam:pubmed:23189196;urn:miriam:refseq:NM_013283;urn:miriam:ensembl:ENSG00000038274;urn:miriam:hgnc:6905;urn:miriam:hgnc:8743;urn:miriam:uniprot:P29120;urn:miriam:uniprot:P29120;urn:miriam:hgnc.symbol:PCSK1;urn:miriam:hgnc.symbol:PCSK1;urn:miriam:ensembl:ENSG00000175426;urn:miriam:ec-code:3.4.21.93;urn:miriam:ncbigene:5122;urn:miriam:refseq:NM_000439;urn:miriam:ncbigene:5122"
      hgnc "HGNC_SYMBOL:MAT2B;HGNC_SYMBOL:PCSK1"
      map_id "M115_64"
      name "NEC1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa49"
      uniprot "UNIPROT:Q9NZL9;UNIPROT:P29120"
    ]
    graphics [
      x 1717.5284091375136
      y 1487.2628584456422
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_194"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10173"
      uniprot "NA"
    ]
    graphics [
      x 1511.3480152597879
      y 1792.228306216623
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_194"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ensembl:ENSG00000093010;urn:miriam:uniprot:P21964;urn:miriam:hgnc.symbol:COMT;urn:miriam:hgnc.symbol:COMT;urn:miriam:hgnc:2228;urn:miriam:ec-code:2.1.1.6;urn:miriam:refseq:NM_000754;urn:miriam:ncbigene:1312;urn:miriam:ncbigene:1312;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:COMT;HGNC_SYMBOL:rep"
      map_id "M115_107"
      name "COMT"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa91"
      uniprot "UNIPROT:P21964;UNIPROT:P0DTD1"
    ]
    graphics [
      x 1426.4590518694517
      y 1623.6789104504733
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_173"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10150"
      uniprot "NA"
    ]
    graphics [
      x 1749.703169111427
      y 974.9017007523172
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_173"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:Q92615;urn:miriam:refseq:NM_015155;urn:miriam:ncbigene:23185;urn:miriam:ncbigene:23185;urn:miriam:hgnc:28987;urn:miriam:ensembl:ENSG00000107929;urn:miriam:hgnc.symbol:LARP4B;urn:miriam:hgnc.symbol:LARP4B"
      hgnc "HGNC_SYMBOL:LARP4B"
      map_id "M115_294"
      name "LARP4B_space_"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1213"
      uniprot "UNIPROT:Q92615"
    ]
    graphics [
      x 1933.7484377268975
      y 1010.8683541362312
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_294"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:uniprot:Q92615;urn:miriam:refseq:NM_015155;urn:miriam:ncbigene:23185;urn:miriam:ncbigene:23185;urn:miriam:hgnc:28987;urn:miriam:ensembl:ENSG00000107929;urn:miriam:hgnc.symbol:LARP4B;urn:miriam:hgnc.symbol:LARP4B"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:LARP4B"
      map_id "M115_86"
      name "LARPcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa71"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q92615"
    ]
    graphics [
      x 1852.7428496273537
      y 1098.0999191529334
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_203"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10182"
      uniprot "NA"
    ]
    graphics [
      x 2012.4215332519739
      y 1102.663407660341
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_203"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:hgnc:667;urn:miriam:refseq:NM_001664;urn:miriam:ensembl:ENSG00000067560;urn:miriam:ec-code:3.6.5.2;urn:miriam:ncbigene:387;urn:miriam:ncbigene:387;urn:miriam:uniprot:P61586;urn:miriam:hgnc.symbol:RHOA;urn:miriam:hgnc.symbol:RHOA;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:RHOA;HGNC_SYMBOL:rep"
      map_id "M115_3"
      name "RHOA7comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa100"
      uniprot "UNIPROT:P61586;UNIPROT:P0DTD1"
    ]
    graphics [
      x 2137.0502763722775
      y 1053.0146618470162
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:9788;urn:miriam:hgnc.symbol:RAB7A;urn:miriam:hgnc.symbol:RAB7A;urn:miriam:uniprot:P51149;urn:miriam:ncbigene:7879;urn:miriam:refseq:NM_004637;urn:miriam:ncbigene:7879;urn:miriam:ensembl:ENSG00000075785"
      hgnc "HGNC_SYMBOL:RAB7A"
      map_id "M115_321"
      name "RAB7A"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1300"
      uniprot "UNIPROT:P51149"
    ]
    graphics [
      x 1830.792318538941
      y 888.0201906176941
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_321"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    cd19dm [
      annotation "PUBMED:10592235"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_202"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10181"
      uniprot "NA"
    ]
    graphics [
      x 1886.5723407176438
      y 658.5694825846467
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_202"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:10592235;urn:miriam:obo.chebi:CHEBI%3A17552;urn:miriam:pubchem.compound:135398619;urn:miriam:hgnc:9788;urn:miriam:hgnc.symbol:RAB7A;urn:miriam:hgnc.symbol:RAB7A;urn:miriam:uniprot:P51149;urn:miriam:ec-code:3.6.5.2;urn:miriam:ncbigene:7879;urn:miriam:refseq:NM_004637;urn:miriam:ncbigene:7879;urn:miriam:ensembl:ENSG00000075785"
      hgnc "HGNC_SYMBOL:RAB7A"
      map_id "M115_115"
      name "RGcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa99"
      uniprot "UNIPROT:P51149"
    ]
    graphics [
      x 1757.9213067439964
      y 598.2154592119002
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 158
    zlevel -1

    cd19dm [
      annotation "PUBMED:8940009"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_157"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10134"
      uniprot "NA"
    ]
    graphics [
      x 1273.6625884148536
      y 820.7901516793472
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_157"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 159
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:env;urn:miriam:uniprot:A0A517FIL8;urn:miriam:taxonomy:11676"
      hgnc "HGNC_SYMBOL:env"
      map_id "M115_277"
      name "ENV"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1163"
      uniprot "UNIPROT:A0A517FIL8"
    ]
    graphics [
      x 1225.889916065212
      y 674.6525385285581
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_277"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 160
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:8940009;urn:miriam:hgnc:8743;urn:miriam:uniprot:P29120;urn:miriam:uniprot:P29120;urn:miriam:hgnc.symbol:PCSK1;urn:miriam:hgnc.symbol:PCSK1;urn:miriam:ensembl:ENSG00000175426;urn:miriam:ec-code:3.4.21.93;urn:miriam:ncbigene:5122;urn:miriam:refseq:NM_000439;urn:miriam:ncbigene:5122;urn:miriam:hgnc.symbol:env;urn:miriam:uniprot:A0A517FIL8;urn:miriam:taxonomy:11676"
      hgnc "HGNC_SYMBOL:PCSK1;HGNC_SYMBOL:env"
      map_id "M115_72"
      name "NECENVComp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa57"
      uniprot "UNIPROT:P29120;UNIPROT:A0A517FIL8"
    ]
    graphics [
      x 1238.502595736786
      y 621.8866776393993
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 161
    zlevel -1

    cd19dm [
      annotation "PUBMED:17620346;PUBMED:16679386"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_163"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10140"
      uniprot "NA"
    ]
    graphics [
      x 2002.6253298396086
      y 781.237982483453
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_163"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 162
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:5090"
      hgnc "NA"
      map_id "M115_272"
      name "Rofecoxib"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1143"
      uniprot "NA"
    ]
    graphics [
      x 2147.78360060404
      y 888.6681044070881
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_272"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 163
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:17620346;urn:miriam:pubmed:16679386;urn:miriam:ncbigene:2006;urn:miriam:ncbigene:2006;urn:miriam:uniprot:P15502;urn:miriam:hgnc:3327;urn:miriam:ensembl:ENSG00000049540;urn:miriam:hgnc.symbol:ELN;urn:miriam:hgnc.symbol:ELN;urn:miriam:refseq:NM_000501;urn:miriam:pubchem.compound:5090"
      hgnc "HGNC_SYMBOL:ELN"
      map_id "M115_67"
      name "RofecoxibComp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa52"
      uniprot "UNIPROT:P15502"
    ]
    graphics [
      x 2179.0377753172115
      y 808.7608688795202
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 164
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_234"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10213"
      uniprot "NA"
    ]
    graphics [
      x 2074.150285127191
      y 1191.5374163660515
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_234"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 165
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbigene:23089;urn:miriam:ncbigene:23089;urn:miriam:hgnc:14005;urn:miriam:refseq:NM_015068;urn:miriam:ensembl:ENSG00000242265;urn:miriam:uniprot:Q86TG7;urn:miriam:hgnc.symbol:PEG10;urn:miriam:hgnc.symbol:PEG10"
      hgnc "HGNC_SYMBOL:PEG10"
      map_id "M115_361"
      name "PEG10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1418"
      uniprot "UNIPROT:Q86TG7"
    ]
    graphics [
      x 1979.4017356884615
      y 1070.998846259095
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_361"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 166
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:doi:10.1101/2020.06.17.156455;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-;urn:miriam:uniprot:P0C6X7;urn:miriam:ncbigene:23089;urn:miriam:ncbigene:23089;urn:miriam:hgnc:14005;urn:miriam:refseq:NM_015068;urn:miriam:ensembl:ENSG00000242265;urn:miriam:uniprot:Q86TG7;urn:miriam:hgnc.symbol:PEG10;urn:miriam:hgnc.symbol:PEG10"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:PEG10"
      map_id "M115_35"
      name "PEG10comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa130"
      uniprot "UNIPROT:P0C6X7;UNIPROT:Q86TG7"
    ]
    graphics [
      x 2225.1336714858753
      y 1185.7837317598155
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 167
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:Q8WTV0;urn:miriam:hgnc:1664;urn:miriam:ensembl:ENSG00000073060;urn:miriam:hgnc.symbol:SCARB1;urn:miriam:hgnc.symbol:SCARB1;urn:miriam:ncbigene:949;urn:miriam:ncbigene:949;urn:miriam:refseq:NM_005505"
      hgnc "HGNC_SYMBOL:SCARB1"
      map_id "M115_298"
      name "SCARB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1225"
      uniprot "UNIPROT:Q8WTV0"
    ]
    graphics [
      x 1988.621505667054
      y 1147.4234164898992
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_298"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 168
    zlevel -1

    cd19dm [
      annotation "PUBMED:16530182;PUBMED:16371234;PUBMED:15541376;PUBMED:14594995;PUBMED:15791597"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_176"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10154"
      uniprot "NA"
    ]
    graphics [
      x 2039.8074320766652
      y 912.4188923306091
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_176"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 169
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:6323481"
      hgnc "NA"
      map_id "M115_297"
      name "Phosphatidyl_space_serine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa1222"
      uniprot "NA"
    ]
    graphics [
      x 2068.5199715454814
      y 785.2233942101656
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_297"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 170
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:16530182;urn:miriam:pubchem.compound:6323481;urn:miriam:uniprot:Q8WTV0;urn:miriam:hgnc:1664;urn:miriam:ensembl:ENSG00000073060;urn:miriam:hgnc.symbol:SCARB1;urn:miriam:hgnc.symbol:SCARB1;urn:miriam:ncbigene:949;urn:miriam:ncbigene:949;urn:miriam:refseq:NM_005505"
      hgnc "HGNC_SYMBOL:SCARB1"
      map_id "M115_89"
      name "lipidcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa74"
      uniprot "UNIPROT:Q8WTV0"
    ]
    graphics [
      x 1892.4807192957546
      y 822.1379698301648
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 171
    zlevel -1

    cd19dm [
      annotation "PUBMED:17355872"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_232"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10211"
      uniprot "NA"
    ]
    graphics [
      x 1073.6560866771174
      y 471.6312558187367
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_232"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 172
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:5361"
      hgnc "NA"
      map_id "M115_359"
      name "Suramin"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1412"
      uniprot "NA"
    ]
    graphics [
      x 888.2055951840624
      y 509.37765508210833
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_359"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 173
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:17355872;urn:miriam:pubchem.compound:5361;urn:miriam:uniprot:Q9NXA8;urn:miriam:ec-code:2.3.1.-;urn:miriam:hgnc.symbol:SIRT5;urn:miriam:hgnc.symbol:SIRT5;urn:miriam:hgnc:14933;urn:miriam:ncbigene:23408;urn:miriam:ensembl:ENSG00000124523;urn:miriam:ncbigene:23408;urn:miriam:refseq:NM_001193267"
      hgnc "HGNC_SYMBOL:SIRT5"
      map_id "M115_32"
      name "SScomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa128"
      uniprot "UNIPROT:Q9NXA8"
    ]
    graphics [
      x 922.1304391727033
      y 392.2348895576266
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 174
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:16713569;urn:miriam:hgnc.symbol:UBQLN4;urn:miriam:hgnc.symbol:UBQLN4;urn:miriam:refseq:NM_020131;urn:miriam:uniprot:Q9NRR5;urn:miriam:hgnc:1237;urn:miriam:ncbigene:56893;urn:miriam:ncbigene:56893;urn:miriam:ensembl:ENSG00000160803;urn:miriam:hgnc:3176;urn:miriam:refseq:NM_001955;urn:miriam:uniprot:P05305;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:ensembl:ENSG00000078401"
      hgnc "HGNC_SYMBOL:UBQLN4;HGNC_SYMBOL:EDN1"
      map_id "M115_58"
      name "EDN1_minus_homo"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa43"
      uniprot "UNIPROT:Q9NRR5;UNIPROT:P05305"
    ]
    graphics [
      x 1649.7702289301208
      y 1760.9599946311866
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 175
    zlevel -1

    cd19dm [
      annotation "PUBMED:16713569"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_151"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10127"
      uniprot "NA"
    ]
    graphics [
      x 1880.7640744260855
      y 1758.678371079227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_151"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 176
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:16713569;urn:miriam:hgnc:3176;urn:miriam:refseq:NM_001955;urn:miriam:uniprot:P05305;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:ensembl:ENSG00000078401;urn:miriam:hgnc.symbol:UBQLN4;urn:miriam:hgnc.symbol:UBQLN4;urn:miriam:refseq:NM_020131;urn:miriam:uniprot:Q9NRR5;urn:miriam:hgnc:1237;urn:miriam:ncbigene:56893;urn:miriam:ncbigene:56893;urn:miriam:ensembl:ENSG00000160803"
      hgnc "HGNC_SYMBOL:EDN1;HGNC_SYMBOL:UBQLN4"
      map_id "M115_59"
      name "EDN1_minus_homo"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa44"
      uniprot "UNIPROT:P05305;UNIPROT:Q9NRR5"
    ]
    graphics [
      x 2011.2553782029831
      y 1808.8754104479162
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 177
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_164"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10141"
      uniprot "NA"
    ]
    graphics [
      x 1047.9848294920175
      y 814.437540318084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_164"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 178
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M115_283"
      name "Nsp7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1183"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1091.6599196212326
      y 940.3014012500163
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_283"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 179
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:doi:10.1101/2020.03.16.993386;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M115_78"
      name "Nsp7812"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa63"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 681.9682862656986
      y 839.820289827082
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 180
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_220"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10199"
      uniprot "NA"
    ]
    graphics [
      x 503.25900012141096
      y 953.2103941382086
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_220"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 181
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:refseq:NM_003136;urn:miriam:hgnc:11301;urn:miriam:ncbigene:6729;urn:miriam:ncbigene:6729;urn:miriam:ensembl:ENSG00000100883;urn:miriam:hgnc.symbol:SRP54;urn:miriam:hgnc.symbol:SRP54;urn:miriam:uniprot:P61011;urn:miriam:ec-code:3.6.5.-"
      hgnc "HGNC_SYMBOL:SRP54"
      map_id "M115_343"
      name "SRP54"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1369"
      uniprot "UNIPROT:P61011"
    ]
    graphics [
      x 371.8788861397027
      y 1042.040883611567
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_343"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 182
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbigene:6728;urn:miriam:ncbigene:6728;urn:miriam:refseq:NM_003135;urn:miriam:hgnc.symbol:SRP19;urn:miriam:ensembl:ENSG00000153037;urn:miriam:hgnc.symbol:SRP19;urn:miriam:hgnc:11300;urn:miriam:uniprot:P09132"
      hgnc "HGNC_SYMBOL:SRP19"
      map_id "M115_344"
      name "SRP19"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1372"
      uniprot "UNIPROT:P09132"
    ]
    graphics [
      x 418.1675770113179
      y 1077.3710517932818
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_344"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 183
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:refseq:NM_003136;urn:miriam:hgnc:11301;urn:miriam:ncbigene:6729;urn:miriam:ncbigene:6729;urn:miriam:ensembl:ENSG00000100883;urn:miriam:hgnc.symbol:SRP54;urn:miriam:hgnc.symbol:SRP54;urn:miriam:uniprot:P61011;urn:miriam:ec-code:3.6.5.-;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ncbigene:6728;urn:miriam:ncbigene:6728;urn:miriam:refseq:NM_003135;urn:miriam:hgnc.symbol:SRP19;urn:miriam:ensembl:ENSG00000153037;urn:miriam:hgnc.symbol:SRP19;urn:miriam:hgnc:11300;urn:miriam:uniprot:P09132"
      hgnc "HGNC_SYMBOL:SRP54;HGNC_SYMBOL:rep;HGNC_SYMBOL:SRP19"
      map_id "M115_20"
      name "SRP54comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa117"
      uniprot "UNIPROT:P61011;UNIPROT:P0DTD1;UNIPROT:P09132"
    ]
    graphics [
      x 467.7155340749299
      y 1120.2291556338696
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 184
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_210"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10189"
      uniprot "NA"
    ]
    graphics [
      x 953.7647227883449
      y 425.28934371165326
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_210"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 185
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:19235;urn:miriam:ensembl:ENSG00000275700;urn:miriam:uniprot:Q9NY61;urn:miriam:ncbigene:26574;urn:miriam:ncbigene:26574;urn:miriam:hgnc.symbol:AATF;urn:miriam:hgnc.symbol:AATF;urn:miriam:refseq:NM_012138"
      hgnc "HGNC_SYMBOL:AATF"
      map_id "M115_329"
      name "AATF"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1330"
      uniprot "UNIPROT:Q9NY61"
    ]
    graphics [
      x 1042.895696999196
      y 287.05884884881254
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_329"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 186
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:hgnc:19235;urn:miriam:ensembl:ENSG00000275700;urn:miriam:uniprot:Q9NY61;urn:miriam:ncbigene:26574;urn:miriam:ncbigene:26574;urn:miriam:hgnc.symbol:AATF;urn:miriam:hgnc.symbol:AATF;urn:miriam:refseq:NM_012138;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:AATF;HGNC_SYMBOL:rep"
      map_id "M115_10"
      name "AATFcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa107"
      uniprot "UNIPROT:Q9NY61;UNIPROT:P0DTD1"
    ]
    graphics [
      x 938.8578224011458
      y 239.08050120357188
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 187
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-;urn:miriam:uniprot:P0C6X7"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M115_366"
      name "Nsp14"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1426"
      uniprot "UNIPROT:P0C6X7"
    ]
    graphics [
      x 1705.383997596545
      y 745.9291586433708
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_366"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 188
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_226"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10205"
      uniprot "NA"
    ]
    graphics [
      x 1504.8083911786825
      y 767.0782474565729
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_226"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 189
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-;urn:miriam:uniprot:P0C6X7;urn:miriam:ncbigene:3615;urn:miriam:ncbigene:3615;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:ec-code:1.1.1.205;urn:miriam:uniprot:P12268;urn:miriam:refseq:NM_000884;urn:miriam:hgnc:6053;urn:miriam:ensembl:ENSG00000178035"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:IMPDH2"
      map_id "M115_27"
      name "INPDH2comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa123"
      uniprot "UNIPROT:P0C6X7;UNIPROT:P12268"
    ]
    graphics [
      x 1359.113696121332
      y 835.2825977624591
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 190
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:3176;urn:miriam:refseq:NM_001955;urn:miriam:uniprot:P05305;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:ensembl:ENSG00000078401"
      hgnc "HGNC_SYMBOL:EDN1"
      map_id "M115_254"
      name "EDN1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1074"
      uniprot "UNIPROT:P05305"
    ]
    graphics [
      x 1734.9552903681338
      y 844.3252844859506
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_254"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 191
    zlevel -1

    cd19dm [
      annotation "PUBMED:15568807"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_137"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10113"
      uniprot "NA"
    ]
    graphics [
      x 1507.8695346036147
      y 1104.797160684474
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_137"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 192
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:3176;urn:miriam:refseq:NM_001955;urn:miriam:uniprot:P05305;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:ensembl:ENSG00000078401"
      hgnc "HGNC_SYMBOL:EDN1"
      map_id "M115_238"
      name "EDN1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1003"
      uniprot "UNIPROT:P05305"
    ]
    graphics [
      x 1858.64404668306
      y 931.8964867798934
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_238"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 193
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:28514442;urn:miriam:hgnc:3176;urn:miriam:refseq:NM_001955;urn:miriam:uniprot:P05305;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:ensembl:ENSG00000078401"
      hgnc "HGNC_SYMBOL:EDN1"
      map_id "M115_51"
      name "EDN1_minus_homo"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa36"
      uniprot "UNIPROT:P05305"
    ]
    graphics [
      x 995.634315513608
      y 1433.5837882151823
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 194
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:P31153;urn:miriam:ec-code:2.5.1.6;urn:miriam:ensembl:ENSG00000168906;urn:miriam:hgnc:6904;urn:miriam:hgnc.symbol:MAT2A;urn:miriam:hgnc.symbol:MAT2A;urn:miriam:ncbigene:4144;urn:miriam:ncbigene:4144;urn:miriam:refseq:NM_005911"
      hgnc "HGNC_SYMBOL:MAT2A"
      map_id "M115_265"
      name "MAT2A"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1122"
      uniprot "UNIPROT:P31153"
    ]
    graphics [
      x 852.4929004408245
      y 1462.6351506969127
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_265"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 195
    zlevel -1

    cd19dm [
      annotation "PUBMED:12023972;PUBMED:12660248;PUBMED:11596649"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_155"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10132"
      uniprot "NA"
    ]
    graphics [
      x 656.1327943105455
      y 1293.4919954418606
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_155"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 196
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:34755"
      hgnc "NA"
      map_id "M115_273"
      name "S_minus_Adenosylmethionine"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1146"
      uniprot "NA"
    ]
    graphics [
      x 584.4121137486002
      y 1246.0368807048567
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_273"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 197
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:12023972;urn:miriam:pubmed:12660248;urn:miriam:pubmed:11596649;urn:miriam:pubchem.compound:34755;urn:miriam:uniprot:P31153;urn:miriam:ec-code:2.5.1.6;urn:miriam:ensembl:ENSG00000168906;urn:miriam:hgnc:6904;urn:miriam:hgnc.symbol:MAT2A;urn:miriam:hgnc.symbol:MAT2A;urn:miriam:ncbigene:4144;urn:miriam:ncbigene:4144;urn:miriam:refseq:NM_005911"
      hgnc "HGNC_SYMBOL:MAT2A"
      map_id "M115_70"
      name "SAdComp2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa55"
      uniprot "UNIPROT:P31153"
    ]
    graphics [
      x 667.1108349276095
      y 1148.4311374222402
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 198
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:doi:10.1126/science.abc1560;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ncbigene:8673700;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M115_79"
      name "Nsp7812"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa64"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 139.87428552293295
      y 647.3841096737148
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 199
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_166"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10143"
      uniprot "NA"
    ]
    graphics [
      x 144.57340958103248
      y 513.976338798059
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_166"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 200
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:doi:10.1126/science.abc1560"
      hgnc "NA"
      map_id "M115_287"
      name "virus_underscore_replication"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa1194"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 489.12749818793
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_287"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 201
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_233"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10212"
      uniprot "NA"
    ]
    graphics [
      x 1902.9607655711688
      y 712.618589326016
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_233"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 202
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000196150;urn:miriam:refseq:NM_021061;urn:miriam:hgnc.symbol:ZNF250;urn:miriam:hgnc.symbol:ZNF250;urn:miriam:ncbigene:58500;urn:miriam:ncbigene:58500;urn:miriam:uniprot:P15622;urn:miriam:hgnc:13044"
      hgnc "HGNC_SYMBOL:ZNF250"
      map_id "M115_360"
      name "ZNF250"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1415"
      uniprot "UNIPROT:P15622"
    ]
    graphics [
      x 1805.9877733183778
      y 661.4442975112809
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_360"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 203
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:doi:10.1101/2020.06.17.156455;urn:miriam:ensembl:ENSG00000196150;urn:miriam:refseq:NM_021061;urn:miriam:hgnc.symbol:ZNF250;urn:miriam:hgnc.symbol:ZNF250;urn:miriam:ncbigene:58500;urn:miriam:ncbigene:58500;urn:miriam:uniprot:P15622;urn:miriam:hgnc:13044;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-;urn:miriam:uniprot:P0C6X7"
      hgnc "HGNC_SYMBOL:ZNF250;HGNC_SYMBOL:rep"
      map_id "M115_33"
      name "ZNF250comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa129"
      uniprot "UNIPROT:P15622;UNIPROT:P0C6X7"
    ]
    graphics [
      x 2006.3697639862103
      y 680.2677502066239
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 204
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:5281078"
      hgnc "NA"
      map_id "M115_355"
      name "Mycophenolate_space_mofetil"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1401"
      uniprot "NA"
    ]
    graphics [
      x 885.4076440125818
      y 1243.488374423262
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_355"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 205
    zlevel -1

    cd19dm [
      annotation "PUBMED:15570183"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_228"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10207"
      uniprot "NA"
    ]
    graphics [
      x 989.736441270157
      y 1140.7409874744574
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_228"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 206
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:2.5.1.6;urn:miriam:ensembl:ENSG00000151224;urn:miriam:hgnc:6903;urn:miriam:hgnc.symbol:MAT1A;urn:miriam:hgnc.symbol:MAT1A;urn:miriam:ncbigene:4143;urn:miriam:ncbigene:4143;urn:miriam:uniprot:Q00266;urn:miriam:refseq:NM_000429"
      hgnc "HGNC_SYMBOL:MAT1A"
      map_id "M115_266"
      name "MAT1A"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1123"
      uniprot "UNIPROT:Q00266"
    ]
    graphics [
      x 915.2143028855295
      y 1470.0203154267856
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_266"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 207
    zlevel -1

    cd19dm [
      annotation "PUBMED:12060674;PUBMED:11301045;PUBMED:12660248;PUBMED:12631701"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_153"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10130"
      uniprot "NA"
    ]
    graphics [
      x 742.6596654246889
      y 1271.62282612586
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_153"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 208
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:12060674;urn:miriam:pubmed:11301045;urn:miriam:pubmed:12660248;urn:miriam:pubmed:12631701;urn:miriam:ec-code:2.5.1.6;urn:miriam:ensembl:ENSG00000151224;urn:miriam:hgnc:6903;urn:miriam:hgnc.symbol:MAT1A;urn:miriam:hgnc.symbol:MAT1A;urn:miriam:ncbigene:4143;urn:miriam:ncbigene:4143;urn:miriam:uniprot:Q00266;urn:miriam:refseq:NM_000429;urn:miriam:pubchem.compound:34755"
      hgnc "HGNC_SYMBOL:MAT1A"
      map_id "M115_68"
      name "SAdComp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa53"
      uniprot "UNIPROT:Q00266"
    ]
    graphics [
      x 826.4527280337197
      y 1083.7243339158242
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 209
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_174"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10151"
      uniprot "NA"
    ]
    graphics [
      x 1611.5191685233503
      y 494.1994642420286
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_174"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 210
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:ZC3H7A;urn:miriam:hgnc.symbol:ZC3H7A;urn:miriam:refseq:NM_014153;urn:miriam:uniprot:Q8IWR0;urn:miriam:ensembl:ENSG00000122299;urn:miriam:ncbigene:29066;urn:miriam:ncbigene:29066;urn:miriam:hgnc:30959"
      hgnc "HGNC_SYMBOL:ZC3H7A"
      map_id "M115_295"
      name "ZC3H7A_space_"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1216"
      uniprot "UNIPROT:Q8IWR0"
    ]
    graphics [
      x 1632.247251421291
      y 300.2965632247698
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_295"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 211
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:hgnc.symbol:ZC3H7A;urn:miriam:hgnc.symbol:ZC3H7A;urn:miriam:refseq:NM_014153;urn:miriam:uniprot:Q8IWR0;urn:miriam:ensembl:ENSG00000122299;urn:miriam:ncbigene:29066;urn:miriam:ncbigene:29066;urn:miriam:hgnc:30959"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:ZC3H7A"
      map_id "M115_87"
      name "ZC3H7Acomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa72"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q8IWR0"
    ]
    graphics [
      x 1594.8912351491738
      y 346.51816001785164
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 212
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_221"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10200"
      uniprot "NA"
    ]
    graphics [
      x 535.9668144642893
      y 1418.8725348597607
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_221"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 213
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:Q96CW1;urn:miriam:refseq:NM_004068;urn:miriam:hgnc.symbol:AP2M1;urn:miriam:hgnc.symbol:AP2M1;urn:miriam:ncbigene:1173;urn:miriam:ensembl:ENSG00000161203;urn:miriam:ncbigene:1173;urn:miriam:hgnc:564"
      hgnc "HGNC_SYMBOL:AP2M1"
      map_id "M115_348"
      name "AP2M1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1380"
      uniprot "UNIPROT:Q96CW1"
    ]
    graphics [
      x 412.51004364725065
      y 1331.382891671094
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_348"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 214
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:uniprot:Q96CW1;urn:miriam:refseq:NM_004068;urn:miriam:hgnc.symbol:AP2M1;urn:miriam:hgnc.symbol:AP2M1;urn:miriam:ncbigene:1173;urn:miriam:ensembl:ENSG00000161203;urn:miriam:ncbigene:1173;urn:miriam:hgnc:564;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:AP2M1;HGNC_SYMBOL:rep"
      map_id "M115_21"
      name "AP2M1comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa118"
      uniprot "UNIPROT:Q96CW1;UNIPROT:P0DTD1"
    ]
    graphics [
      x 397.52681626290314
      y 1419.0487742432238
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 215
    zlevel -1

    cd19dm [
      annotation "PUBMED:20837776"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_117"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10062"
      uniprot "NA"
    ]
    graphics [
      x 2069.950853023781
      y 716.5528273677712
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 216
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_206"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10185"
      uniprot "NA"
    ]
    graphics [
      x 499.6770731597959
      y 616.9699189510625
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_206"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 217
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:Q9H6F5;urn:miriam:hgnc.symbol:CCDC86;urn:miriam:hgnc.symbol:CCDC86;urn:miriam:refseq:NM_024098;urn:miriam:hgnc:28359;urn:miriam:ncbigene:79080;urn:miriam:ncbigene:79080;urn:miriam:ensembl:ENSG00000110104"
      hgnc "HGNC_SYMBOL:CCDC86"
      map_id "M115_324"
      name "CCDC86"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1315"
      uniprot "UNIPROT:Q9H6F5"
    ]
    graphics [
      x 347.30639576239
      y 529.5467120994018
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_324"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 218
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:uniprot:Q9H6F5;urn:miriam:hgnc.symbol:CCDC86;urn:miriam:hgnc.symbol:CCDC86;urn:miriam:refseq:NM_024098;urn:miriam:hgnc:28359;urn:miriam:ncbigene:79080;urn:miriam:ncbigene:79080;urn:miriam:ensembl:ENSG00000110104"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:CCDC86"
      map_id "M115_6"
      name "CCDCcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa103"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q9H6F5"
    ]
    graphics [
      x 394.52784103894965
      y 699.696290901673
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 219
    zlevel -1

    cd19dm [
      annotation "PUBMED:32405421"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_165"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10142"
      uniprot "NA"
    ]
    graphics [
      x 307.50720556834983
      y 773.2633983575496
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_165"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 220
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbigene:8673700"
      hgnc "NA"
      map_id "M115_286"
      name "RdRpassembled"
      node_subtype "RNA"
      node_type "species"
      org_id "sa1189"
      uniprot "NA"
    ]
    graphics [
      x 284.94401852404405
      y 656.2175251914575
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_286"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 221
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:121304016"
      hgnc "NA"
      map_id "M115_285"
      name "remdesivir_space_"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1188"
      uniprot "NA"
    ]
    graphics [
      x 433.0233329203602
      y 702.8623049222547
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_285"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 222
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32620147;urn:miriam:pubchem.compound:492405"
      hgnc "NA"
      map_id "M115_375"
      name "favipiravir"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1435"
      uniprot "NA"
    ]
    graphics [
      x 373.33632422350126
      y 859.7494922506271
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_375"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 223
    zlevel -1

    cd19dm [
      annotation "PUBMED:17016423;PUBMED:17139284"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_230"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10209"
      uniprot "NA"
    ]
    graphics [
      x 1442.2752712910808
      y 652.8172916964544
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_230"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 224
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:37542;urn:miriam:doi:10.1016/S0140-6736(20)31042-4"
      hgnc "NA"
      map_id "M115_357"
      name "Ribavirin"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1405"
      uniprot "NA"
    ]
    graphics [
      x 1359.8182603083608
      y 760.8383337241061
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_357"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 225
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:17139284;urn:miriam:ncbigene:3615;urn:miriam:ncbigene:3615;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:ec-code:1.1.1.205;urn:miriam:uniprot:P12268;urn:miriam:refseq:NM_000884;urn:miriam:hgnc:6053;urn:miriam:ensembl:ENSG00000178035;urn:miriam:pubchem.compound:37542;urn:miriam:doi:10.1016/S0140-6736(20)31042-4"
      hgnc "HGNC_SYMBOL:IMPDH2"
      map_id "M115_30"
      name "IRcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa126"
      uniprot "UNIPROT:P12268"
    ]
    graphics [
      x 1310.1987674851216
      y 714.5449707065998
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 226
    zlevel -1

    cd19dm [
      annotation "PUBMED:17496727"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_227"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10206"
      uniprot "NA"
    ]
    graphics [
      x 1519.3976953839426
      y 469.4888818879522
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_227"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 227
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:17496727;urn:miriam:ncbigene:3615;urn:miriam:ncbigene:3615;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:hgnc.symbol:IMPDH2;urn:miriam:ec-code:1.1.1.205;urn:miriam:uniprot:P12268;urn:miriam:refseq:NM_000884;urn:miriam:hgnc:6053;urn:miriam:ensembl:ENSG00000178035;urn:miriam:pubchem.compound:446541;urn:miriam:pubmed:17496727"
      hgnc "HGNC_SYMBOL:IMPDH2"
      map_id "M115_28"
      name "IMcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa124"
      uniprot "UNIPROT:P12268"
    ]
    graphics [
      x 1513.3025400763293
      y 308.58990505779605
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 228
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_144"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10120"
      uniprot "NA"
    ]
    graphics [
      x 1200.2586594521044
      y 583.5592714037759
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_144"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 229
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:hgnc:23589;urn:miriam:hgnc.symbol:ZNF503;urn:miriam:hgnc.symbol:ZNF503;urn:miriam:uniprot:Q96F45;urn:miriam:ensembl:ENSG00000165655;urn:miriam:refseq:NM_032772;urn:miriam:ncbigene:84858;urn:miriam:ncbigene:84858"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:ZNF503"
      map_id "M115_44"
      name "znfcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa27"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q96F45"
    ]
    graphics [
      x 1047.4728886849884
      y 542.3485118767687
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 230
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbigene:8021;urn:miriam:ncbigene:8021;urn:miriam:uniprot:P35658;urn:miriam:hgnc.symbol:NUP214;urn:miriam:hgnc.symbol:NUP214;urn:miriam:ensembl:ENSG00000126883;urn:miriam:hgnc:8064;urn:miriam:refseq:NM_005085"
      hgnc "HGNC_SYMBOL:NUP214"
      map_id "M115_244"
      name "NUP214"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1045"
      uniprot "UNIPROT:P35658"
    ]
    graphics [
      x 1773.1235526802875
      y 1550.3002261633524
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_244"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 231
    zlevel -1

    cd19dm [
      annotation "PUBMED:9049309"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_140"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10116"
      uniprot "NA"
    ]
    graphics [
      x 1930.0088917382445
      y 1615.8298998854916
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_140"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 232
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000108559;urn:miriam:refseq:NM_002532;urn:miriam:ncbigene:4927;urn:miriam:ncbigene:4927;urn:miriam:uniprot:Q99567;urn:miriam:hgnc.symbol:NUP88;urn:miriam:hgnc.symbol:NUP88;urn:miriam:pubmed:30543681;urn:miriam:hgnc:8067"
      hgnc "HGNC_SYMBOL:NUP88"
      map_id "M115_245"
      name "NUP88"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1046"
      uniprot "UNIPROT:Q99567"
    ]
    graphics [
      x 1878.0238741651447
      y 1436.2311444749585
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_245"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 233
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:9049309;urn:miriam:ensembl:ENSG00000108559;urn:miriam:refseq:NM_002532;urn:miriam:ncbigene:4927;urn:miriam:ncbigene:4927;urn:miriam:uniprot:Q99567;urn:miriam:hgnc.symbol:NUP88;urn:miriam:hgnc.symbol:NUP88;urn:miriam:pubmed:30543681;urn:miriam:hgnc:8067;urn:miriam:ncbigene:8021;urn:miriam:ncbigene:8021;urn:miriam:uniprot:P35658;urn:miriam:hgnc.symbol:NUP214;urn:miriam:hgnc.symbol:NUP214;urn:miriam:ensembl:ENSG00000126883;urn:miriam:hgnc:8064;urn:miriam:refseq:NM_005085"
      hgnc "HGNC_SYMBOL:NUP88;HGNC_SYMBOL:NUP214"
      map_id "M115_48"
      name "nup2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa31"
      uniprot "UNIPROT:Q99567;UNIPROT:P35658"
    ]
    graphics [
      x 1766.9184397266167
      y 1647.2558247847892
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 234
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_145"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10121"
      uniprot "NA"
    ]
    graphics [
      x 1592.965254772246
      y 700.9059950883984
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_145"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 235
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:uniprot:P61962;urn:miriam:pubmed:16887337;urn:miriam:ensembl:ENSG00000136485;urn:miriam:hgnc.symbol:DCAF7;urn:miriam:hgnc.symbol:DCAF7;urn:miriam:ncbigene:10238;urn:miriam:ncbigene:10238;urn:miriam:refseq:NM_005828;urn:miriam:hgnc:30915;urn:miriam:pubmed:16949367"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:DCAF7"
      map_id "M115_43"
      name "dcafcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa26"
      uniprot "UNIPROT:P0DTD1;UNIPROT:P61962"
    ]
    graphics [
      x 1512.0557331313016
      y 707.4135825104679
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 236
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_167"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10144"
      uniprot "NA"
    ]
    graphics [
      x 1123.026137295895
      y 845.5892711842146
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_167"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 237
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbigene:55206;urn:miriam:ncbigene:55206;urn:miriam:hgnc:22973;urn:miriam:hgnc.symbol:SBNO1;urn:miriam:hgnc.symbol:SBNO1;urn:miriam:ensembl:ENSG00000139697;urn:miriam:uniprot:A3KN83;urn:miriam:refseq:NM_018183"
      hgnc "HGNC_SYMBOL:SBNO1"
      map_id "M115_288"
      name "SBNO1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1195"
      uniprot "UNIPROT:A3KN83"
    ]
    graphics [
      x 888.0416231358009
      y 859.9188022325147
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_288"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 238
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ncbigene:55206;urn:miriam:ncbigene:55206;urn:miriam:hgnc:22973;urn:miriam:hgnc.symbol:SBNO1;urn:miriam:hgnc.symbol:SBNO1;urn:miriam:ensembl:ENSG00000139697;urn:miriam:uniprot:A3KN83;urn:miriam:refseq:NM_018183"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:SBNO1"
      map_id "M115_80"
      name "SBNOcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa65"
      uniprot "UNIPROT:P0DTD1;UNIPROT:A3KN83"
    ]
    graphics [
      x 952.2592454546241
      y 878.0562862219605
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 239
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_172"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10149"
      uniprot "NA"
    ]
    graphics [
      x 1666.8938011145126
      y 587.8819742268868
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_172"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 240
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:25617;urn:miriam:ensembl:ENSG00000089682;urn:miriam:refseq:NM_018301;urn:miriam:uniprot:Q96IZ5;urn:miriam:hgnc.symbol:RBM41;urn:miriam:ncbigene:55285;urn:miriam:hgnc.symbol:RBM41;urn:miriam:ncbigene:55285"
      hgnc "HGNC_SYMBOL:RBM41"
      map_id "M115_293"
      name "RBM41"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1210"
      uniprot "UNIPROT:Q96IZ5"
    ]
    graphics [
      x 1701.91714483722
      y 423.4738507546713
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_293"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 241
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:hgnc:25617;urn:miriam:ensembl:ENSG00000089682;urn:miriam:refseq:NM_018301;urn:miriam:uniprot:Q96IZ5;urn:miriam:hgnc.symbol:RBM41;urn:miriam:ncbigene:55285;urn:miriam:hgnc.symbol:RBM41;urn:miriam:ncbigene:55285"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:RBM41"
      map_id "M115_85"
      name "RBMcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa70"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q96IZ5"
    ]
    graphics [
      x 1764.092839955848
      y 496.9847134308606
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 242
    zlevel -1

    cd19dm [
      annotation "PUBMED:18486144;PUBMED:20196537"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_198"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10177"
      uniprot "NA"
    ]
    graphics [
      x 982.0016493179342
      y 2049.356921221949
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_198"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 243
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:34755"
      hgnc "NA"
      map_id "M115_318"
      name "Ademetionine"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1290"
      uniprot "NA"
    ]
    graphics [
      x 874.8501588792823
      y 1894.1509122690902
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_318"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 244
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:20196537;urn:miriam:ensembl:ENSG00000093010;urn:miriam:uniprot:P21964;urn:miriam:hgnc.symbol:COMT;urn:miriam:hgnc.symbol:COMT;urn:miriam:hgnc:2228;urn:miriam:ec-code:2.1.1.6;urn:miriam:refseq:NM_000754;urn:miriam:ncbigene:1312;urn:miriam:ncbigene:1312;urn:miriam:pubchem.compound:34755"
      hgnc "HGNC_SYMBOL:COMT"
      map_id "M115_111"
      name "ACcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa95"
      uniprot "UNIPROT:P21964"
    ]
    graphics [
      x 811.941654595393
      y 1924.8226335428585
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 245
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_148"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10124"
      uniprot "NA"
    ]
    graphics [
      x 876.7193249660747
      y 1165.7760752470847
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_148"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 246
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:Q8TD19;urn:miriam:ec-code:2.7.11.1;urn:miriam:ncbigene:91754;urn:miriam:ncbigene:91754;urn:miriam:hgnc:18591;urn:miriam:refseq:NM_033116;urn:miriam:hgnc.symbol:NEK9;urn:miriam:hgnc.symbol:NEK9;urn:miriam:ensembl:ENSG00000119638"
      hgnc "HGNC_SYMBOL:NEK9"
      map_id "M115_381"
      name "NEK9"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa979"
      uniprot "UNIPROT:Q8TD19"
    ]
    graphics [
      x 701.2160950217706
      y 1381.7999784233766
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_381"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 247
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:uniprot:Q8TD19;urn:miriam:ec-code:2.7.11.1;urn:miriam:ncbigene:91754;urn:miriam:ncbigene:91754;urn:miriam:hgnc:18591;urn:miriam:refseq:NM_033116;urn:miriam:hgnc.symbol:NEK9;urn:miriam:hgnc.symbol:NEK9;urn:miriam:ensembl:ENSG00000119638;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:NEK9;HGNC_SYMBOL:rep"
      map_id "M115_39"
      name "nek9comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa18"
      uniprot "UNIPROT:Q8TD19;UNIPROT:P0DTD1"
    ]
    graphics [
      x 807.3592347248198
      y 1276.7092175218258
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 248
    zlevel -1

    cd19dm [
      annotation "PUBMED:17016423;PUBMED:17139284"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_156"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10133"
      uniprot "NA"
    ]
    graphics [
      x 1223.582028953913
      y 1421.2335909505666
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_156"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 249
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:INS;urn:miriam:ncbigene:3630;urn:miriam:uniprot:P01308"
      hgnc "HGNC_SYMBOL:INS"
      map_id "M115_276"
      name "Insulin"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1160"
      uniprot "UNIPROT:P01308"
    ]
    graphics [
      x 1212.369575676843
      y 1537.6374161035847
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_276"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 250
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:17016423;urn:miriam:pubmed:17139284;urn:miriam:hgnc.symbol:INS;urn:miriam:ncbigene:3630;urn:miriam:uniprot:P01308;urn:miriam:hgnc:8743;urn:miriam:uniprot:P29120;urn:miriam:uniprot:P29120;urn:miriam:hgnc.symbol:PCSK1;urn:miriam:hgnc.symbol:PCSK1;urn:miriam:ensembl:ENSG00000175426;urn:miriam:ec-code:3.4.21.93;urn:miriam:ncbigene:5122;urn:miriam:refseq:NM_000439;urn:miriam:ncbigene:5122"
      hgnc "HGNC_SYMBOL:INS;HGNC_SYMBOL:PCSK1"
      map_id "M115_71"
      name "NECINsComp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa56"
      uniprot "UNIPROT:P01308;UNIPROT:P29120"
    ]
    graphics [
      x 1157.2941857927499
      y 1491.3133487272041
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 251
    zlevel -1

    cd19dm [
      annotation "PUBMED:16713569"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_133"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10109"
      uniprot "NA"
    ]
    graphics [
      x 1320.9729293850532
      y 1735.7840418994329
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_133"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 252
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:UBQLN4;urn:miriam:hgnc.symbol:UBQLN4;urn:miriam:refseq:NM_020131;urn:miriam:uniprot:Q9NRR5;urn:miriam:hgnc:1237;urn:miriam:ncbigene:56893;urn:miriam:ncbigene:56893;urn:miriam:ensembl:ENSG00000160803"
      hgnc "HGNC_SYMBOL:UBQLN4"
      map_id "M115_263"
      name "UBQLN4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1108"
      uniprot "UNIPROT:Q9NRR5"
    ]
    graphics [
      x 1346.4890905262312
      y 1877.5885560580334
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_263"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 253
    zlevel -1

    cd19dm [
      annotation "PUBMED:20811346;PUBMED:19601701;PUBMED:19389876;PUBMED:19920913"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_160"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10137"
      uniprot "NA"
    ]
    graphics [
      x 1117.32923724048
      y 259.330651932082
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_160"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 254
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:6918493"
      hgnc "NA"
      map_id "M115_280"
      name "Ambrisentan"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1172"
      uniprot "NA"
    ]
    graphics [
      x 1242.7064924688061
      y 355.2316441268541
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_280"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 255
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:20811346;urn:miriam:pubchem.compound:6918493;urn:miriam:refseq:NM_001166055;urn:miriam:hgnc:3179;urn:miriam:uniprot:P25101;urn:miriam:ensembl:ENSG00000151617;urn:miriam:ncbigene:1909;urn:miriam:ncbigene:1909;urn:miriam:hgnc.symbol:EDNRA;urn:miriam:hgnc.symbol:EDNRA"
      hgnc "HGNC_SYMBOL:EDNRA"
      map_id "M115_75"
      name "EDNRAmbComp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa60"
      uniprot "UNIPROT:P25101"
    ]
    graphics [
      x 1196.9873964857554
      y 121.6263243801219
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 256
    zlevel -1

    cd19dm [
      annotation "PUBMED:1170911"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_200"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10179"
      uniprot "NA"
    ]
    graphics [
      x 1066.0929594195627
      y 2128.038019183112
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_200"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 257
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:66414"
      hgnc "NA"
      map_id "M115_320"
      name "2_minus_Methoxyestradiol"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1296"
      uniprot "NA"
    ]
    graphics [
      x 958.7463047663105
      y 1944.655299220541
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_320"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 258
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:1170911;urn:miriam:ensembl:ENSG00000093010;urn:miriam:uniprot:P21964;urn:miriam:hgnc.symbol:COMT;urn:miriam:hgnc.symbol:COMT;urn:miriam:hgnc:2228;urn:miriam:ec-code:2.1.1.6;urn:miriam:refseq:NM_000754;urn:miriam:ncbigene:1312;urn:miriam:ncbigene:1312;urn:miriam:pubchem.compound:66414"
      hgnc "HGNC_SYMBOL:COMT"
      map_id "M115_113"
      name "MCcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa97"
      uniprot "UNIPROT:P21964"
    ]
    graphics [
      x 943.4391154629662
      y 2197.4002062254854
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 259
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_178"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10156"
      uniprot "NA"
    ]
    graphics [
      x 1137.9380694803087
      y 1041.5860163811901
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_178"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 260
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:doi:10.1101/2020.06.17.156455;urn:miriam:hgnc:1371;urn:miriam:uniprot:O43570;urn:miriam:ncbigene:771;urn:miriam:refseq:NM_001218;urn:miriam:ncbigene:771;urn:miriam:ec-code:4.2.1.1;urn:miriam:hgnc.symbol:CA12;urn:miriam:hgnc.symbol:CA12;urn:miriam:ensembl:ENSG00000074410;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:CA12;HGNC_SYMBOL:rep"
      map_id "M115_91"
      name "CA12comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa76"
      uniprot "UNIPROT:O43570;UNIPROT:P0DTD1"
    ]
    graphics [
      x 1078.3292704053536
      y 1128.1456863945539
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 261
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000006451;urn:miriam:hgnc.symbol:RALA;urn:miriam:hgnc.symbol:RALA;urn:miriam:refseq:NM_005402;urn:miriam:ec-code:3.6.5.2;urn:miriam:uniprot:P11233;urn:miriam:hgnc:9839;urn:miriam:ncbigene:5898;urn:miriam:ncbigene:5898"
      hgnc "HGNC_SYMBOL:RALA"
      map_id "M115_311"
      name "RALA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1270"
      uniprot "UNIPROT:P11233"
    ]
    graphics [
      x 1784.4281829598604
      y 1066.4495041182895
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_311"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 262
    zlevel -1

    cd19dm [
      annotation "PUBMED:10592235"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_192"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10171"
      uniprot "NA"
    ]
    graphics [
      x 1981.6247485186566
      y 858.5768693340102
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_192"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 263
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:10592235;urn:miriam:obo.chebi:CHEBI%3A17552;urn:miriam:pubchem.compound:135398619;urn:miriam:ensembl:ENSG00000006451;urn:miriam:hgnc.symbol:RALA;urn:miriam:hgnc.symbol:RALA;urn:miriam:refseq:NM_005402;urn:miriam:ec-code:3.6.5.2;urn:miriam:uniprot:P11233;urn:miriam:hgnc:9839;urn:miriam:ncbigene:5898;urn:miriam:ncbigene:5898"
      hgnc "HGNC_SYMBOL:RALA"
      map_id "M115_106"
      name "GDPcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa90"
      uniprot "UNIPROT:P11233"
    ]
    graphics [
      x 2069.627927826231
      y 955.8708591261096
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 264
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:Q7Z3B4;urn:miriam:refseq:NM_001278603;urn:miriam:ensembl:ENSG00000138750;urn:miriam:hgnc:17359;urn:miriam:hgnc.symbol:NUP54;urn:miriam:hgnc.symbol:NUP54;urn:miriam:ncbigene:53371;urn:miriam:ncbigene:53371"
      hgnc "HGNC_SYMBOL:NUP54"
      map_id "M115_242"
      name "NUP54"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1043"
      uniprot "UNIPROT:Q7Z3B4"
    ]
    graphics [
      x 1258.776344263344
      y 1301.660618180612
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_242"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 265
    zlevel -1

    cd19dm [
      annotation "PUBMED:12196509"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_118"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10081"
      uniprot "NA"
    ]
    graphics [
      x 1361.6524889048317
      y 1378.4828277333406
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_118"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 266
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:Q9BVL2;urn:miriam:ensembl:ENSG00000139496;urn:miriam:hgnc.symbol:NUP58;urn:miriam:hgnc.symbol:NUP58;urn:miriam:ncbigene:9818;urn:miriam:ncbigene:9818;urn:miriam:refseq:NM_001008564;urn:miriam:hgnc:20261"
      hgnc "HGNC_SYMBOL:NUP58"
      map_id "M115_243"
      name "NUP58"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1044"
      uniprot "UNIPROT:Q9BVL2"
    ]
    graphics [
      x 1467.0916965565928
      y 1315.9342666807615
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_243"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 267
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000213024;urn:miriam:refseq:NM_153719;urn:miriam:uniprot:P37198;urn:miriam:ncbigene:23636;urn:miriam:ncbigene:23636;urn:miriam:hgnc.symbol:NUP62;urn:miriam:hgnc.symbol:NUP62;urn:miriam:hgnc:8066"
      hgnc "HGNC_SYMBOL:NUP62"
      map_id "M115_241"
      name "NUP62"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1042"
      uniprot "UNIPROT:P37198"
    ]
    graphics [
      x 1294.5123032143576
      y 1258.3149207063066
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_241"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 268
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:12196509;urn:miriam:uniprot:Q9BVL2;urn:miriam:ensembl:ENSG00000139496;urn:miriam:hgnc.symbol:NUP58;urn:miriam:hgnc.symbol:NUP58;urn:miriam:ncbigene:9818;urn:miriam:ncbigene:9818;urn:miriam:refseq:NM_001008564;urn:miriam:hgnc:20261;urn:miriam:uniprot:Q7Z3B4;urn:miriam:refseq:NM_001278603;urn:miriam:ensembl:ENSG00000138750;urn:miriam:hgnc:17359;urn:miriam:hgnc.symbol:NUP54;urn:miriam:hgnc.symbol:NUP54;urn:miriam:ncbigene:53371;urn:miriam:ncbigene:53371;urn:miriam:ensembl:ENSG00000213024;urn:miriam:refseq:NM_153719;urn:miriam:uniprot:P37198;urn:miriam:ncbigene:23636;urn:miriam:ncbigene:23636;urn:miriam:hgnc.symbol:NUP62;urn:miriam:hgnc.symbol:NUP62;urn:miriam:hgnc:8066"
      hgnc "HGNC_SYMBOL:NUP58;HGNC_SYMBOL:NUP54;HGNC_SYMBOL:NUP62"
      map_id "M115_47"
      name "nup1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa30"
      uniprot "UNIPROT:Q9BVL2;UNIPROT:Q7Z3B4;UNIPROT:P37198"
    ]
    graphics [
      x 1392.5466841725993
      y 1582.5211275865781
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 269
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_184"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10163"
      uniprot "NA"
    ]
    graphics [
      x 1953.4292889457065
      y 968.1281385578869
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_184"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 270
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:refseq:NM_004457;urn:miriam:ec-code:6.2.1.3;urn:miriam:ncbigene:2181;urn:miriam:ncbigene:2181;urn:miriam:uniprot:O95573;urn:miriam:uniprot:O95573;urn:miriam:ec-code:6.2.1.15;urn:miriam:ensembl:ENSG00000123983;urn:miriam:hgnc.symbol:ACSL3;urn:miriam:hgnc.symbol:ACSL3;urn:miriam:hgnc:3570"
      hgnc "HGNC_SYMBOL:ACSL3"
      map_id "M115_305"
      name "ACSL3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1247"
      uniprot "UNIPROT:O95573"
    ]
    graphics [
      x 1820.4460479713237
      y 962.2176743086169
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_305"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 271
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:refseq:NM_004457;urn:miriam:ec-code:6.2.1.3;urn:miriam:ncbigene:2181;urn:miriam:ncbigene:2181;urn:miriam:uniprot:O95573;urn:miriam:uniprot:O95573;urn:miriam:ec-code:6.2.1.15;urn:miriam:ensembl:ENSG00000123983;urn:miriam:hgnc.symbol:ACSL3;urn:miriam:hgnc.symbol:ACSL3;urn:miriam:hgnc:3570"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:ACSL3"
      map_id "M115_98"
      name "ACSLcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa82"
      uniprot "UNIPROT:P0DTD1;UNIPROT:O95573"
    ]
    graphics [
      x 1877.6504379481798
      y 789.182163623771
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 272
    zlevel -1

    cd19dm [
      annotation "PUBMED:8934526"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_138"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10114"
      uniprot "NA"
    ]
    graphics [
      x 1026.284255894683
      y 1191.0257761727041
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_138"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 273
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:2.6.1.5;urn:miriam:hgnc:11573;urn:miriam:hgnc.symbol:TAT;urn:miriam:hgnc.symbol:TAT;urn:miriam:hgnc.symbol:tat;urn:miriam:ncbigene:6898;urn:miriam:ensembl:ENSG00000198650;urn:miriam:ncbigene:6898;urn:miriam:uniprot:A6MI22;urn:miriam:refseq:NM_000353;urn:miriam:uniprot:P17735;urn:miriam:uniprot:P17735;urn:miriam:taxonomy:11676"
      hgnc "HGNC_SYMBOL:TAT;HGNC_SYMBOL:tat"
      map_id "M115_250"
      name "TAT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1063"
      uniprot "UNIPROT:A6MI22;UNIPROT:P17735"
    ]
    graphics [
      x 1104.0810545515455
      y 1351.9467529668846
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_250"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 274
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:8934526;urn:miriam:ec-code:2.6.1.5;urn:miriam:hgnc:11573;urn:miriam:hgnc.symbol:TAT;urn:miriam:hgnc.symbol:TAT;urn:miriam:hgnc.symbol:tat;urn:miriam:ncbigene:6898;urn:miriam:ensembl:ENSG00000198650;urn:miriam:ncbigene:6898;urn:miriam:uniprot:A6MI22;urn:miriam:refseq:NM_000353;urn:miriam:uniprot:P17735;urn:miriam:uniprot:P17735;urn:miriam:taxonomy:11676;urn:miriam:pubmed:10428810;urn:miriam:ncbigene:2963;urn:miriam:refseq:NM_004128;urn:miriam:ncbigene:2963;urn:miriam:hgnc:4653;urn:miriam:uniprot:P13984;urn:miriam:ec-code:3.6.4.12;urn:miriam:hgnc.symbol:GTF2F2;urn:miriam:hgnc.symbol:GTF2F2;urn:miriam:ensembl:ENSG00000188342"
      hgnc "HGNC_SYMBOL:TAT;HGNC_SYMBOL:tat;HGNC_SYMBOL:GTF2F2"
      map_id "M115_49"
      name "TAT_minus_HIV"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa33"
      uniprot "UNIPROT:A6MI22;UNIPROT:P17735;UNIPROT:P13984"
    ]
    graphics [
      x 1168.2525962949148
      y 1280.3802206824503
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 275
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_215"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10194"
      uniprot "NA"
    ]
    graphics [
      x 612.7114108980542
      y 1002.3784348684085
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_215"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 276
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:doi:10.1101/2020.03.31.019216;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:hgnc:26274;urn:miriam:uniprot:Q96I59;urn:miriam:hgnc.symbol:NARS2;urn:miriam:ensembl:ENSG00000137513;urn:miriam:hgnc.symbol:NARS2;urn:miriam:ncbigene:79731;urn:miriam:ncbigene:79731;urn:miriam:refseq:NM_024678;urn:miriam:ec-code:6.1.1.22"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:NARS2"
      map_id "M115_15"
      name "NARS2comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa112"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q96I59"
    ]
    graphics [
      x 503.72516112575386
      y 1056.9186394626818
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 277
    zlevel -1

    cd19dm [
      annotation "PUBMED:10727528"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_162"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10139"
      uniprot "NA"
    ]
    graphics [
      x 714.3136709584611
      y 703.1072448095648
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_162"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 278
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:2244"
      hgnc "NA"
      map_id "M115_282"
      name "Acetylsalicylic_space_acid"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1180"
      uniprot "NA"
    ]
    graphics [
      x 597.1552497964797
      y 789.3134004552296
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_282"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 279
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:10727528;urn:miriam:refseq:NM_001166055;urn:miriam:hgnc:3179;urn:miriam:uniprot:P25101;urn:miriam:ensembl:ENSG00000151617;urn:miriam:ncbigene:1909;urn:miriam:ncbigene:1909;urn:miriam:hgnc.symbol:EDNRA;urn:miriam:hgnc.symbol:EDNRA;urn:miriam:pubchem.compound:2244"
      hgnc "HGNC_SYMBOL:EDNRA"
      map_id "M115_77"
      name "EDNRAcetComp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa62"
      uniprot "UNIPROT:P25101"
    ]
    graphics [
      x 569.5071924565323
      y 708.313239192893
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 280
    zlevel -1

    cd19dm [
      annotation "PUBMED:19153232"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_186"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10165"
      uniprot "NA"
    ]
    graphics [
      x 1541.495287995225
      y 1631.1885986188854
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_186"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 281
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:19153232"
      hgnc "NA"
      map_id "M115_306"
      name "ssRNAoligo"
      node_subtype "RNA"
      node_type "species"
      org_id "sa1253"
      uniprot "NA"
    ]
    graphics [
      x 1634.576334842247
      y 1693.8030302648208
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_306"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 282
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:19153232;urn:miriam:pubmed:19153232;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M115_100"
      name "RNArecognition"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa84"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1418.4187450241293
      y 1711.76814647038
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 283
    zlevel -1

    cd19dm [
      annotation "PUBMED:17016423;PUBMED:17139284;PUBMED:10592235"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_158"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10135"
      uniprot "NA"
    ]
    graphics [
      x 1188.7366180957738
      y 1383.4319029171402
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_158"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 284
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:643975"
      hgnc "NA"
      map_id "M115_278"
      name "Flavin_space_adenine_space_dinucleotide"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1166"
      uniprot "NA"
    ]
    graphics [
      x 1080.9774627733611
      y 1297.7035224892538
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_278"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 285
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:17016423;urn:miriam:pubmed:17139284;urn:miriam:pubmed:10592235;urn:miriam:hgnc.symbol:AIFM1;urn:miriam:hgnc.symbol:AIFM1;urn:miriam:hgnc:8768;urn:miriam:ncbigene:9131;urn:miriam:ncbigene:9131;urn:miriam:ec-code:1.6.99.-;urn:miriam:uniprot:O95831;urn:miriam:ensembl:ENSG00000156709;urn:miriam:refseq:NM_001130846;urn:miriam:pubchem.compound:643975"
      hgnc "HGNC_SYMBOL:AIFM1"
      map_id "M115_73"
      name "AIFMFlaComp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa58"
      uniprot "UNIPROT:O95831"
    ]
    graphics [
      x 1203.1920298518737
      y 1239.1338458377172
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 286
    zlevel -1

    cd19dm [
      annotation "PUBMED:21516116;PUBMED:12671891;PUBMED:26186194;PUBMED:28514442;PUBMED:27548429;PUBMED:25416956;PUBMED:22863883"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_125"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10097"
      uniprot "NA"
    ]
    graphics [
      x 1123.6058515283055
      y 1579.3031655098212
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_125"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 287
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:12671891;urn:miriam:uniprot:P31153;urn:miriam:ec-code:2.5.1.6;urn:miriam:ensembl:ENSG00000168906;urn:miriam:hgnc:6904;urn:miriam:hgnc.symbol:MAT2A;urn:miriam:hgnc.symbol:MAT2A;urn:miriam:ncbigene:4144;urn:miriam:ncbigene:4144;urn:miriam:refseq:NM_005911;urn:miriam:ec-code:2.5.1.6;urn:miriam:ensembl:ENSG00000151224;urn:miriam:hgnc:6903;urn:miriam:hgnc.symbol:MAT1A;urn:miriam:hgnc.symbol:MAT1A;urn:miriam:ncbigene:4143;urn:miriam:ncbigene:4143;urn:miriam:uniprot:Q00266;urn:miriam:refseq:NM_000429;urn:miriam:pubmed:10644686;urn:miriam:pubmed:23425511;urn:miriam:pubmed:25075345;urn:miriam:ncbigene:27430;urn:miriam:ncbigene:27430;urn:miriam:uniprot:Q9NZL9;urn:miriam:hgnc.symbol:MAT2B;urn:miriam:hgnc.symbol:MAT2B;urn:miriam:pubmed:23189196;urn:miriam:refseq:NM_013283;urn:miriam:ensembl:ENSG00000038274;urn:miriam:hgnc:6905"
      hgnc "HGNC_SYMBOL:MAT2A;HGNC_SYMBOL:MAT1A;HGNC_SYMBOL:MAT2B"
      map_id "M115_62"
      name "MAT"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa47"
      uniprot "UNIPROT:P31153;UNIPROT:Q00266;UNIPROT:Q9NZL9"
    ]
    graphics [
      x 1226.7990245069439
      y 1708.9233254320477
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 288
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M115_363"
      name "Nsp10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1422"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 521.6223801655025
      y 545.3874312990712
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_363"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 289
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_223"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10202"
      uniprot "NA"
    ]
    graphics [
      x 442.94214740336577
      y 418.8707582243052
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_223"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 290
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc:4236;urn:miriam:ensembl:ENSG00000127554;urn:miriam:uniprot:P55789;urn:miriam:ncbigene:2671;urn:miriam:refseq:NM_005262;urn:miriam:ncbigene:2671;urn:miriam:ec-code:1.8.3.2;urn:miriam:hgnc.symbol:GFER;urn:miriam:hgnc.symbol:GFER"
      hgnc "HGNC_SYMBOL:GFER"
      map_id "M115_350"
      name "GFER"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1386"
      uniprot "UNIPROT:P55789"
    ]
    graphics [
      x 414.70847052499016
      y 291.3194266185609
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_350"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 291
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:hgnc:4236;urn:miriam:ensembl:ENSG00000127554;urn:miriam:uniprot:P55789;urn:miriam:ncbigene:2671;urn:miriam:refseq:NM_005262;urn:miriam:ncbigene:2671;urn:miriam:ec-code:1.8.3.2;urn:miriam:hgnc.symbol:GFER;urn:miriam:hgnc.symbol:GFER"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:GFER"
      map_id "M115_24"
      name "GFERcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa120"
      uniprot "UNIPROT:P0DTD1;UNIPROT:P55789"
    ]
    graphics [
      x 571.820671622235
      y 519.748502106692
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 292
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_170"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10147"
      uniprot "NA"
    ]
    graphics [
      x 1426.4470548084037
      y 825.1353688112952
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_170"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 293
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:refseq:NM_014345;urn:miriam:uniprot:Q5VUA4;urn:miriam:hgnc.symbol:ZNF318;urn:miriam:hgnc.symbol:ZNF318;urn:miriam:hgnc:13578;urn:miriam:ensembl:ENSG00000171467;urn:miriam:ncbigene:24149;urn:miriam:ncbigene:24149"
      hgnc "HGNC_SYMBOL:ZNF318"
      map_id "M115_291"
      name "ZNF318"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1204"
      uniprot "UNIPROT:Q5VUA4"
    ]
    graphics [
      x 1302.2682655759036
      y 879.6270346222207
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_291"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 294
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:refseq:NM_014345;urn:miriam:uniprot:Q5VUA4;urn:miriam:hgnc.symbol:ZNF318;urn:miriam:hgnc.symbol:ZNF318;urn:miriam:hgnc:13578;urn:miriam:ensembl:ENSG00000171467;urn:miriam:ncbigene:24149;urn:miriam:ncbigene:24149;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:ZNF318;HGNC_SYMBOL:rep"
      map_id "M115_83"
      name "ZNFcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa68"
      uniprot "UNIPROT:Q5VUA4;UNIPROT:P0DTD1"
    ]
    graphics [
      x 1381.0638617838404
      y 946.4622656544987
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 295
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_175"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10152"
      uniprot "NA"
    ]
    graphics [
      x 1662.437412198708
      y 943.5547564669853
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_175"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 296
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:2.3.2.33;urn:miriam:uniprot:O75592;urn:miriam:refseq:NM_015057;urn:miriam:ncbigene:23077;urn:miriam:ncbigene:23077;urn:miriam:hgnc.symbol:MYCBP2;urn:miriam:ensembl:ENSG00000005810;urn:miriam:hgnc.symbol:MYCBP2;urn:miriam:hgnc:23386"
      hgnc "HGNC_SYMBOL:MYCBP2"
      map_id "M115_296"
      name "MYCBP2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1219"
      uniprot "UNIPROT:O75592"
    ]
    graphics [
      x 1715.1083611340498
      y 1073.201474325413
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_296"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 297
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ec-code:2.3.2.33;urn:miriam:uniprot:O75592;urn:miriam:refseq:NM_015057;urn:miriam:ncbigene:23077;urn:miriam:ncbigene:23077;urn:miriam:hgnc.symbol:MYCBP2;urn:miriam:ensembl:ENSG00000005810;urn:miriam:hgnc.symbol:MYCBP2;urn:miriam:hgnc:23386"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:MYCBP2"
      map_id "M115_88"
      name "MYCBPcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa73"
      uniprot "UNIPROT:P0DTD1;UNIPROT:O75592"
    ]
    graphics [
      x 1635.6045687537408
      y 1111.5974096818945
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 298
    zlevel -1

    cd19dm [
      annotation "PUBMED:16169070"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_134"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10110"
      uniprot "NA"
    ]
    graphics [
      x 735.0309433580771
      y 1100.3526746389473
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_134"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 299
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:16169070;urn:miriam:hgnc:3176;urn:miriam:refseq:NM_001955;urn:miriam:uniprot:P05305;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:ensembl:ENSG00000078401;urn:miriam:ensembl:ENSG00000168090;urn:miriam:refseq:NM_006833;urn:miriam:ncbigene:10980;urn:miriam:ncbigene:10980;urn:miriam:hgnc.symbol:COPS6;urn:miriam:hgnc.symbol:COPS6;urn:miriam:uniprot:Q7L5N1;urn:miriam:hgnc:21749"
      hgnc "HGNC_SYMBOL:EDN1;HGNC_SYMBOL:COPS6"
      map_id "M115_57"
      name "EDN1_minus_homo"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa42"
      uniprot "UNIPROT:P05305;UNIPROT:Q7L5N1"
    ]
    graphics [
      x 860.7384952383053
      y 1052.4379362505615
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 300
    zlevel -1

    cd19dm [
      annotation "PUBMED:17341833;PUBMED:17040106;PUBMED:17401193;PUBMED:17082011;PUBMED:16814740"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_189"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10168"
      uniprot "NA"
    ]
    graphics [
      x 1230.3909055265822
      y 1646.6911306547815
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_189"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 301
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16908;urn:miriam:pubchem.compound:439153"
      hgnc "NA"
      map_id "M115_309"
      name "NADH"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa1264"
      uniprot "NA"
    ]
    graphics [
      x 1118.310210634115
      y 1765.824899311061
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_309"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 302
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:17341833;urn:miriam:obo.chebi:CHEBI%3A16908;urn:miriam:pubchem.compound:439153;urn:miriam:refseq:NM_000398;urn:miriam:ensembl:ENSG00000100243;urn:miriam:hgnc:2873;urn:miriam:uniprot:P00387;urn:miriam:ncbigene:1727;urn:miriam:ncbigene:1727;urn:miriam:hgnc.symbol:CYB5R3;urn:miriam:hgnc.symbol:CYB5R3;urn:miriam:ec-code:1.6.2.2"
      hgnc "HGNC_SYMBOL:CYB5R3"
      map_id "M115_103"
      name "NADHcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa87"
      uniprot "UNIPROT:P00387"
    ]
    graphics [
      x 1071.773741836769
      y 1718.1714065673327
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 303
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_177"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10155"
      uniprot "NA"
    ]
    graphics [
      x 1872.8298067041717
      y 1288.70790661163
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_177"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 304
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:uniprot:Q8WTV0;urn:miriam:hgnc:1664;urn:miriam:ensembl:ENSG00000073060;urn:miriam:hgnc.symbol:SCARB1;urn:miriam:hgnc.symbol:SCARB1;urn:miriam:ncbigene:949;urn:miriam:ncbigene:949;urn:miriam:refseq:NM_005505;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:SCARB1;HGNC_SYMBOL:rep"
      map_id "M115_90"
      name "SCARB1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa75"
      uniprot "UNIPROT:Q8WTV0;UNIPROT:P0DTD1"
    ]
    graphics [
      x 1718.1845456325836
      y 1310.439842764794
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 305
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_201"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10180"
      uniprot "NA"
    ]
    graphics [
      x 1858.5200805931643
      y 1177.3212668984763
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_201"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 306
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:hgnc:9788;urn:miriam:hgnc.symbol:RAB7A;urn:miriam:hgnc.symbol:RAB7A;urn:miriam:uniprot:P51149;urn:miriam:ncbigene:7879;urn:miriam:refseq:NM_004637;urn:miriam:ncbigene:7879;urn:miriam:ensembl:ENSG00000075785;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:RAB7A;HGNC_SYMBOL:rep"
      map_id "M115_114"
      name "RAB7comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa98"
      uniprot "UNIPROT:P51149;UNIPROT:P0DTD1"
    ]
    graphics [
      x 1934.8671696362085
      y 1242.5320665017098
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 307
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:FOCAD;urn:miriam:hgnc.symbol:FOCAD;urn:miriam:uniprot:Q5VW36;urn:miriam:refseq:NM_017794;urn:miriam:hgnc:23377;urn:miriam:ensembl:ENSG00000188352;urn:miriam:ncbigene:54914;urn:miriam:ncbigene:54914"
      hgnc "HGNC_SYMBOL:FOCAD"
      map_id "M115_325"
      name "FOCAD"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1318"
      uniprot "UNIPROT:Q5VW36"
    ]
    graphics [
      x 1341.9701155133714
      y 1009.707695141578
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_325"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 308
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_207"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10186"
      uniprot "NA"
    ]
    graphics [
      x 1165.882274410498
      y 903.7151838528321
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_207"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 309
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:doi:10.1101/2020.06.17.156455;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:hgnc.symbol:FOCAD;urn:miriam:hgnc.symbol:FOCAD;urn:miriam:uniprot:Q5VW36;urn:miriam:refseq:NM_017794;urn:miriam:hgnc:23377;urn:miriam:ensembl:ENSG00000188352;urn:miriam:ncbigene:54914;urn:miriam:ncbigene:54914;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:FOCAD"
      map_id "M115_7"
      name "FOCADcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa104"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q5VW36"
    ]
    graphics [
      x 1288.954262938367
      y 1031.2596591492047
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 310
    zlevel -1

    cd19dm [
      annotation "PUBMED:20185318"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_180"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10158"
      uniprot "NA"
    ]
    graphics [
      x 1021.4643873362108
      y 761.4721516352606
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_180"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 311
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:5281855"
      hgnc "NA"
      map_id "M115_301"
      name "Ellagic_space_Acid"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1234"
      uniprot "NA"
    ]
    graphics [
      x 910.3144910837258
      y 631.8722539472883
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_301"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 312
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:20185318;urn:miriam:hgnc:1371;urn:miriam:uniprot:O43570;urn:miriam:ncbigene:771;urn:miriam:refseq:NM_001218;urn:miriam:ncbigene:771;urn:miriam:ec-code:4.2.1.1;urn:miriam:hgnc.symbol:CA12;urn:miriam:hgnc.symbol:CA12;urn:miriam:ensembl:ENSG00000074410;urn:miriam:pubchem.compound:5281855"
      hgnc "HGNC_SYMBOL:CA12"
      map_id "M115_93"
      name "EAcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa78"
      uniprot "UNIPROT:O43570"
    ]
    graphics [
      x 897.4685168868904
      y 683.0067222107107
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 313
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:PLG;urn:miriam:hgnc.symbol:PLG;urn:miriam:ec-code:3.4.21.7;urn:miriam:ensembl:ENSG00000122194;urn:miriam:ncbigene:5340;urn:miriam:ncbigene:5340;urn:miriam:hgnc:9071;urn:miriam:refseq:NM_000301;urn:miriam:uniprot:P00747;urn:miriam:uniprot:P00747"
      hgnc "HGNC_SYMBOL:PLG"
      map_id "M115_371"
      name "PLG"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1431"
      uniprot "UNIPROT:P00747"
    ]
    graphics [
      x 977.3892733724382
      y 1296.5320803143568
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_371"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 314
    zlevel -1

    cd19dm [
      annotation "PUBMED:5006793"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_236"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10215"
      uniprot "NA"
    ]
    graphics [
      x 1112.1730551728688
      y 1183.91202817572
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_236"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 315
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:3.4.21.68;urn:miriam:ensembl:ENSG00000104368;urn:miriam:hgnc:9051;urn:miriam:hgnc.symbol:PLAT;urn:miriam:hgnc.symbol:PLAT;urn:miriam:uniprot:P00750;urn:miriam:uniprot:P00750;urn:miriam:ncbigene:5327;urn:miriam:ncbigene:5327;urn:miriam:refseq:NM_000930"
      hgnc "HGNC_SYMBOL:PLAT"
      map_id "M115_372"
      name "PLAT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1432"
      uniprot "UNIPROT:P00750"
    ]
    graphics [
      x 1040.2619887770227
      y 1355.857240853759
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_372"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 316
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32620147;urn:miriam:pubchem.compound:4413"
      hgnc "NA"
      map_id "M115_374"
      name "Nafamostat"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1434"
      uniprot "NA"
    ]
    graphics [
      x 1073.7981630001773
      y 1414.853924664385
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_374"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 317
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_150"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10126"
      uniprot "NA"
    ]
    graphics [
      x 883.4624979722346
      y 947.6937566875155
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_150"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 318
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:pubmed:10428810;urn:miriam:ncbigene:2963;urn:miriam:refseq:NM_004128;urn:miriam:ncbigene:2963;urn:miriam:hgnc:4653;urn:miriam:uniprot:P13984;urn:miriam:ec-code:3.6.4.12;urn:miriam:hgnc.symbol:GTF2F2;urn:miriam:hgnc.symbol:GTF2F2;urn:miriam:ensembl:ENSG00000188342"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:GTF2F2"
      map_id "M115_37"
      name "gtf2f2comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa16"
      uniprot "UNIPROT:P0DTD1;UNIPROT:P13984"
    ]
    graphics [
      x 744.536966015565
      y 982.523418343623
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 319
    zlevel -1

    cd19dm [
      annotation "PUBMED:19119014"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_181"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10159"
      uniprot "NA"
    ]
    graphics [
      x 1360.7671283458797
      y 619.9817145382167
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_181"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 320
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:3639"
      hgnc "NA"
      map_id "M115_302"
      name "Hydrochlorothiazide"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1237"
      uniprot "NA"
    ]
    graphics [
      x 1415.3264095563006
      y 458.64101377181623
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_302"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 321
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:19119014;urn:miriam:pubchem.compound:3639;urn:miriam:hgnc:1371;urn:miriam:uniprot:O43570;urn:miriam:ncbigene:771;urn:miriam:refseq:NM_001218;urn:miriam:ncbigene:771;urn:miriam:ec-code:4.2.1.1;urn:miriam:hgnc.symbol:CA12;urn:miriam:hgnc.symbol:CA12;urn:miriam:ensembl:ENSG00000074410"
      hgnc "HGNC_SYMBOL:CA12"
      map_id "M115_94"
      name "HCTcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa79"
      uniprot "UNIPROT:O43570"
    ]
    graphics [
      x 1345.8011338035856
      y 435.2682705959029
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 322
    zlevel -1

    cd19dm [
      annotation "PUBMED:17353931;PUBMED:26186194;PUBMED:20873783;PUBMED:12101123;PUBMED:28514442;PUBMED:12840024;PUBMED:20562859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_120"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10084"
      uniprot "NA"
    ]
    graphics [
      x 461.4422965002599
      y 1510.1633377987894
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 323
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc.symbol:NEK7;urn:miriam:hgnc.symbol:NEK7;urn:miriam:refseq:NM_133494;urn:miriam:uniprot:Q8TDX7;urn:miriam:ncbigene:140609;urn:miriam:ncbigene:140609;urn:miriam:ensembl:ENSG00000151414;urn:miriam:hgnc:13386"
      hgnc "HGNC_SYMBOL:NEK7"
      map_id "M115_252"
      name "NEK7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1067"
      uniprot "UNIPROT:Q8TDX7"
    ]
    graphics [
      x 324.66775403359713
      y 1496.3118686078965
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_252"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 324
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ncbigene:10783;urn:miriam:ncbigene:10783;urn:miriam:ec-code:2.7.11.1;urn:miriam:uniprot:Q9HC98;urn:miriam:hgnc.symbol:NEK6;urn:miriam:refseq:NM_014397;urn:miriam:hgnc.symbol:NEK6;urn:miriam:hgnc:7749;urn:miriam:ensembl:ENSG00000119408"
      hgnc "HGNC_SYMBOL:NEK6"
      map_id "M115_251"
      name "NEK6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1066"
      uniprot "UNIPROT:Q9HC98"
    ]
    graphics [
      x 398.5195970800031
      y 1633.1162292871827
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_251"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 325
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:28514442;urn:miriam:ncbigene:10783;urn:miriam:ncbigene:10783;urn:miriam:ec-code:2.7.11.1;urn:miriam:uniprot:Q9HC98;urn:miriam:hgnc.symbol:NEK6;urn:miriam:refseq:NM_014397;urn:miriam:hgnc.symbol:NEK6;urn:miriam:hgnc:7749;urn:miriam:ensembl:ENSG00000119408;urn:miriam:uniprot:Q8TD19;urn:miriam:ec-code:2.7.11.1;urn:miriam:ncbigene:91754;urn:miriam:ncbigene:91754;urn:miriam:hgnc:18591;urn:miriam:refseq:NM_033116;urn:miriam:hgnc.symbol:NEK9;urn:miriam:hgnc.symbol:NEK9;urn:miriam:ensembl:ENSG00000119638;urn:miriam:ec-code:2.7.11.1;urn:miriam:hgnc.symbol:NEK7;urn:miriam:hgnc.symbol:NEK7;urn:miriam:refseq:NM_133494;urn:miriam:uniprot:Q8TDX7;urn:miriam:ncbigene:140609;urn:miriam:ncbigene:140609;urn:miriam:ensembl:ENSG00000151414;urn:miriam:hgnc:13386"
      hgnc "HGNC_SYMBOL:NEK6;HGNC_SYMBOL:NEK9;HGNC_SYMBOL:NEK7"
      map_id "M115_50"
      name "NEKs"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa34"
      uniprot "UNIPROT:Q9HC98;UNIPROT:Q8TD19;UNIPROT:Q8TDX7"
    ]
    graphics [
      x 342.40409724867914
      y 1573.1026359621362
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 326
    zlevel -1

    cd19dm [
      annotation "PUBMED:17678888;PUBMED:16741042;PUBMED:20837776;PUBMED:11603923"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_122"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10089"
      uniprot "NA"
    ]
    graphics [
      x 910.7664443158341
      y 1107.4456489310976
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_122"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 327
    zlevel -1

    cd19dm [
      annotation "PUBMED:16713569"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_123"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10090"
      uniprot "NA"
    ]
    graphics [
      x 867.8438945099834
      y 1610.2970848448695
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_123"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 328
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:16713569;urn:miriam:hgnc:3176;urn:miriam:refseq:NM_001955;urn:miriam:uniprot:P05305;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:ensembl:ENSG00000078401"
      hgnc "HGNC_SYMBOL:EDN1"
      map_id "M115_54"
      name "EDN1_minus_homo"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa39"
      uniprot "UNIPROT:P05305"
    ]
    graphics [
      x 748.4463866390602
      y 1689.9818959452527
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 329
    zlevel -1

    cd19dm [
      annotation "PUBMED:17194211;PUBMED:11481605"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_212"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10191"
      uniprot "NA"
    ]
    graphics [
      x 250.44369698923674
      y 710.5301620120485
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_212"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 330
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18405;urn:miriam:pubchem.compound:1051"
      hgnc "NA"
      map_id "M115_333"
      name "Pyridoxal_space_phosphate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa1340"
      uniprot "NA"
    ]
    graphics [
      x 140.25563897534494
      y 807.4213249329579
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_333"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 331
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:17194211;urn:miriam:obo.chebi:CHEBI%3A18405;urn:miriam:pubchem.compound:1051;urn:miriam:ec-code:2.9.1.2;urn:miriam:hgnc:30605;urn:miriam:uniprot:Q9HD40;urn:miriam:hgnc.symbol:SEPSECS;urn:miriam:ncbigene:51091;urn:miriam:refseq:NM_016955;urn:miriam:ensembl:ENSG00000109618;urn:miriam:hgnc.symbol:SEPSECS;urn:miriam:ncbigene:51091"
      hgnc "HGNC_SYMBOL:SEPSECS"
      map_id "M115_12"
      name "SPcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa109"
      uniprot "UNIPROT:Q9HD40"
    ]
    graphics [
      x 139.33181443675608
      y 738.99488904592
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 332
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_219"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10198"
      uniprot "NA"
    ]
    graphics [
      x 488.90085360786475
      y 851.2110462875903
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_219"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 333
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000178105;urn:miriam:hgnc.symbol:DDX10;urn:miriam:hgnc.symbol:DDX10;urn:miriam:refseq:NM_004398;urn:miriam:ec-code:3.6.4.13;urn:miriam:hgnc:2735;urn:miriam:ncbigene:1662;urn:miriam:ncbigene:1662;urn:miriam:uniprot:Q13206"
      hgnc "HGNC_SYMBOL:DDX10"
      map_id "M115_342"
      name "DDX10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1366"
      uniprot "UNIPROT:Q13206"
    ]
    graphics [
      x 317.7714869008622
      y 890.1274604203566
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_342"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 334
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ensembl:ENSG00000178105;urn:miriam:hgnc.symbol:DDX10;urn:miriam:hgnc.symbol:DDX10;urn:miriam:refseq:NM_004398;urn:miriam:ec-code:3.6.4.13;urn:miriam:hgnc:2735;urn:miriam:ncbigene:1662;urn:miriam:ncbigene:1662;urn:miriam:uniprot:Q13206;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:DDX10;HGNC_SYMBOL:rep"
      map_id "M115_19"
      name "DDX10comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa116"
      uniprot "UNIPROT:Q13206;UNIPROT:P0DTD1"
    ]
    graphics [
      x 372.34192359738745
      y 940.2057820776251
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 335
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_191"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10170"
      uniprot "NA"
    ]
    graphics [
      x 1606.1833054128622
      y 1260.4290020318583
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_191"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 336
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ensembl:ENSG00000006451;urn:miriam:hgnc.symbol:RALA;urn:miriam:hgnc.symbol:RALA;urn:miriam:refseq:NM_005402;urn:miriam:ec-code:3.6.5.2;urn:miriam:uniprot:P11233;urn:miriam:hgnc:9839;urn:miriam:ncbigene:5898;urn:miriam:ncbigene:5898"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:RALA"
      map_id "M115_105"
      name "RALAcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa89"
      uniprot "UNIPROT:P0DTD1;UNIPROT:P11233"
    ]
    graphics [
      x 1359.27809652452
      y 1278.5180151629388
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 337
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_139"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10115"
      uniprot "NA"
    ]
    graphics [
      x 1619.282067471755
      y 1600.237010433043
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_139"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 338
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_147"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10123"
      uniprot "NA"
    ]
    graphics [
      x 1886.7746160472616
      y 1469.016468606573
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_147"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 339
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ncbigene:23111;urn:miriam:ncbigene:23111;urn:miriam:hgnc.symbol:SPART;urn:miriam:hgnc.symbol:SPART;urn:miriam:ensembl:ENSG00000133104;urn:miriam:hgnc:18514;urn:miriam:refseq:NM_001142294;urn:miriam:uniprot:Q8N0X7;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:SPART;HGNC_SYMBOL:rep"
      map_id "M115_41"
      name "spartcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa24"
      uniprot "UNIPROT:Q8N0X7;UNIPROT:P0DTD1"
    ]
    graphics [
      x 1867.5197122956758
      y 1591.8159144607844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 340
    zlevel -1

    cd19dm [
      annotation "PUBMED:11873938;PUBMED:12538800;PUBMED:11939936;PUBMED:11440283;PUBMED:10439935;PUBMED:10882160;PUBMED:10981253;PUBMED:11752352;PUBMED:12876237;PUBMED:9808337"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_196"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10175"
      uniprot "NA"
    ]
    graphics [
      x 1188.3574551001457
      y 2204.941162830548
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_196"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 341
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:5281081"
      hgnc "NA"
      map_id "M115_316"
      name "Entacapone"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1284"
      uniprot "NA"
    ]
    graphics [
      x 1102.217940507123
      y 2048.0944670638096
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_316"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 342
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:12876237;urn:miriam:pubchem.compound:5281081;urn:miriam:ensembl:ENSG00000093010;urn:miriam:uniprot:P21964;urn:miriam:hgnc.symbol:COMT;urn:miriam:hgnc.symbol:COMT;urn:miriam:hgnc:2228;urn:miriam:ec-code:2.1.1.6;urn:miriam:refseq:NM_000754;urn:miriam:ncbigene:1312;urn:miriam:ncbigene:1312"
      hgnc "HGNC_SYMBOL:COMT"
      map_id "M115_109"
      name "NCcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa93"
      uniprot "UNIPROT:P21964"
    ]
    graphics [
      x 1089.7540799697301
      y 2236.249498928609
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 343
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M115_369"
      name "Nsp10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1429"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 1834.9563549388395
      y 1983.6408487199642
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_369"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 344
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_222"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10201"
      uniprot "NA"
    ]
    graphics [
      x 1923.0230205280666
      y 2056.2907750751074
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_222"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 345
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:ERGIC1;urn:miriam:ensembl:ENSG00000113719;urn:miriam:hgnc.symbol:ERGIC1;urn:miriam:hgnc:29205;urn:miriam:ncbigene:57222;urn:miriam:ncbigene:57222;urn:miriam:refseq:NM_020462;urn:miriam:uniprot:Q969X5"
      hgnc "HGNC_SYMBOL:ERGIC1"
      map_id "M115_349"
      name "ERGIC1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1383"
      uniprot "UNIPROT:Q969X5"
    ]
    graphics [
      x 1863.0183109054337
      y 2135.615571142559
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_349"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 346
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:hgnc.symbol:ERGIC1;urn:miriam:ensembl:ENSG00000113719;urn:miriam:hgnc.symbol:ERGIC1;urn:miriam:hgnc:29205;urn:miriam:ncbigene:57222;urn:miriam:ncbigene:57222;urn:miriam:refseq:NM_020462;urn:miriam:uniprot:Q969X5"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:ERGIC1"
      map_id "M115_22"
      name "ERGIC1comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa119"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q969X5"
    ]
    graphics [
      x 1761.5294757341446
      y 1933.4812555486471
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 347
    zlevel -1

    cd19dm [
      annotation "PUBMED:14962394"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_131"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10107"
      uniprot "NA"
    ]
    graphics [
      x 1036.2966216346606
      y 902.843486927404
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_131"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 348
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32296183;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ncbiprotein:YP_009725305;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M115_23"
      name "NspComp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa12"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 938.1139078212078
      y 978.2263948690319
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 349
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:uniprot:P46379;urn:miriam:refseq:NM_080703;urn:miriam:ncbigene:7917;urn:miriam:ncbigene:7917;urn:miriam:ensembl:ENSG00000204463;urn:miriam:hgnc:13919;urn:miriam:hgnc.symbol:BAG6;urn:miriam:hgnc.symbol:BAG6"
      hgnc "HGNC_SYMBOL:BAG6"
      map_id "M115_255"
      name "BAG6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1077"
      uniprot "UNIPROT:P46379"
    ]
    graphics [
      x 688.7080640773552
      y 1772.6571049413255
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_255"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 350
    zlevel -1

    cd19dm [
      annotation "PUBMED:16169070"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_136"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10112"
      uniprot "NA"
    ]
    graphics [
      x 830.284392428189
      y 1703.247664871219
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_136"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 351
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:16169070;urn:miriam:hgnc:3176;urn:miriam:refseq:NM_001955;urn:miriam:uniprot:P05305;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:hgnc.symbol:EDN1;urn:miriam:ncbigene:1906;urn:miriam:ensembl:ENSG00000078401;urn:miriam:uniprot:P46379;urn:miriam:refseq:NM_080703;urn:miriam:ncbigene:7917;urn:miriam:ncbigene:7917;urn:miriam:ensembl:ENSG00000204463;urn:miriam:hgnc:13919;urn:miriam:hgnc.symbol:BAG6;urn:miriam:hgnc.symbol:BAG6"
      hgnc "HGNC_SYMBOL:EDN1;HGNC_SYMBOL:BAG6"
      map_id "M115_52"
      name "EDN1_minus_homo"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa37"
      uniprot "UNIPROT:P05305;UNIPROT:P46379"
    ]
    graphics [
      x 737.043730255381
      y 1827.2453158755957
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 352
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_205"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10184"
      uniprot "NA"
    ]
    graphics [
      x 565.8027780853471
      y 629.4006077893939
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_205"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 353
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:MPHOSPH10;urn:miriam:hgnc.symbol:MPHOSPH10;urn:miriam:ncbigene:10199;urn:miriam:ncbigene:10199;urn:miriam:hgnc:7213;urn:miriam:ensembl:ENSG00000124383;urn:miriam:uniprot:O00566;urn:miriam:refseq:NM_005791"
      hgnc "HGNC_SYMBOL:MPHOSPH10"
      map_id "M115_323"
      name "MPHOSPH10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1312"
      uniprot "UNIPROT:O00566"
    ]
    graphics [
      x 472.76696366961187
      y 738.9745742564463
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_323"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 354
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:hgnc.symbol:MPHOSPH10;urn:miriam:hgnc.symbol:MPHOSPH10;urn:miriam:ncbigene:10199;urn:miriam:ncbigene:10199;urn:miriam:hgnc:7213;urn:miriam:ensembl:ENSG00000124383;urn:miriam:uniprot:O00566;urn:miriam:refseq:NM_005791"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:MPHOSPH10"
      map_id "M115_5"
      name "MPHOSPHcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa102"
      uniprot "UNIPROT:P0DTD1;UNIPROT:O00566"
    ]
    graphics [
      x 417.54452592575
      y 601.9010405734532
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 355
    zlevel -1

    cd19dm [
      annotation "PUBMED:10592235"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_224"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10203"
      uniprot "NA"
    ]
    graphics [
      x 524.3026287470215
      y 323.5593068252505
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_224"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 356
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:643975;urn:miriam:obo.chebi:CHEBI%3A16238"
      hgnc "NA"
      map_id "M115_351"
      name "FAD"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa1389"
      uniprot "NA"
    ]
    graphics [
      x 639.6066569017739
      y 405.9120826276228
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_351"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 357
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:10592235;urn:miriam:hgnc:4236;urn:miriam:ensembl:ENSG00000127554;urn:miriam:uniprot:P55789;urn:miriam:ncbigene:2671;urn:miriam:refseq:NM_005262;urn:miriam:ncbigene:2671;urn:miriam:ec-code:1.8.3.2;urn:miriam:hgnc.symbol:GFER;urn:miriam:hgnc.symbol:GFER;urn:miriam:pubchem.compound:643975;urn:miriam:obo.chebi:CHEBI%3A16238"
      hgnc "HGNC_SYMBOL:GFER"
      map_id "M115_25"
      name "FGCOMP"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa121"
      uniprot "UNIPROT:P55789"
    ]
    graphics [
      x 563.072611205114
      y 462.7812085859156
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 358
    zlevel -1

    cd19dm [
      annotation "PUBMED:19119014"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_182"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10161"
      uniprot "NA"
    ]
    graphics [
      x 1404.2581523410126
      y 1236.06025265905
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_182"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 359
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:70876165"
      hgnc "NA"
      map_id "M115_303"
      name "Hydroflumethiazide"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1241"
      uniprot "NA"
    ]
    graphics [
      x 1461.5301702049933
      y 1417.268275083205
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_303"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 360
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:19119014;urn:miriam:pubchem.compound:70876165;urn:miriam:hgnc:1371;urn:miriam:uniprot:O43570;urn:miriam:ncbigene:771;urn:miriam:refseq:NM_001218;urn:miriam:ncbigene:771;urn:miriam:ec-code:4.2.1.1;urn:miriam:hgnc.symbol:CA12;urn:miriam:hgnc.symbol:CA12;urn:miriam:ensembl:ENSG00000074410"
      hgnc "HGNC_SYMBOL:CA12"
      map_id "M115_96"
      name "HFTcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa80"
      uniprot "UNIPROT:O43570"
    ]
    graphics [
      x 1430.4037327052629
      y 1371.8932760970506
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 361
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_213"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10192"
      uniprot "NA"
    ]
    graphics [
      x 888.1902836625611
      y 806.2065579635569
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_213"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 362
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000115761;urn:miriam:ncbigene:79954;urn:miriam:refseq:NM_024894;urn:miriam:ncbigene:79954;urn:miriam:uniprot:Q9BSC4;urn:miriam:hgnc:25862;urn:miriam:hgnc.symbol:NOL10;urn:miriam:hgnc.symbol:NOL10"
      hgnc "HGNC_SYMBOL:NOL10"
      map_id "M115_334"
      name "NOL10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1343"
      uniprot "UNIPROT:Q9BSC4"
    ]
    graphics [
      x 778.2182852177971
      y 879.7083810612318
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_334"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 363
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ncbiprotein:YP_009725304;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:taxonomy:2697049;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ensembl:ENSG00000115761;urn:miriam:ncbigene:79954;urn:miriam:refseq:NM_024894;urn:miriam:ncbigene:79954;urn:miriam:uniprot:Q9BSC4;urn:miriam:hgnc:25862;urn:miriam:hgnc.symbol:NOL10;urn:miriam:hgnc.symbol:NOL10"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:NOL10"
      map_id "M115_13"
      name "NOL10comp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa110"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q9BSC4"
    ]
    graphics [
      x 980.5155311794446
      y 820.6987924152841
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 364
    zlevel -1

    cd19dm [
      annotation "PUBMED:19119014"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_183"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10162"
      uniprot "NA"
    ]
    graphics [
      x 1304.2160526055281
      y 1185.077238689412
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_183"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 365
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:2343"
      hgnc "NA"
      map_id "M115_304"
      name "Benzthiazide"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1244"
      uniprot "NA"
    ]
    graphics [
      x 1309.4549708463155
      y 1359.0090574090852
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_304"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 366
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:19119014;urn:miriam:pubchem.compound:2343;urn:miriam:hgnc:1371;urn:miriam:uniprot:O43570;urn:miriam:ncbigene:771;urn:miriam:refseq:NM_001218;urn:miriam:ncbigene:771;urn:miriam:ec-code:4.2.1.1;urn:miriam:hgnc.symbol:CA12;urn:miriam:hgnc.symbol:CA12;urn:miriam:ensembl:ENSG00000074410"
      hgnc "HGNC_SYMBOL:CA12"
      map_id "M115_97"
      name "BZcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa81"
      uniprot "UNIPROT:O43570"
    ]
    graphics [
      x 1395.8819493362757
      y 1303.8992804195143
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 367
    zlevel -1

    cd19dm [
      annotation "PUBMED:17016423;PUBMED:17139284;PUBMED:10592235"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_169"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10146"
      uniprot "NA"
    ]
    graphics [
      x 1204.9621846683092
      y 1785.7631820887
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_169"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 368
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:6022;urn:miriam:obo.chebi:CHEBI%3A16761"
      hgnc "NA"
      map_id "M115_290"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa1201"
      uniprot "NA"
    ]
    graphics [
      x 1189.5126429472575
      y 1898.4535678271343
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_290"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 369
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:17016423;urn:miriam:pubchem.compound:6022;urn:miriam:obo.chebi:CHEBI%3A16761;urn:miriam:ec-code:2.7.11.4;urn:miriam:uniprot:O14874;urn:miriam:hgnc.symbol:BCKDK;urn:miriam:hgnc.symbol:BCKDK;urn:miriam:ncbigene:10295;urn:miriam:refseq:NM_005881;urn:miriam:ncbigene:10295;urn:miriam:ensembl:ENSG00000103507;urn:miriam:hgnc:16902"
      hgnc "HGNC_SYMBOL:BCKDK"
      map_id "M115_82"
      name "ADPcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa67"
      uniprot "UNIPROT:O14874"
    ]
    graphics [
      x 1120.3536109778718
      y 1902.914581738617
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 370
    zlevel -1

    cd19dm [
      annotation "PUBMED:20837776"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_121"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10087"
      uniprot "NA"
    ]
    graphics [
      x 1935.3316918569544
      y 614.9156449502174
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_121"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 371
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_171"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10148"
      uniprot "NA"
    ]
    graphics [
      x 1201.1576210006735
      y 966.6476350796888
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_171"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 372
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000140262;urn:miriam:hgnc:11623;urn:miriam:ncbigene:6938;urn:miriam:ncbigene:6938;urn:miriam:uniprot:Q99081;urn:miriam:hgnc.symbol:TCF12;urn:miriam:hgnc.symbol:TCF12;urn:miriam:refseq:NM_003205"
      hgnc "HGNC_SYMBOL:TCF12"
      map_id "M115_292"
      name "TCF12"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1207"
      uniprot "UNIPROT:Q99081"
    ]
    graphics [
      x 1065.4200262312559
      y 1065.6096430592688
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_292"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 373
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:32353859;urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:uniprot:P0DTD1;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ncbigene:43740578;urn:miriam:ec-code:3.1.13.-;urn:miriam:ensembl:ENSG00000140262;urn:miriam:hgnc:11623;urn:miriam:ncbigene:6938;urn:miriam:ncbigene:6938;urn:miriam:uniprot:Q99081;urn:miriam:hgnc.symbol:TCF12;urn:miriam:hgnc.symbol:TCF12;urn:miriam:refseq:NM_003205"
      hgnc "HGNC_SYMBOL:rep;HGNC_SYMBOL:TCF12"
      map_id "M115_84"
      name "TCFcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa69"
      uniprot "UNIPROT:P0DTD1;UNIPROT:Q99081"
    ]
    graphics [
      x 1030.3373284784889
      y 980.9883578834625
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 374
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_187"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10166"
      uniprot "NA"
    ]
    graphics [
      x 1326.2618615512254
      y 1456.809803764686
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_187"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 375
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ec-code:3.4.19.12;urn:miriam:ec-code:3.4.22.69;urn:miriam:hgnc.symbol:rep;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:1489680;urn:miriam:ec-code:2.7.7.48;urn:miriam:ec-code:2.1.1.-;urn:miriam:ec-code:3.4.22.-;urn:miriam:ec-code:3.6.4.13;urn:miriam:ec-code:3.6.4.12;urn:miriam:ec-code:3.1.13.-;urn:miriam:uniprot:P0C6X7"
      hgnc "HGNC_SYMBOL:rep"
      map_id "M115_345"
      name "Nsp16"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1374"
      uniprot "UNIPROT:P0C6X7"
    ]
    graphics [
      x 1467.0744285713886
      y 1585.8867288797624
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_345"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 376
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:hgnc.symbol:NEMF;urn:miriam:hgnc.symbol:NEMF;urn:miriam:ncbigene:9147;urn:miriam:ncbigene:9147;urn:miriam:ensembl:ENSG00000165525;urn:miriam:hgnc:10663;urn:miriam:uniprot:O60524;urn:miriam:refseq:NM_004713"
      hgnc "HGNC_SYMBOL:NEMF"
      map_id "M115_346"
      name "NEMF"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1376"
      uniprot "UNIPROT:O60524"
    ]
    graphics [
      x 1359.8915718192736
      y 1635.870447294131
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_346"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 377
    zlevel -1

    cd19dm [
      annotation "PUBMED:10592235"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_199"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10178"
      uniprot "NA"
    ]
    graphics [
      x 1363.5422434097827
      y 2227.8237481863152
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_199"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 378
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:6914595"
      hgnc "NA"
      map_id "M115_319"
      name "(3,4_minus_DIHYDROXY_minus_2_minus_NITROPHENYL)(PHENYL)METHANONE"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1293"
      uniprot "NA"
    ]
    graphics [
      x 1427.5115351888107
      y 2126.8029841642256
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_319"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 379
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:10592235;urn:miriam:pubchem.compound:6914595;urn:miriam:ensembl:ENSG00000093010;urn:miriam:uniprot:P21964;urn:miriam:hgnc.symbol:COMT;urn:miriam:hgnc.symbol:COMT;urn:miriam:hgnc:2228;urn:miriam:ec-code:2.1.1.6;urn:miriam:refseq:NM_000754;urn:miriam:ncbigene:1312;urn:miriam:ncbigene:1312"
      hgnc "HGNC_SYMBOL:COMT"
      map_id "M115_112"
      name "DNCcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa96"
      uniprot "UNIPROT:P21964"
    ]
    graphics [
      x 1304.1960363989074
      y 2053.0580006325845
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 380
    zlevel -1

    cd19dm [
      annotation "PUBMED:24261583;PUBMED:22862294;PUBMED:22458347"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_161"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10138"
      uniprot "NA"
    ]
    graphics [
      x 1018.0065840256392
      y 1024.6161960748263
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_161"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 381
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:16004692"
      hgnc "NA"
      map_id "M115_281"
      name "Macitentan"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1177"
      uniprot "NA"
    ]
    graphics [
      x 989.7792415019356
      y 1216.9136132557041
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_281"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 382
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:24261583;urn:miriam:pubmed:22862294;urn:miriam:pubmed:22458347;urn:miriam:refseq:NM_001166055;urn:miriam:hgnc:3179;urn:miriam:uniprot:P25101;urn:miriam:ensembl:ENSG00000151617;urn:miriam:ncbigene:1909;urn:miriam:ncbigene:1909;urn:miriam:hgnc.symbol:EDNRA;urn:miriam:hgnc.symbol:EDNRA;urn:miriam:pubchem.compound:16004692"
      hgnc "HGNC_SYMBOL:EDNRA"
      map_id "M115_76"
      name "EDNRMacComp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa61"
      uniprot "UNIPROT:P25101"
    ]
    graphics [
      x 1022.7479617501378
      y 1248.8761014341844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 383
    zlevel -1

    cd19dm [
      annotation "PUBMED:11873938;PUBMED:17063156;PUBMED:12538800;PUBMED:20502133;PUBMED:10882160;PUBMED:18046910;PUBMED:9917075;PUBMED:19503773;PUBMED:11752352;PUBMED:15697329"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_195"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10174"
      uniprot "NA"
    ]
    graphics [
      x 1176.383511772234
      y 2315.351816030642
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_195"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 384
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:4659569"
      hgnc "NA"
      map_id "M115_315"
      name "Tolcapone"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1281"
      uniprot "NA"
    ]
    graphics [
      x 1031.9919972906714
      y 2225.0238191043118
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_315"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 385
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:20502133;urn:miriam:ensembl:ENSG00000093010;urn:miriam:uniprot:P21964;urn:miriam:hgnc.symbol:COMT;urn:miriam:hgnc.symbol:COMT;urn:miriam:hgnc:2228;urn:miriam:ec-code:2.1.1.6;urn:miriam:refseq:NM_000754;urn:miriam:ncbigene:1312;urn:miriam:ncbigene:1312;urn:miriam:pubchem.compound:4659569"
      hgnc "HGNC_SYMBOL:COMT"
      map_id "M115_108"
      name "TCcomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa92"
      uniprot "UNIPROT:P21964"
    ]
    graphics [
      x 1062.6930911017348
      y 2369.31210393987
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 386
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_101"
      target_id "M115_193"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 387
    source 2
    target 3
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_193"
      target_id "M115_314"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 388
    source 4
    target 5
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_382"
      target_id "M115_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 389
    source 6
    target 5
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_249"
      target_id "M115_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 390
    source 7
    target 5
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_237"
      target_id "M115_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 391
    source 8
    target 5
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_246"
      target_id "M115_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 392
    source 9
    target 5
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_247"
      target_id "M115_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 393
    source 10
    target 5
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_248"
      target_id "M115_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 394
    source 5
    target 11
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_119"
      target_id "M115_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 395
    source 12
    target 13
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_364"
      target_id "M115_141"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 396
    source 14
    target 13
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_2"
      target_id "M115_141"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 397
    source 13
    target 15
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_141"
      target_id "M115_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 398
    source 16
    target 17
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_270"
      target_id "M115_126"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 399
    source 18
    target 17
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_267"
      target_id "M115_126"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 400
    source 19
    target 17
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CATALYSIS"
      source_id "M115_373"
      target_id "M115_126"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 401
    source 17
    target 20
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_126"
      target_id "M115_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 402
    source 21
    target 22
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_274"
      target_id "M115_154"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 403
    source 23
    target 22
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_275"
      target_id "M115_154"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 404
    source 22
    target 24
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_154"
      target_id "M115_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 405
    source 25
    target 26
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_377"
      target_id "M115_142"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 406
    source 27
    target 26
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_383"
      target_id "M115_142"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 407
    source 26
    target 28
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_142"
      target_id "M115_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 408
    source 29
    target 30
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_53"
      target_id "M115_132"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 409
    source 31
    target 30
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_264"
      target_id "M115_132"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 410
    source 30
    target 32
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_132"
      target_id "M115_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 411
    source 33
    target 34
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_385"
      target_id "M115_211"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 412
    source 35
    target 34
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_332"
      target_id "M115_211"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 413
    source 34
    target 36
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_211"
      target_id "M115_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 414
    source 37
    target 38
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_336"
      target_id "M115_216"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 415
    source 39
    target 38
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_337"
      target_id "M115_216"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 416
    source 38
    target 40
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_216"
      target_id "M115_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 417
    source 41
    target 42
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_1"
      target_id "M115_149"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 418
    source 43
    target 42
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_378"
      target_id "M115_149"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 419
    source 16
    target 42
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_270"
      target_id "M115_149"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 420
    source 19
    target 42
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CATALYSIS"
      source_id "M115_373"
      target_id "M115_149"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 421
    source 42
    target 44
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_149"
      target_id "M115_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 422
    source 33
    target 45
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_385"
      target_id "M115_217"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 423
    source 46
    target 45
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_339"
      target_id "M115_217"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 424
    source 45
    target 47
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_217"
      target_id "M115_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 425
    source 48
    target 49
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_380"
      target_id "M115_128"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 426
    source 50
    target 49
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_269"
      target_id "M115_128"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 427
    source 49
    target 51
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_128"
      target_id "M115_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 428
    source 52
    target 53
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_240"
      target_id "M115_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 429
    source 27
    target 53
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CATALYSIS"
      source_id "M115_383"
      target_id "M115_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 430
    source 53
    target 54
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_116"
      target_id "M115_239"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 431
    source 55
    target 56
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_379"
      target_id "M115_130"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 432
    source 57
    target 56
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_253"
      target_id "M115_130"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 433
    source 56
    target 58
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_130"
      target_id "M115_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 434
    source 59
    target 60
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_362"
      target_id "M115_214"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 435
    source 61
    target 60
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_335"
      target_id "M115_214"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 436
    source 60
    target 62
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_214"
      target_id "M115_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 437
    source 63
    target 64
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_261"
      target_id "M115_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 438
    source 65
    target 64
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_260"
      target_id "M115_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 439
    source 66
    target 64
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_258"
      target_id "M115_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 440
    source 67
    target 64
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_257"
      target_id "M115_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 441
    source 68
    target 64
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_259"
      target_id "M115_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 442
    source 69
    target 64
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_262"
      target_id "M115_124"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 443
    source 64
    target 70
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_124"
      target_id "M115_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 444
    source 71
    target 72
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_284"
      target_id "M115_168"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 445
    source 73
    target 72
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_289"
      target_id "M115_168"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 446
    source 72
    target 74
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_168"
      target_id "M115_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 447
    source 75
    target 76
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_256"
      target_id "M115_135"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 448
    source 70
    target 76
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_55"
      target_id "M115_135"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 449
    source 76
    target 77
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_135"
      target_id "M115_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 450
    source 78
    target 79
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_353"
      target_id "M115_229"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 451
    source 80
    target 79
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_356"
      target_id "M115_229"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 452
    source 79
    target 81
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_229"
      target_id "M115_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 453
    source 82
    target 83
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_368"
      target_id "M115_143"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 454
    source 48
    target 83
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_380"
      target_id "M115_143"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 455
    source 83
    target 84
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_143"
      target_id "M115_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 456
    source 85
    target 86
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_313"
      target_id "M115_197"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 457
    source 87
    target 86
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_317"
      target_id "M115_197"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 458
    source 86
    target 88
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_197"
      target_id "M115_110"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 459
    source 89
    target 90
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_299"
      target_id "M115_179"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 460
    source 91
    target 90
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_300"
      target_id "M115_179"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 461
    source 90
    target 92
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_179"
      target_id "M115_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 462
    source 41
    target 93
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_1"
      target_id "M115_127"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 463
    source 94
    target 93
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_376"
      target_id "M115_127"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 464
    source 93
    target 95
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_127"
      target_id "M115_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 465
    source 96
    target 97
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_347"
      target_id "M115_231"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 466
    source 98
    target 97
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_358"
      target_id "M115_231"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 467
    source 97
    target 99
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_231"
      target_id "M115_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 468
    source 100
    target 101
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_307"
      target_id "M115_225"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 469
    source 102
    target 101
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_352"
      target_id "M115_225"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 470
    source 101
    target 103
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_225"
      target_id "M115_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 471
    source 104
    target 105
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_365"
      target_id "M115_146"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 472
    source 106
    target 105
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_384"
      target_id "M115_146"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 473
    source 105
    target 107
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_146"
      target_id "M115_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 474
    source 108
    target 109
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_370"
      target_id "M115_188"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 475
    source 110
    target 109
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_308"
      target_id "M115_188"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 476
    source 109
    target 111
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_188"
      target_id "M115_102"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 477
    source 110
    target 112
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_308"
      target_id "M115_190"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 478
    source 113
    target 112
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_310"
      target_id "M115_190"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 479
    source 112
    target 114
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_190"
      target_id "M115_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 480
    source 33
    target 115
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_385"
      target_id "M115_218"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 481
    source 116
    target 115
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_340"
      target_id "M115_218"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 482
    source 117
    target 115
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_341"
      target_id "M115_218"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 483
    source 115
    target 118
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_218"
      target_id "M115_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 484
    source 41
    target 119
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_1"
      target_id "M115_185"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 485
    source 119
    target 120
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_185"
      target_id "M115_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 486
    source 121
    target 122
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_367"
      target_id "M115_235"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 487
    source 122
    target 123
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_235"
      target_id "M115_354"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 488
    source 31
    target 124
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_264"
      target_id "M115_159"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 489
    source 125
    target 124
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_279"
      target_id "M115_159"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 490
    source 124
    target 126
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_159"
      target_id "M115_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 491
    source 127
    target 128
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_95"
      target_id "M115_152"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 492
    source 129
    target 128
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_271"
      target_id "M115_152"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 493
    source 19
    target 128
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CATALYSIS"
      source_id "M115_373"
      target_id "M115_152"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 494
    source 128
    target 130
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_152"
      target_id "M115_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 495
    source 131
    target 132
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_326"
      target_id "M115_208"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 496
    source 33
    target 132
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_385"
      target_id "M115_208"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 497
    source 133
    target 132
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_330"
      target_id "M115_208"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 498
    source 134
    target 132
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_331"
      target_id "M115_208"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 499
    source 135
    target 132
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_338"
      target_id "M115_208"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 500
    source 132
    target 136
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_208"
      target_id "M115_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 501
    source 137
    target 138
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_327"
      target_id "M115_209"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 502
    source 33
    target 138
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_385"
      target_id "M115_209"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 503
    source 139
    target 138
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_328"
      target_id "M115_209"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 504
    source 138
    target 140
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_209"
      target_id "M115_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 505
    source 141
    target 142
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_322"
      target_id "M115_204"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 506
    source 143
    target 142
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_312"
      target_id "M115_204"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 507
    source 142
    target 144
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_204"
      target_id "M115_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 508
    source 94
    target 145
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_376"
      target_id "M115_129"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 509
    source 146
    target 145
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_268"
      target_id "M115_129"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 510
    source 145
    target 147
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_129"
      target_id "M115_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 511
    source 108
    target 148
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_370"
      target_id "M115_194"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 512
    source 85
    target 148
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_313"
      target_id "M115_194"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 513
    source 148
    target 149
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_194"
      target_id "M115_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 514
    source 71
    target 150
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_284"
      target_id "M115_173"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 515
    source 151
    target 150
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_294"
      target_id "M115_173"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 516
    source 150
    target 152
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_173"
      target_id "M115_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 517
    source 108
    target 153
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_370"
      target_id "M115_203"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 518
    source 141
    target 153
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_322"
      target_id "M115_203"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 519
    source 153
    target 154
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_203"
      target_id "M115_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 520
    source 155
    target 156
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_321"
      target_id "M115_202"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 521
    source 143
    target 156
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_312"
      target_id "M115_202"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 522
    source 156
    target 157
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_202"
      target_id "M115_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 523
    source 146
    target 158
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_268"
      target_id "M115_157"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 524
    source 159
    target 158
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_277"
      target_id "M115_157"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 525
    source 158
    target 160
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_157"
      target_id "M115_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 526
    source 129
    target 161
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_271"
      target_id "M115_163"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 527
    source 162
    target 161
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_272"
      target_id "M115_163"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 528
    source 161
    target 163
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_163"
      target_id "M115_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 529
    source 96
    target 164
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_347"
      target_id "M115_234"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 530
    source 165
    target 164
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_361"
      target_id "M115_234"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 531
    source 164
    target 166
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_234"
      target_id "M115_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 532
    source 167
    target 168
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_298"
      target_id "M115_176"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 533
    source 169
    target 168
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_297"
      target_id "M115_176"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 534
    source 168
    target 170
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_176"
      target_id "M115_89"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 535
    source 98
    target 171
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_358"
      target_id "M115_232"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 536
    source 172
    target 171
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_359"
      target_id "M115_232"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 537
    source 171
    target 173
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_232"
      target_id "M115_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 538
    source 174
    target 175
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_58"
      target_id "M115_151"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 539
    source 175
    target 176
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_151"
      target_id "M115_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 540
    source 33
    target 177
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_385"
      target_id "M115_164"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 541
    source 178
    target 177
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_283"
      target_id "M115_164"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 542
    source 71
    target 177
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_284"
      target_id "M115_164"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 543
    source 177
    target 179
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_164"
      target_id "M115_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 544
    source 33
    target 180
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_385"
      target_id "M115_220"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 545
    source 181
    target 180
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_343"
      target_id "M115_220"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 546
    source 182
    target 180
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_344"
      target_id "M115_220"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 547
    source 180
    target 183
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_220"
      target_id "M115_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 548
    source 33
    target 184
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_385"
      target_id "M115_210"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 549
    source 185
    target 184
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_329"
      target_id "M115_210"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 550
    source 184
    target 186
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_210"
      target_id "M115_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 551
    source 187
    target 188
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_366"
      target_id "M115_226"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 552
    source 78
    target 188
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_353"
      target_id "M115_226"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 553
    source 188
    target 189
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_226"
      target_id "M115_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 554
    source 190
    target 191
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_254"
      target_id "M115_137"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 555
    source 192
    target 191
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_238"
      target_id "M115_137"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 556
    source 191
    target 193
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_137"
      target_id "M115_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 557
    source 194
    target 195
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_265"
      target_id "M115_155"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 558
    source 196
    target 195
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_273"
      target_id "M115_155"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 559
    source 195
    target 197
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_155"
      target_id "M115_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 560
    source 198
    target 199
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_79"
      target_id "M115_166"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 561
    source 199
    target 200
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_166"
      target_id "M115_287"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 562
    source 187
    target 201
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_366"
      target_id "M115_233"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 563
    source 202
    target 201
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_360"
      target_id "M115_233"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 564
    source 201
    target 203
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_233"
      target_id "M115_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 565
    source 204
    target 205
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_355"
      target_id "M115_228"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 566
    source 205
    target 121
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_228"
      target_id "M115_367"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 567
    source 206
    target 207
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_266"
      target_id "M115_153"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 568
    source 196
    target 207
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_273"
      target_id "M115_153"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 569
    source 207
    target 208
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_153"
      target_id "M115_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 570
    source 71
    target 209
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_284"
      target_id "M115_174"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 571
    source 210
    target 209
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_295"
      target_id "M115_174"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 572
    source 209
    target 211
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_174"
      target_id "M115_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 573
    source 100
    target 212
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_307"
      target_id "M115_221"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 574
    source 213
    target 212
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_348"
      target_id "M115_221"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 575
    source 212
    target 214
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_221"
      target_id "M115_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 576
    source 54
    target 215
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_239"
      target_id "M115_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 577
    source 215
    target 192
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_117"
      target_id "M115_238"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 578
    source 33
    target 216
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_385"
      target_id "M115_206"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 579
    source 217
    target 216
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_324"
      target_id "M115_206"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 580
    source 216
    target 218
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_206"
      target_id "M115_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 581
    source 179
    target 219
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_78"
      target_id "M115_165"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 582
    source 220
    target 219
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_286"
      target_id "M115_165"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 583
    source 221
    target 219
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CATALYSIS"
      source_id "M115_285"
      target_id "M115_165"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 584
    source 222
    target 219
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CATALYSIS"
      source_id "M115_375"
      target_id "M115_165"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 585
    source 219
    target 198
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_165"
      target_id "M115_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 586
    source 78
    target 223
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_353"
      target_id "M115_230"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 587
    source 224
    target 223
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_357"
      target_id "M115_230"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 588
    source 223
    target 225
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_230"
      target_id "M115_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 589
    source 78
    target 226
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_353"
      target_id "M115_227"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 590
    source 123
    target 226
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_354"
      target_id "M115_227"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 591
    source 226
    target 227
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_227"
      target_id "M115_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 592
    source 104
    target 228
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_365"
      target_id "M115_144"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 593
    source 25
    target 228
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_377"
      target_id "M115_144"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 594
    source 228
    target 229
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_144"
      target_id "M115_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 595
    source 230
    target 231
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_244"
      target_id "M115_140"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 596
    source 232
    target 231
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_245"
      target_id "M115_140"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 597
    source 231
    target 233
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_140"
      target_id "M115_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 598
    source 104
    target 234
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_365"
      target_id "M115_145"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 599
    source 27
    target 234
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_383"
      target_id "M115_145"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 600
    source 234
    target 235
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_145"
      target_id "M115_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 601
    source 71
    target 236
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_284"
      target_id "M115_167"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 602
    source 237
    target 236
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_288"
      target_id "M115_167"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 603
    source 236
    target 238
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_167"
      target_id "M115_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 604
    source 71
    target 239
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_284"
      target_id "M115_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 605
    source 240
    target 239
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_293"
      target_id "M115_172"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 606
    source 239
    target 241
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_172"
      target_id "M115_85"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 607
    source 85
    target 242
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_313"
      target_id "M115_198"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 608
    source 243
    target 242
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_318"
      target_id "M115_198"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 609
    source 242
    target 244
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_198"
      target_id "M115_111"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 610
    source 104
    target 245
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_365"
      target_id "M115_148"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 611
    source 246
    target 245
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_381"
      target_id "M115_148"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 612
    source 245
    target 247
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_148"
      target_id "M115_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 613
    source 146
    target 248
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_268"
      target_id "M115_156"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 614
    source 249
    target 248
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_276"
      target_id "M115_156"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 615
    source 248
    target 250
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_156"
      target_id "M115_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 616
    source 193
    target 251
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_51"
      target_id "M115_133"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 617
    source 252
    target 251
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_263"
      target_id "M115_133"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 618
    source 251
    target 174
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_133"
      target_id "M115_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 619
    source 31
    target 253
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_264"
      target_id "M115_160"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 620
    source 254
    target 253
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_280"
      target_id "M115_160"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 621
    source 253
    target 255
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_160"
      target_id "M115_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 622
    source 85
    target 256
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_313"
      target_id "M115_200"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 623
    source 257
    target 256
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_320"
      target_id "M115_200"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 624
    source 256
    target 258
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_200"
      target_id "M115_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 625
    source 178
    target 259
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_283"
      target_id "M115_178"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 626
    source 89
    target 259
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_299"
      target_id "M115_178"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 627
    source 259
    target 260
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_178"
      target_id "M115_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 628
    source 261
    target 262
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_311"
      target_id "M115_192"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 629
    source 143
    target 262
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_312"
      target_id "M115_192"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 630
    source 262
    target 263
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_192"
      target_id "M115_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 631
    source 264
    target 265
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_242"
      target_id "M115_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 632
    source 266
    target 265
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_243"
      target_id "M115_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 633
    source 267
    target 265
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_241"
      target_id "M115_118"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 634
    source 265
    target 268
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_118"
      target_id "M115_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 635
    source 59
    target 269
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_362"
      target_id "M115_184"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 636
    source 270
    target 269
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_305"
      target_id "M115_184"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 637
    source 269
    target 271
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_184"
      target_id "M115_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 638
    source 4
    target 272
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_382"
      target_id "M115_138"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 639
    source 273
    target 272
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_250"
      target_id "M115_138"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 640
    source 272
    target 274
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_138"
      target_id "M115_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 641
    source 33
    target 275
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_385"
      target_id "M115_215"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 642
    source 37
    target 275
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_336"
      target_id "M115_215"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 643
    source 275
    target 276
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_215"
      target_id "M115_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 644
    source 31
    target 277
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_264"
      target_id "M115_162"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 645
    source 278
    target 277
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_282"
      target_id "M115_162"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 646
    source 277
    target 279
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_162"
      target_id "M115_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 647
    source 120
    target 280
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_99"
      target_id "M115_186"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 648
    source 281
    target 280
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_306"
      target_id "M115_186"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 649
    source 280
    target 282
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_186"
      target_id "M115_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 650
    source 57
    target 283
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_253"
      target_id "M115_158"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 651
    source 284
    target 283
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_278"
      target_id "M115_158"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 652
    source 283
    target 285
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_158"
      target_id "M115_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 653
    source 206
    target 286
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_266"
      target_id "M115_125"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 654
    source 194
    target 286
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_265"
      target_id "M115_125"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 655
    source 94
    target 286
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_376"
      target_id "M115_125"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 656
    source 286
    target 287
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_125"
      target_id "M115_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 657
    source 288
    target 289
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_363"
      target_id "M115_223"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 658
    source 290
    target 289
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_350"
      target_id "M115_223"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 659
    source 289
    target 291
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_223"
      target_id "M115_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 660
    source 71
    target 292
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_284"
      target_id "M115_170"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 661
    source 293
    target 292
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_291"
      target_id "M115_170"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 662
    source 292
    target 294
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_170"
      target_id "M115_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 663
    source 71
    target 295
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_284"
      target_id "M115_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 664
    source 296
    target 295
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_296"
      target_id "M115_175"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 665
    source 295
    target 297
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_175"
      target_id "M115_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 666
    source 193
    target 298
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_51"
      target_id "M115_134"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 667
    source 67
    target 298
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_257"
      target_id "M115_134"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 668
    source 298
    target 299
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_134"
      target_id "M115_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 669
    source 110
    target 300
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_308"
      target_id "M115_189"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 670
    source 301
    target 300
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_309"
      target_id "M115_189"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 671
    source 300
    target 302
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_189"
      target_id "M115_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 672
    source 108
    target 303
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_370"
      target_id "M115_177"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 673
    source 167
    target 303
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_298"
      target_id "M115_177"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 674
    source 303
    target 304
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_177"
      target_id "M115_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 675
    source 108
    target 305
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_370"
      target_id "M115_201"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 676
    source 155
    target 305
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_321"
      target_id "M115_201"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 677
    source 305
    target 306
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_201"
      target_id "M115_114"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 678
    source 307
    target 308
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_325"
      target_id "M115_207"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 679
    source 178
    target 308
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_283"
      target_id "M115_207"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 680
    source 33
    target 308
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_385"
      target_id "M115_207"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 681
    source 308
    target 309
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_207"
      target_id "M115_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 682
    source 89
    target 310
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_299"
      target_id "M115_180"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 683
    source 311
    target 310
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_301"
      target_id "M115_180"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 684
    source 310
    target 312
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_180"
      target_id "M115_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 685
    source 313
    target 314
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_371"
      target_id "M115_236"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 686
    source 315
    target 314
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "M115_372"
      target_id "M115_236"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 687
    source 316
    target 314
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "BOOLEAN_LOGIC_GATE_AND"
      source_id "M115_374"
      target_id "M115_236"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 688
    source 314
    target 19
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_236"
      target_id "M115_373"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 689
    source 104
    target 317
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_365"
      target_id "M115_150"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 690
    source 4
    target 317
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_382"
      target_id "M115_150"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 691
    source 317
    target 318
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_150"
      target_id "M115_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 692
    source 89
    target 319
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_299"
      target_id "M115_181"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 693
    source 320
    target 319
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_302"
      target_id "M115_181"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 694
    source 319
    target 321
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_181"
      target_id "M115_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 695
    source 246
    target 322
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_381"
      target_id "M115_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 696
    source 323
    target 322
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_252"
      target_id "M115_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 697
    source 324
    target 322
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_251"
      target_id "M115_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 698
    source 322
    target 325
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_120"
      target_id "M115_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 699
    source 193
    target 326
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_51"
      target_id "M115_122"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 700
    source 326
    target 29
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_122"
      target_id "M115_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 701
    source 193
    target 327
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_51"
      target_id "M115_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 702
    source 327
    target 328
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_123"
      target_id "M115_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 703
    source 35
    target 329
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_332"
      target_id "M115_212"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 704
    source 330
    target 329
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_333"
      target_id "M115_212"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 705
    source 329
    target 331
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_212"
      target_id "M115_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 706
    source 33
    target 332
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_385"
      target_id "M115_219"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 707
    source 333
    target 332
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_342"
      target_id "M115_219"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 708
    source 332
    target 334
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_219"
      target_id "M115_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 709
    source 108
    target 335
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_370"
      target_id "M115_191"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 710
    source 261
    target 335
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_311"
      target_id "M115_191"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 711
    source 335
    target 336
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_191"
      target_id "M115_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 712
    source 233
    target 337
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_48"
      target_id "M115_139"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 713
    source 268
    target 337
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_47"
      target_id "M115_139"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 714
    source 337
    target 14
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_139"
      target_id "M115_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 715
    source 82
    target 338
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_368"
      target_id "M115_147"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 716
    source 55
    target 338
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_379"
      target_id "M115_147"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 717
    source 338
    target 339
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_147"
      target_id "M115_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 718
    source 85
    target 340
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_313"
      target_id "M115_196"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 719
    source 341
    target 340
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_316"
      target_id "M115_196"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 720
    source 340
    target 342
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_196"
      target_id "M115_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 721
    source 343
    target 344
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_369"
      target_id "M115_222"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 722
    source 345
    target 344
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_349"
      target_id "M115_222"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 723
    source 344
    target 346
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_222"
      target_id "M115_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 724
    source 33
    target 347
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_385"
      target_id "M115_131"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 725
    source 41
    target 347
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_1"
      target_id "M115_131"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 726
    source 347
    target 348
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_131"
      target_id "M115_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 727
    source 349
    target 350
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_255"
      target_id "M115_136"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 728
    source 193
    target 350
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_51"
      target_id "M115_136"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 729
    source 350
    target 351
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_136"
      target_id "M115_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 730
    source 33
    target 352
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_385"
      target_id "M115_205"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 731
    source 353
    target 352
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_323"
      target_id "M115_205"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 732
    source 352
    target 354
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_205"
      target_id "M115_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 733
    source 290
    target 355
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_350"
      target_id "M115_224"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 734
    source 356
    target 355
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_351"
      target_id "M115_224"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 735
    source 355
    target 357
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_224"
      target_id "M115_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 736
    source 89
    target 358
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_299"
      target_id "M115_182"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 737
    source 359
    target 358
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_303"
      target_id "M115_182"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 738
    source 358
    target 360
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_182"
      target_id "M115_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 739
    source 33
    target 361
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_385"
      target_id "M115_213"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 740
    source 362
    target 361
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_334"
      target_id "M115_213"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 741
    source 361
    target 363
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_213"
      target_id "M115_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 742
    source 89
    target 364
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_299"
      target_id "M115_183"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 743
    source 365
    target 364
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_304"
      target_id "M115_183"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 744
    source 364
    target 366
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_183"
      target_id "M115_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 745
    source 73
    target 367
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_289"
      target_id "M115_169"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 746
    source 368
    target 367
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_290"
      target_id "M115_169"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 747
    source 367
    target 369
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_169"
      target_id "M115_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 748
    source 54
    target 370
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_239"
      target_id "M115_121"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 749
    source 370
    target 190
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_121"
      target_id "M115_254"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 750
    source 71
    target 371
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_284"
      target_id "M115_171"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 751
    source 372
    target 371
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_292"
      target_id "M115_171"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 752
    source 371
    target 373
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_171"
      target_id "M115_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 753
    source 100
    target 374
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_307"
      target_id "M115_187"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 754
    source 41
    target 374
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_1"
      target_id "M115_187"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 755
    source 375
    target 374
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_345"
      target_id "M115_187"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 756
    source 376
    target 374
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_346"
      target_id "M115_187"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 757
    source 96
    target 374
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_347"
      target_id "M115_187"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 758
    source 374
    target 1
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_187"
      target_id "M115_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 759
    source 85
    target 377
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_313"
      target_id "M115_199"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 760
    source 378
    target 377
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_319"
      target_id "M115_199"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 761
    source 377
    target 379
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_199"
      target_id "M115_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 762
    source 31
    target 380
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_264"
      target_id "M115_161"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 763
    source 381
    target 380
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_281"
      target_id "M115_161"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 764
    source 380
    target 382
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_161"
      target_id "M115_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 765
    source 85
    target 383
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_313"
      target_id "M115_195"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 766
    source 384
    target 383
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_315"
      target_id "M115_195"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 767
    source 383
    target 385
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_195"
      target_id "M115_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
