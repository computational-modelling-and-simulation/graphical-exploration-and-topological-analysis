# generated with VANTED V2.8.2 at Fri Mar 04 09:53:04 AEDT 2022
graph [
  graphbackgroundcolor "#ffffff"
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:wikidata:Q582356"
      hgnc "NA"
      map_id "W21_18"
      name "Interferon_space_gamma_minus_1b"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "acad9"
      uniprot "NA"
    ]
    graphics [
      x 1070.0131216064726
      y 85.55656341717793
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306;PUBMED:15580300"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_132"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "f6ee1"
      uniprot "NA"
    ]
    graphics [
      x 928.8780335233982
      y 62.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000115129"
      hgnc "NA"
      map_id "W21_34"
      name "P53"
      node_subtype "GENE"
      node_type "species"
      org_id "b47f9"
      uniprot "NA"
    ]
    graphics [
      x 795.5732854813464
      y 111.22915621923084
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000131323;urn:miriam:ensembl:ENSG00000102871;urn:miriam:ensembl:ENSG00000127191;urn:miriam:ensembl:ENSG00000175104;urn:miriam:ensembl:ENSG00000082512;urn:miriam:ensembl:ENSG00000088888"
      hgnc "NA"
      map_id "W21_110"
      name "ea43c"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ea43c"
      uniprot "NA"
    ]
    graphics [
      x 1022.9504714564965
      y 2207.2745769001694
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_49"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "ba969"
      uniprot "NA"
    ]
    graphics [
      x 910.3720725999153
      y 2256.5044043004855
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000136560"
      hgnc "NA"
      map_id "W21_116"
      name "TANK"
      node_subtype "GENE"
      node_type "species"
      org_id "ede74"
      uniprot "NA"
    ]
    graphics [
      x 811.5258279175179
      y 2179.495572458395
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "W21_72"
      name "M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "cedc7"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 1430.1619195911949
      y 786.0474898441109
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "PUBMED:33372174;PUBMED:33101306"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_87"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "db7f5"
      uniprot "NA"
    ]
    graphics [
      x 1607.8484337813475
      y 663.471588024117
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ncbigene:424185"
      hgnc "NA"
      map_id "W21_35"
      name "MDA5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b4a31"
      uniprot "NA"
    ]
    graphics [
      x 1718.1187328533192
      y 749.8824910534479
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ncbiprotein:YP_009742611"
      hgnc "NA"
      map_id "W21_117"
      name "NSP4"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "ee721"
      uniprot "NA"
    ]
    graphics [
      x 1210.8998755572693
      y 1696.732784340381
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_119"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "f012f"
      uniprot "NA"
    ]
    graphics [
      x 1160.3225051429115
      y 1576.5878674325086
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000121060"
      hgnc "NA"
      map_id "W21_112"
      name "TRIM25"
      node_subtype "GENE"
      node_type "species"
      org_id "eb329"
      uniprot "NA"
    ]
    graphics [
      x 1195.4692932360963
      y 1433.0561067714582
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:wikidata:Q106020256"
      hgnc "NA"
      map_id "W21_53"
      name "S1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "bcd14"
      uniprot "NA"
    ]
    graphics [
      x 953.7619883717274
      y 967.2426190125086
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_38"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "b66f3"
      uniprot "NA"
    ]
    graphics [
      x 875.1482651362512
      y 1101.711635601931
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:P35613"
      hgnc "NA"
      map_id "W21_15"
      name "BSG"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "aa59a"
      uniprot "UNIPROT:P35613"
    ]
    graphics [
      x 786.3200797111369
      y 1184.820410305589
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:Q87917582"
      hgnc "NA"
      map_id "W21_17"
      name "NSP5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "abe5a"
      uniprot "UNIPROT:Q87917582"
    ]
    graphics [
      x 622.852044842573
      y 663.7522581552464
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306;PUBMED:32353859"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_128"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "f6131"
      uniprot "NA"
    ]
    graphics [
      x 558.9821727276435
      y 759.6202960174239
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_128"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000196591"
      hgnc "NA"
      map_id "W21_151"
      name "HDAC2"
      node_subtype "GENE"
      node_type "species"
      org_id "fdaa0"
      uniprot "NA"
    ]
    graphics [
      x 510.6999519986765
      y 875.4355732822501
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_151"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:pfam:PF00143"
      hgnc "NA"
      map_id "W21_47"
      name "IFN_minus_I"
      node_subtype "GENE"
      node_type "species"
      org_id "b9e39"
      uniprot "NA"
    ]
    graphics [
      x 2042.7905837780318
      y 1760.240571627318
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "PUBMED:17490702;PUBMED:33335518"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_157"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "idde2be3e1"
      uniprot "NA"
    ]
    graphics [
      x 2186.147684542904
      y 1777.9900358784257
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_157"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:P0DTC9"
      hgnc "NA"
      map_id "W21_26"
      name "N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b0f05"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 2156.24968475486
      y 1670.1087405554877
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000109320"
      hgnc "NA"
      map_id "W21_88"
      name "NFKB"
      node_subtype "GENE"
      node_type "species"
      org_id "dcaa7"
      uniprot "NA"
    ]
    graphics [
      x 2151.129319536168
      y 1604.0951858351314
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:pfam:PF00143"
      hgnc "NA"
      map_id "W21_44"
      name "IFN_minus_I"
      node_subtype "GENE"
      node_type "species"
      org_id "b8a45"
      uniprot "NA"
    ]
    graphics [
      x 2082.5978861281574
      y 1848.5100575847807
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000137203"
      hgnc "NA"
      map_id "W21_141"
      name "TFAP2A"
      node_subtype "GENE"
      node_type "species"
      org_id "fb0d0"
      uniprot "NA"
    ]
    graphics [
      x 62.5
      y 1000.2671941918255
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_141"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_135"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "f8c93"
      uniprot "NA"
    ]
    graphics [
      x 127.628185404953
      y 898.8019067168345
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_135"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_136"
      name "Regulation_space_of_space_MHC_space_II_space__br_expression_space_and_space_cytokine_space_production"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "f8f8f"
      uniprot "NA"
    ]
    graphics [
      x 250.5281181879285
      y 890.2664689483452
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_136"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000180871"
      hgnc "NA"
      map_id "W21_45"
      name "CXCR2"
      node_subtype "GENE"
      node_type "species"
      org_id "b9244"
      uniprot "NA"
    ]
    graphics [
      x 1903.4462326962932
      y 1412.7261370345195
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_94"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "e021e"
      uniprot "NA"
    ]
    graphics [
      x 2050.522833188015
      y 1361.7248478285155
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_9"
      name "NETosis"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "a5b14"
      uniprot "NA"
    ]
    graphics [
      x 2168.0312267468225
      y 1292.128740989824
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:wikidata:Q22327223"
      hgnc "NA"
      map_id "W21_123"
      name "IgA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "f3c02"
      uniprot "NA"
    ]
    graphics [
      x 1232.2169700061295
      y 991.6389353255154
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_123"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      annotation "PUBMED:33391280"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_63"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "c830f"
      uniprot "NA"
    ]
    graphics [
      x 1100.3902916998802
      y 993.5250559334954
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_95"
      name "Cytotoxic_space_Function"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "e0973"
      uniprot "NA"
    ]
    graphics [
      x 766.2374753924496
      y 1063.1266914155672
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_67"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "cb2d0"
      uniprot "NA"
    ]
    graphics [
      x 652.3528863827204
      y 962.0760043563506
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000117281"
      hgnc "NA"
      map_id "W21_106"
      name "CD16"
      node_subtype "GENE"
      node_type "species"
      org_id "e8ca6"
      uniprot "NA"
    ]
    graphics [
      x 731.9557773807066
      y 870.1137344910763
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_73"
      name "NA"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "cef7e"
      uniprot "NA"
    ]
    graphics [
      x 403.7202289483804
      y 1431.815843484747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306;PUBMED:33335518"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_91"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "de9d0"
      uniprot "NA"
    ]
    graphics [
      x 289.7836329766284
      y 1411.7970745661903
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:P01375;urn:miriam:uniprot:P22301;urn:miriam:uniprot:B4DNV3;urn:miriam:ensembl:ENSG00000271503;urn:miriam:uniprot:P04141"
      hgnc "NA"
      map_id "W21_54"
      name "bd84b"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "bd84b"
      uniprot "UNIPROT:P01375;UNIPROT:P22301;UNIPROT:B4DNV3;UNIPROT:P04141"
    ]
    graphics [
      x 365.86378560593073
      y 1320.6393395852715
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000183735"
      hgnc "NA"
      map_id "W21_142"
      name "TBK1"
      node_subtype "GENE"
      node_type "species"
      org_id "fbafc"
      uniprot "NA"
    ]
    graphics [
      x 867.521882780863
      y 1876.2614260508244
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_142"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_144"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "fc186"
      uniprot "NA"
    ]
    graphics [
      x 1019.5624789576316
      y 1922.833236179522
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_144"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000126456"
      hgnc "NA"
      map_id "W21_86"
      name "IRF3"
      node_subtype "GENE"
      node_type "species"
      org_id "db31f"
      uniprot "NA"
    ]
    graphics [
      x 1166.768734009872
      y 1931.0291371290064
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306;PUBMED:33335518"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_121"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "f12ae"
      uniprot "NA"
    ]
    graphics [
      x 1690.3031644472146
      y 904.8221147836019
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_121"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:P0DTD2"
      hgnc "NA"
      map_id "W21_124"
      name "ORF9b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f4882"
      uniprot "UNIPROT:P0DTD2"
    ]
    graphics [
      x 1652.1141781252804
      y 1045.797195456433
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_124"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000088888"
      hgnc "NA"
      map_id "W21_24"
      name "MAVS"
      node_subtype "GENE"
      node_type "species"
      org_id "ae650"
      uniprot "NA"
    ]
    graphics [
      x 1592.1015980599352
      y 997.1065152736537
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000105329"
      hgnc "NA"
      map_id "W21_120"
      name "TGFB1"
      node_subtype "GENE"
      node_type "species"
      org_id "f062c"
      uniprot "NA"
    ]
    graphics [
      x 1282.646013929986
      y 778.2147583438827
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "PUBMED:33391280"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_52"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "bcb8e"
      uniprot "NA"
    ]
    graphics [
      x 1355.7035538853065
      y 906.4899974691765
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:P0DTC9"
      hgnc "NA"
      map_id "W21_41"
      name "N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b6d08"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 1537.7517904057659
      y 2019.9735353981623
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_69"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "cc670"
      uniprot "NA"
    ]
    graphics [
      x 1646.002458085005
      y 2030.7953003641219
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000155506"
      hgnc "NA"
      map_id "W21_43"
      name "LARP1"
      node_subtype "GENE"
      node_type "species"
      org_id "b7808"
      uniprot "NA"
    ]
    graphics [
      x 1742.3308278801055
      y 1971.0924156984952
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_150"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "fd934"
      uniprot "NA"
    ]
    graphics [
      x 1306.6143405449843
      y 1911.968974674507
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_150"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000126456"
      hgnc "NA"
      map_id "W21_98"
      name "IRF3"
      node_subtype "GENE"
      node_type "species"
      org_id "e366a"
      uniprot "NA"
    ]
    graphics [
      x 1400.271490689583
      y 1814.03029511377
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_68"
      name "dsRNA"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "cb56c"
      uniprot "NA"
    ]
    graphics [
      x 1455.033908827211
      y 876.2216410524074
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      annotation "PUBMED:26579391;PUBMED:33335518"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_100"
      name "NA"
      node_subtype "UNKNOWN_POSITIVE_INFLUENCE"
      node_type "reaction"
      org_id "e4ea3"
      uniprot "NA"
    ]
    graphics [
      x 1394.0062564018053
      y 1017.000584317267
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000107201"
      hgnc "NA"
      map_id "W21_6"
      name "DDX58"
      node_subtype "GENE"
      node_type "species"
      org_id "a397d"
      uniprot "NA"
    ]
    graphics [
      x 1355.2234879904488
      y 1129.8943814776117
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_7"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "a4481"
      uniprot "NA"
    ]
    graphics [
      x 953.2663411715041
      y 1084.099954866263
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:A0N0L5"
      hgnc "NA"
      map_id "W21_12"
      name "IL_minus_6R"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a8797"
      uniprot "UNIPROT:A0N0L5"
    ]
    graphics [
      x 1167.6744688841125
      y 1129.5536975462178
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000170581;urn:miriam:ensembl:ENSG00000115415"
      hgnc "NA"
      map_id "W21_75"
      name "cfa40"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "cfa40"
      uniprot "NA"
    ]
    graphics [
      x 1489.5074393952673
      y 133.26956478715022
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      annotation "PUBMED:33335518"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_82"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "d6494"
      uniprot "NA"
    ]
    graphics [
      x 1558.259947232145
      y 264.5933457109908
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:Q89226299;urn:miriam:uniprot:YP_009825053"
      hgnc "NA"
      map_id "W21_111"
      name "ea56d"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ea56d"
      uniprot "UNIPROT:Q89226299;UNIPROT:YP_009825053"
    ]
    graphics [
      x 1457.5529701776613
      y 249.31897388773746
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_19"
      name "ISGs"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "ad145"
      uniprot "NA"
    ]
    graphics [
      x 1469.9886479458182
      y 414.4658484414289
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000163737;urn:miriam:ncbigene:2920;urn:miriam:ensembl:ENSG00000169429;urn:miriam:ensembl:ENSG00000169248;urn:miriam:ensembl:ENSG00000163736;urn:miriam:ensembl:ENSG00000156234;urn:miriam:ensembl:ENSG00000275302;urn:miriam:ensembl:ENSG00000108691;urn:miriam:ensembl:ENSG00000107562;urn:miriam:ensembl:ENSG00000169245;urn:miriam:ensembl:ENSG00000163735;urn:miriam:ensembl:ENSG00000163734;urn:miriam:ncbigene:6372;urn:miriam:ensembl:ENSG00000163739;urn:miriam:ensembl:ENSG00000277632;urn:miriam:ensembl:ENSG00000138755"
      hgnc "NA"
      map_id "W21_147"
      name "fcec7"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "fcec7"
      uniprot "NA"
    ]
    graphics [
      x 2133.322495080897
      y 744.66588782452
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_147"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_131"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "f692c"
      uniprot "NA"
    ]
    graphics [
      x 2055.2393552931817
      y 881.6465313864105
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_131"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_101"
      name "Neutrophil_space_recruitment"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "e62ae"
      uniprot "NA"
    ]
    graphics [
      x 1962.7507527644666
      y 972.3026056404435
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_76"
      name "NA"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "cfc78"
      uniprot "NA"
    ]
    graphics [
      x 1992.5046826079306
      y 554.5051070217205
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      annotation "PUBMED:32416070;PUBMED:33335518"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_59"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "c290b"
      uniprot "NA"
    ]
    graphics [
      x 2112.669148090642
      y 606.2177934966148
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_5"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "a3844"
      uniprot "NA"
    ]
    graphics [
      x 838.2046397927286
      y 267.0452011293281
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_21"
      name "Antiproliferative_space_activity"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "adf72"
      uniprot "NA"
    ]
    graphics [
      x 851.8217738761837
      y 401.55992449203075
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000185507"
      hgnc "NA"
      map_id "W21_22"
      name "IRF7"
      node_subtype "GENE"
      node_type "species"
      org_id "ae0a7"
      uniprot "NA"
    ]
    graphics [
      x 856.8207555302388
      y 1709.237504802309
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306;PUBMED:32529952"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_126"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "f5bcb"
      uniprot "NA"
    ]
    graphics [
      x 1029.1728582955232
      y 1700.1874369382904
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_126"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:P0DTC3;urn:miriam:uniprot:6ZSL;urn:miriam:uniprot:P0DTC5;urn:miriam:uniprot:P0DTD2;urn:miriam:uniprot:P0DTC9;urn:miriam:uniprot:Q87917579"
      hgnc "NA"
      map_id "W21_80"
      name "d3cf8"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d3cf8"
      uniprot "UNIPROT:P0DTC3;UNIPROT:6ZSL;UNIPROT:P0DTC5;UNIPROT:P0DTD2;UNIPROT:P0DTC9;UNIPROT:Q87917579"
    ]
    graphics [
      x 1298.497058107449
      y 1613.1966355487516
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:B3KNW1"
      hgnc "NA"
      map_id "W21_153"
      name "AP_minus_1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "fdd5c"
      uniprot "UNIPROT:B3KNW1"
    ]
    graphics [
      x 823.4458431271964
      y 1947.767074635026
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_153"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_37"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "b5f9c"
      uniprot "NA"
    ]
    graphics [
      x 935.1220326421885
      y 1864.2684732136713
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:B3KNW1"
      hgnc "NA"
      map_id "W21_4"
      name "AP_minus_1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "a262e"
      uniprot "UNIPROT:B3KNW1"
    ]
    graphics [
      x 1017.6845360955818
      y 1770.1795259451264
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000110713;urn:miriam:ensembl:ENSG00000101146"
      hgnc "NA"
      map_id "W21_118"
      name "ef9ac"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ef9ac"
      uniprot "NA"
    ]
    graphics [
      x 1125.5600205093726
      y 226.5460534463342
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_118"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859;PUBMED:32529952"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_16"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "ab09f"
      uniprot "NA"
    ]
    graphics [
      x 1033.068309025376
      y 141.57972291621934
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:Q89226299"
      hgnc "NA"
      map_id "W21_62"
      name "ORF6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "c6ec9"
      uniprot "UNIPROT:Q89226299"
    ]
    graphics [
      x 993.2560887694874
      y 235.03659165663294
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000162434"
      hgnc "NA"
      map_id "W21_93"
      name "e01fa"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e01fa"
      uniprot "NA"
    ]
    graphics [
      x 1197.5985219906906
      y 237.2130893313
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_2"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "a0568"
      uniprot "NA"
    ]
    graphics [
      x 1344.6506324356428
      y 154.09478177188885
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_129"
      name "Viral_space_particle"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "f62f1"
      uniprot "NA"
    ]
    graphics [
      x 1939.0644811139086
      y 729.0222449754643
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_129"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_20"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "adadb"
      uniprot "NA"
    ]
    graphics [
      x 2042.1800949936141
      y 761.1961467389965
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000130234"
      hgnc "NA"
      map_id "W21_14"
      name "ACE2"
      node_subtype "GENE"
      node_type "species"
      org_id "a9be1"
      uniprot "NA"
    ]
    graphics [
      x 1950.6173546367838
      y 846.0133610017485
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000159110;urn:miriam:ensembl:ENSG00000142166"
      hgnc "NA"
      map_id "W21_105"
      name "e8804"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e8804"
      uniprot "NA"
    ]
    graphics [
      x 786.0703374962591
      y 323.6372239427475
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      annotation "PUBMED:33335518"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_8"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "a4b7d"
      uniprot "NA"
    ]
    graphics [
      x 994.0919910941475
      y 323.66341588653916
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306;PUBMED:33335518"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_57"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "c18f7"
      uniprot "NA"
    ]
    graphics [
      x 1519.4916750655902
      y 1110.4147813716575
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000166949"
      hgnc "NA"
      map_id "W21_137"
      name "SMAD3"
      node_subtype "GENE"
      node_type "species"
      org_id "f99c6"
      uniprot "NA"
    ]
    graphics [
      x 1056.629973629338
      y 2051.391151847095
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_137"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_66"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "c94fa"
      uniprot "NA"
    ]
    graphics [
      x 1139.9141737917766
      y 2133.390242757611
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000166949"
      hgnc "NA"
      map_id "W21_42"
      name "SMAD3"
      node_subtype "GENE"
      node_type "species"
      org_id "b7200"
      uniprot "NA"
    ]
    graphics [
      x 1198.7231318182476
      y 2040.7520458550248
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ncbiprotein:AAL65234"
      hgnc "NA"
      map_id "W21_130"
      name "NKG2A"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f6900"
      uniprot "NA"
    ]
    graphics [
      x 449.35169127758445
      y 762.9748249573861
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_130"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_99"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "e3a24"
      uniprot "NA"
    ]
    graphics [
      x 353.6250859675307
      y 698.4778958578611
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ncbigene:84868;urn:miriam:ncbigene:3902"
      hgnc "NA"
      map_id "W21_40"
      name "b6cf9"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "b6cf9"
      uniprot "NA"
    ]
    graphics [
      x 439.20930449642856
      y 627.8938338279013
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_28"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "b2c03"
      uniprot "NA"
    ]
    graphics [
      x 642.4826941711478
      y 1059.5843356381656
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000104970"
      hgnc "NA"
      map_id "W21_25"
      name "KIR"
      node_subtype "GENE"
      node_type "species"
      org_id "aea75"
      uniprot "NA"
    ]
    graphics [
      x 728.7464640488386
      y 984.7709885379073
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:P0DTC3"
      hgnc "NA"
      map_id "W21_115"
      name "ORF3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "ed8aa"
      uniprot "UNIPROT:P0DTC3"
    ]
    graphics [
      x 1624.5290749509925
      y 725.1472931598234
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      annotation "PUBMED:32555321"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_83"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "d6cd5"
      uniprot "NA"
    ]
    graphics [
      x 1475.9293272772495
      y 688.8934759000858
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:Q14790"
      hgnc "NA"
      map_id "W21_50"
      name "CASP8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "bc51a"
      uniprot "UNIPROT:Q14790"
    ]
    graphics [
      x 1325.1628916592936
      y 665.5316312725154
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:YP_009742617"
      hgnc "NA"
      map_id "W21_155"
      name "NSP10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "fdf94"
      uniprot "UNIPROT:YP_009742617"
    ]
    graphics [
      x 197.81838909596797
      y 1143.4389825801657
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_155"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_113"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "eb412"
      uniprot "NA"
    ]
    graphics [
      x 77.88934534630357
      y 1119.6082198241686
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_85"
      name "DMV"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "d806a"
      uniprot "NA"
    ]
    graphics [
      x 1195.365565674764
      y 858.5913899182001
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306;PUBMED:33335518"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_127"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "f5be7"
      uniprot "NA"
    ]
    graphics [
      x 1313.3681301200472
      y 850.8716637451506
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_127"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000171855"
      hgnc "NA"
      map_id "W21_89"
      name "IFNB1"
      node_subtype "GENE"
      node_type "species"
      org_id "dcfff"
      uniprot "NA"
    ]
    graphics [
      x 563.413838971819
      y 190.74994296747354
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      annotation "PUBMED:33335518"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_29"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "b2c4d"
      uniprot "NA"
    ]
    graphics [
      x 622.51631871645
      y 311.5006154203586
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:P0DTC9"
      hgnc "NA"
      map_id "W21_46"
      name "N"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b9df2"
      uniprot "UNIPROT:P0DTC9"
    ]
    graphics [
      x 568.8352760962447
      y 436.09023397700946
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000189377;urn:miriam:ensembl:ENSG00000169429;urn:miriam:ensembl:ENSG00000081041"
      hgnc "NA"
      map_id "W21_77"
      name "d00c2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d00c2"
      uniprot "NA"
    ]
    graphics [
      x 1606.666672783392
      y 1559.8727222032733
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_96"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "e0d71"
      uniprot "NA"
    ]
    graphics [
      x 1743.7200649783076
      y 1461.5971164259502
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000109320"
      hgnc "NA"
      map_id "W21_145"
      name "NFKB"
      node_subtype "GENE"
      node_type "species"
      org_id "fc824"
      uniprot "NA"
    ]
    graphics [
      x 1766.0367689111622
      y 1538.0859297878098
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_145"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_152"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "fdaed"
      uniprot "NA"
    ]
    graphics [
      x 1976.3865772891165
      y 1540.5039087740329
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_152"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_32"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "b3aaf"
      uniprot "NA"
    ]
    graphics [
      x 1857.8022520430777
      y 1963.6907160401547
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_114"
      name "Protein_space_synthesis"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "ec9ad"
      uniprot "NA"
    ]
    graphics [
      x 1897.196609863408
      y 2063.632387429848
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ncbigene:8519"
      hgnc "NA"
      map_id "W21_13"
      name "IFITM1"
      node_subtype "GENE"
      node_type "species"
      org_id "a93c7"
      uniprot "NA"
    ]
    graphics [
      x 1187.7110214171225
      y 402.8421983298339
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      annotation "PUBMED:33335518"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_30"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "b2f3d"
      uniprot "NA"
    ]
    graphics [
      x 1318.001239768035
      y 376.89451262837633
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ncbigene:8519"
      hgnc "NA"
      map_id "W21_109"
      name "IFITM1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e997b"
      uniprot "NA"
    ]
    graphics [
      x 1219.7047985328704
      y 326.47804327006634
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:B4DNV3"
      hgnc "NA"
      map_id "W21_146"
      name "IL_minus_6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "fcb4b"
      uniprot "UNIPROT:B4DNV3"
    ]
    graphics [
      x 1642.3164599741226
      y 1203.2333069061478
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_146"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_103"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "e782a"
      uniprot "NA"
    ]
    graphics [
      x 1410.514841188251
      y 1191.0631623541367
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_84"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "d7cd0"
      uniprot "NA"
    ]
    graphics [
      x 725.4373357074194
      y 1907.5232965853493
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      annotation "PUBMED:33335518"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_108"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "e97bb"
      uniprot "NA"
    ]
    graphics [
      x 1565.946721469404
      y 719.3925090988728
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000108771"
      hgnc "NA"
      map_id "W21_156"
      name "LGP2"
      node_subtype "GENE"
      node_type "species"
      org_id "ff178"
      uniprot "NA"
    ]
    graphics [
      x 1577.844332269589
      y 578.4248963877781
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_156"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_33"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "b47a6"
      uniprot "NA"
    ]
    graphics [
      x 1278.1686705905465
      y 1285.2130476625384
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000137275;urn:miriam:ensembl:ENSG00000168040"
      hgnc "NA"
      map_id "W21_55"
      name "bf912"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "bf912"
      uniprot "NA"
    ]
    graphics [
      x 1409.91826016824
      y 1464.1749271686044
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_36"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "b4cf0"
      uniprot "NA"
    ]
    graphics [
      x 1521.8646556233157
      y 1538.807498981078
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:A0N0L5"
      hgnc "NA"
      map_id "W21_139"
      name "IL_minus_6R"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "fa884"
      uniprot "UNIPROT:A0N0L5"
    ]
    graphics [
      x 1970.9329952523954
      y 1199.656893444912
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_139"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_97"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "e32a1"
      uniprot "NA"
    ]
    graphics [
      x 2105.7247805643424
      y 1189.206475690672
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      annotation "PUBMED:33372174;PUBMED:33101306"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_48"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "ba78e"
      uniprot "NA"
    ]
    graphics [
      x 1342.147872985066
      y 966.6471244836177
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_143"
      name "Viral_space_particle"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "fbdf9"
      uniprot "NA"
    ]
    graphics [
      x 1079.0120057592521
      y 741.7701258138893
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_143"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_154"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "fdd8b"
      uniprot "NA"
    ]
    graphics [
      x 969.4795818007186
      y 829.155953656221
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_154"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_92"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "df020"
      uniprot "NA"
    ]
    graphics [
      x 662.0496739381678
      y 114.59620287038126
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000166949;urn:miriam:ensembl:ENSG00000100393"
      hgnc "NA"
      map_id "W21_61"
      name "c69be"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "c69be"
      uniprot "NA"
    ]
    graphics [
      x 727.416318899993
      y 1519.8890686936957
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_149"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "fd17e"
      uniprot "NA"
    ]
    graphics [
      x 839.1434332265997
      y 1514.3842063449038
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_149"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:wikipathways:WP3624"
      hgnc "NA"
      map_id "W21_79"
      name "Lung_space_fibrosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "d2969"
      uniprot "NA"
    ]
    graphics [
      x 946.7898161406551
      y 1442.8997900780034
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      annotation "PUBMED:32416070;PUBMED:31986264"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_58"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "c1910"
      uniprot "NA"
    ]
    graphics [
      x 1816.673420853272
      y 1217.085891095392
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_90"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "de802"
      uniprot "NA"
    ]
    graphics [
      x 1158.0900993851315
      y 695.2297679494492
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:wikipathways:WP254"
      hgnc "NA"
      map_id "W21_11"
      name "Apoptosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "a6ff9"
      uniprot "NA"
    ]
    graphics [
      x 1034.225471183005
      y 675.1749172837535
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ncbigene:4599"
      hgnc "NA"
      map_id "W21_102"
      name "MX1"
      node_subtype "GENE"
      node_type "species"
      org_id "e7804"
      uniprot "NA"
    ]
    graphics [
      x 1489.2833477022957
      y 788.2753594503231
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      annotation "PUBMED:33335518"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_104"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "e849d"
      uniprot "NA"
    ]
    graphics [
      x 1483.198037983324
      y 619.9601126480386
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ncbigene:4599"
      hgnc "NA"
      map_id "W21_39"
      name "MX1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "b67c2"
      uniprot "NA"
    ]
    graphics [
      x 1548.8383396473946
      y 785.6473523709905
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:wikidata:Q94647436"
      hgnc "NA"
      map_id "W21_74"
      name "NSP12"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "cf31a"
      uniprot "NA"
    ]
    graphics [
      x 832.748568083021
      y 1361.07203915799
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_125"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "f5211"
      uniprot "NA"
    ]
    graphics [
      x 956.1735276860279
      y 1353.9795100418035
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_125"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000137275"
      hgnc "NA"
      map_id "W21_134"
      name "RIPK1"
      node_subtype "GENE"
      node_type "species"
      org_id "f7e89"
      uniprot "NA"
    ]
    graphics [
      x 1103.1643831548681
      y 1392.649436740054
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_134"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:pfam:PF15177"
      hgnc "NA"
      map_id "W21_23"
      name "IFN_minus_III"
      node_subtype "GENE"
      node_type "species"
      org_id "ae4ce"
      uniprot "NA"
    ]
    graphics [
      x 2092.447507184086
      y 1546.1610575571744
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      annotation "PUBMED:17490702;PUBMED:33335518"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_3"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "a0f18"
      uniprot "NA"
    ]
    graphics [
      x 2225.961035888415
      y 1529.3202695394202
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:pfam:PF15177"
      hgnc "NA"
      map_id "W21_31"
      name "IFN_minus_III"
      node_subtype "GENE"
      node_type "species"
      org_id "b3038"
      uniprot "NA"
    ]
    graphics [
      x 2236.083599054386
      y 1646.518150163135
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_64"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "c8785"
      uniprot "NA"
    ]
    graphics [
      x 705.1565902988909
      y 1713.3361373725315
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000185507"
      hgnc "NA"
      map_id "W21_81"
      name "IRF7"
      node_subtype "GENE"
      node_type "species"
      org_id "d524f"
      uniprot "NA"
    ]
    graphics [
      x 583.1540042579703
      y 1693.109665059844
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306;PUBMED:19430490;PUBMED:33335518"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_27"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "b21db"
      uniprot "NA"
    ]
    graphics [
      x 1025.549096602591
      y 1077.6798604902124
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000130234"
      hgnc "NA"
      map_id "W21_122"
      name "ACE2"
      node_subtype "GENE"
      node_type "species"
      org_id "f1b6b"
      uniprot "NA"
    ]
    graphics [
      x 1033.3668564669583
      y 1195.9609613280247
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_122"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_51"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "bc934"
      uniprot "NA"
    ]
    graphics [
      x 1751.2010199352994
      y 1094.724774827014
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:A0N0L5"
      hgnc "NA"
      map_id "W21_133"
      name "IL_minus_6R"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f7e47"
      uniprot "UNIPROT:A0N0L5"
    ]
    graphics [
      x 1776.9220554560256
      y 956.1013126039333
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_133"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_10"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "a69de"
      uniprot "NA"
    ]
    graphics [
      x 812.073152424936
      y 2045.3625607243182
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_148"
      name "NA"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "fd08c"
      uniprot "NA"
    ]
    graphics [
      x 1285.1177711854164
      y 1558.4910257005145
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_148"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_71"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ce121"
      uniprot "NA"
    ]
    graphics [
      x 1441.8330454053757
      y 1595.6245446734688
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_78"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "d1890"
      uniprot "NA"
    ]
    graphics [
      x 1271.5312858811083
      y 1419.5355007380476
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:wikidata:Q22327229"
      hgnc "NA"
      map_id "W21_107"
      name "IgM"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "e94ab"
      uniprot "NA"
    ]
    graphics [
      x 1713.3135968307058
      y 1778.2803766159918
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      annotation "PUBMED:33391280"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_65"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "c8a5d"
      uniprot "NA"
    ]
    graphics [
      x 1823.7804688259316
      y 1792.5664787164908
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_60"
      name "RBD_br_"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "c59c3"
      uniprot "NA"
    ]
    graphics [
      x 1824.7081340431216
      y 1682.7075770940473
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ncbigene:3433"
      hgnc "NA"
      map_id "W21_140"
      name "IFIT2"
      node_subtype "GENE"
      node_type "species"
      org_id "facda"
      uniprot "NA"
    ]
    graphics [
      x 1207.9060770878173
      y 496.0381897381319
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_140"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    cd19dm [
      annotation "PUBMED:33335518"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_70"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "ccae3"
      uniprot "NA"
    ]
    graphics [
      x 1337.6908995888214
      y 477.7498133925685
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ncbigene:3433"
      hgnc "NA"
      map_id "W21_138"
      name "IFIT2"
      node_subtype "GENE"
      node_type "species"
      org_id "f9fe4"
      uniprot "NA"
    ]
    graphics [
      x 1242.3211748478577
      y 572.1824544106902
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_138"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    cd19dm [
      annotation "PUBMED:33391280"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_56"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "bfa83"
      uniprot "NA"
    ]
    graphics [
      x 1211.7292000469433
      y 685.8719740255806
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_1"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "a0131"
      uniprot "NA"
    ]
    graphics [
      x 373.0313065217475
      y 904.3878745206152
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 158
    source 1
    target 2
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_18"
      target_id "W21_132"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 159
    source 2
    target 3
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_132"
      target_id "W21_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 160
    source 4
    target 5
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_110"
      target_id "W21_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 161
    source 5
    target 6
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_49"
      target_id "W21_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 162
    source 7
    target 8
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_72"
      target_id "W21_87"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 163
    source 8
    target 9
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_87"
      target_id "W21_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 164
    source 10
    target 11
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_117"
      target_id "W21_119"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 165
    source 11
    target 12
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_119"
      target_id "W21_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 166
    source 13
    target 14
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_53"
      target_id "W21_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 167
    source 14
    target 15
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_38"
      target_id "W21_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 168
    source 16
    target 17
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_17"
      target_id "W21_128"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 169
    source 17
    target 18
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_128"
      target_id "W21_151"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 170
    source 19
    target 20
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_47"
      target_id "W21_157"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 171
    source 21
    target 20
    cd19dm [
      diagram "WP5039"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W21_26"
      target_id "W21_157"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 172
    source 22
    target 20
    cd19dm [
      diagram "WP5039"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W21_88"
      target_id "W21_157"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 173
    source 20
    target 23
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_157"
      target_id "W21_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 174
    source 24
    target 25
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_141"
      target_id "W21_135"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 175
    source 25
    target 26
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_135"
      target_id "W21_136"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 176
    source 27
    target 28
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_45"
      target_id "W21_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 177
    source 28
    target 29
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_94"
      target_id "W21_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 178
    source 30
    target 31
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_123"
      target_id "W21_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 179
    source 31
    target 13
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_63"
      target_id "W21_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 180
    source 32
    target 33
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_95"
      target_id "W21_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 181
    source 33
    target 34
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_67"
      target_id "W21_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 182
    source 35
    target 36
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_73"
      target_id "W21_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 183
    source 36
    target 37
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_91"
      target_id "W21_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 184
    source 38
    target 39
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_142"
      target_id "W21_144"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 185
    source 39
    target 40
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_144"
      target_id "W21_86"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 186
    source 9
    target 41
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_35"
      target_id "W21_121"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 187
    source 42
    target 41
    cd19dm [
      diagram "WP5039"
      edge_type "INHIBITION"
      source_id "W21_124"
      target_id "W21_121"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 188
    source 41
    target 43
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_121"
      target_id "W21_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 189
    source 44
    target 45
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_120"
      target_id "W21_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 190
    source 45
    target 30
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_52"
      target_id "W21_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 191
    source 46
    target 47
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_41"
      target_id "W21_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 192
    source 47
    target 48
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_69"
      target_id "W21_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 193
    source 40
    target 49
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_86"
      target_id "W21_150"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 194
    source 49
    target 50
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_150"
      target_id "W21_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 195
    source 51
    target 52
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_68"
      target_id "W21_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 196
    source 52
    target 53
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_100"
      target_id "W21_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 197
    source 32
    target 54
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_95"
      target_id "W21_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 198
    source 54
    target 55
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_7"
      target_id "W21_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 199
    source 56
    target 57
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_75"
      target_id "W21_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 200
    source 58
    target 57
    cd19dm [
      diagram "WP5039"
      edge_type "INHIBITION"
      source_id "W21_111"
      target_id "W21_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 201
    source 57
    target 59
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_82"
      target_id "W21_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 202
    source 60
    target 61
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_147"
      target_id "W21_131"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 203
    source 61
    target 62
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_131"
      target_id "W21_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 204
    source 63
    target 64
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_76"
      target_id "W21_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 205
    source 64
    target 60
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_59"
      target_id "W21_147"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 206
    source 3
    target 65
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_34"
      target_id "W21_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 207
    source 65
    target 66
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_5"
      target_id "W21_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 208
    source 67
    target 68
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_22"
      target_id "W21_126"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 209
    source 69
    target 68
    cd19dm [
      diagram "WP5039"
      edge_type "INHIBITION"
      source_id "W21_80"
      target_id "W21_126"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 210
    source 68
    target 38
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_126"
      target_id "W21_142"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 211
    source 70
    target 71
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_153"
      target_id "W21_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 212
    source 71
    target 72
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_37"
      target_id "W21_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 213
    source 73
    target 74
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_118"
      target_id "W21_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 214
    source 74
    target 75
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_16"
      target_id "W21_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 215
    source 76
    target 77
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_93"
      target_id "W21_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 216
    source 77
    target 56
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_2"
      target_id "W21_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 217
    source 78
    target 79
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_129"
      target_id "W21_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 218
    source 79
    target 80
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_20"
      target_id "W21_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 219
    source 81
    target 82
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_105"
      target_id "W21_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 220
    source 82
    target 76
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_8"
      target_id "W21_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 221
    source 53
    target 83
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_6"
      target_id "W21_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 222
    source 42
    target 83
    cd19dm [
      diagram "WP5039"
      edge_type "INHIBITION"
      source_id "W21_124"
      target_id "W21_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 223
    source 83
    target 43
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_57"
      target_id "W21_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 224
    source 84
    target 85
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_137"
      target_id "W21_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 225
    source 85
    target 86
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_66"
      target_id "W21_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 226
    source 87
    target 88
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_130"
      target_id "W21_99"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 227
    source 88
    target 89
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_99"
      target_id "W21_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 228
    source 32
    target 90
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_95"
      target_id "W21_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 229
    source 90
    target 91
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_28"
      target_id "W21_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 230
    source 92
    target 93
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_115"
      target_id "W21_83"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 93
    target 94
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_83"
      target_id "W21_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 232
    source 95
    target 96
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_155"
      target_id "W21_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 233
    source 96
    target 24
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_113"
      target_id "W21_141"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 234
    source 97
    target 98
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_85"
      target_id "W21_127"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 235
    source 98
    target 51
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_127"
      target_id "W21_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 236
    source 99
    target 100
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_89"
      target_id "W21_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 237
    source 101
    target 100
    cd19dm [
      diagram "WP5039"
      edge_type "INHIBITION"
      source_id "W21_46"
      target_id "W21_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 238
    source 100
    target 81
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_29"
      target_id "W21_105"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 239
    source 102
    target 103
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_77"
      target_id "W21_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 240
    source 103
    target 27
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_96"
      target_id "W21_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 241
    source 104
    target 105
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_145"
      target_id "W21_152"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 242
    source 105
    target 22
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_152"
      target_id "W21_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 243
    source 48
    target 106
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_43"
      target_id "W21_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 244
    source 106
    target 107
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_32"
      target_id "W21_114"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 245
    source 108
    target 109
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_13"
      target_id "W21_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 246
    source 59
    target 109
    cd19dm [
      diagram "WP5039"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W21_19"
      target_id "W21_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 247
    source 109
    target 110
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_30"
      target_id "W21_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 248
    source 111
    target 112
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_146"
      target_id "W21_103"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 249
    source 112
    target 55
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_103"
      target_id "W21_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 250
    source 38
    target 113
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_142"
      target_id "W21_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 251
    source 113
    target 70
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_84"
      target_id "W21_153"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 252
    source 51
    target 114
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_68"
      target_id "W21_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 253
    source 115
    target 114
    cd19dm [
      diagram "WP5039"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W21_156"
      target_id "W21_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 254
    source 114
    target 9
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_108"
      target_id "W21_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 255
    source 12
    target 116
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_112"
      target_id "W21_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 256
    source 116
    target 53
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_33"
      target_id "W21_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 257
    source 117
    target 118
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_55"
      target_id "W21_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 258
    source 69
    target 118
    cd19dm [
      diagram "WP5039"
      edge_type "INHIBITION"
      source_id "W21_80"
      target_id "W21_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 259
    source 118
    target 104
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_36"
      target_id "W21_145"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 260
    source 119
    target 120
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_139"
      target_id "W21_97"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 261
    source 120
    target 29
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_97"
      target_id "W21_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 262
    source 7
    target 121
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_72"
      target_id "W21_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 263
    source 121
    target 53
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_48"
      target_id "W21_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 264
    source 122
    target 123
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_143"
      target_id "W21_154"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 265
    source 123
    target 13
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_154"
      target_id "W21_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 266
    source 99
    target 124
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_89"
      target_id "W21_92"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 267
    source 124
    target 3
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_92"
      target_id "W21_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 268
    source 125
    target 126
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_61"
      target_id "W21_149"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 269
    source 126
    target 127
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_149"
      target_id "W21_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 270
    source 111
    target 128
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_146"
      target_id "W21_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 271
    source 128
    target 119
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_58"
      target_id "W21_139"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 272
    source 94
    target 129
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_50"
      target_id "W21_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 273
    source 129
    target 130
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_90"
      target_id "W21_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 274
    source 131
    target 132
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_102"
      target_id "W21_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 275
    source 59
    target 132
    cd19dm [
      diagram "WP5039"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W21_19"
      target_id "W21_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 276
    source 132
    target 133
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_104"
      target_id "W21_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 277
    source 134
    target 135
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_74"
      target_id "W21_125"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 278
    source 135
    target 136
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_125"
      target_id "W21_134"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 279
    source 137
    target 138
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_23"
      target_id "W21_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 280
    source 21
    target 138
    cd19dm [
      diagram "WP5039"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W21_26"
      target_id "W21_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 281
    source 22
    target 138
    cd19dm [
      diagram "WP5039"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W21_88"
      target_id "W21_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 282
    source 138
    target 139
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_3"
      target_id "W21_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 283
    source 67
    target 140
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_22"
      target_id "W21_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 284
    source 140
    target 141
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_64"
      target_id "W21_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 285
    source 13
    target 142
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_53"
      target_id "W21_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 286
    source 142
    target 143
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_27"
      target_id "W21_122"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 287
    source 111
    target 144
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_146"
      target_id "W21_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 288
    source 144
    target 145
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_51"
      target_id "W21_133"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 289
    source 6
    target 146
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_116"
      target_id "W21_10"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 290
    source 146
    target 38
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_10"
      target_id "W21_142"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 291
    source 147
    target 148
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_148"
      target_id "W21_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 292
    source 148
    target 102
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_71"
      target_id "W21_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 293
    source 136
    target 149
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_134"
      target_id "W21_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 294
    source 149
    target 117
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_78"
      target_id "W21_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 295
    source 150
    target 151
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_107"
      target_id "W21_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 296
    source 151
    target 152
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_65"
      target_id "W21_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 297
    source 153
    target 154
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_140"
      target_id "W21_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 298
    source 59
    target 154
    cd19dm [
      diagram "WP5039"
      edge_type "PHYSICAL_STIMULATION"
      source_id "W21_19"
      target_id "W21_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 299
    source 154
    target 155
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_70"
      target_id "W21_138"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 300
    source 122
    target 156
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_143"
      target_id "W21_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 301
    source 156
    target 44
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_56"
      target_id "W21_120"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 302
    source 18
    target 157
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_151"
      target_id "W21_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 303
    source 157
    target 26
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_1"
      target_id "W21_136"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
