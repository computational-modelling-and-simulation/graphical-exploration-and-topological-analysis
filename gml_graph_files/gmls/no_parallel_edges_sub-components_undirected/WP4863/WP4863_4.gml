# generated with VANTED V2.8.2 at Fri Mar 04 10:04:32 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_13"
      name "EDEMosome"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "c4176"
      uniprot "NA"
    ]
    graphics [
      x 1393.3822990362519
      y 343.80217567577046
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_29"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id1d777c00"
      uniprot "NA"
    ]
    graphics [
      x 1403.3917395294038
      y 462.61168560846613
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_30"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "id26b440cd"
      uniprot "NA"
    ]
    graphics [
      x 1330.3992310011427
      y 242.5172043706031
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_21"
      name "DMV_space__br_double_space_membrane_space__br_vesicle"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "e5087"
      uniprot "NA"
    ]
    graphics [
      x 1214.526290821328
      y 248.35912614448165
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4863"
      full_annotation "NA"
      hgnc "NA"
      map_id "W7_16"
      name "Endoplasmic_space_Reticulum"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "ceb35"
      uniprot "NA"
    ]
    graphics [
      x 1330.2612636129338
      y 551.3231006101919
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W7_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 6
    source 2
    target 1
    cd19dm [
      diagram "WP4863"
      edge_type "PRODUCTION"
      source_id "W7_29"
      target_id "W7_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 7
    source 1
    target 3
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "W7_13"
      target_id "W7_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 8
    source 5
    target 2
    cd19dm [
      diagram "WP4863"
      edge_type "CONSPUMPTION"
      source_id "W7_16"
      target_id "W7_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 9
    source 3
    target 4
    cd19dm [
      diagram "WP4863"
      edge_type "PRODUCTION"
      source_id "W7_30"
      target_id "W7_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
