# generated with VANTED V2.8.2 at Fri Mar 04 10:04:39 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:uniprot:P0DTD1"
      hgnc "NA"
      map_id "W10_67"
      name "nsp7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d2766"
      uniprot "UNIPROT:P0DTD1"
    ]
    graphics [
      x 373.2622358465535
      y 1707.55622977596
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_77"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "dd0be"
      uniprot "NA"
    ]
    graphics [
      x 264.8289540921676
      y 1760.7411431714695
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ensembl:ENSG00000137806"
      hgnc "NA"
      map_id "W10_94"
      name "NDUFAF1"
      node_subtype "GENE"
      node_type "species"
      org_id "ee9df"
      uniprot "NA"
    ]
    graphics [
      x 255.67941333554575
      y 1646.1326146069255
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 4
    source 1
    target 2
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_67"
      target_id "W10_77"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 5
    source 2
    target 3
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_77"
      target_id "W10_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
