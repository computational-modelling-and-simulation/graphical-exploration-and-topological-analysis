# generated with VANTED V2.8.2 at Fri Mar 04 10:04:39 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:uniprot:P0DTC6"
      hgnc "NA"
      map_id "W10_89"
      name "orf6"
      node_subtype "GENE"
      node_type "species"
      org_id "e9623"
      uniprot "UNIPROT:P0DTC6"
    ]
    graphics [
      x 614.7208725482942
      y 1319.4005776084368
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:32979938"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_109"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "id4345b2ab"
      uniprot "NA"
    ]
    graphics [
      x 670.3855512438454
      y 1421.5278154790599
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ensembl:ENSG00000126456;urn:miriam:ensembl:ENSG00000185507"
      hgnc "NA"
      map_id "W10_21"
      name "ab03e"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "ab03e"
      uniprot "NA"
    ]
    graphics [
      x 721.3770245674348
      y 1314.0919102865219
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "PUBMED:21597473;PUBMED:32979938;PUBMED:33019591"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_40"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "bacb0"
      uniprot "NA"
    ]
    graphics [
      x 542.5604560502253
      y 1416.20380804665
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "PUBMED:21597473;PUBMED:33937724"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_82"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "e1040"
      uniprot "NA"
    ]
    graphics [
      x 889.7051611335835
      y 1173.2348470278876
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ncbigene:3438"
      hgnc "NA"
      map_id "W10_95"
      name "IFN_minus_I"
      node_subtype "GENE"
      node_type "species"
      org_id "f26de"
      uniprot "NA"
    ]
    graphics [
      x 1033.5229812635812
      y 1137.8773351685209
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ensembl:ENSG00000080824.19;urn:miriam:ensembl:ENSG00000183735;urn:miriam:ensembl:ENSG00000126456"
      hgnc "NA"
      map_id "W10_73"
      name "d836e"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "d836e"
      uniprot "NA"
    ]
    graphics [
      x 486.6624820524131
      y 1541.1208926773675
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ensembl:ENSG00000263528"
      hgnc "NA"
      map_id "W10_19"
      name "IKBKE"
      node_subtype "GENE"
      node_type "species"
      org_id "aa6d4"
      uniprot "NA"
    ]
    graphics [
      x 427.1307666128539
      y 1366.2839974648664
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ncbiprotein:YP_009725302"
      hgnc "NA"
      map_id "W10_99"
      name "nsp6"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f6f18"
      uniprot "NA"
    ]
    graphics [
      x 483.9066797714437
      y 1309.934651553229
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "PUBMED:33019591"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_32"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "b6c5f"
      uniprot "NA"
    ]
    graphics [
      x 555.2710330446624
      y 1643.5857029787617
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:ensembl:ENSG00000154174"
      hgnc "NA"
      map_id "W10_41"
      name "TOMM70"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "bc950"
      uniprot "NA"
    ]
    graphics [
      x 670.9816345227426
      y 1681.8599303654141
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "PUBMED:32353859;PUBMED:32728199"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_23"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "ac21e"
      uniprot "NA"
    ]
    graphics [
      x 789.1545954326359
      y 1649.2551329557748
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:uniprot:P0DTD2"
      hgnc "NA"
      map_id "W10_87"
      name "ORF9b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "e62df"
      uniprot "UNIPROT:P0DTD2"
    ]
    graphics [
      x 901.9928739455981
      y 1593.6178476368605
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 14
    source 1
    target 2
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_89"
      target_id "W10_109"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 15
    source 2
    target 3
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_109"
      target_id "W10_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 16
    source 4
    target 3
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_40"
      target_id "W10_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 17
    source 3
    target 5
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_21"
      target_id "W10_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 18
    source 7
    target 4
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_73"
      target_id "W10_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 19
    source 8
    target 4
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_19"
      target_id "W10_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 20
    source 9
    target 4
    cd19dm [
      diagram "WP5038"
      edge_type "INHIBITION"
      source_id "W10_99"
      target_id "W10_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 21
    source 5
    target 6
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_82"
      target_id "W10_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 22
    source 10
    target 7
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_32"
      target_id "W10_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 23
    source 11
    target 10
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_41"
      target_id "W10_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 24
    source 12
    target 11
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_23"
      target_id "W10_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 25
    source 13
    target 12
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_87"
      target_id "W10_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
