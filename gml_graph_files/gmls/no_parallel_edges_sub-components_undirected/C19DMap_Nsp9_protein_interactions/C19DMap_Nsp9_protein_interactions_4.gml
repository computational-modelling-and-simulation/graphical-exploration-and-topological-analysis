# generated with VANTED V2.8.2 at Fri Mar 04 10:04:32 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:ensembl:ENSG00000073756;urn:miriam:hgnc:9605;urn:miriam:hgnc.symbol:PTGS2;urn:miriam:hgnc.symbol:PTGS2;urn:miriam:uniprot:P35354;urn:miriam:uniprot:P35354;urn:miriam:refseq:NM_000963;urn:miriam:ncbigene:5743;urn:miriam:ncbigene:5743;urn:miriam:ec-code:1.14.99.1"
      hgnc "HGNC_SYMBOL:PTGS2"
      map_id "M115_274"
      name "PTGS2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1153"
      uniprot "UNIPROT:P35354"
    ]
    graphics [
      x 401.5479375958796
      y 159.14736515853247
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_274"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:20724158;PUBMED:10555907;PUBMED:10693877;PUBMED:10643177;PUBMED:11398914;PUBMED:20166930;PUBMED:10580458;PUBMED:11752352;PUBMED:10566562"
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "NA"
      hgnc "NA"
      map_id "M115_154"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10131"
      uniprot "NA"
    ]
    graphics [
      x 346.80352930878166
      y 260.58009885789545
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_154"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubchem.compound:5090"
      hgnc "NA"
      map_id "M115_275"
      name "Rofecoxib"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa1154"
      uniprot "NA"
    ]
    graphics [
      x 233.7797053001218
      y 226.49274409833026
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_275"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Nsp9 protein interactions"
      full_annotation "urn:miriam:pubmed:20724158;urn:miriam:ensembl:ENSG00000073756;urn:miriam:hgnc:9605;urn:miriam:hgnc.symbol:PTGS2;urn:miriam:hgnc.symbol:PTGS2;urn:miriam:uniprot:P35354;urn:miriam:uniprot:P35354;urn:miriam:refseq:NM_000963;urn:miriam:ncbigene:5743;urn:miriam:ncbigene:5743;urn:miriam:ec-code:1.14.99.1;urn:miriam:pubchem.compound:5090"
      hgnc "HGNC_SYMBOL:PTGS2"
      map_id "M115_69"
      name "PTGScomp"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa54"
      uniprot "UNIPROT:P35354"
    ]
    graphics [
      x 567.7176885891931
      y 419.71366792348556
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M115_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 5
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_274"
      target_id "M115_154"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 6
    source 3
    target 2
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "CONSPUMPTION"
      source_id "M115_275"
      target_id "M115_154"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 7
    source 2
    target 4
    cd19dm [
      diagram "C19DMap:Nsp9 protein interactions"
      edge_type "PRODUCTION"
      source_id "M115_154"
      target_id "M115_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
