# generated with VANTED V2.8.2 at Fri Mar 04 10:04:34 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:Q87917582"
      hgnc "NA"
      map_id "W21_17"
      name "NSP5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "abe5a"
      uniprot "UNIPROT:Q87917582"
    ]
    graphics [
      x 1518.7805189192002
      y 2129.738142841986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306;PUBMED:32353859"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_128"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "f6131"
      uniprot "NA"
    ]
    graphics [
      x 2268.7805189192004
      y 2132.458314370092
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_128"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000196591"
      hgnc "NA"
      map_id "W21_151"
      name "HDAC2"
      node_subtype "GENE"
      node_type "species"
      org_id "fdaa0"
      uniprot "NA"
    ]
    graphics [
      x 2748.7805189192004
      y 2513.9160260944423
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_151"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_1"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "a0131"
      uniprot "NA"
    ]
    graphics [
      x 3242.5
      y 1373.410114080192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_136"
      name "Regulation_space_of_space_MHC_space_II_space__br_expression_space_and_space_cytokine_space_production"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "f8f8f"
      uniprot "NA"
    ]
    graphics [
      x 2107.4327087287393
      y 545.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_136"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_135"
      name "NA"
      node_subtype "REDUCED_PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "f8c93"
      uniprot "NA"
    ]
    graphics [
      x 1354.6167998029002
      y 338.61056790466614
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_135"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:ensembl:ENSG00000137203"
      hgnc "NA"
      map_id "W21_141"
      name "TFAP2A"
      node_subtype "GENE"
      node_type "species"
      org_id "fb0d0"
      uniprot "NA"
    ]
    graphics [
      x 932.5
      y 906.0001579346998
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_141"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "PUBMED:33101306"
      count 1
      diagram "WP5039"
      full_annotation "NA"
      hgnc "NA"
      map_id "W21_113"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "eb412"
      uniprot "NA"
    ]
    graphics [
      x 1839.4000836075752
      y 302.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5039"
      full_annotation "urn:miriam:uniprot:YP_009742617"
      hgnc "NA"
      map_id "W21_155"
      name "NSP10"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "fdf94"
      uniprot "UNIPROT:YP_009742617"
    ]
    graphics [
      x 1292.5
      y 624.9544377376523
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W21_155"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 10
    source 1
    target 2
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_17"
      target_id "W21_128"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 11
    source 2
    target 3
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_128"
      target_id "W21_151"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 12
    source 3
    target 4
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_151"
      target_id "W21_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 13
    source 4
    target 5
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_1"
      target_id "W21_136"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 14
    source 6
    target 5
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_135"
      target_id "W21_136"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 15
    source 7
    target 6
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_141"
      target_id "W21_135"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 16
    source 8
    target 7
    cd19dm [
      diagram "WP5039"
      edge_type "PRODUCTION"
      source_id "W21_113"
      target_id "W21_141"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 17
    source 9
    target 8
    cd19dm [
      diagram "WP5039"
      edge_type "CONSPUMPTION"
      source_id "W21_155"
      target_id "W21_113"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
