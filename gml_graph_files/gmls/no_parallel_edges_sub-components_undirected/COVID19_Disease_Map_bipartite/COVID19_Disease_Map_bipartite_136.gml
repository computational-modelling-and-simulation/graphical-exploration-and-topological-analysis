# generated with VANTED V2.8.2 at Fri Mar 04 10:04:35 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000130234;urn:miriam:ncbigene:59272;urn:miriam:ncbigene:59272;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:pubmed:19411314;urn:miriam:pubmed:15692567;urn:miriam:pubmed:32264791;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:refseq:NM_001371415;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:ACE2"
      map_id "M118_291"
      name "ACE2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa145"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1831.4316445647612
      y 3002.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_291"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "PUBMED:32075877;PUBMED:32225175"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_330"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re134"
      uniprot "NA"
    ]
    graphics [
      x 572.5
      y 1842.95801449575
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_330"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "PUBMED:31775868;PUBMED:30604460"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_237"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "path_1_re100"
      uniprot "NA"
    ]
    graphics [
      x 1631.043215136001
      y 2603.119912305107
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_237"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "PUBMED:14647384"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_341"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re145"
      uniprot "NA"
    ]
    graphics [
      x 1128.7805189192002
      y 2418.4611593984923
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_341"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "PUBMED:31775868;PUBMED:22511781"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_257"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_1_re137"
      uniprot "NA"
    ]
    graphics [
      x 2492.5
      y 1471.435024234829
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_257"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      annotation "PUBMED:19411314;PUBMED:15983030;PUBMED:32264791"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_334"
      name "NA"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "re138"
      uniprot "NA"
    ]
    graphics [
      x 1338.7805189192002
      y 2245.0755883950264
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_334"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "PUBMED:18441099"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_335"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re139"
      uniprot "NA"
    ]
    graphics [
      x 1938.7805189192002
      y 2192.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_335"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      annotation "PUBMED:31775868;PUBMED:22511781;PUBMED:16940539"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_241"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "path_1_re110"
      uniprot "NA"
    ]
    graphics [
      x 1721.043215136001
      y 2667.1855687946227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_241"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:HSPA5;urn:miriam:ensembl:ENSG00000044574;urn:miriam:refseq:NM_005347;urn:miriam:hgnc:5238;urn:miriam:ncbigene:3309;urn:miriam:uniprot:P11021"
      hgnc "HGNC_SYMBOL:HSPA5"
      map_id "M118_289"
      name "HSPA5"
      node_subtype "GENE"
      node_type "species"
      org_id "path_1_sa125"
      uniprot "UNIPROT:P11021"
    ]
    graphics [
      x 662.5
      y 1925.901254732075
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_289"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:1489668;urn:miriam:uniprot:W6A028;urn:miriam:uniprot:P59594;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:S"
      map_id "M118_347"
      name "S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa275"
      uniprot "UNIPROT:P0DTC2;UNIPROT:W6A028;UNIPROT:P59594"
    ]
    graphics [
      x 2201.043215136001
      y 2663.713309864131
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_347"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:HSPA5;urn:miriam:ensembl:ENSG00000044574;urn:miriam:refseq:NM_005347;urn:miriam:hgnc:5238;urn:miriam:ncbigene:3309;urn:miriam:uniprot:P11021"
      hgnc "HGNC_SYMBOL:HSPA5"
      map_id "M118_295"
      name "HSPA5"
      node_subtype "RNA"
      node_type "species"
      org_id "path_1_sa149"
      uniprot "UNIPROT:P11021"
    ]
    graphics [
      x 1322.5
      y 1167.8321207248905
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_295"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      annotation "PUBMED:19411314;PUBMED:15983030"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_336"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re140"
      uniprot "NA"
    ]
    graphics [
      x 3182.5
      y 1607.7501341961922
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_336"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:pubmed:19411314;urn:miriam:pubmed:32264791;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:ACE2"
      map_id "M118_309"
      name "ACE2,_space_membrane_minus_bound"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa178"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 2582.5
      y 1789.6649132197108
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_309"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:pubmed:19411314;urn:miriam:pubmed:32264791;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:ACE2"
      map_id "M118_310"
      name "ACE2,_space_soluble"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa180"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 722.5
      y 1891.8624255493423
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_310"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:19411314;urn:miriam:uniprot:P59594;urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:1489668;urn:miriam:uniprot:W6A028;urn:miriam:uniprot:P59594;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S;urn:miriam:ensembl:ENSG00000130234;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:pubmed:19411314;urn:miriam:pubmed:32264791;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:ncbigene:59272;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:S;HGNC_SYMBOL:ACE2"
      map_id "M118_3"
      name "ACE2_minus_SARS_minus_CoV_space_interaction"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa31"
      uniprot "UNIPROT:P59594;UNIPROT:P0DTC2;UNIPROT:W6A028;UNIPROT:Q9BYF1"
    ]
    graphics [
      x 2852.5
      y 1273.9317909822832
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "PUBMED:19411314"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_337"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re141"
      uniprot "NA"
    ]
    graphics [
      x 2642.5
      y 1637.7682304796128
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_337"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_350"
      name "SARS_minus_CoV_space_infection"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa279"
      uniprot "NA"
    ]
    graphics [
      x 1562.5
      y 1485.4971941397312
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_350"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0046718;urn:miriam:pubmed:19411314"
      hgnc "NA"
      map_id "M118_346"
      name "viral_space_entry_space_into_space_host_space_cell"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa271"
      uniprot "NA"
    ]
    graphics [
      x 2762.5
      y 1945.8267364055584
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_346"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "PUBMED:29887526"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_339"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re143"
      uniprot "NA"
    ]
    graphics [
      x 1927.432708728739
      y 422.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_339"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "PUBMED:19411314"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_338"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re142"
      uniprot "NA"
    ]
    graphics [
      x 1112.5
      y 1977.0116707084726
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_338"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "PUBMED:14647384;PUBMED:19411314"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_340"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "re144"
      uniprot "NA"
    ]
    graphics [
      x 3422.5
      y 2093.079545692044
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_340"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000130234;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:ACE2"
      map_id "M118_348"
      name "ACE2"
      node_subtype "GENE"
      node_type "species"
      org_id "sa277"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 2672.5
      y 1953.7928516911866
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_348"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000130234;urn:miriam:hgnc:13557;urn:miriam:uniprot:Q9BYF1;urn:miriam:ncbigene:59272;urn:miriam:refseq:NM_001371415;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:ACE2"
      map_id "M118_349"
      name "ACE2"
      node_subtype "RNA"
      node_type "species"
      org_id "sa278"
      uniprot "UNIPROT:Q9BYF1"
    ]
    graphics [
      x 2448.7805189192004
      y 2401.435024234829
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_349"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:pubmed:32340551;urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:1489668;urn:miriam:uniprot:W6A028;urn:miriam:uniprot:P59594;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S;urn:miriam:hgnc.symbol:HSPA5;urn:miriam:ensembl:ENSG00000044574;urn:miriam:hgnc.symbol:HSPA5;urn:miriam:refseq:NM_005347;urn:miriam:hgnc:5238;urn:miriam:ncbigene:3309;urn:miriam:ncbigene:3309;urn:miriam:uniprot:P11021;urn:miriam:uniprot:P11021;urn:miriam:ec-code:3.6.4.10"
      hgnc "HGNC_SYMBOL:S;HGNC_SYMBOL:HSPA5"
      map_id "M118_2"
      name "HSPA5_minus_Spike_space_interaction"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa30"
      uniprot "UNIPROT:P0DTC2;UNIPROT:W6A028;UNIPROT:P59594;UNIPROT:P11021"
    ]
    graphics [
      x 2612.5
      y 638.5683310712454
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "PUBMED:32169481;PUBMED:32340551"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_331"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re135"
      uniprot "NA"
    ]
    graphics [
      x 2312.5
      y 699.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_331"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:1489668;urn:miriam:uniprot:W6A028;urn:miriam:uniprot:P59594;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S"
      hgnc "HGNC_SYMBOL:S"
      map_id "M118_343"
      name "S"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa258"
      uniprot "UNIPROT:P0DTC2;UNIPROT:W6A028;UNIPROT:P59594"
    ]
    graphics [
      x 1142.5
      y 667.1154558223394
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_343"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:HSPA5;urn:miriam:ensembl:ENSG00000044574;urn:miriam:hgnc.symbol:HSPA5;urn:miriam:pubmed:32169481;urn:miriam:hgnc:5238;urn:miriam:pubmed:30978349;urn:miriam:refseq:NM_005347;urn:miriam:ncbigene:3309;urn:miriam:ncbigene:3309;urn:miriam:uniprot:P11021;urn:miriam:uniprot:P11021;urn:miriam:pubmed:32340551;urn:miriam:ec-code:3.6.4.10"
      hgnc "HGNC_SYMBOL:HSPA5"
      map_id "M118_344"
      name "HSPA5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa262"
      uniprot "UNIPROT:P11021"
    ]
    graphics [
      x 2042.5
      y 1618.5318911777056
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_344"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:pubmed:18441099;urn:miriam:taxonomy:9606;urn:miriam:mesh:D011658"
      hgnc "NA"
      map_id "M118_345"
      name "pulmonary_space_fibrosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa266"
      uniprot "NA"
    ]
    graphics [
      x 2072.5
      y 1535.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_345"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000151694;urn:miriam:ncbigene:6868;urn:miriam:ncbigene:6868;urn:miriam:refseq:NM_001382777;urn:miriam:ec-code:3.4.24.86;urn:miriam:hgnc:195;urn:miriam:uniprot:P78536;urn:miriam:uniprot:P78536;urn:miriam:pubmed:32264791;urn:miriam:hgnc.symbol:ADAM17;urn:miriam:hgnc.symbol:ADAM17"
      hgnc "HGNC_SYMBOL:ADAM17"
      map_id "M118_306"
      name "ADAM17"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa171"
      uniprot "UNIPROT:P78536"
    ]
    graphics [
      x 962.5
      y 2100.6216893968285
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_306"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      annotation "PUBMED:26108729"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_264"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "path_1_re153"
      uniprot "NA"
    ]
    graphics [
      x 1442.5
      y 1325.1961954122032
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_264"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:32264791;urn:miriam:pubmed:26108729;urn:miriam:ensembl:ENSG00000151694;urn:miriam:ncbigene:6868;urn:miriam:ncbigene:6868;urn:miriam:refseq:NM_001382777;urn:miriam:ec-code:3.4.24.86;urn:miriam:hgnc:195;urn:miriam:uniprot:P78536;urn:miriam:uniprot:P78536;urn:miriam:pubmed:32264791;urn:miriam:hgnc.symbol:ADAM17;urn:miriam:hgnc.symbol:ADAM17;urn:miriam:pubmed:26108729;urn:miriam:hgnc.symbol:PACS2;urn:miriam:hgnc.symbol:PACS2;urn:miriam:ncbigene:23241;urn:miriam:uniprot:Q86VP3;urn:miriam:uniprot:Q86VP3;urn:miriam:ncbigene:23241;urn:miriam:pubmed:15692567;urn:miriam:hgnc:23794;urn:miriam:refseq:NM_001100913;urn:miriam:ensembl:ENSG00000179364"
      hgnc "HGNC_SYMBOL:ADAM17;HGNC_SYMBOL:PACS2"
      map_id "M118_232"
      name "PACS2_minus_ADAM17_space_interaction"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_1_csa18"
      uniprot "UNIPROT:P78536;UNIPROT:Q86VP3"
    ]
    graphics [
      x 1892.5
      y 1922.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_232"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "PUBMED:26108729"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_263"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_1_re152"
      uniprot "NA"
    ]
    graphics [
      x 1052.5
      y 2108.5794444430944
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_263"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:PACS2;urn:miriam:hgnc.symbol:PACS2;urn:miriam:ncbigene:23241;urn:miriam:uniprot:Q86VP3;urn:miriam:uniprot:Q86VP3;urn:miriam:ncbigene:23241;urn:miriam:pubmed:15692567;urn:miriam:hgnc:23794;urn:miriam:pubmed:26108729;urn:miriam:refseq:NM_001100913;urn:miriam:ensembl:ENSG00000179364"
      hgnc "HGNC_SYMBOL:PACS2"
      map_id "M118_308"
      name "PACS2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa175"
      uniprot "UNIPROT:Q86VP3"
    ]
    graphics [
      x 662.5
      y 1210.535953067229
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_308"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ensembl:ENSG00000151694;urn:miriam:ncbigene:6868;urn:miriam:ncbigene:6868;urn:miriam:refseq:NM_001382777;urn:miriam:ec-code:3.4.24.86;urn:miriam:hgnc:195;urn:miriam:uniprot:P78536;urn:miriam:uniprot:P78536;urn:miriam:pubmed:32264791;urn:miriam:hgnc.symbol:ADAM17;urn:miriam:hgnc.symbol:ADAM17;urn:miriam:pubmed:26108729"
      hgnc "HGNC_SYMBOL:ADAM17"
      map_id "M118_307"
      name "ADAM17"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa174"
      uniprot "UNIPROT:P78536"
    ]
    graphics [
      x 2042.5
      y 1828.5318911777056
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_307"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:pubmed:22511781;urn:miriam:taxonomy:10090;urn:miriam:obo.go:GO%3A0034976"
      hgnc "NA"
      map_id "M118_322"
      name "ER_space_stress"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa69"
      uniprot "NA"
    ]
    graphics [
      x 1952.5
      y 1742.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_322"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:HSPA5;urn:miriam:ensembl:ENSG00000044574;urn:miriam:hgnc.symbol:HSPA5;urn:miriam:refseq:NM_005347;urn:miriam:hgnc:5238;urn:miriam:ncbigene:3309;urn:miriam:ncbigene:3309;urn:miriam:uniprot:P11021;urn:miriam:uniprot:P11021;urn:miriam:ec-code:3.6.4.10"
      hgnc "HGNC_SYMBOL:HSPA5"
      map_id "M118_325"
      name "HSPA5"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa87"
      uniprot "UNIPROT:P11021"
    ]
    graphics [
      x 1682.5
      y 717.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_325"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      annotation "PUBMED:30590907;PUBMED:17981125;PUBMED:25704011"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_267"
      name "NA"
      node_subtype "DISSOCIATION"
      node_type "reaction"
      org_id "path_1_re40"
      uniprot "NA"
    ]
    graphics [
      x 2072.5
      y 782.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_267"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "PUBMED:9388233"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_261"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re146"
      uniprot "NA"
    ]
    graphics [
      x 2222.5
      y 1676.7158381002334
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_261"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      annotation "PUBMED:30590907;PUBMED:17981125;PUBMED:25704011"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_262"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_1_re147"
      uniprot "NA"
    ]
    graphics [
      x 2642.5
      y 617.7682304796128
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_262"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_005866;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ensembl:ENSG00000147955;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ncbigene:10280;urn:miriam:ncbigene:10280;urn:miriam:hgnc:8157;urn:miriam:uniprot:Q99720;urn:miriam:uniprot:Q99720"
      hgnc "HGNC_SYMBOL:SIGMAR1"
      map_id "M118_323"
      name "SIGMAR1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa82"
      uniprot "UNIPROT:Q99720"
    ]
    graphics [
      x 2822.5
      y 1348.5723545793105
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_323"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:pubmed:30590907;urn:miriam:pubmed:17981125;urn:miriam:taxonomy:10029;urn:miriam:pubmed:25704011;urn:miriam:hgnc.symbol:HSPA5;urn:miriam:ensembl:ENSG00000044574;urn:miriam:hgnc.symbol:HSPA5;urn:miriam:refseq:NM_005347;urn:miriam:hgnc:5238;urn:miriam:ncbigene:3309;urn:miriam:ncbigene:3309;urn:miriam:uniprot:P11021;urn:miriam:uniprot:P11021;urn:miriam:ec-code:3.6.4.10;urn:miriam:refseq:NM_005866;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ensembl:ENSG00000147955;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ncbigene:10280;urn:miriam:ncbigene:10280;urn:miriam:hgnc:8157;urn:miriam:uniprot:Q99720;urn:miriam:uniprot:Q99720"
      hgnc "HGNC_SYMBOL:HSPA5;HGNC_SYMBOL:SIGMAR1"
      map_id "M118_228"
      name "SIGMAR1:HSPA5"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_1_csa13"
      uniprot "UNIPROT:P11021;UNIPROT:Q99720"
    ]
    graphics [
      x 1352.5
      y 1119.311461366438
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_228"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:pubmed:22511781;urn:miriam:taxonomy:10090;urn:miriam:obo.go:GO%3A0032469"
      hgnc "NA"
      map_id "M118_290"
      name "endoplasmic_space_reticulum_space_calcium_space_ion_space_homeostasis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa143"
      uniprot "NA"
    ]
    graphics [
      x 1232.5
      y 1692.9973497539163
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_290"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "PUBMED:22511781"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_250"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re122"
      uniprot "NA"
    ]
    graphics [
      x 1652.5
      y 1587.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_250"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_014874;urn:miriam:pubmed:22511781;urn:miriam:hgnc:16877;urn:miriam:ec-code:3.6.5.-;urn:miriam:pubmed:29491369;urn:miriam:hgnc.symbol:MFN2;urn:miriam:ensembl:ENSG00000116688;urn:miriam:uniprot:O95140;urn:miriam:uniprot:O95140;urn:miriam:hgnc.symbol:MFN2;urn:miriam:ncbigene:9927;urn:miriam:ncbigene:9927;urn:miriam:pubmed:19052620"
      hgnc "HGNC_SYMBOL:MFN2"
      map_id "M118_328"
      name "MFN2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa98"
      uniprot "UNIPROT:O95140"
    ]
    graphics [
      x 708.7805189192002
      y 2273.428726907034
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_328"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      annotation "PUBMED:22511781"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_239"
      name "NA"
      node_subtype "TRANSLATION"
      node_type "reaction"
      org_id "path_1_re108"
      uniprot "NA"
    ]
    graphics [
      x 1391.043215136001
      y 2645.8116520583294
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_239"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      annotation "PUBMED:30590907;PUBMED:29491369;PUBMED:19052620"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_276"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_1_re53"
      uniprot "NA"
    ]
    graphics [
      x 902.5
      y 2190.766784787864
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_276"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      annotation "PUBMED:19052620"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_279"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re59"
      uniprot "NA"
    ]
    graphics [
      x 1428.7805189192002
      y 2212.5398405932747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_279"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      annotation "PUBMED:29491369;PUBMED:19052620"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_249"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_1_re120"
      uniprot "NA"
    ]
    graphics [
      x 1837.432708728739
      y 392.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_249"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      annotation "PUBMED:22511781"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_282"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re71"
      uniprot "NA"
    ]
    graphics [
      x 1481.043215136001
      y 2556.193905259498
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_282"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      annotation "PUBMED:19052620"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_277"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re57"
      uniprot "NA"
    ]
    graphics [
      x 992.5
      y 1195.5770925595111
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_277"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      annotation "PUBMED:22511781"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_281"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re70"
      uniprot "NA"
    ]
    graphics [
      x 2222.5
      y 1916.7158381002334
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_281"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:19052620;urn:miriam:obo.go:GO%3A0007029"
      hgnc "NA"
      map_id "M118_284"
      name "endoplasmic_space_reticulum_space_organization"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa109"
      uniprot "NA"
    ]
    graphics [
      x 1417.432708728739
      y 472.5398405932747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_284"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.go:GO%3A0030968;urn:miriam:pubmed:22511781;urn:miriam:taxonomy:10090"
      hgnc "NA"
      map_id "M118_287"
      name "unfolded_space_protein_space_response_space_(UPR)"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa120"
      uniprot "NA"
    ]
    graphics [
      x 1442.5
      y 1355.1961954122032
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_287"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      annotation "PUBMED:15692567"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_260"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re144"
      uniprot "NA"
    ]
    graphics [
      x 1232.5
      y 1032.9973497539163
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_260"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      annotation "PUBMED:22511781"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_283"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re73"
      uniprot "NA"
    ]
    graphics [
      x 2432.5
      y 1376.0089009888093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_283"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:16695;urn:miriam:refseq:NM_005745;urn:miriam:ensembl:ENSG00000185825;urn:miriam:ncbigene:10134;urn:miriam:ncbigene:10134;urn:miriam:hgnc.symbol:BCAP31;urn:miriam:uniprot:P51572;urn:miriam:uniprot:P51572"
      hgnc "HGNC_SYMBOL:BCAP31"
      map_id "M118_304"
      name "p20"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa166"
      uniprot "UNIPROT:P51572"
    ]
    graphics [
      x 2087.953740494815
      y 332.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_304"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      annotation "PUBMED:15692567"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_256"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "path_1_re133"
      uniprot "NA"
    ]
    graphics [
      x 1442.5
      y 1175.1961954122032
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_256"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:16695;urn:miriam:refseq:NM_005745;urn:miriam:ensembl:ENSG00000185825;urn:miriam:ncbigene:10134;urn:miriam:ncbigene:10134;urn:miriam:hgnc.symbol:BCAP31;urn:miriam:hgnc.symbol:BCAP31;urn:miriam:uniprot:P51572;urn:miriam:uniprot:P51572"
      hgnc "HGNC_SYMBOL:BCAP31"
      map_id "M118_301"
      name "BCAP31"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa156"
      uniprot "UNIPROT:P51572"
    ]
    graphics [
      x 2972.5
      y 1964.936290989684
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_301"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc.symbol:PACS2;urn:miriam:hgnc.symbol:PACS2;urn:miriam:ncbigene:23241;urn:miriam:uniprot:Q86VP3;urn:miriam:uniprot:Q86VP3;urn:miriam:ncbigene:23241;urn:miriam:pubmed:15692567;urn:miriam:hgnc:23794;urn:miriam:refseq:NM_001100913;urn:miriam:ensembl:ENSG00000179364"
      hgnc "HGNC_SYMBOL:PACS2"
      map_id "M118_311"
      name "PACS2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa3"
      uniprot "UNIPROT:Q86VP3"
    ]
    graphics [
      x 1788.7805189192002
      y 2162.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_311"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:22511781;urn:miriam:obo.go:GO%3A0006915;urn:miriam:pubmed:19052620;urn:miriam:pubmed:15692567"
      hgnc "NA"
      map_id "M118_286"
      name "Apoptosis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa110"
      uniprot "NA"
    ]
    graphics [
      x 1712.5
      y 1797.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_286"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      annotation "PUBMED:17981125"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_333"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re137"
      uniprot "NA"
    ]
    graphics [
      x 2328.7805189192004
      y 2229.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_333"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      annotation "PUBMED:22511781"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_280"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re67"
      uniprot "NA"
    ]
    graphics [
      x 2192.5
      y 1463.7133098641311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_280"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_005866;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ensembl:ENSG00000147955;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:pubmed:17981125;urn:miriam:taxonomy:10029;urn:miriam:ncbigene:10280;urn:miriam:ncbigene:10280;urn:miriam:hgnc:8157;urn:miriam:pubmed:25704011;urn:miriam:uniprot:Q99720;urn:miriam:uniprot:Q99720"
      hgnc "HGNC_SYMBOL:SIGMAR1"
      map_id "M118_324"
      name "SIGMAR1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa86"
      uniprot "UNIPROT:Q99720"
    ]
    graphics [
      x 3212.5
      y 984.6072728798446
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_324"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      annotation "PUBMED:30590907;PUBMED:17981125"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_268"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_1_re42"
      uniprot "NA"
    ]
    graphics [
      x 2762.5
      y 968.1323001980676
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_268"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      annotation "PUBMED:17981125"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_332"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "re136"
      uniprot "NA"
    ]
    graphics [
      x 2142.223682853007
      y 323.71330986413113
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_332"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_002224;urn:miriam:hgnc.symbol:ITPR3;urn:miriam:hgnc.symbol:ITPR3;urn:miriam:pubmed:17981125;urn:miriam:uniprot:Q14573;urn:miriam:uniprot:Q14573;urn:miriam:hgnc:6182;urn:miriam:ncbigene:3710;urn:miriam:ensembl:ENSG00000096433;urn:miriam:ncbigene:3710"
      hgnc "HGNC_SYMBOL:ITPR3"
      map_id "M118_326"
      name "ITPR3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa88"
      uniprot "UNIPROT:Q14573"
    ]
    graphics [
      x 2112.223682853007
      y 302.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_326"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:pubmed:30590907;urn:miriam:obo.go:GO%3A0032471;urn:miriam:pubmed:17981125;urn:miriam:taxonomy:10029"
      hgnc "NA"
      map_id "M118_319"
      name "Ca2_plus__space_ER_space_depletion"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa60"
      uniprot "NA"
    ]
    graphics [
      x 1892.5
      y 1982.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_319"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:pubmed:30590907;urn:miriam:pubmed:17981125;urn:miriam:taxonomy:10029;urn:miriam:refseq:NM_002224;urn:miriam:hgnc.symbol:ITPR3;urn:miriam:hgnc.symbol:ITPR3;urn:miriam:pubmed:17981125;urn:miriam:uniprot:Q14573;urn:miriam:uniprot:Q14573;urn:miriam:hgnc:6182;urn:miriam:ncbigene:3710;urn:miriam:ensembl:ENSG00000096433;urn:miriam:ncbigene:3710;urn:miriam:refseq:NM_005866;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ensembl:ENSG00000147955;urn:miriam:hgnc.symbol:SIGMAR1;urn:miriam:ncbigene:10280;urn:miriam:ncbigene:10280;urn:miriam:hgnc:8157;urn:miriam:uniprot:Q99720;urn:miriam:uniprot:Q99720"
      hgnc "HGNC_SYMBOL:ITPR3;HGNC_SYMBOL:SIGMAR1"
      map_id "M118_229"
      name "SIGMAR1:ITPR3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_1_csa14"
      uniprot "UNIPROT:Q14573;UNIPROT:Q99720"
    ]
    graphics [
      x 2418.7805189192004
      y 2216.0089009888093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_229"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      annotation "PUBMED:30590907;PUBMED:30590033"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_265"
      name "NA"
      node_subtype "TRANSPORT"
      node_type "reaction"
      org_id "path_1_re19"
      uniprot "NA"
    ]
    graphics [
      x 3092.5
      y 1570.1860833815003
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_265"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29108"
      hgnc "NA"
      map_id "M118_317"
      name "Ca2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "path_1_sa54"
      uniprot "NA"
    ]
    graphics [
      x 2178.7805189192004
      y 2393.713309864131
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_317"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:pubmed:30590033;urn:miriam:taxonomy:9606;urn:miriam:pubmed:29491369;urn:miriam:refseq:NM_002224;urn:miriam:hgnc.symbol:ITPR3;urn:miriam:hgnc.symbol:ITPR3;urn:miriam:pubmed:17981125;urn:miriam:uniprot:Q14573;urn:miriam:uniprot:Q14573;urn:miriam:hgnc:6182;urn:miriam:ncbigene:3710;urn:miriam:ensembl:ENSG00000096433;urn:miriam:ncbigene:3710;urn:miriam:ensembl:ENSG00000213585;urn:miriam:hgnc:12669;urn:miriam:refseq:NM_003374;urn:miriam:uniprot:P21796;urn:miriam:uniprot:P21796;urn:miriam:hgnc.symbol:VDAC1;urn:miriam:hgnc.symbol:VDAC1;urn:miriam:ncbigene:7416;urn:miriam:ncbigene:7416;urn:miriam:hgnc:5244;urn:miriam:refseq:NM_004134;urn:miriam:ensembl:ENSG00000113013;urn:miriam:uniprot:P38646;urn:miriam:uniprot:P38646;urn:miriam:hgnc.symbol:HSPA9;urn:miriam:hgnc.symbol:HSPA9;urn:miriam:ncbigene:3313;urn:miriam:ncbigene:3313"
      hgnc "HGNC_SYMBOL:ITPR3;HGNC_SYMBOL:VDAC1;HGNC_SYMBOL:HSPA9"
      map_id "M118_233"
      name "ITPR3:HSPA9:VDAC1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_1_csa2"
      uniprot "UNIPROT:Q14573;UNIPROT:P21796;UNIPROT:P38646"
    ]
    graphics [
      x 2252.5
      y 812.4583143700918
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_233"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_002224;urn:miriam:hgnc.symbol:ITPR3;urn:miriam:hgnc.symbol:ITPR3;urn:miriam:pubmed:17981125;urn:miriam:uniprot:Q14573;urn:miriam:uniprot:Q14573;urn:miriam:hgnc:6182;urn:miriam:ncbigene:3710;urn:miriam:ensembl:ENSG00000096433;urn:miriam:ncbigene:3710"
      hgnc "HGNC_SYMBOL:ITPR3"
      map_id "M118_285"
      name "ITPR3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa11"
      uniprot "UNIPROT:Q14573"
    ]
    graphics [
      x 2492.5
      y 691.4350242348289
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_285"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29108"
      hgnc "NA"
      map_id "M118_318"
      name "Ca2_plus_"
      node_subtype "ION"
      node_type "species"
      org_id "path_1_sa55"
      uniprot "NA"
    ]
    graphics [
      x 1832.5
      y 782.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_318"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      annotation "PUBMED:28132811"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_248"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "path_1_re119"
      uniprot "NA"
    ]
    graphics [
      x 1892.5
      y 482.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_248"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:obo.go:GO%3A0006914;urn:miriam:pubmed:28132811"
      hgnc "NA"
      map_id "M118_298"
      name "autophagy"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa152"
      uniprot "NA"
    ]
    graphics [
      x 2102.5
      y 935.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_298"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      annotation "PUBMED:28132811"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_247"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "path_1_re118"
      uniprot "NA"
    ]
    graphics [
      x 2372.5
      y 1072.0608348716244
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_247"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      annotation "PUBMED:28132811"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_244"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "path_1_re114"
      uniprot "NA"
    ]
    graphics [
      x 1248.7805189192002
      y 2166.9780694277015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_244"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      annotation "PUBMED:28132811"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_243"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "path_1_re113"
      uniprot "NA"
    ]
    graphics [
      x 1622.5
      y 1162.5365071385852
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_243"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:Q96TC7;urn:miriam:uniprot:Q96TC7;urn:miriam:refseq:NM_018145;urn:miriam:ncbigene:55177;urn:miriam:ncbigene:55177;urn:miriam:hgnc.symbol:RMDN3;urn:miriam:hgnc.symbol:RMDN3;urn:miriam:ensembl:ENSG00000137824;urn:miriam:hgnc:25550"
      hgnc "HGNC_SYMBOL:RMDN3"
      map_id "M118_292"
      name "RMDN3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa146"
      uniprot "UNIPROT:Q96TC7"
    ]
    graphics [
      x 2312.5
      y 2019.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_292"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      annotation "PUBMED:28132811"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_242"
      name "NA"
      node_subtype "PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "path_1_re111"
      uniprot "NA"
    ]
    graphics [
      x 1952.5
      y 1502.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_242"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      annotation "PUBMED:28132811"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_246"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "path_1_re117"
      uniprot "NA"
    ]
    graphics [
      x 3032.5
      y 1830.7962381347772
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_246"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      annotation "PUBMED:28132811"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_238"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_1_re103"
      uniprot "NA"
    ]
    graphics [
      x 2531.043215136001
      y 2552.97150358576
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_238"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:12649;urn:miriam:ensembl:ENSG00000124164;urn:miriam:hgnc.symbol:VAPB;urn:miriam:hgnc.symbol:VAPB;urn:miriam:uniprot:O95292;urn:miriam:uniprot:O95292;urn:miriam:ncbigene:9217;urn:miriam:refseq:NM_001195677;urn:miriam:ncbigene:9217"
      hgnc "HGNC_SYMBOL:VAPB"
      map_id "M118_293"
      name "VAPB"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa147"
      uniprot "UNIPROT:O95292"
    ]
    graphics [
      x 2178.7805189192004
      y 2483.713309864131
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_293"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:29491369;urn:miriam:pubmed:28132811;urn:miriam:hgnc:12649;urn:miriam:ensembl:ENSG00000124164;urn:miriam:hgnc.symbol:VAPB;urn:miriam:hgnc.symbol:VAPB;urn:miriam:uniprot:O95292;urn:miriam:uniprot:O95292;urn:miriam:ncbigene:9217;urn:miriam:refseq:NM_001195677;urn:miriam:ncbigene:9217;urn:miriam:uniprot:Q96TC7;urn:miriam:uniprot:Q96TC7;urn:miriam:refseq:NM_018145;urn:miriam:ncbigene:55177;urn:miriam:ncbigene:55177;urn:miriam:hgnc.symbol:RMDN3;urn:miriam:hgnc.symbol:RMDN3;urn:miriam:ensembl:ENSG00000137824;urn:miriam:hgnc:25550"
      hgnc "HGNC_SYMBOL:VAPB;HGNC_SYMBOL:RMDN3"
      map_id "M118_235"
      name "VAPB:RMDN3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_1_csa5"
      uniprot "UNIPROT:O95292;UNIPROT:Q96TC7"
    ]
    graphics [
      x 2312.5
      y 1329.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_235"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      annotation "PUBMED:30590907;PUBMED:28132811"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_274"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re50"
      uniprot "NA"
    ]
    graphics [
      x 1862.5
      y 1922.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_274"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:pubmed:27457486;urn:miriam:pubmed:30590907;urn:miriam:pubmed:29491369;urn:miriam:obo.go:GO%3A1990456;urn:miriam:pubmed:28132811;urn:miriam:pubmed:19052620"
      hgnc "NA"
      map_id "M118_327"
      name "mitochondrion_minus_endoplasmic_space_reticulum_space_membrane_space_tethering_space_"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa97"
      uniprot "NA"
    ]
    graphics [
      x 1601.043215136001
      y 2633.119912305107
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_327"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      annotation "PUBMED:30590907;PUBMED:29491369;PUBMED:19052620"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_273"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re49"
      uniprot "NA"
    ]
    graphics [
      x 1052.5
      y 1838.5794444430944
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_273"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      annotation "PUBMED:30590907;PUBMED:29491369;PUBMED:21183955"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_272"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re48"
      uniprot "NA"
    ]
    graphics [
      x 2072.5
      y 1925.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_272"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      annotation "PUBMED:30590907;PUBMED:29491369;PUBMED:19052620"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_271"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re47"
      uniprot "NA"
    ]
    graphics [
      x 1469.0458120231497
      y 2752.5398405932747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_271"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      annotation "PUBMED:15692567"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_251"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re123"
      uniprot "NA"
    ]
    graphics [
      x 1031.043215136001
      y 2548.109585504439
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_251"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:29491369;urn:miriam:pubmed:19052620;urn:miriam:refseq:NM_014874;urn:miriam:hgnc.symbol:MFN2;urn:miriam:ensembl:ENSG00000116688;urn:miriam:uniprot:O95140;urn:miriam:uniprot:O95140;urn:miriam:hgnc.symbol:MFN2;urn:miriam:ncbigene:9927;urn:miriam:ncbigene:9927;urn:miriam:hgnc:16877;urn:miriam:ec-code:3.6.5.-;urn:miriam:ncbigene:1093;urn:miriam:ensembl:ENSG00000230681;urn:miriam:refseq:NG_001099;urn:miriam:hgnc:1826;urn:miriam:hgnc.symbol:CEACAMP4"
      hgnc "HGNC_SYMBOL:MFN2;HGNC_SYMBOL:CEACAMP4"
      map_id "M118_234"
      name "MFN1:MFN2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_1_csa4"
      uniprot "UNIPROT:O95140"
    ]
    graphics [
      x 1352.5
      y 1299.311461366438
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_234"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:15692567;urn:miriam:pubmed:21183955;urn:miriam:hgnc:16695;urn:miriam:refseq:NM_005745;urn:miriam:ensembl:ENSG00000185825;urn:miriam:ncbigene:10134;urn:miriam:ncbigene:10134;urn:miriam:hgnc.symbol:BCAP31;urn:miriam:hgnc.symbol:BCAP31;urn:miriam:uniprot:P51572;urn:miriam:uniprot:P51572;urn:miriam:hgnc:21689;urn:miriam:hgnc.symbol:FIS1;urn:miriam:refseq:NM_016068;urn:miriam:hgnc.symbol:FIS1;urn:miriam:ncbigene:51024;urn:miriam:ncbigene:51024;urn:miriam:ensembl:ENSG00000214253;urn:miriam:uniprot:Q9Y3D6;urn:miriam:uniprot:Q9Y3D6"
      hgnc "HGNC_SYMBOL:BCAP31;HGNC_SYMBOL:FIS1"
      map_id "M118_227"
      name "FIS1:BCAP31"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_1_csa1"
      uniprot "UNIPROT:P51572;UNIPROT:Q9Y3D6"
    ]
    graphics [
      x 2942.5
      y 2152.6505321512923
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_227"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      annotation "PUBMED:21183955"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_252"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "path_1_re126"
      uniprot "NA"
    ]
    graphics [
      x 2642.5
      y 1697.7682304796128
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_252"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:hgnc:21689;urn:miriam:hgnc.symbol:FIS1;urn:miriam:refseq:NM_016068;urn:miriam:hgnc.symbol:FIS1;urn:miriam:ncbigene:51024;urn:miriam:ncbigene:51024;urn:miriam:ensembl:ENSG00000214253;urn:miriam:uniprot:Q9Y3D6;urn:miriam:uniprot:Q9Y3D6"
      hgnc "HGNC_SYMBOL:FIS1"
      map_id "M118_302"
      name "FIS1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa158"
      uniprot "UNIPROT:Q9Y3D6"
    ]
    graphics [
      x 978.7805189192002
      y 2340.6216893968285
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_302"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      annotation "PUBMED:21183955"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_253"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re127"
      uniprot "NA"
    ]
    graphics [
      x 1934.292827536552
      y 2762.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_253"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      annotation "PUBMED:9606;PUBMED:21183955"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_254"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re128"
      uniprot "NA"
    ]
    graphics [
      x 302.5
      y 2037.3614847483402
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_254"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:obo.go:GO%3A0051561;urn:miriam:pubmed:28132811"
      hgnc "NA"
      map_id "M118_296"
      name "Ca2_plus__space_mitochondrial_space_concentration"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa150"
      uniprot "NA"
    ]
    graphics [
      x 842.5
      y 1373.1189418163667
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_296"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      annotation "PUBMED:28132811"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_255"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re129"
      uniprot "NA"
    ]
    graphics [
      x 1652.5
      y 1317.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_255"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:obo.go:GO%3A0051882;urn:miriam:pubmed:21183955"
      hgnc "NA"
      map_id "M118_303"
      name "mitochondrial_space_outer_space_membrane_space_depolarization"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa159"
      uniprot "NA"
    ]
    graphics [
      x 1728.7805189192002
      y 2337.1855687946227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_303"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:29491369;urn:miriam:pubmed:19052620;urn:miriam:refseq:NM_014874;urn:miriam:hgnc.symbol:MFN2;urn:miriam:ensembl:ENSG00000116688;urn:miriam:uniprot:O95140;urn:miriam:uniprot:O95140;urn:miriam:hgnc.symbol:MFN2;urn:miriam:ncbigene:9927;urn:miriam:ncbigene:9927;urn:miriam:hgnc:16877;urn:miriam:ec-code:3.6.5.-"
      hgnc "HGNC_SYMBOL:MFN2"
      map_id "M118_231"
      name "MFN2:MFN2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "path_1_csa17"
      uniprot "UNIPROT:O95140"
    ]
    graphics [
      x 1447.432708728739
      y 586.4685701968617
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_231"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      annotation "PUBMED:28132811"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_245"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "path_1_re116"
      uniprot "NA"
    ]
    graphics [
      x 2312.5
      y 1239.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_245"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:28132811;urn:miriam:obo.go:GO%3A0007204"
      hgnc "NA"
      map_id "M118_297"
      name "Ca2_plus__space_cytosolic_space_concentration"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa151"
      uniprot "NA"
    ]
    graphics [
      x 1982.5
      y 1952.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_297"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      annotation "PUBMED:15692567"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_258"
      name "NA"
      node_subtype "NEGATIVE_INFLUENCE"
      node_type "reaction"
      org_id "path_1_re140"
      uniprot "NA"
    ]
    graphics [
      x 1772.5
      y 632.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_258"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:15692567;urn:miriam:obo.go:GO%3A0000266"
      hgnc "NA"
      map_id "M118_305"
      name "mitochondrial_space_fission"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa169"
      uniprot "NA"
    ]
    graphics [
      x 1622.5
      y 892.5365071385852
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_305"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      annotation "PUBMED:15692567"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_259"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "path_1_re142"
      uniprot "NA"
    ]
    graphics [
      x 2377.4327087287393
      y 452.96350238195305
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_259"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:taxonomy:9606;urn:miriam:pubmed:15692567;urn:miriam:obo.go:GO%3A0043653"
      hgnc "NA"
      map_id "M118_300"
      name "mitochondria_space_fragmentation"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "path_1_sa154"
      uniprot "NA"
    ]
    graphics [
      x 2152.2022116121307
      y 203.71330986413113
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_300"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_014874;urn:miriam:hgnc.symbol:MFN2;urn:miriam:ensembl:ENSG00000116688;urn:miriam:uniprot:O95140;urn:miriam:uniprot:O95140;urn:miriam:hgnc.symbol:MFN2;urn:miriam:ncbigene:9927;urn:miriam:ncbigene:9927;urn:miriam:hgnc:16877;urn:miriam:ec-code:3.6.5.-"
      hgnc "HGNC_SYMBOL:MFN2"
      map_id "M118_299"
      name "MFN2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa153"
      uniprot "UNIPROT:O95140"
    ]
    graphics [
      x 1493.4001201247063
      y 509.738142841986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_299"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:ncbigene:1093;urn:miriam:ensembl:ENSG00000230681;urn:miriam:refseq:NG_001099;urn:miriam:hgnc:1826;urn:miriam:hgnc.symbol:CEACAMP4"
      hgnc "HGNC_SYMBOL:CEACAMP4"
      map_id "M118_329"
      name "MFN1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "path_1_sa99"
      uniprot "NA"
    ]
    graphics [
      x 2192.5
      y 1403.7133098641311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_329"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_014874;urn:miriam:hgnc.symbol:MFN2;urn:miriam:ensembl:ENSG00000116688;urn:miriam:uniprot:O95140;urn:miriam:ncbigene:9927;urn:miriam:hgnc:16877"
      hgnc "HGNC_SYMBOL:MFN2"
      map_id "M118_294"
      name "MFN2"
      node_subtype "RNA"
      node_type "species"
      org_id "path_1_sa148"
      uniprot "UNIPROT:O95140"
    ]
    graphics [
      x 2148.896465160893
      y 2813.713309864131
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_294"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      annotation "PUBMED:22511781"
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "NA"
      hgnc "NA"
      map_id "M118_240"
      name "NA"
      node_subtype "TRANSCRIPTION"
      node_type "reaction"
      org_id "path_1_re109"
      uniprot "NA"
    ]
    graphics [
      x 2231.043215136001
      y 2633.713309864131
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_240"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:refseq:NM_014874;urn:miriam:hgnc.symbol:MFN2;urn:miriam:ensembl:ENSG00000116688;urn:miriam:uniprot:O95140;urn:miriam:ncbigene:9927;urn:miriam:hgnc:16877"
      hgnc "HGNC_SYMBOL:MFN2"
      map_id "M118_288"
      name "MFN2"
      node_subtype "GENE"
      node_type "species"
      org_id "path_1_sa122"
      uniprot "UNIPROT:O95140"
    ]
    graphics [
      x 1758.7805189192002
      y 2342.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_288"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:pubmed:14647384;urn:miriam:obo.go:GO%3A0039694"
      hgnc "NA"
      map_id "M118_351"
      name "viral_space_RNA_space_genome_space_replication"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa280"
      uniprot "NA"
    ]
    graphics [
      x 692.5
      y 1517.3568694866976
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_351"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      full_annotation "urn:miriam:uniprot:P0DTC2;urn:miriam:ncbigene:1489668;urn:miriam:uniprot:W6A028;urn:miriam:uniprot:P59594;urn:miriam:ncbigene:43740568;urn:miriam:hgnc.symbol:S;urn:miriam:ensembl:ENSG00000130234;urn:miriam:ncbigene:59272;urn:miriam:ncbigene:59272;urn:miriam:ec-code:3.4.17.23;urn:miriam:ec-code:3.4.17.-;urn:miriam:hgnc:13557;urn:miriam:pubmed:19411314;urn:miriam:pubmed:15692567;urn:miriam:pubmed:32264791;urn:miriam:uniprot:Q9BYF1;urn:miriam:uniprot:Q9BYF1;urn:miriam:refseq:NM_001371415;urn:miriam:hgnc.symbol:ACE2;urn:miriam:hgnc.symbol:ACE2"
      hgnc "HGNC_SYMBOL:S;HGNC_SYMBOL:ACE2"
      map_id "M118_1"
      name "ACE2:Spike"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa29"
      uniprot "UNIPROT:P0DTC2;UNIPROT:W6A028;UNIPROT:P59594;UNIPROT:Q9BYF1"
    ]
    graphics [
      x 1833.739665380976
      y 1892.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M118_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 114
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_291"
      target_id "M118_330"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 115
    source 1
    target 3
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_291"
      target_id "M118_237"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 116
    source 1
    target 4
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_291"
      target_id "M118_341"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 117
    source 1
    target 5
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "M118_291"
      target_id "M118_257"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 118
    source 1
    target 6
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_291"
      target_id "M118_334"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 119
    source 1
    target 7
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_291"
      target_id "M118_335"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 120
    source 1
    target 8
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "M118_291"
      target_id "M118_241"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 121
    source 26
    target 2
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_343"
      target_id "M118_330"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 122
    source 2
    target 113
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_330"
      target_id "M118_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 123
    source 3
    target 35
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_237"
      target_id "M118_322"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 124
    source 4
    target 112
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_341"
      target_id "M118_351"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 125
    source 11
    target 5
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_295"
      target_id "M118_257"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 126
    source 35
    target 5
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "TRIGGER"
      source_id "M118_322"
      target_id "M118_257"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 127
    source 5
    target 36
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_257"
      target_id "M118_325"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 128
    source 29
    target 6
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CATALYSIS"
      source_id "M118_306"
      target_id "M118_334"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 129
    source 6
    target 14
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_334"
      target_id "M118_310"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 130
    source 6
    target 13
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_334"
      target_id "M118_309"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 131
    source 7
    target 28
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_335"
      target_id "M118_345"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 132
    source 9
    target 8
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_289"
      target_id "M118_241"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 133
    source 10
    target 8
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "TRIGGER"
      source_id "M118_347"
      target_id "M118_241"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 134
    source 8
    target 11
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_241"
      target_id "M118_295"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 135
    source 10
    target 12
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_347"
      target_id "M118_336"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 136
    source 13
    target 12
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_309"
      target_id "M118_336"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 137
    source 14
    target 12
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "M118_310"
      target_id "M118_336"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 138
    source 12
    target 15
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_336"
      target_id "M118_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 139
    source 14
    target 20
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_310"
      target_id "M118_338"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 140
    source 15
    target 16
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_3"
      target_id "M118_337"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 141
    source 17
    target 16
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_350"
      target_id "M118_337"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 142
    source 16
    target 18
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_337"
      target_id "M118_346"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 143
    source 19
    target 18
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_339"
      target_id "M118_346"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 144
    source 20
    target 18
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_338"
      target_id "M118_346"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 145
    source 18
    target 21
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "M118_346"
      target_id "M118_340"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 146
    source 24
    target 19
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_2"
      target_id "M118_339"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 147
    source 22
    target 21
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_348"
      target_id "M118_340"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 148
    source 21
    target 23
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_340"
      target_id "M118_349"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 149
    source 25
    target 24
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_331"
      target_id "M118_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 150
    source 26
    target 25
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_343"
      target_id "M118_331"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 151
    source 27
    target 25
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_344"
      target_id "M118_331"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 152
    source 30
    target 29
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_264"
      target_id "M118_306"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 153
    source 31
    target 30
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_232"
      target_id "M118_264"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 154
    source 32
    target 31
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_263"
      target_id "M118_232"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 155
    source 33
    target 32
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_308"
      target_id "M118_263"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 156
    source 34
    target 32
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_307"
      target_id "M118_263"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 157
    source 65
    target 35
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_332"
      target_id "M118_322"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 158
    source 51
    target 35
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_281"
      target_id "M118_322"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 159
    source 35
    target 64
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "TRIGGER"
      source_id "M118_322"
      target_id "M118_268"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 160
    source 35
    target 62
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_322"
      target_id "M118_280"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 161
    source 35
    target 45
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "TRIGGER"
      source_id "M118_322"
      target_id "M118_239"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 162
    source 35
    target 55
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_322"
      target_id "M118_283"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 163
    source 35
    target 37
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "TRIGGER"
      source_id "M118_322"
      target_id "M118_267"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 164
    source 37
    target 36
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_267"
      target_id "M118_325"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 165
    source 36
    target 38
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_325"
      target_id "M118_261"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 166
    source 36
    target 39
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_325"
      target_id "M118_262"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 167
    source 41
    target 37
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_228"
      target_id "M118_267"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 168
    source 67
    target 37
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "TRIGGER"
      source_id "M118_319"
      target_id "M118_267"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 169
    source 37
    target 63
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_267"
      target_id "M118_324"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 170
    source 38
    target 42
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_261"
      target_id "M118_290"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 171
    source 40
    target 39
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_323"
      target_id "M118_262"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 172
    source 39
    target 41
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_262"
      target_id "M118_228"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 173
    source 43
    target 42
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_250"
      target_id "M118_290"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 174
    source 44
    target 43
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_328"
      target_id "M118_250"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 175
    source 45
    target 44
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_239"
      target_id "M118_328"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 176
    source 44
    target 46
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_328"
      target_id "M118_276"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 177
    source 44
    target 47
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_328"
      target_id "M118_279"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 178
    source 44
    target 48
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_328"
      target_id "M118_249"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 179
    source 44
    target 49
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_328"
      target_id "M118_282"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 180
    source 44
    target 50
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_328"
      target_id "M118_277"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 181
    source 44
    target 51
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_328"
      target_id "M118_281"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 182
    source 109
    target 45
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_294"
      target_id "M118_239"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 183
    source 108
    target 46
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_329"
      target_id "M118_276"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 184
    source 46
    target 91
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_276"
      target_id "M118_234"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 185
    source 47
    target 60
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_279"
      target_id "M118_286"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 186
    source 107
    target 48
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_299"
      target_id "M118_249"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 187
    source 48
    target 100
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_249"
      target_id "M118_231"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 188
    source 49
    target 53
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_282"
      target_id "M118_287"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 189
    source 50
    target 52
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_277"
      target_id "M118_284"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 190
    source 54
    target 53
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_260"
      target_id "M118_287"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 191
    source 55
    target 53
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_283"
      target_id "M118_287"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 192
    source 56
    target 54
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_304"
      target_id "M118_260"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 193
    source 57
    target 56
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_256"
      target_id "M118_304"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 194
    source 58
    target 57
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_301"
      target_id "M118_256"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 195
    source 59
    target 57
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "INHIBITION"
      source_id "M118_311"
      target_id "M118_256"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 196
    source 60
    target 57
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M118_286"
      target_id "M118_256"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 197
    source 58
    target 93
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_301"
      target_id "M118_252"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 198
    source 59
    target 103
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_311"
      target_id "M118_258"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 199
    source 59
    target 90
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_311"
      target_id "M118_251"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 200
    source 61
    target 60
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_333"
      target_id "M118_286"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 201
    source 62
    target 60
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_280"
      target_id "M118_286"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 202
    source 63
    target 61
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_324"
      target_id "M118_333"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 203
    source 63
    target 64
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_324"
      target_id "M118_268"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 204
    source 63
    target 65
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_324"
      target_id "M118_332"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 205
    source 66
    target 64
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_326"
      target_id "M118_268"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 206
    source 67
    target 64
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "TRIGGER"
      source_id "M118_319"
      target_id "M118_268"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 207
    source 64
    target 68
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_268"
      target_id "M118_229"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 208
    source 66
    target 74
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_326"
      target_id "M118_248"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 209
    source 68
    target 69
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "TRIGGER"
      source_id "M118_229"
      target_id "M118_265"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 210
    source 70
    target 69
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_317"
      target_id "M118_265"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 211
    source 71
    target 69
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "TRIGGER"
      source_id "M118_233"
      target_id "M118_265"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 212
    source 72
    target 69
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "TRIGGER"
      source_id "M118_285"
      target_id "M118_265"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 213
    source 69
    target 73
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_265"
      target_id "M118_318"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 214
    source 74
    target 75
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_248"
      target_id "M118_298"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 215
    source 76
    target 75
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_247"
      target_id "M118_298"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 216
    source 77
    target 75
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_244"
      target_id "M118_298"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 217
    source 78
    target 75
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_243"
      target_id "M118_298"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 218
    source 86
    target 76
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_327"
      target_id "M118_247"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 219
    source 83
    target 77
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_293"
      target_id "M118_244"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 220
    source 79
    target 78
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_292"
      target_id "M118_243"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 221
    source 79
    target 80
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_292"
      target_id "M118_242"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 222
    source 79
    target 81
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_292"
      target_id "M118_246"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 223
    source 79
    target 82
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_292"
      target_id "M118_238"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 224
    source 80
    target 97
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_242"
      target_id "M118_296"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 225
    source 81
    target 102
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_246"
      target_id "M118_297"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 226
    source 83
    target 82
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_293"
      target_id "M118_238"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 227
    source 82
    target 84
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_238"
      target_id "M118_235"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 228
    source 83
    target 98
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_293"
      target_id "M118_255"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 229
    source 83
    target 101
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_293"
      target_id "M118_245"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 230
    source 84
    target 85
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_235"
      target_id "M118_274"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 85
    target 86
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_274"
      target_id "M118_327"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 232
    source 87
    target 86
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_273"
      target_id "M118_327"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 233
    source 88
    target 86
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_272"
      target_id "M118_327"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 234
    source 89
    target 86
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_271"
      target_id "M118_327"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 235
    source 90
    target 86
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_251"
      target_id "M118_327"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 236
    source 100
    target 87
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_231"
      target_id "M118_273"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 237
    source 92
    target 88
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_227"
      target_id "M118_272"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 238
    source 91
    target 89
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_234"
      target_id "M118_271"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 239
    source 93
    target 92
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_252"
      target_id "M118_227"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 240
    source 94
    target 93
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_302"
      target_id "M118_252"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 241
    source 94
    target 95
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_302"
      target_id "M118_253"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 242
    source 94
    target 96
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_302"
      target_id "M118_254"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 243
    source 95
    target 99
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_253"
      target_id "M118_303"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 244
    source 96
    target 97
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_254"
      target_id "M118_296"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 245
    source 98
    target 97
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_255"
      target_id "M118_296"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 246
    source 101
    target 102
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_245"
      target_id "M118_297"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 247
    source 103
    target 104
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_258"
      target_id "M118_305"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 248
    source 104
    target 105
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_305"
      target_id "M118_259"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 249
    source 105
    target 106
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_259"
      target_id "M118_300"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 250
    source 110
    target 109
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "PRODUCTION"
      source_id "M118_240"
      target_id "M118_294"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 251
    source 111
    target 110
    cd19dm [
      diagram "C19DMap:Endoplasmatic Reticulum stress"
      edge_type "CONSPUMPTION"
      source_id "M118_288"
      target_id "M118_240"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
