# generated with VANTED V2.8.2 at Fri Mar 04 10:04:33 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_85"
      name "NA"
      node_subtype "UNKNOWN"
      node_type "species"
      org_id "e45d6"
      uniprot "NA"
    ]
    graphics [
      x 1871.043215136001
      y 2912.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP5038"
      full_annotation "NA"
      hgnc "NA"
      map_id "W10_59"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "cc8d7"
      uniprot "NA"
    ]
    graphics [
      x 2462.5
      y 2071.435024234829
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP5038"
      full_annotation "urn:miriam:uniprot:P0DTC5"
      hgnc "NA"
      map_id "W10_65"
      name "Membrane_space_Glycoprotein_space_M"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d1b58"
      uniprot "UNIPROT:P0DTC5"
    ]
    graphics [
      x 1502.5
      y 1147.3397236403869
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W10_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 4
    source 1
    target 2
    cd19dm [
      diagram "WP5038"
      edge_type "CONSPUMPTION"
      source_id "W10_85"
      target_id "W10_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 5
    source 2
    target 3
    cd19dm [
      diagram "WP5038"
      edge_type "PRODUCTION"
      source_id "W10_59"
      target_id "W10_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
