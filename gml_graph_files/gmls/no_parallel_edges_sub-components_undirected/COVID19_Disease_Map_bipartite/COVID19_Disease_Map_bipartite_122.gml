# generated with VANTED V2.8.2 at Fri Mar 04 10:04:34 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.go:GO%3A1990231;urn:miriam:obo.chebi:CHEBI%3A75947;urn:miriam:hgnc.symbol:STING1;urn:miriam:uniprot:Q86WV6;urn:miriam:ncbigene:340061"
      hgnc "HGNC_SYMBOL:STING1"
      map_id "M114_11"
      name "cGAMP:STING"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa3"
      uniprot "UNIPROT:Q86WV6"
    ]
    graphics [
      x 1601.043215136001
      y 2543.119912305107
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_11"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_40"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re40"
      uniprot "NA"
    ]
    graphics [
      x 992.5
      y 1225.5770925595111
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      annotation "PUBMED:27324217"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_42"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re42"
      uniprot "NA"
    ]
    graphics [
      x 1278.7805189192002
      y 2316.9780694277015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_42"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_55"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re58"
      uniprot "NA"
    ]
    graphics [
      x 708.7805189192002
      y 2476.3859097686304
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_55"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:uniprot:Q9NR31;urn:miriam:ensembl:ENSG00000079332;urn:miriam:hgnc:10534;urn:miriam:hgnc.symbol:SAR1A;urn:miriam:refseq:NM_001142648;urn:miriam:ncbigene:56681"
      hgnc "HGNC_SYMBOL:SAR1A"
      map_id "M114_161"
      name "SAR1A"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa92"
      uniprot "UNIPROT:Q9NR31"
    ]
    graphics [
      x 2021.043215136001
      y 2792.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_161"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:hgnc.symbol:STING1;urn:miriam:uniprot:Q86WV6;urn:miriam:ncbigene:340061;urn:miriam:obo.chebi:CHEBI%3A75947"
      hgnc "HGNC_SYMBOL:STING1"
      map_id "M114_3"
      name "cGAMP_minus_STING"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa11"
      uniprot "UNIPROT:Q86WV6"
    ]
    graphics [
      x 662.5
      y 1454.2883837541774
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      annotation "PUBMED:29241549"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_51"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re53"
      uniprot "NA"
    ]
    graphics [
      x 302.5
      y 1882.330950829379
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_51"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A75947;urn:miriam:hgnc.symbol:STING1;urn:miriam:uniprot:Q86WV6;urn:miriam:ncbigene:340061"
      hgnc "HGNC_SYMBOL:STING1"
      map_id "M114_5"
      name "cGAMP_minus_STING"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa16"
      uniprot "UNIPROT:Q86WV6"
    ]
    graphics [
      x 962.5
      y 1287.6083120490775
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "PUBMED:30842662"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_56"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re59"
      uniprot "NA"
    ]
    graphics [
      x 2658.7805189192004
      y 2388.3611314016125
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_56"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_163"
      name "LC3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa94"
      uniprot "NA"
    ]
    graphics [
      x 1548.7805189192002
      y 2189.738142841986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_163"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:refseq:NM_015610;urn:miriam:hgnc:32225;urn:miriam:hgnc.symbol:WIPI2;urn:miriam:ensembl:ENSG00000157954;urn:miriam:ncbigene:26100;urn:miriam:uniprot:Q9Y4P8"
      hgnc "HGNC_SYMBOL:WIPI2"
      map_id "M114_162"
      name "WIPI2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa93"
      uniprot "UNIPROT:Q9Y4P8"
    ]
    graphics [
      x 2083.403181474062
      y 2852.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_162"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A75947;urn:miriam:hgnc.symbol:STING1;urn:miriam:uniprot:Q86WV6;urn:miriam:ncbigene:340061"
      hgnc "HGNC_SYMBOL:STING1"
      map_id "M114_4"
      name "cGAMP:STING:LC3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa13"
      uniprot "UNIPROT:Q86WV6"
    ]
    graphics [
      x 3078.7805189192004
      y 2350.1860833815003
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_69"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re88"
      uniprot "NA"
    ]
    graphics [
      x 2504.67282953685
      y 2877.636979529104
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_69"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_95"
      name "HCoVs_space_autophagy_space_(WP4863)"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa177"
      uniprot "NA"
    ]
    graphics [
      x 1751.043215136001
      y 2607.1855687946227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_95"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.go:GO%3A1990231;urn:miriam:obo.chebi:CHEBI%3A75947;urn:miriam:hgnc.symbol:STING1;urn:miriam:uniprot:Q86WV6;urn:miriam:ncbigene:340061"
      hgnc "HGNC_SYMBOL:STING1"
      map_id "M114_12"
      name "cGAMP:STING"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa4"
      uniprot "UNIPROT:Q86WV6"
    ]
    graphics [
      x 2381.043215136001
      y 2692.060834871624
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_41"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re41"
      uniprot "NA"
    ]
    graphics [
      x 1874.292827536552
      y 2762.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:uniprot:Q9UHD2;urn:miriam:ec-code:2.7.11.1;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:ncbigene:29110"
      hgnc "HGNC_SYMBOL:TBK1"
      map_id "M114_148"
      name "TBK1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa80"
      uniprot "UNIPROT:Q9UHD2"
    ]
    graphics [
      x 2111.043215136001
      y 2615.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_148"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:pubmed:30842653;urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:uniprot:Q9UHD2;urn:miriam:ec-code:2.7.11.1;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:ncbigene:29110"
      hgnc "HGNC_SYMBOL:TBK1"
      map_id "M114_7"
      name "STING:TBK1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa20"
      uniprot "UNIPROT:Q9UHD2"
    ]
    graphics [
      x 3032.5
      y 1791.1000550923975
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      annotation "PUBMED:25636800"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_58"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re63"
      uniprot "NA"
    ]
    graphics [
      x 1338.7805189192002
      y 2087.7634103488813
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_58"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      annotation "PUBMED:30842662;PUBMED:29241549"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_43"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re43"
      uniprot "NA"
    ]
    graphics [
      x 2718.7805189192004
      y 2313.1316429369444
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_43"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:pubmed:30842653;urn:miriam:hgnc.symbol:TBK1;urn:miriam:ensembl:ENSG00000183735;urn:miriam:uniprot:Q9UHD2;urn:miriam:ec-code:2.7.11.1;urn:miriam:refseq:NM_013254;urn:miriam:hgnc:11584;urn:miriam:ncbigene:29110"
      hgnc "HGNC_SYMBOL:TBK1"
      map_id "M114_13"
      name "STING:TBK1"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa5"
      uniprot "UNIPROT:Q9UHD2"
    ]
    graphics [
      x 2702.5
      y 1154.6209790551018
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      annotation "PUBMED:25636800;PUBMED:22394562"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_54"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re57"
      uniprot "NA"
    ]
    graphics [
      x 2449.8951650312483
      y 481.4350242348289
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_54"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:hgnc:6118;urn:miriam:uniprot:Q14653;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661"
      hgnc "HGNC_SYMBOL:IRF3"
      map_id "M114_152"
      name "IRF3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa84"
      uniprot "UNIPROT:Q14653"
    ]
    graphics [
      x 2142.223682853007
      y 353.71330986413113
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_152"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:hgnc:6118;urn:miriam:uniprot:Q14653;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661"
      hgnc "HGNC_SYMBOL:IRF3"
      map_id "M114_153"
      name "IRF3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa85"
      uniprot "UNIPROT:Q14653"
    ]
    graphics [
      x 2021.0708947697178
      y 152.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_153"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_63"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re71"
      uniprot "NA"
    ]
    graphics [
      x 2702.5
      y 1034.6209790551018
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_63"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.go:GO%3A0071159;urn:miriam:hgnc:9955;urn:miriam:ensembl:ENSG00000173039;urn:miriam:ncbigene:5970;urn:miriam:ncbigene:5970;urn:miriam:refseq:NM_021975;urn:miriam:hgnc.symbol:RELA;urn:miriam:hgnc.symbol:RELA;urn:miriam:uniprot:Q04206;urn:miriam:uniprot:Q04206;urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998"
      hgnc "HGNC_SYMBOL:RELA;HGNC_SYMBOL:NFKB1"
      map_id "M114_6"
      name "NF_minus_kB"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa19"
      uniprot "UNIPROT:Q04206;UNIPROT:P19838"
    ]
    graphics [
      x 1742.5
      y 1077.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.go:GO%3A0071159;urn:miriam:hgnc:6118;urn:miriam:uniprot:Q14653;urn:miriam:ensembl:ENSG00000126456;urn:miriam:refseq:NM_001571;urn:miriam:hgnc.symbol:IRF3;urn:miriam:ncbigene:3661"
      hgnc "HGNC_SYMBOL:IRF3"
      map_id "M114_15"
      name "IFNB1_space_expression_space_complex"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa8"
      uniprot "UNIPROT:Q14653"
    ]
    graphics [
      x 2748.7805189192004
      y 2225.9233578751837
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      annotation "PUBMED:16979567"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_62"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re70"
      uniprot "NA"
    ]
    graphics [
      x 3122.5
      y 1312.8836900580434
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_62"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      annotation "PUBMED:20610653"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_50"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re51"
      uniprot "NA"
    ]
    graphics [
      x 2162.5
      y 2063.713309864131
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_50"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:ensembl:ENSG00000171855;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc:5434;urn:miriam:uniprot:P01574;urn:miriam:refseq:NM_002176;urn:miriam:ncbigene:3456"
      hgnc "HGNC_SYMBOL:IFNB1"
      map_id "M114_87"
      name "IFNB1"
      node_subtype "GENE"
      node_type "species"
      org_id "sa147"
      uniprot "UNIPROT:P01574"
    ]
    graphics [
      x 992.5
      y 1546.326837145245
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_87"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:ensembl:ENSG00000171855;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc:5434;urn:miriam:uniprot:P01574;urn:miriam:refseq:NM_002176;urn:miriam:ncbigene:3456"
      hgnc "HGNC_SYMBOL:IFNB1"
      map_id "M114_157"
      name "IFNB1"
      node_subtype "RNA"
      node_type "species"
      org_id "sa89"
      uniprot "UNIPROT:P01574"
    ]
    graphics [
      x 2808.7805189192004
      y 2392.3300977750346
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_157"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_48"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re49"
      uniprot "NA"
    ]
    graphics [
      x 3092.5
      y 1180.1860833815003
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_48"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:ensembl:ENSG00000171855;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc:5434;urn:miriam:uniprot:P01574;urn:miriam:refseq:NM_002176;urn:miriam:ncbigene:3456"
      hgnc "HGNC_SYMBOL:IFNB1"
      map_id "M114_160"
      name "IFNB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa91"
      uniprot "UNIPROT:P01574"
    ]
    graphics [
      x 1802.5
      y 782.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_160"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_46"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re47"
      uniprot "NA"
    ]
    graphics [
      x 1562.5
      y 1815.4971941397312
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_46"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:ensembl:ENSG00000171855;urn:miriam:hgnc.symbol:IFNB1;urn:miriam:hgnc:5434;urn:miriam:uniprot:P01574;urn:miriam:refseq:NM_002176;urn:miriam:ncbigene:3456"
      hgnc "HGNC_SYMBOL:IFNB1"
      map_id "M114_155"
      name "IFNB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa87"
      uniprot "UNIPROT:P01574"
    ]
    graphics [
      x 1267.432708728739
      y 470.332422796959
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_155"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_72"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re90"
      uniprot "NA"
    ]
    graphics [
      x 812.5
      y 1090.6005374060633
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_72"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_75"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re94"
      uniprot "NA"
    ]
    graphics [
      x 302.5
      y 1703.5140714581185
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_75"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_73"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re91"
      uniprot "NA"
    ]
    graphics [
      x 1495.9532156587113
      y 359.59896275305005
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_73"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 39
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_96"
      name "HCoVs_space_IFN_space_induction_space_WP4880"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa178"
      uniprot "NA"
    ]
    graphics [
      x 2762.5
      y 998.1323001980676
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_96"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 40
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_70"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re89"
      uniprot "NA"
    ]
    graphics [
      x 902.5
      y 1751.7973208312726
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_70"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 41
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:hgnc.symbol:IFNA1;urn:miriam:ncbigene:3439;urn:miriam:refseq:NM_024013;urn:miriam:uniprot:P01562;urn:miriam:hgnc:5417;urn:miriam:ensembl:ENSG00000197919"
      hgnc "HGNC_SYMBOL:IFNA1"
      map_id "M114_154"
      name "IFNA1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa86"
      uniprot "UNIPROT:P01562"
    ]
    graphics [
      x 828.7805189192002
      y 2275.4401700720628
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_154"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 42
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_45"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re46"
      uniprot "NA"
    ]
    graphics [
      x 1301.043215136001
      y 2573.3146446645715
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_45"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 43
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_76"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re95"
      uniprot "NA"
    ]
    graphics [
      x 1112.5
      y 1587.0116707084728
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_76"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 44
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_74"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re93"
      uniprot "NA"
    ]
    graphics [
      x 182.5
      y 1637.2064689128206
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_74"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 45
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_93"
      name "HCoVs_space_Type_space_I_space_Ifn_space_signalling(WP4868)"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa175"
      uniprot "NA"
    ]
    graphics [
      x 302.5
      y 1779.6922935567054
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_93"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 46
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_94"
      name "Interferon_space_1_space_pathway"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa176"
      uniprot "NA"
    ]
    graphics [
      x 482.5
      y 975.2044344778127
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_94"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 47
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:hgnc.symbol:IFNA1;urn:miriam:ncbigene:3439;urn:miriam:refseq:NM_024013;urn:miriam:uniprot:P01562;urn:miriam:hgnc:5417;urn:miriam:ensembl:ENSG00000197919"
      hgnc "HGNC_SYMBOL:IFNA1"
      map_id "M114_159"
      name "IFNA1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa90"
      uniprot "UNIPROT:P01562"
    ]
    graphics [
      x 452.5
      y 1387.404927148254
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_159"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 48
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_47"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re48"
      uniprot "NA"
    ]
    graphics [
      x 1112.5
      y 1617.0116707084728
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_47"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 49
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:hgnc.symbol:IFNA1;urn:miriam:ncbigene:3439;urn:miriam:refseq:NM_024013;urn:miriam:uniprot:P01562;urn:miriam:hgnc:5417;urn:miriam:ensembl:ENSG00000197919"
      hgnc "HGNC_SYMBOL:IFNA1"
      map_id "M114_156"
      name "IFNA1"
      node_subtype "RNA"
      node_type "species"
      org_id "sa88"
      uniprot "UNIPROT:P01562"
    ]
    graphics [
      x 2522.5
      y 1322.9715035857598
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_156"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 50
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:hgnc.symbol:IFNA1;urn:miriam:ncbigene:3439;urn:miriam:refseq:NM_024013;urn:miriam:uniprot:P01562;urn:miriam:hgnc:5417;urn:miriam:ensembl:ENSG00000197919"
      hgnc "HGNC_SYMBOL:IFNA1"
      map_id "M114_86"
      name "IFNA1"
      node_subtype "GENE"
      node_type "species"
      org_id "sa146"
      uniprot "UNIPROT:P01562"
    ]
    graphics [
      x 2293.1635872027346
      y 429.06618499081924
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_86"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 51
    zlevel -1

    cd19dm [
      annotation "PUBMED:28579255"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_44"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re44"
      uniprot "NA"
    ]
    graphics [
      x 2132.5
      y 1205.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_44"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 52
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.go:GO%3A0071159;urn:miriam:hgnc:9955;urn:miriam:ensembl:ENSG00000173039;urn:miriam:ncbigene:5970;urn:miriam:ncbigene:5970;urn:miriam:refseq:NM_021975;urn:miriam:hgnc.symbol:RELA;urn:miriam:hgnc.symbol:RELA;urn:miriam:uniprot:Q04206;urn:miriam:uniprot:Q04206;urn:miriam:ncbigene:4790;urn:miriam:ncbigene:4790;urn:miriam:ensembl:ENSG00000109320;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc.symbol:NFKB1;urn:miriam:hgnc:7794;urn:miriam:uniprot:P19838;urn:miriam:uniprot:P19838;urn:miriam:refseq:NM_003998"
      hgnc "HGNC_SYMBOL:RELA;HGNC_SYMBOL:NFKB1"
      map_id "M114_14"
      name "NF_minus_kB"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa6"
      uniprot "UNIPROT:Q04206;UNIPROT:P19838"
    ]
    graphics [
      x 932.5
      y 1056.3349225565285
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 53
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:hgnc.symbol:STING1;urn:miriam:uniprot:Q86WV6;urn:miriam:ncbigene:340061"
      hgnc "HGNC_SYMBOL:STING1"
      map_id "M114_79"
      name "STING"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa100"
      uniprot "UNIPROT:Q86WV6"
    ]
    graphics [
      x 1802.5
      y 1472.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_79"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 54
    zlevel -1

    cd19dm [
      annotation "PUBMED:23388631"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_57"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re62"
      uniprot "NA"
    ]
    graphics [
      x 1262.5
      y 2106.9780694277015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_57"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 55
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:hgnc.symbol:STING1;urn:miriam:hgnc.symbol:STING1;urn:miriam:uniprot:Q86WV6;urn:miriam:ncbigene:340061;urn:miriam:ncbigene:340061;urn:miriam:hgnc:27962;urn:miriam:refseq:NM_198282;urn:miriam:ensembl:ENSG00000184584"
      hgnc "HGNC_SYMBOL:STING1"
      map_id "M114_166"
      name "STING"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa98"
      uniprot "UNIPROT:Q86WV6"
    ]
    graphics [
      x 1742.5
      y 1137.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_166"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 56
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:hgnc.symbol:MRE11;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:4361;urn:miriam:uniprot:P49959;urn:miriam:ensembl:ENSG00000020922;urn:miriam:refseq:NM_005591;urn:miriam:hgnc:7230;urn:miriam:ec-code:3.6.-.-;urn:miriam:ncbigene:10111;urn:miriam:refseq:NM_005732;urn:miriam:uniprot:Q92878;urn:miriam:hgnc.symbol:RAD50;urn:miriam:ensembl:ENSG00000113522;urn:miriam:hgnc:9816;urn:miriam:obo.chebi:CHEBI%3A75909"
      hgnc "HGNC_SYMBOL:MRE11;HGNC_SYMBOL:RAD50"
      map_id "M114_9"
      name "MRE11:RAD50:DNA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa24"
      uniprot "UNIPROT:P49959;UNIPROT:Q92878"
    ]
    graphics [
      x 1061.043215136001
      y 2682.906650739764
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 57
    zlevel -1

    cd19dm [
      annotation "PUBMED:23388631"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_59"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re64"
      uniprot "NA"
    ]
    graphics [
      x 1630.9708073167296
      y 2877.1855687946227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_59"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 58
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:hgnc.symbol:MRE11;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:4361;urn:miriam:uniprot:P49959;urn:miriam:ensembl:ENSG00000020922;urn:miriam:refseq:NM_005591;urn:miriam:hgnc:7230;urn:miriam:obo.chebi:CHEBI%3A75909;urn:miriam:ec-code:3.6.-.-;urn:miriam:ncbigene:10111;urn:miriam:refseq:NM_005732;urn:miriam:uniprot:Q92878;urn:miriam:hgnc.symbol:RAD50;urn:miriam:ensembl:ENSG00000113522;urn:miriam:hgnc:9816"
      hgnc "HGNC_SYMBOL:MRE11;HGNC_SYMBOL:RAD50"
      map_id "M114_2"
      name "MRE11:RAD50:DNA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa10"
      uniprot "UNIPROT:P49959;UNIPROT:Q92878"
    ]
    graphics [
      x 3062.5
      y 1954.764634507153
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_2"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 59
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_53"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re55"
      uniprot "NA"
    ]
    graphics [
      x 1428.7805189192002
      y 2332.5398405932747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_53"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 60
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:ec-code:3.6.-.-;urn:miriam:ncbigene:10111;urn:miriam:refseq:NM_005732;urn:miriam:uniprot:Q92878;urn:miriam:hgnc.symbol:RAD50;urn:miriam:ensembl:ENSG00000113522;urn:miriam:hgnc:9816"
      hgnc "HGNC_SYMBOL:RAD50"
      map_id "M114_165"
      name "RAD50"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa97"
      uniprot "UNIPROT:Q92878"
    ]
    graphics [
      x 1211.043215136001
      y 2726.9796758294588
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_165"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 61
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.go:GO%3A0003690;urn:miriam:hgnc.symbol:MRE11;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:4361;urn:miriam:uniprot:P49959;urn:miriam:ensembl:ENSG00000020922;urn:miriam:refseq:NM_005591;urn:miriam:hgnc:7230;urn:miriam:obo.chebi:CHEBI%3A75909"
      hgnc "HGNC_SYMBOL:MRE11"
      map_id "M114_8"
      name "MRE11:dsDNA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa23"
      uniprot "UNIPROT:P49959"
    ]
    graphics [
      x 1982.5
      y 1922.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 62
    zlevel -1

    cd19dm [
      annotation "PUBMED:23388631"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_52"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re54"
      uniprot "NA"
    ]
    graphics [
      x 2942.5
      y 1732.6505321512923
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_52"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 63
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A75909"
      hgnc "NA"
      map_id "M114_145"
      name "dsDNA"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa77"
      uniprot "NA"
    ]
    graphics [
      x 1172.5
      y 2037.3156502059937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_145"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 64
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:hgnc.symbol:MRE11;urn:miriam:ec-code:3.1.-.-;urn:miriam:ncbigene:4361;urn:miriam:uniprot:P49959;urn:miriam:ensembl:ENSG00000020922;urn:miriam:refseq:NM_005591;urn:miriam:hgnc:7230"
      hgnc "HGNC_SYMBOL:MRE11"
      map_id "M114_164"
      name "MRE11"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa96"
      uniprot "UNIPROT:P49959"
    ]
    graphics [
      x 3242.5
      y 1553.410114080192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_164"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 65
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_64"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re76"
      uniprot "NA"
    ]
    graphics [
      x 2222.5
      y 872.4583143700918
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_64"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 66
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_37"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re38"
      uniprot "NA"
    ]
    graphics [
      x 1481.4427074764133
      y 2831.826764661486
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 67
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_60"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re69"
      uniprot "NA"
    ]
    graphics [
      x 1712.5
      y 2067.1855687946227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_60"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 68
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18077"
      hgnc "NA"
      map_id "M114_141"
      name "dTTP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa62"
      uniprot "NA"
    ]
    graphics [
      x 2118.7805189192004
      y 2405.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_141"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 69
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.go:GO%3A0071897"
      hgnc "NA"
      map_id "M114_83"
      name "DNA_space_biosynthesis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa141"
      uniprot "NA"
    ]
    graphics [
      x 2132.5
      y 1925.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_83"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 70
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.go:GO%3A0019079"
      hgnc "NA"
      map_id "M114_85"
      name "virus_space_replication"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa143"
      uniprot "NA"
    ]
    graphics [
      x 1172.5
      y 1947.3156502059937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_85"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 71
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_84"
      name "DNA"
      node_subtype "GENE"
      node_type "species"
      org_id "sa142"
      uniprot "NA"
    ]
    graphics [
      x 2718.7805189192004
      y 2283.1316429369444
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_84"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 72
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_36"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re33"
      uniprot "NA"
    ]
    graphics [
      x 2882.5
      y 1833.858034494286
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 73
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18075"
      hgnc "NA"
      map_id "M114_139"
      name "dTDP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa60"
      uniprot "NA"
    ]
    graphics [
      x 2441.043215136001
      y 2666.0089009888093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_139"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 74
    zlevel -1

    cd19dm [
      annotation "PUBMED:8024690"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_35"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re32"
      uniprot "NA"
    ]
    graphics [
      x 2081.043215136001
      y 2615.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 75
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17013"
      hgnc "NA"
      map_id "M114_136"
      name "dTMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa58"
      uniprot "NA"
    ]
    graphics [
      x 3182.5
      y 1427.7501341961922
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_136"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 76
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:ncbigene:1841;urn:miriam:ncbigene:1841;urn:miriam:refseq:NM_012145;urn:miriam:hgnc.symbol:DTYMK;urn:miriam:ensembl:ENSG00000168393;urn:miriam:hgnc.symbol:DTYMK;urn:miriam:uniprot:P23919;urn:miriam:hgnc:3061;urn:miriam:ec-code:2.7.4.9"
      hgnc "HGNC_SYMBOL:DTYMK"
      map_id "M114_140"
      name "dTYMK"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa61"
      uniprot "UNIPROT:P23919"
    ]
    graphics [
      x 1428.7805189192002
      y 2182.5398405932747
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_140"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 77
    zlevel -1

    cd19dm [
      annotation "PUBMED:3099389"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_34"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re31"
      uniprot "NA"
    ]
    graphics [
      x 3212.5
      y 2046.17663070123
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 78
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17622"
      hgnc "NA"
      map_id "M114_131"
      name "dUMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa52"
      uniprot "NA"
    ]
    graphics [
      x 1901.043215136001
      y 2732.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_131"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 79
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:uniprot:P04818;urn:miriam:ncbigene:7298;urn:miriam:ncbigene:7298;urn:miriam:hgnc.symbol:TYMS;urn:miriam:refseq:NM_001071;urn:miriam:hgnc.symbol:TYMS;urn:miriam:hgnc:12441;urn:miriam:ensembl:ENSG00000176890;urn:miriam:ec-code:2.1.1.45"
      hgnc "HGNC_SYMBOL:TYMS"
      map_id "M114_137"
      name "TYMS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa59"
      uniprot "UNIPROT:P04818"
    ]
    graphics [
      x 3182.5
      y 1834.4358956619628
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_137"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 80
    zlevel -1

    cd19dm [
      annotation "PUBMED:18837522"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_31"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re28"
      uniprot "NA"
    ]
    graphics [
      x 902.5
      y 1691.7973208312726
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 81
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_65"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re77"
      uniprot "NA"
    ]
    graphics [
      x 2598.7805189192004
      y 2168.4291303703953
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_65"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 82
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15918"
      hgnc "NA"
      map_id "M114_132"
      name "dCMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa53"
      uniprot "NA"
    ]
    graphics [
      x 2072.5
      y 1265.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_132"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 83
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:refseq:NM_001012732;urn:miriam:uniprot:P32321;urn:miriam:ncbigene:1635;urn:miriam:ncbigene:1635;urn:miriam:hgnc:2710;urn:miriam:ensembl:ENSG00000129187;urn:miriam:hgnc.symbol:DCTD;urn:miriam:hgnc.symbol:DCTD;urn:miriam:ec-code:3.5.4.12"
      hgnc "HGNC_SYMBOL:DCTD"
      map_id "M114_135"
      name "DCTD"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa57"
      uniprot "UNIPROT:P32321"
    ]
    graphics [
      x 3482.5
      y 1802.3387624862335
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_135"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 84
    zlevel -1

    cd19dm [
      annotation "PUBMED:13788541"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_32"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re29"
      uniprot "NA"
    ]
    graphics [
      x 212.5
      y 1521.849049957318
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 85
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16311"
      hgnc "NA"
      map_id "M114_129"
      name "dCTP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa48"
      uniprot "NA"
    ]
    graphics [
      x 752.5
      y 1928.3665515504927
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_129"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 86
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:hgnc.symbol:DCTPP1;urn:miriam:hgnc.symbol:DCTPP1;urn:miriam:hgnc:28777;urn:miriam:uniprot:Q9H773;urn:miriam:ensembl:ENSG00000179958;urn:miriam:ec-code:3.6.1.12;urn:miriam:refseq:NM_024096;urn:miriam:ncbigene:79077;urn:miriam:ncbigene:79077"
      hgnc "HGNC_SYMBOL:DCTPP1"
      map_id "M114_134"
      name "DCTPP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa56"
      uniprot "UNIPROT:Q9H773"
    ]
    graphics [
      x 1172.5
      y 1617.3156502059937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_134"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 87
    zlevel -1

    cd19dm [
      annotation "PUBMED:17827303"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_29"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re26"
      uniprot "NA"
    ]
    graphics [
      x 1562.5
      y 765.4971941397312
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 88
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A28846"
      hgnc "NA"
      map_id "M114_127"
      name "dCDP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa46"
      uniprot "NA"
    ]
    graphics [
      x 1802.5
      y 2012.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_127"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 89
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:ensembl:ENSG00000239672;urn:miriam:uniprot:P15531;urn:miriam:ec-code:2.7.4.6;urn:miriam:hgnc:7849;urn:miriam:hgnc.symbol:NME1;urn:miriam:refseq:NM_000269;urn:miriam:hgnc.symbol:NME1;urn:miriam:ncbigene:4830;urn:miriam:ncbigene:4830"
      hgnc "HGNC_SYMBOL:NME1"
      map_id "M114_130"
      name "NME1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa51"
      uniprot "UNIPROT:P15531"
    ]
    graphics [
      x 1262.5
      y 697.7617350228634
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_130"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 90
    zlevel -1

    cd19dm [
      annotation "PUBMED:17827303"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_28"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re25"
      uniprot "NA"
    ]
    graphics [
      x 2312.5
      y 1059.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 91
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A28850"
      hgnc "NA"
      map_id "M114_126"
      name "dUDP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa45"
      uniprot "NA"
    ]
    graphics [
      x 2317.4327087287393
      y 459.06618499081924
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_126"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 92
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17625"
      hgnc "NA"
      map_id "M114_128"
      name "dUTP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa47"
      uniprot "NA"
    ]
    graphics [
      x 2012.5
      y 1415.8526976066757
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_128"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 93
    zlevel -1

    cd19dm [
      annotation "PUBMED:24809024"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_27"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re24"
      uniprot "NA"
    ]
    graphics [
      x 2402.5
      y 1396.0046064486714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 94
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17659"
      hgnc "NA"
      map_id "M114_116"
      name "UDP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa35"
      uniprot "NA"
    ]
    graphics [
      x 2132.5
      y 1835.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_116"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 95
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:brenda:1.17.4.1;urn:miriam:obo.go:GO%3A0005971;urn:miriam:refseq:NM_001034;urn:miriam:ncbigene:6241;urn:miriam:ncbigene:6241;urn:miriam:hgnc:10452;urn:miriam:ec-code:1.17.4.1;urn:miriam:hgnc.symbol:RRM2;urn:miriam:hgnc.symbol:RRM2;urn:miriam:ensembl:ENSG00000171848;urn:miriam:uniprot:P31350;urn:miriam:uniprot:P31350"
      hgnc "HGNC_SYMBOL:RRM2"
      map_id "M114_10"
      name "ribonucleoside_minus_diphosphate_space_reductase"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa25"
      uniprot "UNIPROT:P31350"
    ]
    graphics [
      x 1458.7805189192002
      y 2242.0998194395315
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 96
    zlevel -1

    cd19dm [
      annotation "PUBMED:24809024"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_30"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re27"
      uniprot "NA"
    ]
    graphics [
      x 1338.7805189192002
      y 2395.0755883950264
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 97
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17239"
      hgnc "NA"
      map_id "M114_117"
      name "CDP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa36"
      uniprot "NA"
    ]
    graphics [
      x 1592.5
      y 1373.119912305107
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_117"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 98
    zlevel -1

    cd19dm [
      annotation "PUBMED:10462544"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_22"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re18"
      uniprot "NA"
    ]
    graphics [
      x 2702.5
      y 761.1955197980467
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 99
    zlevel -1

    cd19dm [
      annotation "PUBMED:28458037"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_25"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re20"
      uniprot "NA"
    ]
    graphics [
      x 1532.5
      y 1829.738142841986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 100
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:ensembl:ENSG00000239672;urn:miriam:uniprot:P15531;urn:miriam:ec-code:2.7.4.6;urn:miriam:hgnc:7849;urn:miriam:hgnc.symbol:NME1;urn:miriam:refseq:NM_000269;urn:miriam:hgnc.symbol:NME1;urn:miriam:ncbigene:4830;urn:miriam:ncbigene:4830"
      hgnc "HGNC_SYMBOL:NME1"
      map_id "M114_124"
      name "NME1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa42"
      uniprot "UNIPROT:P15531"
    ]
    graphics [
      x 902.5
      y 1721.7973208312726
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_124"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 101
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17677"
      hgnc "NA"
      map_id "M114_123"
      name "CTP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa41"
      uniprot "NA"
    ]
    graphics [
      x 1052.5
      y 958.8948945564143
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_123"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 102
    zlevel -1

    cd19dm [
      annotation "PUBMED:5411547"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_26"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re21"
      uniprot "NA"
    ]
    graphics [
      x 2418.7805189192004
      y 2426.0089009888093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 103
    zlevel -1

    cd19dm [
      annotation "PUBMED:32531208"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_67"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re86"
      uniprot "NA"
    ]
    graphics [
      x 1172.5
      y 1120.9505467799008
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 104
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_90"
      name "RTC_space_and_space_transcription_space_pw"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa162"
      uniprot "NA"
    ]
    graphics [
      x 1502.5
      y 2017.3397236403869
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_90"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 105
    zlevel -1

    cd19dm [
      annotation "PUBMED:32531208"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_68"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re87"
      uniprot "NA"
    ]
    graphics [
      x 2672.5
      y 1923.7928516911866
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_68"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 106
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.go:GO%3A0032774"
      hgnc "NA"
      map_id "M114_82"
      name "RNA_space_biosynthesis"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa140"
      uniprot "NA"
    ]
    graphics [
      x 2732.5
      y 906.1787316983737
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_82"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 107
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15713"
      hgnc "NA"
      map_id "M114_122"
      name "UTP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa40"
      uniprot "NA"
    ]
    graphics [
      x 1514.8466319862935
      y 2783.9635883168285
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_122"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 108
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:ncbigene:1503;urn:miriam:ncbigene:1503;urn:miriam:ec-code:6.3.4.2;urn:miriam:refseq:NM_001905;urn:miriam:uniprot:P17812;urn:miriam:hgnc.symbol:CTPS1;urn:miriam:hgnc.symbol:CTPS1;urn:miriam:ensembl:ENSG00000171793;urn:miriam:hgnc:2519"
      hgnc "HGNC_SYMBOL:CTPS1"
      map_id "M114_125"
      name "CTPS1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa43"
      uniprot "UNIPROT:P17812"
    ]
    graphics [
      x 2561.043215136001
      y 2591.1584053190477
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_125"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 109
    zlevel -1

    cd19dm [
      annotation "PUBMED:28458037"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_23"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re19"
      uniprot "NA"
    ]
    graphics [
      x 752.5
      y 1746.579031299839
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 110
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17361"
      hgnc "NA"
      map_id "M114_115"
      name "CMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa34"
      uniprot "NA"
    ]
    graphics [
      x 2462.5
      y 1951.435024234829
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_115"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 111
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:hgnc.symbol:CMPK1;urn:miriam:hgnc.symbol:CMPK1;urn:miriam:hgnc:18170;urn:miriam:ensembl:ENSG00000162368;urn:miriam:ec-code:2.7.4.14;urn:miriam:ec-code:2.7.4.6;urn:miriam:ncbigene:51727;urn:miriam:ncbigene:51727;urn:miriam:uniprot:P30085;urn:miriam:refseq:NM_016308"
      hgnc "HGNC_SYMBOL:CMPK1"
      map_id "M114_120"
      name "CMPK"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa39"
      uniprot "UNIPROT:P30085"
    ]
    graphics [
      x 1657.432708728739
      y 507.18556879462244
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_120"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 112
    zlevel -1

    cd19dm [
      annotation "PUBMED:10462544"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_21"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re17"
      uniprot "NA"
    ]
    graphics [
      x 1202.5
      y 1617.3156502059937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 113
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A57865"
      hgnc "NA"
      map_id "M114_107"
      name "UMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa27"
      uniprot "NA"
    ]
    graphics [
      x 332.5
      y 2105.9106481566537
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_107"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 114
    zlevel -1

    cd19dm [
      annotation "PUBMED:15130468"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_17"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re12"
      uniprot "NA"
    ]
    graphics [
      x 212.5
      y 1963.153046392792
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 115
    zlevel -1

    cd19dm [
      annotation "REACTOME:REACT_1698"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_71"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re9"
      uniprot "NA"
    ]
    graphics [
      x 842.5
      y 1004.6921688379064
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_71"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 116
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15842"
      hgnc "NA"
      map_id "M114_104"
      name "orotidine_space_5'_minus_monophosphate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa23"
      uniprot "NA"
    ]
    graphics [
      x 752.5
      y 1072.6097915613125
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_104"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 117
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:ncbigene:7372;urn:miriam:ec-code:2.4.2.10;urn:miriam:refseq:NM_000373;urn:miriam:hgnc.symbol:UMPS;urn:miriam:hgnc:12563;urn:miriam:ensembl:ENSG00000114491;urn:miriam:uniprot:P11172;urn:miriam:ec-code:4.1.1.23"
      hgnc "HGNC_SYMBOL:UMPS"
      map_id "M114_105"
      name "UMPS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa25"
      uniprot "UNIPROT:P11172"
    ]
    graphics [
      x 362.5
      y 1343.0730545379486
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_105"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 118
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16526"
      hgnc "NA"
      map_id "M114_106"
      name "CO2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa26"
      uniprot "NA"
    ]
    graphics [
      x 2012.5
      y 1865.8526976066757
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_106"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 119
    zlevel -1

    cd19dm [
      annotation "REACTOME:REACT_1698"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_61"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re7"
      uniprot "NA"
    ]
    graphics [
      x 1802.5
      y 1292.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_61"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 120
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30839"
      hgnc "NA"
      map_id "M114_138"
      name "orotate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa6"
      uniprot "NA"
    ]
    graphics [
      x 2462.5
      y 1771.435024234829
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_138"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 121
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17111"
      hgnc "NA"
      map_id "M114_109"
      name "PRPP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa29"
      uniprot "NA"
    ]
    graphics [
      x 722.5
      y 1098.582096328788
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_109"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 122
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18361"
      hgnc "NA"
      map_id "M114_108"
      name "PPi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa28"
      uniprot "NA"
    ]
    graphics [
      x 692.5
      y 1681.8473988697738
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_108"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 123
    zlevel -1

    cd19dm [
      annotation "REACTOME:REACT_1698"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_49"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re5"
      uniprot "NA"
    ]
    graphics [
      x 3422.5
      y 1819.7629776640294
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_49"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 124
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A30864"
      hgnc "NA"
      map_id "M114_121"
      name "(S)_minus_dihydroorotate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa4"
      uniprot "NA"
    ]
    graphics [
      x 2312.5
      y 1899.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_121"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 125
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16389"
      hgnc "NA"
      map_id "M114_102"
      name "CoQ"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa21"
      uniprot "NA"
    ]
    graphics [
      x 3362.5
      y 2045.376563522507
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_102"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 126
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:ncbigene:1723;urn:miriam:hgnc.symbol:DHODH;urn:miriam:hgnc:2867"
      hgnc "HGNC_SYMBOL:DHODH"
      map_id "M114_103"
      name "dihydroorotate_space_dehydrogenase_space_holoenzyme"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa22"
      uniprot "NA"
    ]
    graphics [
      x 1788.7805189192002
      y 2192.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_103"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 127
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:pubmed:31740051"
      hgnc "NA"
      map_id "M114_142"
      name "P1788"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa64"
      uniprot "NA"
    ]
    graphics [
      x 3542.5
      y 1734.2528232882507
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_142"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 128
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:drugbank:DB08880"
      hgnc "NA"
      map_id "M114_144"
      name "Teriflunomide"
      node_subtype "DRUG"
      node_type "species"
      org_id "sa76"
      uniprot "NA"
    ]
    graphics [
      x 2462.5
      y 1801.435024234829
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_144"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 129
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17976"
      hgnc "NA"
      map_id "M114_101"
      name "QH2"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa20"
      uniprot "NA"
    ]
    graphics [
      x 1592.5
      y 1223.119912305107
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_101"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 130
    zlevel -1

    cd19dm [
      annotation "PUBMED:29221122"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_66"
      name "NA"
      node_subtype "TRIGGER"
      node_type "reaction"
      org_id "re85"
      uniprot "NA"
    ]
    graphics [
      x 2192.5
      y 683.7133098641311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_66"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 131
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_91"
      name "DNA_space_damage"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa163"
      uniprot "NA"
    ]
    graphics [
      x 3122.5
      y 1237.7596167211623
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_91"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 132
    zlevel -1

    cd19dm [
      annotation "REACTOME:REACT_1698"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_39"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re4"
      uniprot "NA"
    ]
    graphics [
      x 2688.7805189192004
      y 2494.180704320391
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 133
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A32814"
      hgnc "NA"
      map_id "M114_110"
      name "N_minus_carbamoyl_minus_L_minus_aspartate"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa3"
      uniprot "NA"
    ]
    graphics [
      x 2868.7805189192004
      y 2396.1611807055756
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_110"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 134
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15378"
      hgnc "NA"
      map_id "M114_92"
      name "H_underscore_plus"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa17"
      uniprot "NA"
    ]
    graphics [
      x 2402.5
      y 1946.0089009888093
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_92"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 135
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:refseq:NM_001306079;urn:miriam:ec-code:3.5.2.3;urn:miriam:hgnc:1424;urn:miriam:hgnc.symbol:CAD;urn:miriam:uniprot:P27708;urn:miriam:ncbigene:790;urn:miriam:ensembl:ENSG00000084774;urn:miriam:ec-code:6.3.5.5;urn:miriam:ec-code:2.1.3.2"
      hgnc "HGNC_SYMBOL:CAD"
      map_id "M114_99"
      name "CAD"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa19"
      uniprot "UNIPROT:P27708"
    ]
    graphics [
      x 2522.5
      y 1562.9715035857598
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_99"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 136
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "M114_98"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa18"
      uniprot "NA"
    ]
    graphics [
      x 2342.5
      y 1322.963502381953
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_98"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 137
    zlevel -1

    cd19dm [
      annotation "REACTOME:REACT_1698"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_33"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re3"
      uniprot "NA"
    ]
    graphics [
      x 1728.7805189192002
      y 2307.1855687946227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 138
    zlevel -1

    cd19dm [
      annotation "REACTOME:REACT_73577"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_24"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re2"
      uniprot "NA"
    ]
    graphics [
      x 602.5
      y 1209.295395563012
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 139
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18050"
      hgnc "NA"
      map_id "M114_77"
      name "L_minus_Gln"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa1"
      uniprot "NA"
    ]
    graphics [
      x 1532.5
      y 959.738142841986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_77"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 140
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17544"
      hgnc "NA"
      map_id "M114_143"
      name "HCO3_underscore_minus"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa7"
      uniprot "NA"
    ]
    graphics [
      x 1502.5
      y 626.4103423706738
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_143"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 141
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15377"
      hgnc "NA"
      map_id "M114_147"
      name "H2O"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa8"
      uniprot "NA"
    ]
    graphics [
      x 888.7805189192002
      y 2231.3781918298628
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_147"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 142
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15422"
      hgnc "NA"
      map_id "M114_158"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa9"
      uniprot "NA"
    ]
    graphics [
      x 1712.5
      y 747.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_158"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 143
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17672"
      hgnc "NA"
      map_id "M114_100"
      name "CAP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa2"
      uniprot "NA"
    ]
    graphics [
      x 1202.5
      y 1377.3156502059937
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_100"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 144
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16015"
      hgnc "NA"
      map_id "M114_78"
      name "L_minus_Glu"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa10"
      uniprot "NA"
    ]
    graphics [
      x 662.5
      y 2211.224286585304
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_78"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 145
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16761"
      hgnc "NA"
      map_id "M114_80"
      name "ADP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa13"
      uniprot "NA"
    ]
    graphics [
      x 1870.182406995184
      y 1572.7477316329034
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_80"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 146
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18367"
      hgnc "NA"
      map_id "M114_81"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa14"
      uniprot "NA"
    ]
    graphics [
      x 1022.5
      y 1117.8045420565481
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_81"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 147
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A29991"
      hgnc "NA"
      map_id "M114_89"
      name "L_minus_Asp"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa16"
      uniprot "NA"
    ]
    graphics [
      x 1052.5
      y 1195.3176960101873
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_89"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 148
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A18367"
      hgnc "NA"
      map_id "M114_88"
      name "Pi"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa15"
      uniprot "NA"
    ]
    graphics [
      x 1278.7805189192002
      y 2376.9780694277015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_88"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 149
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16704"
      hgnc "NA"
      map_id "M114_111"
      name "uridine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa30"
      uniprot "NA"
    ]
    graphics [
      x 1682.5
      y 1377.1855687946224
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_111"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 150
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:ncbigene:7371;urn:miriam:hgnc.symbol:UCK1;urn:miriam:ncbigene:7371;urn:miriam:uniprot:Q9HA47;urn:miriam:refseq:NM_012474;urn:miriam:hgnc:12562;urn:miriam:uniprot:Q9BZX2;urn:miriam:ncbigene:83549;urn:miriam:uniprot:Q9BZX2;urn:miriam:hgnc.symbol:UCK2;urn:miriam:hgnc.symbol:UCK2;urn:miriam:ec-code:2.7.1.48;urn:miriam:ensembl:ENSG00000143179"
      hgnc "HGNC_SYMBOL:UCK1;HGNC_SYMBOL:UCK2"
      map_id "M114_114"
      name "UCK2"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa33"
      uniprot "UNIPROT:Q9HA47;UNIPROT:Q9BZX2"
    ]
    graphics [
      x 1352.5
      y 1029.311461366438
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_114"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 151
    zlevel -1

    cd19dm [
      annotation "PUBMED:15130468"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_18"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re14"
      uniprot "NA"
    ]
    graphics [
      x 2898.7805189192004
      y 2207.7034190231884
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_18"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 152
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17562"
      hgnc "NA"
      map_id "M114_112"
      name "cytidine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa31"
      uniprot "NA"
    ]
    graphics [
      x 1548.7805189192002
      y 2429.738142841986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_112"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 153
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_20"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re16"
      uniprot "NA"
    ]
    graphics [
      x 2171.043215136001
      y 2663.713309864131
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 154
    zlevel -1

    cd19dm [
      annotation "PUBMED:7923172;PUBMED:15689149"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_16"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re11"
      uniprot "NA"
    ]
    graphics [
      x 2342.5
      y 1232.963502381953
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 155
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:uniprot:P32320;urn:miriam:ncbigene:978;urn:miriam:refseq:NM_001785;urn:miriam:ensembl:ENSG00000158825;urn:miriam:ec-code:3.5.4.5;urn:miriam:hgnc.symbol:CDA;urn:miriam:hgnc:1712"
      hgnc "HGNC_SYMBOL:CDA"
      map_id "M114_113"
      name "CDA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa32"
      uniprot "UNIPROT:P32320"
    ]
    graphics [
      x 2642.5
      y 707.7682304796128
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_113"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 156
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A17562"
      hgnc "NA"
      map_id "M114_119"
      name "cytidine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa38"
      uniprot "NA"
    ]
    graphics [
      x 2192.5
      y 1583.7133098641311
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_119"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 157
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_19"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re15"
      uniprot "NA"
    ]
    graphics [
      x 2268.7805189192004
      y 2399.8927857516264
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 158
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A16704"
      hgnc "NA"
      map_id "M114_118"
      name "uridine"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa37"
      uniprot "NA"
    ]
    graphics [
      x 2231.043215136001
      y 2663.713309864131
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_118"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 159
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:refseq:NM_001025248;urn:miriam:ncbigene:1854;urn:miriam:ncbigene:1854;urn:miriam:hgnc:3078;urn:miriam:uniprot:P33316;urn:miriam:ensembl:ENSG00000128951;urn:miriam:hgnc.symbol:DUT;urn:miriam:hgnc.symbol:DUT;urn:miriam:ec-code:3.6.1.23"
      hgnc "HGNC_SYMBOL:DUT"
      map_id "M114_133"
      name "DUT"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa54"
      uniprot "UNIPROT:P33316"
    ]
    graphics [
      x 272.5
      y 1568.608199685056
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_133"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 160
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:ensembl:ENSG00000164430;urn:miriam:hgnc:21367;urn:miriam:ncbigene:115004;urn:miriam:uniprot:Q8N884;urn:miriam:hgnc.symbol:CGAS;urn:miriam:refseq:NM_138441;urn:miriam:ec-code:2.7.7.86"
      hgnc "HGNC_SYMBOL:CGAS"
      map_id "M114_146"
      name "cGAS"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa78"
      uniprot "UNIPROT:Q8N884"
    ]
    graphics [
      x 1885.5437299022901
      y 2822.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_146"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 161
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:pubmed:28363908;urn:miriam:ensembl:ENSG00000164430;urn:miriam:hgnc:21367;urn:miriam:ncbigene:115004;urn:miriam:uniprot:Q8N884;urn:miriam:hgnc.symbol:CGAS;urn:miriam:refseq:NM_138441;urn:miriam:ec-code:2.7.7.86;urn:miriam:obo.chebi:CHEBI%3A75909"
      hgnc "HGNC_SYMBOL:CGAS"
      map_id "M114_1"
      name "cGAS:dsDNA"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa1"
      uniprot "UNIPROT:Q8N884"
    ]
    graphics [
      x 1903.6642091719086
      y 2162.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 162
    zlevel -1

    cd19dm [
      annotation "PUBMED:29622565"
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_38"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re39"
      uniprot "NA"
    ]
    graphics [
      x 2988.7805189192004
      y 2414.936290989684
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 163
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15422"
      hgnc "NA"
      map_id "M114_149"
      name "ATP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa81"
      uniprot "NA"
    ]
    graphics [
      x 1278.7805189192002
      y 2286.9780694277015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_149"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 164
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A15996"
      hgnc "NA"
      map_id "M114_150"
      name "GTP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa82"
      uniprot "NA"
    ]
    graphics [
      x 3302.5
      y 2201.387702487932
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_150"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 165
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "urn:miriam:obo.chebi:CHEBI%3A75947"
      hgnc "NA"
      map_id "M114_151"
      name "cGAMP"
      node_subtype "SIMPLE_MOLECULE"
      node_type "species"
      org_id "sa83"
      uniprot "NA"
    ]
    graphics [
      x 3212.5
      y 1881.3422926161
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_151"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 166
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:Pyrimidine deprivation"
      full_annotation "NA"
      hgnc "NA"
      map_id "M114_97"
      name "s152"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa179"
      uniprot "NA"
    ]
    graphics [
      x 1112.5
      y 805.6654693121959
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M114_97"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 167
    source 2
    target 1
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_40"
      target_id "M114_11"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 168
    source 1
    target 3
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_11"
      target_id "M114_42"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 169
    source 1
    target 4
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_11"
      target_id "M114_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 170
    source 165
    target 2
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_151"
      target_id "M114_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 171
    source 55
    target 2
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_166"
      target_id "M114_40"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 172
    source 3
    target 15
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_42"
      target_id "M114_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 173
    source 5
    target 4
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "M114_161"
      target_id "M114_55"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 174
    source 4
    target 6
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_55"
      target_id "M114_3"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 175
    source 6
    target 7
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_3"
      target_id "M114_51"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 176
    source 7
    target 8
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_51"
      target_id "M114_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 177
    source 8
    target 9
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_5"
      target_id "M114_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 178
    source 10
    target 9
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_163"
      target_id "M114_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 179
    source 11
    target 9
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "M114_162"
      target_id "M114_56"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 180
    source 9
    target 12
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_56"
      target_id "M114_4"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 181
    source 12
    target 13
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_4"
      target_id "M114_69"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 182
    source 13
    target 14
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_69"
      target_id "M114_95"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 183
    source 15
    target 16
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_12"
      target_id "M114_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 184
    source 17
    target 16
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_148"
      target_id "M114_41"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 185
    source 16
    target 18
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_41"
      target_id "M114_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 186
    source 17
    target 19
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_148"
      target_id "M114_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 187
    source 19
    target 18
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_58"
      target_id "M114_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 188
    source 18
    target 20
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_7"
      target_id "M114_43"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 189
    source 53
    target 19
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_79"
      target_id "M114_58"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 190
    source 20
    target 21
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_43"
      target_id "M114_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 191
    source 21
    target 22
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "M114_13"
      target_id "M114_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 192
    source 23
    target 22
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_152"
      target_id "M114_54"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 193
    source 22
    target 24
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_54"
      target_id "M114_153"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 194
    source 24
    target 25
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_153"
      target_id "M114_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 195
    source 26
    target 25
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_6"
      target_id "M114_63"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 196
    source 25
    target 27
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_63"
      target_id "M114_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 197
    source 51
    target 26
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_44"
      target_id "M114_6"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 198
    source 27
    target 28
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M114_15"
      target_id "M114_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 199
    source 27
    target 29
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M114_15"
      target_id "M114_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 200
    source 50
    target 28
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_86"
      target_id "M114_62"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 201
    source 28
    target 49
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_62"
      target_id "M114_156"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 202
    source 30
    target 29
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_87"
      target_id "M114_50"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 203
    source 29
    target 31
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_50"
      target_id "M114_157"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 204
    source 31
    target 32
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_157"
      target_id "M114_48"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 205
    source 32
    target 33
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_48"
      target_id "M114_160"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 206
    source 33
    target 34
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_160"
      target_id "M114_46"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 207
    source 34
    target 35
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_46"
      target_id "M114_155"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 208
    source 35
    target 36
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_155"
      target_id "M114_72"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 209
    source 35
    target 37
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_155"
      target_id "M114_75"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 210
    source 35
    target 38
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_155"
      target_id "M114_73"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 211
    source 36
    target 45
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_72"
      target_id "M114_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 212
    source 37
    target 46
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_75"
      target_id "M114_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 213
    source 38
    target 39
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_73"
      target_id "M114_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 214
    source 40
    target 39
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_70"
      target_id "M114_96"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 215
    source 41
    target 40
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_154"
      target_id "M114_70"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 216
    source 42
    target 41
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_45"
      target_id "M114_154"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 217
    source 41
    target 43
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_154"
      target_id "M114_76"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 218
    source 41
    target 44
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_154"
      target_id "M114_74"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 219
    source 47
    target 42
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_159"
      target_id "M114_45"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 220
    source 43
    target 46
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_76"
      target_id "M114_94"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 221
    source 44
    target 45
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_74"
      target_id "M114_93"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 222
    source 48
    target 47
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_47"
      target_id "M114_159"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 223
    source 49
    target 48
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_156"
      target_id "M114_47"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 224
    source 52
    target 51
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_14"
      target_id "M114_44"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 225
    source 54
    target 53
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_57"
      target_id "M114_79"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 226
    source 55
    target 54
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_166"
      target_id "M114_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 227
    source 56
    target 54
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "M114_9"
      target_id "M114_57"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 228
    source 57
    target 56
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_59"
      target_id "M114_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 229
    source 58
    target 57
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_2"
      target_id "M114_59"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 230
    source 59
    target 58
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_53"
      target_id "M114_2"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 231
    source 60
    target 59
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_165"
      target_id "M114_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 232
    source 61
    target 59
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_8"
      target_id "M114_53"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 233
    source 62
    target 61
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_52"
      target_id "M114_8"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 234
    source 63
    target 62
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_145"
      target_id "M114_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 235
    source 64
    target 62
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_164"
      target_id "M114_52"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 236
    source 65
    target 63
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_64"
      target_id "M114_145"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 237
    source 63
    target 66
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_145"
      target_id "M114_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 238
    source 63
    target 67
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_145"
      target_id "M114_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 239
    source 166
    target 65
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_97"
      target_id "M114_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 240
    source 131
    target 65
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M114_91"
      target_id "M114_64"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 241
    source 160
    target 66
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_146"
      target_id "M114_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 242
    source 66
    target 161
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_37"
      target_id "M114_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 243
    source 68
    target 67
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_141"
      target_id "M114_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 244
    source 69
    target 67
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "M114_83"
      target_id "M114_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 245
    source 70
    target 67
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "INHIBITION"
      source_id "M114_85"
      target_id "M114_60"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 246
    source 67
    target 71
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_60"
      target_id "M114_84"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 247
    source 72
    target 68
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_36"
      target_id "M114_141"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 248
    source 73
    target 72
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_139"
      target_id "M114_36"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 249
    source 74
    target 73
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_35"
      target_id "M114_139"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 250
    source 75
    target 74
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_136"
      target_id "M114_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 251
    source 76
    target 74
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "M114_140"
      target_id "M114_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 252
    source 77
    target 75
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_34"
      target_id "M114_136"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 253
    source 78
    target 77
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_131"
      target_id "M114_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 254
    source 79
    target 77
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "M114_137"
      target_id "M114_34"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 255
    source 80
    target 78
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_31"
      target_id "M114_131"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 256
    source 81
    target 78
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_65"
      target_id "M114_131"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 257
    source 92
    target 80
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_128"
      target_id "M114_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 258
    source 159
    target 80
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "M114_133"
      target_id "M114_31"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 259
    source 82
    target 81
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_132"
      target_id "M114_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 260
    source 83
    target 81
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "M114_135"
      target_id "M114_65"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 261
    source 84
    target 82
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_32"
      target_id "M114_132"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 262
    source 85
    target 84
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_129"
      target_id "M114_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 263
    source 86
    target 84
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "M114_134"
      target_id "M114_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 264
    source 87
    target 85
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_29"
      target_id "M114_129"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 265
    source 88
    target 87
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_127"
      target_id "M114_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 266
    source 89
    target 87
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "M114_130"
      target_id "M114_29"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 267
    source 96
    target 88
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_30"
      target_id "M114_127"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 268
    source 89
    target 90
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "M114_130"
      target_id "M114_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 269
    source 91
    target 90
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_126"
      target_id "M114_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 270
    source 90
    target 92
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_28"
      target_id "M114_128"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 271
    source 93
    target 91
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_27"
      target_id "M114_126"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 272
    source 94
    target 93
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_116"
      target_id "M114_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 273
    source 95
    target 93
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "M114_10"
      target_id "M114_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 274
    source 112
    target 94
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_21"
      target_id "M114_116"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 275
    source 94
    target 109
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_116"
      target_id "M114_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 276
    source 95
    target 96
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "M114_10"
      target_id "M114_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 277
    source 97
    target 96
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_117"
      target_id "M114_30"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 278
    source 98
    target 97
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_22"
      target_id "M114_117"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 279
    source 97
    target 99
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_117"
      target_id "M114_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 280
    source 110
    target 98
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_115"
      target_id "M114_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 281
    source 111
    target 98
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "M114_120"
      target_id "M114_22"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 282
    source 100
    target 99
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "M114_124"
      target_id "M114_25"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 283
    source 99
    target 101
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_25"
      target_id "M114_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 284
    source 100
    target 109
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "M114_124"
      target_id "M114_23"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 285
    source 102
    target 101
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_26"
      target_id "M114_123"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 286
    source 101
    target 103
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_123"
      target_id "M114_67"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 287
    source 107
    target 102
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_122"
      target_id "M114_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 288
    source 108
    target 102
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "M114_125"
      target_id "M114_26"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 289
    source 103
    target 104
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_67"
      target_id "M114_90"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 290
    source 104
    target 105
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_90"
      target_id "M114_68"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 291
    source 105
    target 106
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_68"
      target_id "M114_82"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 292
    source 109
    target 107
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_23"
      target_id "M114_122"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 293
    source 151
    target 110
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_18"
      target_id "M114_115"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 294
    source 111
    target 112
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "M114_120"
      target_id "M114_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 295
    source 113
    target 112
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_107"
      target_id "M114_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 296
    source 114
    target 113
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_17"
      target_id "M114_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 297
    source 115
    target 113
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_71"
      target_id "M114_107"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 298
    source 149
    target 114
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_111"
      target_id "M114_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 299
    source 150
    target 114
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "M114_114"
      target_id "M114_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 300
    source 116
    target 115
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_104"
      target_id "M114_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 301
    source 117
    target 115
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "M114_105"
      target_id "M114_71"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 302
    source 115
    target 118
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_71"
      target_id "M114_106"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 303
    source 119
    target 116
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_61"
      target_id "M114_104"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 304
    source 117
    target 119
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "M114_105"
      target_id "M114_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 305
    source 120
    target 119
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_138"
      target_id "M114_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 306
    source 121
    target 119
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_109"
      target_id "M114_61"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 307
    source 119
    target 122
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_61"
      target_id "M114_108"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 308
    source 123
    target 120
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_49"
      target_id "M114_138"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 309
    source 124
    target 123
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_121"
      target_id "M114_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 310
    source 125
    target 123
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_102"
      target_id "M114_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 311
    source 126
    target 123
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "M114_103"
      target_id "M114_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 312
    source 127
    target 123
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "INHIBITION"
      source_id "M114_142"
      target_id "M114_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 313
    source 128
    target 123
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "INHIBITION"
      source_id "M114_144"
      target_id "M114_49"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 314
    source 123
    target 129
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_49"
      target_id "M114_101"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 315
    source 132
    target 124
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_39"
      target_id "M114_121"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 316
    source 128
    target 130
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_144"
      target_id "M114_66"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 317
    source 130
    target 131
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_66"
      target_id "M114_91"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 318
    source 133
    target 132
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_110"
      target_id "M114_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 319
    source 134
    target 132
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_92"
      target_id "M114_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 320
    source 135
    target 132
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "M114_99"
      target_id "M114_39"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 321
    source 132
    target 136
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_39"
      target_id "M114_98"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 322
    source 137
    target 133
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_33"
      target_id "M114_110"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 323
    source 135
    target 137
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "M114_99"
      target_id "M114_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 324
    source 135
    target 138
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "M114_99"
      target_id "M114_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 325
    source 143
    target 137
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_100"
      target_id "M114_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 326
    source 147
    target 137
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_89"
      target_id "M114_33"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 327
    source 137
    target 148
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_33"
      target_id "M114_88"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 328
    source 139
    target 138
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_77"
      target_id "M114_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 329
    source 140
    target 138
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_143"
      target_id "M114_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 330
    source 141
    target 138
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_147"
      target_id "M114_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 331
    source 142
    target 138
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_158"
      target_id "M114_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 332
    source 138
    target 143
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_24"
      target_id "M114_100"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 333
    source 138
    target 144
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_24"
      target_id "M114_78"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 334
    source 138
    target 145
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_24"
      target_id "M114_80"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 335
    source 138
    target 146
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_24"
      target_id "M114_81"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 336
    source 154
    target 149
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_16"
      target_id "M114_111"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 337
    source 157
    target 149
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_19"
      target_id "M114_111"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 338
    source 150
    target 151
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "M114_114"
      target_id "M114_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 339
    source 152
    target 151
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_112"
      target_id "M114_18"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 340
    source 153
    target 152
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_20"
      target_id "M114_112"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 341
    source 152
    target 154
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_112"
      target_id "M114_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 342
    source 156
    target 153
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_119"
      target_id "M114_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 343
    source 155
    target 154
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "M114_113"
      target_id "M114_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 344
    source 158
    target 157
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_118"
      target_id "M114_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 345
    source 161
    target 162
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CATALYSIS"
      source_id "M114_1"
      target_id "M114_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 346
    source 163
    target 162
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_149"
      target_id "M114_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 347
    source 164
    target 162
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "CONSPUMPTION"
      source_id "M114_150"
      target_id "M114_38"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 348
    source 162
    target 165
    cd19dm [
      diagram "C19DMap:Pyrimidine deprivation"
      edge_type "PRODUCTION"
      source_id "M114_38"
      target_id "M114_151"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
