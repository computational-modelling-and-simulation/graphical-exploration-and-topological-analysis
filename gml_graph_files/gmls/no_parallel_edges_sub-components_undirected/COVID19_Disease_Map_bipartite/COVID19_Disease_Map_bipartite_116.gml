# generated with VANTED V2.8.2 at Fri Mar 04 10:04:34 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:hgnc:10436;urn:miriam:uniprot:P23443;urn:miriam:uniprot:P23443;urn:miriam:ec-code:2.7.11.1;urn:miriam:ncbigene:6198;urn:miriam:ncbigene:6198;urn:miriam:ensembl:ENSG00000108443;urn:miriam:hgnc.symbol:RPS6KB1;urn:miriam:hgnc.symbol:RPS6KB1;urn:miriam:refseq:NM_003161"
      hgnc "HGNC_SYMBOL:RPS6KB1"
      map_id "M19_29"
      name "RPS6KB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa41"
      uniprot "UNIPROT:P23443"
    ]
    graphics [
      x 2762.5
      y 2125.826736405558
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_29"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_12"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re10"
      uniprot "NA"
    ]
    graphics [
      x 3092.5
      y 1084.7559928623186
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_12"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:ncbigene:5518;urn:miriam:uniprot:P30153;urn:miriam:uniprot:P30153;urn:miriam:ncbigene:5518;urn:miriam:refseq:NM_014225;urn:miriam:hgnc:9302;urn:miriam:hgnc.symbol:PPP2R1A;urn:miriam:hgnc.symbol:PPP2R1A;urn:miriam:ensembl:ENSG00000105568;urn:miriam:ncbigene:5515;urn:miriam:ncbigene:5515;urn:miriam:ensembl:ENSG00000113575;urn:miriam:refseq:NM_002715;urn:miriam:ec-code:3.1.3.16;urn:miriam:uniprot:P67775;urn:miriam:uniprot:P67775;urn:miriam:hgnc:9299;urn:miriam:hgnc.symbol:PPP2CA;urn:miriam:hgnc.symbol:PPP2CA"
      hgnc "HGNC_SYMBOL:PPP2R1A;HGNC_SYMBOL:PPP2CA"
      map_id "M19_5"
      name "PP2A"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa16"
      uniprot "UNIPROT:P30153;UNIPROT:P67775"
    ]
    graphics [
      x 3152.5
      y 1533.699630803535
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_5"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 4
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:hgnc:10436;urn:miriam:uniprot:P23443;urn:miriam:uniprot:P23443;urn:miriam:ec-code:2.7.11.1;urn:miriam:ncbigene:6198;urn:miriam:ncbigene:6198;urn:miriam:ensembl:ENSG00000108443;urn:miriam:hgnc.symbol:RPS6KB1;urn:miriam:hgnc.symbol:RPS6KB1;urn:miriam:refseq:NM_003161"
      hgnc "HGNC_SYMBOL:RPS6KB1"
      map_id "M19_28"
      name "RPS6KB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa40"
      uniprot "UNIPROT:P23443"
    ]
    graphics [
      x 2912.5
      y 763.6513650409242
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_28"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 5
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_21"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re8"
      uniprot "NA"
    ]
    graphics [
      x 3422.5
      y 1996.5844186682982
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_21"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 6
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:ncbigene:5518;urn:miriam:uniprot:P30153;urn:miriam:uniprot:P30153;urn:miriam:ncbigene:5518;urn:miriam:refseq:NM_014225;urn:miriam:hgnc:9302;urn:miriam:hgnc.symbol:PPP2R1A;urn:miriam:hgnc.symbol:PPP2R1A;urn:miriam:ensembl:ENSG00000105568;urn:miriam:ncbigene:5515;urn:miriam:ncbigene:5515;urn:miriam:ensembl:ENSG00000113575;urn:miriam:refseq:NM_002715;urn:miriam:ec-code:3.1.3.16;urn:miriam:uniprot:P67775;urn:miriam:uniprot:P67775;urn:miriam:hgnc:9299;urn:miriam:hgnc.symbol:PPP2CA;urn:miriam:hgnc.symbol:PPP2CA"
      hgnc "HGNC_SYMBOL:PPP2R1A;HGNC_SYMBOL:PPP2CA"
      map_id "M19_4"
      name "PP2A"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa15"
      uniprot "UNIPROT:P30153;UNIPROT:P67775"
    ]
    graphics [
      x 2852.5
      y 1909.2792916844683
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_4"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 7
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:refseq:NM_000660;urn:miriam:ncbigene:7040;urn:miriam:ncbigene:7040;urn:miriam:hgnc:11766;urn:miriam:hgnc.symbol:TGFB1;urn:miriam:uniprot:P01137;urn:miriam:uniprot:P01137;urn:miriam:hgnc.symbol:TGFB1;urn:miriam:ensembl:ENSG00000105329"
      hgnc "HGNC_SYMBOL:TGFB1"
      map_id "M19_1"
      name "TGFB_slash_TGFBR"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa11"
      uniprot "UNIPROT:P01137"
    ]
    graphics [
      x 3092.5
      y 1960.1860833815003
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_1"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 8
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:uniprot:P59635;urn:miriam:ncbigene:1489674"
      hgnc "NA"
      map_id "M19_41"
      name "Orf7a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa84"
      uniprot "UNIPROT:P59635"
    ]
    graphics [
      x 2462.5
      y 1621.435024234829
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_41"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 9
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_17"
      name "NA"
      node_subtype "HETERODIMER_ASSOCIATION"
      node_type "reaction"
      org_id "re32"
      uniprot "NA"
    ]
    graphics [
      x 1597.432708728739
      y 563.1199123051069
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_17"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 10
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_20"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re7"
      uniprot "NA"
    ]
    graphics [
      x 3542.5
      y 1609.1746391513316
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_20"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 11
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_19"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re6"
      uniprot "NA"
    ]
    graphics [
      x 2748.7805189192004
      y 2371.9741938860507
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_19"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 12
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:ensembl:ENSG00000175387;urn:miriam:hgnc:6768;urn:miriam:ncbigene:4087;urn:miriam:uniprot:Q15796;urn:miriam:uniprot:Q15796;urn:miriam:ncbigene:4087;urn:miriam:hgnc.symbol:SMAD2;urn:miriam:hgnc.symbol:SMAD2;urn:miriam:refseq:NM_005901;urn:miriam:hgnc:6769;urn:miriam:ncbigene:4088;urn:miriam:ncbigene:4088;urn:miriam:uniprot:P84022;urn:miriam:uniprot:P84022;urn:miriam:hgnc.symbol:SMAD3;urn:miriam:refseq:NM_005902;urn:miriam:hgnc.symbol:SMAD3;urn:miriam:ensembl:ENSG00000166949"
      hgnc "HGNC_SYMBOL:SMAD2;HGNC_SYMBOL:SMAD3"
      map_id "M19_8"
      name "SMAD2_slash_3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa4"
      uniprot "UNIPROT:Q15796;UNIPROT:P84022"
    ]
    graphics [
      x 3452.5
      y 1875.1001068356327
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_8"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 13
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:uniprot:Q13145;urn:miriam:uniprot:Q13145;urn:miriam:refseq:NM_012342;urn:miriam:ensembl:ENSG00000095739;urn:miriam:hgnc.symbol:BAMBI;urn:miriam:ncbigene:25805;urn:miriam:hgnc.symbol:BAMBI;urn:miriam:ncbigene:25805;urn:miriam:hgnc:30251"
      hgnc "HGNC_SYMBOL:BAMBI"
      map_id "M19_26"
      name "BAMBI"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa33"
      uniprot "UNIPROT:Q13145"
    ]
    graphics [
      x 782.5
      y 1327.835840060344
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_26"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 14
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:uniprot:P62877;urn:miriam:uniprot:P62877;urn:miriam:ec-code:2.3.2.27;urn:miriam:refseq:NM_014248;urn:miriam:ec-code:2.3.2.32;urn:miriam:hgnc:9928;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:hgnc.symbol:RBX1;urn:miriam:ncbigene:9978;urn:miriam:ensembl:ENSG00000100387"
      hgnc "HGNC_SYMBOL:RBX1"
      map_id "M19_24"
      name "RBX1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa11"
      uniprot "UNIPROT:P62877"
    ]
    graphics [
      x 2021.043215136001
      y 2822.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 15
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:ncbigene:90;urn:miriam:ncbigene:90;urn:miriam:hgnc:171;urn:miriam:ec-code:2.7.11.30;urn:miriam:uniprot:Q04771;urn:miriam:uniprot:Q04771;urn:miriam:hgnc.symbol:ACVR1;urn:miriam:refseq:NM_001105;urn:miriam:hgnc.symbol:ACVR1;urn:miriam:ensembl:ENSG00000115170"
      hgnc "HGNC_SYMBOL:ACVR1"
      map_id "M19_30"
      name "ACVR1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa44"
      uniprot "UNIPROT:Q04771"
    ]
    graphics [
      x 2282.5
      y 909.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_30"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 16
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:hgnc.symbol:ACVR1B;urn:miriam:uniprot:P36896;urn:miriam:uniprot:P36896;urn:miriam:hgnc.symbol:ACVR1B;urn:miriam:ec-code:2.7.11.30;urn:miriam:ncbigene:91;urn:miriam:ncbigene:91;urn:miriam:hgnc:172;urn:miriam:ensembl:ENSG00000135503;urn:miriam:refseq:NM_020328"
      hgnc "HGNC_SYMBOL:ACVR1B"
      map_id "M19_31"
      name "ACVR1B"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa45"
      uniprot "UNIPROT:P36896"
    ]
    graphics [
      x 1952.5
      y 1562.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_31"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 17
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:ec-code:2.7.11.24;urn:miriam:ensembl:ENSG00000102882;urn:miriam:hgnc:6877;urn:miriam:hgnc.symbol:MAPK3;urn:miriam:hgnc.symbol:MAPK3;urn:miriam:refseq:NM_001040056;urn:miriam:ncbigene:5595;urn:miriam:ncbigene:5595;urn:miriam:uniprot:P27361;urn:miriam:uniprot:P27361"
      hgnc "HGNC_SYMBOL:MAPK3"
      map_id "M19_25"
      name "MAPK3"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa18"
      uniprot "UNIPROT:P27361"
    ]
    graphics [
      x 1548.7805189192002
      y 2399.738142841986
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_25"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 18
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:uniprot:P59632;urn:miriam:ncbigene:1489669"
      hgnc "NA"
      map_id "M19_34"
      name "Orf3a"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa65"
      uniprot "UNIPROT:P59632"
    ]
    graphics [
      x 1970.6404712427336
      y 3062.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_34"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 19
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:uniprot:Q7TFA1;urn:miriam:ncbigene:1489675"
      hgnc "NA"
      map_id "M19_33"
      name "Nsp7b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa64"
      uniprot "UNIPROT:Q7TFA1"
    ]
    graphics [
      x 2312.5
      y 1929.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_33"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 20
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:ensembl:ENSG00000175387;urn:miriam:hgnc:6768;urn:miriam:ncbigene:4087;urn:miriam:uniprot:Q15796;urn:miriam:uniprot:Q15796;urn:miriam:ncbigene:4087;urn:miriam:hgnc.symbol:SMAD2;urn:miriam:hgnc.symbol:SMAD2;urn:miriam:refseq:NM_005901;urn:miriam:hgnc:6769;urn:miriam:ncbigene:4088;urn:miriam:ncbigene:4088;urn:miriam:uniprot:P84022;urn:miriam:uniprot:P84022;urn:miriam:hgnc.symbol:SMAD3;urn:miriam:refseq:NM_005902;urn:miriam:hgnc.symbol:SMAD3;urn:miriam:ensembl:ENSG00000166949"
      hgnc "HGNC_SYMBOL:SMAD2;HGNC_SYMBOL:SMAD3"
      map_id "M19_9"
      name "SMAD2_slash_3"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa5"
      uniprot "UNIPROT:Q15796;UNIPROT:P84022"
    ]
    graphics [
      x 798.7805189192002
      y 2438.8075159969617
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_9"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 21
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_14"
      name "NA"
      node_subtype "PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "re14"
      uniprot "NA"
    ]
    graphics [
      x 1068.7805189192002
      y 2258.5794444430944
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_14"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 22
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:obo.go:GO%3A0000074"
      hgnc "NA"
      map_id "M19_32"
      name "Modulation_space_of_space_cell_space_cycle"
      node_subtype "PHENOTYPE"
      node_type "species"
      org_id "sa55"
      uniprot "NA"
    ]
    graphics [
      x 648.7805189192002
      y 2483.3889144488658
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_32"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 23
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_13"
      name "NA"
      node_subtype "PHYSICAL_STIMULATION"
      node_type "reaction"
      org_id "re13"
      uniprot "NA"
    ]
    graphics [
      x 1608.7805189192002
      y 2213.119912305107
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_13"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 24
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:hgnc:6767;urn:miriam:ncbigene:4086;urn:miriam:uniprot:Q15797;urn:miriam:uniprot:Q15797;urn:miriam:ncbigene:4086;urn:miriam:ensembl:ENSG00000170365;urn:miriam:hgnc.symbol:SMAD1;urn:miriam:refseq:NM_005900;urn:miriam:hgnc.symbol:SMAD1;urn:miriam:hgnc:6774;urn:miriam:ncbigene:4093;urn:miriam:ncbigene:4093;urn:miriam:hgnc.symbol:SMAD9;urn:miriam:ensembl:ENSG00000120693;urn:miriam:hgnc.symbol:SMAD9;urn:miriam:refseq:NM_005905;urn:miriam:uniprot:O15198;urn:miriam:uniprot:O15198;urn:miriam:uniprot:Q99717;urn:miriam:uniprot:Q99717;urn:miriam:hgnc.symbol:SMAD5;urn:miriam:hgnc.symbol:SMAD5;urn:miriam:refseq:NM_005903;urn:miriam:hgnc:6771;urn:miriam:ncbigene:4090;urn:miriam:ncbigene:4090;urn:miriam:ensembl:ENSG00000113658"
      hgnc "HGNC_SYMBOL:SMAD1;HGNC_SYMBOL:SMAD9;HGNC_SYMBOL:SMAD5"
      map_id "M19_7"
      name "SMAD1_slash_5_slash_8"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa3"
      uniprot "UNIPROT:Q15797;UNIPROT:O15198;UNIPROT:Q99717"
    ]
    graphics [
      x 1098.7805189192002
      y 2260.7083768520633
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_7"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 25
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_16"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re3"
      uniprot "NA"
    ]
    graphics [
      x 1871.043215136001
      y 2882.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_16"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 26
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:hgnc:6767;urn:miriam:ncbigene:4086;urn:miriam:uniprot:Q15797;urn:miriam:uniprot:Q15797;urn:miriam:ncbigene:4086;urn:miriam:ensembl:ENSG00000170365;urn:miriam:hgnc.symbol:SMAD1;urn:miriam:refseq:NM_005900;urn:miriam:hgnc.symbol:SMAD1;urn:miriam:uniprot:Q99717;urn:miriam:uniprot:Q99717;urn:miriam:hgnc.symbol:SMAD5;urn:miriam:hgnc.symbol:SMAD5;urn:miriam:refseq:NM_005903;urn:miriam:hgnc:6771;urn:miriam:ncbigene:4090;urn:miriam:ncbigene:4090;urn:miriam:ensembl:ENSG00000113658;urn:miriam:hgnc:6774;urn:miriam:ncbigene:4093;urn:miriam:ncbigene:4093;urn:miriam:hgnc.symbol:SMAD9;urn:miriam:ensembl:ENSG00000120693;urn:miriam:hgnc.symbol:SMAD9;urn:miriam:refseq:NM_005905;urn:miriam:uniprot:O15198;urn:miriam:uniprot:O15198"
      hgnc "HGNC_SYMBOL:SMAD1;HGNC_SYMBOL:SMAD5;HGNC_SYMBOL:SMAD9"
      map_id "M19_6"
      name "SMAD1_slash_5_slash_8"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa2"
      uniprot "UNIPROT:Q15797;UNIPROT:Q99717;UNIPROT:O15198"
    ]
    graphics [
      x 1818.7805189192002
      y 2372.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_6"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 27
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:hgnc:169;urn:miriam:hgnc.symbol:ACTR2;urn:miriam:hgnc.symbol:ACTR2;urn:miriam:uniprot:P61160;urn:miriam:uniprot:P61160;urn:miriam:ncbigene:10097;urn:miriam:ncbigene:10097;urn:miriam:ensembl:ENSG00000138071;urn:miriam:refseq:NM_001005386;urn:miriam:ncbigene:659;urn:miriam:ncbigene:659;urn:miriam:ensembl:ENSG00000204217;urn:miriam:hgnc:1078;urn:miriam:ec-code:2.7.11.30;urn:miriam:uniprot:Q13873;urn:miriam:uniprot:Q13873;urn:miriam:refseq:NM_001204;urn:miriam:hgnc.symbol:BMPR2;urn:miriam:hgnc.symbol:BMPR2"
      hgnc "HGNC_SYMBOL:ACTR2;HGNC_SYMBOL:BMPR2"
      map_id "M19_10"
      name "BMPR1_slash_2_slash_ACTR2"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa7"
      uniprot "UNIPROT:P61160;UNIPROT:Q13873"
    ]
    graphics [
      x 2298.7805189192004
      y 2199.0661849908192
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_10"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 28
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:uniprot:Q7TFA1;urn:miriam:ncbigene:1489675"
      hgnc "NA"
      map_id "M19_38"
      name "Nsp7b"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa70"
      uniprot "UNIPROT:Q7TFA1"
    ]
    graphics [
      x 2748.7805189192004
      y 2281.7961035072476
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_38"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 29
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:uniprot:P59637;urn:miriam:ncbigene:1489671;urn:miriam:hgnc.symbol:E"
      hgnc "HGNC_SYMBOL:E"
      map_id "M19_36"
      name "E"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa69"
      uniprot "UNIPROT:P59637"
    ]
    graphics [
      x 1737.867992679123
      y 3032.5
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_36"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 30
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_15"
      name "NA"
      node_subtype "STATE_TRANSITION"
      node_type "reaction"
      org_id "re20"
      uniprot "NA"
    ]
    graphics [
      x 1052.5
      y 2078.5794444430944
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_15"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 31
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "NA"
      hgnc "NA"
      map_id "M19_35"
      name "sa44_underscore_degraded"
      node_subtype "DEGRADED"
      node_type "species"
      org_id "sa66"
      uniprot "NA"
    ]
    graphics [
      x 1721.043215136001
      y 2607.1855687946227
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_35"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 32
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:hgnc:667;urn:miriam:refseq:NM_001664;urn:miriam:ensembl:ENSG00000067560;urn:miriam:ec-code:3.6.5.2;urn:miriam:ncbigene:387;urn:miriam:ncbigene:387;urn:miriam:uniprot:P61586;urn:miriam:uniprot:P61586;urn:miriam:hgnc.symbol:RHOA;urn:miriam:hgnc.symbol:RHOA"
      hgnc "HGNC_SYMBOL:RHOA"
      map_id "M19_22"
      name "RHOA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa1"
      uniprot "UNIPROT:P61586"
    ]
    graphics [
      x 3242.5
      y 1096.660071086727
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_22"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 33
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:uniprot:Q7TFA1;urn:miriam:ncbigene:1489675"
      hgnc "NA"
      map_id "M19_40"
      name "Nsp7"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa76"
      uniprot "UNIPROT:Q7TFA1"
    ]
    graphics [
      x 2102.5
      y 1325.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_40"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 34
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:hgnc:667;urn:miriam:refseq:NM_001664;urn:miriam:ensembl:ENSG00000067560;urn:miriam:ec-code:3.6.5.2;urn:miriam:ncbigene:387;urn:miriam:ncbigene:387;urn:miriam:uniprot:P61586;urn:miriam:uniprot:P61586;urn:miriam:hgnc.symbol:RHOA;urn:miriam:hgnc.symbol:RHOA"
      hgnc "HGNC_SYMBOL:RHOA"
      map_id "M19_27"
      name "RHOA"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa38"
      uniprot "UNIPROT:P61586"
    ]
    graphics [
      x 3272.5
      y 1099.9470895919978
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_27"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 35
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:refseq:NM_000660;urn:miriam:ncbigene:7040;urn:miriam:ncbigene:7040;urn:miriam:hgnc:11766;urn:miriam:hgnc.symbol:TGFB1;urn:miriam:uniprot:P01137;urn:miriam:uniprot:P01137;urn:miriam:hgnc.symbol:TGFB1;urn:miriam:ensembl:ENSG00000105329"
      hgnc "HGNC_SYMBOL:TGFB1"
      map_id "M19_23"
      name "TGFB1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa10"
      uniprot "UNIPROT:P01137"
    ]
    graphics [
      x 2140.913546386292
      y 965.7909944672015
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_23"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 36
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:ec-code:2.7.11.30;urn:miriam:refseq:NM_001024847;urn:miriam:hgnc:11773;urn:miriam:ensembl:ENSG00000163513;urn:miriam:ncbigene:7048;urn:miriam:ncbigene:7048;urn:miriam:uniprot:P37173;urn:miriam:uniprot:P37173;urn:miriam:hgnc.symbol:TGFBR2;urn:miriam:hgnc.symbol:TGFBR2;urn:miriam:ensembl:ENSG00000106799;urn:miriam:uniprot:P36897;urn:miriam:uniprot:P36897;urn:miriam:ncbigene:7046;urn:miriam:ncbigene:7046;urn:miriam:ec-code:2.7.11.30;urn:miriam:hgnc:11772;urn:miriam:hgnc.symbol:TGFBR1;urn:miriam:hgnc.symbol:TGFBR1;urn:miriam:refseq:NM_001130916"
      hgnc "HGNC_SYMBOL:TGFBR2;HGNC_SYMBOL:TGFBR1"
      map_id "M19_3"
      name "TGFBR"
      node_subtype "COMPLEX"
      node_type "species"
      org_id "csa13"
      uniprot "UNIPROT:P37173;UNIPROT:P36897"
    ]
    graphics [
      x 3002.5
      y 950.4293325034714
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_3"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 37
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:refseq:NM_206943;urn:miriam:hgnc.symbol:LTBP1;urn:miriam:ncbigene:4052;urn:miriam:hgnc.symbol:LTBP1;urn:miriam:ncbigene:4052;urn:miriam:uniprot:Q14766;urn:miriam:uniprot:Q14766;urn:miriam:hgnc:6714;urn:miriam:ensembl:ENSG00000049323"
      hgnc "HGNC_SYMBOL:LTBP1"
      map_id "M19_37"
      name "LTBP1"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa7"
      uniprot "UNIPROT:Q14766"
    ]
    graphics [
      x 2252.5
      y 1262.4583143700918
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 38
    zlevel -1

    cd19dm [
      count 1
      diagram "C19DMap:TGFbeta signalling"
      full_annotation "urn:miriam:uniprot:Q80H93;urn:miriam:uniprot:Q7TFA0;urn:miriam:ncbigene:1489677;urn:miriam:ncbigene:1489676"
      hgnc "NA"
      map_id "M19_39"
      name "Orf8"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "sa72"
      uniprot "UNIPROT:Q80H93;UNIPROT:Q7TFA0"
    ]
    graphics [
      x 2582.5
      y 753.5767619391758
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "M19_39"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 39
    source 1
    target 2
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "M19_29"
      target_id "M19_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 40
    source 3
    target 2
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CATALYSIS"
      source_id "M19_5"
      target_id "M19_12"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 41
    source 2
    target 4
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PRODUCTION"
      source_id "M19_12"
      target_id "M19_28"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 42
    source 5
    target 3
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PRODUCTION"
      source_id "M19_21"
      target_id "M19_5"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 43
    source 6
    target 5
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "M19_4"
      target_id "M19_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 44
    source 7
    target 5
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M19_1"
      target_id "M19_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 45
    source 8
    target 5
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M19_41"
      target_id "M19_21"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 46
    source 9
    target 7
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PRODUCTION"
      source_id "M19_17"
      target_id "M19_1"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 47
    source 7
    target 10
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M19_1"
      target_id "M19_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 48
    source 7
    target 11
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M19_1"
      target_id "M19_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 49
    source 35
    target 9
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "M19_23"
      target_id "M19_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 50
    source 36
    target 9
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "M19_3"
      target_id "M19_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 51
    source 37
    target 9
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "INHIBITION"
      source_id "M19_37"
      target_id "M19_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 52
    source 13
    target 9
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "INHIBITION"
      source_id "M19_26"
      target_id "M19_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 53
    source 38
    target 9
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M19_39"
      target_id "M19_17"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 54
    source 32
    target 10
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "M19_22"
      target_id "M19_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 55
    source 33
    target 10
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M19_40"
      target_id "M19_20"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 56
    source 10
    target 34
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PRODUCTION"
      source_id "M19_20"
      target_id "M19_27"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 57
    source 12
    target 11
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "M19_8"
      target_id "M19_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 58
    source 13
    target 11
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "INHIBITION"
      source_id "M19_26"
      target_id "M19_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 59
    source 14
    target 11
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "INHIBITION"
      source_id "M19_24"
      target_id "M19_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 60
    source 15
    target 11
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CATALYSIS"
      source_id "M19_30"
      target_id "M19_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 61
    source 16
    target 11
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CATALYSIS"
      source_id "M19_31"
      target_id "M19_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 62
    source 17
    target 11
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "INHIBITION"
      source_id "M19_25"
      target_id "M19_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 63
    source 18
    target 11
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M19_34"
      target_id "M19_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 64
    source 19
    target 11
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M19_33"
      target_id "M19_19"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 65
    source 11
    target 20
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PRODUCTION"
      source_id "M19_19"
      target_id "M19_9"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 66
    source 13
    target 30
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "M19_26"
      target_id "M19_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 67
    source 13
    target 25
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "INHIBITION"
      source_id "M19_26"
      target_id "M19_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 68
    source 17
    target 25
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "INHIBITION"
      source_id "M19_25"
      target_id "M19_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 69
    source 18
    target 30
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M19_34"
      target_id "M19_15"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 70
    source 18
    target 25
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M19_34"
      target_id "M19_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 71
    source 20
    target 21
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "M19_9"
      target_id "M19_14"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 72
    source 21
    target 22
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PRODUCTION"
      source_id "M19_14"
      target_id "M19_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 73
    source 23
    target 22
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PRODUCTION"
      source_id "M19_13"
      target_id "M19_32"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 74
    source 24
    target 23
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "M19_7"
      target_id "M19_13"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 75
    source 25
    target 24
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PRODUCTION"
      source_id "M19_16"
      target_id "M19_7"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 76
    source 26
    target 25
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CONSPUMPTION"
      source_id "M19_6"
      target_id "M19_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 77
    source 27
    target 25
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "CATALYSIS"
      source_id "M19_10"
      target_id "M19_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 78
    source 28
    target 25
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M19_38"
      target_id "M19_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 79
    source 29
    target 25
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PHYSICAL_STIMULATION"
      source_id "M19_36"
      target_id "M19_16"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 80
    source 30
    target 31
    cd19dm [
      diagram "C19DMap:TGFbeta signalling"
      edge_type "PRODUCTION"
      source_id "M19_15"
      target_id "M19_35"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
