# generated with VANTED V2.8.2 at Fri Mar 04 10:04:33 AEDT 2022
graph [
  directed 0
  node [
    id 1
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:uniprot:P02671"
      hgnc "NA"
      map_id "W14_67"
      name "FGA_space_(A_alpha_)"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "f7650"
      uniprot "UNIPROT:P02671"
    ]
    graphics [
      x 2672.5
      y 1446.9352187264367
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_67"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 2
    zlevel -1

    cd19dm [
      annotation "NA"
      count 1
      diagram "WP4927"
      full_annotation "NA"
      hgnc "NA"
      map_id "W14_24"
      name "NA"
      node_subtype "UNKNOWN_TRANSITION"
      node_type "reaction"
      org_id "bf421"
      uniprot "NA"
    ]
    graphics [
      x 3602.5
      y 1758.2080750796795
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_24"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  node [
    id 3
    zlevel -1

    cd19dm [
      count 1
      diagram "WP4927"
      full_annotation "urn:miriam:ncbiprotein:NP_000499.1"
      hgnc "NA"
      map_id "W14_37"
      name "FGA_space_(A_alpha_E)"
      node_subtype "PROTEIN"
      node_type "species"
      org_id "d3c11"
      uniprot "NA"
    ]
    graphics [
      x 2882.5
      y 1440.091819165615
      w 25.0
      h 25.0
      fill "#FFFFFF"
      outline "#000000"
      frameThickness 2.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      type "rectangle"
    ]
    label "W14_37"
    labelgraphics [
      alignment "center"
      anchor "c"
      color "#000000"
      fontName "Arial"
      fontSize 12
      fontStyle "plain"
      labelOffset [
        x 0.0
        y 0.0
      ]
      type "text"
    ]
  ]
  edge [
    id 4
    source 1
    target 2
    cd19dm [
      diagram "WP4927"
      edge_type "CONSPUMPTION"
      source_id "W14_67"
      target_id "W14_24"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
  edge [
    id 5
    source 2
    target 3
    cd19dm [
      diagram "WP4927"
      edge_type "PRODUCTION"
      source_id "W14_24"
      target_id "W14_37"
    ]
    graphics [
      fill "#000000"
      outline "#000000"
      arrow "last"
      frameThickness 1.0
      gradient 0.0
      opacity 1.0
      rounding 5.0
      thickness 1.0
    ]
  ]
]
